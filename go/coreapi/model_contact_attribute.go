/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the ContactAttribute type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ContactAttribute{}

// ContactAttribute struct for ContactAttribute
type ContactAttribute struct {
	Key string `json:"key"`
	Type string `json:"type"`
	EntityType *string `json:"entity_type,omitempty"`
	Label *string `json:"label,omitempty"`
}

// NewContactAttribute instantiates a new ContactAttribute object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewContactAttribute(key string, type_ string) *ContactAttribute {
	this := ContactAttribute{}
	this.Key = key
	this.Type = type_
	return &this
}

// NewContactAttributeWithDefaults instantiates a new ContactAttribute object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewContactAttributeWithDefaults() *ContactAttribute {
	this := ContactAttribute{}
	return &this
}

// GetKey returns the Key field value
func (o *ContactAttribute) GetKey() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Key
}

// GetKeyOk returns a tuple with the Key field value
// and a boolean to check if the value has been set.
func (o *ContactAttribute) GetKeyOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Key, true
}

// SetKey sets field value
func (o *ContactAttribute) SetKey(v string) {
	o.Key = v
}

// GetType returns the Type field value
func (o *ContactAttribute) GetType() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Type
}

// GetTypeOk returns a tuple with the Type field value
// and a boolean to check if the value has been set.
func (o *ContactAttribute) GetTypeOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Type, true
}

// SetType sets field value
func (o *ContactAttribute) SetType(v string) {
	o.Type = v
}

// GetEntityType returns the EntityType field value if set, zero value otherwise.
func (o *ContactAttribute) GetEntityType() string {
	if o == nil || IsNil(o.EntityType) {
		var ret string
		return ret
	}
	return *o.EntityType
}

// GetEntityTypeOk returns a tuple with the EntityType field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContactAttribute) GetEntityTypeOk() (*string, bool) {
	if o == nil || IsNil(o.EntityType) {
		return nil, false
	}
	return o.EntityType, true
}

// HasEntityType returns a boolean if a field has been set.
func (o *ContactAttribute) HasEntityType() bool {
	if o != nil && !IsNil(o.EntityType) {
		return true
	}

	return false
}

// SetEntityType gets a reference to the given string and assigns it to the EntityType field.
func (o *ContactAttribute) SetEntityType(v string) {
	o.EntityType = &v
}

// GetLabel returns the Label field value if set, zero value otherwise.
func (o *ContactAttribute) GetLabel() string {
	if o == nil || IsNil(o.Label) {
		var ret string
		return ret
	}
	return *o.Label
}

// GetLabelOk returns a tuple with the Label field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContactAttribute) GetLabelOk() (*string, bool) {
	if o == nil || IsNil(o.Label) {
		return nil, false
	}
	return o.Label, true
}

// HasLabel returns a boolean if a field has been set.
func (o *ContactAttribute) HasLabel() bool {
	if o != nil && !IsNil(o.Label) {
		return true
	}

	return false
}

// SetLabel gets a reference to the given string and assigns it to the Label field.
func (o *ContactAttribute) SetLabel(v string) {
	o.Label = &v
}

func (o ContactAttribute) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ContactAttribute) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["key"] = o.Key
	toSerialize["type"] = o.Type
	if !IsNil(o.EntityType) {
		toSerialize["entity_type"] = o.EntityType
	}
	if !IsNil(o.Label) {
		toSerialize["label"] = o.Label
	}
	return toSerialize, nil
}

type NullableContactAttribute struct {
	value *ContactAttribute
	isSet bool
}

func (v NullableContactAttribute) Get() *ContactAttribute {
	return v.value
}

func (v *NullableContactAttribute) Set(val *ContactAttribute) {
	v.value = val
	v.isSet = true
}

func (v NullableContactAttribute) IsSet() bool {
	return v.isSet
}

func (v *NullableContactAttribute) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableContactAttribute(val *ContactAttribute) *NullableContactAttribute {
	return &NullableContactAttribute{value: val, isSet: true}
}

func (v NullableContactAttribute) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableContactAttribute) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


