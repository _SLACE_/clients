/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the WebhookEventComChannel type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &WebhookEventComChannel{}

// WebhookEventComChannel struct for WebhookEventComChannel
type WebhookEventComChannel struct {
	Id string `json:"id"`
	ContactId string `json:"contact_id"`
	Name string `json:"name"`
	Type string `json:"type"`
}

// NewWebhookEventComChannel instantiates a new WebhookEventComChannel object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewWebhookEventComChannel(id string, contactId string, name string, type_ string) *WebhookEventComChannel {
	this := WebhookEventComChannel{}
	this.Id = id
	this.ContactId = contactId
	this.Name = name
	this.Type = type_
	return &this
}

// NewWebhookEventComChannelWithDefaults instantiates a new WebhookEventComChannel object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewWebhookEventComChannelWithDefaults() *WebhookEventComChannel {
	this := WebhookEventComChannel{}
	return &this
}

// GetId returns the Id field value
func (o *WebhookEventComChannel) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *WebhookEventComChannel) GetIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *WebhookEventComChannel) SetId(v string) {
	o.Id = v
}

// GetContactId returns the ContactId field value
func (o *WebhookEventComChannel) GetContactId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ContactId
}

// GetContactIdOk returns a tuple with the ContactId field value
// and a boolean to check if the value has been set.
func (o *WebhookEventComChannel) GetContactIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ContactId, true
}

// SetContactId sets field value
func (o *WebhookEventComChannel) SetContactId(v string) {
	o.ContactId = v
}

// GetName returns the Name field value
func (o *WebhookEventComChannel) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *WebhookEventComChannel) GetNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *WebhookEventComChannel) SetName(v string) {
	o.Name = v
}

// GetType returns the Type field value
func (o *WebhookEventComChannel) GetType() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Type
}

// GetTypeOk returns a tuple with the Type field value
// and a boolean to check if the value has been set.
func (o *WebhookEventComChannel) GetTypeOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Type, true
}

// SetType sets field value
func (o *WebhookEventComChannel) SetType(v string) {
	o.Type = v
}

func (o WebhookEventComChannel) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o WebhookEventComChannel) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["id"] = o.Id
	toSerialize["contact_id"] = o.ContactId
	toSerialize["name"] = o.Name
	toSerialize["type"] = o.Type
	return toSerialize, nil
}

type NullableWebhookEventComChannel struct {
	value *WebhookEventComChannel
	isSet bool
}

func (v NullableWebhookEventComChannel) Get() *WebhookEventComChannel {
	return v.value
}

func (v *NullableWebhookEventComChannel) Set(val *WebhookEventComChannel) {
	v.value = val
	v.isSet = true
}

func (v NullableWebhookEventComChannel) IsSet() bool {
	return v.isSet
}

func (v *NullableWebhookEventComChannel) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableWebhookEventComChannel(val *WebhookEventComChannel) *NullableWebhookEventComChannel {
	return &NullableWebhookEventComChannel{value: val, isSet: true}
}

func (v NullableWebhookEventComChannel) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableWebhookEventComChannel) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


