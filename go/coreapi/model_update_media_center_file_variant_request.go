/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the UpdateMediaCenterFileVariantRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &UpdateMediaCenterFileVariantRequest{}

// UpdateMediaCenterFileVariantRequest struct for UpdateMediaCenterFileVariantRequest
type UpdateMediaCenterFileVariantRequest struct {
	Url *string `json:"url,omitempty"`
}

// NewUpdateMediaCenterFileVariantRequest instantiates a new UpdateMediaCenterFileVariantRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewUpdateMediaCenterFileVariantRequest() *UpdateMediaCenterFileVariantRequest {
	this := UpdateMediaCenterFileVariantRequest{}
	return &this
}

// NewUpdateMediaCenterFileVariantRequestWithDefaults instantiates a new UpdateMediaCenterFileVariantRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewUpdateMediaCenterFileVariantRequestWithDefaults() *UpdateMediaCenterFileVariantRequest {
	this := UpdateMediaCenterFileVariantRequest{}
	return &this
}

// GetUrl returns the Url field value if set, zero value otherwise.
func (o *UpdateMediaCenterFileVariantRequest) GetUrl() string {
	if o == nil || IsNil(o.Url) {
		var ret string
		return ret
	}
	return *o.Url
}

// GetUrlOk returns a tuple with the Url field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UpdateMediaCenterFileVariantRequest) GetUrlOk() (*string, bool) {
	if o == nil || IsNil(o.Url) {
		return nil, false
	}
	return o.Url, true
}

// HasUrl returns a boolean if a field has been set.
func (o *UpdateMediaCenterFileVariantRequest) HasUrl() bool {
	if o != nil && !IsNil(o.Url) {
		return true
	}

	return false
}

// SetUrl gets a reference to the given string and assigns it to the Url field.
func (o *UpdateMediaCenterFileVariantRequest) SetUrl(v string) {
	o.Url = &v
}

func (o UpdateMediaCenterFileVariantRequest) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o UpdateMediaCenterFileVariantRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Url) {
		toSerialize["url"] = o.Url
	}
	return toSerialize, nil
}

type NullableUpdateMediaCenterFileVariantRequest struct {
	value *UpdateMediaCenterFileVariantRequest
	isSet bool
}

func (v NullableUpdateMediaCenterFileVariantRequest) Get() *UpdateMediaCenterFileVariantRequest {
	return v.value
}

func (v *NullableUpdateMediaCenterFileVariantRequest) Set(val *UpdateMediaCenterFileVariantRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableUpdateMediaCenterFileVariantRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableUpdateMediaCenterFileVariantRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableUpdateMediaCenterFileVariantRequest(val *UpdateMediaCenterFileVariantRequest) *NullableUpdateMediaCenterFileVariantRequest {
	return &NullableUpdateMediaCenterFileVariantRequest{value: val, isSet: true}
}

func (v NullableUpdateMediaCenterFileVariantRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableUpdateMediaCenterFileVariantRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


