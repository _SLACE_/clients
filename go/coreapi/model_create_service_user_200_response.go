/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the CreateServiceUser200Response type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &CreateServiceUser200Response{}

// CreateServiceUser200Response struct for CreateServiceUser200Response
type CreateServiceUser200Response struct {
	AccessToken *string `json:"access_token,omitempty"`
}

// NewCreateServiceUser200Response instantiates a new CreateServiceUser200Response object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCreateServiceUser200Response() *CreateServiceUser200Response {
	this := CreateServiceUser200Response{}
	return &this
}

// NewCreateServiceUser200ResponseWithDefaults instantiates a new CreateServiceUser200Response object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCreateServiceUser200ResponseWithDefaults() *CreateServiceUser200Response {
	this := CreateServiceUser200Response{}
	return &this
}

// GetAccessToken returns the AccessToken field value if set, zero value otherwise.
func (o *CreateServiceUser200Response) GetAccessToken() string {
	if o == nil || IsNil(o.AccessToken) {
		var ret string
		return ret
	}
	return *o.AccessToken
}

// GetAccessTokenOk returns a tuple with the AccessToken field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateServiceUser200Response) GetAccessTokenOk() (*string, bool) {
	if o == nil || IsNil(o.AccessToken) {
		return nil, false
	}
	return o.AccessToken, true
}

// HasAccessToken returns a boolean if a field has been set.
func (o *CreateServiceUser200Response) HasAccessToken() bool {
	if o != nil && !IsNil(o.AccessToken) {
		return true
	}

	return false
}

// SetAccessToken gets a reference to the given string and assigns it to the AccessToken field.
func (o *CreateServiceUser200Response) SetAccessToken(v string) {
	o.AccessToken = &v
}

func (o CreateServiceUser200Response) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o CreateServiceUser200Response) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.AccessToken) {
		toSerialize["access_token"] = o.AccessToken
	}
	return toSerialize, nil
}

type NullableCreateServiceUser200Response struct {
	value *CreateServiceUser200Response
	isSet bool
}

func (v NullableCreateServiceUser200Response) Get() *CreateServiceUser200Response {
	return v.value
}

func (v *NullableCreateServiceUser200Response) Set(val *CreateServiceUser200Response) {
	v.value = val
	v.isSet = true
}

func (v NullableCreateServiceUser200Response) IsSet() bool {
	return v.isSet
}

func (v *NullableCreateServiceUser200Response) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCreateServiceUser200Response(val *CreateServiceUser200Response) *NullableCreateServiceUser200Response {
	return &NullableCreateServiceUser200Response{value: val, isSet: true}
}

func (v NullableCreateServiceUser200Response) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCreateServiceUser200Response) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


