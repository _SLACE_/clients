/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the WebhookEventMessageDataLocation type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &WebhookEventMessageDataLocation{}

// WebhookEventMessageDataLocation struct for WebhookEventMessageDataLocation
type WebhookEventMessageDataLocation struct {
	Latitude float32 `json:"latitude"`
	Longitude float32 `json:"longitude"`
}

// NewWebhookEventMessageDataLocation instantiates a new WebhookEventMessageDataLocation object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewWebhookEventMessageDataLocation(latitude float32, longitude float32) *WebhookEventMessageDataLocation {
	this := WebhookEventMessageDataLocation{}
	this.Latitude = latitude
	this.Longitude = longitude
	return &this
}

// NewWebhookEventMessageDataLocationWithDefaults instantiates a new WebhookEventMessageDataLocation object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewWebhookEventMessageDataLocationWithDefaults() *WebhookEventMessageDataLocation {
	this := WebhookEventMessageDataLocation{}
	return &this
}

// GetLatitude returns the Latitude field value
func (o *WebhookEventMessageDataLocation) GetLatitude() float32 {
	if o == nil {
		var ret float32
		return ret
	}

	return o.Latitude
}

// GetLatitudeOk returns a tuple with the Latitude field value
// and a boolean to check if the value has been set.
func (o *WebhookEventMessageDataLocation) GetLatitudeOk() (*float32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Latitude, true
}

// SetLatitude sets field value
func (o *WebhookEventMessageDataLocation) SetLatitude(v float32) {
	o.Latitude = v
}

// GetLongitude returns the Longitude field value
func (o *WebhookEventMessageDataLocation) GetLongitude() float32 {
	if o == nil {
		var ret float32
		return ret
	}

	return o.Longitude
}

// GetLongitudeOk returns a tuple with the Longitude field value
// and a boolean to check if the value has been set.
func (o *WebhookEventMessageDataLocation) GetLongitudeOk() (*float32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Longitude, true
}

// SetLongitude sets field value
func (o *WebhookEventMessageDataLocation) SetLongitude(v float32) {
	o.Longitude = v
}

func (o WebhookEventMessageDataLocation) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o WebhookEventMessageDataLocation) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["latitude"] = o.Latitude
	toSerialize["longitude"] = o.Longitude
	return toSerialize, nil
}

type NullableWebhookEventMessageDataLocation struct {
	value *WebhookEventMessageDataLocation
	isSet bool
}

func (v NullableWebhookEventMessageDataLocation) Get() *WebhookEventMessageDataLocation {
	return v.value
}

func (v *NullableWebhookEventMessageDataLocation) Set(val *WebhookEventMessageDataLocation) {
	v.value = val
	v.isSet = true
}

func (v NullableWebhookEventMessageDataLocation) IsSet() bool {
	return v.isSet
}

func (v *NullableWebhookEventMessageDataLocation) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableWebhookEventMessageDataLocation(val *WebhookEventMessageDataLocation) *NullableWebhookEventMessageDataLocation {
	return &NullableWebhookEventMessageDataLocation{value: val, isSet: true}
}

func (v NullableWebhookEventMessageDataLocation) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableWebhookEventMessageDataLocation) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


