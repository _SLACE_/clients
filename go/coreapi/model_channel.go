/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
	"time"
)

// checks if the Channel type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &Channel{}

// Channel struct for Channel
type Channel struct {
	Id string `json:"id"`
	Name string `json:"name"`
	Timezone *string `json:"timezone,omitempty"`
	DefaultLanguage *string `json:"default_language,omitempty"`
	Readonly bool `json:"readonly"`
	AttributionEnabled *bool `json:"attribution_enabled,omitempty"`
	StopKeywords []string `json:"stop_keywords"`
	Tags []ChannelTagsInner `json:"tags,omitempty"`
	Customer Customer `json:"customer"`
	Organization *Organization `json:"organization,omitempty"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	FallbackLanguage *string `json:"fallback_language,omitempty"`
	DefaultDialog360Tier *Dialog360Tier `json:"default_dialog360_tier,omitempty"`
	Dialog360Tier *Dialog360Tier `json:"dialog360_tier,omitempty"`
}

// NewChannel instantiates a new Channel object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewChannel(id string, name string, readonly bool, stopKeywords []string, customer Customer, createdAt time.Time, updatedAt time.Time) *Channel {
	this := Channel{}
	this.Id = id
	this.Name = name
	this.Readonly = readonly
	this.StopKeywords = stopKeywords
	this.Customer = customer
	this.CreatedAt = createdAt
	this.UpdatedAt = updatedAt
	return &this
}

// NewChannelWithDefaults instantiates a new Channel object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewChannelWithDefaults() *Channel {
	this := Channel{}
	return &this
}

// GetId returns the Id field value
func (o *Channel) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *Channel) GetIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *Channel) SetId(v string) {
	o.Id = v
}

// GetName returns the Name field value
func (o *Channel) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *Channel) GetNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *Channel) SetName(v string) {
	o.Name = v
}

// GetTimezone returns the Timezone field value if set, zero value otherwise.
func (o *Channel) GetTimezone() string {
	if o == nil || IsNil(o.Timezone) {
		var ret string
		return ret
	}
	return *o.Timezone
}

// GetTimezoneOk returns a tuple with the Timezone field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Channel) GetTimezoneOk() (*string, bool) {
	if o == nil || IsNil(o.Timezone) {
		return nil, false
	}
	return o.Timezone, true
}

// HasTimezone returns a boolean if a field has been set.
func (o *Channel) HasTimezone() bool {
	if o != nil && !IsNil(o.Timezone) {
		return true
	}

	return false
}

// SetTimezone gets a reference to the given string and assigns it to the Timezone field.
func (o *Channel) SetTimezone(v string) {
	o.Timezone = &v
}

// GetDefaultLanguage returns the DefaultLanguage field value if set, zero value otherwise.
func (o *Channel) GetDefaultLanguage() string {
	if o == nil || IsNil(o.DefaultLanguage) {
		var ret string
		return ret
	}
	return *o.DefaultLanguage
}

// GetDefaultLanguageOk returns a tuple with the DefaultLanguage field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Channel) GetDefaultLanguageOk() (*string, bool) {
	if o == nil || IsNil(o.DefaultLanguage) {
		return nil, false
	}
	return o.DefaultLanguage, true
}

// HasDefaultLanguage returns a boolean if a field has been set.
func (o *Channel) HasDefaultLanguage() bool {
	if o != nil && !IsNil(o.DefaultLanguage) {
		return true
	}

	return false
}

// SetDefaultLanguage gets a reference to the given string and assigns it to the DefaultLanguage field.
func (o *Channel) SetDefaultLanguage(v string) {
	o.DefaultLanguage = &v
}

// GetReadonly returns the Readonly field value
func (o *Channel) GetReadonly() bool {
	if o == nil {
		var ret bool
		return ret
	}

	return o.Readonly
}

// GetReadonlyOk returns a tuple with the Readonly field value
// and a boolean to check if the value has been set.
func (o *Channel) GetReadonlyOk() (*bool, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Readonly, true
}

// SetReadonly sets field value
func (o *Channel) SetReadonly(v bool) {
	o.Readonly = v
}

// GetAttributionEnabled returns the AttributionEnabled field value if set, zero value otherwise.
func (o *Channel) GetAttributionEnabled() bool {
	if o == nil || IsNil(o.AttributionEnabled) {
		var ret bool
		return ret
	}
	return *o.AttributionEnabled
}

// GetAttributionEnabledOk returns a tuple with the AttributionEnabled field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Channel) GetAttributionEnabledOk() (*bool, bool) {
	if o == nil || IsNil(o.AttributionEnabled) {
		return nil, false
	}
	return o.AttributionEnabled, true
}

// HasAttributionEnabled returns a boolean if a field has been set.
func (o *Channel) HasAttributionEnabled() bool {
	if o != nil && !IsNil(o.AttributionEnabled) {
		return true
	}

	return false
}

// SetAttributionEnabled gets a reference to the given bool and assigns it to the AttributionEnabled field.
func (o *Channel) SetAttributionEnabled(v bool) {
	o.AttributionEnabled = &v
}

// GetStopKeywords returns the StopKeywords field value
func (o *Channel) GetStopKeywords() []string {
	if o == nil {
		var ret []string
		return ret
	}

	return o.StopKeywords
}

// GetStopKeywordsOk returns a tuple with the StopKeywords field value
// and a boolean to check if the value has been set.
func (o *Channel) GetStopKeywordsOk() ([]string, bool) {
	if o == nil {
		return nil, false
	}
	return o.StopKeywords, true
}

// SetStopKeywords sets field value
func (o *Channel) SetStopKeywords(v []string) {
	o.StopKeywords = v
}

// GetTags returns the Tags field value if set, zero value otherwise.
func (o *Channel) GetTags() []ChannelTagsInner {
	if o == nil || IsNil(o.Tags) {
		var ret []ChannelTagsInner
		return ret
	}
	return o.Tags
}

// GetTagsOk returns a tuple with the Tags field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Channel) GetTagsOk() ([]ChannelTagsInner, bool) {
	if o == nil || IsNil(o.Tags) {
		return nil, false
	}
	return o.Tags, true
}

// HasTags returns a boolean if a field has been set.
func (o *Channel) HasTags() bool {
	if o != nil && !IsNil(o.Tags) {
		return true
	}

	return false
}

// SetTags gets a reference to the given []ChannelTagsInner and assigns it to the Tags field.
func (o *Channel) SetTags(v []ChannelTagsInner) {
	o.Tags = v
}

// GetCustomer returns the Customer field value
func (o *Channel) GetCustomer() Customer {
	if o == nil {
		var ret Customer
		return ret
	}

	return o.Customer
}

// GetCustomerOk returns a tuple with the Customer field value
// and a boolean to check if the value has been set.
func (o *Channel) GetCustomerOk() (*Customer, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Customer, true
}

// SetCustomer sets field value
func (o *Channel) SetCustomer(v Customer) {
	o.Customer = v
}

// GetOrganization returns the Organization field value if set, zero value otherwise.
func (o *Channel) GetOrganization() Organization {
	if o == nil || IsNil(o.Organization) {
		var ret Organization
		return ret
	}
	return *o.Organization
}

// GetOrganizationOk returns a tuple with the Organization field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Channel) GetOrganizationOk() (*Organization, bool) {
	if o == nil || IsNil(o.Organization) {
		return nil, false
	}
	return o.Organization, true
}

// HasOrganization returns a boolean if a field has been set.
func (o *Channel) HasOrganization() bool {
	if o != nil && !IsNil(o.Organization) {
		return true
	}

	return false
}

// SetOrganization gets a reference to the given Organization and assigns it to the Organization field.
func (o *Channel) SetOrganization(v Organization) {
	o.Organization = &v
}

// GetCreatedAt returns the CreatedAt field value
func (o *Channel) GetCreatedAt() time.Time {
	if o == nil {
		var ret time.Time
		return ret
	}

	return o.CreatedAt
}

// GetCreatedAtOk returns a tuple with the CreatedAt field value
// and a boolean to check if the value has been set.
func (o *Channel) GetCreatedAtOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return &o.CreatedAt, true
}

// SetCreatedAt sets field value
func (o *Channel) SetCreatedAt(v time.Time) {
	o.CreatedAt = v
}

// GetUpdatedAt returns the UpdatedAt field value
func (o *Channel) GetUpdatedAt() time.Time {
	if o == nil {
		var ret time.Time
		return ret
	}

	return o.UpdatedAt
}

// GetUpdatedAtOk returns a tuple with the UpdatedAt field value
// and a boolean to check if the value has been set.
func (o *Channel) GetUpdatedAtOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return &o.UpdatedAt, true
}

// SetUpdatedAt sets field value
func (o *Channel) SetUpdatedAt(v time.Time) {
	o.UpdatedAt = v
}

// GetFallbackLanguage returns the FallbackLanguage field value if set, zero value otherwise.
func (o *Channel) GetFallbackLanguage() string {
	if o == nil || IsNil(o.FallbackLanguage) {
		var ret string
		return ret
	}
	return *o.FallbackLanguage
}

// GetFallbackLanguageOk returns a tuple with the FallbackLanguage field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Channel) GetFallbackLanguageOk() (*string, bool) {
	if o == nil || IsNil(o.FallbackLanguage) {
		return nil, false
	}
	return o.FallbackLanguage, true
}

// HasFallbackLanguage returns a boolean if a field has been set.
func (o *Channel) HasFallbackLanguage() bool {
	if o != nil && !IsNil(o.FallbackLanguage) {
		return true
	}

	return false
}

// SetFallbackLanguage gets a reference to the given string and assigns it to the FallbackLanguage field.
func (o *Channel) SetFallbackLanguage(v string) {
	o.FallbackLanguage = &v
}

// GetDefaultDialog360Tier returns the DefaultDialog360Tier field value if set, zero value otherwise.
func (o *Channel) GetDefaultDialog360Tier() Dialog360Tier {
	if o == nil || IsNil(o.DefaultDialog360Tier) {
		var ret Dialog360Tier
		return ret
	}
	return *o.DefaultDialog360Tier
}

// GetDefaultDialog360TierOk returns a tuple with the DefaultDialog360Tier field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Channel) GetDefaultDialog360TierOk() (*Dialog360Tier, bool) {
	if o == nil || IsNil(o.DefaultDialog360Tier) {
		return nil, false
	}
	return o.DefaultDialog360Tier, true
}

// HasDefaultDialog360Tier returns a boolean if a field has been set.
func (o *Channel) HasDefaultDialog360Tier() bool {
	if o != nil && !IsNil(o.DefaultDialog360Tier) {
		return true
	}

	return false
}

// SetDefaultDialog360Tier gets a reference to the given Dialog360Tier and assigns it to the DefaultDialog360Tier field.
func (o *Channel) SetDefaultDialog360Tier(v Dialog360Tier) {
	o.DefaultDialog360Tier = &v
}

// GetDialog360Tier returns the Dialog360Tier field value if set, zero value otherwise.
func (o *Channel) GetDialog360Tier() Dialog360Tier {
	if o == nil || IsNil(o.Dialog360Tier) {
		var ret Dialog360Tier
		return ret
	}
	return *o.Dialog360Tier
}

// GetDialog360TierOk returns a tuple with the Dialog360Tier field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Channel) GetDialog360TierOk() (*Dialog360Tier, bool) {
	if o == nil || IsNil(o.Dialog360Tier) {
		return nil, false
	}
	return o.Dialog360Tier, true
}

// HasDialog360Tier returns a boolean if a field has been set.
func (o *Channel) HasDialog360Tier() bool {
	if o != nil && !IsNil(o.Dialog360Tier) {
		return true
	}

	return false
}

// SetDialog360Tier gets a reference to the given Dialog360Tier and assigns it to the Dialog360Tier field.
func (o *Channel) SetDialog360Tier(v Dialog360Tier) {
	o.Dialog360Tier = &v
}

func (o Channel) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o Channel) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["id"] = o.Id
	toSerialize["name"] = o.Name
	if !IsNil(o.Timezone) {
		toSerialize["timezone"] = o.Timezone
	}
	if !IsNil(o.DefaultLanguage) {
		toSerialize["default_language"] = o.DefaultLanguage
	}
	toSerialize["readonly"] = o.Readonly
	if !IsNil(o.AttributionEnabled) {
		toSerialize["attribution_enabled"] = o.AttributionEnabled
	}
	toSerialize["stop_keywords"] = o.StopKeywords
	if !IsNil(o.Tags) {
		toSerialize["tags"] = o.Tags
	}
	toSerialize["customer"] = o.Customer
	if !IsNil(o.Organization) {
		toSerialize["organization"] = o.Organization
	}
	toSerialize["created_at"] = o.CreatedAt
	toSerialize["updated_at"] = o.UpdatedAt
	if !IsNil(o.FallbackLanguage) {
		toSerialize["fallback_language"] = o.FallbackLanguage
	}
	if !IsNil(o.DefaultDialog360Tier) {
		toSerialize["default_dialog360_tier"] = o.DefaultDialog360Tier
	}
	if !IsNil(o.Dialog360Tier) {
		toSerialize["dialog360_tier"] = o.Dialog360Tier
	}
	return toSerialize, nil
}

type NullableChannel struct {
	value *Channel
	isSet bool
}

func (v NullableChannel) Get() *Channel {
	return v.value
}

func (v *NullableChannel) Set(val *Channel) {
	v.value = val
	v.isSet = true
}

func (v NullableChannel) IsSet() bool {
	return v.isSet
}

func (v *NullableChannel) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableChannel(val *Channel) *NullableChannel {
	return &NullableChannel{value: val, isSet: true}
}

func (v NullableChannel) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableChannel) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


