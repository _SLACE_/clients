/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the UserActivationData type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &UserActivationData{}

// UserActivationData struct for UserActivationData
type UserActivationData struct {
	Code string `json:"code"`
	Password string `json:"password"`
}

// NewUserActivationData instantiates a new UserActivationData object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewUserActivationData(code string, password string) *UserActivationData {
	this := UserActivationData{}
	this.Code = code
	this.Password = password
	return &this
}

// NewUserActivationDataWithDefaults instantiates a new UserActivationData object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewUserActivationDataWithDefaults() *UserActivationData {
	this := UserActivationData{}
	return &this
}

// GetCode returns the Code field value
func (o *UserActivationData) GetCode() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Code
}

// GetCodeOk returns a tuple with the Code field value
// and a boolean to check if the value has been set.
func (o *UserActivationData) GetCodeOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Code, true
}

// SetCode sets field value
func (o *UserActivationData) SetCode(v string) {
	o.Code = v
}

// GetPassword returns the Password field value
func (o *UserActivationData) GetPassword() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Password
}

// GetPasswordOk returns a tuple with the Password field value
// and a boolean to check if the value has been set.
func (o *UserActivationData) GetPasswordOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Password, true
}

// SetPassword sets field value
func (o *UserActivationData) SetPassword(v string) {
	o.Password = v
}

func (o UserActivationData) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o UserActivationData) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["code"] = o.Code
	toSerialize["password"] = o.Password
	return toSerialize, nil
}

type NullableUserActivationData struct {
	value *UserActivationData
	isSet bool
}

func (v NullableUserActivationData) Get() *UserActivationData {
	return v.value
}

func (v *NullableUserActivationData) Set(val *UserActivationData) {
	v.value = val
	v.isSet = true
}

func (v NullableUserActivationData) IsSet() bool {
	return v.isSet
}

func (v *NullableUserActivationData) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableUserActivationData(val *UserActivationData) *NullableUserActivationData {
	return &NullableUserActivationData{value: val, isSet: true}
}

func (v NullableUserActivationData) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableUserActivationData) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


