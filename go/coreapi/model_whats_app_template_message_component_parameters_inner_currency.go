/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the WhatsAppTemplateMessageComponentParametersInnerCurrency type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &WhatsAppTemplateMessageComponentParametersInnerCurrency{}

// WhatsAppTemplateMessageComponentParametersInnerCurrency struct for WhatsAppTemplateMessageComponentParametersInnerCurrency
type WhatsAppTemplateMessageComponentParametersInnerCurrency struct {
	FallbackValue *string `json:"fallback_value,omitempty"`
	Code *string `json:"code,omitempty"`
	Amount1000 *int32 `json:"amount_1000,omitempty"`
}

// NewWhatsAppTemplateMessageComponentParametersInnerCurrency instantiates a new WhatsAppTemplateMessageComponentParametersInnerCurrency object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewWhatsAppTemplateMessageComponentParametersInnerCurrency() *WhatsAppTemplateMessageComponentParametersInnerCurrency {
	this := WhatsAppTemplateMessageComponentParametersInnerCurrency{}
	return &this
}

// NewWhatsAppTemplateMessageComponentParametersInnerCurrencyWithDefaults instantiates a new WhatsAppTemplateMessageComponentParametersInnerCurrency object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewWhatsAppTemplateMessageComponentParametersInnerCurrencyWithDefaults() *WhatsAppTemplateMessageComponentParametersInnerCurrency {
	this := WhatsAppTemplateMessageComponentParametersInnerCurrency{}
	return &this
}

// GetFallbackValue returns the FallbackValue field value if set, zero value otherwise.
func (o *WhatsAppTemplateMessageComponentParametersInnerCurrency) GetFallbackValue() string {
	if o == nil || IsNil(o.FallbackValue) {
		var ret string
		return ret
	}
	return *o.FallbackValue
}

// GetFallbackValueOk returns a tuple with the FallbackValue field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateMessageComponentParametersInnerCurrency) GetFallbackValueOk() (*string, bool) {
	if o == nil || IsNil(o.FallbackValue) {
		return nil, false
	}
	return o.FallbackValue, true
}

// HasFallbackValue returns a boolean if a field has been set.
func (o *WhatsAppTemplateMessageComponentParametersInnerCurrency) HasFallbackValue() bool {
	if o != nil && !IsNil(o.FallbackValue) {
		return true
	}

	return false
}

// SetFallbackValue gets a reference to the given string and assigns it to the FallbackValue field.
func (o *WhatsAppTemplateMessageComponentParametersInnerCurrency) SetFallbackValue(v string) {
	o.FallbackValue = &v
}

// GetCode returns the Code field value if set, zero value otherwise.
func (o *WhatsAppTemplateMessageComponentParametersInnerCurrency) GetCode() string {
	if o == nil || IsNil(o.Code) {
		var ret string
		return ret
	}
	return *o.Code
}

// GetCodeOk returns a tuple with the Code field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateMessageComponentParametersInnerCurrency) GetCodeOk() (*string, bool) {
	if o == nil || IsNil(o.Code) {
		return nil, false
	}
	return o.Code, true
}

// HasCode returns a boolean if a field has been set.
func (o *WhatsAppTemplateMessageComponentParametersInnerCurrency) HasCode() bool {
	if o != nil && !IsNil(o.Code) {
		return true
	}

	return false
}

// SetCode gets a reference to the given string and assigns it to the Code field.
func (o *WhatsAppTemplateMessageComponentParametersInnerCurrency) SetCode(v string) {
	o.Code = &v
}

// GetAmount1000 returns the Amount1000 field value if set, zero value otherwise.
func (o *WhatsAppTemplateMessageComponentParametersInnerCurrency) GetAmount1000() int32 {
	if o == nil || IsNil(o.Amount1000) {
		var ret int32
		return ret
	}
	return *o.Amount1000
}

// GetAmount1000Ok returns a tuple with the Amount1000 field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateMessageComponentParametersInnerCurrency) GetAmount1000Ok() (*int32, bool) {
	if o == nil || IsNil(o.Amount1000) {
		return nil, false
	}
	return o.Amount1000, true
}

// HasAmount1000 returns a boolean if a field has been set.
func (o *WhatsAppTemplateMessageComponentParametersInnerCurrency) HasAmount1000() bool {
	if o != nil && !IsNil(o.Amount1000) {
		return true
	}

	return false
}

// SetAmount1000 gets a reference to the given int32 and assigns it to the Amount1000 field.
func (o *WhatsAppTemplateMessageComponentParametersInnerCurrency) SetAmount1000(v int32) {
	o.Amount1000 = &v
}

func (o WhatsAppTemplateMessageComponentParametersInnerCurrency) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o WhatsAppTemplateMessageComponentParametersInnerCurrency) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.FallbackValue) {
		toSerialize["fallback_value"] = o.FallbackValue
	}
	if !IsNil(o.Code) {
		toSerialize["code"] = o.Code
	}
	if !IsNil(o.Amount1000) {
		toSerialize["amount_1000"] = o.Amount1000
	}
	return toSerialize, nil
}

type NullableWhatsAppTemplateMessageComponentParametersInnerCurrency struct {
	value *WhatsAppTemplateMessageComponentParametersInnerCurrency
	isSet bool
}

func (v NullableWhatsAppTemplateMessageComponentParametersInnerCurrency) Get() *WhatsAppTemplateMessageComponentParametersInnerCurrency {
	return v.value
}

func (v *NullableWhatsAppTemplateMessageComponentParametersInnerCurrency) Set(val *WhatsAppTemplateMessageComponentParametersInnerCurrency) {
	v.value = val
	v.isSet = true
}

func (v NullableWhatsAppTemplateMessageComponentParametersInnerCurrency) IsSet() bool {
	return v.isSet
}

func (v *NullableWhatsAppTemplateMessageComponentParametersInnerCurrency) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableWhatsAppTemplateMessageComponentParametersInnerCurrency(val *WhatsAppTemplateMessageComponentParametersInnerCurrency) *NullableWhatsAppTemplateMessageComponentParametersInnerCurrency {
	return &NullableWhatsAppTemplateMessageComponentParametersInnerCurrency{value: val, isSet: true}
}

func (v NullableWhatsAppTemplateMessageComponentParametersInnerCurrency) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableWhatsAppTemplateMessageComponentParametersInnerCurrency) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


