/*
Core

Testing ChannelsAPIService

*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech);

package coreapi

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
	openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func Test_coreapi_ChannelsAPIService(t *testing.T) {

	configuration := openapiclient.NewConfiguration()
	apiClient := openapiclient.NewAPIClient(configuration)

	t.Run("Test ChannelsAPIService CreateChannel", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		resp, httpRes, err := apiClient.ChannelsAPI.CreateChannel(context.Background()).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test ChannelsAPIService CreateChannelTag", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string

		httpRes, err := apiClient.ChannelsAPI.CreateChannelTag(context.Background(), channelId).Execute()

		require.Nil(t, err)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test ChannelsAPIService DeleteChannel", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string

		httpRes, err := apiClient.ChannelsAPI.DeleteChannel(context.Background(), channelId).Execute()

		require.Nil(t, err)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test ChannelsAPIService DeleteChannelsChannelIdTags", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var tagId string

		httpRes, err := apiClient.ChannelsAPI.DeleteChannelsChannelIdTags(context.Background(), channelId, tagId).Execute()

		require.Nil(t, err)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test ChannelsAPIService GetChannel", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string

		resp, httpRes, err := apiClient.ChannelsAPI.GetChannel(context.Background(), channelId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test ChannelsAPIService GetChannelTags", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string

		resp, httpRes, err := apiClient.ChannelsAPI.GetChannelTags(context.Background(), channelId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test ChannelsAPIService ListChannels", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		resp, httpRes, err := apiClient.ChannelsAPI.ListChannels(context.Background()).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test ChannelsAPIService UpdateChannel", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string

		resp, httpRes, err := apiClient.ChannelsAPI.UpdateChannel(context.Background(), channelId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

}
