/*
Core

Testing ConsentsAPIService

*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech);

package coreapi

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
	openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func Test_coreapi_ConsentsAPIService(t *testing.T) {

	configuration := openapiclient.NewConfiguration()
	apiClient := openapiclient.NewAPIClient(configuration)

	t.Run("Test ConsentsAPIService CreateNextConsentVersion", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var organizationId string

		resp, httpRes, err := apiClient.ConsentsAPI.CreateNextConsentVersion(context.Background(), organizationId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test ConsentsAPIService DeleteConsent", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var organizationId string
		var consentId string

		httpRes, err := apiClient.ConsentsAPI.DeleteConsent(context.Background(), organizationId, consentId).Execute()

		require.Nil(t, err)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test ConsentsAPIService ListConsents", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var organizationId string

		resp, httpRes, err := apiClient.ConsentsAPI.ListConsents(context.Background(), organizationId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

}
