/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the ObjectPermission type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ObjectPermission{}

// ObjectPermission struct for ObjectPermission
type ObjectPermission struct {
	ObjectId string `json:"object_id"`
	// Available permissions list:   PermissionDenied  Permission = 0    PermissionRead    Permission = 1     PermissionWrite   Permission = 2    PermissionExecute Permission = 4    PermissionDelete  Permission = 8   Permissions can be combined
	ObjectPermission int32 `json:"object_permission"`
	Permissions ObjectPermissionPermissions `json:"permissions"`
	Permission Permission `json:"permission"`
}

// NewObjectPermission instantiates a new ObjectPermission object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewObjectPermission(objectId string, objectPermission int32, permissions ObjectPermissionPermissions, permission Permission) *ObjectPermission {
	this := ObjectPermission{}
	this.ObjectId = objectId
	this.ObjectPermission = objectPermission
	this.Permissions = permissions
	this.Permission = permission
	return &this
}

// NewObjectPermissionWithDefaults instantiates a new ObjectPermission object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewObjectPermissionWithDefaults() *ObjectPermission {
	this := ObjectPermission{}
	return &this
}

// GetObjectId returns the ObjectId field value
func (o *ObjectPermission) GetObjectId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ObjectId
}

// GetObjectIdOk returns a tuple with the ObjectId field value
// and a boolean to check if the value has been set.
func (o *ObjectPermission) GetObjectIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ObjectId, true
}

// SetObjectId sets field value
func (o *ObjectPermission) SetObjectId(v string) {
	o.ObjectId = v
}

// GetObjectPermission returns the ObjectPermission field value
func (o *ObjectPermission) GetObjectPermission() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.ObjectPermission
}

// GetObjectPermissionOk returns a tuple with the ObjectPermission field value
// and a boolean to check if the value has been set.
func (o *ObjectPermission) GetObjectPermissionOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ObjectPermission, true
}

// SetObjectPermission sets field value
func (o *ObjectPermission) SetObjectPermission(v int32) {
	o.ObjectPermission = v
}

// GetPermissions returns the Permissions field value
func (o *ObjectPermission) GetPermissions() ObjectPermissionPermissions {
	if o == nil {
		var ret ObjectPermissionPermissions
		return ret
	}

	return o.Permissions
}

// GetPermissionsOk returns a tuple with the Permissions field value
// and a boolean to check if the value has been set.
func (o *ObjectPermission) GetPermissionsOk() (*ObjectPermissionPermissions, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Permissions, true
}

// SetPermissions sets field value
func (o *ObjectPermission) SetPermissions(v ObjectPermissionPermissions) {
	o.Permissions = v
}

// GetPermission returns the Permission field value
func (o *ObjectPermission) GetPermission() Permission {
	if o == nil {
		var ret Permission
		return ret
	}

	return o.Permission
}

// GetPermissionOk returns a tuple with the Permission field value
// and a boolean to check if the value has been set.
func (o *ObjectPermission) GetPermissionOk() (*Permission, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Permission, true
}

// SetPermission sets field value
func (o *ObjectPermission) SetPermission(v Permission) {
	o.Permission = v
}

func (o ObjectPermission) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ObjectPermission) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["object_id"] = o.ObjectId
	toSerialize["object_permission"] = o.ObjectPermission
	toSerialize["permissions"] = o.Permissions
	toSerialize["permission"] = o.Permission
	return toSerialize, nil
}

type NullableObjectPermission struct {
	value *ObjectPermission
	isSet bool
}

func (v NullableObjectPermission) Get() *ObjectPermission {
	return v.value
}

func (v *NullableObjectPermission) Set(val *ObjectPermission) {
	v.value = val
	v.isSet = true
}

func (v NullableObjectPermission) IsSet() bool {
	return v.isSet
}

func (v *NullableObjectPermission) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableObjectPermission(val *ObjectPermission) *NullableObjectPermission {
	return &NullableObjectPermission{value: val, isSet: true}
}

func (v NullableObjectPermission) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableObjectPermission) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


