/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the ChannelLocationCoordinates type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ChannelLocationCoordinates{}

// ChannelLocationCoordinates struct for ChannelLocationCoordinates
type ChannelLocationCoordinates struct {
	Latitude *string `json:"latitude,omitempty"`
	Longitude *string `json:"longitude,omitempty"`
}

// NewChannelLocationCoordinates instantiates a new ChannelLocationCoordinates object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewChannelLocationCoordinates() *ChannelLocationCoordinates {
	this := ChannelLocationCoordinates{}
	return &this
}

// NewChannelLocationCoordinatesWithDefaults instantiates a new ChannelLocationCoordinates object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewChannelLocationCoordinatesWithDefaults() *ChannelLocationCoordinates {
	this := ChannelLocationCoordinates{}
	return &this
}

// GetLatitude returns the Latitude field value if set, zero value otherwise.
func (o *ChannelLocationCoordinates) GetLatitude() string {
	if o == nil || IsNil(o.Latitude) {
		var ret string
		return ret
	}
	return *o.Latitude
}

// GetLatitudeOk returns a tuple with the Latitude field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ChannelLocationCoordinates) GetLatitudeOk() (*string, bool) {
	if o == nil || IsNil(o.Latitude) {
		return nil, false
	}
	return o.Latitude, true
}

// HasLatitude returns a boolean if a field has been set.
func (o *ChannelLocationCoordinates) HasLatitude() bool {
	if o != nil && !IsNil(o.Latitude) {
		return true
	}

	return false
}

// SetLatitude gets a reference to the given string and assigns it to the Latitude field.
func (o *ChannelLocationCoordinates) SetLatitude(v string) {
	o.Latitude = &v
}

// GetLongitude returns the Longitude field value if set, zero value otherwise.
func (o *ChannelLocationCoordinates) GetLongitude() string {
	if o == nil || IsNil(o.Longitude) {
		var ret string
		return ret
	}
	return *o.Longitude
}

// GetLongitudeOk returns a tuple with the Longitude field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ChannelLocationCoordinates) GetLongitudeOk() (*string, bool) {
	if o == nil || IsNil(o.Longitude) {
		return nil, false
	}
	return o.Longitude, true
}

// HasLongitude returns a boolean if a field has been set.
func (o *ChannelLocationCoordinates) HasLongitude() bool {
	if o != nil && !IsNil(o.Longitude) {
		return true
	}

	return false
}

// SetLongitude gets a reference to the given string and assigns it to the Longitude field.
func (o *ChannelLocationCoordinates) SetLongitude(v string) {
	o.Longitude = &v
}

func (o ChannelLocationCoordinates) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ChannelLocationCoordinates) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Latitude) {
		toSerialize["latitude"] = o.Latitude
	}
	if !IsNil(o.Longitude) {
		toSerialize["longitude"] = o.Longitude
	}
	return toSerialize, nil
}

type NullableChannelLocationCoordinates struct {
	value *ChannelLocationCoordinates
	isSet bool
}

func (v NullableChannelLocationCoordinates) Get() *ChannelLocationCoordinates {
	return v.value
}

func (v *NullableChannelLocationCoordinates) Set(val *ChannelLocationCoordinates) {
	v.value = val
	v.isSet = true
}

func (v NullableChannelLocationCoordinates) IsSet() bool {
	return v.isSet
}

func (v *NullableChannelLocationCoordinates) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableChannelLocationCoordinates(val *ChannelLocationCoordinates) *NullableChannelLocationCoordinates {
	return &NullableChannelLocationCoordinates{value: val, isSet: true}
}

func (v NullableChannelLocationCoordinates) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableChannelLocationCoordinates) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


