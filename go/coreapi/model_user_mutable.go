/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the UserMutable type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &UserMutable{}

// UserMutable struct for UserMutable
type UserMutable struct {
	Name string `json:"name"`
	Email string `json:"email"`
	Active bool `json:"active"`
	Role Role `json:"role"`
	Language *string `json:"language,omitempty"`
	PhoneNumber *string `json:"phone_number,omitempty"`
	Type *Role `json:"type,omitempty"`
}

// NewUserMutable instantiates a new UserMutable object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewUserMutable(name string, email string, active bool, role Role) *UserMutable {
	this := UserMutable{}
	this.Name = name
	this.Email = email
	this.Active = active
	this.Role = role
	return &this
}

// NewUserMutableWithDefaults instantiates a new UserMutable object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewUserMutableWithDefaults() *UserMutable {
	this := UserMutable{}
	return &this
}

// GetName returns the Name field value
func (o *UserMutable) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *UserMutable) GetNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *UserMutable) SetName(v string) {
	o.Name = v
}

// GetEmail returns the Email field value
func (o *UserMutable) GetEmail() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Email
}

// GetEmailOk returns a tuple with the Email field value
// and a boolean to check if the value has been set.
func (o *UserMutable) GetEmailOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Email, true
}

// SetEmail sets field value
func (o *UserMutable) SetEmail(v string) {
	o.Email = v
}

// GetActive returns the Active field value
func (o *UserMutable) GetActive() bool {
	if o == nil {
		var ret bool
		return ret
	}

	return o.Active
}

// GetActiveOk returns a tuple with the Active field value
// and a boolean to check if the value has been set.
func (o *UserMutable) GetActiveOk() (*bool, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Active, true
}

// SetActive sets field value
func (o *UserMutable) SetActive(v bool) {
	o.Active = v
}

// GetRole returns the Role field value
func (o *UserMutable) GetRole() Role {
	if o == nil {
		var ret Role
		return ret
	}

	return o.Role
}

// GetRoleOk returns a tuple with the Role field value
// and a boolean to check if the value has been set.
func (o *UserMutable) GetRoleOk() (*Role, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Role, true
}

// SetRole sets field value
func (o *UserMutable) SetRole(v Role) {
	o.Role = v
}

// GetLanguage returns the Language field value if set, zero value otherwise.
func (o *UserMutable) GetLanguage() string {
	if o == nil || IsNil(o.Language) {
		var ret string
		return ret
	}
	return *o.Language
}

// GetLanguageOk returns a tuple with the Language field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UserMutable) GetLanguageOk() (*string, bool) {
	if o == nil || IsNil(o.Language) {
		return nil, false
	}
	return o.Language, true
}

// HasLanguage returns a boolean if a field has been set.
func (o *UserMutable) HasLanguage() bool {
	if o != nil && !IsNil(o.Language) {
		return true
	}

	return false
}

// SetLanguage gets a reference to the given string and assigns it to the Language field.
func (o *UserMutable) SetLanguage(v string) {
	o.Language = &v
}

// GetPhoneNumber returns the PhoneNumber field value if set, zero value otherwise.
func (o *UserMutable) GetPhoneNumber() string {
	if o == nil || IsNil(o.PhoneNumber) {
		var ret string
		return ret
	}
	return *o.PhoneNumber
}

// GetPhoneNumberOk returns a tuple with the PhoneNumber field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UserMutable) GetPhoneNumberOk() (*string, bool) {
	if o == nil || IsNil(o.PhoneNumber) {
		return nil, false
	}
	return o.PhoneNumber, true
}

// HasPhoneNumber returns a boolean if a field has been set.
func (o *UserMutable) HasPhoneNumber() bool {
	if o != nil && !IsNil(o.PhoneNumber) {
		return true
	}

	return false
}

// SetPhoneNumber gets a reference to the given string and assigns it to the PhoneNumber field.
func (o *UserMutable) SetPhoneNumber(v string) {
	o.PhoneNumber = &v
}

// GetType returns the Type field value if set, zero value otherwise.
func (o *UserMutable) GetType() Role {
	if o == nil || IsNil(o.Type) {
		var ret Role
		return ret
	}
	return *o.Type
}

// GetTypeOk returns a tuple with the Type field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UserMutable) GetTypeOk() (*Role, bool) {
	if o == nil || IsNil(o.Type) {
		return nil, false
	}
	return o.Type, true
}

// HasType returns a boolean if a field has been set.
func (o *UserMutable) HasType() bool {
	if o != nil && !IsNil(o.Type) {
		return true
	}

	return false
}

// SetType gets a reference to the given Role and assigns it to the Type field.
func (o *UserMutable) SetType(v Role) {
	o.Type = &v
}

func (o UserMutable) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o UserMutable) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["name"] = o.Name
	toSerialize["email"] = o.Email
	toSerialize["active"] = o.Active
	toSerialize["role"] = o.Role
	if !IsNil(o.Language) {
		toSerialize["language"] = o.Language
	}
	if !IsNil(o.PhoneNumber) {
		toSerialize["phone_number"] = o.PhoneNumber
	}
	if !IsNil(o.Type) {
		toSerialize["type"] = o.Type
	}
	return toSerialize, nil
}

type NullableUserMutable struct {
	value *UserMutable
	isSet bool
}

func (v NullableUserMutable) Get() *UserMutable {
	return v.value
}

func (v *NullableUserMutable) Set(val *UserMutable) {
	v.value = val
	v.isSet = true
}

func (v NullableUserMutable) IsSet() bool {
	return v.isSet
}

func (v *NullableUserMutable) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableUserMutable(val *UserMutable) *NullableUserMutable {
	return &NullableUserMutable{value: val, isSet: true}
}

func (v NullableUserMutable) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableUserMutable) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


