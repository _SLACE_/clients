/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the WhatsAppTemplateDefaults type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &WhatsAppTemplateDefaults{}

// WhatsAppTemplateDefaults struct for WhatsAppTemplateDefaults
type WhatsAppTemplateDefaults struct {
	Components []WhatsAppTemplateDefaultsComponent `json:"components"`
}

// NewWhatsAppTemplateDefaults instantiates a new WhatsAppTemplateDefaults object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewWhatsAppTemplateDefaults(components []WhatsAppTemplateDefaultsComponent) *WhatsAppTemplateDefaults {
	this := WhatsAppTemplateDefaults{}
	this.Components = components
	return &this
}

// NewWhatsAppTemplateDefaultsWithDefaults instantiates a new WhatsAppTemplateDefaults object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewWhatsAppTemplateDefaultsWithDefaults() *WhatsAppTemplateDefaults {
	this := WhatsAppTemplateDefaults{}
	return &this
}

// GetComponents returns the Components field value
func (o *WhatsAppTemplateDefaults) GetComponents() []WhatsAppTemplateDefaultsComponent {
	if o == nil {
		var ret []WhatsAppTemplateDefaultsComponent
		return ret
	}

	return o.Components
}

// GetComponentsOk returns a tuple with the Components field value
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateDefaults) GetComponentsOk() ([]WhatsAppTemplateDefaultsComponent, bool) {
	if o == nil {
		return nil, false
	}
	return o.Components, true
}

// SetComponents sets field value
func (o *WhatsAppTemplateDefaults) SetComponents(v []WhatsAppTemplateDefaultsComponent) {
	o.Components = v
}

func (o WhatsAppTemplateDefaults) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o WhatsAppTemplateDefaults) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["components"] = o.Components
	return toSerialize, nil
}

type NullableWhatsAppTemplateDefaults struct {
	value *WhatsAppTemplateDefaults
	isSet bool
}

func (v NullableWhatsAppTemplateDefaults) Get() *WhatsAppTemplateDefaults {
	return v.value
}

func (v *NullableWhatsAppTemplateDefaults) Set(val *WhatsAppTemplateDefaults) {
	v.value = val
	v.isSet = true
}

func (v NullableWhatsAppTemplateDefaults) IsSet() bool {
	return v.isSet
}

func (v *NullableWhatsAppTemplateDefaults) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableWhatsAppTemplateDefaults(val *WhatsAppTemplateDefaults) *NullableWhatsAppTemplateDefaults {
	return &NullableWhatsAppTemplateDefaults{value: val, isSet: true}
}

func (v NullableWhatsAppTemplateDefaults) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableWhatsAppTemplateDefaults) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


