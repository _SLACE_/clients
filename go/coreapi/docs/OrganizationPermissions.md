# OrganizationPermissions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Context dependant, not always available | [optional] 
**ObjectPermission** | **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | 
**Channels** | Pointer to [**[]ChannelPermission**](ChannelPermission.md) |  | [optional] 
**Permissions** | Pointer to [**OrganizationPermissionsPermissions**](OrganizationPermissionsPermissions.md) |  | [optional] 
**Permission** | [**Permission**](Permission.md) |  | 

## Methods

### NewOrganizationPermissions

`func NewOrganizationPermissions(objectPermission int32, permission Permission, ) *OrganizationPermissions`

NewOrganizationPermissions instantiates a new OrganizationPermissions object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrganizationPermissionsWithDefaults

`func NewOrganizationPermissionsWithDefaults() *OrganizationPermissions`

NewOrganizationPermissionsWithDefaults instantiates a new OrganizationPermissions object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *OrganizationPermissions) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *OrganizationPermissions) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *OrganizationPermissions) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *OrganizationPermissions) HasId() bool`

HasId returns a boolean if a field has been set.

### GetObjectPermission

`func (o *OrganizationPermissions) GetObjectPermission() int32`

GetObjectPermission returns the ObjectPermission field if non-nil, zero value otherwise.

### GetObjectPermissionOk

`func (o *OrganizationPermissions) GetObjectPermissionOk() (*int32, bool)`

GetObjectPermissionOk returns a tuple with the ObjectPermission field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetObjectPermission

`func (o *OrganizationPermissions) SetObjectPermission(v int32)`

SetObjectPermission sets ObjectPermission field to given value.


### GetChannels

`func (o *OrganizationPermissions) GetChannels() []ChannelPermission`

GetChannels returns the Channels field if non-nil, zero value otherwise.

### GetChannelsOk

`func (o *OrganizationPermissions) GetChannelsOk() (*[]ChannelPermission, bool)`

GetChannelsOk returns a tuple with the Channels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannels

`func (o *OrganizationPermissions) SetChannels(v []ChannelPermission)`

SetChannels sets Channels field to given value.

### HasChannels

`func (o *OrganizationPermissions) HasChannels() bool`

HasChannels returns a boolean if a field has been set.

### GetPermissions

`func (o *OrganizationPermissions) GetPermissions() OrganizationPermissionsPermissions`

GetPermissions returns the Permissions field if non-nil, zero value otherwise.

### GetPermissionsOk

`func (o *OrganizationPermissions) GetPermissionsOk() (*OrganizationPermissionsPermissions, bool)`

GetPermissionsOk returns a tuple with the Permissions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermissions

`func (o *OrganizationPermissions) SetPermissions(v OrganizationPermissionsPermissions)`

SetPermissions sets Permissions field to given value.

### HasPermissions

`func (o *OrganizationPermissions) HasPermissions() bool`

HasPermissions returns a boolean if a field has been set.

### GetPermission

`func (o *OrganizationPermissions) GetPermission() Permission`

GetPermission returns the Permission field if non-nil, zero value otherwise.

### GetPermissionOk

`func (o *OrganizationPermissions) GetPermissionOk() (*Permission, bool)`

GetPermissionOk returns a tuple with the Permission field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermission

`func (o *OrganizationPermissions) SetPermission(v Permission)`

SetPermission sets Permission field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


