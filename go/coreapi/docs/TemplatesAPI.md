# \TemplatesAPI

All URIs are relative to *https://api.slace.com/core*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CloneTemplates**](TemplatesAPI.md#CloneTemplates) | **Post** /templates-clone | Clone Templates
[**CreateTemplate**](TemplatesAPI.md#CreateTemplate) | **Post** /templates | Create template
[**DeleteTemplate**](TemplatesAPI.md#DeleteTemplate) | **Delete** /templates/{id} | Delete template
[**GetTemplate**](TemplatesAPI.md#GetTemplate) | **Get** /templates/{id} | Get template
[**GetTemplateGrouped**](TemplatesAPI.md#GetTemplateGrouped) | **Get** /templates-grouped/{template_id} | Get Template Grouped
[**ListTemplates**](TemplatesAPI.md#ListTemplates) | **Get** /templates | List templates
[**ListTemplatesGrouped**](TemplatesAPI.md#ListTemplatesGrouped) | **Get** /templates-grouped | List Templates Grouped
[**PatchTemplateDefaults**](TemplatesAPI.md#PatchTemplateDefaults) | **Patch** /templates/{id}/defaults | Patch template defaults
[**RequestTemplateReview**](TemplatesAPI.md#RequestTemplateReview) | **Post** /templates/{id}/request-review | Request template review
[**ReviewTemplates**](TemplatesAPI.md#ReviewTemplates) | **Post** /templates/{id}/review | Review template
[**UpdateTemplate**](TemplatesAPI.md#UpdateTemplate) | **Put** /templates/{id} | Update template
[**UpdateTemplateDefaults**](TemplatesAPI.md#UpdateTemplateDefaults) | **Put** /templates/{id}/defaults | Update template defaults



## CloneTemplates

> TemplatesCloneResult CloneTemplates(ctx).TemplatesCloneRequest(templatesCloneRequest).Execute()

Clone Templates



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    templatesCloneRequest := *openapiclient.NewTemplatesCloneRequest([]string{"TemplateIds_example"}, []string{"ChannelIds_example"}) // TemplatesCloneRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TemplatesAPI.CloneTemplates(context.Background()).TemplatesCloneRequest(templatesCloneRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TemplatesAPI.CloneTemplates``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CloneTemplates`: TemplatesCloneResult
    fmt.Fprintf(os.Stdout, "Response from `TemplatesAPI.CloneTemplates`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCloneTemplatesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **templatesCloneRequest** | [**TemplatesCloneRequest**](TemplatesCloneRequest.md) |  | 

### Return type

[**TemplatesCloneResult**](TemplatesCloneResult.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateTemplate

> Template CreateTemplate(ctx).TemplateMutable(templateMutable).Execute()

Create template



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    templateMutable := *openapiclient.NewTemplateMutable("Name_example", "OrganizationId_example", openapiclient.TemplateType("internal"), openapiclient.TemplateVisibility("admin"), "Language_example") // TemplateMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TemplatesAPI.CreateTemplate(context.Background()).TemplateMutable(templateMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TemplatesAPI.CreateTemplate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateTemplate`: Template
    fmt.Fprintf(os.Stdout, "Response from `TemplatesAPI.CreateTemplate`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateTemplateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **templateMutable** | [**TemplateMutable**](TemplateMutable.md) |  | 

### Return type

[**Template**](Template.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteTemplate

> DeleteTemplate(ctx, id).Execute()

Delete template



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.TemplatesAPI.DeleteTemplate(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TemplatesAPI.DeleteTemplate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteTemplateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTemplate

> Template GetTemplate(ctx, id).Execute()

Get template



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TemplatesAPI.GetTemplate(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TemplatesAPI.GetTemplate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTemplate`: Template
    fmt.Fprintf(os.Stdout, "Response from `TemplatesAPI.GetTemplate`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTemplateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Template**](Template.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTemplateGrouped

> TemplateGrouped GetTemplateGrouped(ctx, templateId).Execute()

Get Template Grouped



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    templateId := "templateId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TemplatesAPI.GetTemplateGrouped(context.Background(), templateId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TemplatesAPI.GetTemplateGrouped``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTemplateGrouped`: TemplateGrouped
    fmt.Fprintf(os.Stdout, "Response from `TemplatesAPI.GetTemplateGrouped`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**templateId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTemplateGroupedRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**TemplateGrouped**](TemplateGrouped.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListTemplates

> TemplatesList ListTemplates(ctx).Offset(offset).Limit(limit).Sort(sort).OrganizationId(organizationId).Name(name).Type_(type_).Language(language).CreatedAt(createdAt).UpdatedAt(updatedAt).GatewayId(gatewayId).Status(status).Execute()

List templates



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    offset := int32(56) // int32 |  (optional)
    limit := int32(56) // int32 |  (optional)
    sort := "sort_example" // string | For reversed sort, use '-'. Example: \"sort=-updated_at\" (optional)
    organizationId := "organizationId_example" // string |  (optional)
    name := "name_example" // string |  (optional)
    type_ := "type__example" // string |  (optional)
    language := "language_example" // string |  (optional)
    createdAt := time.Now() // time.Time |  (optional)
    updatedAt := time.Now() // time.Time |  (optional)
    gatewayId := "gatewayId_example" // string | When provided results will also contain templates without any gateway set (optional)
    status := "status_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TemplatesAPI.ListTemplates(context.Background()).Offset(offset).Limit(limit).Sort(sort).OrganizationId(organizationId).Name(name).Type_(type_).Language(language).CreatedAt(createdAt).UpdatedAt(updatedAt).GatewayId(gatewayId).Status(status).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TemplatesAPI.ListTemplates``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListTemplates`: TemplatesList
    fmt.Fprintf(os.Stdout, "Response from `TemplatesAPI.ListTemplates`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListTemplatesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int32** |  | 
 **limit** | **int32** |  | 
 **sort** | **string** | For reversed sort, use &#39;-&#39;. Example: \&quot;sort&#x3D;-updated_at\&quot; | 
 **organizationId** | **string** |  | 
 **name** | **string** |  | 
 **type_** | **string** |  | 
 **language** | **string** |  | 
 **createdAt** | **time.Time** |  | 
 **updatedAt** | **time.Time** |  | 
 **gatewayId** | **string** | When provided results will also contain templates without any gateway set | 
 **status** | **string** |  | 

### Return type

[**TemplatesList**](TemplatesList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListTemplatesGrouped

> TemplatesGroupedList ListTemplatesGrouped(ctx).Offset(offset).Limit(limit).Sort(sort).OrganizationId(organizationId).Name(name).Type_(type_).Language(language).CreatedAt(createdAt).UpdatedAt(updatedAt).GatewayId(gatewayId).Status(status).Execute()

List Templates Grouped



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    offset := int32(56) // int32 |  (optional)
    limit := int32(56) // int32 |  (optional)
    sort := "sort_example" // string | For reversed sort, use '-'. Example: \"sort=-updated_at\" (optional)
    organizationId := "organizationId_example" // string |  (optional)
    name := "name_example" // string |  (optional)
    type_ := "type__example" // string |  (optional)
    language := "language_example" // string |  (optional)
    createdAt := time.Now() // time.Time |  (optional)
    updatedAt := time.Now() // time.Time |  (optional)
    gatewayId := "gatewayId_example" // string | When provided results will also contain templates without any gateway set (optional)
    status := "status_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TemplatesAPI.ListTemplatesGrouped(context.Background()).Offset(offset).Limit(limit).Sort(sort).OrganizationId(organizationId).Name(name).Type_(type_).Language(language).CreatedAt(createdAt).UpdatedAt(updatedAt).GatewayId(gatewayId).Status(status).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TemplatesAPI.ListTemplatesGrouped``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListTemplatesGrouped`: TemplatesGroupedList
    fmt.Fprintf(os.Stdout, "Response from `TemplatesAPI.ListTemplatesGrouped`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListTemplatesGroupedRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int32** |  | 
 **limit** | **int32** |  | 
 **sort** | **string** | For reversed sort, use &#39;-&#39;. Example: \&quot;sort&#x3D;-updated_at\&quot; | 
 **organizationId** | **string** |  | 
 **name** | **string** |  | 
 **type_** | **string** |  | 
 **language** | **string** |  | 
 **createdAt** | **time.Time** |  | 
 **updatedAt** | **time.Time** |  | 
 **gatewayId** | **string** | When provided results will also contain templates without any gateway set | 
 **status** | **string** |  | 

### Return type

[**TemplatesGroupedList**](TemplatesGroupedList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PatchTemplateDefaults

> Template PatchTemplateDefaults(ctx, id).TemplateDefaultsMutable(templateDefaultsMutable).Execute()

Patch template defaults



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    templateDefaultsMutable := *openapiclient.NewTemplateDefaultsMutable() // TemplateDefaultsMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TemplatesAPI.PatchTemplateDefaults(context.Background(), id).TemplateDefaultsMutable(templateDefaultsMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TemplatesAPI.PatchTemplateDefaults``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PatchTemplateDefaults`: Template
    fmt.Fprintf(os.Stdout, "Response from `TemplatesAPI.PatchTemplateDefaults`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPatchTemplateDefaultsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **templateDefaultsMutable** | [**TemplateDefaultsMutable**](TemplateDefaultsMutable.md) |  | 

### Return type

[**Template**](Template.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RequestTemplateReview

> Template RequestTemplateReview(ctx, id).Execute()

Request template review



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TemplatesAPI.RequestTemplateReview(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TemplatesAPI.RequestTemplateReview``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RequestTemplateReview`: Template
    fmt.Fprintf(os.Stdout, "Response from `TemplatesAPI.RequestTemplateReview`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiRequestTemplateReviewRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Template**](Template.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ReviewTemplates

> Template ReviewTemplates(ctx, id).TemplateReviewData(templateReviewData).Execute()

Review template



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    templateReviewData := *openapiclient.NewTemplateReviewData(false) // TemplateReviewData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TemplatesAPI.ReviewTemplates(context.Background(), id).TemplateReviewData(templateReviewData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TemplatesAPI.ReviewTemplates``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ReviewTemplates`: Template
    fmt.Fprintf(os.Stdout, "Response from `TemplatesAPI.ReviewTemplates`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiReviewTemplatesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **templateReviewData** | [**TemplateReviewData**](TemplateReviewData.md) |  | 

### Return type

[**Template**](Template.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateTemplate

> Template UpdateTemplate(ctx, id).TemplateMutable(templateMutable).Execute()

Update template



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    templateMutable := *openapiclient.NewTemplateMutable("Name_example", "OrganizationId_example", openapiclient.TemplateType("internal"), openapiclient.TemplateVisibility("admin"), "Language_example") // TemplateMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TemplatesAPI.UpdateTemplate(context.Background(), id).TemplateMutable(templateMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TemplatesAPI.UpdateTemplate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateTemplate`: Template
    fmt.Fprintf(os.Stdout, "Response from `TemplatesAPI.UpdateTemplate`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateTemplateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **templateMutable** | [**TemplateMutable**](TemplateMutable.md) |  | 

### Return type

[**Template**](Template.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateTemplateDefaults

> Template UpdateTemplateDefaults(ctx, id).TemplateDefaultsMutable(templateDefaultsMutable).Execute()

Update template defaults



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    templateDefaultsMutable := *openapiclient.NewTemplateDefaultsMutable() // TemplateDefaultsMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TemplatesAPI.UpdateTemplateDefaults(context.Background(), id).TemplateDefaultsMutable(templateDefaultsMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TemplatesAPI.UpdateTemplateDefaults``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateTemplateDefaults`: Template
    fmt.Fprintf(os.Stdout, "Response from `TemplatesAPI.UpdateTemplateDefaults`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateTemplateDefaultsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **templateDefaultsMutable** | [**TemplateDefaultsMutable**](TemplateDefaultsMutable.md) |  | 

### Return type

[**Template**](Template.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

