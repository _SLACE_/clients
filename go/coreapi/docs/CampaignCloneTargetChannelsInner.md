# CampaignCloneTargetChannelsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelId** | **string** |  | 
**InteractionId** | **string** |  | 
**Main** | Pointer to **bool** |  | [optional] 

## Methods

### NewCampaignCloneTargetChannelsInner

`func NewCampaignCloneTargetChannelsInner(channelId string, interactionId string, ) *CampaignCloneTargetChannelsInner`

NewCampaignCloneTargetChannelsInner instantiates a new CampaignCloneTargetChannelsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignCloneTargetChannelsInnerWithDefaults

`func NewCampaignCloneTargetChannelsInnerWithDefaults() *CampaignCloneTargetChannelsInner`

NewCampaignCloneTargetChannelsInnerWithDefaults instantiates a new CampaignCloneTargetChannelsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetChannelId

`func (o *CampaignCloneTargetChannelsInner) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CampaignCloneTargetChannelsInner) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CampaignCloneTargetChannelsInner) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetInteractionId

`func (o *CampaignCloneTargetChannelsInner) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *CampaignCloneTargetChannelsInner) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *CampaignCloneTargetChannelsInner) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.


### GetMain

`func (o *CampaignCloneTargetChannelsInner) GetMain() bool`

GetMain returns the Main field if non-nil, zero value otherwise.

### GetMainOk

`func (o *CampaignCloneTargetChannelsInner) GetMainOk() (*bool, bool)`

GetMainOk returns a tuple with the Main field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMain

`func (o *CampaignCloneTargetChannelsInner) SetMain(v bool)`

SetMain sets Main field to given value.

### HasMain

`func (o *CampaignCloneTargetChannelsInner) HasMain() bool`

HasMain returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


