# RevokeUserPermissionsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Target** | Pointer to **string** |  | [optional] 
**Channels** | Pointer to **[]string** |  | [optional] 

## Methods

### NewRevokeUserPermissionsRequest

`func NewRevokeUserPermissionsRequest() *RevokeUserPermissionsRequest`

NewRevokeUserPermissionsRequest instantiates a new RevokeUserPermissionsRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRevokeUserPermissionsRequestWithDefaults

`func NewRevokeUserPermissionsRequestWithDefaults() *RevokeUserPermissionsRequest`

NewRevokeUserPermissionsRequestWithDefaults instantiates a new RevokeUserPermissionsRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTarget

`func (o *RevokeUserPermissionsRequest) GetTarget() string`

GetTarget returns the Target field if non-nil, zero value otherwise.

### GetTargetOk

`func (o *RevokeUserPermissionsRequest) GetTargetOk() (*string, bool)`

GetTargetOk returns a tuple with the Target field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTarget

`func (o *RevokeUserPermissionsRequest) SetTarget(v string)`

SetTarget sets Target field to given value.

### HasTarget

`func (o *RevokeUserPermissionsRequest) HasTarget() bool`

HasTarget returns a boolean if a field has been set.

### GetChannels

`func (o *RevokeUserPermissionsRequest) GetChannels() []string`

GetChannels returns the Channels field if non-nil, zero value otherwise.

### GetChannelsOk

`func (o *RevokeUserPermissionsRequest) GetChannelsOk() (*[]string, bool)`

GetChannelsOk returns a tuple with the Channels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannels

`func (o *RevokeUserPermissionsRequest) SetChannels(v []string)`

SetChannels sets Channels field to given value.

### HasChannels

`func (o *RevokeUserPermissionsRequest) HasChannels() bool`

HasChannels returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


