# IceBreakerCallToActionsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Question** | Pointer to **string** |  | [optional] 
**Payload** | Pointer to **string** |  | [optional] 

## Methods

### NewIceBreakerCallToActionsInner

`func NewIceBreakerCallToActionsInner() *IceBreakerCallToActionsInner`

NewIceBreakerCallToActionsInner instantiates a new IceBreakerCallToActionsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIceBreakerCallToActionsInnerWithDefaults

`func NewIceBreakerCallToActionsInnerWithDefaults() *IceBreakerCallToActionsInner`

NewIceBreakerCallToActionsInnerWithDefaults instantiates a new IceBreakerCallToActionsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetQuestion

`func (o *IceBreakerCallToActionsInner) GetQuestion() string`

GetQuestion returns the Question field if non-nil, zero value otherwise.

### GetQuestionOk

`func (o *IceBreakerCallToActionsInner) GetQuestionOk() (*string, bool)`

GetQuestionOk returns a tuple with the Question field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuestion

`func (o *IceBreakerCallToActionsInner) SetQuestion(v string)`

SetQuestion sets Question field to given value.

### HasQuestion

`func (o *IceBreakerCallToActionsInner) HasQuestion() bool`

HasQuestion returns a boolean if a field has been set.

### GetPayload

`func (o *IceBreakerCallToActionsInner) GetPayload() string`

GetPayload returns the Payload field if non-nil, zero value otherwise.

### GetPayloadOk

`func (o *IceBreakerCallToActionsInner) GetPayloadOk() (*string, bool)`

GetPayloadOk returns a tuple with the Payload field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayload

`func (o *IceBreakerCallToActionsInner) SetPayload(v string)`

SetPayload sets Payload field to given value.

### HasPayload

`func (o *IceBreakerCallToActionsInner) HasPayload() bool`

HasPayload returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


