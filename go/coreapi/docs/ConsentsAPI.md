# \ConsentsAPI

All URIs are relative to *https://api.slace.com/core*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateNextConsentVersion**](ConsentsAPI.md#CreateNextConsentVersion) | **Post** /organizations/{organization_id}/consents | Create next consent version
[**DeleteConsent**](ConsentsAPI.md#DeleteConsent) | **Delete** /organizations/{organization_id}/consents/{consent_id} | Delete consent version
[**ListConsents**](ConsentsAPI.md#ListConsents) | **Get** /organizations/{organization_id}/consents | List consents



## CreateNextConsentVersion

> Consent CreateNextConsentVersion(ctx, organizationId).Consent(consent).Execute()

Create next consent version

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    consent := *openapiclient.NewConsent("Id_example", "Key_example", "OrganizationId_example", "Type_example", int32(123), false, time.Now(), time.Now()) // Consent |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConsentsAPI.CreateNextConsentVersion(context.Background(), organizationId).Consent(consent).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConsentsAPI.CreateNextConsentVersion``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateNextConsentVersion`: Consent
    fmt.Fprintf(os.Stdout, "Response from `ConsentsAPI.CreateNextConsentVersion`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateNextConsentVersionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **consent** | [**Consent**](Consent.md) |  | 

### Return type

[**Consent**](Consent.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteConsent

> DeleteConsent(ctx, organizationId, consentId).Execute()

Delete consent version

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    consentId := "consentId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ConsentsAPI.DeleteConsent(context.Background(), organizationId, consentId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConsentsAPI.DeleteConsent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**consentId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteConsentRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListConsents

> []Consent ListConsents(ctx, organizationId).Execute()

List consents

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConsentsAPI.ListConsents(context.Background(), organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConsentsAPI.ListConsents``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListConsents`: []Consent
    fmt.Fprintf(os.Stdout, "Response from `ConsentsAPI.ListConsents`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListConsentsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]Consent**](Consent.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

