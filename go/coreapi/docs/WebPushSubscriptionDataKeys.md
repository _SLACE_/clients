# WebPushSubscriptionDataKeys

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Auth** | **string** |  | 
**P256dh** | **string** |  | 

## Methods

### NewWebPushSubscriptionDataKeys

`func NewWebPushSubscriptionDataKeys(auth string, p256dh string, ) *WebPushSubscriptionDataKeys`

NewWebPushSubscriptionDataKeys instantiates a new WebPushSubscriptionDataKeys object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebPushSubscriptionDataKeysWithDefaults

`func NewWebPushSubscriptionDataKeysWithDefaults() *WebPushSubscriptionDataKeys`

NewWebPushSubscriptionDataKeysWithDefaults instantiates a new WebPushSubscriptionDataKeys object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAuth

`func (o *WebPushSubscriptionDataKeys) GetAuth() string`

GetAuth returns the Auth field if non-nil, zero value otherwise.

### GetAuthOk

`func (o *WebPushSubscriptionDataKeys) GetAuthOk() (*string, bool)`

GetAuthOk returns a tuple with the Auth field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuth

`func (o *WebPushSubscriptionDataKeys) SetAuth(v string)`

SetAuth sets Auth field to given value.


### GetP256dh

`func (o *WebPushSubscriptionDataKeys) GetP256dh() string`

GetP256dh returns the P256dh field if non-nil, zero value otherwise.

### GetP256dhOk

`func (o *WebPushSubscriptionDataKeys) GetP256dhOk() (*string, bool)`

GetP256dhOk returns a tuple with the P256dh field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetP256dh

`func (o *WebPushSubscriptionDataKeys) SetP256dh(v string)`

SetP256dh sets P256dh field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


