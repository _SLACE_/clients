# ContactInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Email** | **string** |  | 
**PhoneNumber** | Pointer to **string** |  | [optional] 
**Webpage** | Pointer to **string** |  | [optional] 
**Address** | Pointer to **string** |  | [optional] 
**FullAddress** | Pointer to [**ContactInfoFullAddress**](ContactInfoFullAddress.md) |  | [optional] 

## Methods

### NewContactInfo

`func NewContactInfo(email string, ) *ContactInfo`

NewContactInfo instantiates a new ContactInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactInfoWithDefaults

`func NewContactInfoWithDefaults() *ContactInfo`

NewContactInfoWithDefaults instantiates a new ContactInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetEmail

`func (o *ContactInfo) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *ContactInfo) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *ContactInfo) SetEmail(v string)`

SetEmail sets Email field to given value.


### GetPhoneNumber

`func (o *ContactInfo) GetPhoneNumber() string`

GetPhoneNumber returns the PhoneNumber field if non-nil, zero value otherwise.

### GetPhoneNumberOk

`func (o *ContactInfo) GetPhoneNumberOk() (*string, bool)`

GetPhoneNumberOk returns a tuple with the PhoneNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhoneNumber

`func (o *ContactInfo) SetPhoneNumber(v string)`

SetPhoneNumber sets PhoneNumber field to given value.

### HasPhoneNumber

`func (o *ContactInfo) HasPhoneNumber() bool`

HasPhoneNumber returns a boolean if a field has been set.

### GetWebpage

`func (o *ContactInfo) GetWebpage() string`

GetWebpage returns the Webpage field if non-nil, zero value otherwise.

### GetWebpageOk

`func (o *ContactInfo) GetWebpageOk() (*string, bool)`

GetWebpageOk returns a tuple with the Webpage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWebpage

`func (o *ContactInfo) SetWebpage(v string)`

SetWebpage sets Webpage field to given value.

### HasWebpage

`func (o *ContactInfo) HasWebpage() bool`

HasWebpage returns a boolean if a field has been set.

### GetAddress

`func (o *ContactInfo) GetAddress() string`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *ContactInfo) GetAddressOk() (*string, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *ContactInfo) SetAddress(v string)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *ContactInfo) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetFullAddress

`func (o *ContactInfo) GetFullAddress() ContactInfoFullAddress`

GetFullAddress returns the FullAddress field if non-nil, zero value otherwise.

### GetFullAddressOk

`func (o *ContactInfo) GetFullAddressOk() (*ContactInfoFullAddress, bool)`

GetFullAddressOk returns a tuple with the FullAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFullAddress

`func (o *ContactInfo) SetFullAddress(v ContactInfoFullAddress)`

SetFullAddress sets FullAddress field to given value.

### HasFullAddress

`func (o *ContactInfo) HasFullAddress() bool`

HasFullAddress returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


