# TemplatesGroupedList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]TemplateGrouped**](TemplateGrouped.md) |  | 
**Total** | **int32** |  | 

## Methods

### NewTemplatesGroupedList

`func NewTemplatesGroupedList(results []TemplateGrouped, total int32, ) *TemplatesGroupedList`

NewTemplatesGroupedList instantiates a new TemplatesGroupedList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTemplatesGroupedListWithDefaults

`func NewTemplatesGroupedListWithDefaults() *TemplatesGroupedList`

NewTemplatesGroupedListWithDefaults instantiates a new TemplatesGroupedList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *TemplatesGroupedList) GetResults() []TemplateGrouped`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *TemplatesGroupedList) GetResultsOk() (*[]TemplateGrouped, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *TemplatesGroupedList) SetResults(v []TemplateGrouped)`

SetResults sets Results field to given value.


### GetTotal

`func (o *TemplatesGroupedList) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *TemplatesGroupedList) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *TemplatesGroupedList) SetTotal(v int32)`

SetTotal sets Total field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


