# NotificationSeverity

## Enum


* `SeverityInfo` (value: `10`)

* `SeverityWarning` (value: `20`)

* `SeverityCritical` (value: `30`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


