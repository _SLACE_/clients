# CampaignClone

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TargetChannels** | [**[]CampaignCloneTargetChannelsInner**](CampaignCloneTargetChannelsInner.md) |  | 
**OrganizationId** | **string** |  | 

## Methods

### NewCampaignClone

`func NewCampaignClone(targetChannels []CampaignCloneTargetChannelsInner, organizationId string, ) *CampaignClone`

NewCampaignClone instantiates a new CampaignClone object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignCloneWithDefaults

`func NewCampaignCloneWithDefaults() *CampaignClone`

NewCampaignCloneWithDefaults instantiates a new CampaignClone object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTargetChannels

`func (o *CampaignClone) GetTargetChannels() []CampaignCloneTargetChannelsInner`

GetTargetChannels returns the TargetChannels field if non-nil, zero value otherwise.

### GetTargetChannelsOk

`func (o *CampaignClone) GetTargetChannelsOk() (*[]CampaignCloneTargetChannelsInner, bool)`

GetTargetChannelsOk returns a tuple with the TargetChannels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetChannels

`func (o *CampaignClone) SetTargetChannels(v []CampaignCloneTargetChannelsInner)`

SetTargetChannels sets TargetChannels field to given value.


### GetOrganizationId

`func (o *CampaignClone) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *CampaignClone) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *CampaignClone) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


