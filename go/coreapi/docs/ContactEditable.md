# ContactEditable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | Profile name. Contacts created automatically out of conversations have messenger profile name here | 
**Timezone** | Pointer to **string** | Contact timezone in IANA format | [optional] 
**Mobile** | Pointer to **string** | Mobile number in E.164 format | [optional] 
**MobileVerified** | Pointer to **bool** | NOT EDITABLE VIA API  Flag to mark mobile number as verified | [optional] 
**FirstName** | Pointer to **string** |  | [optional] 
**LastName** | Pointer to **string** |  | [optional] 
**Email** | Pointer to **string** |  | [optional] 
**Gender** | Pointer to **string** |  | [optional] 
**Title** | Pointer to **string** |  | [optional] 
**Formal** | Pointer to **bool** |  | [optional] 
**Birthdate** | Pointer to **string** | Birthdate in format specified in organization settings. Default is yyyy-mm-dd | [optional] 
**DayOfBirth** | Pointer to **int32** | Overriden with birthdate data if provided | [optional] 
**MonthOfBirth** | Pointer to **int32** | Overriden with birthdate data if provided | [optional] 
**Address** | Pointer to [**ContactAddress**](ContactAddress.md) |  | [optional] 
**LoyaltyPoints** | Pointer to **map[string]int32** | NOT EDITABLE VIA API  Stores current loyalty points of a contact. Format [providerName_projectId]: [pointsAmount] } | [optional] 
**LifetimeLoyaltyPoints** | Pointer to **map[string]int32** | NOT EDITABLE VIA API  Stores lifetime loyalty points of a contact. Format { [providerName_projectId]: [pointsAmount] } | [optional] 
**StrictAttributes** | Pointer to [**map[string]ContactStrictAttribute**](ContactStrictAttribute.md) | JSON with custom properties as defined in organization settings. Format: { [propertyName]: { type: [string|float|bool|date|datetime], value: [propertyValue] } } | [optional] 
**CareOf** | Pointer to **string** |  | [optional] 
**Avatars** | Pointer to [**ContactAvatars**](ContactAvatars.md) |  | [optional] 
**Languages** | Pointer to [**ContactLanguages**](ContactLanguages.md) |  | [optional] 
**MobileVerifiedAt** | Pointer to **time.Time** | NOT EDITABLE VIA API  | [optional] 
**ExternalIds** | Pointer to **map[string]interface{}** | NOT EDITABLE VIA API | [optional] 
**ErConfidence** | Pointer to [**ContactERConfidence**](ContactERConfidence.md) |  | [optional] 

## Methods

### NewContactEditable

`func NewContactEditable(name string, ) *ContactEditable`

NewContactEditable instantiates a new ContactEditable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactEditableWithDefaults

`func NewContactEditableWithDefaults() *ContactEditable`

NewContactEditableWithDefaults instantiates a new ContactEditable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ContactEditable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ContactEditable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ContactEditable) SetName(v string)`

SetName sets Name field to given value.


### GetTimezone

`func (o *ContactEditable) GetTimezone() string`

GetTimezone returns the Timezone field if non-nil, zero value otherwise.

### GetTimezoneOk

`func (o *ContactEditable) GetTimezoneOk() (*string, bool)`

GetTimezoneOk returns a tuple with the Timezone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimezone

`func (o *ContactEditable) SetTimezone(v string)`

SetTimezone sets Timezone field to given value.

### HasTimezone

`func (o *ContactEditable) HasTimezone() bool`

HasTimezone returns a boolean if a field has been set.

### GetMobile

`func (o *ContactEditable) GetMobile() string`

GetMobile returns the Mobile field if non-nil, zero value otherwise.

### GetMobileOk

`func (o *ContactEditable) GetMobileOk() (*string, bool)`

GetMobileOk returns a tuple with the Mobile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobile

`func (o *ContactEditable) SetMobile(v string)`

SetMobile sets Mobile field to given value.

### HasMobile

`func (o *ContactEditable) HasMobile() bool`

HasMobile returns a boolean if a field has been set.

### GetMobileVerified

`func (o *ContactEditable) GetMobileVerified() bool`

GetMobileVerified returns the MobileVerified field if non-nil, zero value otherwise.

### GetMobileVerifiedOk

`func (o *ContactEditable) GetMobileVerifiedOk() (*bool, bool)`

GetMobileVerifiedOk returns a tuple with the MobileVerified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobileVerified

`func (o *ContactEditable) SetMobileVerified(v bool)`

SetMobileVerified sets MobileVerified field to given value.

### HasMobileVerified

`func (o *ContactEditable) HasMobileVerified() bool`

HasMobileVerified returns a boolean if a field has been set.

### GetFirstName

`func (o *ContactEditable) GetFirstName() string`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *ContactEditable) GetFirstNameOk() (*string, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *ContactEditable) SetFirstName(v string)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *ContactEditable) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *ContactEditable) GetLastName() string`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *ContactEditable) GetLastNameOk() (*string, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *ContactEditable) SetLastName(v string)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *ContactEditable) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetEmail

`func (o *ContactEditable) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *ContactEditable) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *ContactEditable) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *ContactEditable) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetGender

`func (o *ContactEditable) GetGender() string`

GetGender returns the Gender field if non-nil, zero value otherwise.

### GetGenderOk

`func (o *ContactEditable) GetGenderOk() (*string, bool)`

GetGenderOk returns a tuple with the Gender field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGender

`func (o *ContactEditable) SetGender(v string)`

SetGender sets Gender field to given value.

### HasGender

`func (o *ContactEditable) HasGender() bool`

HasGender returns a boolean if a field has been set.

### GetTitle

`func (o *ContactEditable) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *ContactEditable) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *ContactEditable) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *ContactEditable) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetFormal

`func (o *ContactEditable) GetFormal() bool`

GetFormal returns the Formal field if non-nil, zero value otherwise.

### GetFormalOk

`func (o *ContactEditable) GetFormalOk() (*bool, bool)`

GetFormalOk returns a tuple with the Formal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormal

`func (o *ContactEditable) SetFormal(v bool)`

SetFormal sets Formal field to given value.

### HasFormal

`func (o *ContactEditable) HasFormal() bool`

HasFormal returns a boolean if a field has been set.

### GetBirthdate

`func (o *ContactEditable) GetBirthdate() string`

GetBirthdate returns the Birthdate field if non-nil, zero value otherwise.

### GetBirthdateOk

`func (o *ContactEditable) GetBirthdateOk() (*string, bool)`

GetBirthdateOk returns a tuple with the Birthdate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBirthdate

`func (o *ContactEditable) SetBirthdate(v string)`

SetBirthdate sets Birthdate field to given value.

### HasBirthdate

`func (o *ContactEditable) HasBirthdate() bool`

HasBirthdate returns a boolean if a field has been set.

### GetDayOfBirth

`func (o *ContactEditable) GetDayOfBirth() int32`

GetDayOfBirth returns the DayOfBirth field if non-nil, zero value otherwise.

### GetDayOfBirthOk

`func (o *ContactEditable) GetDayOfBirthOk() (*int32, bool)`

GetDayOfBirthOk returns a tuple with the DayOfBirth field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDayOfBirth

`func (o *ContactEditable) SetDayOfBirth(v int32)`

SetDayOfBirth sets DayOfBirth field to given value.

### HasDayOfBirth

`func (o *ContactEditable) HasDayOfBirth() bool`

HasDayOfBirth returns a boolean if a field has been set.

### GetMonthOfBirth

`func (o *ContactEditable) GetMonthOfBirth() int32`

GetMonthOfBirth returns the MonthOfBirth field if non-nil, zero value otherwise.

### GetMonthOfBirthOk

`func (o *ContactEditable) GetMonthOfBirthOk() (*int32, bool)`

GetMonthOfBirthOk returns a tuple with the MonthOfBirth field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMonthOfBirth

`func (o *ContactEditable) SetMonthOfBirth(v int32)`

SetMonthOfBirth sets MonthOfBirth field to given value.

### HasMonthOfBirth

`func (o *ContactEditable) HasMonthOfBirth() bool`

HasMonthOfBirth returns a boolean if a field has been set.

### GetAddress

`func (o *ContactEditable) GetAddress() ContactAddress`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *ContactEditable) GetAddressOk() (*ContactAddress, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *ContactEditable) SetAddress(v ContactAddress)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *ContactEditable) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetLoyaltyPoints

`func (o *ContactEditable) GetLoyaltyPoints() map[string]int32`

GetLoyaltyPoints returns the LoyaltyPoints field if non-nil, zero value otherwise.

### GetLoyaltyPointsOk

`func (o *ContactEditable) GetLoyaltyPointsOk() (*map[string]int32, bool)`

GetLoyaltyPointsOk returns a tuple with the LoyaltyPoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLoyaltyPoints

`func (o *ContactEditable) SetLoyaltyPoints(v map[string]int32)`

SetLoyaltyPoints sets LoyaltyPoints field to given value.

### HasLoyaltyPoints

`func (o *ContactEditable) HasLoyaltyPoints() bool`

HasLoyaltyPoints returns a boolean if a field has been set.

### GetLifetimeLoyaltyPoints

`func (o *ContactEditable) GetLifetimeLoyaltyPoints() map[string]int32`

GetLifetimeLoyaltyPoints returns the LifetimeLoyaltyPoints field if non-nil, zero value otherwise.

### GetLifetimeLoyaltyPointsOk

`func (o *ContactEditable) GetLifetimeLoyaltyPointsOk() (*map[string]int32, bool)`

GetLifetimeLoyaltyPointsOk returns a tuple with the LifetimeLoyaltyPoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLifetimeLoyaltyPoints

`func (o *ContactEditable) SetLifetimeLoyaltyPoints(v map[string]int32)`

SetLifetimeLoyaltyPoints sets LifetimeLoyaltyPoints field to given value.

### HasLifetimeLoyaltyPoints

`func (o *ContactEditable) HasLifetimeLoyaltyPoints() bool`

HasLifetimeLoyaltyPoints returns a boolean if a field has been set.

### GetStrictAttributes

`func (o *ContactEditable) GetStrictAttributes() map[string]ContactStrictAttribute`

GetStrictAttributes returns the StrictAttributes field if non-nil, zero value otherwise.

### GetStrictAttributesOk

`func (o *ContactEditable) GetStrictAttributesOk() (*map[string]ContactStrictAttribute, bool)`

GetStrictAttributesOk returns a tuple with the StrictAttributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStrictAttributes

`func (o *ContactEditable) SetStrictAttributes(v map[string]ContactStrictAttribute)`

SetStrictAttributes sets StrictAttributes field to given value.

### HasStrictAttributes

`func (o *ContactEditable) HasStrictAttributes() bool`

HasStrictAttributes returns a boolean if a field has been set.

### GetCareOf

`func (o *ContactEditable) GetCareOf() string`

GetCareOf returns the CareOf field if non-nil, zero value otherwise.

### GetCareOfOk

`func (o *ContactEditable) GetCareOfOk() (*string, bool)`

GetCareOfOk returns a tuple with the CareOf field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCareOf

`func (o *ContactEditable) SetCareOf(v string)`

SetCareOf sets CareOf field to given value.

### HasCareOf

`func (o *ContactEditable) HasCareOf() bool`

HasCareOf returns a boolean if a field has been set.

### GetAvatars

`func (o *ContactEditable) GetAvatars() ContactAvatars`

GetAvatars returns the Avatars field if non-nil, zero value otherwise.

### GetAvatarsOk

`func (o *ContactEditable) GetAvatarsOk() (*ContactAvatars, bool)`

GetAvatarsOk returns a tuple with the Avatars field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAvatars

`func (o *ContactEditable) SetAvatars(v ContactAvatars)`

SetAvatars sets Avatars field to given value.

### HasAvatars

`func (o *ContactEditable) HasAvatars() bool`

HasAvatars returns a boolean if a field has been set.

### GetLanguages

`func (o *ContactEditable) GetLanguages() ContactLanguages`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *ContactEditable) GetLanguagesOk() (*ContactLanguages, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *ContactEditable) SetLanguages(v ContactLanguages)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *ContactEditable) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetMobileVerifiedAt

`func (o *ContactEditable) GetMobileVerifiedAt() time.Time`

GetMobileVerifiedAt returns the MobileVerifiedAt field if non-nil, zero value otherwise.

### GetMobileVerifiedAtOk

`func (o *ContactEditable) GetMobileVerifiedAtOk() (*time.Time, bool)`

GetMobileVerifiedAtOk returns a tuple with the MobileVerifiedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobileVerifiedAt

`func (o *ContactEditable) SetMobileVerifiedAt(v time.Time)`

SetMobileVerifiedAt sets MobileVerifiedAt field to given value.

### HasMobileVerifiedAt

`func (o *ContactEditable) HasMobileVerifiedAt() bool`

HasMobileVerifiedAt returns a boolean if a field has been set.

### GetExternalIds

`func (o *ContactEditable) GetExternalIds() map[string]interface{}`

GetExternalIds returns the ExternalIds field if non-nil, zero value otherwise.

### GetExternalIdsOk

`func (o *ContactEditable) GetExternalIdsOk() (*map[string]interface{}, bool)`

GetExternalIdsOk returns a tuple with the ExternalIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalIds

`func (o *ContactEditable) SetExternalIds(v map[string]interface{})`

SetExternalIds sets ExternalIds field to given value.

### HasExternalIds

`func (o *ContactEditable) HasExternalIds() bool`

HasExternalIds returns a boolean if a field has been set.

### GetErConfidence

`func (o *ContactEditable) GetErConfidence() ContactERConfidence`

GetErConfidence returns the ErConfidence field if non-nil, zero value otherwise.

### GetErConfidenceOk

`func (o *ContactEditable) GetErConfidenceOk() (*ContactERConfidence, bool)`

GetErConfidenceOk returns a tuple with the ErConfidence field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErConfidence

`func (o *ContactEditable) SetErConfidence(v ContactERConfidence)`

SetErConfidence sets ErConfidence field to given value.

### HasErConfidence

`func (o *ContactEditable) HasErConfidence() bool`

HasErConfidence returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


