# CustomerMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**ContactInfo** | [**ContactInfo**](ContactInfo.md) |  | 
**Active** | Pointer to **bool** |  | [optional] 
**BillingInfo** | Pointer to **string** |  | [optional] 

## Methods

### NewCustomerMutable

`func NewCustomerMutable(name string, contactInfo ContactInfo, ) *CustomerMutable`

NewCustomerMutable instantiates a new CustomerMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCustomerMutableWithDefaults

`func NewCustomerMutableWithDefaults() *CustomerMutable`

NewCustomerMutableWithDefaults instantiates a new CustomerMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *CustomerMutable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CustomerMutable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CustomerMutable) SetName(v string)`

SetName sets Name field to given value.


### GetContactInfo

`func (o *CustomerMutable) GetContactInfo() ContactInfo`

GetContactInfo returns the ContactInfo field if non-nil, zero value otherwise.

### GetContactInfoOk

`func (o *CustomerMutable) GetContactInfoOk() (*ContactInfo, bool)`

GetContactInfoOk returns a tuple with the ContactInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactInfo

`func (o *CustomerMutable) SetContactInfo(v ContactInfo)`

SetContactInfo sets ContactInfo field to given value.


### GetActive

`func (o *CustomerMutable) GetActive() bool`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *CustomerMutable) GetActiveOk() (*bool, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *CustomerMutable) SetActive(v bool)`

SetActive sets Active field to given value.

### HasActive

`func (o *CustomerMutable) HasActive() bool`

HasActive returns a boolean if a field has been set.

### GetBillingInfo

`func (o *CustomerMutable) GetBillingInfo() string`

GetBillingInfo returns the BillingInfo field if non-nil, zero value otherwise.

### GetBillingInfoOk

`func (o *CustomerMutable) GetBillingInfoOk() (*string, bool)`

GetBillingInfoOk returns a tuple with the BillingInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingInfo

`func (o *CustomerMutable) SetBillingInfo(v string)`

SetBillingInfo sets BillingInfo field to given value.

### HasBillingInfo

`func (o *CustomerMutable) HasBillingInfo() bool`

HasBillingInfo returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


