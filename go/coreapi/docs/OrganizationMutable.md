# OrganizationMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CustomerId** | **string** |  | 
**Name** | **string** |  | 
**ContactInfo** | [**ContactInfo**](ContactInfo.md) |  | 
**Active** | Pointer to **bool** |  | [optional] 
**ExternalReferences** | Pointer to **map[string]string** | The map of any string values used for cutom external references.  | [optional] 
**Timezone** | Pointer to **string** |  | [optional] 
**OfficeHours** | Pointer to [**OfficeHoursSettings**](OfficeHoursSettings.md) |  | [optional] 
**InfoMessagesActive** | Pointer to **bool** |  | [optional] 
**DirectCalling** | Pointer to **bool** |  | [optional] 
**ActivityThreshold** | [**Threshold**](Threshold.md) |  | 
**ExternalSystems** | Pointer to [**[]ExternalSystem**](ExternalSystem.md) |  | [optional] 
**CooldownThreshold** | Pointer to [**Threshold**](Threshold.md) |  | [optional] 
**AutoresponderCooldown** | [**Threshold**](Threshold.md) |  | 
**CouponProviders** | Pointer to [**[]CouponProvider**](CouponProvider.md) |  | [optional] 
**ExternalIntegrations** | Pointer to [**ExternalIntegrations**](ExternalIntegrations.md) |  | [optional] 
**LifecycleSettings** | Pointer to [**OrganizationMutableLifecycleSettings**](OrganizationMutableLifecycleSettings.md) |  | [optional] 
**ContactAttributes** | Pointer to [**[]ContactAttribute**](ContactAttribute.md) |  | [optional] 
**DateFormat** | Pointer to **string** | \&quot;d\&quot; -&gt; \&quot;2006-01-02\&quot; \&quot;u\&quot; -&gt; \&quot;01/02/2006\&quot; \&quot;e\&quot; -&gt; \&quot;02/01/2006\&quot; \&quot;x\&quot; -&gt; \&quot;02.01.2006\&quot; | [optional] 
**StopKeywords** | **[]string** |  | 
**LanguageSettings** | Pointer to [**OrganizationMutableLanguageSettings**](OrganizationMutableLanguageSettings.md) |  | [optional] 
**Formality** | [**Formality**](Formality.md) |  | 
**ValidationCenterSettings** | Pointer to [**OrganizationMutableValidationCenterSettings**](OrganizationMutableValidationCenterSettings.md) |  | [optional] 
**PolyprintShopId** | Pointer to **string** |  | [optional] 
**PolyprintGroupId** | Pointer to **string** |  | [optional] 
**PolyprintNfcProductId** | Pointer to **string** |  | [optional] 
**DefaultWhatsappProvider** | Pointer to [**Provider**](Provider.md) |  | [optional] 
**BillingAccountId** | Pointer to **string** | Default billing account for newly created org. If none is selected, new will be auto-created. | [optional] 

## Methods

### NewOrganizationMutable

`func NewOrganizationMutable(customerId string, name string, contactInfo ContactInfo, activityThreshold Threshold, autoresponderCooldown Threshold, stopKeywords []string, formality Formality, ) *OrganizationMutable`

NewOrganizationMutable instantiates a new OrganizationMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrganizationMutableWithDefaults

`func NewOrganizationMutableWithDefaults() *OrganizationMutable`

NewOrganizationMutableWithDefaults instantiates a new OrganizationMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCustomerId

`func (o *OrganizationMutable) GetCustomerId() string`

GetCustomerId returns the CustomerId field if non-nil, zero value otherwise.

### GetCustomerIdOk

`func (o *OrganizationMutable) GetCustomerIdOk() (*string, bool)`

GetCustomerIdOk returns a tuple with the CustomerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerId

`func (o *OrganizationMutable) SetCustomerId(v string)`

SetCustomerId sets CustomerId field to given value.


### GetName

`func (o *OrganizationMutable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *OrganizationMutable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *OrganizationMutable) SetName(v string)`

SetName sets Name field to given value.


### GetContactInfo

`func (o *OrganizationMutable) GetContactInfo() ContactInfo`

GetContactInfo returns the ContactInfo field if non-nil, zero value otherwise.

### GetContactInfoOk

`func (o *OrganizationMutable) GetContactInfoOk() (*ContactInfo, bool)`

GetContactInfoOk returns a tuple with the ContactInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactInfo

`func (o *OrganizationMutable) SetContactInfo(v ContactInfo)`

SetContactInfo sets ContactInfo field to given value.


### GetActive

`func (o *OrganizationMutable) GetActive() bool`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *OrganizationMutable) GetActiveOk() (*bool, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *OrganizationMutable) SetActive(v bool)`

SetActive sets Active field to given value.

### HasActive

`func (o *OrganizationMutable) HasActive() bool`

HasActive returns a boolean if a field has been set.

### GetExternalReferences

`func (o *OrganizationMutable) GetExternalReferences() map[string]string`

GetExternalReferences returns the ExternalReferences field if non-nil, zero value otherwise.

### GetExternalReferencesOk

`func (o *OrganizationMutable) GetExternalReferencesOk() (*map[string]string, bool)`

GetExternalReferencesOk returns a tuple with the ExternalReferences field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalReferences

`func (o *OrganizationMutable) SetExternalReferences(v map[string]string)`

SetExternalReferences sets ExternalReferences field to given value.

### HasExternalReferences

`func (o *OrganizationMutable) HasExternalReferences() bool`

HasExternalReferences returns a boolean if a field has been set.

### GetTimezone

`func (o *OrganizationMutable) GetTimezone() string`

GetTimezone returns the Timezone field if non-nil, zero value otherwise.

### GetTimezoneOk

`func (o *OrganizationMutable) GetTimezoneOk() (*string, bool)`

GetTimezoneOk returns a tuple with the Timezone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimezone

`func (o *OrganizationMutable) SetTimezone(v string)`

SetTimezone sets Timezone field to given value.

### HasTimezone

`func (o *OrganizationMutable) HasTimezone() bool`

HasTimezone returns a boolean if a field has been set.

### GetOfficeHours

`func (o *OrganizationMutable) GetOfficeHours() OfficeHoursSettings`

GetOfficeHours returns the OfficeHours field if non-nil, zero value otherwise.

### GetOfficeHoursOk

`func (o *OrganizationMutable) GetOfficeHoursOk() (*OfficeHoursSettings, bool)`

GetOfficeHoursOk returns a tuple with the OfficeHours field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOfficeHours

`func (o *OrganizationMutable) SetOfficeHours(v OfficeHoursSettings)`

SetOfficeHours sets OfficeHours field to given value.

### HasOfficeHours

`func (o *OrganizationMutable) HasOfficeHours() bool`

HasOfficeHours returns a boolean if a field has been set.

### GetInfoMessagesActive

`func (o *OrganizationMutable) GetInfoMessagesActive() bool`

GetInfoMessagesActive returns the InfoMessagesActive field if non-nil, zero value otherwise.

### GetInfoMessagesActiveOk

`func (o *OrganizationMutable) GetInfoMessagesActiveOk() (*bool, bool)`

GetInfoMessagesActiveOk returns a tuple with the InfoMessagesActive field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInfoMessagesActive

`func (o *OrganizationMutable) SetInfoMessagesActive(v bool)`

SetInfoMessagesActive sets InfoMessagesActive field to given value.

### HasInfoMessagesActive

`func (o *OrganizationMutable) HasInfoMessagesActive() bool`

HasInfoMessagesActive returns a boolean if a field has been set.

### GetDirectCalling

`func (o *OrganizationMutable) GetDirectCalling() bool`

GetDirectCalling returns the DirectCalling field if non-nil, zero value otherwise.

### GetDirectCallingOk

`func (o *OrganizationMutable) GetDirectCallingOk() (*bool, bool)`

GetDirectCallingOk returns a tuple with the DirectCalling field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDirectCalling

`func (o *OrganizationMutable) SetDirectCalling(v bool)`

SetDirectCalling sets DirectCalling field to given value.

### HasDirectCalling

`func (o *OrganizationMutable) HasDirectCalling() bool`

HasDirectCalling returns a boolean if a field has been set.

### GetActivityThreshold

`func (o *OrganizationMutable) GetActivityThreshold() Threshold`

GetActivityThreshold returns the ActivityThreshold field if non-nil, zero value otherwise.

### GetActivityThresholdOk

`func (o *OrganizationMutable) GetActivityThresholdOk() (*Threshold, bool)`

GetActivityThresholdOk returns a tuple with the ActivityThreshold field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActivityThreshold

`func (o *OrganizationMutable) SetActivityThreshold(v Threshold)`

SetActivityThreshold sets ActivityThreshold field to given value.


### GetExternalSystems

`func (o *OrganizationMutable) GetExternalSystems() []ExternalSystem`

GetExternalSystems returns the ExternalSystems field if non-nil, zero value otherwise.

### GetExternalSystemsOk

`func (o *OrganizationMutable) GetExternalSystemsOk() (*[]ExternalSystem, bool)`

GetExternalSystemsOk returns a tuple with the ExternalSystems field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalSystems

`func (o *OrganizationMutable) SetExternalSystems(v []ExternalSystem)`

SetExternalSystems sets ExternalSystems field to given value.

### HasExternalSystems

`func (o *OrganizationMutable) HasExternalSystems() bool`

HasExternalSystems returns a boolean if a field has been set.

### GetCooldownThreshold

`func (o *OrganizationMutable) GetCooldownThreshold() Threshold`

GetCooldownThreshold returns the CooldownThreshold field if non-nil, zero value otherwise.

### GetCooldownThresholdOk

`func (o *OrganizationMutable) GetCooldownThresholdOk() (*Threshold, bool)`

GetCooldownThresholdOk returns a tuple with the CooldownThreshold field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCooldownThreshold

`func (o *OrganizationMutable) SetCooldownThreshold(v Threshold)`

SetCooldownThreshold sets CooldownThreshold field to given value.

### HasCooldownThreshold

`func (o *OrganizationMutable) HasCooldownThreshold() bool`

HasCooldownThreshold returns a boolean if a field has been set.

### GetAutoresponderCooldown

`func (o *OrganizationMutable) GetAutoresponderCooldown() Threshold`

GetAutoresponderCooldown returns the AutoresponderCooldown field if non-nil, zero value otherwise.

### GetAutoresponderCooldownOk

`func (o *OrganizationMutable) GetAutoresponderCooldownOk() (*Threshold, bool)`

GetAutoresponderCooldownOk returns a tuple with the AutoresponderCooldown field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAutoresponderCooldown

`func (o *OrganizationMutable) SetAutoresponderCooldown(v Threshold)`

SetAutoresponderCooldown sets AutoresponderCooldown field to given value.


### GetCouponProviders

`func (o *OrganizationMutable) GetCouponProviders() []CouponProvider`

GetCouponProviders returns the CouponProviders field if non-nil, zero value otherwise.

### GetCouponProvidersOk

`func (o *OrganizationMutable) GetCouponProvidersOk() (*[]CouponProvider, bool)`

GetCouponProvidersOk returns a tuple with the CouponProviders field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCouponProviders

`func (o *OrganizationMutable) SetCouponProviders(v []CouponProvider)`

SetCouponProviders sets CouponProviders field to given value.

### HasCouponProviders

`func (o *OrganizationMutable) HasCouponProviders() bool`

HasCouponProviders returns a boolean if a field has been set.

### GetExternalIntegrations

`func (o *OrganizationMutable) GetExternalIntegrations() ExternalIntegrations`

GetExternalIntegrations returns the ExternalIntegrations field if non-nil, zero value otherwise.

### GetExternalIntegrationsOk

`func (o *OrganizationMutable) GetExternalIntegrationsOk() (*ExternalIntegrations, bool)`

GetExternalIntegrationsOk returns a tuple with the ExternalIntegrations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalIntegrations

`func (o *OrganizationMutable) SetExternalIntegrations(v ExternalIntegrations)`

SetExternalIntegrations sets ExternalIntegrations field to given value.

### HasExternalIntegrations

`func (o *OrganizationMutable) HasExternalIntegrations() bool`

HasExternalIntegrations returns a boolean if a field has been set.

### GetLifecycleSettings

`func (o *OrganizationMutable) GetLifecycleSettings() OrganizationMutableLifecycleSettings`

GetLifecycleSettings returns the LifecycleSettings field if non-nil, zero value otherwise.

### GetLifecycleSettingsOk

`func (o *OrganizationMutable) GetLifecycleSettingsOk() (*OrganizationMutableLifecycleSettings, bool)`

GetLifecycleSettingsOk returns a tuple with the LifecycleSettings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLifecycleSettings

`func (o *OrganizationMutable) SetLifecycleSettings(v OrganizationMutableLifecycleSettings)`

SetLifecycleSettings sets LifecycleSettings field to given value.

### HasLifecycleSettings

`func (o *OrganizationMutable) HasLifecycleSettings() bool`

HasLifecycleSettings returns a boolean if a field has been set.

### GetContactAttributes

`func (o *OrganizationMutable) GetContactAttributes() []ContactAttribute`

GetContactAttributes returns the ContactAttributes field if non-nil, zero value otherwise.

### GetContactAttributesOk

`func (o *OrganizationMutable) GetContactAttributesOk() (*[]ContactAttribute, bool)`

GetContactAttributesOk returns a tuple with the ContactAttributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactAttributes

`func (o *OrganizationMutable) SetContactAttributes(v []ContactAttribute)`

SetContactAttributes sets ContactAttributes field to given value.

### HasContactAttributes

`func (o *OrganizationMutable) HasContactAttributes() bool`

HasContactAttributes returns a boolean if a field has been set.

### GetDateFormat

`func (o *OrganizationMutable) GetDateFormat() string`

GetDateFormat returns the DateFormat field if non-nil, zero value otherwise.

### GetDateFormatOk

`func (o *OrganizationMutable) GetDateFormatOk() (*string, bool)`

GetDateFormatOk returns a tuple with the DateFormat field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateFormat

`func (o *OrganizationMutable) SetDateFormat(v string)`

SetDateFormat sets DateFormat field to given value.

### HasDateFormat

`func (o *OrganizationMutable) HasDateFormat() bool`

HasDateFormat returns a boolean if a field has been set.

### GetStopKeywords

`func (o *OrganizationMutable) GetStopKeywords() []string`

GetStopKeywords returns the StopKeywords field if non-nil, zero value otherwise.

### GetStopKeywordsOk

`func (o *OrganizationMutable) GetStopKeywordsOk() (*[]string, bool)`

GetStopKeywordsOk returns a tuple with the StopKeywords field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStopKeywords

`func (o *OrganizationMutable) SetStopKeywords(v []string)`

SetStopKeywords sets StopKeywords field to given value.


### GetLanguageSettings

`func (o *OrganizationMutable) GetLanguageSettings() OrganizationMutableLanguageSettings`

GetLanguageSettings returns the LanguageSettings field if non-nil, zero value otherwise.

### GetLanguageSettingsOk

`func (o *OrganizationMutable) GetLanguageSettingsOk() (*OrganizationMutableLanguageSettings, bool)`

GetLanguageSettingsOk returns a tuple with the LanguageSettings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguageSettings

`func (o *OrganizationMutable) SetLanguageSettings(v OrganizationMutableLanguageSettings)`

SetLanguageSettings sets LanguageSettings field to given value.

### HasLanguageSettings

`func (o *OrganizationMutable) HasLanguageSettings() bool`

HasLanguageSettings returns a boolean if a field has been set.

### GetFormality

`func (o *OrganizationMutable) GetFormality() Formality`

GetFormality returns the Formality field if non-nil, zero value otherwise.

### GetFormalityOk

`func (o *OrganizationMutable) GetFormalityOk() (*Formality, bool)`

GetFormalityOk returns a tuple with the Formality field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormality

`func (o *OrganizationMutable) SetFormality(v Formality)`

SetFormality sets Formality field to given value.


### GetValidationCenterSettings

`func (o *OrganizationMutable) GetValidationCenterSettings() OrganizationMutableValidationCenterSettings`

GetValidationCenterSettings returns the ValidationCenterSettings field if non-nil, zero value otherwise.

### GetValidationCenterSettingsOk

`func (o *OrganizationMutable) GetValidationCenterSettingsOk() (*OrganizationMutableValidationCenterSettings, bool)`

GetValidationCenterSettingsOk returns a tuple with the ValidationCenterSettings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidationCenterSettings

`func (o *OrganizationMutable) SetValidationCenterSettings(v OrganizationMutableValidationCenterSettings)`

SetValidationCenterSettings sets ValidationCenterSettings field to given value.

### HasValidationCenterSettings

`func (o *OrganizationMutable) HasValidationCenterSettings() bool`

HasValidationCenterSettings returns a boolean if a field has been set.

### GetPolyprintShopId

`func (o *OrganizationMutable) GetPolyprintShopId() string`

GetPolyprintShopId returns the PolyprintShopId field if non-nil, zero value otherwise.

### GetPolyprintShopIdOk

`func (o *OrganizationMutable) GetPolyprintShopIdOk() (*string, bool)`

GetPolyprintShopIdOk returns a tuple with the PolyprintShopId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolyprintShopId

`func (o *OrganizationMutable) SetPolyprintShopId(v string)`

SetPolyprintShopId sets PolyprintShopId field to given value.

### HasPolyprintShopId

`func (o *OrganizationMutable) HasPolyprintShopId() bool`

HasPolyprintShopId returns a boolean if a field has been set.

### GetPolyprintGroupId

`func (o *OrganizationMutable) GetPolyprintGroupId() string`

GetPolyprintGroupId returns the PolyprintGroupId field if non-nil, zero value otherwise.

### GetPolyprintGroupIdOk

`func (o *OrganizationMutable) GetPolyprintGroupIdOk() (*string, bool)`

GetPolyprintGroupIdOk returns a tuple with the PolyprintGroupId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolyprintGroupId

`func (o *OrganizationMutable) SetPolyprintGroupId(v string)`

SetPolyprintGroupId sets PolyprintGroupId field to given value.

### HasPolyprintGroupId

`func (o *OrganizationMutable) HasPolyprintGroupId() bool`

HasPolyprintGroupId returns a boolean if a field has been set.

### GetPolyprintNfcProductId

`func (o *OrganizationMutable) GetPolyprintNfcProductId() string`

GetPolyprintNfcProductId returns the PolyprintNfcProductId field if non-nil, zero value otherwise.

### GetPolyprintNfcProductIdOk

`func (o *OrganizationMutable) GetPolyprintNfcProductIdOk() (*string, bool)`

GetPolyprintNfcProductIdOk returns a tuple with the PolyprintNfcProductId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolyprintNfcProductId

`func (o *OrganizationMutable) SetPolyprintNfcProductId(v string)`

SetPolyprintNfcProductId sets PolyprintNfcProductId field to given value.

### HasPolyprintNfcProductId

`func (o *OrganizationMutable) HasPolyprintNfcProductId() bool`

HasPolyprintNfcProductId returns a boolean if a field has been set.

### GetDefaultWhatsappProvider

`func (o *OrganizationMutable) GetDefaultWhatsappProvider() Provider`

GetDefaultWhatsappProvider returns the DefaultWhatsappProvider field if non-nil, zero value otherwise.

### GetDefaultWhatsappProviderOk

`func (o *OrganizationMutable) GetDefaultWhatsappProviderOk() (*Provider, bool)`

GetDefaultWhatsappProviderOk returns a tuple with the DefaultWhatsappProvider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultWhatsappProvider

`func (o *OrganizationMutable) SetDefaultWhatsappProvider(v Provider)`

SetDefaultWhatsappProvider sets DefaultWhatsappProvider field to given value.

### HasDefaultWhatsappProvider

`func (o *OrganizationMutable) HasDefaultWhatsappProvider() bool`

HasDefaultWhatsappProvider returns a boolean if a field has been set.

### GetBillingAccountId

`func (o *OrganizationMutable) GetBillingAccountId() string`

GetBillingAccountId returns the BillingAccountId field if non-nil, zero value otherwise.

### GetBillingAccountIdOk

`func (o *OrganizationMutable) GetBillingAccountIdOk() (*string, bool)`

GetBillingAccountIdOk returns a tuple with the BillingAccountId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingAccountId

`func (o *OrganizationMutable) SetBillingAccountId(v string)`

SetBillingAccountId sets BillingAccountId field to given value.

### HasBillingAccountId

`func (o *OrganizationMutable) HasBillingAccountId() bool`

HasBillingAccountId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


