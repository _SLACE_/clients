# Permission

## Enum


* `PERMISSION_DENIED` (value: `"Denied"`)

* `PERMISSION_READ` (value: `"Read"`)

* `PERMISSION_WRITE` (value: `"Write"`)

* `PERMISSION_ADMIN` (value: `"Admin"`)

* `PERMISSION_OWNER` (value: `"Owner"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


