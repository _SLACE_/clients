# MediaCenterFileVariant

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Key** | **string** |  | 
**ChannelId** | **string** |  | 
**MediaFileId** | **string** |  | 
**MediaFile** | Pointer to [**MediaCenterFile**](MediaCenterFile.md) |  | [optional] 
**MimeType** | **string** |  | 
**Type** | **string** |  | 
**StorageId** | **string** |  | 
**Size** | **int32** | Byte size | 
**Width** | Pointer to **int32** |  | [optional] 
**Height** | Pointer to **int32** |  | [optional] 
**Url** | **string** |  | 
**ValidFrom** | Pointer to **time.Time** |  | [optional] 
**ValidUntil** | Pointer to **time.Time** |  | [optional] 
**CreatedAt** | **time.Time** |  | 
**FallbackLanguage** | Pointer to **string** |  | [optional] 
**Translations** | Pointer to [**[]MediaCenterTranslation**](MediaCenterTranslation.md) |  | [optional] 

## Methods

### NewMediaCenterFileVariant

`func NewMediaCenterFileVariant(id string, key string, channelId string, mediaFileId string, mimeType string, type_ string, storageId string, size int32, url string, createdAt time.Time, ) *MediaCenterFileVariant`

NewMediaCenterFileVariant instantiates a new MediaCenterFileVariant object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMediaCenterFileVariantWithDefaults

`func NewMediaCenterFileVariantWithDefaults() *MediaCenterFileVariant`

NewMediaCenterFileVariantWithDefaults instantiates a new MediaCenterFileVariant object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *MediaCenterFileVariant) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *MediaCenterFileVariant) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *MediaCenterFileVariant) SetId(v string)`

SetId sets Id field to given value.


### GetKey

`func (o *MediaCenterFileVariant) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *MediaCenterFileVariant) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *MediaCenterFileVariant) SetKey(v string)`

SetKey sets Key field to given value.


### GetChannelId

`func (o *MediaCenterFileVariant) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *MediaCenterFileVariant) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *MediaCenterFileVariant) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetMediaFileId

`func (o *MediaCenterFileVariant) GetMediaFileId() string`

GetMediaFileId returns the MediaFileId field if non-nil, zero value otherwise.

### GetMediaFileIdOk

`func (o *MediaCenterFileVariant) GetMediaFileIdOk() (*string, bool)`

GetMediaFileIdOk returns a tuple with the MediaFileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaFileId

`func (o *MediaCenterFileVariant) SetMediaFileId(v string)`

SetMediaFileId sets MediaFileId field to given value.


### GetMediaFile

`func (o *MediaCenterFileVariant) GetMediaFile() MediaCenterFile`

GetMediaFile returns the MediaFile field if non-nil, zero value otherwise.

### GetMediaFileOk

`func (o *MediaCenterFileVariant) GetMediaFileOk() (*MediaCenterFile, bool)`

GetMediaFileOk returns a tuple with the MediaFile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaFile

`func (o *MediaCenterFileVariant) SetMediaFile(v MediaCenterFile)`

SetMediaFile sets MediaFile field to given value.

### HasMediaFile

`func (o *MediaCenterFileVariant) HasMediaFile() bool`

HasMediaFile returns a boolean if a field has been set.

### GetMimeType

`func (o *MediaCenterFileVariant) GetMimeType() string`

GetMimeType returns the MimeType field if non-nil, zero value otherwise.

### GetMimeTypeOk

`func (o *MediaCenterFileVariant) GetMimeTypeOk() (*string, bool)`

GetMimeTypeOk returns a tuple with the MimeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMimeType

`func (o *MediaCenterFileVariant) SetMimeType(v string)`

SetMimeType sets MimeType field to given value.


### GetType

`func (o *MediaCenterFileVariant) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MediaCenterFileVariant) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MediaCenterFileVariant) SetType(v string)`

SetType sets Type field to given value.


### GetStorageId

`func (o *MediaCenterFileVariant) GetStorageId() string`

GetStorageId returns the StorageId field if non-nil, zero value otherwise.

### GetStorageIdOk

`func (o *MediaCenterFileVariant) GetStorageIdOk() (*string, bool)`

GetStorageIdOk returns a tuple with the StorageId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStorageId

`func (o *MediaCenterFileVariant) SetStorageId(v string)`

SetStorageId sets StorageId field to given value.


### GetSize

`func (o *MediaCenterFileVariant) GetSize() int32`

GetSize returns the Size field if non-nil, zero value otherwise.

### GetSizeOk

`func (o *MediaCenterFileVariant) GetSizeOk() (*int32, bool)`

GetSizeOk returns a tuple with the Size field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSize

`func (o *MediaCenterFileVariant) SetSize(v int32)`

SetSize sets Size field to given value.


### GetWidth

`func (o *MediaCenterFileVariant) GetWidth() int32`

GetWidth returns the Width field if non-nil, zero value otherwise.

### GetWidthOk

`func (o *MediaCenterFileVariant) GetWidthOk() (*int32, bool)`

GetWidthOk returns a tuple with the Width field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWidth

`func (o *MediaCenterFileVariant) SetWidth(v int32)`

SetWidth sets Width field to given value.

### HasWidth

`func (o *MediaCenterFileVariant) HasWidth() bool`

HasWidth returns a boolean if a field has been set.

### GetHeight

`func (o *MediaCenterFileVariant) GetHeight() int32`

GetHeight returns the Height field if non-nil, zero value otherwise.

### GetHeightOk

`func (o *MediaCenterFileVariant) GetHeightOk() (*int32, bool)`

GetHeightOk returns a tuple with the Height field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeight

`func (o *MediaCenterFileVariant) SetHeight(v int32)`

SetHeight sets Height field to given value.

### HasHeight

`func (o *MediaCenterFileVariant) HasHeight() bool`

HasHeight returns a boolean if a field has been set.

### GetUrl

`func (o *MediaCenterFileVariant) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *MediaCenterFileVariant) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *MediaCenterFileVariant) SetUrl(v string)`

SetUrl sets Url field to given value.


### GetValidFrom

`func (o *MediaCenterFileVariant) GetValidFrom() time.Time`

GetValidFrom returns the ValidFrom field if non-nil, zero value otherwise.

### GetValidFromOk

`func (o *MediaCenterFileVariant) GetValidFromOk() (*time.Time, bool)`

GetValidFromOk returns a tuple with the ValidFrom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidFrom

`func (o *MediaCenterFileVariant) SetValidFrom(v time.Time)`

SetValidFrom sets ValidFrom field to given value.

### HasValidFrom

`func (o *MediaCenterFileVariant) HasValidFrom() bool`

HasValidFrom returns a boolean if a field has been set.

### GetValidUntil

`func (o *MediaCenterFileVariant) GetValidUntil() time.Time`

GetValidUntil returns the ValidUntil field if non-nil, zero value otherwise.

### GetValidUntilOk

`func (o *MediaCenterFileVariant) GetValidUntilOk() (*time.Time, bool)`

GetValidUntilOk returns a tuple with the ValidUntil field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidUntil

`func (o *MediaCenterFileVariant) SetValidUntil(v time.Time)`

SetValidUntil sets ValidUntil field to given value.

### HasValidUntil

`func (o *MediaCenterFileVariant) HasValidUntil() bool`

HasValidUntil returns a boolean if a field has been set.

### GetCreatedAt

`func (o *MediaCenterFileVariant) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *MediaCenterFileVariant) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *MediaCenterFileVariant) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetFallbackLanguage

`func (o *MediaCenterFileVariant) GetFallbackLanguage() string`

GetFallbackLanguage returns the FallbackLanguage field if non-nil, zero value otherwise.

### GetFallbackLanguageOk

`func (o *MediaCenterFileVariant) GetFallbackLanguageOk() (*string, bool)`

GetFallbackLanguageOk returns a tuple with the FallbackLanguage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFallbackLanguage

`func (o *MediaCenterFileVariant) SetFallbackLanguage(v string)`

SetFallbackLanguage sets FallbackLanguage field to given value.

### HasFallbackLanguage

`func (o *MediaCenterFileVariant) HasFallbackLanguage() bool`

HasFallbackLanguage returns a boolean if a field has been set.

### GetTranslations

`func (o *MediaCenterFileVariant) GetTranslations() []MediaCenterTranslation`

GetTranslations returns the Translations field if non-nil, zero value otherwise.

### GetTranslationsOk

`func (o *MediaCenterFileVariant) GetTranslationsOk() (*[]MediaCenterTranslation, bool)`

GetTranslationsOk returns a tuple with the Translations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTranslations

`func (o *MediaCenterFileVariant) SetTranslations(v []MediaCenterTranslation)`

SetTranslations sets Translations field to given value.

### HasTranslations

`func (o *MediaCenterFileVariant) HasTranslations() bool`

HasTranslations returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


