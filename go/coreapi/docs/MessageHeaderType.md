# MessageHeaderType

## Enum


* `MESSAGE_HEADER_TEXT` (value: `"text"`)

* `MESSAGE_HEADER_IMAGE` (value: `"image"`)

* `MESSAGE_HEADER_VIDEO` (value: `"video"`)

* `MESSAGE_HEADER_DOCUMENT` (value: `"document"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


