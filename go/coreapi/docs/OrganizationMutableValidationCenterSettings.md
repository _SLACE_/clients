# OrganizationMutableValidationCenterSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InteractionSettings** | Pointer to [**OrganizationMutableValidationCenterSettingsInteractionSettings**](OrganizationMutableValidationCenterSettingsInteractionSettings.md) |  | [optional] 

## Methods

### NewOrganizationMutableValidationCenterSettings

`func NewOrganizationMutableValidationCenterSettings() *OrganizationMutableValidationCenterSettings`

NewOrganizationMutableValidationCenterSettings instantiates a new OrganizationMutableValidationCenterSettings object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrganizationMutableValidationCenterSettingsWithDefaults

`func NewOrganizationMutableValidationCenterSettingsWithDefaults() *OrganizationMutableValidationCenterSettings`

NewOrganizationMutableValidationCenterSettingsWithDefaults instantiates a new OrganizationMutableValidationCenterSettings object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInteractionSettings

`func (o *OrganizationMutableValidationCenterSettings) GetInteractionSettings() OrganizationMutableValidationCenterSettingsInteractionSettings`

GetInteractionSettings returns the InteractionSettings field if non-nil, zero value otherwise.

### GetInteractionSettingsOk

`func (o *OrganizationMutableValidationCenterSettings) GetInteractionSettingsOk() (*OrganizationMutableValidationCenterSettingsInteractionSettings, bool)`

GetInteractionSettingsOk returns a tuple with the InteractionSettings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionSettings

`func (o *OrganizationMutableValidationCenterSettings) SetInteractionSettings(v OrganizationMutableValidationCenterSettingsInteractionSettings)`

SetInteractionSettings sets InteractionSettings field to given value.

### HasInteractionSettings

`func (o *OrganizationMutableValidationCenterSettings) HasInteractionSettings() bool`

HasInteractionSettings returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


