# OrganizationMutableValidationCenterSettingsInteractionSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ValidateScripts** | Pointer to **bool** |  | [optional] 
**ValidateActions** | Pointer to **bool** |  | [optional] 
**ValidateActivityActions** | Pointer to **bool** |  | [optional] 
**ValidateBranchesActions** | Pointer to **bool** |  | [optional] 
**ValidateEntrypointActions** | Pointer to **bool** |  | [optional] 
**ValidateMessageLinkActions** | Pointer to **bool** |  | [optional] 
**ValidateSignatureActions** | Pointer to **bool** |  | [optional] 
**ValidateVloopActions** | Pointer to **bool** |  | [optional] 
**ValidatePartnerActions** | Pointer to **bool** |  | [optional] 
**ValidateTransactionParams** | Pointer to **bool** |  | [optional] 
**ValidateTriggers** | Pointer to **bool** |  | [optional] 
**ValidateEvents** | Pointer to **bool** |  | [optional] 
**ValidateMetrics** | Pointer to **bool** |  | [optional] 
**ValidateReports** | Pointer to **bool** |  | [optional] 

## Methods

### NewOrganizationMutableValidationCenterSettingsInteractionSettings

`func NewOrganizationMutableValidationCenterSettingsInteractionSettings() *OrganizationMutableValidationCenterSettingsInteractionSettings`

NewOrganizationMutableValidationCenterSettingsInteractionSettings instantiates a new OrganizationMutableValidationCenterSettingsInteractionSettings object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrganizationMutableValidationCenterSettingsInteractionSettingsWithDefaults

`func NewOrganizationMutableValidationCenterSettingsInteractionSettingsWithDefaults() *OrganizationMutableValidationCenterSettingsInteractionSettings`

NewOrganizationMutableValidationCenterSettingsInteractionSettingsWithDefaults instantiates a new OrganizationMutableValidationCenterSettingsInteractionSettings object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetValidateScripts

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateScripts() bool`

GetValidateScripts returns the ValidateScripts field if non-nil, zero value otherwise.

### GetValidateScriptsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateScriptsOk() (*bool, bool)`

GetValidateScriptsOk returns a tuple with the ValidateScripts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateScripts

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateScripts(v bool)`

SetValidateScripts sets ValidateScripts field to given value.

### HasValidateScripts

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateScripts() bool`

HasValidateScripts returns a boolean if a field has been set.

### GetValidateActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateActions() bool`

GetValidateActions returns the ValidateActions field if non-nil, zero value otherwise.

### GetValidateActionsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateActionsOk() (*bool, bool)`

GetValidateActionsOk returns a tuple with the ValidateActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateActions(v bool)`

SetValidateActions sets ValidateActions field to given value.

### HasValidateActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateActions() bool`

HasValidateActions returns a boolean if a field has been set.

### GetValidateActivityActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateActivityActions() bool`

GetValidateActivityActions returns the ValidateActivityActions field if non-nil, zero value otherwise.

### GetValidateActivityActionsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateActivityActionsOk() (*bool, bool)`

GetValidateActivityActionsOk returns a tuple with the ValidateActivityActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateActivityActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateActivityActions(v bool)`

SetValidateActivityActions sets ValidateActivityActions field to given value.

### HasValidateActivityActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateActivityActions() bool`

HasValidateActivityActions returns a boolean if a field has been set.

### GetValidateBranchesActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateBranchesActions() bool`

GetValidateBranchesActions returns the ValidateBranchesActions field if non-nil, zero value otherwise.

### GetValidateBranchesActionsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateBranchesActionsOk() (*bool, bool)`

GetValidateBranchesActionsOk returns a tuple with the ValidateBranchesActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateBranchesActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateBranchesActions(v bool)`

SetValidateBranchesActions sets ValidateBranchesActions field to given value.

### HasValidateBranchesActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateBranchesActions() bool`

HasValidateBranchesActions returns a boolean if a field has been set.

### GetValidateEntrypointActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateEntrypointActions() bool`

GetValidateEntrypointActions returns the ValidateEntrypointActions field if non-nil, zero value otherwise.

### GetValidateEntrypointActionsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateEntrypointActionsOk() (*bool, bool)`

GetValidateEntrypointActionsOk returns a tuple with the ValidateEntrypointActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateEntrypointActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateEntrypointActions(v bool)`

SetValidateEntrypointActions sets ValidateEntrypointActions field to given value.

### HasValidateEntrypointActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateEntrypointActions() bool`

HasValidateEntrypointActions returns a boolean if a field has been set.

### GetValidateMessageLinkActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateMessageLinkActions() bool`

GetValidateMessageLinkActions returns the ValidateMessageLinkActions field if non-nil, zero value otherwise.

### GetValidateMessageLinkActionsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateMessageLinkActionsOk() (*bool, bool)`

GetValidateMessageLinkActionsOk returns a tuple with the ValidateMessageLinkActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateMessageLinkActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateMessageLinkActions(v bool)`

SetValidateMessageLinkActions sets ValidateMessageLinkActions field to given value.

### HasValidateMessageLinkActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateMessageLinkActions() bool`

HasValidateMessageLinkActions returns a boolean if a field has been set.

### GetValidateSignatureActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateSignatureActions() bool`

GetValidateSignatureActions returns the ValidateSignatureActions field if non-nil, zero value otherwise.

### GetValidateSignatureActionsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateSignatureActionsOk() (*bool, bool)`

GetValidateSignatureActionsOk returns a tuple with the ValidateSignatureActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateSignatureActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateSignatureActions(v bool)`

SetValidateSignatureActions sets ValidateSignatureActions field to given value.

### HasValidateSignatureActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateSignatureActions() bool`

HasValidateSignatureActions returns a boolean if a field has been set.

### GetValidateVloopActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateVloopActions() bool`

GetValidateVloopActions returns the ValidateVloopActions field if non-nil, zero value otherwise.

### GetValidateVloopActionsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateVloopActionsOk() (*bool, bool)`

GetValidateVloopActionsOk returns a tuple with the ValidateVloopActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateVloopActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateVloopActions(v bool)`

SetValidateVloopActions sets ValidateVloopActions field to given value.

### HasValidateVloopActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateVloopActions() bool`

HasValidateVloopActions returns a boolean if a field has been set.

### GetValidatePartnerActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidatePartnerActions() bool`

GetValidatePartnerActions returns the ValidatePartnerActions field if non-nil, zero value otherwise.

### GetValidatePartnerActionsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidatePartnerActionsOk() (*bool, bool)`

GetValidatePartnerActionsOk returns a tuple with the ValidatePartnerActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidatePartnerActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidatePartnerActions(v bool)`

SetValidatePartnerActions sets ValidatePartnerActions field to given value.

### HasValidatePartnerActions

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidatePartnerActions() bool`

HasValidatePartnerActions returns a boolean if a field has been set.

### GetValidateTransactionParams

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateTransactionParams() bool`

GetValidateTransactionParams returns the ValidateTransactionParams field if non-nil, zero value otherwise.

### GetValidateTransactionParamsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateTransactionParamsOk() (*bool, bool)`

GetValidateTransactionParamsOk returns a tuple with the ValidateTransactionParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateTransactionParams

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateTransactionParams(v bool)`

SetValidateTransactionParams sets ValidateTransactionParams field to given value.

### HasValidateTransactionParams

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateTransactionParams() bool`

HasValidateTransactionParams returns a boolean if a field has been set.

### GetValidateTriggers

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateTriggers() bool`

GetValidateTriggers returns the ValidateTriggers field if non-nil, zero value otherwise.

### GetValidateTriggersOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateTriggersOk() (*bool, bool)`

GetValidateTriggersOk returns a tuple with the ValidateTriggers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateTriggers

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateTriggers(v bool)`

SetValidateTriggers sets ValidateTriggers field to given value.

### HasValidateTriggers

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateTriggers() bool`

HasValidateTriggers returns a boolean if a field has been set.

### GetValidateEvents

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateEvents() bool`

GetValidateEvents returns the ValidateEvents field if non-nil, zero value otherwise.

### GetValidateEventsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateEventsOk() (*bool, bool)`

GetValidateEventsOk returns a tuple with the ValidateEvents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateEvents

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateEvents(v bool)`

SetValidateEvents sets ValidateEvents field to given value.

### HasValidateEvents

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateEvents() bool`

HasValidateEvents returns a boolean if a field has been set.

### GetValidateMetrics

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateMetrics() bool`

GetValidateMetrics returns the ValidateMetrics field if non-nil, zero value otherwise.

### GetValidateMetricsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateMetricsOk() (*bool, bool)`

GetValidateMetricsOk returns a tuple with the ValidateMetrics field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateMetrics

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateMetrics(v bool)`

SetValidateMetrics sets ValidateMetrics field to given value.

### HasValidateMetrics

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateMetrics() bool`

HasValidateMetrics returns a boolean if a field has been set.

### GetValidateReports

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateReports() bool`

GetValidateReports returns the ValidateReports field if non-nil, zero value otherwise.

### GetValidateReportsOk

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) GetValidateReportsOk() (*bool, bool)`

GetValidateReportsOk returns a tuple with the ValidateReports field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidateReports

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) SetValidateReports(v bool)`

SetValidateReports sets ValidateReports field to given value.

### HasValidateReports

`func (o *OrganizationMutableValidationCenterSettingsInteractionSettings) HasValidateReports() bool`

HasValidateReports returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


