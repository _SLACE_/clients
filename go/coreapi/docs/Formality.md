# Formality

## Enum


* `FormalityPreferLess` (value: `"prefer_less"`)

* `FormalityPreferMore` (value: `"prefer_more"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


