# WhatsAppTemplateDefaultsButton

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Index** | **int32** |  | 
**TextParams** | **[]string** |  | 
**Ttl** | Pointer to **int32** |  | [optional] 
**ExpiredUrl** | Pointer to **string** |  | [optional] 

## Methods

### NewWhatsAppTemplateDefaultsButton

`func NewWhatsAppTemplateDefaultsButton(index int32, textParams []string, ) *WhatsAppTemplateDefaultsButton`

NewWhatsAppTemplateDefaultsButton instantiates a new WhatsAppTemplateDefaultsButton object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateDefaultsButtonWithDefaults

`func NewWhatsAppTemplateDefaultsButtonWithDefaults() *WhatsAppTemplateDefaultsButton`

NewWhatsAppTemplateDefaultsButtonWithDefaults instantiates a new WhatsAppTemplateDefaultsButton object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIndex

`func (o *WhatsAppTemplateDefaultsButton) GetIndex() int32`

GetIndex returns the Index field if non-nil, zero value otherwise.

### GetIndexOk

`func (o *WhatsAppTemplateDefaultsButton) GetIndexOk() (*int32, bool)`

GetIndexOk returns a tuple with the Index field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIndex

`func (o *WhatsAppTemplateDefaultsButton) SetIndex(v int32)`

SetIndex sets Index field to given value.


### GetTextParams

`func (o *WhatsAppTemplateDefaultsButton) GetTextParams() []string`

GetTextParams returns the TextParams field if non-nil, zero value otherwise.

### GetTextParamsOk

`func (o *WhatsAppTemplateDefaultsButton) GetTextParamsOk() (*[]string, bool)`

GetTextParamsOk returns a tuple with the TextParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTextParams

`func (o *WhatsAppTemplateDefaultsButton) SetTextParams(v []string)`

SetTextParams sets TextParams field to given value.


### GetTtl

`func (o *WhatsAppTemplateDefaultsButton) GetTtl() int32`

GetTtl returns the Ttl field if non-nil, zero value otherwise.

### GetTtlOk

`func (o *WhatsAppTemplateDefaultsButton) GetTtlOk() (*int32, bool)`

GetTtlOk returns a tuple with the Ttl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTtl

`func (o *WhatsAppTemplateDefaultsButton) SetTtl(v int32)`

SetTtl sets Ttl field to given value.

### HasTtl

`func (o *WhatsAppTemplateDefaultsButton) HasTtl() bool`

HasTtl returns a boolean if a field has been set.

### GetExpiredUrl

`func (o *WhatsAppTemplateDefaultsButton) GetExpiredUrl() string`

GetExpiredUrl returns the ExpiredUrl field if non-nil, zero value otherwise.

### GetExpiredUrlOk

`func (o *WhatsAppTemplateDefaultsButton) GetExpiredUrlOk() (*string, bool)`

GetExpiredUrlOk returns a tuple with the ExpiredUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiredUrl

`func (o *WhatsAppTemplateDefaultsButton) SetExpiredUrl(v string)`

SetExpiredUrl sets ExpiredUrl field to given value.

### HasExpiredUrl

`func (o *WhatsAppTemplateDefaultsButton) HasExpiredUrl() bool`

HasExpiredUrl returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


