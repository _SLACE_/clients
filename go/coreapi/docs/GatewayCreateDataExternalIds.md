# GatewayCreateDataExternalIds

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Var360ExternalId** | Pointer to **string** |  | [optional] 
**Var360ClientId** | Pointer to **string** |  | [optional] 
**WaPhoneNumberId** | Pointer to **string** |  | [optional] 
**WaWabaId** | Pointer to **string** |  | [optional] 

## Methods

### NewGatewayCreateDataExternalIds

`func NewGatewayCreateDataExternalIds() *GatewayCreateDataExternalIds`

NewGatewayCreateDataExternalIds instantiates a new GatewayCreateDataExternalIds object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGatewayCreateDataExternalIdsWithDefaults

`func NewGatewayCreateDataExternalIdsWithDefaults() *GatewayCreateDataExternalIds`

NewGatewayCreateDataExternalIdsWithDefaults instantiates a new GatewayCreateDataExternalIds object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVar360ExternalId

`func (o *GatewayCreateDataExternalIds) GetVar360ExternalId() string`

GetVar360ExternalId returns the Var360ExternalId field if non-nil, zero value otherwise.

### GetVar360ExternalIdOk

`func (o *GatewayCreateDataExternalIds) GetVar360ExternalIdOk() (*string, bool)`

GetVar360ExternalIdOk returns a tuple with the Var360ExternalId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVar360ExternalId

`func (o *GatewayCreateDataExternalIds) SetVar360ExternalId(v string)`

SetVar360ExternalId sets Var360ExternalId field to given value.

### HasVar360ExternalId

`func (o *GatewayCreateDataExternalIds) HasVar360ExternalId() bool`

HasVar360ExternalId returns a boolean if a field has been set.

### GetVar360ClientId

`func (o *GatewayCreateDataExternalIds) GetVar360ClientId() string`

GetVar360ClientId returns the Var360ClientId field if non-nil, zero value otherwise.

### GetVar360ClientIdOk

`func (o *GatewayCreateDataExternalIds) GetVar360ClientIdOk() (*string, bool)`

GetVar360ClientIdOk returns a tuple with the Var360ClientId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVar360ClientId

`func (o *GatewayCreateDataExternalIds) SetVar360ClientId(v string)`

SetVar360ClientId sets Var360ClientId field to given value.

### HasVar360ClientId

`func (o *GatewayCreateDataExternalIds) HasVar360ClientId() bool`

HasVar360ClientId returns a boolean if a field has been set.

### GetWaPhoneNumberId

`func (o *GatewayCreateDataExternalIds) GetWaPhoneNumberId() string`

GetWaPhoneNumberId returns the WaPhoneNumberId field if non-nil, zero value otherwise.

### GetWaPhoneNumberIdOk

`func (o *GatewayCreateDataExternalIds) GetWaPhoneNumberIdOk() (*string, bool)`

GetWaPhoneNumberIdOk returns a tuple with the WaPhoneNumberId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWaPhoneNumberId

`func (o *GatewayCreateDataExternalIds) SetWaPhoneNumberId(v string)`

SetWaPhoneNumberId sets WaPhoneNumberId field to given value.

### HasWaPhoneNumberId

`func (o *GatewayCreateDataExternalIds) HasWaPhoneNumberId() bool`

HasWaPhoneNumberId returns a boolean if a field has been set.

### GetWaWabaId

`func (o *GatewayCreateDataExternalIds) GetWaWabaId() string`

GetWaWabaId returns the WaWabaId field if non-nil, zero value otherwise.

### GetWaWabaIdOk

`func (o *GatewayCreateDataExternalIds) GetWaWabaIdOk() (*string, bool)`

GetWaWabaIdOk returns a tuple with the WaWabaId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWaWabaId

`func (o *GatewayCreateDataExternalIds) SetWaWabaId(v string)`

SetWaWabaId sets WaWabaId field to given value.

### HasWaWabaId

`func (o *GatewayCreateDataExternalIds) HasWaWabaId() bool`

HasWaWabaId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


