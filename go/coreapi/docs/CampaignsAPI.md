# \CampaignsAPI

All URIs are relative to *https://api.slace.com/core*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ArchiveCampaign**](CampaignsAPI.md#ArchiveCampaign) | **Put** /campaigns/{id}/archive | Archive campaign
[**CampaignMetrics**](CampaignsAPI.md#CampaignMetrics) | **Get** /campaigns/{id}/metrics | Campaign Metrics
[**CampaignsSettings**](CampaignsAPI.md#CampaignsSettings) | **Get** /campaigns-settngs | Campaigns settings
[**CheckProgress**](CampaignsAPI.md#CheckProgress) | **Get** /campaigns/{id}/progress | Check progress of running campaign
[**CloneCampaign**](CampaignsAPI.md#CloneCampaign) | **Post** /campaigns/{id}/clone | Clone campaign
[**CreateCampaign**](CampaignsAPI.md#CreateCampaign) | **Post** /campaigns | Create campaign
[**CreateSendout**](CampaignsAPI.md#CreateSendout) | **Post** /campaigns/{id}/sendouts | Create sendout
[**DeleteCampaign**](CampaignsAPI.md#DeleteCampaign) | **Delete** /campaigns/{id} | Delete campaign
[**EstimatedContactsCount**](CampaignsAPI.md#EstimatedContactsCount) | **Get** /campaigns/{id}/estimation | Estimated contacts
[**ExecuteForGroup**](CampaignsAPI.md#ExecuteForGroup) | **Put** /campaigns/{id}/execute-group | Send campaign to contacts group
[**GetCampaign**](CampaignsAPI.md#GetCampaign) | **Get** /campaigns/{id} | Get campaign
[**GetCampaignExecs**](CampaignsAPI.md#GetCampaignExecs) | **Get** /campaigns/{id}/executions | Get campaign executions
[**GetCampaignSendoutStarts**](CampaignsAPI.md#GetCampaignSendoutStarts) | **Get** /campaigns/{id}/sendouts/{sid}/starts | Get campaign sendout starts
[**ListCampaigns**](CampaignsAPI.md#ListCampaigns) | **Get** /campaigns | List campaigns
[**ListUpcomingSendouts**](CampaignsAPI.md#ListUpcomingSendouts) | **Get** /campaigns/{id}/upcoming-sendouts | List upcoming sendouts
[**PatchCampaign**](CampaignsAPI.md#PatchCampaign) | **Patch** /campaigns/{id} | Patch campaign
[**PauseCampaign**](CampaignsAPI.md#PauseCampaign) | **Put** /campaigns/{id}/pause | Pause campaign
[**PauseSendout**](CampaignsAPI.md#PauseSendout) | **Put** /campaigns/{id}/sendouts/{sid}/pause | Pause sendout
[**RescheduleSendout**](CampaignsAPI.md#RescheduleSendout) | **Put** /campaigns/{id}/sendouts/{sid}/reschedule | Reschedule sendout
[**ResumeSendout**](CampaignsAPI.md#ResumeSendout) | **Put** /campaigns/{id}/sendouts/{sid}/resume | Resume sendout
[**ScheduleCampaign**](CampaignsAPI.md#ScheduleCampaign) | **Put** /campaigns/{id}/schedule | Schedule campaign
[**SkipSendout**](CampaignsAPI.md#SkipSendout) | **Put** /campaigns/{id}/sendouts/{sid}/skip | Skip sendout
[**StopCampaign**](CampaignsAPI.md#StopCampaign) | **Put** /campaigns/{id}/stop | Stop campaign (kill switch)
[**TriggerTestRun**](CampaignsAPI.md#TriggerTestRun) | **Put** /campaigns/{id}/test | Trigger test run
[**UpdateCampaign**](CampaignsAPI.md#UpdateCampaign) | **Put** /campaigns/{id} | Update campaign



## ArchiveCampaign

> Campaign ArchiveCampaign(ctx, id).Execute()

Archive campaign

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.ArchiveCampaign(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.ArchiveCampaign``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ArchiveCampaign`: Campaign
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.ArchiveCampaign`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiArchiveCampaignRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Campaign**](Campaign.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CampaignMetrics

> CampaignMetrics CampaignMetrics(ctx, id).Execute()

Campaign Metrics

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.CampaignMetrics(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.CampaignMetrics``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CampaignMetrics`: CampaignMetrics
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.CampaignMetrics`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCampaignMetricsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**CampaignMetrics**](CampaignMetrics.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CampaignsSettings

> CampaignsSettings200Response CampaignsSettings(ctx).Execute()

Campaigns settings



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.CampaignsSettings(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.CampaignsSettings``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CampaignsSettings`: CampaignsSettings200Response
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.CampaignsSettings`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiCampaignsSettingsRequest struct via the builder pattern


### Return type

[**CampaignsSettings200Response**](CampaignsSettings200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CheckProgress

> CheckProgress200Response CheckProgress(ctx, id).Execute()

Check progress of running campaign

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.CheckProgress(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.CheckProgress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CheckProgress`: CheckProgress200Response
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.CheckProgress`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCheckProgressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**CheckProgress200Response**](CheckProgress200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CloneCampaign

> Campaign CloneCampaign(ctx, id).CampaignClone(campaignClone).Execute()

Clone campaign

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    campaignClone := *openapiclient.NewCampaignClone([]openapiclient.CampaignCloneTargetChannelsInner{*openapiclient.NewCampaignCloneTargetChannelsInner("ChannelId_example", "InteractionId_example")}, "OrganizationId_example") // CampaignClone |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.CloneCampaign(context.Background(), id).CampaignClone(campaignClone).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.CloneCampaign``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CloneCampaign`: Campaign
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.CloneCampaign`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCloneCampaignRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **campaignClone** | [**CampaignClone**](CampaignClone.md) |  | 

### Return type

[**Campaign**](Campaign.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateCampaign

> Campaign CreateCampaign(ctx).Campaign(campaign).Execute()

Create campaign



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    campaign := *openapiclient.NewCampaign("Id_example", "Type_example", "Name_example", "Status_example", "OrganizationId_example", time.Now(), time.Now(), time.Now(), int32(123), false, false, []openapiclient.CampaignTargetChannelsInner{*openapiclient.NewCampaignTargetChannelsInner("ChannelId_example", "InteractionId_example")}, false, "TargetType_example") // Campaign |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.CreateCampaign(context.Background()).Campaign(campaign).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.CreateCampaign``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateCampaign`: Campaign
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.CreateCampaign`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateCampaignRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **campaign** | [**Campaign**](Campaign.md) |  | 

### Return type

[**Campaign**](Campaign.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateSendout

> CampaignSendout CreateSendout(ctx, id).CampaignSendout(campaignSendout).Execute()

Create sendout

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    campaignSendout := *openapiclient.NewCampaignSendout("Id_example", "CampaignId_example", time.Now(), "Status_example", time.Now(), time.Now()) // CampaignSendout |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.CreateSendout(context.Background(), id).CampaignSendout(campaignSendout).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.CreateSendout``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateSendout`: CampaignSendout
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.CreateSendout`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateSendoutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **campaignSendout** | [**CampaignSendout**](CampaignSendout.md) |  | 

### Return type

[**CampaignSendout**](CampaignSendout.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteCampaign

> DeleteCampaign(ctx, id).Execute()

Delete campaign

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.CampaignsAPI.DeleteCampaign(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.DeleteCampaign``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteCampaignRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## EstimatedContactsCount

> EstimationResult EstimatedContactsCount(ctx, id).Limit(limit).Offset(offset).Execute()

Estimated contacts



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    limit := int32(56) // int32 |  (optional)
    offset := "offset_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.EstimatedContactsCount(context.Background(), id).Limit(limit).Offset(offset).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.EstimatedContactsCount``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `EstimatedContactsCount`: EstimationResult
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.EstimatedContactsCount`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiEstimatedContactsCountRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **limit** | **int32** |  | 
 **offset** | **string** |  | 

### Return type

[**EstimationResult**](EstimationResult.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ExecuteForGroup

> ExecuteForGroup(ctx, id).ExecuteForGroupRequest(executeForGroupRequest).Execute()

Send campaign to contacts group

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    executeForGroupRequest := *openapiclient.NewExecuteForGroupRequest() // ExecuteForGroupRequest | Body allows also for standard contacts filter (as in  [GET /contacts](https://docs.slace.com/docs/stoplight-docs-core/3737bc07d1d9b-list-contacts)). Here it should b sent in body instead of query. (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.CampaignsAPI.ExecuteForGroup(context.Background(), id).ExecuteForGroupRequest(executeForGroupRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.ExecuteForGroup``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiExecuteForGroupRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **executeForGroupRequest** | [**ExecuteForGroupRequest**](ExecuteForGroupRequest.md) | Body allows also for standard contacts filter (as in  [GET /contacts](https://docs.slace.com/docs/stoplight-docs-core/3737bc07d1d9b-list-contacts)). Here it should b sent in body instead of query. | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCampaign

> Campaign GetCampaign(ctx, id).Execute()

Get campaign

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.GetCampaign(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.GetCampaign``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCampaign`: Campaign
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.GetCampaign`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetCampaignRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Campaign**](Campaign.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCampaignExecs

> []CampaignExecution GetCampaignExecs(ctx, id).Status(status).Limit(limit).Offset(offset).Sort(sort).Execute()

Get campaign executions

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    status := "status_example" // string |  (optional)
    limit := int32(56) // int32 |  (optional)
    offset := int32(56) // int32 |  (optional)
    sort := "sort_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.GetCampaignExecs(context.Background(), id).Status(status).Limit(limit).Offset(offset).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.GetCampaignExecs``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCampaignExecs`: []CampaignExecution
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.GetCampaignExecs`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetCampaignExecsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **status** | **string** |  | 
 **limit** | **int32** |  | 
 **offset** | **int32** |  | 
 **sort** | **string** |  | 

### Return type

[**[]CampaignExecution**](CampaignExecution.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCampaignSendoutStarts

> GetCampaignSendoutStarts200Response GetCampaignSendoutStarts(ctx, id, sid).Execute()

Get campaign sendout starts

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    sid := "sid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.GetCampaignSendoutStarts(context.Background(), id, sid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.GetCampaignSendoutStarts``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCampaignSendoutStarts`: GetCampaignSendoutStarts200Response
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.GetCampaignSendoutStarts`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 
**sid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetCampaignSendoutStartsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**GetCampaignSendoutStarts200Response**](GetCampaignSendoutStarts200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListCampaigns

> CampaignsList ListCampaigns(ctx).Search(search).Name(name).Sort(sort).OrganizationId(organizationId).ChannelId(channelId).ScriptId(scriptId).InteractionId(interactionId).Status(status).Type_(type_).Offset(offset).Limit(limit).GroupId(groupId).SubType(subType).Execute()

List campaigns



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    search := "search_example" // string |  (optional)
    name := "name_example" // string |  (optional)
    sort := "sort_example" // string |  (optional)
    organizationId := "organizationId_example" // string |  (optional)
    channelId := "channelId_example" // string |  (optional)
    scriptId := "scriptId_example" // string |  (optional)
    interactionId := "interactionId_example" // string |  (optional)
    status := "status_example" // string |  (optional)
    type_ := "type__example" // string |  (optional)
    offset := int32(56) // int32 |  (optional)
    limit := int32(56) // int32 |  (optional)
    groupId := "groupId_example" // string |  (optional)
    subType := "subType_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.ListCampaigns(context.Background()).Search(search).Name(name).Sort(sort).OrganizationId(organizationId).ChannelId(channelId).ScriptId(scriptId).InteractionId(interactionId).Status(status).Type_(type_).Offset(offset).Limit(limit).GroupId(groupId).SubType(subType).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.ListCampaigns``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListCampaigns`: CampaignsList
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.ListCampaigns`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListCampaignsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **string** |  | 
 **name** | **string** |  | 
 **sort** | **string** |  | 
 **organizationId** | **string** |  | 
 **channelId** | **string** |  | 
 **scriptId** | **string** |  | 
 **interactionId** | **string** |  | 
 **status** | **string** |  | 
 **type_** | **string** |  | 
 **offset** | **int32** |  | 
 **limit** | **int32** |  | 
 **groupId** | **string** |  | 
 **subType** | **string** |  | 

### Return type

[**CampaignsList**](CampaignsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListUpcomingSendouts

> []CampaignSendout ListUpcomingSendouts(ctx, id).Until(until).From(from).Execute()

List upcoming sendouts



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    until := time.Now() // time.Time |  (optional)
    from := time.Now() // time.Time |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.ListUpcomingSendouts(context.Background(), id).Until(until).From(from).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.ListUpcomingSendouts``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListUpcomingSendouts`: []CampaignSendout
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.ListUpcomingSendouts`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListUpcomingSendoutsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **until** | **time.Time** |  | 
 **from** | **time.Time** |  | 

### Return type

[**[]CampaignSendout**](CampaignSendout.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PatchCampaign

> Campaign PatchCampaign(ctx, id).CampaignPatchable(campaignPatchable).Execute()

Patch campaign

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    campaignPatchable := *openapiclient.NewCampaignPatchable() // CampaignPatchable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.PatchCampaign(context.Background(), id).CampaignPatchable(campaignPatchable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.PatchCampaign``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PatchCampaign`: Campaign
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.PatchCampaign`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPatchCampaignRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **campaignPatchable** | [**CampaignPatchable**](CampaignPatchable.md) |  | 

### Return type

[**Campaign**](Campaign.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PauseCampaign

> Campaign PauseCampaign(ctx, id).Execute()

Pause campaign

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.PauseCampaign(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.PauseCampaign``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PauseCampaign`: Campaign
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.PauseCampaign`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPauseCampaignRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Campaign**](Campaign.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PauseSendout

> CampaignSendout PauseSendout(ctx, id, sid).Execute()

Pause sendout

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    sid := "sid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.PauseSendout(context.Background(), id, sid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.PauseSendout``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PauseSendout`: CampaignSendout
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.PauseSendout`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 
**sid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPauseSendoutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**CampaignSendout**](CampaignSendout.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RescheduleSendout

> CampaignSendout RescheduleSendout(ctx, id, sid).CampaignSendoutRescheduleData(campaignSendoutRescheduleData).Execute()

Reschedule sendout

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    sid := "sid_example" // string | 
    campaignSendoutRescheduleData := *openapiclient.NewCampaignSendoutRescheduleData(time.Now()) // CampaignSendoutRescheduleData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.RescheduleSendout(context.Background(), id, sid).CampaignSendoutRescheduleData(campaignSendoutRescheduleData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.RescheduleSendout``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RescheduleSendout`: CampaignSendout
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.RescheduleSendout`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 
**sid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiRescheduleSendoutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **campaignSendoutRescheduleData** | [**CampaignSendoutRescheduleData**](CampaignSendoutRescheduleData.md) |  | 

### Return type

[**CampaignSendout**](CampaignSendout.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ResumeSendout

> CampaignSendout ResumeSendout(ctx, id, sid).Execute()

Resume sendout

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    sid := "sid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.ResumeSendout(context.Background(), id, sid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.ResumeSendout``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ResumeSendout`: CampaignSendout
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.ResumeSendout`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 
**sid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiResumeSendoutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**CampaignSendout**](CampaignSendout.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ScheduleCampaign

> Campaign ScheduleCampaign(ctx, id).Execute()

Schedule campaign



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.ScheduleCampaign(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.ScheduleCampaign``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ScheduleCampaign`: Campaign
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.ScheduleCampaign`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiScheduleCampaignRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Campaign**](Campaign.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SkipSendout

> CampaignSendout SkipSendout(ctx, id, sid).Execute()

Skip sendout

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    sid := "sid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.SkipSendout(context.Background(), id, sid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.SkipSendout``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SkipSendout`: CampaignSendout
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.SkipSendout`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 
**sid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiSkipSendoutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**CampaignSendout**](CampaignSendout.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StopCampaign

> Campaign StopCampaign(ctx, id).Execute()

Stop campaign (kill switch)

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.StopCampaign(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.StopCampaign``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StopCampaign`: Campaign
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.StopCampaign`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStopCampaignRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Campaign**](Campaign.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## TriggerTestRun

> TriggerTestRun(ctx, id).TriggerTestRunRequest(triggerTestRunRequest).Execute()

Trigger test run

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    triggerTestRunRequest := *openapiclient.NewTriggerTestRunRequest("ContactId_example") // TriggerTestRunRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.CampaignsAPI.TriggerTestRun(context.Background(), id).TriggerTestRunRequest(triggerTestRunRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.TriggerTestRun``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiTriggerTestRunRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **triggerTestRunRequest** | [**TriggerTestRunRequest**](TriggerTestRunRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateCampaign

> Campaign UpdateCampaign(ctx, id).Campaign(campaign).Execute()

Update campaign

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    campaign := *openapiclient.NewCampaign("Id_example", "Type_example", "Name_example", "Status_example", "OrganizationId_example", time.Now(), time.Now(), time.Now(), int32(123), false, false, []openapiclient.CampaignTargetChannelsInner{*openapiclient.NewCampaignTargetChannelsInner("ChannelId_example", "InteractionId_example")}, false, "TargetType_example") // Campaign |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CampaignsAPI.UpdateCampaign(context.Background(), id).Campaign(campaign).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CampaignsAPI.UpdateCampaign``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateCampaign`: Campaign
    fmt.Fprintf(os.Stdout, "Response from `CampaignsAPI.UpdateCampaign`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateCampaignRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **campaign** | [**Campaign**](Campaign.md) |  | 

### Return type

[**Campaign**](Campaign.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

