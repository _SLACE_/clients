# ChannelTagsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 

## Methods

### NewChannelTagsInner

`func NewChannelTagsInner() *ChannelTagsInner`

NewChannelTagsInner instantiates a new ChannelTagsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChannelTagsInnerWithDefaults

`func NewChannelTagsInnerWithDefaults() *ChannelTagsInner`

NewChannelTagsInnerWithDefaults instantiates a new ChannelTagsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ChannelTagsInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ChannelTagsInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ChannelTagsInner) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *ChannelTagsInner) HasId() bool`

HasId returns a boolean if a field has been set.

### GetName

`func (o *ChannelTagsInner) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ChannelTagsInner) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ChannelTagsInner) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ChannelTagsInner) HasName() bool`

HasName returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


