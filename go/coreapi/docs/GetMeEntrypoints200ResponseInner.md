# GetMeEntrypoints200ResponseInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**UserId** | Pointer to **string** |  | [optional] 
**EntrypointId** | Pointer to **string** |  | [optional] 

## Methods

### NewGetMeEntrypoints200ResponseInner

`func NewGetMeEntrypoints200ResponseInner() *GetMeEntrypoints200ResponseInner`

NewGetMeEntrypoints200ResponseInner instantiates a new GetMeEntrypoints200ResponseInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetMeEntrypoints200ResponseInnerWithDefaults

`func NewGetMeEntrypoints200ResponseInnerWithDefaults() *GetMeEntrypoints200ResponseInner`

NewGetMeEntrypoints200ResponseInnerWithDefaults instantiates a new GetMeEntrypoints200ResponseInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *GetMeEntrypoints200ResponseInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *GetMeEntrypoints200ResponseInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *GetMeEntrypoints200ResponseInner) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *GetMeEntrypoints200ResponseInner) HasId() bool`

HasId returns a boolean if a field has been set.

### GetUserId

`func (o *GetMeEntrypoints200ResponseInner) GetUserId() string`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *GetMeEntrypoints200ResponseInner) GetUserIdOk() (*string, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *GetMeEntrypoints200ResponseInner) SetUserId(v string)`

SetUserId sets UserId field to given value.

### HasUserId

`func (o *GetMeEntrypoints200ResponseInner) HasUserId() bool`

HasUserId returns a boolean if a field has been set.

### GetEntrypointId

`func (o *GetMeEntrypoints200ResponseInner) GetEntrypointId() string`

GetEntrypointId returns the EntrypointId field if non-nil, zero value otherwise.

### GetEntrypointIdOk

`func (o *GetMeEntrypoints200ResponseInner) GetEntrypointIdOk() (*string, bool)`

GetEntrypointIdOk returns a tuple with the EntrypointId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEntrypointId

`func (o *GetMeEntrypoints200ResponseInner) SetEntrypointId(v string)`

SetEntrypointId sets EntrypointId field to given value.

### HasEntrypointId

`func (o *GetMeEntrypoints200ResponseInner) HasEntrypointId() bool`

HasEntrypointId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


