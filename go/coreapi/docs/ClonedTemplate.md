# ClonedTemplate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Name** | **string** |  | 
**SourceId** | **string** | Id of template from which it was created | 
**GatewayId** | **string** |  | 
**ChannelId** | **string** |  | 
**SourceGatewayId** | **string** |  | 

## Methods

### NewClonedTemplate

`func NewClonedTemplate(id string, name string, sourceId string, gatewayId string, channelId string, sourceGatewayId string, ) *ClonedTemplate`

NewClonedTemplate instantiates a new ClonedTemplate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewClonedTemplateWithDefaults

`func NewClonedTemplateWithDefaults() *ClonedTemplate`

NewClonedTemplateWithDefaults instantiates a new ClonedTemplate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ClonedTemplate) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ClonedTemplate) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ClonedTemplate) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *ClonedTemplate) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ClonedTemplate) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ClonedTemplate) SetName(v string)`

SetName sets Name field to given value.


### GetSourceId

`func (o *ClonedTemplate) GetSourceId() string`

GetSourceId returns the SourceId field if non-nil, zero value otherwise.

### GetSourceIdOk

`func (o *ClonedTemplate) GetSourceIdOk() (*string, bool)`

GetSourceIdOk returns a tuple with the SourceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceId

`func (o *ClonedTemplate) SetSourceId(v string)`

SetSourceId sets SourceId field to given value.


### GetGatewayId

`func (o *ClonedTemplate) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *ClonedTemplate) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *ClonedTemplate) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.


### GetChannelId

`func (o *ClonedTemplate) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *ClonedTemplate) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *ClonedTemplate) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetSourceGatewayId

`func (o *ClonedTemplate) GetSourceGatewayId() string`

GetSourceGatewayId returns the SourceGatewayId field if non-nil, zero value otherwise.

### GetSourceGatewayIdOk

`func (o *ClonedTemplate) GetSourceGatewayIdOk() (*string, bool)`

GetSourceGatewayIdOk returns a tuple with the SourceGatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceGatewayId

`func (o *ClonedTemplate) SetSourceGatewayId(v string)`

SetSourceGatewayId sets SourceGatewayId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


