# MessageType

## Enum


* `TEXT` (value: `"text"`)

* `IMAGE` (value: `"image"`)

* `VIDEO` (value: `"video"`)

* `AUDIO` (value: `"audio"`)

* `DOCUMENT` (value: `"document"`)

* `FILE` (value: `"file"`)

* `STICKER` (value: `"sticker"`)

* `LOCATION` (value: `"location"`)

* `CONTACT` (value: `"contact"`)

* `TEMPLATE` (value: `"template"`)

* `BUTTONS` (value: `"buttons"`)

* `LIST` (value: `"list"`)

* `INFO` (value: `"info"`)

* `ORDER_DETAILS` (value: `"order_details"`)

* `ORDER_STATUS` (value: `"order_status"`)

* `STORY` (value: `"story"`)

* `REQUEST_LOCATION` (value: `"request_location"`)

* `REQUEST_PHONE_NUMBER` (value: `"request_phone_number"`)

* `REMOVE_KEYBOARD` (value: `"remove_keyboard"`)

* `REACTION` (value: `"reaction"`)

* `MEDIA_CAROUSEL` (value: `"media_carousel"`)

* `UNKNOWN_MESSAGE` (value: `"unknown_message"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


