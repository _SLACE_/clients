# WebhookEventMessageDataLocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Latitude** | **float32** |  | 
**Longitude** | **float32** |  | 

## Methods

### NewWebhookEventMessageDataLocation

`func NewWebhookEventMessageDataLocation(latitude float32, longitude float32, ) *WebhookEventMessageDataLocation`

NewWebhookEventMessageDataLocation instantiates a new WebhookEventMessageDataLocation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookEventMessageDataLocationWithDefaults

`func NewWebhookEventMessageDataLocationWithDefaults() *WebhookEventMessageDataLocation`

NewWebhookEventMessageDataLocationWithDefaults instantiates a new WebhookEventMessageDataLocation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLatitude

`func (o *WebhookEventMessageDataLocation) GetLatitude() float32`

GetLatitude returns the Latitude field if non-nil, zero value otherwise.

### GetLatitudeOk

`func (o *WebhookEventMessageDataLocation) GetLatitudeOk() (*float32, bool)`

GetLatitudeOk returns a tuple with the Latitude field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLatitude

`func (o *WebhookEventMessageDataLocation) SetLatitude(v float32)`

SetLatitude sets Latitude field to given value.


### GetLongitude

`func (o *WebhookEventMessageDataLocation) GetLongitude() float32`

GetLongitude returns the Longitude field if non-nil, zero value otherwise.

### GetLongitudeOk

`func (o *WebhookEventMessageDataLocation) GetLongitudeOk() (*float32, bool)`

GetLongitudeOk returns a tuple with the Longitude field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLongitude

`func (o *WebhookEventMessageDataLocation) SetLongitude(v float32)`

SetLongitude sets Longitude field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


