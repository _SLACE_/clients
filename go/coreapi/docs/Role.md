# Role

## Enum


* `ROLE_SUPERADMIN` (value: `"superadmin"`)

* `ROLE_CLIENT` (value: `"client"`)

* `ROLE_SERVICE` (value: `"service"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


