# ContactAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Street** | Pointer to **string** |  | [optional] 
**HouseNumber** | Pointer to **string** |  | [optional] 
**DoorNumber** | Pointer to **string** |  | [optional] 
**City** | Pointer to **string** |  | [optional] 
**Zip** | Pointer to **string** |  | [optional] 
**Country** | Pointer to **string** | 2-character ISO code (DE, US) | [optional] 
**CountryName** | Pointer to **string** |  | [optional] 
**CompanyName** | Pointer to **string** |  | [optional] 

## Methods

### NewContactAddress

`func NewContactAddress() *ContactAddress`

NewContactAddress instantiates a new ContactAddress object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactAddressWithDefaults

`func NewContactAddressWithDefaults() *ContactAddress`

NewContactAddressWithDefaults instantiates a new ContactAddress object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStreet

`func (o *ContactAddress) GetStreet() string`

GetStreet returns the Street field if non-nil, zero value otherwise.

### GetStreetOk

`func (o *ContactAddress) GetStreetOk() (*string, bool)`

GetStreetOk returns a tuple with the Street field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreet

`func (o *ContactAddress) SetStreet(v string)`

SetStreet sets Street field to given value.

### HasStreet

`func (o *ContactAddress) HasStreet() bool`

HasStreet returns a boolean if a field has been set.

### GetHouseNumber

`func (o *ContactAddress) GetHouseNumber() string`

GetHouseNumber returns the HouseNumber field if non-nil, zero value otherwise.

### GetHouseNumberOk

`func (o *ContactAddress) GetHouseNumberOk() (*string, bool)`

GetHouseNumberOk returns a tuple with the HouseNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHouseNumber

`func (o *ContactAddress) SetHouseNumber(v string)`

SetHouseNumber sets HouseNumber field to given value.

### HasHouseNumber

`func (o *ContactAddress) HasHouseNumber() bool`

HasHouseNumber returns a boolean if a field has been set.

### GetDoorNumber

`func (o *ContactAddress) GetDoorNumber() string`

GetDoorNumber returns the DoorNumber field if non-nil, zero value otherwise.

### GetDoorNumberOk

`func (o *ContactAddress) GetDoorNumberOk() (*string, bool)`

GetDoorNumberOk returns a tuple with the DoorNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDoorNumber

`func (o *ContactAddress) SetDoorNumber(v string)`

SetDoorNumber sets DoorNumber field to given value.

### HasDoorNumber

`func (o *ContactAddress) HasDoorNumber() bool`

HasDoorNumber returns a boolean if a field has been set.

### GetCity

`func (o *ContactAddress) GetCity() string`

GetCity returns the City field if non-nil, zero value otherwise.

### GetCityOk

`func (o *ContactAddress) GetCityOk() (*string, bool)`

GetCityOk returns a tuple with the City field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCity

`func (o *ContactAddress) SetCity(v string)`

SetCity sets City field to given value.

### HasCity

`func (o *ContactAddress) HasCity() bool`

HasCity returns a boolean if a field has been set.

### GetZip

`func (o *ContactAddress) GetZip() string`

GetZip returns the Zip field if non-nil, zero value otherwise.

### GetZipOk

`func (o *ContactAddress) GetZipOk() (*string, bool)`

GetZipOk returns a tuple with the Zip field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetZip

`func (o *ContactAddress) SetZip(v string)`

SetZip sets Zip field to given value.

### HasZip

`func (o *ContactAddress) HasZip() bool`

HasZip returns a boolean if a field has been set.

### GetCountry

`func (o *ContactAddress) GetCountry() string`

GetCountry returns the Country field if non-nil, zero value otherwise.

### GetCountryOk

`func (o *ContactAddress) GetCountryOk() (*string, bool)`

GetCountryOk returns a tuple with the Country field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountry

`func (o *ContactAddress) SetCountry(v string)`

SetCountry sets Country field to given value.

### HasCountry

`func (o *ContactAddress) HasCountry() bool`

HasCountry returns a boolean if a field has been set.

### GetCountryName

`func (o *ContactAddress) GetCountryName() string`

GetCountryName returns the CountryName field if non-nil, zero value otherwise.

### GetCountryNameOk

`func (o *ContactAddress) GetCountryNameOk() (*string, bool)`

GetCountryNameOk returns a tuple with the CountryName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountryName

`func (o *ContactAddress) SetCountryName(v string)`

SetCountryName sets CountryName field to given value.

### HasCountryName

`func (o *ContactAddress) HasCountryName() bool`

HasCountryName returns a boolean if a field has been set.

### GetCompanyName

`func (o *ContactAddress) GetCompanyName() string`

GetCompanyName returns the CompanyName field if non-nil, zero value otherwise.

### GetCompanyNameOk

`func (o *ContactAddress) GetCompanyNameOk() (*string, bool)`

GetCompanyNameOk returns a tuple with the CompanyName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompanyName

`func (o *ContactAddress) SetCompanyName(v string)`

SetCompanyName sets CompanyName field to given value.

### HasCompanyName

`func (o *ContactAddress) HasCompanyName() bool`

HasCompanyName returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


