# PostMeEntrypoints200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelId** | Pointer to **string** |  | [optional] 
**CreatedAt** | Pointer to **time.Time** |  | [optional] 
**CustomerExperienceId** | Pointer to **string** |  | [optional] 
**Id** | Pointer to **string** |  | [optional] 
**OrganizationId** | Pointer to **string** |  | [optional] 
**Ttl** | Pointer to **int32** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 

## Methods

### NewPostMeEntrypoints200Response

`func NewPostMeEntrypoints200Response() *PostMeEntrypoints200Response`

NewPostMeEntrypoints200Response instantiates a new PostMeEntrypoints200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPostMeEntrypoints200ResponseWithDefaults

`func NewPostMeEntrypoints200ResponseWithDefaults() *PostMeEntrypoints200Response`

NewPostMeEntrypoints200ResponseWithDefaults instantiates a new PostMeEntrypoints200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetChannelId

`func (o *PostMeEntrypoints200Response) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *PostMeEntrypoints200Response) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *PostMeEntrypoints200Response) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *PostMeEntrypoints200Response) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetCreatedAt

`func (o *PostMeEntrypoints200Response) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *PostMeEntrypoints200Response) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *PostMeEntrypoints200Response) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.

### HasCreatedAt

`func (o *PostMeEntrypoints200Response) HasCreatedAt() bool`

HasCreatedAt returns a boolean if a field has been set.

### GetCustomerExperienceId

`func (o *PostMeEntrypoints200Response) GetCustomerExperienceId() string`

GetCustomerExperienceId returns the CustomerExperienceId field if non-nil, zero value otherwise.

### GetCustomerExperienceIdOk

`func (o *PostMeEntrypoints200Response) GetCustomerExperienceIdOk() (*string, bool)`

GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerExperienceId

`func (o *PostMeEntrypoints200Response) SetCustomerExperienceId(v string)`

SetCustomerExperienceId sets CustomerExperienceId field to given value.

### HasCustomerExperienceId

`func (o *PostMeEntrypoints200Response) HasCustomerExperienceId() bool`

HasCustomerExperienceId returns a boolean if a field has been set.

### GetId

`func (o *PostMeEntrypoints200Response) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *PostMeEntrypoints200Response) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *PostMeEntrypoints200Response) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *PostMeEntrypoints200Response) HasId() bool`

HasId returns a boolean if a field has been set.

### GetOrganizationId

`func (o *PostMeEntrypoints200Response) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *PostMeEntrypoints200Response) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *PostMeEntrypoints200Response) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *PostMeEntrypoints200Response) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetTtl

`func (o *PostMeEntrypoints200Response) GetTtl() int32`

GetTtl returns the Ttl field if non-nil, zero value otherwise.

### GetTtlOk

`func (o *PostMeEntrypoints200Response) GetTtlOk() (*int32, bool)`

GetTtlOk returns a tuple with the Ttl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTtl

`func (o *PostMeEntrypoints200Response) SetTtl(v int32)`

SetTtl sets Ttl field to given value.

### HasTtl

`func (o *PostMeEntrypoints200Response) HasTtl() bool`

HasTtl returns a boolean if a field has been set.

### GetType

`func (o *PostMeEntrypoints200Response) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *PostMeEntrypoints200Response) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *PostMeEntrypoints200Response) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *PostMeEntrypoints200Response) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


