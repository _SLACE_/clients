# UserMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**Email** | **string** |  | 
**Active** | **bool** |  | 
**Role** | [**Role**](Role.md) |  | 
**Language** | Pointer to **string** |  | [optional] 
**PhoneNumber** | Pointer to **string** |  | [optional] 
**Type** | Pointer to [**Role**](Role.md) |  | [optional] 

## Methods

### NewUserMutable

`func NewUserMutable(name string, email string, active bool, role Role, ) *UserMutable`

NewUserMutable instantiates a new UserMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUserMutableWithDefaults

`func NewUserMutableWithDefaults() *UserMutable`

NewUserMutableWithDefaults instantiates a new UserMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *UserMutable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *UserMutable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *UserMutable) SetName(v string)`

SetName sets Name field to given value.


### GetEmail

`func (o *UserMutable) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *UserMutable) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *UserMutable) SetEmail(v string)`

SetEmail sets Email field to given value.


### GetActive

`func (o *UserMutable) GetActive() bool`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *UserMutable) GetActiveOk() (*bool, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *UserMutable) SetActive(v bool)`

SetActive sets Active field to given value.


### GetRole

`func (o *UserMutable) GetRole() Role`

GetRole returns the Role field if non-nil, zero value otherwise.

### GetRoleOk

`func (o *UserMutable) GetRoleOk() (*Role, bool)`

GetRoleOk returns a tuple with the Role field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRole

`func (o *UserMutable) SetRole(v Role)`

SetRole sets Role field to given value.


### GetLanguage

`func (o *UserMutable) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *UserMutable) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *UserMutable) SetLanguage(v string)`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *UserMutable) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.

### GetPhoneNumber

`func (o *UserMutable) GetPhoneNumber() string`

GetPhoneNumber returns the PhoneNumber field if non-nil, zero value otherwise.

### GetPhoneNumberOk

`func (o *UserMutable) GetPhoneNumberOk() (*string, bool)`

GetPhoneNumberOk returns a tuple with the PhoneNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhoneNumber

`func (o *UserMutable) SetPhoneNumber(v string)`

SetPhoneNumber sets PhoneNumber field to given value.

### HasPhoneNumber

`func (o *UserMutable) HasPhoneNumber() bool`

HasPhoneNumber returns a boolean if a field has been set.

### GetType

`func (o *UserMutable) GetType() Role`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *UserMutable) GetTypeOk() (*Role, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *UserMutable) SetType(v Role)`

SetType sets Type field to given value.

### HasType

`func (o *UserMutable) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


