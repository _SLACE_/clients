# \OrganizationsAPI

All URIs are relative to *https://api.slace.com/core*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateOrganization**](OrganizationsAPI.md#CreateOrganization) | **Post** /organizations | Create organization
[**DeactivateUser**](OrganizationsAPI.md#DeactivateUser) | **Post** /organizations/{organization_id}/users/{user_id}/deactivate | Deactivate user
[**DeleteOrganization**](OrganizationsAPI.md#DeleteOrganization) | **Delete** /organizations/{organization_id} | Delete organization
[**GetOrganization**](OrganizationsAPI.md#GetOrganization) | **Get** /organizations/{organization_id} | Get organization
[**GetOrganizationUser**](OrganizationsAPI.md#GetOrganizationUser) | **Get** /organizations/{organization_id}/users/{user_id} | Get organization user
[**GetOrganizationsOrganizationIdTags**](OrganizationsAPI.md#GetOrganizationsOrganizationIdTags) | **Get** /organizations/{organization_id}/tags | Get tags list
[**GetUsersPreferredConversation**](OrganizationsAPI.md#GetUsersPreferredConversation) | **Get** /organizations/{organization_id}/users/{user_id}/preferred-conversation/{channel_id} | Users preferred conversation
[**ImportUsers**](OrganizationsAPI.md#ImportUsers) | **Post** /organizations/{organization_id}/users-import | Import organization or channel users (pre-invited)
[**InviteOrganizationUser**](OrganizationsAPI.md#InviteOrganizationUser) | **Post** /organizations/{organization_id}/users | Invite new user
[**ListOrganizationUsers**](OrganizationsAPI.md#ListOrganizationUsers) | **Get** /organizations/{organization_id}/users | List organization users
[**ListOrganizations**](OrganizationsAPI.md#ListOrganizations) | **Get** /organizations | List organizations
[**PostOrganizationsOrganizationIdLogo**](OrganizationsAPI.md#PostOrganizationsOrganizationIdLogo) | **Post** /organizations/{organization_id}/logo | Upload logo
[**RemoveOrganizationUser**](OrganizationsAPI.md#RemoveOrganizationUser) | **Delete** /organizations/{organization_id}/users/{user_id} | Remove access for user
[**ResendUserInvitation**](OrganizationsAPI.md#ResendUserInvitation) | **Post** /organizations/{organization_id}/users/{user_id}/resend-invitation | Resend user invitation
[**RevokeUserPermissions**](OrganizationsAPI.md#RevokeUserPermissions) | **Post** /organizations/{organization_id}/users/{user_id}/revoke | Revoke User Permissions
[**UpdateOrganization**](OrganizationsAPI.md#UpdateOrganization) | **Put** /organizations/{organization_id} | Update organization
[**UpdateOrganizationUser**](OrganizationsAPI.md#UpdateOrganizationUser) | **Put** /organizations/{organization_id}/users/{user_id} | Update user



## CreateOrganization

> Organization CreateOrganization(ctx).OrganizationMutable(organizationMutable).Execute()

Create organization



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationMutable := *openapiclient.NewOrganizationMutable("CustomerId_example", "Name_example", *openapiclient.NewContactInfo("Email_example"), *openapiclient.NewThreshold(float32(123), "Unit_example"), *openapiclient.NewThreshold(float32(123), "Unit_example"), []string{"StopKeywords_example"}, openapiclient.Formality("prefer_less")) // OrganizationMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.CreateOrganization(context.Background()).OrganizationMutable(organizationMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.CreateOrganization``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateOrganization`: Organization
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.CreateOrganization`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateOrganizationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationMutable** | [**OrganizationMutable**](OrganizationMutable.md) |  | 

### Return type

[**Organization**](Organization.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeactivateUser

> OrganizationUser DeactivateUser(ctx, organizationId, userId).Execute()

Deactivate user

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    userId := "userId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.DeactivateUser(context.Background(), organizationId, userId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.DeactivateUser``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeactivateUser`: OrganizationUser
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.DeactivateUser`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**userId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeactivateUserRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**OrganizationUser**](OrganizationUser.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteOrganization

> DeleteOrganization(ctx, organizationId).Execute()

Delete organization



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.OrganizationsAPI.DeleteOrganization(context.Background(), organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.DeleteOrganization``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteOrganizationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganization

> Organization GetOrganization(ctx, organizationId).Execute()

Get organization



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.GetOrganization(context.Background(), organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.GetOrganization``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganization`: Organization
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.GetOrganization`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Organization**](Organization.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationUser

> OrganizationUser GetOrganizationUser(ctx, organizationId, userId).Execute()

Get organization user

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    userId := "userId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.GetOrganizationUser(context.Background(), organizationId, userId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.GetOrganizationUser``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationUser`: OrganizationUser
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.GetOrganizationUser`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**userId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationUserRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**OrganizationUser**](OrganizationUser.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationsOrganizationIdTags

> GetOrganizationsOrganizationIdTags200Response GetOrganizationsOrganizationIdTags(ctx, organizationId).Execute()

Get tags list

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.GetOrganizationsOrganizationIdTags(context.Background(), organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.GetOrganizationsOrganizationIdTags``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationsOrganizationIdTags`: GetOrganizationsOrganizationIdTags200Response
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.GetOrganizationsOrganizationIdTags`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationsOrganizationIdTagsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**GetOrganizationsOrganizationIdTags200Response**](GetOrganizationsOrganizationIdTags200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetUsersPreferredConversation

> Conversation GetUsersPreferredConversation(ctx, organizationId, userId, channelId).Execute()

Users preferred conversation

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    userId := "userId_example" // string | 
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.GetUsersPreferredConversation(context.Background(), organizationId, userId, channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.GetUsersPreferredConversation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetUsersPreferredConversation`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.GetUsersPreferredConversation`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**userId** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetUsersPreferredConversationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ImportUsers

> []OrganizationUser ImportUsers(ctx, organizationId).Execute()

Import organization or channel users (pre-invited)

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.ImportUsers(context.Background(), organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.ImportUsers``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ImportUsers`: []OrganizationUser
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.ImportUsers`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiImportUsersRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]OrganizationUser**](OrganizationUser.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## InviteOrganizationUser

> OrganizationUser InviteOrganizationUser(ctx, organizationId).OrganizationUserInvitation(organizationUserInvitation).Execute()

Invite new user



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    organizationUserInvitation := *openapiclient.NewOrganizationUserInvitation("Target_example", "Mode_example", int32(123)) // OrganizationUserInvitation |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.InviteOrganizationUser(context.Background(), organizationId).OrganizationUserInvitation(organizationUserInvitation).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.InviteOrganizationUser``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `InviteOrganizationUser`: OrganizationUser
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.InviteOrganizationUser`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiInviteOrganizationUserRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **organizationUserInvitation** | [**OrganizationUserInvitation**](OrganizationUserInvitation.md) |  | 

### Return type

[**OrganizationUser**](OrganizationUser.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListOrganizationUsers

> OrganizationUsersList ListOrganizationUsers(ctx, organizationId).ChannelId(channelId).Search(search).Limit(limit).Offset(offset).Sort(sort).Roles(roles).Statuses(statuses).ListContext(listContext).ExternalId(externalId).TagsAny(tagsAny).TagsAll(tagsAll).TagsNone(tagsNone).Execute()

List organization users



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    channelId := "channelId_example" // string |  (optional)
    search := "search_example" // string |  (optional)
    limit := int32(56) // int32 |  (optional)
    offset := int32(56) // int32 |  (optional)
    sort := "sort_example" // string |  (optional)
    roles := []string{"Inner_example"} // []string |  (optional)
    statuses := []string{"Inner_example"} // []string |  (optional)
    listContext := "listContext_example" // string |  (optional)
    externalId := "externalId_example" // string |  (optional)
    tagsAny := []string{"Inner_example"} // []string |  (optional)
    tagsAll := []string{"Inner_example"} // []string |  (optional)
    tagsNone := []string{"Inner_example"} // []string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.ListOrganizationUsers(context.Background(), organizationId).ChannelId(channelId).Search(search).Limit(limit).Offset(offset).Sort(sort).Roles(roles).Statuses(statuses).ListContext(listContext).ExternalId(externalId).TagsAny(tagsAny).TagsAll(tagsAll).TagsNone(tagsNone).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.ListOrganizationUsers``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListOrganizationUsers`: OrganizationUsersList
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.ListOrganizationUsers`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListOrganizationUsersRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **channelId** | **string** |  | 
 **search** | **string** |  | 
 **limit** | **int32** |  | 
 **offset** | **int32** |  | 
 **sort** | **string** |  | 
 **roles** | **[]string** |  | 
 **statuses** | **[]string** |  | 
 **listContext** | **string** |  | 
 **externalId** | **string** |  | 
 **tagsAny** | **[]string** |  | 
 **tagsAll** | **[]string** |  | 
 **tagsNone** | **[]string** |  | 

### Return type

[**OrganizationUsersList**](OrganizationUsersList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListOrganizations

> OrganizationsList ListOrganizations(ctx).CustomerId(customerId).Name(name).Search(search).Active(active).WithChannels(withChannels).Sort(sort).Limit(limit).Offset(offset).Execute()

List organizations



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    customerId := "customerId_example" // string |  (optional)
    name := "name_example" // string |  (optional)
    search := "search_example" // string |  (optional)
    active := true // bool |  (optional)
    withChannels := true // bool |  (optional)
    sort := "sort_example" // string |  (optional)
    limit := int32(56) // int32 |  (optional)
    offset := int32(56) // int32 |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.ListOrganizations(context.Background()).CustomerId(customerId).Name(name).Search(search).Active(active).WithChannels(withChannels).Sort(sort).Limit(limit).Offset(offset).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.ListOrganizations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListOrganizations`: OrganizationsList
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.ListOrganizations`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListOrganizationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **string** |  | 
 **name** | **string** |  | 
 **search** | **string** |  | 
 **active** | **bool** |  | 
 **withChannels** | **bool** |  | 
 **sort** | **string** |  | 
 **limit** | **int32** |  | 
 **offset** | **int32** |  | 

### Return type

[**OrganizationsList**](OrganizationsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostOrganizationsOrganizationIdLogo

> Organization PostOrganizationsOrganizationIdLogo(ctx, organizationId).File(file).Execute()

Upload logo

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    file := os.NewFile(1234, "some_file") // *os.File | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.PostOrganizationsOrganizationIdLogo(context.Background(), organizationId).File(file).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.PostOrganizationsOrganizationIdLogo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PostOrganizationsOrganizationIdLogo`: Organization
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.PostOrganizationsOrganizationIdLogo`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPostOrganizationsOrganizationIdLogoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **file** | ***os.File** |  | 

### Return type

[**Organization**](Organization.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RemoveOrganizationUser

> RemoveOrganizationUser(ctx, organizationId, userId).Execute()

Remove access for user



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    userId := "userId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.OrganizationsAPI.RemoveOrganizationUser(context.Background(), organizationId, userId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.RemoveOrganizationUser``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**userId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiRemoveOrganizationUserRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ResendUserInvitation

> GenericResponse ResendUserInvitation(ctx, organizationId, userId).Execute()

Resend user invitation



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    userId := "userId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.ResendUserInvitation(context.Background(), organizationId, userId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.ResendUserInvitation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ResendUserInvitation`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.ResendUserInvitation`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**userId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiResendUserInvitationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RevokeUserPermissions

> OrganizationUser RevokeUserPermissions(ctx, organizationId, userId).RevokeUserPermissionsRequest(revokeUserPermissionsRequest).Execute()

Revoke User Permissions

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    userId := "userId_example" // string | 
    revokeUserPermissionsRequest := *openapiclient.NewRevokeUserPermissionsRequest() // RevokeUserPermissionsRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.RevokeUserPermissions(context.Background(), organizationId, userId).RevokeUserPermissionsRequest(revokeUserPermissionsRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.RevokeUserPermissions``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RevokeUserPermissions`: OrganizationUser
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.RevokeUserPermissions`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**userId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiRevokeUserPermissionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **revokeUserPermissionsRequest** | [**RevokeUserPermissionsRequest**](RevokeUserPermissionsRequest.md) |  | 

### Return type

[**OrganizationUser**](OrganizationUser.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateOrganization

> Organization UpdateOrganization(ctx, organizationId).OrganizationMutable(organizationMutable).Execute()

Update organization



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    organizationMutable := *openapiclient.NewOrganizationMutable("CustomerId_example", "Name_example", *openapiclient.NewContactInfo("Email_example"), *openapiclient.NewThreshold(float32(123), "Unit_example"), *openapiclient.NewThreshold(float32(123), "Unit_example"), []string{"StopKeywords_example"}, openapiclient.Formality("prefer_less")) // OrganizationMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.UpdateOrganization(context.Background(), organizationId).OrganizationMutable(organizationMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.UpdateOrganization``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateOrganization`: Organization
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.UpdateOrganization`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateOrganizationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **organizationMutable** | [**OrganizationMutable**](OrganizationMutable.md) |  | 

### Return type

[**Organization**](Organization.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateOrganizationUser

> OrganizationUser UpdateOrganizationUser(ctx, organizationId, userId).OrganizationUserMutable(organizationUserMutable).Execute()

Update user

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    userId := "userId_example" // string | 
    organizationUserMutable := *openapiclient.NewOrganizationUserMutable() // OrganizationUserMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrganizationsAPI.UpdateOrganizationUser(context.Background(), organizationId, userId).OrganizationUserMutable(organizationUserMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrganizationsAPI.UpdateOrganizationUser``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateOrganizationUser`: OrganizationUser
    fmt.Fprintf(os.Stdout, "Response from `OrganizationsAPI.UpdateOrganizationUser`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**userId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateOrganizationUserRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **organizationUserMutable** | [**OrganizationUserMutable**](OrganizationUserMutable.md) |  | 

### Return type

[**OrganizationUser**](OrganizationUser.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

