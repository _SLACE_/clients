# CustomersList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]Customer**](Customer.md) |  | 

## Methods

### NewCustomersList

`func NewCustomersList(results []Customer, ) *CustomersList`

NewCustomersList instantiates a new CustomersList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCustomersListWithDefaults

`func NewCustomersListWithDefaults() *CustomersList`

NewCustomersListWithDefaults instantiates a new CustomersList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *CustomersList) GetResults() []Customer`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *CustomersList) GetResultsOk() (*[]Customer, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *CustomersList) SetResults(v []Customer)`

SetResults sets Results field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


