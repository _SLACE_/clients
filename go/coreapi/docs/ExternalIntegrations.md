# ExternalIntegrations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Unzer** | Pointer to [**ExternalIntegrationsUnzer**](ExternalIntegrationsUnzer.md) |  | [optional] 

## Methods

### NewExternalIntegrations

`func NewExternalIntegrations() *ExternalIntegrations`

NewExternalIntegrations instantiates a new ExternalIntegrations object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExternalIntegrationsWithDefaults

`func NewExternalIntegrationsWithDefaults() *ExternalIntegrations`

NewExternalIntegrationsWithDefaults instantiates a new ExternalIntegrations object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUnzer

`func (o *ExternalIntegrations) GetUnzer() ExternalIntegrationsUnzer`

GetUnzer returns the Unzer field if non-nil, zero value otherwise.

### GetUnzerOk

`func (o *ExternalIntegrations) GetUnzerOk() (*ExternalIntegrationsUnzer, bool)`

GetUnzerOk returns a tuple with the Unzer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnzer

`func (o *ExternalIntegrations) SetUnzer(v ExternalIntegrationsUnzer)`

SetUnzer sets Unzer field to given value.

### HasUnzer

`func (o *ExternalIntegrations) HasUnzer() bool`

HasUnzer returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


