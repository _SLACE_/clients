# ContactERConfidence

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FirstName** | Pointer to **int32** |  | [optional] 
**LastName** | Pointer to **int32** |  | [optional] 
**Gender** | Pointer to **int32** |  | [optional] 
**Title** | Pointer to **int32** |  | [optional] 

## Methods

### NewContactERConfidence

`func NewContactERConfidence() *ContactERConfidence`

NewContactERConfidence instantiates a new ContactERConfidence object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactERConfidenceWithDefaults

`func NewContactERConfidenceWithDefaults() *ContactERConfidence`

NewContactERConfidenceWithDefaults instantiates a new ContactERConfidence object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFirstName

`func (o *ContactERConfidence) GetFirstName() int32`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *ContactERConfidence) GetFirstNameOk() (*int32, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *ContactERConfidence) SetFirstName(v int32)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *ContactERConfidence) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *ContactERConfidence) GetLastName() int32`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *ContactERConfidence) GetLastNameOk() (*int32, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *ContactERConfidence) SetLastName(v int32)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *ContactERConfidence) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetGender

`func (o *ContactERConfidence) GetGender() int32`

GetGender returns the Gender field if non-nil, zero value otherwise.

### GetGenderOk

`func (o *ContactERConfidence) GetGenderOk() (*int32, bool)`

GetGenderOk returns a tuple with the Gender field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGender

`func (o *ContactERConfidence) SetGender(v int32)`

SetGender sets Gender field to given value.

### HasGender

`func (o *ContactERConfidence) HasGender() bool`

HasGender returns a boolean if a field has been set.

### GetTitle

`func (o *ContactERConfidence) GetTitle() int32`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *ContactERConfidence) GetTitleOk() (*int32, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *ContactERConfidence) SetTitle(v int32)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *ContactERConfidence) HasTitle() bool`

HasTitle returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


