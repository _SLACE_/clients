# IceBreaker

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CallToActions** | Pointer to [**[]IceBreakerCallToActionsInner**](IceBreakerCallToActionsInner.md) |  | [optional] 
**Locale** | Pointer to **string** |  | [optional] 

## Methods

### NewIceBreaker

`func NewIceBreaker() *IceBreaker`

NewIceBreaker instantiates a new IceBreaker object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIceBreakerWithDefaults

`func NewIceBreakerWithDefaults() *IceBreaker`

NewIceBreakerWithDefaults instantiates a new IceBreaker object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCallToActions

`func (o *IceBreaker) GetCallToActions() []IceBreakerCallToActionsInner`

GetCallToActions returns the CallToActions field if non-nil, zero value otherwise.

### GetCallToActionsOk

`func (o *IceBreaker) GetCallToActionsOk() (*[]IceBreakerCallToActionsInner, bool)`

GetCallToActionsOk returns a tuple with the CallToActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCallToActions

`func (o *IceBreaker) SetCallToActions(v []IceBreakerCallToActionsInner)`

SetCallToActions sets CallToActions field to given value.

### HasCallToActions

`func (o *IceBreaker) HasCallToActions() bool`

HasCallToActions returns a boolean if a field has been set.

### GetLocale

`func (o *IceBreaker) GetLocale() string`

GetLocale returns the Locale field if non-nil, zero value otherwise.

### GetLocaleOk

`func (o *IceBreaker) GetLocaleOk() (*string, bool)`

GetLocaleOk returns a tuple with the Locale field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocale

`func (o *IceBreaker) SetLocale(v string)`

SetLocale sets Locale field to given value.

### HasLocale

`func (o *IceBreaker) HasLocale() bool`

HasLocale returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


