# Dialog360Tier

## Enum


* `TierDefault` (value: `"default"`)

* `TierBasic` (value: `"basic"`)

* `TierRegular` (value: `"regular"`)

* `TierPremium` (value: `"premium"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


