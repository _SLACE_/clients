# WebhookEventConversationCustomData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Locale** | Pointer to **string** |  | [optional] 
**TransactionId** | Pointer to **string** |  | [optional] 
**Data** | Pointer to **map[string][]string** | Here you can find entry point data | [optional] 

## Methods

### NewWebhookEventConversationCustomData

`func NewWebhookEventConversationCustomData() *WebhookEventConversationCustomData`

NewWebhookEventConversationCustomData instantiates a new WebhookEventConversationCustomData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookEventConversationCustomDataWithDefaults

`func NewWebhookEventConversationCustomDataWithDefaults() *WebhookEventConversationCustomData`

NewWebhookEventConversationCustomDataWithDefaults instantiates a new WebhookEventConversationCustomData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLocale

`func (o *WebhookEventConversationCustomData) GetLocale() string`

GetLocale returns the Locale field if non-nil, zero value otherwise.

### GetLocaleOk

`func (o *WebhookEventConversationCustomData) GetLocaleOk() (*string, bool)`

GetLocaleOk returns a tuple with the Locale field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocale

`func (o *WebhookEventConversationCustomData) SetLocale(v string)`

SetLocale sets Locale field to given value.

### HasLocale

`func (o *WebhookEventConversationCustomData) HasLocale() bool`

HasLocale returns a boolean if a field has been set.

### GetTransactionId

`func (o *WebhookEventConversationCustomData) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *WebhookEventConversationCustomData) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *WebhookEventConversationCustomData) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.

### HasTransactionId

`func (o *WebhookEventConversationCustomData) HasTransactionId() bool`

HasTransactionId returns a boolean if a field has been set.

### GetData

`func (o *WebhookEventConversationCustomData) GetData() map[string][]string`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *WebhookEventConversationCustomData) GetDataOk() (*map[string][]string, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *WebhookEventConversationCustomData) SetData(v map[string][]string)`

SetData sets Data field to given value.

### HasData

`func (o *WebhookEventConversationCustomData) HasData() bool`

HasData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


