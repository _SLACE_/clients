# UserPermissions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Organizations** | Pointer to [**[]ObjectPermission**](ObjectPermission.md) |  | [optional] 
**Channels** | Pointer to [**[]ObjectPermission**](ObjectPermission.md) |  | [optional] 

## Methods

### NewUserPermissions

`func NewUserPermissions() *UserPermissions`

NewUserPermissions instantiates a new UserPermissions object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUserPermissionsWithDefaults

`func NewUserPermissionsWithDefaults() *UserPermissions`

NewUserPermissionsWithDefaults instantiates a new UserPermissions object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOrganizations

`func (o *UserPermissions) GetOrganizations() []ObjectPermission`

GetOrganizations returns the Organizations field if non-nil, zero value otherwise.

### GetOrganizationsOk

`func (o *UserPermissions) GetOrganizationsOk() (*[]ObjectPermission, bool)`

GetOrganizationsOk returns a tuple with the Organizations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizations

`func (o *UserPermissions) SetOrganizations(v []ObjectPermission)`

SetOrganizations sets Organizations field to given value.

### HasOrganizations

`func (o *UserPermissions) HasOrganizations() bool`

HasOrganizations returns a boolean if a field has been set.

### GetChannels

`func (o *UserPermissions) GetChannels() []ObjectPermission`

GetChannels returns the Channels field if non-nil, zero value otherwise.

### GetChannelsOk

`func (o *UserPermissions) GetChannelsOk() (*[]ObjectPermission, bool)`

GetChannelsOk returns a tuple with the Channels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannels

`func (o *UserPermissions) SetChannels(v []ObjectPermission)`

SetChannels sets Channels field to given value.

### HasChannels

`func (o *UserPermissions) HasChannels() bool`

HasChannels returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


