# CampaignStrategyChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TagPreference** | Pointer to **string** |  | [optional] 
**UsagePreference** | Pointer to **string** |  | [optional] 

## Methods

### NewCampaignStrategyChannel

`func NewCampaignStrategyChannel() *CampaignStrategyChannel`

NewCampaignStrategyChannel instantiates a new CampaignStrategyChannel object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignStrategyChannelWithDefaults

`func NewCampaignStrategyChannelWithDefaults() *CampaignStrategyChannel`

NewCampaignStrategyChannelWithDefaults instantiates a new CampaignStrategyChannel object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTagPreference

`func (o *CampaignStrategyChannel) GetTagPreference() string`

GetTagPreference returns the TagPreference field if non-nil, zero value otherwise.

### GetTagPreferenceOk

`func (o *CampaignStrategyChannel) GetTagPreferenceOk() (*string, bool)`

GetTagPreferenceOk returns a tuple with the TagPreference field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTagPreference

`func (o *CampaignStrategyChannel) SetTagPreference(v string)`

SetTagPreference sets TagPreference field to given value.

### HasTagPreference

`func (o *CampaignStrategyChannel) HasTagPreference() bool`

HasTagPreference returns a boolean if a field has been set.

### GetUsagePreference

`func (o *CampaignStrategyChannel) GetUsagePreference() string`

GetUsagePreference returns the UsagePreference field if non-nil, zero value otherwise.

### GetUsagePreferenceOk

`func (o *CampaignStrategyChannel) GetUsagePreferenceOk() (*string, bool)`

GetUsagePreferenceOk returns a tuple with the UsagePreference field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsagePreference

`func (o *CampaignStrategyChannel) SetUsagePreference(v string)`

SetUsagePreference sets UsagePreference field to given value.

### HasUsagePreference

`func (o *CampaignStrategyChannel) HasUsagePreference() bool`

HasUsagePreference returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


