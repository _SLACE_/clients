# WebhookEventMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**AuthorId** | Pointer to **string** |  | [optional] 
**Type** | [**MessageType**](MessageType.md) |  | 
**Direction** | **string** |  | 
**Text** | Pointer to **string** |  | [optional] 
**Source** | Pointer to **string** |  | [optional] 
**ReplyToId** | Pointer to **string** |  | [optional] 
**Labels** | Pointer to **[]string** |  | [optional] 
**Data** | Pointer to [**WebhookEventMessageData**](WebhookEventMessageData.md) |  | [optional] 
**Timestamp** | **time.Time** |  | 

## Methods

### NewWebhookEventMessage

`func NewWebhookEventMessage(id string, type_ MessageType, direction string, timestamp time.Time, ) *WebhookEventMessage`

NewWebhookEventMessage instantiates a new WebhookEventMessage object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookEventMessageWithDefaults

`func NewWebhookEventMessageWithDefaults() *WebhookEventMessage`

NewWebhookEventMessageWithDefaults instantiates a new WebhookEventMessage object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WebhookEventMessage) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WebhookEventMessage) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WebhookEventMessage) SetId(v string)`

SetId sets Id field to given value.


### GetAuthorId

`func (o *WebhookEventMessage) GetAuthorId() string`

GetAuthorId returns the AuthorId field if non-nil, zero value otherwise.

### GetAuthorIdOk

`func (o *WebhookEventMessage) GetAuthorIdOk() (*string, bool)`

GetAuthorIdOk returns a tuple with the AuthorId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthorId

`func (o *WebhookEventMessage) SetAuthorId(v string)`

SetAuthorId sets AuthorId field to given value.

### HasAuthorId

`func (o *WebhookEventMessage) HasAuthorId() bool`

HasAuthorId returns a boolean if a field has been set.

### GetType

`func (o *WebhookEventMessage) GetType() MessageType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WebhookEventMessage) GetTypeOk() (*MessageType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WebhookEventMessage) SetType(v MessageType)`

SetType sets Type field to given value.


### GetDirection

`func (o *WebhookEventMessage) GetDirection() string`

GetDirection returns the Direction field if non-nil, zero value otherwise.

### GetDirectionOk

`func (o *WebhookEventMessage) GetDirectionOk() (*string, bool)`

GetDirectionOk returns a tuple with the Direction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDirection

`func (o *WebhookEventMessage) SetDirection(v string)`

SetDirection sets Direction field to given value.


### GetText

`func (o *WebhookEventMessage) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *WebhookEventMessage) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *WebhookEventMessage) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *WebhookEventMessage) HasText() bool`

HasText returns a boolean if a field has been set.

### GetSource

`func (o *WebhookEventMessage) GetSource() string`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *WebhookEventMessage) GetSourceOk() (*string, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *WebhookEventMessage) SetSource(v string)`

SetSource sets Source field to given value.

### HasSource

`func (o *WebhookEventMessage) HasSource() bool`

HasSource returns a boolean if a field has been set.

### GetReplyToId

`func (o *WebhookEventMessage) GetReplyToId() string`

GetReplyToId returns the ReplyToId field if non-nil, zero value otherwise.

### GetReplyToIdOk

`func (o *WebhookEventMessage) GetReplyToIdOk() (*string, bool)`

GetReplyToIdOk returns a tuple with the ReplyToId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReplyToId

`func (o *WebhookEventMessage) SetReplyToId(v string)`

SetReplyToId sets ReplyToId field to given value.

### HasReplyToId

`func (o *WebhookEventMessage) HasReplyToId() bool`

HasReplyToId returns a boolean if a field has been set.

### GetLabels

`func (o *WebhookEventMessage) GetLabels() []string`

GetLabels returns the Labels field if non-nil, zero value otherwise.

### GetLabelsOk

`func (o *WebhookEventMessage) GetLabelsOk() (*[]string, bool)`

GetLabelsOk returns a tuple with the Labels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabels

`func (o *WebhookEventMessage) SetLabels(v []string)`

SetLabels sets Labels field to given value.

### HasLabels

`func (o *WebhookEventMessage) HasLabels() bool`

HasLabels returns a boolean if a field has been set.

### GetData

`func (o *WebhookEventMessage) GetData() WebhookEventMessageData`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *WebhookEventMessage) GetDataOk() (*WebhookEventMessageData, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *WebhookEventMessage) SetData(v WebhookEventMessageData)`

SetData sets Data field to given value.

### HasData

`func (o *WebhookEventMessage) HasData() bool`

HasData returns a boolean if a field has been set.

### GetTimestamp

`func (o *WebhookEventMessage) GetTimestamp() time.Time`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *WebhookEventMessage) GetTimestampOk() (*time.Time, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *WebhookEventMessage) SetTimestamp(v time.Time)`

SetTimestamp sets Timestamp field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


