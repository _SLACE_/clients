# WebhookEventGateway

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**MessengerId** | **string** |  | 
**Provider** | [**Provider**](Provider.md) |  | 
**MessengerType** | [**Messenger**](Messenger.md) |  | 

## Methods

### NewWebhookEventGateway

`func NewWebhookEventGateway(id string, messengerId string, provider Provider, messengerType Messenger, ) *WebhookEventGateway`

NewWebhookEventGateway instantiates a new WebhookEventGateway object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookEventGatewayWithDefaults

`func NewWebhookEventGatewayWithDefaults() *WebhookEventGateway`

NewWebhookEventGatewayWithDefaults instantiates a new WebhookEventGateway object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WebhookEventGateway) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WebhookEventGateway) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WebhookEventGateway) SetId(v string)`

SetId sets Id field to given value.


### GetMessengerId

`func (o *WebhookEventGateway) GetMessengerId() string`

GetMessengerId returns the MessengerId field if non-nil, zero value otherwise.

### GetMessengerIdOk

`func (o *WebhookEventGateway) GetMessengerIdOk() (*string, bool)`

GetMessengerIdOk returns a tuple with the MessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerId

`func (o *WebhookEventGateway) SetMessengerId(v string)`

SetMessengerId sets MessengerId field to given value.


### GetProvider

`func (o *WebhookEventGateway) GetProvider() Provider`

GetProvider returns the Provider field if non-nil, zero value otherwise.

### GetProviderOk

`func (o *WebhookEventGateway) GetProviderOk() (*Provider, bool)`

GetProviderOk returns a tuple with the Provider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProvider

`func (o *WebhookEventGateway) SetProvider(v Provider)`

SetProvider sets Provider field to given value.


### GetMessengerType

`func (o *WebhookEventGateway) GetMessengerType() Messenger`

GetMessengerType returns the MessengerType field if non-nil, zero value otherwise.

### GetMessengerTypeOk

`func (o *WebhookEventGateway) GetMessengerTypeOk() (*Messenger, bool)`

GetMessengerTypeOk returns a tuple with the MessengerType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerType

`func (o *WebhookEventGateway) SetMessengerType(v Messenger)`

SetMessengerType sets MessengerType field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


