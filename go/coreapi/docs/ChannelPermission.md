# ChannelPermission

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**ObjectPermission** | Pointer to **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | [optional] 
**Permissions** | Pointer to [**ChannelPermissionPermissions**](ChannelPermissionPermissions.md) |  | [optional] 
**Permission** | Pointer to [**Permission**](Permission.md) |  | [optional] 

## Methods

### NewChannelPermission

`func NewChannelPermission() *ChannelPermission`

NewChannelPermission instantiates a new ChannelPermission object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChannelPermissionWithDefaults

`func NewChannelPermissionWithDefaults() *ChannelPermission`

NewChannelPermissionWithDefaults instantiates a new ChannelPermission object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ChannelPermission) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ChannelPermission) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ChannelPermission) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *ChannelPermission) HasId() bool`

HasId returns a boolean if a field has been set.

### GetObjectPermission

`func (o *ChannelPermission) GetObjectPermission() int32`

GetObjectPermission returns the ObjectPermission field if non-nil, zero value otherwise.

### GetObjectPermissionOk

`func (o *ChannelPermission) GetObjectPermissionOk() (*int32, bool)`

GetObjectPermissionOk returns a tuple with the ObjectPermission field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetObjectPermission

`func (o *ChannelPermission) SetObjectPermission(v int32)`

SetObjectPermission sets ObjectPermission field to given value.

### HasObjectPermission

`func (o *ChannelPermission) HasObjectPermission() bool`

HasObjectPermission returns a boolean if a field has been set.

### GetPermissions

`func (o *ChannelPermission) GetPermissions() ChannelPermissionPermissions`

GetPermissions returns the Permissions field if non-nil, zero value otherwise.

### GetPermissionsOk

`func (o *ChannelPermission) GetPermissionsOk() (*ChannelPermissionPermissions, bool)`

GetPermissionsOk returns a tuple with the Permissions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermissions

`func (o *ChannelPermission) SetPermissions(v ChannelPermissionPermissions)`

SetPermissions sets Permissions field to given value.

### HasPermissions

`func (o *ChannelPermission) HasPermissions() bool`

HasPermissions returns a boolean if a field has been set.

### GetPermission

`func (o *ChannelPermission) GetPermission() Permission`

GetPermission returns the Permission field if non-nil, zero value otherwise.

### GetPermissionOk

`func (o *ChannelPermission) GetPermissionOk() (*Permission, bool)`

GetPermissionOk returns a tuple with the Permission field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermission

`func (o *ChannelPermission) SetPermission(v Permission)`

SetPermission sets Permission field to given value.

### HasPermission

`func (o *ChannelPermission) HasPermission() bool`

HasPermission returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


