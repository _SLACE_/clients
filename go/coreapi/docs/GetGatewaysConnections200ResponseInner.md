# GetGatewaysConnections200ResponseInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**PhoneNumber** | Pointer to **string** |  | [optional] 
**PhoneName** | Pointer to **string** |  | [optional] 
**Status** | Pointer to **string** |  | [optional] 
**Error** | Pointer to **string** |  | [optional] 

## Methods

### NewGetGatewaysConnections200ResponseInner

`func NewGetGatewaysConnections200ResponseInner() *GetGatewaysConnections200ResponseInner`

NewGetGatewaysConnections200ResponseInner instantiates a new GetGatewaysConnections200ResponseInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetGatewaysConnections200ResponseInnerWithDefaults

`func NewGetGatewaysConnections200ResponseInnerWithDefaults() *GetGatewaysConnections200ResponseInner`

NewGetGatewaysConnections200ResponseInnerWithDefaults instantiates a new GetGatewaysConnections200ResponseInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *GetGatewaysConnections200ResponseInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *GetGatewaysConnections200ResponseInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *GetGatewaysConnections200ResponseInner) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *GetGatewaysConnections200ResponseInner) HasId() bool`

HasId returns a boolean if a field has been set.

### GetPhoneNumber

`func (o *GetGatewaysConnections200ResponseInner) GetPhoneNumber() string`

GetPhoneNumber returns the PhoneNumber field if non-nil, zero value otherwise.

### GetPhoneNumberOk

`func (o *GetGatewaysConnections200ResponseInner) GetPhoneNumberOk() (*string, bool)`

GetPhoneNumberOk returns a tuple with the PhoneNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhoneNumber

`func (o *GetGatewaysConnections200ResponseInner) SetPhoneNumber(v string)`

SetPhoneNumber sets PhoneNumber field to given value.

### HasPhoneNumber

`func (o *GetGatewaysConnections200ResponseInner) HasPhoneNumber() bool`

HasPhoneNumber returns a boolean if a field has been set.

### GetPhoneName

`func (o *GetGatewaysConnections200ResponseInner) GetPhoneName() string`

GetPhoneName returns the PhoneName field if non-nil, zero value otherwise.

### GetPhoneNameOk

`func (o *GetGatewaysConnections200ResponseInner) GetPhoneNameOk() (*string, bool)`

GetPhoneNameOk returns a tuple with the PhoneName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhoneName

`func (o *GetGatewaysConnections200ResponseInner) SetPhoneName(v string)`

SetPhoneName sets PhoneName field to given value.

### HasPhoneName

`func (o *GetGatewaysConnections200ResponseInner) HasPhoneName() bool`

HasPhoneName returns a boolean if a field has been set.

### GetStatus

`func (o *GetGatewaysConnections200ResponseInner) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *GetGatewaysConnections200ResponseInner) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *GetGatewaysConnections200ResponseInner) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *GetGatewaysConnections200ResponseInner) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetError

`func (o *GetGatewaysConnections200ResponseInner) GetError() string`

GetError returns the Error field if non-nil, zero value otherwise.

### GetErrorOk

`func (o *GetGatewaysConnections200ResponseInner) GetErrorOk() (*string, bool)`

GetErrorOk returns a tuple with the Error field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetError

`func (o *GetGatewaysConnections200ResponseInner) SetError(v string)`

SetError sets Error field to given value.

### HasError

`func (o *GetGatewaysConnections200ResponseInner) HasError() bool`

HasError returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


