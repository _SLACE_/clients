# MessageOrderDetailsDiscount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Amount** | **string** |  | 
**Description** | Pointer to **string** |  | [optional] 
**ProgramName** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageOrderDetailsDiscount

`func NewMessageOrderDetailsDiscount(amount string, ) *MessageOrderDetailsDiscount`

NewMessageOrderDetailsDiscount instantiates a new MessageOrderDetailsDiscount object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageOrderDetailsDiscountWithDefaults

`func NewMessageOrderDetailsDiscountWithDefaults() *MessageOrderDetailsDiscount`

NewMessageOrderDetailsDiscountWithDefaults instantiates a new MessageOrderDetailsDiscount object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAmount

`func (o *MessageOrderDetailsDiscount) GetAmount() string`

GetAmount returns the Amount field if non-nil, zero value otherwise.

### GetAmountOk

`func (o *MessageOrderDetailsDiscount) GetAmountOk() (*string, bool)`

GetAmountOk returns a tuple with the Amount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAmount

`func (o *MessageOrderDetailsDiscount) SetAmount(v string)`

SetAmount sets Amount field to given value.


### GetDescription

`func (o *MessageOrderDetailsDiscount) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *MessageOrderDetailsDiscount) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *MessageOrderDetailsDiscount) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *MessageOrderDetailsDiscount) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetProgramName

`func (o *MessageOrderDetailsDiscount) GetProgramName() string`

GetProgramName returns the ProgramName field if non-nil, zero value otherwise.

### GetProgramNameOk

`func (o *MessageOrderDetailsDiscount) GetProgramNameOk() (*string, bool)`

GetProgramNameOk returns a tuple with the ProgramName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProgramName

`func (o *MessageOrderDetailsDiscount) SetProgramName(v string)`

SetProgramName sets ProgramName field to given value.

### HasProgramName

`func (o *MessageOrderDetailsDiscount) HasProgramName() bool`

HasProgramName returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


