# GatewaysList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]Gateway**](Gateway.md) |  | 
**Total** | **int32** |  | 

## Methods

### NewGatewaysList

`func NewGatewaysList(results []Gateway, total int32, ) *GatewaysList`

NewGatewaysList instantiates a new GatewaysList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGatewaysListWithDefaults

`func NewGatewaysListWithDefaults() *GatewaysList`

NewGatewaysListWithDefaults instantiates a new GatewaysList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *GatewaysList) GetResults() []Gateway`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *GatewaysList) GetResultsOk() (*[]Gateway, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *GatewaysList) SetResults(v []Gateway)`

SetResults sets Results field to given value.


### GetTotal

`func (o *GatewaysList) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *GatewaysList) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *GatewaysList) SetTotal(v int32)`

SetTotal sets Total field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


