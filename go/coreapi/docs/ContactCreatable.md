# ContactCreatable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**FirstName** | Pointer to **string** |  | [optional] 
**LastName** | Pointer to **string** |  | [optional] 
**Timezone** | Pointer to **string** |  | [optional] 
**Email** | Pointer to **string** |  | [optional] 
**Mobile** | Pointer to **string** |  | [optional] 
**StrictAttributes** | Pointer to **map[string]interface{}** |  | [optional] 
**Languages** | Pointer to [**ContactCreatableLanguages**](ContactCreatableLanguages.md) |  | [optional] 

## Methods

### NewContactCreatable

`func NewContactCreatable() *ContactCreatable`

NewContactCreatable instantiates a new ContactCreatable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactCreatableWithDefaults

`func NewContactCreatableWithDefaults() *ContactCreatable`

NewContactCreatableWithDefaults instantiates a new ContactCreatable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ContactCreatable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ContactCreatable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ContactCreatable) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ContactCreatable) HasName() bool`

HasName returns a boolean if a field has been set.

### GetFirstName

`func (o *ContactCreatable) GetFirstName() string`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *ContactCreatable) GetFirstNameOk() (*string, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *ContactCreatable) SetFirstName(v string)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *ContactCreatable) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *ContactCreatable) GetLastName() string`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *ContactCreatable) GetLastNameOk() (*string, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *ContactCreatable) SetLastName(v string)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *ContactCreatable) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetTimezone

`func (o *ContactCreatable) GetTimezone() string`

GetTimezone returns the Timezone field if non-nil, zero value otherwise.

### GetTimezoneOk

`func (o *ContactCreatable) GetTimezoneOk() (*string, bool)`

GetTimezoneOk returns a tuple with the Timezone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimezone

`func (o *ContactCreatable) SetTimezone(v string)`

SetTimezone sets Timezone field to given value.

### HasTimezone

`func (o *ContactCreatable) HasTimezone() bool`

HasTimezone returns a boolean if a field has been set.

### GetEmail

`func (o *ContactCreatable) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *ContactCreatable) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *ContactCreatable) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *ContactCreatable) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetMobile

`func (o *ContactCreatable) GetMobile() string`

GetMobile returns the Mobile field if non-nil, zero value otherwise.

### GetMobileOk

`func (o *ContactCreatable) GetMobileOk() (*string, bool)`

GetMobileOk returns a tuple with the Mobile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobile

`func (o *ContactCreatable) SetMobile(v string)`

SetMobile sets Mobile field to given value.

### HasMobile

`func (o *ContactCreatable) HasMobile() bool`

HasMobile returns a boolean if a field has been set.

### GetStrictAttributes

`func (o *ContactCreatable) GetStrictAttributes() map[string]interface{}`

GetStrictAttributes returns the StrictAttributes field if non-nil, zero value otherwise.

### GetStrictAttributesOk

`func (o *ContactCreatable) GetStrictAttributesOk() (*map[string]interface{}, bool)`

GetStrictAttributesOk returns a tuple with the StrictAttributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStrictAttributes

`func (o *ContactCreatable) SetStrictAttributes(v map[string]interface{})`

SetStrictAttributes sets StrictAttributes field to given value.

### HasStrictAttributes

`func (o *ContactCreatable) HasStrictAttributes() bool`

HasStrictAttributes returns a boolean if a field has been set.

### GetLanguages

`func (o *ContactCreatable) GetLanguages() ContactCreatableLanguages`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *ContactCreatable) GetLanguagesOk() (*ContactCreatableLanguages, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *ContactCreatable) SetLanguages(v ContactCreatableLanguages)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *ContactCreatable) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


