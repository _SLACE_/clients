# MessengerSettingsProfilePhoto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MimeType** | **string** |  | 
**Binary** | **string** |  | 

## Methods

### NewMessengerSettingsProfilePhoto

`func NewMessengerSettingsProfilePhoto(mimeType string, binary string, ) *MessengerSettingsProfilePhoto`

NewMessengerSettingsProfilePhoto instantiates a new MessengerSettingsProfilePhoto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessengerSettingsProfilePhotoWithDefaults

`func NewMessengerSettingsProfilePhotoWithDefaults() *MessengerSettingsProfilePhoto`

NewMessengerSettingsProfilePhotoWithDefaults instantiates a new MessengerSettingsProfilePhoto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMimeType

`func (o *MessengerSettingsProfilePhoto) GetMimeType() string`

GetMimeType returns the MimeType field if non-nil, zero value otherwise.

### GetMimeTypeOk

`func (o *MessengerSettingsProfilePhoto) GetMimeTypeOk() (*string, bool)`

GetMimeTypeOk returns a tuple with the MimeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMimeType

`func (o *MessengerSettingsProfilePhoto) SetMimeType(v string)`

SetMimeType sets MimeType field to given value.


### GetBinary

`func (o *MessengerSettingsProfilePhoto) GetBinary() string`

GetBinary returns the Binary field if non-nil, zero value otherwise.

### GetBinaryOk

`func (o *MessengerSettingsProfilePhoto) GetBinaryOk() (*string, bool)`

GetBinaryOk returns a tuple with the Binary field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBinary

`func (o *MessengerSettingsProfilePhoto) SetBinary(v string)`

SetBinary sets Binary field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


