# Gateway

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Name** | **string** |  | 
**Maid** | **string** |  | 
**Namespace** | Pointer to **string** |  | [optional] 
**Type** | [**Messenger**](Messenger.md) |  | 
**Provider** | [**Provider**](Provider.md) |  | 
**Customer** | [**Customer**](Customer.md) |  | 
**Organization** | Pointer to [**Organization**](Organization.md) |  | [optional] 
**Channel** | [**Channel**](Channel.md) |  | 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 
**Draft** | **bool** |  | 
**Active** | **bool** |  | 
**ContextualHelp** | Pointer to [**GatewayContextualHelp**](GatewayContextualHelp.md) |  | [optional] 

## Methods

### NewGateway

`func NewGateway(id string, name string, maid string, type_ Messenger, provider Provider, customer Customer, channel Channel, createdAt time.Time, updatedAt time.Time, draft bool, active bool, ) *Gateway`

NewGateway instantiates a new Gateway object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGatewayWithDefaults

`func NewGatewayWithDefaults() *Gateway`

NewGatewayWithDefaults instantiates a new Gateway object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Gateway) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Gateway) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Gateway) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *Gateway) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Gateway) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Gateway) SetName(v string)`

SetName sets Name field to given value.


### GetMaid

`func (o *Gateway) GetMaid() string`

GetMaid returns the Maid field if non-nil, zero value otherwise.

### GetMaidOk

`func (o *Gateway) GetMaidOk() (*string, bool)`

GetMaidOk returns a tuple with the Maid field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaid

`func (o *Gateway) SetMaid(v string)`

SetMaid sets Maid field to given value.


### GetNamespace

`func (o *Gateway) GetNamespace() string`

GetNamespace returns the Namespace field if non-nil, zero value otherwise.

### GetNamespaceOk

`func (o *Gateway) GetNamespaceOk() (*string, bool)`

GetNamespaceOk returns a tuple with the Namespace field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNamespace

`func (o *Gateway) SetNamespace(v string)`

SetNamespace sets Namespace field to given value.

### HasNamespace

`func (o *Gateway) HasNamespace() bool`

HasNamespace returns a boolean if a field has been set.

### GetType

`func (o *Gateway) GetType() Messenger`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Gateway) GetTypeOk() (*Messenger, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Gateway) SetType(v Messenger)`

SetType sets Type field to given value.


### GetProvider

`func (o *Gateway) GetProvider() Provider`

GetProvider returns the Provider field if non-nil, zero value otherwise.

### GetProviderOk

`func (o *Gateway) GetProviderOk() (*Provider, bool)`

GetProviderOk returns a tuple with the Provider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProvider

`func (o *Gateway) SetProvider(v Provider)`

SetProvider sets Provider field to given value.


### GetCustomer

`func (o *Gateway) GetCustomer() Customer`

GetCustomer returns the Customer field if non-nil, zero value otherwise.

### GetCustomerOk

`func (o *Gateway) GetCustomerOk() (*Customer, bool)`

GetCustomerOk returns a tuple with the Customer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomer

`func (o *Gateway) SetCustomer(v Customer)`

SetCustomer sets Customer field to given value.


### GetOrganization

`func (o *Gateway) GetOrganization() Organization`

GetOrganization returns the Organization field if non-nil, zero value otherwise.

### GetOrganizationOk

`func (o *Gateway) GetOrganizationOk() (*Organization, bool)`

GetOrganizationOk returns a tuple with the Organization field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganization

`func (o *Gateway) SetOrganization(v Organization)`

SetOrganization sets Organization field to given value.

### HasOrganization

`func (o *Gateway) HasOrganization() bool`

HasOrganization returns a boolean if a field has been set.

### GetChannel

`func (o *Gateway) GetChannel() Channel`

GetChannel returns the Channel field if non-nil, zero value otherwise.

### GetChannelOk

`func (o *Gateway) GetChannelOk() (*Channel, bool)`

GetChannelOk returns a tuple with the Channel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannel

`func (o *Gateway) SetChannel(v Channel)`

SetChannel sets Channel field to given value.


### GetCreatedAt

`func (o *Gateway) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Gateway) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Gateway) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *Gateway) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Gateway) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Gateway) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetDraft

`func (o *Gateway) GetDraft() bool`

GetDraft returns the Draft field if non-nil, zero value otherwise.

### GetDraftOk

`func (o *Gateway) GetDraftOk() (*bool, bool)`

GetDraftOk returns a tuple with the Draft field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDraft

`func (o *Gateway) SetDraft(v bool)`

SetDraft sets Draft field to given value.


### GetActive

`func (o *Gateway) GetActive() bool`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *Gateway) GetActiveOk() (*bool, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *Gateway) SetActive(v bool)`

SetActive sets Active field to given value.


### GetContextualHelp

`func (o *Gateway) GetContextualHelp() GatewayContextualHelp`

GetContextualHelp returns the ContextualHelp field if non-nil, zero value otherwise.

### GetContextualHelpOk

`func (o *Gateway) GetContextualHelpOk() (*GatewayContextualHelp, bool)`

GetContextualHelpOk returns a tuple with the ContextualHelp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContextualHelp

`func (o *Gateway) SetContextualHelp(v GatewayContextualHelp)`

SetContextualHelp sets ContextualHelp field to given value.

### HasContextualHelp

`func (o *Gateway) HasContextualHelp() bool`

HasContextualHelp returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


