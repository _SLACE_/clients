# TemplateMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**OrganizationId** | **string** |  | 
**GatewayId** | Pointer to **string** |  | [optional] 
**Type** | [**TemplateType**](TemplateType.md) |  | 
**Visibility** | [**TemplateVisibility**](TemplateVisibility.md) |  | 
**Language** | **string** |  | 
**Internal** | Pointer to [**InternalTemplate**](InternalTemplate.md) |  | [optional] 
**Whatsapp** | Pointer to [**WhatsAppTemplate**](WhatsAppTemplate.md) |  | [optional] 
**WhatsappDefaults** | Pointer to [**WhatsAppTemplateDefaults**](WhatsAppTemplateDefaults.md) |  | [optional] 

## Methods

### NewTemplateMutable

`func NewTemplateMutable(name string, organizationId string, type_ TemplateType, visibility TemplateVisibility, language string, ) *TemplateMutable`

NewTemplateMutable instantiates a new TemplateMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTemplateMutableWithDefaults

`func NewTemplateMutableWithDefaults() *TemplateMutable`

NewTemplateMutableWithDefaults instantiates a new TemplateMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *TemplateMutable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *TemplateMutable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *TemplateMutable) SetName(v string)`

SetName sets Name field to given value.


### GetOrganizationId

`func (o *TemplateMutable) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *TemplateMutable) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *TemplateMutable) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetGatewayId

`func (o *TemplateMutable) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *TemplateMutable) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *TemplateMutable) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.

### HasGatewayId

`func (o *TemplateMutable) HasGatewayId() bool`

HasGatewayId returns a boolean if a field has been set.

### GetType

`func (o *TemplateMutable) GetType() TemplateType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *TemplateMutable) GetTypeOk() (*TemplateType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *TemplateMutable) SetType(v TemplateType)`

SetType sets Type field to given value.


### GetVisibility

`func (o *TemplateMutable) GetVisibility() TemplateVisibility`

GetVisibility returns the Visibility field if non-nil, zero value otherwise.

### GetVisibilityOk

`func (o *TemplateMutable) GetVisibilityOk() (*TemplateVisibility, bool)`

GetVisibilityOk returns a tuple with the Visibility field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVisibility

`func (o *TemplateMutable) SetVisibility(v TemplateVisibility)`

SetVisibility sets Visibility field to given value.


### GetLanguage

`func (o *TemplateMutable) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *TemplateMutable) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *TemplateMutable) SetLanguage(v string)`

SetLanguage sets Language field to given value.


### GetInternal

`func (o *TemplateMutable) GetInternal() InternalTemplate`

GetInternal returns the Internal field if non-nil, zero value otherwise.

### GetInternalOk

`func (o *TemplateMutable) GetInternalOk() (*InternalTemplate, bool)`

GetInternalOk returns a tuple with the Internal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInternal

`func (o *TemplateMutable) SetInternal(v InternalTemplate)`

SetInternal sets Internal field to given value.

### HasInternal

`func (o *TemplateMutable) HasInternal() bool`

HasInternal returns a boolean if a field has been set.

### GetWhatsapp

`func (o *TemplateMutable) GetWhatsapp() WhatsAppTemplate`

GetWhatsapp returns the Whatsapp field if non-nil, zero value otherwise.

### GetWhatsappOk

`func (o *TemplateMutable) GetWhatsappOk() (*WhatsAppTemplate, bool)`

GetWhatsappOk returns a tuple with the Whatsapp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWhatsapp

`func (o *TemplateMutable) SetWhatsapp(v WhatsAppTemplate)`

SetWhatsapp sets Whatsapp field to given value.

### HasWhatsapp

`func (o *TemplateMutable) HasWhatsapp() bool`

HasWhatsapp returns a boolean if a field has been set.

### GetWhatsappDefaults

`func (o *TemplateMutable) GetWhatsappDefaults() WhatsAppTemplateDefaults`

GetWhatsappDefaults returns the WhatsappDefaults field if non-nil, zero value otherwise.

### GetWhatsappDefaultsOk

`func (o *TemplateMutable) GetWhatsappDefaultsOk() (*WhatsAppTemplateDefaults, bool)`

GetWhatsappDefaultsOk returns a tuple with the WhatsappDefaults field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWhatsappDefaults

`func (o *TemplateMutable) SetWhatsappDefaults(v WhatsAppTemplateDefaults)`

SetWhatsappDefaults sets WhatsappDefaults field to given value.

### HasWhatsappDefaults

`func (o *TemplateMutable) HasWhatsappDefaults() bool`

HasWhatsappDefaults returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


