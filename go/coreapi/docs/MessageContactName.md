# MessageContactName

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FormattedName** | Pointer to **string** |  | [optional] 
**FirstName** | Pointer to **string** |  | [optional] 
**LastName** | Pointer to **string** |  | [optional] 
**AdditionalName** | Pointer to **string** |  | [optional] 
**Prefix** | Pointer to **string** |  | [optional] 
**Suffix** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageContactName

`func NewMessageContactName() *MessageContactName`

NewMessageContactName instantiates a new MessageContactName object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageContactNameWithDefaults

`func NewMessageContactNameWithDefaults() *MessageContactName`

NewMessageContactNameWithDefaults instantiates a new MessageContactName object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFormattedName

`func (o *MessageContactName) GetFormattedName() string`

GetFormattedName returns the FormattedName field if non-nil, zero value otherwise.

### GetFormattedNameOk

`func (o *MessageContactName) GetFormattedNameOk() (*string, bool)`

GetFormattedNameOk returns a tuple with the FormattedName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormattedName

`func (o *MessageContactName) SetFormattedName(v string)`

SetFormattedName sets FormattedName field to given value.

### HasFormattedName

`func (o *MessageContactName) HasFormattedName() bool`

HasFormattedName returns a boolean if a field has been set.

### GetFirstName

`func (o *MessageContactName) GetFirstName() string`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *MessageContactName) GetFirstNameOk() (*string, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *MessageContactName) SetFirstName(v string)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *MessageContactName) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *MessageContactName) GetLastName() string`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *MessageContactName) GetLastNameOk() (*string, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *MessageContactName) SetLastName(v string)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *MessageContactName) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetAdditionalName

`func (o *MessageContactName) GetAdditionalName() string`

GetAdditionalName returns the AdditionalName field if non-nil, zero value otherwise.

### GetAdditionalNameOk

`func (o *MessageContactName) GetAdditionalNameOk() (*string, bool)`

GetAdditionalNameOk returns a tuple with the AdditionalName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalName

`func (o *MessageContactName) SetAdditionalName(v string)`

SetAdditionalName sets AdditionalName field to given value.

### HasAdditionalName

`func (o *MessageContactName) HasAdditionalName() bool`

HasAdditionalName returns a boolean if a field has been set.

### GetPrefix

`func (o *MessageContactName) GetPrefix() string`

GetPrefix returns the Prefix field if non-nil, zero value otherwise.

### GetPrefixOk

`func (o *MessageContactName) GetPrefixOk() (*string, bool)`

GetPrefixOk returns a tuple with the Prefix field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrefix

`func (o *MessageContactName) SetPrefix(v string)`

SetPrefix sets Prefix field to given value.

### HasPrefix

`func (o *MessageContactName) HasPrefix() bool`

HasPrefix returns a boolean if a field has been set.

### GetSuffix

`func (o *MessageContactName) GetSuffix() string`

GetSuffix returns the Suffix field if non-nil, zero value otherwise.

### GetSuffixOk

`func (o *MessageContactName) GetSuffixOk() (*string, bool)`

GetSuffixOk returns a tuple with the Suffix field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSuffix

`func (o *MessageContactName) SetSuffix(v string)`

SetSuffix sets Suffix field to given value.

### HasSuffix

`func (o *MessageContactName) HasSuffix() bool`

HasSuffix returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


