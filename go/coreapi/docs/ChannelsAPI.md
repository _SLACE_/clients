# \ChannelsAPI

All URIs are relative to *https://api.slace.com/core*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateChannel**](ChannelsAPI.md#CreateChannel) | **Post** /channels | Create channel
[**CreateChannelTag**](ChannelsAPI.md#CreateChannelTag) | **Post** /channels/{channel_id}/tags | Create channel tag
[**DeleteChannel**](ChannelsAPI.md#DeleteChannel) | **Delete** /channels/{channel_id} | Delete channel
[**DeleteChannelsChannelIdTags**](ChannelsAPI.md#DeleteChannelsChannelIdTags) | **Delete** /channels/{channel_id}/tags/{tag_id} | Delete channel tag
[**GetChannel**](ChannelsAPI.md#GetChannel) | **Get** /channels/{channel_id} | Get channel
[**GetChannelTags**](ChannelsAPI.md#GetChannelTags) | **Get** /channels/{channel_id}/tags | Get channel tags
[**ListChannels**](ChannelsAPI.md#ListChannels) | **Get** /channels | List channels
[**UpdateChannel**](ChannelsAPI.md#UpdateChannel) | **Put** /channels/{channel_id} | Update channel



## CreateChannel

> Channel CreateChannel(ctx).ChannelMutable(channelMutable).Execute()

Create channel



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelMutable := *openapiclient.NewChannelMutable("OrganizationId_example", "Name_example", []string{"StopKeywords_example"}) // ChannelMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ChannelsAPI.CreateChannel(context.Background()).ChannelMutable(channelMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ChannelsAPI.CreateChannel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateChannel`: Channel
    fmt.Fprintf(os.Stdout, "Response from `ChannelsAPI.CreateChannel`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateChannelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelMutable** | [**ChannelMutable**](ChannelMutable.md) |  | 

### Return type

[**Channel**](Channel.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateChannelTag

> CreateChannelTag(ctx, channelId).CreateChannelTagRequest(createChannelTagRequest).Execute()

Create channel tag



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 
    createChannelTagRequest := *openapiclient.NewCreateChannelTagRequest() // CreateChannelTagRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ChannelsAPI.CreateChannelTag(context.Background(), channelId).CreateChannelTagRequest(createChannelTagRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ChannelsAPI.CreateChannelTag``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateChannelTagRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **createChannelTagRequest** | [**CreateChannelTagRequest**](CreateChannelTagRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json, application/xml
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteChannel

> DeleteChannel(ctx, channelId).Execute()

Delete channel



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ChannelsAPI.DeleteChannel(context.Background(), channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ChannelsAPI.DeleteChannel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteChannelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteChannelsChannelIdTags

> DeleteChannelsChannelIdTags(ctx, channelId, tagId).Execute()

Delete channel tag

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 
    tagId := "tagId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ChannelsAPI.DeleteChannelsChannelIdTags(context.Background(), channelId, tagId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ChannelsAPI.DeleteChannelsChannelIdTags``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**tagId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteChannelsChannelIdTagsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChannel

> Channel GetChannel(ctx, channelId).Execute()

Get channel



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ChannelsAPI.GetChannel(context.Background(), channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ChannelsAPI.GetChannel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChannel`: Channel
    fmt.Fprintf(os.Stdout, "Response from `ChannelsAPI.GetChannel`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChannelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Channel**](Channel.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChannelTags

> GetChannelTags200Response GetChannelTags(ctx, channelId).Execute()

Get channel tags



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ChannelsAPI.GetChannelTags(context.Background(), channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ChannelsAPI.GetChannelTags``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChannelTags`: GetChannelTags200Response
    fmt.Fprintf(os.Stdout, "Response from `ChannelsAPI.GetChannelTags`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChannelTagsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**GetChannelTags200Response**](GetChannelTags200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListChannels

> ChannelsList ListChannels(ctx).CustomerId(customerId).OrganizationId(organizationId).Search(search).Tags(tags).Sort(sort).Limit(limit).Offset(offset).Execute()

List channels



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    customerId := "customerId_example" // string |  (optional)
    organizationId := "organizationId_example" // string |  (optional)
    search := "search_example" // string |  (optional)
    tags := []string{"Inner_example"} // []string |  (optional)
    sort := "sort_example" // string |  (optional)
    limit := int32(56) // int32 |  (optional)
    offset := int32(56) // int32 |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ChannelsAPI.ListChannels(context.Background()).CustomerId(customerId).OrganizationId(organizationId).Search(search).Tags(tags).Sort(sort).Limit(limit).Offset(offset).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ChannelsAPI.ListChannels``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListChannels`: ChannelsList
    fmt.Fprintf(os.Stdout, "Response from `ChannelsAPI.ListChannels`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListChannelsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerId** | **string** |  | 
 **organizationId** | **string** |  | 
 **search** | **string** |  | 
 **tags** | **[]string** |  | 
 **sort** | **string** |  | 
 **limit** | **int32** |  | 
 **offset** | **int32** |  | 

### Return type

[**ChannelsList**](ChannelsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateChannel

> Channel UpdateChannel(ctx, channelId).ChannelMutable(channelMutable).Execute()

Update channel



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 
    channelMutable := *openapiclient.NewChannelMutable("OrganizationId_example", "Name_example", []string{"StopKeywords_example"}) // ChannelMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ChannelsAPI.UpdateChannel(context.Background(), channelId).ChannelMutable(channelMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ChannelsAPI.UpdateChannel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateChannel`: Channel
    fmt.Fprintf(os.Stdout, "Response from `ChannelsAPI.UpdateChannel`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateChannelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **channelMutable** | [**ChannelMutable**](ChannelMutable.md) |  | 

### Return type

[**Channel**](Channel.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

