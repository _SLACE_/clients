# CampaignSendout

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [readonly] 
**CampaignId** | **string** |  | [readonly] 
**SendAt** | **time.Time** |  | 
**Status** | **string** |  | [readonly] 
**SentContactIds** | Pointer to **[]string** |  | [optional] 
**Split** | Pointer to [**[]CampaignSendoutSplitInner**](CampaignSendoutSplitInner.md) |  | [optional] 
**RescheduledToId** | Pointer to **string** |  | [optional] [readonly] 
**RescheduledToTime** | Pointer to **time.Time** |  | [optional] [readonly] 
**RescheduledFromId** | Pointer to **string** |  | [optional] [readonly] 
**RescheduledFromTime** | Pointer to **time.Time** |  | [optional] [readonly] 
**Unsaved** | Pointer to **bool** |  | [optional] [readonly] 
**CreatedAt** | **time.Time** |  | [readonly] 
**UpdatedAt** | **time.Time** |  | [readonly] 

## Methods

### NewCampaignSendout

`func NewCampaignSendout(id string, campaignId string, sendAt time.Time, status string, createdAt time.Time, updatedAt time.Time, ) *CampaignSendout`

NewCampaignSendout instantiates a new CampaignSendout object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignSendoutWithDefaults

`func NewCampaignSendoutWithDefaults() *CampaignSendout`

NewCampaignSendoutWithDefaults instantiates a new CampaignSendout object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CampaignSendout) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CampaignSendout) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CampaignSendout) SetId(v string)`

SetId sets Id field to given value.


### GetCampaignId

`func (o *CampaignSendout) GetCampaignId() string`

GetCampaignId returns the CampaignId field if non-nil, zero value otherwise.

### GetCampaignIdOk

`func (o *CampaignSendout) GetCampaignIdOk() (*string, bool)`

GetCampaignIdOk returns a tuple with the CampaignId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCampaignId

`func (o *CampaignSendout) SetCampaignId(v string)`

SetCampaignId sets CampaignId field to given value.


### GetSendAt

`func (o *CampaignSendout) GetSendAt() time.Time`

GetSendAt returns the SendAt field if non-nil, zero value otherwise.

### GetSendAtOk

`func (o *CampaignSendout) GetSendAtOk() (*time.Time, bool)`

GetSendAtOk returns a tuple with the SendAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSendAt

`func (o *CampaignSendout) SetSendAt(v time.Time)`

SetSendAt sets SendAt field to given value.


### GetStatus

`func (o *CampaignSendout) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *CampaignSendout) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *CampaignSendout) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetSentContactIds

`func (o *CampaignSendout) GetSentContactIds() []string`

GetSentContactIds returns the SentContactIds field if non-nil, zero value otherwise.

### GetSentContactIdsOk

`func (o *CampaignSendout) GetSentContactIdsOk() (*[]string, bool)`

GetSentContactIdsOk returns a tuple with the SentContactIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSentContactIds

`func (o *CampaignSendout) SetSentContactIds(v []string)`

SetSentContactIds sets SentContactIds field to given value.

### HasSentContactIds

`func (o *CampaignSendout) HasSentContactIds() bool`

HasSentContactIds returns a boolean if a field has been set.

### GetSplit

`func (o *CampaignSendout) GetSplit() []CampaignSendoutSplitInner`

GetSplit returns the Split field if non-nil, zero value otherwise.

### GetSplitOk

`func (o *CampaignSendout) GetSplitOk() (*[]CampaignSendoutSplitInner, bool)`

GetSplitOk returns a tuple with the Split field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSplit

`func (o *CampaignSendout) SetSplit(v []CampaignSendoutSplitInner)`

SetSplit sets Split field to given value.

### HasSplit

`func (o *CampaignSendout) HasSplit() bool`

HasSplit returns a boolean if a field has been set.

### GetRescheduledToId

`func (o *CampaignSendout) GetRescheduledToId() string`

GetRescheduledToId returns the RescheduledToId field if non-nil, zero value otherwise.

### GetRescheduledToIdOk

`func (o *CampaignSendout) GetRescheduledToIdOk() (*string, bool)`

GetRescheduledToIdOk returns a tuple with the RescheduledToId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRescheduledToId

`func (o *CampaignSendout) SetRescheduledToId(v string)`

SetRescheduledToId sets RescheduledToId field to given value.

### HasRescheduledToId

`func (o *CampaignSendout) HasRescheduledToId() bool`

HasRescheduledToId returns a boolean if a field has been set.

### GetRescheduledToTime

`func (o *CampaignSendout) GetRescheduledToTime() time.Time`

GetRescheduledToTime returns the RescheduledToTime field if non-nil, zero value otherwise.

### GetRescheduledToTimeOk

`func (o *CampaignSendout) GetRescheduledToTimeOk() (*time.Time, bool)`

GetRescheduledToTimeOk returns a tuple with the RescheduledToTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRescheduledToTime

`func (o *CampaignSendout) SetRescheduledToTime(v time.Time)`

SetRescheduledToTime sets RescheduledToTime field to given value.

### HasRescheduledToTime

`func (o *CampaignSendout) HasRescheduledToTime() bool`

HasRescheduledToTime returns a boolean if a field has been set.

### GetRescheduledFromId

`func (o *CampaignSendout) GetRescheduledFromId() string`

GetRescheduledFromId returns the RescheduledFromId field if non-nil, zero value otherwise.

### GetRescheduledFromIdOk

`func (o *CampaignSendout) GetRescheduledFromIdOk() (*string, bool)`

GetRescheduledFromIdOk returns a tuple with the RescheduledFromId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRescheduledFromId

`func (o *CampaignSendout) SetRescheduledFromId(v string)`

SetRescheduledFromId sets RescheduledFromId field to given value.

### HasRescheduledFromId

`func (o *CampaignSendout) HasRescheduledFromId() bool`

HasRescheduledFromId returns a boolean if a field has been set.

### GetRescheduledFromTime

`func (o *CampaignSendout) GetRescheduledFromTime() time.Time`

GetRescheduledFromTime returns the RescheduledFromTime field if non-nil, zero value otherwise.

### GetRescheduledFromTimeOk

`func (o *CampaignSendout) GetRescheduledFromTimeOk() (*time.Time, bool)`

GetRescheduledFromTimeOk returns a tuple with the RescheduledFromTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRescheduledFromTime

`func (o *CampaignSendout) SetRescheduledFromTime(v time.Time)`

SetRescheduledFromTime sets RescheduledFromTime field to given value.

### HasRescheduledFromTime

`func (o *CampaignSendout) HasRescheduledFromTime() bool`

HasRescheduledFromTime returns a boolean if a field has been set.

### GetUnsaved

`func (o *CampaignSendout) GetUnsaved() bool`

GetUnsaved returns the Unsaved field if non-nil, zero value otherwise.

### GetUnsavedOk

`func (o *CampaignSendout) GetUnsavedOk() (*bool, bool)`

GetUnsavedOk returns a tuple with the Unsaved field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnsaved

`func (o *CampaignSendout) SetUnsaved(v bool)`

SetUnsaved sets Unsaved field to given value.

### HasUnsaved

`func (o *CampaignSendout) HasUnsaved() bool`

HasUnsaved returns a boolean if a field has been set.

### GetCreatedAt

`func (o *CampaignSendout) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *CampaignSendout) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *CampaignSendout) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *CampaignSendout) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *CampaignSendout) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *CampaignSendout) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


