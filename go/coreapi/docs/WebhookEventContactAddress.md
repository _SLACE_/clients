# WebhookEventContactAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Street** | Pointer to **string** |  | [optional] 
**StreetNumber** | Pointer to **string** |  | [optional] 
**DoorNumber** | Pointer to **string** |  | [optional] 
**City** | Pointer to **string** |  | [optional] 
**Zip** | Pointer to **string** |  | [optional] 
**Country** | Pointer to **string** |  | [optional] 

## Methods

### NewWebhookEventContactAddress

`func NewWebhookEventContactAddress() *WebhookEventContactAddress`

NewWebhookEventContactAddress instantiates a new WebhookEventContactAddress object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookEventContactAddressWithDefaults

`func NewWebhookEventContactAddressWithDefaults() *WebhookEventContactAddress`

NewWebhookEventContactAddressWithDefaults instantiates a new WebhookEventContactAddress object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStreet

`func (o *WebhookEventContactAddress) GetStreet() string`

GetStreet returns the Street field if non-nil, zero value otherwise.

### GetStreetOk

`func (o *WebhookEventContactAddress) GetStreetOk() (*string, bool)`

GetStreetOk returns a tuple with the Street field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreet

`func (o *WebhookEventContactAddress) SetStreet(v string)`

SetStreet sets Street field to given value.

### HasStreet

`func (o *WebhookEventContactAddress) HasStreet() bool`

HasStreet returns a boolean if a field has been set.

### GetStreetNumber

`func (o *WebhookEventContactAddress) GetStreetNumber() string`

GetStreetNumber returns the StreetNumber field if non-nil, zero value otherwise.

### GetStreetNumberOk

`func (o *WebhookEventContactAddress) GetStreetNumberOk() (*string, bool)`

GetStreetNumberOk returns a tuple with the StreetNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreetNumber

`func (o *WebhookEventContactAddress) SetStreetNumber(v string)`

SetStreetNumber sets StreetNumber field to given value.

### HasStreetNumber

`func (o *WebhookEventContactAddress) HasStreetNumber() bool`

HasStreetNumber returns a boolean if a field has been set.

### GetDoorNumber

`func (o *WebhookEventContactAddress) GetDoorNumber() string`

GetDoorNumber returns the DoorNumber field if non-nil, zero value otherwise.

### GetDoorNumberOk

`func (o *WebhookEventContactAddress) GetDoorNumberOk() (*string, bool)`

GetDoorNumberOk returns a tuple with the DoorNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDoorNumber

`func (o *WebhookEventContactAddress) SetDoorNumber(v string)`

SetDoorNumber sets DoorNumber field to given value.

### HasDoorNumber

`func (o *WebhookEventContactAddress) HasDoorNumber() bool`

HasDoorNumber returns a boolean if a field has been set.

### GetCity

`func (o *WebhookEventContactAddress) GetCity() string`

GetCity returns the City field if non-nil, zero value otherwise.

### GetCityOk

`func (o *WebhookEventContactAddress) GetCityOk() (*string, bool)`

GetCityOk returns a tuple with the City field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCity

`func (o *WebhookEventContactAddress) SetCity(v string)`

SetCity sets City field to given value.

### HasCity

`func (o *WebhookEventContactAddress) HasCity() bool`

HasCity returns a boolean if a field has been set.

### GetZip

`func (o *WebhookEventContactAddress) GetZip() string`

GetZip returns the Zip field if non-nil, zero value otherwise.

### GetZipOk

`func (o *WebhookEventContactAddress) GetZipOk() (*string, bool)`

GetZipOk returns a tuple with the Zip field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetZip

`func (o *WebhookEventContactAddress) SetZip(v string)`

SetZip sets Zip field to given value.

### HasZip

`func (o *WebhookEventContactAddress) HasZip() bool`

HasZip returns a boolean if a field has been set.

### GetCountry

`func (o *WebhookEventContactAddress) GetCountry() string`

GetCountry returns the Country field if non-nil, zero value otherwise.

### GetCountryOk

`func (o *WebhookEventContactAddress) GetCountryOk() (*string, bool)`

GetCountryOk returns a tuple with the Country field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountry

`func (o *WebhookEventContactAddress) SetCountry(v string)`

SetCountry sets Country field to given value.

### HasCountry

`func (o *WebhookEventContactAddress) HasCountry() bool`

HasCountry returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


