# WhatsAppTemplateMessageComponent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**SubType** | Pointer to **string** | For buttons only | [optional] 
**Index** | Pointer to **string** | For buttons only | [optional] 
**Parameters** | Pointer to [**[]WhatsAppTemplateMessageComponentParametersInner**](WhatsAppTemplateMessageComponentParametersInner.md) |  | [optional] 
**Cards** | Pointer to [**[]WhatsAppTemplateMessageCard**](WhatsAppTemplateMessageCard.md) |  | [optional] 

## Methods

### NewWhatsAppTemplateMessageComponent

`func NewWhatsAppTemplateMessageComponent(type_ string, ) *WhatsAppTemplateMessageComponent`

NewWhatsAppTemplateMessageComponent instantiates a new WhatsAppTemplateMessageComponent object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMessageComponentWithDefaults

`func NewWhatsAppTemplateMessageComponentWithDefaults() *WhatsAppTemplateMessageComponent`

NewWhatsAppTemplateMessageComponentWithDefaults instantiates a new WhatsAppTemplateMessageComponent object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *WhatsAppTemplateMessageComponent) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WhatsAppTemplateMessageComponent) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WhatsAppTemplateMessageComponent) SetType(v string)`

SetType sets Type field to given value.


### GetSubType

`func (o *WhatsAppTemplateMessageComponent) GetSubType() string`

GetSubType returns the SubType field if non-nil, zero value otherwise.

### GetSubTypeOk

`func (o *WhatsAppTemplateMessageComponent) GetSubTypeOk() (*string, bool)`

GetSubTypeOk returns a tuple with the SubType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSubType

`func (o *WhatsAppTemplateMessageComponent) SetSubType(v string)`

SetSubType sets SubType field to given value.

### HasSubType

`func (o *WhatsAppTemplateMessageComponent) HasSubType() bool`

HasSubType returns a boolean if a field has been set.

### GetIndex

`func (o *WhatsAppTemplateMessageComponent) GetIndex() string`

GetIndex returns the Index field if non-nil, zero value otherwise.

### GetIndexOk

`func (o *WhatsAppTemplateMessageComponent) GetIndexOk() (*string, bool)`

GetIndexOk returns a tuple with the Index field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIndex

`func (o *WhatsAppTemplateMessageComponent) SetIndex(v string)`

SetIndex sets Index field to given value.

### HasIndex

`func (o *WhatsAppTemplateMessageComponent) HasIndex() bool`

HasIndex returns a boolean if a field has been set.

### GetParameters

`func (o *WhatsAppTemplateMessageComponent) GetParameters() []WhatsAppTemplateMessageComponentParametersInner`

GetParameters returns the Parameters field if non-nil, zero value otherwise.

### GetParametersOk

`func (o *WhatsAppTemplateMessageComponent) GetParametersOk() (*[]WhatsAppTemplateMessageComponentParametersInner, bool)`

GetParametersOk returns a tuple with the Parameters field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParameters

`func (o *WhatsAppTemplateMessageComponent) SetParameters(v []WhatsAppTemplateMessageComponentParametersInner)`

SetParameters sets Parameters field to given value.

### HasParameters

`func (o *WhatsAppTemplateMessageComponent) HasParameters() bool`

HasParameters returns a boolean if a field has been set.

### GetCards

`func (o *WhatsAppTemplateMessageComponent) GetCards() []WhatsAppTemplateMessageCard`

GetCards returns the Cards field if non-nil, zero value otherwise.

### GetCardsOk

`func (o *WhatsAppTemplateMessageComponent) GetCardsOk() (*[]WhatsAppTemplateMessageCard, bool)`

GetCardsOk returns a tuple with the Cards field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCards

`func (o *WhatsAppTemplateMessageComponent) SetCards(v []WhatsAppTemplateMessageCard)`

SetCards sets Cards field to given value.

### HasCards

`func (o *WhatsAppTemplateMessageComponent) HasCards() bool`

HasCards returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


