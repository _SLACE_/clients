# ContactCreatableLanguages

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Primary** | Pointer to **string** |  | [optional] 
**Additional** | Pointer to **map[string]interface{}** |  | [optional] 

## Methods

### NewContactCreatableLanguages

`func NewContactCreatableLanguages() *ContactCreatableLanguages`

NewContactCreatableLanguages instantiates a new ContactCreatableLanguages object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactCreatableLanguagesWithDefaults

`func NewContactCreatableLanguagesWithDefaults() *ContactCreatableLanguages`

NewContactCreatableLanguagesWithDefaults instantiates a new ContactCreatableLanguages object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPrimary

`func (o *ContactCreatableLanguages) GetPrimary() string`

GetPrimary returns the Primary field if non-nil, zero value otherwise.

### GetPrimaryOk

`func (o *ContactCreatableLanguages) GetPrimaryOk() (*string, bool)`

GetPrimaryOk returns a tuple with the Primary field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrimary

`func (o *ContactCreatableLanguages) SetPrimary(v string)`

SetPrimary sets Primary field to given value.

### HasPrimary

`func (o *ContactCreatableLanguages) HasPrimary() bool`

HasPrimary returns a boolean if a field has been set.

### GetAdditional

`func (o *ContactCreatableLanguages) GetAdditional() map[string]interface{}`

GetAdditional returns the Additional field if non-nil, zero value otherwise.

### GetAdditionalOk

`func (o *ContactCreatableLanguages) GetAdditionalOk() (*map[string]interface{}, bool)`

GetAdditionalOk returns a tuple with the Additional field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditional

`func (o *ContactCreatableLanguages) SetAdditional(v map[string]interface{})`

SetAdditional sets Additional field to given value.

### HasAdditional

`func (o *ContactCreatableLanguages) HasAdditional() bool`

HasAdditional returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


