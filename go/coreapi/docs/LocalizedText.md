# LocalizedText

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Locale** | Pointer to **string** |  | [optional] 
**Text** | Pointer to **string** |  | [optional] 

## Methods

### NewLocalizedText

`func NewLocalizedText() *LocalizedText`

NewLocalizedText instantiates a new LocalizedText object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLocalizedTextWithDefaults

`func NewLocalizedTextWithDefaults() *LocalizedText`

NewLocalizedTextWithDefaults instantiates a new LocalizedText object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLocale

`func (o *LocalizedText) GetLocale() string`

GetLocale returns the Locale field if non-nil, zero value otherwise.

### GetLocaleOk

`func (o *LocalizedText) GetLocaleOk() (*string, bool)`

GetLocaleOk returns a tuple with the Locale field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocale

`func (o *LocalizedText) SetLocale(v string)`

SetLocale sets Locale field to given value.

### HasLocale

`func (o *LocalizedText) HasLocale() bool`

HasLocale returns a boolean if a field has been set.

### GetText

`func (o *LocalizedText) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *LocalizedText) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *LocalizedText) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *LocalizedText) HasText() bool`

HasText returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


