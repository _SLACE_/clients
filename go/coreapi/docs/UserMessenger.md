# UserMessenger

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Verified** | **bool** |  | 
**UserId** | **string** |  | 
**Purpose** | **string** |  | 
**MessegerId** | **string** |  | 
**MessengerType** | **string** |  | 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 
**ProfileName** | Pointer to **string** |  | [optional] 

## Methods

### NewUserMessenger

`func NewUserMessenger(id string, verified bool, userId string, purpose string, messegerId string, messengerType string, createdAt time.Time, updatedAt time.Time, ) *UserMessenger`

NewUserMessenger instantiates a new UserMessenger object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUserMessengerWithDefaults

`func NewUserMessengerWithDefaults() *UserMessenger`

NewUserMessengerWithDefaults instantiates a new UserMessenger object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *UserMessenger) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *UserMessenger) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *UserMessenger) SetId(v string)`

SetId sets Id field to given value.


### GetVerified

`func (o *UserMessenger) GetVerified() bool`

GetVerified returns the Verified field if non-nil, zero value otherwise.

### GetVerifiedOk

`func (o *UserMessenger) GetVerifiedOk() (*bool, bool)`

GetVerifiedOk returns a tuple with the Verified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVerified

`func (o *UserMessenger) SetVerified(v bool)`

SetVerified sets Verified field to given value.


### GetUserId

`func (o *UserMessenger) GetUserId() string`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *UserMessenger) GetUserIdOk() (*string, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *UserMessenger) SetUserId(v string)`

SetUserId sets UserId field to given value.


### GetPurpose

`func (o *UserMessenger) GetPurpose() string`

GetPurpose returns the Purpose field if non-nil, zero value otherwise.

### GetPurposeOk

`func (o *UserMessenger) GetPurposeOk() (*string, bool)`

GetPurposeOk returns a tuple with the Purpose field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPurpose

`func (o *UserMessenger) SetPurpose(v string)`

SetPurpose sets Purpose field to given value.


### GetMessegerId

`func (o *UserMessenger) GetMessegerId() string`

GetMessegerId returns the MessegerId field if non-nil, zero value otherwise.

### GetMessegerIdOk

`func (o *UserMessenger) GetMessegerIdOk() (*string, bool)`

GetMessegerIdOk returns a tuple with the MessegerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessegerId

`func (o *UserMessenger) SetMessegerId(v string)`

SetMessegerId sets MessegerId field to given value.


### GetMessengerType

`func (o *UserMessenger) GetMessengerType() string`

GetMessengerType returns the MessengerType field if non-nil, zero value otherwise.

### GetMessengerTypeOk

`func (o *UserMessenger) GetMessengerTypeOk() (*string, bool)`

GetMessengerTypeOk returns a tuple with the MessengerType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerType

`func (o *UserMessenger) SetMessengerType(v string)`

SetMessengerType sets MessengerType field to given value.


### GetCreatedAt

`func (o *UserMessenger) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *UserMessenger) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *UserMessenger) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *UserMessenger) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *UserMessenger) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *UserMessenger) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetProfileName

`func (o *UserMessenger) GetProfileName() string`

GetProfileName returns the ProfileName field if non-nil, zero value otherwise.

### GetProfileNameOk

`func (o *UserMessenger) GetProfileNameOk() (*string, bool)`

GetProfileNameOk returns a tuple with the ProfileName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProfileName

`func (o *UserMessenger) SetProfileName(v string)`

SetProfileName sets ProfileName field to given value.

### HasProfileName

`func (o *UserMessenger) HasProfileName() bool`

HasProfileName returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


