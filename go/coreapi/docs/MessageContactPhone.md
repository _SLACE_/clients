# MessageContactPhone

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Phone** | **string** |  | 
**Type** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageContactPhone

`func NewMessageContactPhone(phone string, ) *MessageContactPhone`

NewMessageContactPhone instantiates a new MessageContactPhone object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageContactPhoneWithDefaults

`func NewMessageContactPhoneWithDefaults() *MessageContactPhone`

NewMessageContactPhoneWithDefaults instantiates a new MessageContactPhone object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPhone

`func (o *MessageContactPhone) GetPhone() string`

GetPhone returns the Phone field if non-nil, zero value otherwise.

### GetPhoneOk

`func (o *MessageContactPhone) GetPhoneOk() (*string, bool)`

GetPhoneOk returns a tuple with the Phone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhone

`func (o *MessageContactPhone) SetPhone(v string)`

SetPhone sets Phone field to given value.


### GetType

`func (o *MessageContactPhone) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MessageContactPhone) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MessageContactPhone) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *MessageContactPhone) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


