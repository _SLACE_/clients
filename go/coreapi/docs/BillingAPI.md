# \BillingAPI

All URIs are relative to *https://api.slace.com/core*

Method | HTTP request | Description
------------- | ------------- | -------------
[**BillingReportGet**](BillingAPI.md#BillingReportGet) | **Get** /billing/report | Get billing report
[**CreateBillingAccount**](BillingAPI.md#CreateBillingAccount) | **Post** /billing/accounts | Create billing account
[**CreateBillingAccountAssignment**](BillingAPI.md#CreateBillingAccountAssignment) | **Post** /billing/account_assignments | Create billing account assignment
[**DeleteBillingAccount**](BillingAPI.md#DeleteBillingAccount) | **Delete** /billing/accounts/{account_id} | Delete billing account
[**EditBillingAccount**](BillingAPI.md#EditBillingAccount) | **Patch** /billing/accounts/{account_id} | Edit billing account
[**GetBillingAccount**](BillingAPI.md#GetBillingAccount) | **Get** /billing/accounts/{account_id} | Get billing account
[**GetBillingAccounts**](BillingAPI.md#GetBillingAccounts) | **Get** /billing/accounts | Get billing accounts
[**GetBillingAccountsAssignments**](BillingAPI.md#GetBillingAccountsAssignments) | **Get** /billing/account_assignments | Get billing accounts assignments



## BillingReportGet

> BillingItemsList BillingReportGet(ctx).OrganizationId(organizationId).Year(year).Month(month).UseExternalApi(useExternalApi).Format(format).Execute()

Get billing report

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    year := int32(56) // int32 | 
    month := int32(56) // int32 | 1-12
    useExternalApi := true // bool | Use external API to get billing data (true) or base on events calculation (default false) (optional)
    format := "format_example" // string |  (optional) (default to "json")

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.BillingReportGet(context.Background()).OrganizationId(organizationId).Year(year).Month(month).UseExternalApi(useExternalApi).Format(format).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.BillingReportGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `BillingReportGet`: BillingItemsList
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.BillingReportGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiBillingReportGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | **string** |  | 
 **year** | **int32** |  | 
 **month** | **int32** | 1-12 | 
 **useExternalApi** | **bool** | Use external API to get billing data (true) or base on events calculation (default false) | 
 **format** | **string** |  | [default to &quot;json&quot;]

### Return type

[**BillingItemsList**](BillingItemsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, text/csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateBillingAccount

> BillingAccount CreateBillingAccount(ctx).BillingAccountMutable(billingAccountMutable).Execute()

Create billing account

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    billingAccountMutable := *openapiclient.NewBillingAccountMutable("Name_example") // BillingAccountMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.CreateBillingAccount(context.Background()).BillingAccountMutable(billingAccountMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.CreateBillingAccount``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateBillingAccount`: BillingAccount
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.CreateBillingAccount`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateBillingAccountRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **billingAccountMutable** | [**BillingAccountMutable**](BillingAccountMutable.md) |  | 

### Return type

[**BillingAccount**](BillingAccount.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateBillingAccountAssignment

> BillingAccountAssignment CreateBillingAccountAssignment(ctx).BillingAccountAssignment(billingAccountAssignment).Execute()

Create billing account assignment

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    billingAccountAssignment := *openapiclient.NewBillingAccountAssignment("AccountId_example") // BillingAccountAssignment |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.CreateBillingAccountAssignment(context.Background()).BillingAccountAssignment(billingAccountAssignment).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.CreateBillingAccountAssignment``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateBillingAccountAssignment`: BillingAccountAssignment
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.CreateBillingAccountAssignment`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateBillingAccountAssignmentRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **billingAccountAssignment** | [**BillingAccountAssignment**](BillingAccountAssignment.md) |  | 

### Return type

[**BillingAccountAssignment**](BillingAccountAssignment.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteBillingAccount

> DeleteBillingAccount(ctx, accountId).Execute()

Delete billing account

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    accountId := "accountId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.BillingAPI.DeleteBillingAccount(context.Background(), accountId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.DeleteBillingAccount``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteBillingAccountRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## EditBillingAccount

> BillingAccount EditBillingAccount(ctx, accountId).BillingAccountMutable(billingAccountMutable).Execute()

Edit billing account

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    accountId := "accountId_example" // string | 
    billingAccountMutable := *openapiclient.NewBillingAccountMutable("Name_example") // BillingAccountMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.EditBillingAccount(context.Background(), accountId).BillingAccountMutable(billingAccountMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.EditBillingAccount``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `EditBillingAccount`: BillingAccount
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.EditBillingAccount`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiEditBillingAccountRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **billingAccountMutable** | [**BillingAccountMutable**](BillingAccountMutable.md) |  | 

### Return type

[**BillingAccount**](BillingAccount.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetBillingAccount

> BillingAccount GetBillingAccount(ctx, accountId).Execute()

Get billing account

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    accountId := "accountId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.GetBillingAccount(context.Background(), accountId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.GetBillingAccount``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBillingAccount`: BillingAccount
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.GetBillingAccount`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetBillingAccountRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**BillingAccount**](BillingAccount.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetBillingAccounts

> BillingAccountsList GetBillingAccounts(ctx).Name(name).Offset(offset).Limit(limit).Sort(sort).Execute()

Get billing accounts

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    name := "name_example" // string |  (optional)
    offset := "offset_example" // string |  (optional)
    limit := "limit_example" // string |  (optional)
    sort := "sort_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.GetBillingAccounts(context.Background()).Name(name).Offset(offset).Limit(limit).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.GetBillingAccounts``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBillingAccounts`: BillingAccountsList
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.GetBillingAccounts`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetBillingAccountsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string** |  | 
 **offset** | **string** |  | 
 **limit** | **string** |  | 
 **sort** | **string** |  | 

### Return type

[**BillingAccountsList**](BillingAccountsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetBillingAccountsAssignments

> BillingAccountAssignmentList GetBillingAccountsAssignments(ctx).AccountId(accountId).OrganizationId(organizationId).ChannelId(channelId).InteractionId(interactionId).Offset(offset).Limit(limit).Sort(sort).Execute()

Get billing accounts assignments

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    accountId := "accountId_example" // string | At most one of account_id, organization_id, channel_id or interaction_id is required (optional)
    organizationId := "organizationId_example" // string | At most one of account_id, organization_id, channel_id or interaction_id is required (optional)
    channelId := "channelId_example" // string | At most one of account_id, organization_id, channel_id or interaction_id is required (optional)
    interactionId := "interactionId_example" // string | At most one of account_id, organization_id, channel_id or interaction_id is required (optional)
    offset := "offset_example" // string |  (optional)
    limit := "limit_example" // string |  (optional)
    sort := "sort_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.GetBillingAccountsAssignments(context.Background()).AccountId(accountId).OrganizationId(organizationId).ChannelId(channelId).InteractionId(interactionId).Offset(offset).Limit(limit).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.GetBillingAccountsAssignments``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBillingAccountsAssignments`: BillingAccountAssignmentList
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.GetBillingAccountsAssignments`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetBillingAccountsAssignmentsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **string** | At most one of account_id, organization_id, channel_id or interaction_id is required | 
 **organizationId** | **string** | At most one of account_id, organization_id, channel_id or interaction_id is required | 
 **channelId** | **string** | At most one of account_id, organization_id, channel_id or interaction_id is required | 
 **interactionId** | **string** | At most one of account_id, organization_id, channel_id or interaction_id is required | 
 **offset** | **string** |  | 
 **limit** | **string** |  | 
 **sort** | **string** |  | 

### Return type

[**BillingAccountAssignmentList**](BillingAccountAssignmentList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

