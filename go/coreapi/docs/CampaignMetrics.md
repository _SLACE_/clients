# CampaignMetrics

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Addressed** | **int32** |  | 
**Sent** | **int32** |  | 
**Executions** | **int32** |  | 
**Rescheduled** | **int32** |  | 
**OptOuts** | **int32** |  | 
**MarketingOptOuts** | **int32** |  | 
**Unreachable** | **int32** |  | 
**Reactance** | **int32** |  | 
**Errors** | **int32** |  | 
**Delivered** | **int32** |  | 
**Read** | **int32** |  | 

## Methods

### NewCampaignMetrics

`func NewCampaignMetrics(addressed int32, sent int32, executions int32, rescheduled int32, optOuts int32, marketingOptOuts int32, unreachable int32, reactance int32, errors int32, delivered int32, read int32, ) *CampaignMetrics`

NewCampaignMetrics instantiates a new CampaignMetrics object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignMetricsWithDefaults

`func NewCampaignMetricsWithDefaults() *CampaignMetrics`

NewCampaignMetricsWithDefaults instantiates a new CampaignMetrics object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAddressed

`func (o *CampaignMetrics) GetAddressed() int32`

GetAddressed returns the Addressed field if non-nil, zero value otherwise.

### GetAddressedOk

`func (o *CampaignMetrics) GetAddressedOk() (*int32, bool)`

GetAddressedOk returns a tuple with the Addressed field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddressed

`func (o *CampaignMetrics) SetAddressed(v int32)`

SetAddressed sets Addressed field to given value.


### GetSent

`func (o *CampaignMetrics) GetSent() int32`

GetSent returns the Sent field if non-nil, zero value otherwise.

### GetSentOk

`func (o *CampaignMetrics) GetSentOk() (*int32, bool)`

GetSentOk returns a tuple with the Sent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSent

`func (o *CampaignMetrics) SetSent(v int32)`

SetSent sets Sent field to given value.


### GetExecutions

`func (o *CampaignMetrics) GetExecutions() int32`

GetExecutions returns the Executions field if non-nil, zero value otherwise.

### GetExecutionsOk

`func (o *CampaignMetrics) GetExecutionsOk() (*int32, bool)`

GetExecutionsOk returns a tuple with the Executions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExecutions

`func (o *CampaignMetrics) SetExecutions(v int32)`

SetExecutions sets Executions field to given value.


### GetRescheduled

`func (o *CampaignMetrics) GetRescheduled() int32`

GetRescheduled returns the Rescheduled field if non-nil, zero value otherwise.

### GetRescheduledOk

`func (o *CampaignMetrics) GetRescheduledOk() (*int32, bool)`

GetRescheduledOk returns a tuple with the Rescheduled field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRescheduled

`func (o *CampaignMetrics) SetRescheduled(v int32)`

SetRescheduled sets Rescheduled field to given value.


### GetOptOuts

`func (o *CampaignMetrics) GetOptOuts() int32`

GetOptOuts returns the OptOuts field if non-nil, zero value otherwise.

### GetOptOutsOk

`func (o *CampaignMetrics) GetOptOutsOk() (*int32, bool)`

GetOptOutsOk returns a tuple with the OptOuts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptOuts

`func (o *CampaignMetrics) SetOptOuts(v int32)`

SetOptOuts sets OptOuts field to given value.


### GetMarketingOptOuts

`func (o *CampaignMetrics) GetMarketingOptOuts() int32`

GetMarketingOptOuts returns the MarketingOptOuts field if non-nil, zero value otherwise.

### GetMarketingOptOutsOk

`func (o *CampaignMetrics) GetMarketingOptOutsOk() (*int32, bool)`

GetMarketingOptOutsOk returns a tuple with the MarketingOptOuts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMarketingOptOuts

`func (o *CampaignMetrics) SetMarketingOptOuts(v int32)`

SetMarketingOptOuts sets MarketingOptOuts field to given value.


### GetUnreachable

`func (o *CampaignMetrics) GetUnreachable() int32`

GetUnreachable returns the Unreachable field if non-nil, zero value otherwise.

### GetUnreachableOk

`func (o *CampaignMetrics) GetUnreachableOk() (*int32, bool)`

GetUnreachableOk returns a tuple with the Unreachable field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnreachable

`func (o *CampaignMetrics) SetUnreachable(v int32)`

SetUnreachable sets Unreachable field to given value.


### GetReactance

`func (o *CampaignMetrics) GetReactance() int32`

GetReactance returns the Reactance field if non-nil, zero value otherwise.

### GetReactanceOk

`func (o *CampaignMetrics) GetReactanceOk() (*int32, bool)`

GetReactanceOk returns a tuple with the Reactance field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReactance

`func (o *CampaignMetrics) SetReactance(v int32)`

SetReactance sets Reactance field to given value.


### GetErrors

`func (o *CampaignMetrics) GetErrors() int32`

GetErrors returns the Errors field if non-nil, zero value otherwise.

### GetErrorsOk

`func (o *CampaignMetrics) GetErrorsOk() (*int32, bool)`

GetErrorsOk returns a tuple with the Errors field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrors

`func (o *CampaignMetrics) SetErrors(v int32)`

SetErrors sets Errors field to given value.


### GetDelivered

`func (o *CampaignMetrics) GetDelivered() int32`

GetDelivered returns the Delivered field if non-nil, zero value otherwise.

### GetDeliveredOk

`func (o *CampaignMetrics) GetDeliveredOk() (*int32, bool)`

GetDeliveredOk returns a tuple with the Delivered field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDelivered

`func (o *CampaignMetrics) SetDelivered(v int32)`

SetDelivered sets Delivered field to given value.


### GetRead

`func (o *CampaignMetrics) GetRead() int32`

GetRead returns the Read field if non-nil, zero value otherwise.

### GetReadOk

`func (o *CampaignMetrics) GetReadOk() (*int32, bool)`

GetReadOk returns a tuple with the Read field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRead

`func (o *CampaignMetrics) SetRead(v int32)`

SetRead sets Read field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


