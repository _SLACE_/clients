# CampaignStrategy

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ComChannel** | Pointer to [**CampaignStrategyComChannel**](CampaignStrategyComChannel.md) |  | [optional] 
**Channel** | Pointer to [**CampaignStrategyChannel**](CampaignStrategyChannel.md) |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 

## Methods

### NewCampaignStrategy

`func NewCampaignStrategy() *CampaignStrategy`

NewCampaignStrategy instantiates a new CampaignStrategy object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignStrategyWithDefaults

`func NewCampaignStrategyWithDefaults() *CampaignStrategy`

NewCampaignStrategyWithDefaults instantiates a new CampaignStrategy object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetComChannel

`func (o *CampaignStrategy) GetComChannel() CampaignStrategyComChannel`

GetComChannel returns the ComChannel field if non-nil, zero value otherwise.

### GetComChannelOk

`func (o *CampaignStrategy) GetComChannelOk() (*CampaignStrategyComChannel, bool)`

GetComChannelOk returns a tuple with the ComChannel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComChannel

`func (o *CampaignStrategy) SetComChannel(v CampaignStrategyComChannel)`

SetComChannel sets ComChannel field to given value.

### HasComChannel

`func (o *CampaignStrategy) HasComChannel() bool`

HasComChannel returns a boolean if a field has been set.

### GetChannel

`func (o *CampaignStrategy) GetChannel() CampaignStrategyChannel`

GetChannel returns the Channel field if non-nil, zero value otherwise.

### GetChannelOk

`func (o *CampaignStrategy) GetChannelOk() (*CampaignStrategyChannel, bool)`

GetChannelOk returns a tuple with the Channel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannel

`func (o *CampaignStrategy) SetChannel(v CampaignStrategyChannel)`

SetChannel sets Channel field to given value.

### HasChannel

`func (o *CampaignStrategy) HasChannel() bool`

HasChannel returns a boolean if a field has been set.

### GetType

`func (o *CampaignStrategy) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *CampaignStrategy) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *CampaignStrategy) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *CampaignStrategy) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


