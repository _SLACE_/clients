# NotificationAssetMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Translations** | Pointer to [**[]NotificationAssetTranslation**](NotificationAssetTranslation.md) |  | [optional] 

## Methods

### NewNotificationAssetMutable

`func NewNotificationAssetMutable() *NotificationAssetMutable`

NewNotificationAssetMutable instantiates a new NotificationAssetMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNotificationAssetMutableWithDefaults

`func NewNotificationAssetMutableWithDefaults() *NotificationAssetMutable`

NewNotificationAssetMutableWithDefaults instantiates a new NotificationAssetMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTranslations

`func (o *NotificationAssetMutable) GetTranslations() []NotificationAssetTranslation`

GetTranslations returns the Translations field if non-nil, zero value otherwise.

### GetTranslationsOk

`func (o *NotificationAssetMutable) GetTranslationsOk() (*[]NotificationAssetTranslation, bool)`

GetTranslationsOk returns a tuple with the Translations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTranslations

`func (o *NotificationAssetMutable) SetTranslations(v []NotificationAssetTranslation)`

SetTranslations sets Translations field to given value.

### HasTranslations

`func (o *NotificationAssetMutable) HasTranslations() bool`

HasTranslations returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


