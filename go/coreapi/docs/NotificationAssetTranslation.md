# NotificationAssetTranslation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Language** | [**SystemLanguage**](SystemLanguage.md) |  | 
**Subject** | **string** |  | 
**Text** | **string** |  | 
**CallToAction** | Pointer to **string** |  | [optional] 

## Methods

### NewNotificationAssetTranslation

`func NewNotificationAssetTranslation(language SystemLanguage, subject string, text string, ) *NotificationAssetTranslation`

NewNotificationAssetTranslation instantiates a new NotificationAssetTranslation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNotificationAssetTranslationWithDefaults

`func NewNotificationAssetTranslationWithDefaults() *NotificationAssetTranslation`

NewNotificationAssetTranslationWithDefaults instantiates a new NotificationAssetTranslation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLanguage

`func (o *NotificationAssetTranslation) GetLanguage() SystemLanguage`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *NotificationAssetTranslation) GetLanguageOk() (*SystemLanguage, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *NotificationAssetTranslation) SetLanguage(v SystemLanguage)`

SetLanguage sets Language field to given value.


### GetSubject

`func (o *NotificationAssetTranslation) GetSubject() string`

GetSubject returns the Subject field if non-nil, zero value otherwise.

### GetSubjectOk

`func (o *NotificationAssetTranslation) GetSubjectOk() (*string, bool)`

GetSubjectOk returns a tuple with the Subject field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSubject

`func (o *NotificationAssetTranslation) SetSubject(v string)`

SetSubject sets Subject field to given value.


### GetText

`func (o *NotificationAssetTranslation) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *NotificationAssetTranslation) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *NotificationAssetTranslation) SetText(v string)`

SetText sets Text field to given value.


### GetCallToAction

`func (o *NotificationAssetTranslation) GetCallToAction() string`

GetCallToAction returns the CallToAction field if non-nil, zero value otherwise.

### GetCallToActionOk

`func (o *NotificationAssetTranslation) GetCallToActionOk() (*string, bool)`

GetCallToActionOk returns a tuple with the CallToAction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCallToAction

`func (o *NotificationAssetTranslation) SetCallToAction(v string)`

SetCallToAction sets CallToAction field to given value.

### HasCallToAction

`func (o *NotificationAssetTranslation) HasCallToAction() bool`

HasCallToAction returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


