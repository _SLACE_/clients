# PlatformCommunicationChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Type** | **string** |  | 
**ContactId** | Pointer to **string** |  | [optional] 
**ProfileName** | **string** |  | 
**ProfilePhoto** | **string** |  | 
**PlatformId** | **string** |  | 
**Permission** | [**ComChannelPermission**](ComChannelPermission.md) |  | 
**Attributes** | Pointer to **map[string]string** |  | [optional] 
**CreatedAt** | **string** |  | 
**UpdatedAt** | **string** |  | 

## Methods

### NewPlatformCommunicationChannel

`func NewPlatformCommunicationChannel(id string, type_ string, profileName string, profilePhoto string, platformId string, permission ComChannelPermission, createdAt string, updatedAt string, ) *PlatformCommunicationChannel`

NewPlatformCommunicationChannel instantiates a new PlatformCommunicationChannel object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPlatformCommunicationChannelWithDefaults

`func NewPlatformCommunicationChannelWithDefaults() *PlatformCommunicationChannel`

NewPlatformCommunicationChannelWithDefaults instantiates a new PlatformCommunicationChannel object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *PlatformCommunicationChannel) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *PlatformCommunicationChannel) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *PlatformCommunicationChannel) SetId(v string)`

SetId sets Id field to given value.


### GetType

`func (o *PlatformCommunicationChannel) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *PlatformCommunicationChannel) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *PlatformCommunicationChannel) SetType(v string)`

SetType sets Type field to given value.


### GetContactId

`func (o *PlatformCommunicationChannel) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *PlatformCommunicationChannel) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *PlatformCommunicationChannel) SetContactId(v string)`

SetContactId sets ContactId field to given value.

### HasContactId

`func (o *PlatformCommunicationChannel) HasContactId() bool`

HasContactId returns a boolean if a field has been set.

### GetProfileName

`func (o *PlatformCommunicationChannel) GetProfileName() string`

GetProfileName returns the ProfileName field if non-nil, zero value otherwise.

### GetProfileNameOk

`func (o *PlatformCommunicationChannel) GetProfileNameOk() (*string, bool)`

GetProfileNameOk returns a tuple with the ProfileName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProfileName

`func (o *PlatformCommunicationChannel) SetProfileName(v string)`

SetProfileName sets ProfileName field to given value.


### GetProfilePhoto

`func (o *PlatformCommunicationChannel) GetProfilePhoto() string`

GetProfilePhoto returns the ProfilePhoto field if non-nil, zero value otherwise.

### GetProfilePhotoOk

`func (o *PlatformCommunicationChannel) GetProfilePhotoOk() (*string, bool)`

GetProfilePhotoOk returns a tuple with the ProfilePhoto field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProfilePhoto

`func (o *PlatformCommunicationChannel) SetProfilePhoto(v string)`

SetProfilePhoto sets ProfilePhoto field to given value.


### GetPlatformId

`func (o *PlatformCommunicationChannel) GetPlatformId() string`

GetPlatformId returns the PlatformId field if non-nil, zero value otherwise.

### GetPlatformIdOk

`func (o *PlatformCommunicationChannel) GetPlatformIdOk() (*string, bool)`

GetPlatformIdOk returns a tuple with the PlatformId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPlatformId

`func (o *PlatformCommunicationChannel) SetPlatformId(v string)`

SetPlatformId sets PlatformId field to given value.


### GetPermission

`func (o *PlatformCommunicationChannel) GetPermission() ComChannelPermission`

GetPermission returns the Permission field if non-nil, zero value otherwise.

### GetPermissionOk

`func (o *PlatformCommunicationChannel) GetPermissionOk() (*ComChannelPermission, bool)`

GetPermissionOk returns a tuple with the Permission field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermission

`func (o *PlatformCommunicationChannel) SetPermission(v ComChannelPermission)`

SetPermission sets Permission field to given value.


### GetAttributes

`func (o *PlatformCommunicationChannel) GetAttributes() map[string]string`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *PlatformCommunicationChannel) GetAttributesOk() (*map[string]string, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *PlatformCommunicationChannel) SetAttributes(v map[string]string)`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *PlatformCommunicationChannel) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetCreatedAt

`func (o *PlatformCommunicationChannel) GetCreatedAt() string`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *PlatformCommunicationChannel) GetCreatedAtOk() (*string, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *PlatformCommunicationChannel) SetCreatedAt(v string)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *PlatformCommunicationChannel) GetUpdatedAt() string`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *PlatformCommunicationChannel) GetUpdatedAtOk() (*string, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *PlatformCommunicationChannel) SetUpdatedAt(v string)`

SetUpdatedAt sets UpdatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


