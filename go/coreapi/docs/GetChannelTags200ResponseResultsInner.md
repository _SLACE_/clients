# GetChannelTags200ResponseResultsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**OrganizationId** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**CreatedAt** | Pointer to **string** |  | [optional] 
**UpdatedAt** | Pointer to **string** |  | [optional] 

## Methods

### NewGetChannelTags200ResponseResultsInner

`func NewGetChannelTags200ResponseResultsInner() *GetChannelTags200ResponseResultsInner`

NewGetChannelTags200ResponseResultsInner instantiates a new GetChannelTags200ResponseResultsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetChannelTags200ResponseResultsInnerWithDefaults

`func NewGetChannelTags200ResponseResultsInnerWithDefaults() *GetChannelTags200ResponseResultsInner`

NewGetChannelTags200ResponseResultsInnerWithDefaults instantiates a new GetChannelTags200ResponseResultsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *GetChannelTags200ResponseResultsInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *GetChannelTags200ResponseResultsInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *GetChannelTags200ResponseResultsInner) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *GetChannelTags200ResponseResultsInner) HasId() bool`

HasId returns a boolean if a field has been set.

### GetName

`func (o *GetChannelTags200ResponseResultsInner) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *GetChannelTags200ResponseResultsInner) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *GetChannelTags200ResponseResultsInner) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *GetChannelTags200ResponseResultsInner) HasName() bool`

HasName returns a boolean if a field has been set.

### GetOrganizationId

`func (o *GetChannelTags200ResponseResultsInner) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *GetChannelTags200ResponseResultsInner) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *GetChannelTags200ResponseResultsInner) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *GetChannelTags200ResponseResultsInner) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetChannelId

`func (o *GetChannelTags200ResponseResultsInner) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *GetChannelTags200ResponseResultsInner) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *GetChannelTags200ResponseResultsInner) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *GetChannelTags200ResponseResultsInner) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetCreatedAt

`func (o *GetChannelTags200ResponseResultsInner) GetCreatedAt() string`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *GetChannelTags200ResponseResultsInner) GetCreatedAtOk() (*string, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *GetChannelTags200ResponseResultsInner) SetCreatedAt(v string)`

SetCreatedAt sets CreatedAt field to given value.

### HasCreatedAt

`func (o *GetChannelTags200ResponseResultsInner) HasCreatedAt() bool`

HasCreatedAt returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *GetChannelTags200ResponseResultsInner) GetUpdatedAt() string`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *GetChannelTags200ResponseResultsInner) GetUpdatedAtOk() (*string, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *GetChannelTags200ResponseResultsInner) SetUpdatedAt(v string)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *GetChannelTags200ResponseResultsInner) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


