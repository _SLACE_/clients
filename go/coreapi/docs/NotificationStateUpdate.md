# NotificationStateUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ReadAt** | Pointer to **string** |  | [optional] 
**Mute** | Pointer to **bool** |  | [optional] 

## Methods

### NewNotificationStateUpdate

`func NewNotificationStateUpdate() *NotificationStateUpdate`

NewNotificationStateUpdate instantiates a new NotificationStateUpdate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNotificationStateUpdateWithDefaults

`func NewNotificationStateUpdateWithDefaults() *NotificationStateUpdate`

NewNotificationStateUpdateWithDefaults instantiates a new NotificationStateUpdate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetReadAt

`func (o *NotificationStateUpdate) GetReadAt() string`

GetReadAt returns the ReadAt field if non-nil, zero value otherwise.

### GetReadAtOk

`func (o *NotificationStateUpdate) GetReadAtOk() (*string, bool)`

GetReadAtOk returns a tuple with the ReadAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReadAt

`func (o *NotificationStateUpdate) SetReadAt(v string)`

SetReadAt sets ReadAt field to given value.

### HasReadAt

`func (o *NotificationStateUpdate) HasReadAt() bool`

HasReadAt returns a boolean if a field has been set.

### GetMute

`func (o *NotificationStateUpdate) GetMute() bool`

GetMute returns the Mute field if non-nil, zero value otherwise.

### GetMuteOk

`func (o *NotificationStateUpdate) GetMuteOk() (*bool, bool)`

GetMuteOk returns a tuple with the Mute field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMute

`func (o *NotificationStateUpdate) SetMute(v bool)`

SetMute sets Mute field to given value.

### HasMute

`func (o *NotificationStateUpdate) HasMute() bool`

HasMute returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


