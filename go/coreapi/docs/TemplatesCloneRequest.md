# TemplatesCloneRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TemplateIds** | **[]string** | Ids of templates you want to clone | 
**ChannelIds** | **[]string** | Target channel ids to where provided templates should be cloned | 
**TemplatesByName** | Pointer to [**[]TemplateCloneByName**](TemplateCloneByName.md) |  | [optional] 
**AutoSubmit** | Pointer to **bool** | Templates will get automatically submitted to whatsapp when true | [optional] 
**OrganizationId** | Pointer to **string** | Target organization id where templates will be cloned, required if target is different than source org | [optional] 

## Methods

### NewTemplatesCloneRequest

`func NewTemplatesCloneRequest(templateIds []string, channelIds []string, ) *TemplatesCloneRequest`

NewTemplatesCloneRequest instantiates a new TemplatesCloneRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTemplatesCloneRequestWithDefaults

`func NewTemplatesCloneRequestWithDefaults() *TemplatesCloneRequest`

NewTemplatesCloneRequestWithDefaults instantiates a new TemplatesCloneRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTemplateIds

`func (o *TemplatesCloneRequest) GetTemplateIds() []string`

GetTemplateIds returns the TemplateIds field if non-nil, zero value otherwise.

### GetTemplateIdsOk

`func (o *TemplatesCloneRequest) GetTemplateIdsOk() (*[]string, bool)`

GetTemplateIdsOk returns a tuple with the TemplateIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplateIds

`func (o *TemplatesCloneRequest) SetTemplateIds(v []string)`

SetTemplateIds sets TemplateIds field to given value.


### GetChannelIds

`func (o *TemplatesCloneRequest) GetChannelIds() []string`

GetChannelIds returns the ChannelIds field if non-nil, zero value otherwise.

### GetChannelIdsOk

`func (o *TemplatesCloneRequest) GetChannelIdsOk() (*[]string, bool)`

GetChannelIdsOk returns a tuple with the ChannelIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelIds

`func (o *TemplatesCloneRequest) SetChannelIds(v []string)`

SetChannelIds sets ChannelIds field to given value.


### GetTemplatesByName

`func (o *TemplatesCloneRequest) GetTemplatesByName() []TemplateCloneByName`

GetTemplatesByName returns the TemplatesByName field if non-nil, zero value otherwise.

### GetTemplatesByNameOk

`func (o *TemplatesCloneRequest) GetTemplatesByNameOk() (*[]TemplateCloneByName, bool)`

GetTemplatesByNameOk returns a tuple with the TemplatesByName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplatesByName

`func (o *TemplatesCloneRequest) SetTemplatesByName(v []TemplateCloneByName)`

SetTemplatesByName sets TemplatesByName field to given value.

### HasTemplatesByName

`func (o *TemplatesCloneRequest) HasTemplatesByName() bool`

HasTemplatesByName returns a boolean if a field has been set.

### GetAutoSubmit

`func (o *TemplatesCloneRequest) GetAutoSubmit() bool`

GetAutoSubmit returns the AutoSubmit field if non-nil, zero value otherwise.

### GetAutoSubmitOk

`func (o *TemplatesCloneRequest) GetAutoSubmitOk() (*bool, bool)`

GetAutoSubmitOk returns a tuple with the AutoSubmit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAutoSubmit

`func (o *TemplatesCloneRequest) SetAutoSubmit(v bool)`

SetAutoSubmit sets AutoSubmit field to given value.

### HasAutoSubmit

`func (o *TemplatesCloneRequest) HasAutoSubmit() bool`

HasAutoSubmit returns a boolean if a field has been set.

### GetOrganizationId

`func (o *TemplatesCloneRequest) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *TemplatesCloneRequest) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *TemplatesCloneRequest) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *TemplatesCloneRequest) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


