# Template

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**OrganizationId** | **string** |  | 
**GatewayId** | Pointer to **string** |  | [optional] 
**Type** | [**TemplateType**](TemplateType.md) |  | 
**Visibility** | [**TemplateVisibility**](TemplateVisibility.md) |  | 
**Language** | **string** |  | 
**Internal** | Pointer to [**InternalTemplate**](InternalTemplate.md) |  | [optional] 
**Whatsapp** | Pointer to [**WhatsAppTemplate**](WhatsAppTemplate.md) |  | [optional] 
**WhatsappDefaults** | Pointer to [**WhatsAppTemplateDefaults**](WhatsAppTemplateDefaults.md) |  | [optional] 
**Id** | **string** |  | 
**Status** | **string** |  | 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 

## Methods

### NewTemplate

`func NewTemplate(name string, organizationId string, type_ TemplateType, visibility TemplateVisibility, language string, id string, status string, createdAt time.Time, updatedAt time.Time, ) *Template`

NewTemplate instantiates a new Template object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTemplateWithDefaults

`func NewTemplateWithDefaults() *Template`

NewTemplateWithDefaults instantiates a new Template object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *Template) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Template) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Template) SetName(v string)`

SetName sets Name field to given value.


### GetOrganizationId

`func (o *Template) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *Template) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *Template) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetGatewayId

`func (o *Template) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *Template) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *Template) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.

### HasGatewayId

`func (o *Template) HasGatewayId() bool`

HasGatewayId returns a boolean if a field has been set.

### GetType

`func (o *Template) GetType() TemplateType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Template) GetTypeOk() (*TemplateType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Template) SetType(v TemplateType)`

SetType sets Type field to given value.


### GetVisibility

`func (o *Template) GetVisibility() TemplateVisibility`

GetVisibility returns the Visibility field if non-nil, zero value otherwise.

### GetVisibilityOk

`func (o *Template) GetVisibilityOk() (*TemplateVisibility, bool)`

GetVisibilityOk returns a tuple with the Visibility field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVisibility

`func (o *Template) SetVisibility(v TemplateVisibility)`

SetVisibility sets Visibility field to given value.


### GetLanguage

`func (o *Template) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *Template) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *Template) SetLanguage(v string)`

SetLanguage sets Language field to given value.


### GetInternal

`func (o *Template) GetInternal() InternalTemplate`

GetInternal returns the Internal field if non-nil, zero value otherwise.

### GetInternalOk

`func (o *Template) GetInternalOk() (*InternalTemplate, bool)`

GetInternalOk returns a tuple with the Internal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInternal

`func (o *Template) SetInternal(v InternalTemplate)`

SetInternal sets Internal field to given value.

### HasInternal

`func (o *Template) HasInternal() bool`

HasInternal returns a boolean if a field has been set.

### GetWhatsapp

`func (o *Template) GetWhatsapp() WhatsAppTemplate`

GetWhatsapp returns the Whatsapp field if non-nil, zero value otherwise.

### GetWhatsappOk

`func (o *Template) GetWhatsappOk() (*WhatsAppTemplate, bool)`

GetWhatsappOk returns a tuple with the Whatsapp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWhatsapp

`func (o *Template) SetWhatsapp(v WhatsAppTemplate)`

SetWhatsapp sets Whatsapp field to given value.

### HasWhatsapp

`func (o *Template) HasWhatsapp() bool`

HasWhatsapp returns a boolean if a field has been set.

### GetWhatsappDefaults

`func (o *Template) GetWhatsappDefaults() WhatsAppTemplateDefaults`

GetWhatsappDefaults returns the WhatsappDefaults field if non-nil, zero value otherwise.

### GetWhatsappDefaultsOk

`func (o *Template) GetWhatsappDefaultsOk() (*WhatsAppTemplateDefaults, bool)`

GetWhatsappDefaultsOk returns a tuple with the WhatsappDefaults field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWhatsappDefaults

`func (o *Template) SetWhatsappDefaults(v WhatsAppTemplateDefaults)`

SetWhatsappDefaults sets WhatsappDefaults field to given value.

### HasWhatsappDefaults

`func (o *Template) HasWhatsappDefaults() bool`

HasWhatsappDefaults returns a boolean if a field has been set.

### GetId

`func (o *Template) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Template) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Template) SetId(v string)`

SetId sets Id field to given value.


### GetStatus

`func (o *Template) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *Template) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *Template) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetCreatedAt

`func (o *Template) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Template) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Template) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *Template) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Template) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Template) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


