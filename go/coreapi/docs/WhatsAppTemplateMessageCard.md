# WhatsAppTemplateMessageCard

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CardIndex** | **int64** |  | 
**Components** | [**[]WhatsAppTemplateMessageComponent**](WhatsAppTemplateMessageComponent.md) |  | 

## Methods

### NewWhatsAppTemplateMessageCard

`func NewWhatsAppTemplateMessageCard(cardIndex int64, components []WhatsAppTemplateMessageComponent, ) *WhatsAppTemplateMessageCard`

NewWhatsAppTemplateMessageCard instantiates a new WhatsAppTemplateMessageCard object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMessageCardWithDefaults

`func NewWhatsAppTemplateMessageCardWithDefaults() *WhatsAppTemplateMessageCard`

NewWhatsAppTemplateMessageCardWithDefaults instantiates a new WhatsAppTemplateMessageCard object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCardIndex

`func (o *WhatsAppTemplateMessageCard) GetCardIndex() int64`

GetCardIndex returns the CardIndex field if non-nil, zero value otherwise.

### GetCardIndexOk

`func (o *WhatsAppTemplateMessageCard) GetCardIndexOk() (*int64, bool)`

GetCardIndexOk returns a tuple with the CardIndex field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCardIndex

`func (o *WhatsAppTemplateMessageCard) SetCardIndex(v int64)`

SetCardIndex sets CardIndex field to given value.


### GetComponents

`func (o *WhatsAppTemplateMessageCard) GetComponents() []WhatsAppTemplateMessageComponent`

GetComponents returns the Components field if non-nil, zero value otherwise.

### GetComponentsOk

`func (o *WhatsAppTemplateMessageCard) GetComponentsOk() (*[]WhatsAppTemplateMessageComponent, bool)`

GetComponentsOk returns a tuple with the Components field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComponents

`func (o *WhatsAppTemplateMessageCard) SetComponents(v []WhatsAppTemplateMessageComponent)`

SetComponents sets Components field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


