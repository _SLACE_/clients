# OrganizationMutableLifecycleSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**UpdateStatus** | **string** |  | 
**RegularCustomer** | [**[]CustomerEngagementSection**](CustomerEngagementSection.md) |  | 
**ChurnRiskCustomer** | [**[]CustomerEngagementSection**](CustomerEngagementSection.md) |  | 
**ChurnedCustomer** | [**[]CustomerEngagementSection**](CustomerEngagementSection.md) |  | 
**LastChangeAt** | **time.Time** |  | 

## Methods

### NewOrganizationMutableLifecycleSettings

`func NewOrganizationMutableLifecycleSettings(updateStatus string, regularCustomer []CustomerEngagementSection, churnRiskCustomer []CustomerEngagementSection, churnedCustomer []CustomerEngagementSection, lastChangeAt time.Time, ) *OrganizationMutableLifecycleSettings`

NewOrganizationMutableLifecycleSettings instantiates a new OrganizationMutableLifecycleSettings object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrganizationMutableLifecycleSettingsWithDefaults

`func NewOrganizationMutableLifecycleSettingsWithDefaults() *OrganizationMutableLifecycleSettings`

NewOrganizationMutableLifecycleSettingsWithDefaults instantiates a new OrganizationMutableLifecycleSettings object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUpdateStatus

`func (o *OrganizationMutableLifecycleSettings) GetUpdateStatus() string`

GetUpdateStatus returns the UpdateStatus field if non-nil, zero value otherwise.

### GetUpdateStatusOk

`func (o *OrganizationMutableLifecycleSettings) GetUpdateStatusOk() (*string, bool)`

GetUpdateStatusOk returns a tuple with the UpdateStatus field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdateStatus

`func (o *OrganizationMutableLifecycleSettings) SetUpdateStatus(v string)`

SetUpdateStatus sets UpdateStatus field to given value.


### GetRegularCustomer

`func (o *OrganizationMutableLifecycleSettings) GetRegularCustomer() []CustomerEngagementSection`

GetRegularCustomer returns the RegularCustomer field if non-nil, zero value otherwise.

### GetRegularCustomerOk

`func (o *OrganizationMutableLifecycleSettings) GetRegularCustomerOk() (*[]CustomerEngagementSection, bool)`

GetRegularCustomerOk returns a tuple with the RegularCustomer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRegularCustomer

`func (o *OrganizationMutableLifecycleSettings) SetRegularCustomer(v []CustomerEngagementSection)`

SetRegularCustomer sets RegularCustomer field to given value.


### GetChurnRiskCustomer

`func (o *OrganizationMutableLifecycleSettings) GetChurnRiskCustomer() []CustomerEngagementSection`

GetChurnRiskCustomer returns the ChurnRiskCustomer field if non-nil, zero value otherwise.

### GetChurnRiskCustomerOk

`func (o *OrganizationMutableLifecycleSettings) GetChurnRiskCustomerOk() (*[]CustomerEngagementSection, bool)`

GetChurnRiskCustomerOk returns a tuple with the ChurnRiskCustomer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChurnRiskCustomer

`func (o *OrganizationMutableLifecycleSettings) SetChurnRiskCustomer(v []CustomerEngagementSection)`

SetChurnRiskCustomer sets ChurnRiskCustomer field to given value.


### GetChurnedCustomer

`func (o *OrganizationMutableLifecycleSettings) GetChurnedCustomer() []CustomerEngagementSection`

GetChurnedCustomer returns the ChurnedCustomer field if non-nil, zero value otherwise.

### GetChurnedCustomerOk

`func (o *OrganizationMutableLifecycleSettings) GetChurnedCustomerOk() (*[]CustomerEngagementSection, bool)`

GetChurnedCustomerOk returns a tuple with the ChurnedCustomer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChurnedCustomer

`func (o *OrganizationMutableLifecycleSettings) SetChurnedCustomer(v []CustomerEngagementSection)`

SetChurnedCustomer sets ChurnedCustomer field to given value.


### GetLastChangeAt

`func (o *OrganizationMutableLifecycleSettings) GetLastChangeAt() time.Time`

GetLastChangeAt returns the LastChangeAt field if non-nil, zero value otherwise.

### GetLastChangeAtOk

`func (o *OrganizationMutableLifecycleSettings) GetLastChangeAtOk() (*time.Time, bool)`

GetLastChangeAtOk returns a tuple with the LastChangeAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastChangeAt

`func (o *OrganizationMutableLifecycleSettings) SetLastChangeAt(v time.Time)`

SetLastChangeAt sets LastChangeAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


