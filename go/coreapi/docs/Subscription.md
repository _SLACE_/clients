# Subscription

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**UserId** | **string** |  | 
**OrganizationId** | **string** |  | 
**Category** | [**NotificationCategory**](NotificationCategory.md) |  | 
**Channel** | [**NotificationChannel**](NotificationChannel.md) |  | 
**MinSeverity** | [**NotificationSeverity**](NotificationSeverity.md) |  | 
**Frequency** | [**NotificationFrequency**](NotificationFrequency.md) |  | 
**Muted** | **bool** |  | 
**NotifiedAt** | Pointer to **time.Time** |  | [optional] 

## Methods

### NewSubscription

`func NewSubscription(userId string, organizationId string, category NotificationCategory, channel NotificationChannel, minSeverity NotificationSeverity, frequency NotificationFrequency, muted bool, ) *Subscription`

NewSubscription instantiates a new Subscription object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSubscriptionWithDefaults

`func NewSubscriptionWithDefaults() *Subscription`

NewSubscriptionWithDefaults instantiates a new Subscription object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUserId

`func (o *Subscription) GetUserId() string`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *Subscription) GetUserIdOk() (*string, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *Subscription) SetUserId(v string)`

SetUserId sets UserId field to given value.


### GetOrganizationId

`func (o *Subscription) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *Subscription) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *Subscription) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetCategory

`func (o *Subscription) GetCategory() NotificationCategory`

GetCategory returns the Category field if non-nil, zero value otherwise.

### GetCategoryOk

`func (o *Subscription) GetCategoryOk() (*NotificationCategory, bool)`

GetCategoryOk returns a tuple with the Category field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCategory

`func (o *Subscription) SetCategory(v NotificationCategory)`

SetCategory sets Category field to given value.


### GetChannel

`func (o *Subscription) GetChannel() NotificationChannel`

GetChannel returns the Channel field if non-nil, zero value otherwise.

### GetChannelOk

`func (o *Subscription) GetChannelOk() (*NotificationChannel, bool)`

GetChannelOk returns a tuple with the Channel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannel

`func (o *Subscription) SetChannel(v NotificationChannel)`

SetChannel sets Channel field to given value.


### GetMinSeverity

`func (o *Subscription) GetMinSeverity() NotificationSeverity`

GetMinSeverity returns the MinSeverity field if non-nil, zero value otherwise.

### GetMinSeverityOk

`func (o *Subscription) GetMinSeverityOk() (*NotificationSeverity, bool)`

GetMinSeverityOk returns a tuple with the MinSeverity field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMinSeverity

`func (o *Subscription) SetMinSeverity(v NotificationSeverity)`

SetMinSeverity sets MinSeverity field to given value.


### GetFrequency

`func (o *Subscription) GetFrequency() NotificationFrequency`

GetFrequency returns the Frequency field if non-nil, zero value otherwise.

### GetFrequencyOk

`func (o *Subscription) GetFrequencyOk() (*NotificationFrequency, bool)`

GetFrequencyOk returns a tuple with the Frequency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFrequency

`func (o *Subscription) SetFrequency(v NotificationFrequency)`

SetFrequency sets Frequency field to given value.


### GetMuted

`func (o *Subscription) GetMuted() bool`

GetMuted returns the Muted field if non-nil, zero value otherwise.

### GetMutedOk

`func (o *Subscription) GetMutedOk() (*bool, bool)`

GetMutedOk returns a tuple with the Muted field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMuted

`func (o *Subscription) SetMuted(v bool)`

SetMuted sets Muted field to given value.


### GetNotifiedAt

`func (o *Subscription) GetNotifiedAt() time.Time`

GetNotifiedAt returns the NotifiedAt field if non-nil, zero value otherwise.

### GetNotifiedAtOk

`func (o *Subscription) GetNotifiedAtOk() (*time.Time, bool)`

GetNotifiedAtOk returns a tuple with the NotifiedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNotifiedAt

`func (o *Subscription) SetNotifiedAt(v time.Time)`

SetNotifiedAt sets NotifiedAt field to given value.

### HasNotifiedAt

`func (o *Subscription) HasNotifiedAt() bool`

HasNotifiedAt returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


