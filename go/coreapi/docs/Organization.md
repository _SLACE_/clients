# Organization

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CustomerId** | **string** |  | 
**Name** | **string** |  | 
**ContactInfo** | [**ContactInfo**](ContactInfo.md) |  | 
**Active** | Pointer to **bool** |  | [optional] 
**ExternalReferences** | Pointer to **map[string]string** | The map of any string values used for cutom external references.  | [optional] 
**Timezone** | Pointer to **string** |  | [optional] 
**OfficeHours** | Pointer to [**OfficeHoursSettings**](OfficeHoursSettings.md) |  | [optional] 
**InfoMessagesActive** | Pointer to **bool** |  | [optional] 
**DirectCalling** | Pointer to **bool** |  | [optional] 
**ActivityThreshold** | [**Threshold**](Threshold.md) |  | 
**ExternalSystems** | Pointer to [**[]ExternalSystem**](ExternalSystem.md) |  | [optional] 
**CooldownThreshold** | Pointer to [**Threshold**](Threshold.md) |  | [optional] 
**AutoresponderCooldown** | [**Threshold**](Threshold.md) |  | 
**CouponProviders** | Pointer to [**[]CouponProvider**](CouponProvider.md) |  | [optional] 
**ExternalIntegrations** | Pointer to [**ExternalIntegrations**](ExternalIntegrations.md) |  | [optional] 
**LifecycleSettings** | Pointer to [**OrganizationMutableLifecycleSettings**](OrganizationMutableLifecycleSettings.md) |  | [optional] 
**ContactAttributes** | Pointer to [**[]ContactAttribute**](ContactAttribute.md) |  | [optional] 
**DateFormat** | Pointer to **string** | \&quot;d\&quot; -&gt; \&quot;2006-01-02\&quot; \&quot;u\&quot; -&gt; \&quot;01/02/2006\&quot; \&quot;e\&quot; -&gt; \&quot;02/01/2006\&quot; \&quot;x\&quot; -&gt; \&quot;02.01.2006\&quot; | [optional] 
**StopKeywords** | **[]string** |  | 
**LanguageSettings** | Pointer to [**OrganizationMutableLanguageSettings**](OrganizationMutableLanguageSettings.md) |  | [optional] 
**Formality** | [**Formality**](Formality.md) |  | 
**ValidationCenterSettings** | Pointer to [**OrganizationMutableValidationCenterSettings**](OrganizationMutableValidationCenterSettings.md) |  | [optional] 
**PolyprintShopId** | Pointer to **string** |  | [optional] 
**PolyprintGroupId** | Pointer to **string** |  | [optional] 
**PolyprintNfcProductId** | Pointer to **string** |  | [optional] 
**DefaultWhatsappProvider** | Pointer to [**Provider**](Provider.md) |  | [optional] 
**BillingAccountId** | Pointer to **string** | Default billing account for newly created org. If none is selected, new will be auto-created. | [optional] 
**Id** | **string** |  | 
**Owner** | **bool** |  | 
**Customer** | Pointer to [**Customer**](Customer.md) |  | [optional] 
**LogoUrl** | Pointer to **string** |  | [optional] 

## Methods

### NewOrganization

`func NewOrganization(customerId string, name string, contactInfo ContactInfo, activityThreshold Threshold, autoresponderCooldown Threshold, stopKeywords []string, formality Formality, id string, owner bool, ) *Organization`

NewOrganization instantiates a new Organization object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrganizationWithDefaults

`func NewOrganizationWithDefaults() *Organization`

NewOrganizationWithDefaults instantiates a new Organization object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCustomerId

`func (o *Organization) GetCustomerId() string`

GetCustomerId returns the CustomerId field if non-nil, zero value otherwise.

### GetCustomerIdOk

`func (o *Organization) GetCustomerIdOk() (*string, bool)`

GetCustomerIdOk returns a tuple with the CustomerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerId

`func (o *Organization) SetCustomerId(v string)`

SetCustomerId sets CustomerId field to given value.


### GetName

`func (o *Organization) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Organization) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Organization) SetName(v string)`

SetName sets Name field to given value.


### GetContactInfo

`func (o *Organization) GetContactInfo() ContactInfo`

GetContactInfo returns the ContactInfo field if non-nil, zero value otherwise.

### GetContactInfoOk

`func (o *Organization) GetContactInfoOk() (*ContactInfo, bool)`

GetContactInfoOk returns a tuple with the ContactInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactInfo

`func (o *Organization) SetContactInfo(v ContactInfo)`

SetContactInfo sets ContactInfo field to given value.


### GetActive

`func (o *Organization) GetActive() bool`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *Organization) GetActiveOk() (*bool, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *Organization) SetActive(v bool)`

SetActive sets Active field to given value.

### HasActive

`func (o *Organization) HasActive() bool`

HasActive returns a boolean if a field has been set.

### GetExternalReferences

`func (o *Organization) GetExternalReferences() map[string]string`

GetExternalReferences returns the ExternalReferences field if non-nil, zero value otherwise.

### GetExternalReferencesOk

`func (o *Organization) GetExternalReferencesOk() (*map[string]string, bool)`

GetExternalReferencesOk returns a tuple with the ExternalReferences field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalReferences

`func (o *Organization) SetExternalReferences(v map[string]string)`

SetExternalReferences sets ExternalReferences field to given value.

### HasExternalReferences

`func (o *Organization) HasExternalReferences() bool`

HasExternalReferences returns a boolean if a field has been set.

### GetTimezone

`func (o *Organization) GetTimezone() string`

GetTimezone returns the Timezone field if non-nil, zero value otherwise.

### GetTimezoneOk

`func (o *Organization) GetTimezoneOk() (*string, bool)`

GetTimezoneOk returns a tuple with the Timezone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimezone

`func (o *Organization) SetTimezone(v string)`

SetTimezone sets Timezone field to given value.

### HasTimezone

`func (o *Organization) HasTimezone() bool`

HasTimezone returns a boolean if a field has been set.

### GetOfficeHours

`func (o *Organization) GetOfficeHours() OfficeHoursSettings`

GetOfficeHours returns the OfficeHours field if non-nil, zero value otherwise.

### GetOfficeHoursOk

`func (o *Organization) GetOfficeHoursOk() (*OfficeHoursSettings, bool)`

GetOfficeHoursOk returns a tuple with the OfficeHours field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOfficeHours

`func (o *Organization) SetOfficeHours(v OfficeHoursSettings)`

SetOfficeHours sets OfficeHours field to given value.

### HasOfficeHours

`func (o *Organization) HasOfficeHours() bool`

HasOfficeHours returns a boolean if a field has been set.

### GetInfoMessagesActive

`func (o *Organization) GetInfoMessagesActive() bool`

GetInfoMessagesActive returns the InfoMessagesActive field if non-nil, zero value otherwise.

### GetInfoMessagesActiveOk

`func (o *Organization) GetInfoMessagesActiveOk() (*bool, bool)`

GetInfoMessagesActiveOk returns a tuple with the InfoMessagesActive field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInfoMessagesActive

`func (o *Organization) SetInfoMessagesActive(v bool)`

SetInfoMessagesActive sets InfoMessagesActive field to given value.

### HasInfoMessagesActive

`func (o *Organization) HasInfoMessagesActive() bool`

HasInfoMessagesActive returns a boolean if a field has been set.

### GetDirectCalling

`func (o *Organization) GetDirectCalling() bool`

GetDirectCalling returns the DirectCalling field if non-nil, zero value otherwise.

### GetDirectCallingOk

`func (o *Organization) GetDirectCallingOk() (*bool, bool)`

GetDirectCallingOk returns a tuple with the DirectCalling field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDirectCalling

`func (o *Organization) SetDirectCalling(v bool)`

SetDirectCalling sets DirectCalling field to given value.

### HasDirectCalling

`func (o *Organization) HasDirectCalling() bool`

HasDirectCalling returns a boolean if a field has been set.

### GetActivityThreshold

`func (o *Organization) GetActivityThreshold() Threshold`

GetActivityThreshold returns the ActivityThreshold field if non-nil, zero value otherwise.

### GetActivityThresholdOk

`func (o *Organization) GetActivityThresholdOk() (*Threshold, bool)`

GetActivityThresholdOk returns a tuple with the ActivityThreshold field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActivityThreshold

`func (o *Organization) SetActivityThreshold(v Threshold)`

SetActivityThreshold sets ActivityThreshold field to given value.


### GetExternalSystems

`func (o *Organization) GetExternalSystems() []ExternalSystem`

GetExternalSystems returns the ExternalSystems field if non-nil, zero value otherwise.

### GetExternalSystemsOk

`func (o *Organization) GetExternalSystemsOk() (*[]ExternalSystem, bool)`

GetExternalSystemsOk returns a tuple with the ExternalSystems field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalSystems

`func (o *Organization) SetExternalSystems(v []ExternalSystem)`

SetExternalSystems sets ExternalSystems field to given value.

### HasExternalSystems

`func (o *Organization) HasExternalSystems() bool`

HasExternalSystems returns a boolean if a field has been set.

### GetCooldownThreshold

`func (o *Organization) GetCooldownThreshold() Threshold`

GetCooldownThreshold returns the CooldownThreshold field if non-nil, zero value otherwise.

### GetCooldownThresholdOk

`func (o *Organization) GetCooldownThresholdOk() (*Threshold, bool)`

GetCooldownThresholdOk returns a tuple with the CooldownThreshold field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCooldownThreshold

`func (o *Organization) SetCooldownThreshold(v Threshold)`

SetCooldownThreshold sets CooldownThreshold field to given value.

### HasCooldownThreshold

`func (o *Organization) HasCooldownThreshold() bool`

HasCooldownThreshold returns a boolean if a field has been set.

### GetAutoresponderCooldown

`func (o *Organization) GetAutoresponderCooldown() Threshold`

GetAutoresponderCooldown returns the AutoresponderCooldown field if non-nil, zero value otherwise.

### GetAutoresponderCooldownOk

`func (o *Organization) GetAutoresponderCooldownOk() (*Threshold, bool)`

GetAutoresponderCooldownOk returns a tuple with the AutoresponderCooldown field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAutoresponderCooldown

`func (o *Organization) SetAutoresponderCooldown(v Threshold)`

SetAutoresponderCooldown sets AutoresponderCooldown field to given value.


### GetCouponProviders

`func (o *Organization) GetCouponProviders() []CouponProvider`

GetCouponProviders returns the CouponProviders field if non-nil, zero value otherwise.

### GetCouponProvidersOk

`func (o *Organization) GetCouponProvidersOk() (*[]CouponProvider, bool)`

GetCouponProvidersOk returns a tuple with the CouponProviders field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCouponProviders

`func (o *Organization) SetCouponProviders(v []CouponProvider)`

SetCouponProviders sets CouponProviders field to given value.

### HasCouponProviders

`func (o *Organization) HasCouponProviders() bool`

HasCouponProviders returns a boolean if a field has been set.

### GetExternalIntegrations

`func (o *Organization) GetExternalIntegrations() ExternalIntegrations`

GetExternalIntegrations returns the ExternalIntegrations field if non-nil, zero value otherwise.

### GetExternalIntegrationsOk

`func (o *Organization) GetExternalIntegrationsOk() (*ExternalIntegrations, bool)`

GetExternalIntegrationsOk returns a tuple with the ExternalIntegrations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalIntegrations

`func (o *Organization) SetExternalIntegrations(v ExternalIntegrations)`

SetExternalIntegrations sets ExternalIntegrations field to given value.

### HasExternalIntegrations

`func (o *Organization) HasExternalIntegrations() bool`

HasExternalIntegrations returns a boolean if a field has been set.

### GetLifecycleSettings

`func (o *Organization) GetLifecycleSettings() OrganizationMutableLifecycleSettings`

GetLifecycleSettings returns the LifecycleSettings field if non-nil, zero value otherwise.

### GetLifecycleSettingsOk

`func (o *Organization) GetLifecycleSettingsOk() (*OrganizationMutableLifecycleSettings, bool)`

GetLifecycleSettingsOk returns a tuple with the LifecycleSettings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLifecycleSettings

`func (o *Organization) SetLifecycleSettings(v OrganizationMutableLifecycleSettings)`

SetLifecycleSettings sets LifecycleSettings field to given value.

### HasLifecycleSettings

`func (o *Organization) HasLifecycleSettings() bool`

HasLifecycleSettings returns a boolean if a field has been set.

### GetContactAttributes

`func (o *Organization) GetContactAttributes() []ContactAttribute`

GetContactAttributes returns the ContactAttributes field if non-nil, zero value otherwise.

### GetContactAttributesOk

`func (o *Organization) GetContactAttributesOk() (*[]ContactAttribute, bool)`

GetContactAttributesOk returns a tuple with the ContactAttributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactAttributes

`func (o *Organization) SetContactAttributes(v []ContactAttribute)`

SetContactAttributes sets ContactAttributes field to given value.

### HasContactAttributes

`func (o *Organization) HasContactAttributes() bool`

HasContactAttributes returns a boolean if a field has been set.

### GetDateFormat

`func (o *Organization) GetDateFormat() string`

GetDateFormat returns the DateFormat field if non-nil, zero value otherwise.

### GetDateFormatOk

`func (o *Organization) GetDateFormatOk() (*string, bool)`

GetDateFormatOk returns a tuple with the DateFormat field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateFormat

`func (o *Organization) SetDateFormat(v string)`

SetDateFormat sets DateFormat field to given value.

### HasDateFormat

`func (o *Organization) HasDateFormat() bool`

HasDateFormat returns a boolean if a field has been set.

### GetStopKeywords

`func (o *Organization) GetStopKeywords() []string`

GetStopKeywords returns the StopKeywords field if non-nil, zero value otherwise.

### GetStopKeywordsOk

`func (o *Organization) GetStopKeywordsOk() (*[]string, bool)`

GetStopKeywordsOk returns a tuple with the StopKeywords field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStopKeywords

`func (o *Organization) SetStopKeywords(v []string)`

SetStopKeywords sets StopKeywords field to given value.


### GetLanguageSettings

`func (o *Organization) GetLanguageSettings() OrganizationMutableLanguageSettings`

GetLanguageSettings returns the LanguageSettings field if non-nil, zero value otherwise.

### GetLanguageSettingsOk

`func (o *Organization) GetLanguageSettingsOk() (*OrganizationMutableLanguageSettings, bool)`

GetLanguageSettingsOk returns a tuple with the LanguageSettings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguageSettings

`func (o *Organization) SetLanguageSettings(v OrganizationMutableLanguageSettings)`

SetLanguageSettings sets LanguageSettings field to given value.

### HasLanguageSettings

`func (o *Organization) HasLanguageSettings() bool`

HasLanguageSettings returns a boolean if a field has been set.

### GetFormality

`func (o *Organization) GetFormality() Formality`

GetFormality returns the Formality field if non-nil, zero value otherwise.

### GetFormalityOk

`func (o *Organization) GetFormalityOk() (*Formality, bool)`

GetFormalityOk returns a tuple with the Formality field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormality

`func (o *Organization) SetFormality(v Formality)`

SetFormality sets Formality field to given value.


### GetValidationCenterSettings

`func (o *Organization) GetValidationCenterSettings() OrganizationMutableValidationCenterSettings`

GetValidationCenterSettings returns the ValidationCenterSettings field if non-nil, zero value otherwise.

### GetValidationCenterSettingsOk

`func (o *Organization) GetValidationCenterSettingsOk() (*OrganizationMutableValidationCenterSettings, bool)`

GetValidationCenterSettingsOk returns a tuple with the ValidationCenterSettings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidationCenterSettings

`func (o *Organization) SetValidationCenterSettings(v OrganizationMutableValidationCenterSettings)`

SetValidationCenterSettings sets ValidationCenterSettings field to given value.

### HasValidationCenterSettings

`func (o *Organization) HasValidationCenterSettings() bool`

HasValidationCenterSettings returns a boolean if a field has been set.

### GetPolyprintShopId

`func (o *Organization) GetPolyprintShopId() string`

GetPolyprintShopId returns the PolyprintShopId field if non-nil, zero value otherwise.

### GetPolyprintShopIdOk

`func (o *Organization) GetPolyprintShopIdOk() (*string, bool)`

GetPolyprintShopIdOk returns a tuple with the PolyprintShopId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolyprintShopId

`func (o *Organization) SetPolyprintShopId(v string)`

SetPolyprintShopId sets PolyprintShopId field to given value.

### HasPolyprintShopId

`func (o *Organization) HasPolyprintShopId() bool`

HasPolyprintShopId returns a boolean if a field has been set.

### GetPolyprintGroupId

`func (o *Organization) GetPolyprintGroupId() string`

GetPolyprintGroupId returns the PolyprintGroupId field if non-nil, zero value otherwise.

### GetPolyprintGroupIdOk

`func (o *Organization) GetPolyprintGroupIdOk() (*string, bool)`

GetPolyprintGroupIdOk returns a tuple with the PolyprintGroupId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolyprintGroupId

`func (o *Organization) SetPolyprintGroupId(v string)`

SetPolyprintGroupId sets PolyprintGroupId field to given value.

### HasPolyprintGroupId

`func (o *Organization) HasPolyprintGroupId() bool`

HasPolyprintGroupId returns a boolean if a field has been set.

### GetPolyprintNfcProductId

`func (o *Organization) GetPolyprintNfcProductId() string`

GetPolyprintNfcProductId returns the PolyprintNfcProductId field if non-nil, zero value otherwise.

### GetPolyprintNfcProductIdOk

`func (o *Organization) GetPolyprintNfcProductIdOk() (*string, bool)`

GetPolyprintNfcProductIdOk returns a tuple with the PolyprintNfcProductId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolyprintNfcProductId

`func (o *Organization) SetPolyprintNfcProductId(v string)`

SetPolyprintNfcProductId sets PolyprintNfcProductId field to given value.

### HasPolyprintNfcProductId

`func (o *Organization) HasPolyprintNfcProductId() bool`

HasPolyprintNfcProductId returns a boolean if a field has been set.

### GetDefaultWhatsappProvider

`func (o *Organization) GetDefaultWhatsappProvider() Provider`

GetDefaultWhatsappProvider returns the DefaultWhatsappProvider field if non-nil, zero value otherwise.

### GetDefaultWhatsappProviderOk

`func (o *Organization) GetDefaultWhatsappProviderOk() (*Provider, bool)`

GetDefaultWhatsappProviderOk returns a tuple with the DefaultWhatsappProvider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultWhatsappProvider

`func (o *Organization) SetDefaultWhatsappProvider(v Provider)`

SetDefaultWhatsappProvider sets DefaultWhatsappProvider field to given value.

### HasDefaultWhatsappProvider

`func (o *Organization) HasDefaultWhatsappProvider() bool`

HasDefaultWhatsappProvider returns a boolean if a field has been set.

### GetBillingAccountId

`func (o *Organization) GetBillingAccountId() string`

GetBillingAccountId returns the BillingAccountId field if non-nil, zero value otherwise.

### GetBillingAccountIdOk

`func (o *Organization) GetBillingAccountIdOk() (*string, bool)`

GetBillingAccountIdOk returns a tuple with the BillingAccountId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingAccountId

`func (o *Organization) SetBillingAccountId(v string)`

SetBillingAccountId sets BillingAccountId field to given value.

### HasBillingAccountId

`func (o *Organization) HasBillingAccountId() bool`

HasBillingAccountId returns a boolean if a field has been set.

### GetId

`func (o *Organization) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Organization) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Organization) SetId(v string)`

SetId sets Id field to given value.


### GetOwner

`func (o *Organization) GetOwner() bool`

GetOwner returns the Owner field if non-nil, zero value otherwise.

### GetOwnerOk

`func (o *Organization) GetOwnerOk() (*bool, bool)`

GetOwnerOk returns a tuple with the Owner field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOwner

`func (o *Organization) SetOwner(v bool)`

SetOwner sets Owner field to given value.


### GetCustomer

`func (o *Organization) GetCustomer() Customer`

GetCustomer returns the Customer field if non-nil, zero value otherwise.

### GetCustomerOk

`func (o *Organization) GetCustomerOk() (*Customer, bool)`

GetCustomerOk returns a tuple with the Customer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomer

`func (o *Organization) SetCustomer(v Customer)`

SetCustomer sets Customer field to given value.

### HasCustomer

`func (o *Organization) HasCustomer() bool`

HasCustomer returns a boolean if a field has been set.

### GetLogoUrl

`func (o *Organization) GetLogoUrl() string`

GetLogoUrl returns the LogoUrl field if non-nil, zero value otherwise.

### GetLogoUrlOk

`func (o *Organization) GetLogoUrlOk() (*string, bool)`

GetLogoUrlOk returns a tuple with the LogoUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLogoUrl

`func (o *Organization) SetLogoUrl(v string)`

SetLogoUrl sets LogoUrl field to given value.

### HasLogoUrl

`func (o *Organization) HasLogoUrl() bool`

HasLogoUrl returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


