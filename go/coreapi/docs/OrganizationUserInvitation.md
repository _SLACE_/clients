# OrganizationUserInvitation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Target** | **string** |  | 
**Mode** | **string** |  | 
**Permission** | **int32** |  | 
**Email** | Pointer to **string** |  | [optional] 
**Channels** | Pointer to **[]string** |  | [optional] 
**UserIds** | Pointer to **[]string** |  | [optional] 
**FirstName** | Pointer to **string** |  | [optional] 
**LastName** | Pointer to **string** |  | [optional] 
**Company** | Pointer to **string** |  | [optional] 
**Department** | Pointer to **string** |  | [optional] 
**JobTitle** | Pointer to **string** |  | [optional] 
**Info** | Pointer to **string** |  | [optional] 
**ExternalIds** | Pointer to **map[string]interface{}** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 

## Methods

### NewOrganizationUserInvitation

`func NewOrganizationUserInvitation(target string, mode string, permission int32, ) *OrganizationUserInvitation`

NewOrganizationUserInvitation instantiates a new OrganizationUserInvitation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrganizationUserInvitationWithDefaults

`func NewOrganizationUserInvitationWithDefaults() *OrganizationUserInvitation`

NewOrganizationUserInvitationWithDefaults instantiates a new OrganizationUserInvitation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTarget

`func (o *OrganizationUserInvitation) GetTarget() string`

GetTarget returns the Target field if non-nil, zero value otherwise.

### GetTargetOk

`func (o *OrganizationUserInvitation) GetTargetOk() (*string, bool)`

GetTargetOk returns a tuple with the Target field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTarget

`func (o *OrganizationUserInvitation) SetTarget(v string)`

SetTarget sets Target field to given value.


### GetMode

`func (o *OrganizationUserInvitation) GetMode() string`

GetMode returns the Mode field if non-nil, zero value otherwise.

### GetModeOk

`func (o *OrganizationUserInvitation) GetModeOk() (*string, bool)`

GetModeOk returns a tuple with the Mode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMode

`func (o *OrganizationUserInvitation) SetMode(v string)`

SetMode sets Mode field to given value.


### GetPermission

`func (o *OrganizationUserInvitation) GetPermission() int32`

GetPermission returns the Permission field if non-nil, zero value otherwise.

### GetPermissionOk

`func (o *OrganizationUserInvitation) GetPermissionOk() (*int32, bool)`

GetPermissionOk returns a tuple with the Permission field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermission

`func (o *OrganizationUserInvitation) SetPermission(v int32)`

SetPermission sets Permission field to given value.


### GetEmail

`func (o *OrganizationUserInvitation) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *OrganizationUserInvitation) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *OrganizationUserInvitation) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *OrganizationUserInvitation) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetChannels

`func (o *OrganizationUserInvitation) GetChannels() []string`

GetChannels returns the Channels field if non-nil, zero value otherwise.

### GetChannelsOk

`func (o *OrganizationUserInvitation) GetChannelsOk() (*[]string, bool)`

GetChannelsOk returns a tuple with the Channels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannels

`func (o *OrganizationUserInvitation) SetChannels(v []string)`

SetChannels sets Channels field to given value.

### HasChannels

`func (o *OrganizationUserInvitation) HasChannels() bool`

HasChannels returns a boolean if a field has been set.

### GetUserIds

`func (o *OrganizationUserInvitation) GetUserIds() []string`

GetUserIds returns the UserIds field if non-nil, zero value otherwise.

### GetUserIdsOk

`func (o *OrganizationUserInvitation) GetUserIdsOk() (*[]string, bool)`

GetUserIdsOk returns a tuple with the UserIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserIds

`func (o *OrganizationUserInvitation) SetUserIds(v []string)`

SetUserIds sets UserIds field to given value.

### HasUserIds

`func (o *OrganizationUserInvitation) HasUserIds() bool`

HasUserIds returns a boolean if a field has been set.

### GetFirstName

`func (o *OrganizationUserInvitation) GetFirstName() string`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *OrganizationUserInvitation) GetFirstNameOk() (*string, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *OrganizationUserInvitation) SetFirstName(v string)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *OrganizationUserInvitation) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *OrganizationUserInvitation) GetLastName() string`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *OrganizationUserInvitation) GetLastNameOk() (*string, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *OrganizationUserInvitation) SetLastName(v string)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *OrganizationUserInvitation) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetCompany

`func (o *OrganizationUserInvitation) GetCompany() string`

GetCompany returns the Company field if non-nil, zero value otherwise.

### GetCompanyOk

`func (o *OrganizationUserInvitation) GetCompanyOk() (*string, bool)`

GetCompanyOk returns a tuple with the Company field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompany

`func (o *OrganizationUserInvitation) SetCompany(v string)`

SetCompany sets Company field to given value.

### HasCompany

`func (o *OrganizationUserInvitation) HasCompany() bool`

HasCompany returns a boolean if a field has been set.

### GetDepartment

`func (o *OrganizationUserInvitation) GetDepartment() string`

GetDepartment returns the Department field if non-nil, zero value otherwise.

### GetDepartmentOk

`func (o *OrganizationUserInvitation) GetDepartmentOk() (*string, bool)`

GetDepartmentOk returns a tuple with the Department field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDepartment

`func (o *OrganizationUserInvitation) SetDepartment(v string)`

SetDepartment sets Department field to given value.

### HasDepartment

`func (o *OrganizationUserInvitation) HasDepartment() bool`

HasDepartment returns a boolean if a field has been set.

### GetJobTitle

`func (o *OrganizationUserInvitation) GetJobTitle() string`

GetJobTitle returns the JobTitle field if non-nil, zero value otherwise.

### GetJobTitleOk

`func (o *OrganizationUserInvitation) GetJobTitleOk() (*string, bool)`

GetJobTitleOk returns a tuple with the JobTitle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetJobTitle

`func (o *OrganizationUserInvitation) SetJobTitle(v string)`

SetJobTitle sets JobTitle field to given value.

### HasJobTitle

`func (o *OrganizationUserInvitation) HasJobTitle() bool`

HasJobTitle returns a boolean if a field has been set.

### GetInfo

`func (o *OrganizationUserInvitation) GetInfo() string`

GetInfo returns the Info field if non-nil, zero value otherwise.

### GetInfoOk

`func (o *OrganizationUserInvitation) GetInfoOk() (*string, bool)`

GetInfoOk returns a tuple with the Info field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInfo

`func (o *OrganizationUserInvitation) SetInfo(v string)`

SetInfo sets Info field to given value.

### HasInfo

`func (o *OrganizationUserInvitation) HasInfo() bool`

HasInfo returns a boolean if a field has been set.

### GetExternalIds

`func (o *OrganizationUserInvitation) GetExternalIds() map[string]interface{}`

GetExternalIds returns the ExternalIds field if non-nil, zero value otherwise.

### GetExternalIdsOk

`func (o *OrganizationUserInvitation) GetExternalIdsOk() (*map[string]interface{}, bool)`

GetExternalIdsOk returns a tuple with the ExternalIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalIds

`func (o *OrganizationUserInvitation) SetExternalIds(v map[string]interface{})`

SetExternalIds sets ExternalIds field to given value.

### HasExternalIds

`func (o *OrganizationUserInvitation) HasExternalIds() bool`

HasExternalIds returns a boolean if a field has been set.

### GetTags

`func (o *OrganizationUserInvitation) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *OrganizationUserInvitation) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *OrganizationUserInvitation) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *OrganizationUserInvitation) HasTags() bool`

HasTags returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


