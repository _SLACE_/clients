# \DefaultAPI

All URIs are relative to *https://api.slace.com/core*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateServiceUser**](DefaultAPI.md#CreateServiceUser) | **Post** /service-users | Create service user
[**ListOrgUsersMessengers**](DefaultAPI.md#ListOrgUsersMessengers) | **Get** /organizations/{organization_id}/users-messengers | List messengers for org users
[**ListServiceUsers**](DefaultAPI.md#ListServiceUsers) | **Get** /service-users | List service users
[**UpdateConsent**](DefaultAPI.md#UpdateConsent) | **Put** /organizations/{organization_id}/consents/{consent_id} | Update consent
[**UpdateGatewayActivity**](DefaultAPI.md#UpdateGatewayActivity) | **Post** /gateways/{gateway_id}/active | Update gateway activity



## CreateServiceUser

> CreateServiceUser200Response CreateServiceUser(ctx).CreateServiceUserRequest(createServiceUserRequest).Execute()

Create service user

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    createServiceUserRequest := *openapiclient.NewCreateServiceUserRequest("Name_example", []string{"OrganizationIds_example"}) // CreateServiceUserRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultAPI.CreateServiceUser(context.Background()).CreateServiceUserRequest(createServiceUserRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.CreateServiceUser``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateServiceUser`: CreateServiceUser200Response
    fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.CreateServiceUser`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateServiceUserRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createServiceUserRequest** | [**CreateServiceUserRequest**](CreateServiceUserRequest.md) |  | 

### Return type

[**CreateServiceUser200Response**](CreateServiceUser200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListOrgUsersMessengers

> map[string]interface{} ListOrgUsersMessengers(ctx, organizationId).UserIds(userIds).Execute()

List messengers for org users

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    userIds := []string{"Inner_example"} // []string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultAPI.ListOrgUsersMessengers(context.Background(), organizationId).UserIds(userIds).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.ListOrgUsersMessengers``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListOrgUsersMessengers`: map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.ListOrgUsersMessengers`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListOrgUsersMessengersRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **userIds** | **[]string** |  | 

### Return type

**map[string]interface{}**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListServiceUsers

> UsersList ListServiceUsers(ctx).Execute()

List service users

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultAPI.ListServiceUsers(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.ListServiceUsers``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListServiceUsers`: UsersList
    fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.ListServiceUsers`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiListServiceUsersRequest struct via the builder pattern


### Return type

[**UsersList**](UsersList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateConsent

> Consent UpdateConsent(ctx, organizationId, consentId).UpdateConsentRequest(updateConsentRequest).Execute()

Update consent

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    consentId := "consentId_example" // string | 
    updateConsentRequest := *openapiclient.NewUpdateConsentRequest(false) // UpdateConsentRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultAPI.UpdateConsent(context.Background(), organizationId, consentId).UpdateConsentRequest(updateConsentRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.UpdateConsent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateConsent`: Consent
    fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.UpdateConsent`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**consentId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateConsentRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **updateConsentRequest** | [**UpdateConsentRequest**](UpdateConsentRequest.md) |  | 

### Return type

[**Consent**](Consent.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateGatewayActivity

> Gateway UpdateGatewayActivity(ctx, gatewayId).UpdateGatewayActivityRequest(updateGatewayActivityRequest).Execute()

Update gateway activity

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    updateGatewayActivityRequest := *openapiclient.NewUpdateGatewayActivityRequest() // UpdateGatewayActivityRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultAPI.UpdateGatewayActivity(context.Background(), gatewayId).UpdateGatewayActivityRequest(updateGatewayActivityRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.UpdateGatewayActivity``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateGatewayActivity`: Gateway
    fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.UpdateGatewayActivity`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateGatewayActivityRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **updateGatewayActivityRequest** | [**UpdateGatewayActivityRequest**](UpdateGatewayActivityRequest.md) |  | 

### Return type

[**Gateway**](Gateway.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

