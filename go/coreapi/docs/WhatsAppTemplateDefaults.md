# WhatsAppTemplateDefaults

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Components** | [**[]WhatsAppTemplateDefaultsComponent**](WhatsAppTemplateDefaultsComponent.md) |  | 

## Methods

### NewWhatsAppTemplateDefaults

`func NewWhatsAppTemplateDefaults(components []WhatsAppTemplateDefaultsComponent, ) *WhatsAppTemplateDefaults`

NewWhatsAppTemplateDefaults instantiates a new WhatsAppTemplateDefaults object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateDefaultsWithDefaults

`func NewWhatsAppTemplateDefaultsWithDefaults() *WhatsAppTemplateDefaults`

NewWhatsAppTemplateDefaultsWithDefaults instantiates a new WhatsAppTemplateDefaults object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetComponents

`func (o *WhatsAppTemplateDefaults) GetComponents() []WhatsAppTemplateDefaultsComponent`

GetComponents returns the Components field if non-nil, zero value otherwise.

### GetComponentsOk

`func (o *WhatsAppTemplateDefaults) GetComponentsOk() (*[]WhatsAppTemplateDefaultsComponent, bool)`

GetComponentsOk returns a tuple with the Components field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComponents

`func (o *WhatsAppTemplateDefaults) SetComponents(v []WhatsAppTemplateDefaultsComponent)`

SetComponents sets Components field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


