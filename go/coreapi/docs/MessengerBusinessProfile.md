# MessengerBusinessProfile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Address** | Pointer to **string** |  | [optional] 
**Description** | Pointer to **string** |  | [optional] 
**Email** | Pointer to **string** |  | [optional] 
**Vertical** | Pointer to **string** |  | [optional] 
**Websites** | Pointer to **[]string** |  | [optional] 

## Methods

### NewMessengerBusinessProfile

`func NewMessengerBusinessProfile() *MessengerBusinessProfile`

NewMessengerBusinessProfile instantiates a new MessengerBusinessProfile object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessengerBusinessProfileWithDefaults

`func NewMessengerBusinessProfileWithDefaults() *MessengerBusinessProfile`

NewMessengerBusinessProfileWithDefaults instantiates a new MessengerBusinessProfile object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAddress

`func (o *MessengerBusinessProfile) GetAddress() string`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *MessengerBusinessProfile) GetAddressOk() (*string, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *MessengerBusinessProfile) SetAddress(v string)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *MessengerBusinessProfile) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetDescription

`func (o *MessengerBusinessProfile) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *MessengerBusinessProfile) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *MessengerBusinessProfile) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *MessengerBusinessProfile) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetEmail

`func (o *MessengerBusinessProfile) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *MessengerBusinessProfile) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *MessengerBusinessProfile) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *MessengerBusinessProfile) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetVertical

`func (o *MessengerBusinessProfile) GetVertical() string`

GetVertical returns the Vertical field if non-nil, zero value otherwise.

### GetVerticalOk

`func (o *MessengerBusinessProfile) GetVerticalOk() (*string, bool)`

GetVerticalOk returns a tuple with the Vertical field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVertical

`func (o *MessengerBusinessProfile) SetVertical(v string)`

SetVertical sets Vertical field to given value.

### HasVertical

`func (o *MessengerBusinessProfile) HasVertical() bool`

HasVertical returns a boolean if a field has been set.

### GetWebsites

`func (o *MessengerBusinessProfile) GetWebsites() []string`

GetWebsites returns the Websites field if non-nil, zero value otherwise.

### GetWebsitesOk

`func (o *MessengerBusinessProfile) GetWebsitesOk() (*[]string, bool)`

GetWebsitesOk returns a tuple with the Websites field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWebsites

`func (o *MessengerBusinessProfile) SetWebsites(v []string)`

SetWebsites sets Websites field to given value.

### HasWebsites

`func (o *MessengerBusinessProfile) HasWebsites() bool`

HasWebsites returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


