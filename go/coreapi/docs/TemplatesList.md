# TemplatesList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]Template**](Template.md) |  | 
**Total** | **string** |  | 

## Methods

### NewTemplatesList

`func NewTemplatesList(results []Template, total string, ) *TemplatesList`

NewTemplatesList instantiates a new TemplatesList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTemplatesListWithDefaults

`func NewTemplatesListWithDefaults() *TemplatesList`

NewTemplatesListWithDefaults instantiates a new TemplatesList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *TemplatesList) GetResults() []Template`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *TemplatesList) GetResultsOk() (*[]Template, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *TemplatesList) SetResults(v []Template)`

SetResults sets Results field to given value.


### GetTotal

`func (o *TemplatesList) GetTotal() string`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *TemplatesList) GetTotalOk() (*string, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *TemplatesList) SetTotal(v string)`

SetTotal sets Total field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


