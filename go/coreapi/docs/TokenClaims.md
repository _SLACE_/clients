# TokenClaims

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**R** | **string** | Role | 
**V** | **int32** | Version | 
**T** | **string** | Type | 
**Sid** | Pointer to **string** | Service Account ID | [optional] 

## Methods

### NewTokenClaims

`func NewTokenClaims(r string, v int32, t string, ) *TokenClaims`

NewTokenClaims instantiates a new TokenClaims object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTokenClaimsWithDefaults

`func NewTokenClaimsWithDefaults() *TokenClaims`

NewTokenClaimsWithDefaults instantiates a new TokenClaims object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetR

`func (o *TokenClaims) GetR() string`

GetR returns the R field if non-nil, zero value otherwise.

### GetROk

`func (o *TokenClaims) GetROk() (*string, bool)`

GetROk returns a tuple with the R field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetR

`func (o *TokenClaims) SetR(v string)`

SetR sets R field to given value.


### GetV

`func (o *TokenClaims) GetV() int32`

GetV returns the V field if non-nil, zero value otherwise.

### GetVOk

`func (o *TokenClaims) GetVOk() (*int32, bool)`

GetVOk returns a tuple with the V field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetV

`func (o *TokenClaims) SetV(v int32)`

SetV sets V field to given value.


### GetT

`func (o *TokenClaims) GetT() string`

GetT returns the T field if non-nil, zero value otherwise.

### GetTOk

`func (o *TokenClaims) GetTOk() (*string, bool)`

GetTOk returns a tuple with the T field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetT

`func (o *TokenClaims) SetT(v string)`

SetT sets T field to given value.


### GetSid

`func (o *TokenClaims) GetSid() string`

GetSid returns the Sid field if non-nil, zero value otherwise.

### GetSidOk

`func (o *TokenClaims) GetSidOk() (*string, bool)`

GetSidOk returns a tuple with the Sid field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSid

`func (o *TokenClaims) SetSid(v string)`

SetSid sets Sid field to given value.

### HasSid

`func (o *TokenClaims) HasSid() bool`

HasSid returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


