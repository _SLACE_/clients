# GatewayCreateData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelId** | **string** |  | 
**Name** | **string** |  | 
**Maid** | **string** |  | 
**Type** | [**Messenger**](Messenger.md) |  | 
**Provider** | [**Provider**](Provider.md) |  | 
**AuthKey** | **string** |  | 
**AuthSecret** | **string** |  | 
**Namespace** | Pointer to **string** |  | [optional] 
**ExternalIds** | Pointer to [**GatewayCreateDataExternalIds**](GatewayCreateDataExternalIds.md) |  | [optional] 

## Methods

### NewGatewayCreateData

`func NewGatewayCreateData(channelId string, name string, maid string, type_ Messenger, provider Provider, authKey string, authSecret string, ) *GatewayCreateData`

NewGatewayCreateData instantiates a new GatewayCreateData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGatewayCreateDataWithDefaults

`func NewGatewayCreateDataWithDefaults() *GatewayCreateData`

NewGatewayCreateDataWithDefaults instantiates a new GatewayCreateData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetChannelId

`func (o *GatewayCreateData) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *GatewayCreateData) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *GatewayCreateData) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetName

`func (o *GatewayCreateData) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *GatewayCreateData) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *GatewayCreateData) SetName(v string)`

SetName sets Name field to given value.


### GetMaid

`func (o *GatewayCreateData) GetMaid() string`

GetMaid returns the Maid field if non-nil, zero value otherwise.

### GetMaidOk

`func (o *GatewayCreateData) GetMaidOk() (*string, bool)`

GetMaidOk returns a tuple with the Maid field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaid

`func (o *GatewayCreateData) SetMaid(v string)`

SetMaid sets Maid field to given value.


### GetType

`func (o *GatewayCreateData) GetType() Messenger`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *GatewayCreateData) GetTypeOk() (*Messenger, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *GatewayCreateData) SetType(v Messenger)`

SetType sets Type field to given value.


### GetProvider

`func (o *GatewayCreateData) GetProvider() Provider`

GetProvider returns the Provider field if non-nil, zero value otherwise.

### GetProviderOk

`func (o *GatewayCreateData) GetProviderOk() (*Provider, bool)`

GetProviderOk returns a tuple with the Provider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProvider

`func (o *GatewayCreateData) SetProvider(v Provider)`

SetProvider sets Provider field to given value.


### GetAuthKey

`func (o *GatewayCreateData) GetAuthKey() string`

GetAuthKey returns the AuthKey field if non-nil, zero value otherwise.

### GetAuthKeyOk

`func (o *GatewayCreateData) GetAuthKeyOk() (*string, bool)`

GetAuthKeyOk returns a tuple with the AuthKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthKey

`func (o *GatewayCreateData) SetAuthKey(v string)`

SetAuthKey sets AuthKey field to given value.


### GetAuthSecret

`func (o *GatewayCreateData) GetAuthSecret() string`

GetAuthSecret returns the AuthSecret field if non-nil, zero value otherwise.

### GetAuthSecretOk

`func (o *GatewayCreateData) GetAuthSecretOk() (*string, bool)`

GetAuthSecretOk returns a tuple with the AuthSecret field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthSecret

`func (o *GatewayCreateData) SetAuthSecret(v string)`

SetAuthSecret sets AuthSecret field to given value.


### GetNamespace

`func (o *GatewayCreateData) GetNamespace() string`

GetNamespace returns the Namespace field if non-nil, zero value otherwise.

### GetNamespaceOk

`func (o *GatewayCreateData) GetNamespaceOk() (*string, bool)`

GetNamespaceOk returns a tuple with the Namespace field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNamespace

`func (o *GatewayCreateData) SetNamespace(v string)`

SetNamespace sets Namespace field to given value.

### HasNamespace

`func (o *GatewayCreateData) HasNamespace() bool`

HasNamespace returns a boolean if a field has been set.

### GetExternalIds

`func (o *GatewayCreateData) GetExternalIds() GatewayCreateDataExternalIds`

GetExternalIds returns the ExternalIds field if non-nil, zero value otherwise.

### GetExternalIdsOk

`func (o *GatewayCreateData) GetExternalIdsOk() (*GatewayCreateDataExternalIds, bool)`

GetExternalIdsOk returns a tuple with the ExternalIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalIds

`func (o *GatewayCreateData) SetExternalIds(v GatewayCreateDataExternalIds)`

SetExternalIds sets ExternalIds field to given value.

### HasExternalIds

`func (o *GatewayCreateData) HasExternalIds() bool`

HasExternalIds returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


