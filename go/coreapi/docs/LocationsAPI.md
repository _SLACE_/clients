# \LocationsAPI

All URIs are relative to *https://api.slace.com/core*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateLocation**](LocationsAPI.md#CreateLocation) | **Post** /locations | Create location
[**DeleteLocation**](LocationsAPI.md#DeleteLocation) | **Delete** /locations/{id} | Delete location
[**FindLocationByID**](LocationsAPI.md#FindLocationByID) | **Get** /locations/{id} | Find location by ID
[**GetLocationsExport**](LocationsAPI.md#GetLocationsExport) | **Get** /locations-export | Export locations
[**ImportLocations**](LocationsAPI.md#ImportLocations) | **Post** /locations-import | Import locations
[**ListLocations**](LocationsAPI.md#ListLocations) | **Get** /locations | List locations
[**UpdateLocation**](LocationsAPI.md#UpdateLocation) | **Put** /locations/{id} | Update location



## CreateLocation

> ChannelLocation CreateLocation(ctx).ChannelLocation(channelLocation).Execute()

Create location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelLocation := *openapiclient.NewChannelLocation("Id_example", "Type_example", "TagName_example", "ExternalName_example", "OrganizationId_example", "Name_example", "CreatedAt_example", "UpdatedAt_example", "Scope_example") // ChannelLocation |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.LocationsAPI.CreateLocation(context.Background()).ChannelLocation(channelLocation).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `LocationsAPI.CreateLocation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateLocation`: ChannelLocation
    fmt.Fprintf(os.Stdout, "Response from `LocationsAPI.CreateLocation`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateLocationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelLocation** | [**ChannelLocation**](ChannelLocation.md) |  | 

### Return type

[**ChannelLocation**](ChannelLocation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteLocation

> DeleteLocation(ctx, id).Execute()

Delete location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.LocationsAPI.DeleteLocation(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `LocationsAPI.DeleteLocation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteLocationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## FindLocationByID

> ChannelLocation FindLocationByID(ctx, id).Execute()

Find location by ID

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.LocationsAPI.FindLocationByID(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `LocationsAPI.FindLocationByID``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `FindLocationByID`: ChannelLocation
    fmt.Fprintf(os.Stdout, "Response from `LocationsAPI.FindLocationByID`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiFindLocationByIDRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**ChannelLocation**](ChannelLocation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetLocationsExport

> map[string]interface{} GetLocationsExport(ctx).Limit(limit).Offset(offset).Sort(sort).Format(format).OrganizationId(organizationId).ChannelId(channelId).Execute()

Export locations

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    limit := int32(56) // int32 |  (optional)
    offset := int32(56) // int32 |  (optional)
    sort := "sort_example" // string |  (optional)
    format := "format_example" // string |  (optional)
    organizationId := "organizationId_example" // string |  (optional)
    channelId := "channelId_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.LocationsAPI.GetLocationsExport(context.Background()).Limit(limit).Offset(offset).Sort(sort).Format(format).OrganizationId(organizationId).ChannelId(channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `LocationsAPI.GetLocationsExport``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetLocationsExport`: map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `LocationsAPI.GetLocationsExport`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetLocationsExportRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int32** |  | 
 **offset** | **int32** |  | 
 **sort** | **string** |  | 
 **format** | **string** |  | 
 **organizationId** | **string** |  | 
 **channelId** | **string** |  | 

### Return type

**map[string]interface{}**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ImportLocations

> ImportLocations(ctx).File(file).Execute()

Import locations



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    file := "file_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.LocationsAPI.ImportLocations(context.Background()).File(file).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `LocationsAPI.ImportLocations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiImportLocationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **string** |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListLocations

> LocationsList ListLocations(ctx).Limit(limit).Offset(offset).Sort(sort).Format(format).OrganizationId(organizationId).ChannelId(channelId).Default_(default_).Search(search).Lat(lat).Lng(lng).Billing(billing).Scope(scope).Execute()

List locations

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    limit := int32(56) // int32 |  (optional)
    offset := int32(56) // int32 |  (optional)
    sort := "sort_example" // string |  (optional)
    format := "format_example" // string |  (optional)
    organizationId := "organizationId_example" // string |  (optional)
    channelId := "channelId_example" // string |  (optional)
    default_ := "default__example" // string |  (optional)
    search := "search_example" // string |  (optional)
    lat := "lat_example" // string |  (optional)
    lng := "lng_example" // string |  (optional)
    billing := "billing_example" // string |  (optional)
    scope := "scope_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.LocationsAPI.ListLocations(context.Background()).Limit(limit).Offset(offset).Sort(sort).Format(format).OrganizationId(organizationId).ChannelId(channelId).Default_(default_).Search(search).Lat(lat).Lng(lng).Billing(billing).Scope(scope).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `LocationsAPI.ListLocations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListLocations`: LocationsList
    fmt.Fprintf(os.Stdout, "Response from `LocationsAPI.ListLocations`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListLocationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int32** |  | 
 **offset** | **int32** |  | 
 **sort** | **string** |  | 
 **format** | **string** |  | 
 **organizationId** | **string** |  | 
 **channelId** | **string** |  | 
 **default_** | **string** |  | 
 **search** | **string** |  | 
 **lat** | **string** |  | 
 **lng** | **string** |  | 
 **billing** | **string** |  | 
 **scope** | **string** |  | 

### Return type

[**LocationsList**](LocationsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateLocation

> ChannelLocation UpdateLocation(ctx, id).ChannelLocation(channelLocation).Execute()

Update location

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    id := "id_example" // string | 
    channelLocation := *openapiclient.NewChannelLocation("Id_example", "Type_example", "TagName_example", "ExternalName_example", "OrganizationId_example", "Name_example", "CreatedAt_example", "UpdatedAt_example", "Scope_example") // ChannelLocation |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.LocationsAPI.UpdateLocation(context.Background(), id).ChannelLocation(channelLocation).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `LocationsAPI.UpdateLocation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateLocation`: ChannelLocation
    fmt.Fprintf(os.Stdout, "Response from `LocationsAPI.UpdateLocation`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateLocationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **channelLocation** | [**ChannelLocation**](ChannelLocation.md) |  | 

### Return type

[**ChannelLocation**](ChannelLocation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

