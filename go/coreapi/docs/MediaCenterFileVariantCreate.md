# MediaCenterFileVariantCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Key** | Pointer to **string** |  | [optional] 
**FileKey** | Pointer to **string** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 
**Default** | Pointer to **bool** |  | [optional] 
**ValidFrom** | Pointer to **time.Time** |  | [optional] 
**ValidUntil** | Pointer to **time.Time** |  | [optional] 
**Url** | Pointer to **string** | File url that will be downloaded in SLACE cdn | [optional] 
**External** | Pointer to **bool** |  | [optional] 
**Translations** | Pointer to [**[]MediaCenterTranslation**](MediaCenterTranslation.md) |  | [optional] 
**FallbackLanguage** | Pointer to **string** |  | [optional] 

## Methods

### NewMediaCenterFileVariantCreate

`func NewMediaCenterFileVariantCreate() *MediaCenterFileVariantCreate`

NewMediaCenterFileVariantCreate instantiates a new MediaCenterFileVariantCreate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMediaCenterFileVariantCreateWithDefaults

`func NewMediaCenterFileVariantCreateWithDefaults() *MediaCenterFileVariantCreate`

NewMediaCenterFileVariantCreateWithDefaults instantiates a new MediaCenterFileVariantCreate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetKey

`func (o *MediaCenterFileVariantCreate) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *MediaCenterFileVariantCreate) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *MediaCenterFileVariantCreate) SetKey(v string)`

SetKey sets Key field to given value.

### HasKey

`func (o *MediaCenterFileVariantCreate) HasKey() bool`

HasKey returns a boolean if a field has been set.

### GetFileKey

`func (o *MediaCenterFileVariantCreate) GetFileKey() string`

GetFileKey returns the FileKey field if non-nil, zero value otherwise.

### GetFileKeyOk

`func (o *MediaCenterFileVariantCreate) GetFileKeyOk() (*string, bool)`

GetFileKeyOk returns a tuple with the FileKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFileKey

`func (o *MediaCenterFileVariantCreate) SetFileKey(v string)`

SetFileKey sets FileKey field to given value.

### HasFileKey

`func (o *MediaCenterFileVariantCreate) HasFileKey() bool`

HasFileKey returns a boolean if a field has been set.

### GetType

`func (o *MediaCenterFileVariantCreate) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MediaCenterFileVariantCreate) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MediaCenterFileVariantCreate) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *MediaCenterFileVariantCreate) HasType() bool`

HasType returns a boolean if a field has been set.

### GetDefault

`func (o *MediaCenterFileVariantCreate) GetDefault() bool`

GetDefault returns the Default field if non-nil, zero value otherwise.

### GetDefaultOk

`func (o *MediaCenterFileVariantCreate) GetDefaultOk() (*bool, bool)`

GetDefaultOk returns a tuple with the Default field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefault

`func (o *MediaCenterFileVariantCreate) SetDefault(v bool)`

SetDefault sets Default field to given value.

### HasDefault

`func (o *MediaCenterFileVariantCreate) HasDefault() bool`

HasDefault returns a boolean if a field has been set.

### GetValidFrom

`func (o *MediaCenterFileVariantCreate) GetValidFrom() time.Time`

GetValidFrom returns the ValidFrom field if non-nil, zero value otherwise.

### GetValidFromOk

`func (o *MediaCenterFileVariantCreate) GetValidFromOk() (*time.Time, bool)`

GetValidFromOk returns a tuple with the ValidFrom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidFrom

`func (o *MediaCenterFileVariantCreate) SetValidFrom(v time.Time)`

SetValidFrom sets ValidFrom field to given value.

### HasValidFrom

`func (o *MediaCenterFileVariantCreate) HasValidFrom() bool`

HasValidFrom returns a boolean if a field has been set.

### GetValidUntil

`func (o *MediaCenterFileVariantCreate) GetValidUntil() time.Time`

GetValidUntil returns the ValidUntil field if non-nil, zero value otherwise.

### GetValidUntilOk

`func (o *MediaCenterFileVariantCreate) GetValidUntilOk() (*time.Time, bool)`

GetValidUntilOk returns a tuple with the ValidUntil field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidUntil

`func (o *MediaCenterFileVariantCreate) SetValidUntil(v time.Time)`

SetValidUntil sets ValidUntil field to given value.

### HasValidUntil

`func (o *MediaCenterFileVariantCreate) HasValidUntil() bool`

HasValidUntil returns a boolean if a field has been set.

### GetUrl

`func (o *MediaCenterFileVariantCreate) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *MediaCenterFileVariantCreate) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *MediaCenterFileVariantCreate) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *MediaCenterFileVariantCreate) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetExternal

`func (o *MediaCenterFileVariantCreate) GetExternal() bool`

GetExternal returns the External field if non-nil, zero value otherwise.

### GetExternalOk

`func (o *MediaCenterFileVariantCreate) GetExternalOk() (*bool, bool)`

GetExternalOk returns a tuple with the External field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternal

`func (o *MediaCenterFileVariantCreate) SetExternal(v bool)`

SetExternal sets External field to given value.

### HasExternal

`func (o *MediaCenterFileVariantCreate) HasExternal() bool`

HasExternal returns a boolean if a field has been set.

### GetTranslations

`func (o *MediaCenterFileVariantCreate) GetTranslations() []MediaCenterTranslation`

GetTranslations returns the Translations field if non-nil, zero value otherwise.

### GetTranslationsOk

`func (o *MediaCenterFileVariantCreate) GetTranslationsOk() (*[]MediaCenterTranslation, bool)`

GetTranslationsOk returns a tuple with the Translations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTranslations

`func (o *MediaCenterFileVariantCreate) SetTranslations(v []MediaCenterTranslation)`

SetTranslations sets Translations field to given value.

### HasTranslations

`func (o *MediaCenterFileVariantCreate) HasTranslations() bool`

HasTranslations returns a boolean if a field has been set.

### GetFallbackLanguage

`func (o *MediaCenterFileVariantCreate) GetFallbackLanguage() string`

GetFallbackLanguage returns the FallbackLanguage field if non-nil, zero value otherwise.

### GetFallbackLanguageOk

`func (o *MediaCenterFileVariantCreate) GetFallbackLanguageOk() (*string, bool)`

GetFallbackLanguageOk returns a tuple with the FallbackLanguage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFallbackLanguage

`func (o *MediaCenterFileVariantCreate) SetFallbackLanguage(v string)`

SetFallbackLanguage sets FallbackLanguage field to given value.

### HasFallbackLanguage

`func (o *MediaCenterFileVariantCreate) HasFallbackLanguage() bool`

HasFallbackLanguage returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


