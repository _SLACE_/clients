# CampaignStrategyComChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TypePreference** | Pointer to **[]string** |  | [optional] 

## Methods

### NewCampaignStrategyComChannel

`func NewCampaignStrategyComChannel() *CampaignStrategyComChannel`

NewCampaignStrategyComChannel instantiates a new CampaignStrategyComChannel object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignStrategyComChannelWithDefaults

`func NewCampaignStrategyComChannelWithDefaults() *CampaignStrategyComChannel`

NewCampaignStrategyComChannelWithDefaults instantiates a new CampaignStrategyComChannel object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTypePreference

`func (o *CampaignStrategyComChannel) GetTypePreference() []string`

GetTypePreference returns the TypePreference field if non-nil, zero value otherwise.

### GetTypePreferenceOk

`func (o *CampaignStrategyComChannel) GetTypePreferenceOk() (*[]string, bool)`

GetTypePreferenceOk returns a tuple with the TypePreference field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTypePreference

`func (o *CampaignStrategyComChannel) SetTypePreference(v []string)`

SetTypePreference sets TypePreference field to given value.

### HasTypePreference

`func (o *CampaignStrategyComChannel) HasTypePreference() bool`

HasTypePreference returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


