# BillingAccountMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**Type** | Pointer to **string** |  | [optional] 
**TaxId** | Pointer to **string** |  | [optional] 
**ContactInfo** | Pointer to [**BillingContactInfo**](BillingContactInfo.md) |  | [optional] 
**BillingAddress** | Pointer to [**ContactAddress**](ContactAddress.md) |  | [optional] 

## Methods

### NewBillingAccountMutable

`func NewBillingAccountMutable(name string, ) *BillingAccountMutable`

NewBillingAccountMutable instantiates a new BillingAccountMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBillingAccountMutableWithDefaults

`func NewBillingAccountMutableWithDefaults() *BillingAccountMutable`

NewBillingAccountMutableWithDefaults instantiates a new BillingAccountMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *BillingAccountMutable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *BillingAccountMutable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *BillingAccountMutable) SetName(v string)`

SetName sets Name field to given value.


### GetType

`func (o *BillingAccountMutable) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *BillingAccountMutable) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *BillingAccountMutable) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *BillingAccountMutable) HasType() bool`

HasType returns a boolean if a field has been set.

### GetTaxId

`func (o *BillingAccountMutable) GetTaxId() string`

GetTaxId returns the TaxId field if non-nil, zero value otherwise.

### GetTaxIdOk

`func (o *BillingAccountMutable) GetTaxIdOk() (*string, bool)`

GetTaxIdOk returns a tuple with the TaxId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTaxId

`func (o *BillingAccountMutable) SetTaxId(v string)`

SetTaxId sets TaxId field to given value.

### HasTaxId

`func (o *BillingAccountMutable) HasTaxId() bool`

HasTaxId returns a boolean if a field has been set.

### GetContactInfo

`func (o *BillingAccountMutable) GetContactInfo() BillingContactInfo`

GetContactInfo returns the ContactInfo field if non-nil, zero value otherwise.

### GetContactInfoOk

`func (o *BillingAccountMutable) GetContactInfoOk() (*BillingContactInfo, bool)`

GetContactInfoOk returns a tuple with the ContactInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactInfo

`func (o *BillingAccountMutable) SetContactInfo(v BillingContactInfo)`

SetContactInfo sets ContactInfo field to given value.

### HasContactInfo

`func (o *BillingAccountMutable) HasContactInfo() bool`

HasContactInfo returns a boolean if a field has been set.

### GetBillingAddress

`func (o *BillingAccountMutable) GetBillingAddress() ContactAddress`

GetBillingAddress returns the BillingAddress field if non-nil, zero value otherwise.

### GetBillingAddressOk

`func (o *BillingAccountMutable) GetBillingAddressOk() (*ContactAddress, bool)`

GetBillingAddressOk returns a tuple with the BillingAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingAddress

`func (o *BillingAccountMutable) SetBillingAddress(v ContactAddress)`

SetBillingAddress sets BillingAddress field to given value.

### HasBillingAddress

`func (o *BillingAccountMutable) HasBillingAddress() bool`

HasBillingAddress returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


