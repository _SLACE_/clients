# ExternalSystemLink

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SystemName** | **string** | External sytem name as defined in organization settings | 
**UserLink** | **string** | User ID or full profile page URL as defined in organization settings | 

## Methods

### NewExternalSystemLink

`func NewExternalSystemLink(systemName string, userLink string, ) *ExternalSystemLink`

NewExternalSystemLink instantiates a new ExternalSystemLink object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExternalSystemLinkWithDefaults

`func NewExternalSystemLinkWithDefaults() *ExternalSystemLink`

NewExternalSystemLinkWithDefaults instantiates a new ExternalSystemLink object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSystemName

`func (o *ExternalSystemLink) GetSystemName() string`

GetSystemName returns the SystemName field if non-nil, zero value otherwise.

### GetSystemNameOk

`func (o *ExternalSystemLink) GetSystemNameOk() (*string, bool)`

GetSystemNameOk returns a tuple with the SystemName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSystemName

`func (o *ExternalSystemLink) SetSystemName(v string)`

SetSystemName sets SystemName field to given value.


### GetUserLink

`func (o *ExternalSystemLink) GetUserLink() string`

GetUserLink returns the UserLink field if non-nil, zero value otherwise.

### GetUserLinkOk

`func (o *ExternalSystemLink) GetUserLinkOk() (*string, bool)`

GetUserLinkOk returns a tuple with the UserLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserLink

`func (o *ExternalSystemLink) SetUserLink(v string)`

SetUserLink sets UserLink field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


