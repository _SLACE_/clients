# SubscriptionMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Category** | Pointer to [**NotificationCategory**](NotificationCategory.md) |  | [optional] 
**Channel** | Pointer to [**NotificationChannel**](NotificationChannel.md) |  | [optional] 
**MinSeverity** | Pointer to [**NotificationSeverity**](NotificationSeverity.md) |  | [optional] 
**Frequency** | Pointer to [**NotificationFrequency**](NotificationFrequency.md) |  | [optional] 
**Muted** | Pointer to **bool** |  | [optional] 

## Methods

### NewSubscriptionMutable

`func NewSubscriptionMutable() *SubscriptionMutable`

NewSubscriptionMutable instantiates a new SubscriptionMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSubscriptionMutableWithDefaults

`func NewSubscriptionMutableWithDefaults() *SubscriptionMutable`

NewSubscriptionMutableWithDefaults instantiates a new SubscriptionMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCategory

`func (o *SubscriptionMutable) GetCategory() NotificationCategory`

GetCategory returns the Category field if non-nil, zero value otherwise.

### GetCategoryOk

`func (o *SubscriptionMutable) GetCategoryOk() (*NotificationCategory, bool)`

GetCategoryOk returns a tuple with the Category field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCategory

`func (o *SubscriptionMutable) SetCategory(v NotificationCategory)`

SetCategory sets Category field to given value.

### HasCategory

`func (o *SubscriptionMutable) HasCategory() bool`

HasCategory returns a boolean if a field has been set.

### GetChannel

`func (o *SubscriptionMutable) GetChannel() NotificationChannel`

GetChannel returns the Channel field if non-nil, zero value otherwise.

### GetChannelOk

`func (o *SubscriptionMutable) GetChannelOk() (*NotificationChannel, bool)`

GetChannelOk returns a tuple with the Channel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannel

`func (o *SubscriptionMutable) SetChannel(v NotificationChannel)`

SetChannel sets Channel field to given value.

### HasChannel

`func (o *SubscriptionMutable) HasChannel() bool`

HasChannel returns a boolean if a field has been set.

### GetMinSeverity

`func (o *SubscriptionMutable) GetMinSeverity() NotificationSeverity`

GetMinSeverity returns the MinSeverity field if non-nil, zero value otherwise.

### GetMinSeverityOk

`func (o *SubscriptionMutable) GetMinSeverityOk() (*NotificationSeverity, bool)`

GetMinSeverityOk returns a tuple with the MinSeverity field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMinSeverity

`func (o *SubscriptionMutable) SetMinSeverity(v NotificationSeverity)`

SetMinSeverity sets MinSeverity field to given value.

### HasMinSeverity

`func (o *SubscriptionMutable) HasMinSeverity() bool`

HasMinSeverity returns a boolean if a field has been set.

### GetFrequency

`func (o *SubscriptionMutable) GetFrequency() NotificationFrequency`

GetFrequency returns the Frequency field if non-nil, zero value otherwise.

### GetFrequencyOk

`func (o *SubscriptionMutable) GetFrequencyOk() (*NotificationFrequency, bool)`

GetFrequencyOk returns a tuple with the Frequency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFrequency

`func (o *SubscriptionMutable) SetFrequency(v NotificationFrequency)`

SetFrequency sets Frequency field to given value.

### HasFrequency

`func (o *SubscriptionMutable) HasFrequency() bool`

HasFrequency returns a boolean if a field has been set.

### GetMuted

`func (o *SubscriptionMutable) GetMuted() bool`

GetMuted returns the Muted field if non-nil, zero value otherwise.

### GetMutedOk

`func (o *SubscriptionMutable) GetMutedOk() (*bool, bool)`

GetMutedOk returns a tuple with the Muted field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMuted

`func (o *SubscriptionMutable) SetMuted(v bool)`

SetMuted sets Muted field to given value.

### HasMuted

`func (o *SubscriptionMutable) HasMuted() bool`

HasMuted returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


