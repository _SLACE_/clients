# BillingItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OrganizationId** | **string** |  | 
**Organization** | **string** |  | 
**ChannelId** | **string** |  | 
**Channel** | **string** |  | 
**GatewayId** | Pointer to **string** |  | [optional] 
**MessengerId** | Pointer to **string** |  | [optional] 
**Messenger** | Pointer to **string** |  | [optional] 
**Provider** | Pointer to **string** |  | [optional] 
**Quantity** | Pointer to **int32** |  | [optional] 
**ItemPrice** | Pointer to **float32** |  | [optional] 
**Cost** | Pointer to **float32** |  | [optional] 
**Currency** | Pointer to **string** |  | [optional] 
**Country** | Pointer to **string** |  | [optional] 
**Region** | Pointer to **string** |  | [optional] 
**ConversationType** | Pointer to **string** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 

## Methods

### NewBillingItem

`func NewBillingItem(organizationId string, organization string, channelId string, channel string, ) *BillingItem`

NewBillingItem instantiates a new BillingItem object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBillingItemWithDefaults

`func NewBillingItemWithDefaults() *BillingItem`

NewBillingItemWithDefaults instantiates a new BillingItem object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOrganizationId

`func (o *BillingItem) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *BillingItem) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *BillingItem) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetOrganization

`func (o *BillingItem) GetOrganization() string`

GetOrganization returns the Organization field if non-nil, zero value otherwise.

### GetOrganizationOk

`func (o *BillingItem) GetOrganizationOk() (*string, bool)`

GetOrganizationOk returns a tuple with the Organization field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganization

`func (o *BillingItem) SetOrganization(v string)`

SetOrganization sets Organization field to given value.


### GetChannelId

`func (o *BillingItem) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *BillingItem) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *BillingItem) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetChannel

`func (o *BillingItem) GetChannel() string`

GetChannel returns the Channel field if non-nil, zero value otherwise.

### GetChannelOk

`func (o *BillingItem) GetChannelOk() (*string, bool)`

GetChannelOk returns a tuple with the Channel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannel

`func (o *BillingItem) SetChannel(v string)`

SetChannel sets Channel field to given value.


### GetGatewayId

`func (o *BillingItem) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *BillingItem) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *BillingItem) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.

### HasGatewayId

`func (o *BillingItem) HasGatewayId() bool`

HasGatewayId returns a boolean if a field has been set.

### GetMessengerId

`func (o *BillingItem) GetMessengerId() string`

GetMessengerId returns the MessengerId field if non-nil, zero value otherwise.

### GetMessengerIdOk

`func (o *BillingItem) GetMessengerIdOk() (*string, bool)`

GetMessengerIdOk returns a tuple with the MessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerId

`func (o *BillingItem) SetMessengerId(v string)`

SetMessengerId sets MessengerId field to given value.

### HasMessengerId

`func (o *BillingItem) HasMessengerId() bool`

HasMessengerId returns a boolean if a field has been set.

### GetMessenger

`func (o *BillingItem) GetMessenger() string`

GetMessenger returns the Messenger field if non-nil, zero value otherwise.

### GetMessengerOk

`func (o *BillingItem) GetMessengerOk() (*string, bool)`

GetMessengerOk returns a tuple with the Messenger field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessenger

`func (o *BillingItem) SetMessenger(v string)`

SetMessenger sets Messenger field to given value.

### HasMessenger

`func (o *BillingItem) HasMessenger() bool`

HasMessenger returns a boolean if a field has been set.

### GetProvider

`func (o *BillingItem) GetProvider() string`

GetProvider returns the Provider field if non-nil, zero value otherwise.

### GetProviderOk

`func (o *BillingItem) GetProviderOk() (*string, bool)`

GetProviderOk returns a tuple with the Provider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProvider

`func (o *BillingItem) SetProvider(v string)`

SetProvider sets Provider field to given value.

### HasProvider

`func (o *BillingItem) HasProvider() bool`

HasProvider returns a boolean if a field has been set.

### GetQuantity

`func (o *BillingItem) GetQuantity() int32`

GetQuantity returns the Quantity field if non-nil, zero value otherwise.

### GetQuantityOk

`func (o *BillingItem) GetQuantityOk() (*int32, bool)`

GetQuantityOk returns a tuple with the Quantity field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuantity

`func (o *BillingItem) SetQuantity(v int32)`

SetQuantity sets Quantity field to given value.

### HasQuantity

`func (o *BillingItem) HasQuantity() bool`

HasQuantity returns a boolean if a field has been set.

### GetItemPrice

`func (o *BillingItem) GetItemPrice() float32`

GetItemPrice returns the ItemPrice field if non-nil, zero value otherwise.

### GetItemPriceOk

`func (o *BillingItem) GetItemPriceOk() (*float32, bool)`

GetItemPriceOk returns a tuple with the ItemPrice field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItemPrice

`func (o *BillingItem) SetItemPrice(v float32)`

SetItemPrice sets ItemPrice field to given value.

### HasItemPrice

`func (o *BillingItem) HasItemPrice() bool`

HasItemPrice returns a boolean if a field has been set.

### GetCost

`func (o *BillingItem) GetCost() float32`

GetCost returns the Cost field if non-nil, zero value otherwise.

### GetCostOk

`func (o *BillingItem) GetCostOk() (*float32, bool)`

GetCostOk returns a tuple with the Cost field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCost

`func (o *BillingItem) SetCost(v float32)`

SetCost sets Cost field to given value.

### HasCost

`func (o *BillingItem) HasCost() bool`

HasCost returns a boolean if a field has been set.

### GetCurrency

`func (o *BillingItem) GetCurrency() string`

GetCurrency returns the Currency field if non-nil, zero value otherwise.

### GetCurrencyOk

`func (o *BillingItem) GetCurrencyOk() (*string, bool)`

GetCurrencyOk returns a tuple with the Currency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrency

`func (o *BillingItem) SetCurrency(v string)`

SetCurrency sets Currency field to given value.

### HasCurrency

`func (o *BillingItem) HasCurrency() bool`

HasCurrency returns a boolean if a field has been set.

### GetCountry

`func (o *BillingItem) GetCountry() string`

GetCountry returns the Country field if non-nil, zero value otherwise.

### GetCountryOk

`func (o *BillingItem) GetCountryOk() (*string, bool)`

GetCountryOk returns a tuple with the Country field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountry

`func (o *BillingItem) SetCountry(v string)`

SetCountry sets Country field to given value.

### HasCountry

`func (o *BillingItem) HasCountry() bool`

HasCountry returns a boolean if a field has been set.

### GetRegion

`func (o *BillingItem) GetRegion() string`

GetRegion returns the Region field if non-nil, zero value otherwise.

### GetRegionOk

`func (o *BillingItem) GetRegionOk() (*string, bool)`

GetRegionOk returns a tuple with the Region field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRegion

`func (o *BillingItem) SetRegion(v string)`

SetRegion sets Region field to given value.

### HasRegion

`func (o *BillingItem) HasRegion() bool`

HasRegion returns a boolean if a field has been set.

### GetConversationType

`func (o *BillingItem) GetConversationType() string`

GetConversationType returns the ConversationType field if non-nil, zero value otherwise.

### GetConversationTypeOk

`func (o *BillingItem) GetConversationTypeOk() (*string, bool)`

GetConversationTypeOk returns a tuple with the ConversationType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationType

`func (o *BillingItem) SetConversationType(v string)`

SetConversationType sets ConversationType field to given value.

### HasConversationType

`func (o *BillingItem) HasConversationType() bool`

HasConversationType returns a boolean if a field has been set.

### GetType

`func (o *BillingItem) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *BillingItem) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *BillingItem) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *BillingItem) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


