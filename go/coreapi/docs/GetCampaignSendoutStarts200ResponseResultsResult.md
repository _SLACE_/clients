# GetCampaignSendoutStarts200ResponseResultsResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Status** | Pointer to **string** |  | [optional] 
**Errors** | Pointer to **[]string** |  | [optional] 

## Methods

### NewGetCampaignSendoutStarts200ResponseResultsResult

`func NewGetCampaignSendoutStarts200ResponseResultsResult() *GetCampaignSendoutStarts200ResponseResultsResult`

NewGetCampaignSendoutStarts200ResponseResultsResult instantiates a new GetCampaignSendoutStarts200ResponseResultsResult object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCampaignSendoutStarts200ResponseResultsResultWithDefaults

`func NewGetCampaignSendoutStarts200ResponseResultsResultWithDefaults() *GetCampaignSendoutStarts200ResponseResultsResult`

NewGetCampaignSendoutStarts200ResponseResultsResultWithDefaults instantiates a new GetCampaignSendoutStarts200ResponseResultsResult object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStatus

`func (o *GetCampaignSendoutStarts200ResponseResultsResult) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *GetCampaignSendoutStarts200ResponseResultsResult) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *GetCampaignSendoutStarts200ResponseResultsResult) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *GetCampaignSendoutStarts200ResponseResultsResult) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetErrors

`func (o *GetCampaignSendoutStarts200ResponseResultsResult) GetErrors() []string`

GetErrors returns the Errors field if non-nil, zero value otherwise.

### GetErrorsOk

`func (o *GetCampaignSendoutStarts200ResponseResultsResult) GetErrorsOk() (*[]string, bool)`

GetErrorsOk returns a tuple with the Errors field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrors

`func (o *GetCampaignSendoutStarts200ResponseResultsResult) SetErrors(v []string)`

SetErrors sets Errors field to given value.

### HasErrors

`func (o *GetCampaignSendoutStarts200ResponseResultsResult) HasErrors() bool`

HasErrors returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


