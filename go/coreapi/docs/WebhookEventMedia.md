# WebhookEventMedia

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | **string** |  | 
**MimeType** | **string** |  | 
**Status** | **string** |  | 

## Methods

### NewWebhookEventMedia

`func NewWebhookEventMedia(url string, mimeType string, status string, ) *WebhookEventMedia`

NewWebhookEventMedia instantiates a new WebhookEventMedia object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookEventMediaWithDefaults

`func NewWebhookEventMediaWithDefaults() *WebhookEventMedia`

NewWebhookEventMediaWithDefaults instantiates a new WebhookEventMedia object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUrl

`func (o *WebhookEventMedia) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *WebhookEventMedia) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *WebhookEventMedia) SetUrl(v string)`

SetUrl sets Url field to given value.


### GetMimeType

`func (o *WebhookEventMedia) GetMimeType() string`

GetMimeType returns the MimeType field if non-nil, zero value otherwise.

### GetMimeTypeOk

`func (o *WebhookEventMedia) GetMimeTypeOk() (*string, bool)`

GetMimeTypeOk returns a tuple with the MimeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMimeType

`func (o *WebhookEventMedia) SetMimeType(v string)`

SetMimeType sets MimeType field to given value.


### GetStatus

`func (o *WebhookEventMedia) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *WebhookEventMedia) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *WebhookEventMedia) SetStatus(v string)`

SetStatus sets Status field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


