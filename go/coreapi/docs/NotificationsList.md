# NotificationsList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]Notification**](Notification.md) |  | 
**Total** | **int32** |  | 

## Methods

### NewNotificationsList

`func NewNotificationsList(results []Notification, total int32, ) *NotificationsList`

NewNotificationsList instantiates a new NotificationsList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNotificationsListWithDefaults

`func NewNotificationsListWithDefaults() *NotificationsList`

NewNotificationsListWithDefaults instantiates a new NotificationsList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *NotificationsList) GetResults() []Notification`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *NotificationsList) GetResultsOk() (*[]Notification, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *NotificationsList) SetResults(v []Notification)`

SetResults sets Results field to given value.


### GetTotal

`func (o *NotificationsList) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *NotificationsList) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *NotificationsList) SetTotal(v int32)`

SetTotal sets Total field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


