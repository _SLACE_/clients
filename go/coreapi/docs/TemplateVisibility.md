# TemplateVisibility

## Enum


* `TEMPLATE_VISIBILITY_ADMIN` (value: `"admin"`)

* `TEMPLATE_VISIBILITY_INBOX` (value: `"inbox"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


