# GetCampaignSendoutStarts200ResponseResults

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 
**CampaignId** | Pointer to **string** |  | [optional] 
**CampaignSendoutId** | Pointer to **string** |  | [optional] 
**ContactConversations** | Pointer to **map[string]interface{}** |  | [optional] 
**CampaignSnapshot** | Pointer to **int32** |  | [optional] 
**EstimationCount** | Pointer to **int32** |  | [optional] 
**ExecutionCount** | Pointer to **int32** |  | [optional] 
**RescheduleCount** | Pointer to **int32** |  | [optional] 
**MarketingOptOutCount** | Pointer to **int32** |  | [optional] 
**OptOutCount** | Pointer to **int32** |  | [optional] 
**UnreachableCount** | Pointer to **int32** |  | [optional] 
**Progress** | Pointer to **int32** |  | [optional] 
**EstimatedCostCents** | Pointer to **int32** |  | [optional] 
**Result** | Pointer to [**GetCampaignSendoutStarts200ResponseResultsResult**](GetCampaignSendoutStarts200ResponseResultsResult.md) |  | [optional] 

## Methods

### NewGetCampaignSendoutStarts200ResponseResults

`func NewGetCampaignSendoutStarts200ResponseResults() *GetCampaignSendoutStarts200ResponseResults`

NewGetCampaignSendoutStarts200ResponseResults instantiates a new GetCampaignSendoutStarts200ResponseResults object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCampaignSendoutStarts200ResponseResultsWithDefaults

`func NewGetCampaignSendoutStarts200ResponseResultsWithDefaults() *GetCampaignSendoutStarts200ResponseResults`

NewGetCampaignSendoutStarts200ResponseResultsWithDefaults instantiates a new GetCampaignSendoutStarts200ResponseResults object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *GetCampaignSendoutStarts200ResponseResults) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *GetCampaignSendoutStarts200ResponseResults) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *GetCampaignSendoutStarts200ResponseResults) HasId() bool`

HasId returns a boolean if a field has been set.

### GetType

`func (o *GetCampaignSendoutStarts200ResponseResults) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *GetCampaignSendoutStarts200ResponseResults) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *GetCampaignSendoutStarts200ResponseResults) HasType() bool`

HasType returns a boolean if a field has been set.

### GetCampaignId

`func (o *GetCampaignSendoutStarts200ResponseResults) GetCampaignId() string`

GetCampaignId returns the CampaignId field if non-nil, zero value otherwise.

### GetCampaignIdOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetCampaignIdOk() (*string, bool)`

GetCampaignIdOk returns a tuple with the CampaignId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCampaignId

`func (o *GetCampaignSendoutStarts200ResponseResults) SetCampaignId(v string)`

SetCampaignId sets CampaignId field to given value.

### HasCampaignId

`func (o *GetCampaignSendoutStarts200ResponseResults) HasCampaignId() bool`

HasCampaignId returns a boolean if a field has been set.

### GetCampaignSendoutId

`func (o *GetCampaignSendoutStarts200ResponseResults) GetCampaignSendoutId() string`

GetCampaignSendoutId returns the CampaignSendoutId field if non-nil, zero value otherwise.

### GetCampaignSendoutIdOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetCampaignSendoutIdOk() (*string, bool)`

GetCampaignSendoutIdOk returns a tuple with the CampaignSendoutId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCampaignSendoutId

`func (o *GetCampaignSendoutStarts200ResponseResults) SetCampaignSendoutId(v string)`

SetCampaignSendoutId sets CampaignSendoutId field to given value.

### HasCampaignSendoutId

`func (o *GetCampaignSendoutStarts200ResponseResults) HasCampaignSendoutId() bool`

HasCampaignSendoutId returns a boolean if a field has been set.

### GetContactConversations

`func (o *GetCampaignSendoutStarts200ResponseResults) GetContactConversations() map[string]interface{}`

GetContactConversations returns the ContactConversations field if non-nil, zero value otherwise.

### GetContactConversationsOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetContactConversationsOk() (*map[string]interface{}, bool)`

GetContactConversationsOk returns a tuple with the ContactConversations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactConversations

`func (o *GetCampaignSendoutStarts200ResponseResults) SetContactConversations(v map[string]interface{})`

SetContactConversations sets ContactConversations field to given value.

### HasContactConversations

`func (o *GetCampaignSendoutStarts200ResponseResults) HasContactConversations() bool`

HasContactConversations returns a boolean if a field has been set.

### GetCampaignSnapshot

`func (o *GetCampaignSendoutStarts200ResponseResults) GetCampaignSnapshot() int32`

GetCampaignSnapshot returns the CampaignSnapshot field if non-nil, zero value otherwise.

### GetCampaignSnapshotOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetCampaignSnapshotOk() (*int32, bool)`

GetCampaignSnapshotOk returns a tuple with the CampaignSnapshot field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCampaignSnapshot

`func (o *GetCampaignSendoutStarts200ResponseResults) SetCampaignSnapshot(v int32)`

SetCampaignSnapshot sets CampaignSnapshot field to given value.

### HasCampaignSnapshot

`func (o *GetCampaignSendoutStarts200ResponseResults) HasCampaignSnapshot() bool`

HasCampaignSnapshot returns a boolean if a field has been set.

### GetEstimationCount

`func (o *GetCampaignSendoutStarts200ResponseResults) GetEstimationCount() int32`

GetEstimationCount returns the EstimationCount field if non-nil, zero value otherwise.

### GetEstimationCountOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetEstimationCountOk() (*int32, bool)`

GetEstimationCountOk returns a tuple with the EstimationCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEstimationCount

`func (o *GetCampaignSendoutStarts200ResponseResults) SetEstimationCount(v int32)`

SetEstimationCount sets EstimationCount field to given value.

### HasEstimationCount

`func (o *GetCampaignSendoutStarts200ResponseResults) HasEstimationCount() bool`

HasEstimationCount returns a boolean if a field has been set.

### GetExecutionCount

`func (o *GetCampaignSendoutStarts200ResponseResults) GetExecutionCount() int32`

GetExecutionCount returns the ExecutionCount field if non-nil, zero value otherwise.

### GetExecutionCountOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetExecutionCountOk() (*int32, bool)`

GetExecutionCountOk returns a tuple with the ExecutionCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExecutionCount

`func (o *GetCampaignSendoutStarts200ResponseResults) SetExecutionCount(v int32)`

SetExecutionCount sets ExecutionCount field to given value.

### HasExecutionCount

`func (o *GetCampaignSendoutStarts200ResponseResults) HasExecutionCount() bool`

HasExecutionCount returns a boolean if a field has been set.

### GetRescheduleCount

`func (o *GetCampaignSendoutStarts200ResponseResults) GetRescheduleCount() int32`

GetRescheduleCount returns the RescheduleCount field if non-nil, zero value otherwise.

### GetRescheduleCountOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetRescheduleCountOk() (*int32, bool)`

GetRescheduleCountOk returns a tuple with the RescheduleCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRescheduleCount

`func (o *GetCampaignSendoutStarts200ResponseResults) SetRescheduleCount(v int32)`

SetRescheduleCount sets RescheduleCount field to given value.

### HasRescheduleCount

`func (o *GetCampaignSendoutStarts200ResponseResults) HasRescheduleCount() bool`

HasRescheduleCount returns a boolean if a field has been set.

### GetMarketingOptOutCount

`func (o *GetCampaignSendoutStarts200ResponseResults) GetMarketingOptOutCount() int32`

GetMarketingOptOutCount returns the MarketingOptOutCount field if non-nil, zero value otherwise.

### GetMarketingOptOutCountOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetMarketingOptOutCountOk() (*int32, bool)`

GetMarketingOptOutCountOk returns a tuple with the MarketingOptOutCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMarketingOptOutCount

`func (o *GetCampaignSendoutStarts200ResponseResults) SetMarketingOptOutCount(v int32)`

SetMarketingOptOutCount sets MarketingOptOutCount field to given value.

### HasMarketingOptOutCount

`func (o *GetCampaignSendoutStarts200ResponseResults) HasMarketingOptOutCount() bool`

HasMarketingOptOutCount returns a boolean if a field has been set.

### GetOptOutCount

`func (o *GetCampaignSendoutStarts200ResponseResults) GetOptOutCount() int32`

GetOptOutCount returns the OptOutCount field if non-nil, zero value otherwise.

### GetOptOutCountOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetOptOutCountOk() (*int32, bool)`

GetOptOutCountOk returns a tuple with the OptOutCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptOutCount

`func (o *GetCampaignSendoutStarts200ResponseResults) SetOptOutCount(v int32)`

SetOptOutCount sets OptOutCount field to given value.

### HasOptOutCount

`func (o *GetCampaignSendoutStarts200ResponseResults) HasOptOutCount() bool`

HasOptOutCount returns a boolean if a field has been set.

### GetUnreachableCount

`func (o *GetCampaignSendoutStarts200ResponseResults) GetUnreachableCount() int32`

GetUnreachableCount returns the UnreachableCount field if non-nil, zero value otherwise.

### GetUnreachableCountOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetUnreachableCountOk() (*int32, bool)`

GetUnreachableCountOk returns a tuple with the UnreachableCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnreachableCount

`func (o *GetCampaignSendoutStarts200ResponseResults) SetUnreachableCount(v int32)`

SetUnreachableCount sets UnreachableCount field to given value.

### HasUnreachableCount

`func (o *GetCampaignSendoutStarts200ResponseResults) HasUnreachableCount() bool`

HasUnreachableCount returns a boolean if a field has been set.

### GetProgress

`func (o *GetCampaignSendoutStarts200ResponseResults) GetProgress() int32`

GetProgress returns the Progress field if non-nil, zero value otherwise.

### GetProgressOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetProgressOk() (*int32, bool)`

GetProgressOk returns a tuple with the Progress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProgress

`func (o *GetCampaignSendoutStarts200ResponseResults) SetProgress(v int32)`

SetProgress sets Progress field to given value.

### HasProgress

`func (o *GetCampaignSendoutStarts200ResponseResults) HasProgress() bool`

HasProgress returns a boolean if a field has been set.

### GetEstimatedCostCents

`func (o *GetCampaignSendoutStarts200ResponseResults) GetEstimatedCostCents() int32`

GetEstimatedCostCents returns the EstimatedCostCents field if non-nil, zero value otherwise.

### GetEstimatedCostCentsOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetEstimatedCostCentsOk() (*int32, bool)`

GetEstimatedCostCentsOk returns a tuple with the EstimatedCostCents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEstimatedCostCents

`func (o *GetCampaignSendoutStarts200ResponseResults) SetEstimatedCostCents(v int32)`

SetEstimatedCostCents sets EstimatedCostCents field to given value.

### HasEstimatedCostCents

`func (o *GetCampaignSendoutStarts200ResponseResults) HasEstimatedCostCents() bool`

HasEstimatedCostCents returns a boolean if a field has been set.

### GetResult

`func (o *GetCampaignSendoutStarts200ResponseResults) GetResult() GetCampaignSendoutStarts200ResponseResultsResult`

GetResult returns the Result field if non-nil, zero value otherwise.

### GetResultOk

`func (o *GetCampaignSendoutStarts200ResponseResults) GetResultOk() (*GetCampaignSendoutStarts200ResponseResultsResult, bool)`

GetResultOk returns a tuple with the Result field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResult

`func (o *GetCampaignSendoutStarts200ResponseResults) SetResult(v GetCampaignSendoutStarts200ResponseResultsResult)`

SetResult sets Result field to given value.

### HasResult

`func (o *GetCampaignSendoutStarts200ResponseResults) HasResult() bool`

HasResult returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


