# CommunicationChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**OrganizationId** | **string** |  | 
**ContactId** | Pointer to **string** |  | [optional] 
**Type** | **string** |  | 
**Name** | **string** |  | 
**ProfilePhoto** | Pointer to **string** |  | [optional] 
**MessengerId** | **string** |  | 
**MessengerType** | [**Messenger**](Messenger.md) |  | 
**Permission** | Pointer to [**ComChannelPermission**](ComChannelPermission.md) |  | [optional] 
**CreatedAt** | **string** |  | 
**UpdatedAt** | **string** |  | 
**Conversations** | Pointer to [**[]Conversation**](Conversation.md) |  | [optional] 

## Methods

### NewCommunicationChannel

`func NewCommunicationChannel(id string, organizationId string, type_ string, name string, messengerId string, messengerType Messenger, createdAt string, updatedAt string, ) *CommunicationChannel`

NewCommunicationChannel instantiates a new CommunicationChannel object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCommunicationChannelWithDefaults

`func NewCommunicationChannelWithDefaults() *CommunicationChannel`

NewCommunicationChannelWithDefaults instantiates a new CommunicationChannel object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CommunicationChannel) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CommunicationChannel) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CommunicationChannel) SetId(v string)`

SetId sets Id field to given value.


### GetOrganizationId

`func (o *CommunicationChannel) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *CommunicationChannel) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *CommunicationChannel) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetContactId

`func (o *CommunicationChannel) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *CommunicationChannel) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *CommunicationChannel) SetContactId(v string)`

SetContactId sets ContactId field to given value.

### HasContactId

`func (o *CommunicationChannel) HasContactId() bool`

HasContactId returns a boolean if a field has been set.

### GetType

`func (o *CommunicationChannel) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *CommunicationChannel) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *CommunicationChannel) SetType(v string)`

SetType sets Type field to given value.


### GetName

`func (o *CommunicationChannel) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CommunicationChannel) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CommunicationChannel) SetName(v string)`

SetName sets Name field to given value.


### GetProfilePhoto

`func (o *CommunicationChannel) GetProfilePhoto() string`

GetProfilePhoto returns the ProfilePhoto field if non-nil, zero value otherwise.

### GetProfilePhotoOk

`func (o *CommunicationChannel) GetProfilePhotoOk() (*string, bool)`

GetProfilePhotoOk returns a tuple with the ProfilePhoto field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProfilePhoto

`func (o *CommunicationChannel) SetProfilePhoto(v string)`

SetProfilePhoto sets ProfilePhoto field to given value.

### HasProfilePhoto

`func (o *CommunicationChannel) HasProfilePhoto() bool`

HasProfilePhoto returns a boolean if a field has been set.

### GetMessengerId

`func (o *CommunicationChannel) GetMessengerId() string`

GetMessengerId returns the MessengerId field if non-nil, zero value otherwise.

### GetMessengerIdOk

`func (o *CommunicationChannel) GetMessengerIdOk() (*string, bool)`

GetMessengerIdOk returns a tuple with the MessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerId

`func (o *CommunicationChannel) SetMessengerId(v string)`

SetMessengerId sets MessengerId field to given value.


### GetMessengerType

`func (o *CommunicationChannel) GetMessengerType() Messenger`

GetMessengerType returns the MessengerType field if non-nil, zero value otherwise.

### GetMessengerTypeOk

`func (o *CommunicationChannel) GetMessengerTypeOk() (*Messenger, bool)`

GetMessengerTypeOk returns a tuple with the MessengerType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerType

`func (o *CommunicationChannel) SetMessengerType(v Messenger)`

SetMessengerType sets MessengerType field to given value.


### GetPermission

`func (o *CommunicationChannel) GetPermission() ComChannelPermission`

GetPermission returns the Permission field if non-nil, zero value otherwise.

### GetPermissionOk

`func (o *CommunicationChannel) GetPermissionOk() (*ComChannelPermission, bool)`

GetPermissionOk returns a tuple with the Permission field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermission

`func (o *CommunicationChannel) SetPermission(v ComChannelPermission)`

SetPermission sets Permission field to given value.

### HasPermission

`func (o *CommunicationChannel) HasPermission() bool`

HasPermission returns a boolean if a field has been set.

### GetCreatedAt

`func (o *CommunicationChannel) GetCreatedAt() string`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *CommunicationChannel) GetCreatedAtOk() (*string, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *CommunicationChannel) SetCreatedAt(v string)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *CommunicationChannel) GetUpdatedAt() string`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *CommunicationChannel) GetUpdatedAtOk() (*string, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *CommunicationChannel) SetUpdatedAt(v string)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetConversations

`func (o *CommunicationChannel) GetConversations() []Conversation`

GetConversations returns the Conversations field if non-nil, zero value otherwise.

### GetConversationsOk

`func (o *CommunicationChannel) GetConversationsOk() (*[]Conversation, bool)`

GetConversationsOk returns a tuple with the Conversations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversations

`func (o *CommunicationChannel) SetConversations(v []Conversation)`

SetConversations sets Conversations field to given value.

### HasConversations

`func (o *CommunicationChannel) HasConversations() bool`

HasConversations returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


