# MessageItemType

## Enum


* `ITEM` (value: `"item"`)

* `SECTION` (value: `"section"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


