# GatewayDraftCreateData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelId** | **string** |  | 
**Type** | [**Messenger**](Messenger.md) |  | 
**Provider** | [**Provider**](Provider.md) |  | 

## Methods

### NewGatewayDraftCreateData

`func NewGatewayDraftCreateData(channelId string, type_ Messenger, provider Provider, ) *GatewayDraftCreateData`

NewGatewayDraftCreateData instantiates a new GatewayDraftCreateData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGatewayDraftCreateDataWithDefaults

`func NewGatewayDraftCreateDataWithDefaults() *GatewayDraftCreateData`

NewGatewayDraftCreateDataWithDefaults instantiates a new GatewayDraftCreateData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetChannelId

`func (o *GatewayDraftCreateData) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *GatewayDraftCreateData) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *GatewayDraftCreateData) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetType

`func (o *GatewayDraftCreateData) GetType() Messenger`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *GatewayDraftCreateData) GetTypeOk() (*Messenger, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *GatewayDraftCreateData) SetType(v Messenger)`

SetType sets Type field to given value.


### GetProvider

`func (o *GatewayDraftCreateData) GetProvider() Provider`

GetProvider returns the Provider field if non-nil, zero value otherwise.

### GetProviderOk

`func (o *GatewayDraftCreateData) GetProviderOk() (*Provider, bool)`

GetProviderOk returns a tuple with the Provider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProvider

`func (o *GatewayDraftCreateData) SetProvider(v Provider)`

SetProvider sets Provider field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


