# Contact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | Profile name. Contacts created automatically out of conversations have messenger profile name here | 
**Timezone** | Pointer to **string** | Contact timezone in IANA format | [optional] 
**Mobile** | Pointer to **string** | Mobile number in E.164 format | [optional] 
**MobileVerified** | Pointer to **bool** | NOT EDITABLE VIA API  Flag to mark mobile number as verified | [optional] 
**FirstName** | Pointer to **string** |  | [optional] 
**LastName** | Pointer to **string** |  | [optional] 
**Email** | Pointer to **string** |  | [optional] 
**Gender** | Pointer to **string** |  | [optional] 
**Title** | Pointer to **string** |  | [optional] 
**Formal** | Pointer to **bool** |  | [optional] 
**Birthdate** | Pointer to **string** | Birthdate in format specified in organization settings. Default is yyyy-mm-dd | [optional] 
**DayOfBirth** | Pointer to **int32** | Overriden with birthdate data if provided | [optional] 
**MonthOfBirth** | Pointer to **int32** | Overriden with birthdate data if provided | [optional] 
**Address** | Pointer to [**ContactAddress**](ContactAddress.md) |  | [optional] 
**LoyaltyPoints** | Pointer to **map[string]int32** | NOT EDITABLE VIA API  Stores current loyalty points of a contact. Format [providerName_projectId]: [pointsAmount] } | [optional] 
**LifetimeLoyaltyPoints** | Pointer to **map[string]int32** | NOT EDITABLE VIA API  Stores lifetime loyalty points of a contact. Format { [providerName_projectId]: [pointsAmount] } | [optional] 
**StrictAttributes** | Pointer to [**map[string]ContactStrictAttribute**](ContactStrictAttribute.md) | JSON with custom properties as defined in organization settings. Format: { [propertyName]: { type: [string|float|bool|date|datetime], value: [propertyValue] } } | [optional] 
**CareOf** | Pointer to **string** |  | [optional] 
**Avatars** | Pointer to [**ContactAvatars**](ContactAvatars.md) |  | [optional] 
**Languages** | Pointer to [**ContactLanguages**](ContactLanguages.md) |  | [optional] 
**MobileVerifiedAt** | Pointer to **string** | NOT EDITABLE VIA API  Timestamp when mobile number was marked as verified | [optional] 
**ExternalIds** | Pointer to **map[string]interface{}** | NOT EDITABLE VIA API  JSON with external IDs. Format: { [system_name]: [user_id] } | [optional] 
**ErConfidence** | Pointer to [**ContactERConfidence**](ContactERConfidence.md) |  | [optional] 
**Id** | **string** |  | 
**OrganizationId** | **string** |  | 
**Segments** | Pointer to [**[]Segment**](Segment.md) | NOT EDITABLE VIA API | [optional] 
**Active** | **bool** | NOT EDITABLE VIA API  Based on contact chat activity. Contact is &#39;active&#39; if his last message is not older than activity threshold configured in Organization | 
**Status** | **string** | NOT EDITABLE VIA API  Indicator whether contact is active or deleted | 
**LastActiveAt** | Pointer to **time.Time** | NOT EDITABLE VIA API  Timestamp of last contact activity, usually message sent to SLACE conversation | [optional] 
**ExternalLinks** | Pointer to [**[]ExternalSystemLink**](ExternalSystemLink.md) | NOT EDITABLE VIA API | [optional] 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 
**ComChannels** | Pointer to [**[]CommunicationChannel**](CommunicationChannel.md) | NOT EDITABLE VIA API | [optional] 
**PlatformComChannels** | Pointer to [**[]PlatformCommunicationChannel**](PlatformCommunicationChannel.md) | NOT EDITABLE VIA API | [optional] 
**Coupons** | Pointer to [**[]Coupon**](Coupon.md) | NOT EDITABLE VIA API | [optional] 
**Devices** | Pointer to [**[]ContactDevices**](ContactDevices.md) | NOT EDITABLE VIA API | [optional] 
**MobileDevice** | Pointer to [**ContactDevices**](ContactDevices.md) |  | [optional] 
**ImportBatchId** | Pointer to **string** | NOT EDITABLE VIA API  Imported contacts contain reference to the import batch (unique ID of single import) | [optional] 
**Age** | Pointer to **int32** | Age (in full years) based on birthdate | [optional] 
**GivenConsents** | Pointer to [**ContactAllOfGivenConsents**](ContactAllOfGivenConsents.md) |  | [optional] 
**ConsentKeys** | **[]string** |  | 

## Methods

### NewContact

`func NewContact(name string, id string, organizationId string, active bool, status string, createdAt time.Time, updatedAt time.Time, consentKeys []string, ) *Contact`

NewContact instantiates a new Contact object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactWithDefaults

`func NewContactWithDefaults() *Contact`

NewContactWithDefaults instantiates a new Contact object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *Contact) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Contact) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Contact) SetName(v string)`

SetName sets Name field to given value.


### GetTimezone

`func (o *Contact) GetTimezone() string`

GetTimezone returns the Timezone field if non-nil, zero value otherwise.

### GetTimezoneOk

`func (o *Contact) GetTimezoneOk() (*string, bool)`

GetTimezoneOk returns a tuple with the Timezone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimezone

`func (o *Contact) SetTimezone(v string)`

SetTimezone sets Timezone field to given value.

### HasTimezone

`func (o *Contact) HasTimezone() bool`

HasTimezone returns a boolean if a field has been set.

### GetMobile

`func (o *Contact) GetMobile() string`

GetMobile returns the Mobile field if non-nil, zero value otherwise.

### GetMobileOk

`func (o *Contact) GetMobileOk() (*string, bool)`

GetMobileOk returns a tuple with the Mobile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobile

`func (o *Contact) SetMobile(v string)`

SetMobile sets Mobile field to given value.

### HasMobile

`func (o *Contact) HasMobile() bool`

HasMobile returns a boolean if a field has been set.

### GetMobileVerified

`func (o *Contact) GetMobileVerified() bool`

GetMobileVerified returns the MobileVerified field if non-nil, zero value otherwise.

### GetMobileVerifiedOk

`func (o *Contact) GetMobileVerifiedOk() (*bool, bool)`

GetMobileVerifiedOk returns a tuple with the MobileVerified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobileVerified

`func (o *Contact) SetMobileVerified(v bool)`

SetMobileVerified sets MobileVerified field to given value.

### HasMobileVerified

`func (o *Contact) HasMobileVerified() bool`

HasMobileVerified returns a boolean if a field has been set.

### GetFirstName

`func (o *Contact) GetFirstName() string`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *Contact) GetFirstNameOk() (*string, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *Contact) SetFirstName(v string)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *Contact) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *Contact) GetLastName() string`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *Contact) GetLastNameOk() (*string, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *Contact) SetLastName(v string)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *Contact) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetEmail

`func (o *Contact) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *Contact) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *Contact) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *Contact) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetGender

`func (o *Contact) GetGender() string`

GetGender returns the Gender field if non-nil, zero value otherwise.

### GetGenderOk

`func (o *Contact) GetGenderOk() (*string, bool)`

GetGenderOk returns a tuple with the Gender field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGender

`func (o *Contact) SetGender(v string)`

SetGender sets Gender field to given value.

### HasGender

`func (o *Contact) HasGender() bool`

HasGender returns a boolean if a field has been set.

### GetTitle

`func (o *Contact) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *Contact) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *Contact) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *Contact) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetFormal

`func (o *Contact) GetFormal() bool`

GetFormal returns the Formal field if non-nil, zero value otherwise.

### GetFormalOk

`func (o *Contact) GetFormalOk() (*bool, bool)`

GetFormalOk returns a tuple with the Formal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormal

`func (o *Contact) SetFormal(v bool)`

SetFormal sets Formal field to given value.

### HasFormal

`func (o *Contact) HasFormal() bool`

HasFormal returns a boolean if a field has been set.

### GetBirthdate

`func (o *Contact) GetBirthdate() string`

GetBirthdate returns the Birthdate field if non-nil, zero value otherwise.

### GetBirthdateOk

`func (o *Contact) GetBirthdateOk() (*string, bool)`

GetBirthdateOk returns a tuple with the Birthdate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBirthdate

`func (o *Contact) SetBirthdate(v string)`

SetBirthdate sets Birthdate field to given value.

### HasBirthdate

`func (o *Contact) HasBirthdate() bool`

HasBirthdate returns a boolean if a field has been set.

### GetDayOfBirth

`func (o *Contact) GetDayOfBirth() int32`

GetDayOfBirth returns the DayOfBirth field if non-nil, zero value otherwise.

### GetDayOfBirthOk

`func (o *Contact) GetDayOfBirthOk() (*int32, bool)`

GetDayOfBirthOk returns a tuple with the DayOfBirth field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDayOfBirth

`func (o *Contact) SetDayOfBirth(v int32)`

SetDayOfBirth sets DayOfBirth field to given value.

### HasDayOfBirth

`func (o *Contact) HasDayOfBirth() bool`

HasDayOfBirth returns a boolean if a field has been set.

### GetMonthOfBirth

`func (o *Contact) GetMonthOfBirth() int32`

GetMonthOfBirth returns the MonthOfBirth field if non-nil, zero value otherwise.

### GetMonthOfBirthOk

`func (o *Contact) GetMonthOfBirthOk() (*int32, bool)`

GetMonthOfBirthOk returns a tuple with the MonthOfBirth field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMonthOfBirth

`func (o *Contact) SetMonthOfBirth(v int32)`

SetMonthOfBirth sets MonthOfBirth field to given value.

### HasMonthOfBirth

`func (o *Contact) HasMonthOfBirth() bool`

HasMonthOfBirth returns a boolean if a field has been set.

### GetAddress

`func (o *Contact) GetAddress() ContactAddress`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *Contact) GetAddressOk() (*ContactAddress, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *Contact) SetAddress(v ContactAddress)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *Contact) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetLoyaltyPoints

`func (o *Contact) GetLoyaltyPoints() map[string]int32`

GetLoyaltyPoints returns the LoyaltyPoints field if non-nil, zero value otherwise.

### GetLoyaltyPointsOk

`func (o *Contact) GetLoyaltyPointsOk() (*map[string]int32, bool)`

GetLoyaltyPointsOk returns a tuple with the LoyaltyPoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLoyaltyPoints

`func (o *Contact) SetLoyaltyPoints(v map[string]int32)`

SetLoyaltyPoints sets LoyaltyPoints field to given value.

### HasLoyaltyPoints

`func (o *Contact) HasLoyaltyPoints() bool`

HasLoyaltyPoints returns a boolean if a field has been set.

### GetLifetimeLoyaltyPoints

`func (o *Contact) GetLifetimeLoyaltyPoints() map[string]int32`

GetLifetimeLoyaltyPoints returns the LifetimeLoyaltyPoints field if non-nil, zero value otherwise.

### GetLifetimeLoyaltyPointsOk

`func (o *Contact) GetLifetimeLoyaltyPointsOk() (*map[string]int32, bool)`

GetLifetimeLoyaltyPointsOk returns a tuple with the LifetimeLoyaltyPoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLifetimeLoyaltyPoints

`func (o *Contact) SetLifetimeLoyaltyPoints(v map[string]int32)`

SetLifetimeLoyaltyPoints sets LifetimeLoyaltyPoints field to given value.

### HasLifetimeLoyaltyPoints

`func (o *Contact) HasLifetimeLoyaltyPoints() bool`

HasLifetimeLoyaltyPoints returns a boolean if a field has been set.

### GetStrictAttributes

`func (o *Contact) GetStrictAttributes() map[string]ContactStrictAttribute`

GetStrictAttributes returns the StrictAttributes field if non-nil, zero value otherwise.

### GetStrictAttributesOk

`func (o *Contact) GetStrictAttributesOk() (*map[string]ContactStrictAttribute, bool)`

GetStrictAttributesOk returns a tuple with the StrictAttributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStrictAttributes

`func (o *Contact) SetStrictAttributes(v map[string]ContactStrictAttribute)`

SetStrictAttributes sets StrictAttributes field to given value.

### HasStrictAttributes

`func (o *Contact) HasStrictAttributes() bool`

HasStrictAttributes returns a boolean if a field has been set.

### GetCareOf

`func (o *Contact) GetCareOf() string`

GetCareOf returns the CareOf field if non-nil, zero value otherwise.

### GetCareOfOk

`func (o *Contact) GetCareOfOk() (*string, bool)`

GetCareOfOk returns a tuple with the CareOf field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCareOf

`func (o *Contact) SetCareOf(v string)`

SetCareOf sets CareOf field to given value.

### HasCareOf

`func (o *Contact) HasCareOf() bool`

HasCareOf returns a boolean if a field has been set.

### GetAvatars

`func (o *Contact) GetAvatars() ContactAvatars`

GetAvatars returns the Avatars field if non-nil, zero value otherwise.

### GetAvatarsOk

`func (o *Contact) GetAvatarsOk() (*ContactAvatars, bool)`

GetAvatarsOk returns a tuple with the Avatars field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAvatars

`func (o *Contact) SetAvatars(v ContactAvatars)`

SetAvatars sets Avatars field to given value.

### HasAvatars

`func (o *Contact) HasAvatars() bool`

HasAvatars returns a boolean if a field has been set.

### GetLanguages

`func (o *Contact) GetLanguages() ContactLanguages`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *Contact) GetLanguagesOk() (*ContactLanguages, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *Contact) SetLanguages(v ContactLanguages)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *Contact) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetMobileVerifiedAt

`func (o *Contact) GetMobileVerifiedAt() string`

GetMobileVerifiedAt returns the MobileVerifiedAt field if non-nil, zero value otherwise.

### GetMobileVerifiedAtOk

`func (o *Contact) GetMobileVerifiedAtOk() (*string, bool)`

GetMobileVerifiedAtOk returns a tuple with the MobileVerifiedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobileVerifiedAt

`func (o *Contact) SetMobileVerifiedAt(v string)`

SetMobileVerifiedAt sets MobileVerifiedAt field to given value.

### HasMobileVerifiedAt

`func (o *Contact) HasMobileVerifiedAt() bool`

HasMobileVerifiedAt returns a boolean if a field has been set.

### GetExternalIds

`func (o *Contact) GetExternalIds() map[string]interface{}`

GetExternalIds returns the ExternalIds field if non-nil, zero value otherwise.

### GetExternalIdsOk

`func (o *Contact) GetExternalIdsOk() (*map[string]interface{}, bool)`

GetExternalIdsOk returns a tuple with the ExternalIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalIds

`func (o *Contact) SetExternalIds(v map[string]interface{})`

SetExternalIds sets ExternalIds field to given value.

### HasExternalIds

`func (o *Contact) HasExternalIds() bool`

HasExternalIds returns a boolean if a field has been set.

### GetErConfidence

`func (o *Contact) GetErConfidence() ContactERConfidence`

GetErConfidence returns the ErConfidence field if non-nil, zero value otherwise.

### GetErConfidenceOk

`func (o *Contact) GetErConfidenceOk() (*ContactERConfidence, bool)`

GetErConfidenceOk returns a tuple with the ErConfidence field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErConfidence

`func (o *Contact) SetErConfidence(v ContactERConfidence)`

SetErConfidence sets ErConfidence field to given value.

### HasErConfidence

`func (o *Contact) HasErConfidence() bool`

HasErConfidence returns a boolean if a field has been set.

### GetId

`func (o *Contact) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Contact) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Contact) SetId(v string)`

SetId sets Id field to given value.


### GetOrganizationId

`func (o *Contact) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *Contact) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *Contact) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetSegments

`func (o *Contact) GetSegments() []Segment`

GetSegments returns the Segments field if non-nil, zero value otherwise.

### GetSegmentsOk

`func (o *Contact) GetSegmentsOk() (*[]Segment, bool)`

GetSegmentsOk returns a tuple with the Segments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSegments

`func (o *Contact) SetSegments(v []Segment)`

SetSegments sets Segments field to given value.

### HasSegments

`func (o *Contact) HasSegments() bool`

HasSegments returns a boolean if a field has been set.

### GetActive

`func (o *Contact) GetActive() bool`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *Contact) GetActiveOk() (*bool, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *Contact) SetActive(v bool)`

SetActive sets Active field to given value.


### GetStatus

`func (o *Contact) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *Contact) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *Contact) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetLastActiveAt

`func (o *Contact) GetLastActiveAt() time.Time`

GetLastActiveAt returns the LastActiveAt field if non-nil, zero value otherwise.

### GetLastActiveAtOk

`func (o *Contact) GetLastActiveAtOk() (*time.Time, bool)`

GetLastActiveAtOk returns a tuple with the LastActiveAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastActiveAt

`func (o *Contact) SetLastActiveAt(v time.Time)`

SetLastActiveAt sets LastActiveAt field to given value.

### HasLastActiveAt

`func (o *Contact) HasLastActiveAt() bool`

HasLastActiveAt returns a boolean if a field has been set.

### GetExternalLinks

`func (o *Contact) GetExternalLinks() []ExternalSystemLink`

GetExternalLinks returns the ExternalLinks field if non-nil, zero value otherwise.

### GetExternalLinksOk

`func (o *Contact) GetExternalLinksOk() (*[]ExternalSystemLink, bool)`

GetExternalLinksOk returns a tuple with the ExternalLinks field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalLinks

`func (o *Contact) SetExternalLinks(v []ExternalSystemLink)`

SetExternalLinks sets ExternalLinks field to given value.

### HasExternalLinks

`func (o *Contact) HasExternalLinks() bool`

HasExternalLinks returns a boolean if a field has been set.

### GetCreatedAt

`func (o *Contact) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Contact) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Contact) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *Contact) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Contact) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Contact) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetComChannels

`func (o *Contact) GetComChannels() []CommunicationChannel`

GetComChannels returns the ComChannels field if non-nil, zero value otherwise.

### GetComChannelsOk

`func (o *Contact) GetComChannelsOk() (*[]CommunicationChannel, bool)`

GetComChannelsOk returns a tuple with the ComChannels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComChannels

`func (o *Contact) SetComChannels(v []CommunicationChannel)`

SetComChannels sets ComChannels field to given value.

### HasComChannels

`func (o *Contact) HasComChannels() bool`

HasComChannels returns a boolean if a field has been set.

### GetPlatformComChannels

`func (o *Contact) GetPlatformComChannels() []PlatformCommunicationChannel`

GetPlatformComChannels returns the PlatformComChannels field if non-nil, zero value otherwise.

### GetPlatformComChannelsOk

`func (o *Contact) GetPlatformComChannelsOk() (*[]PlatformCommunicationChannel, bool)`

GetPlatformComChannelsOk returns a tuple with the PlatformComChannels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPlatformComChannels

`func (o *Contact) SetPlatformComChannels(v []PlatformCommunicationChannel)`

SetPlatformComChannels sets PlatformComChannels field to given value.

### HasPlatformComChannels

`func (o *Contact) HasPlatformComChannels() bool`

HasPlatformComChannels returns a boolean if a field has been set.

### GetCoupons

`func (o *Contact) GetCoupons() []Coupon`

GetCoupons returns the Coupons field if non-nil, zero value otherwise.

### GetCouponsOk

`func (o *Contact) GetCouponsOk() (*[]Coupon, bool)`

GetCouponsOk returns a tuple with the Coupons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCoupons

`func (o *Contact) SetCoupons(v []Coupon)`

SetCoupons sets Coupons field to given value.

### HasCoupons

`func (o *Contact) HasCoupons() bool`

HasCoupons returns a boolean if a field has been set.

### GetDevices

`func (o *Contact) GetDevices() []ContactDevices`

GetDevices returns the Devices field if non-nil, zero value otherwise.

### GetDevicesOk

`func (o *Contact) GetDevicesOk() (*[]ContactDevices, bool)`

GetDevicesOk returns a tuple with the Devices field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDevices

`func (o *Contact) SetDevices(v []ContactDevices)`

SetDevices sets Devices field to given value.

### HasDevices

`func (o *Contact) HasDevices() bool`

HasDevices returns a boolean if a field has been set.

### GetMobileDevice

`func (o *Contact) GetMobileDevice() ContactDevices`

GetMobileDevice returns the MobileDevice field if non-nil, zero value otherwise.

### GetMobileDeviceOk

`func (o *Contact) GetMobileDeviceOk() (*ContactDevices, bool)`

GetMobileDeviceOk returns a tuple with the MobileDevice field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobileDevice

`func (o *Contact) SetMobileDevice(v ContactDevices)`

SetMobileDevice sets MobileDevice field to given value.

### HasMobileDevice

`func (o *Contact) HasMobileDevice() bool`

HasMobileDevice returns a boolean if a field has been set.

### GetImportBatchId

`func (o *Contact) GetImportBatchId() string`

GetImportBatchId returns the ImportBatchId field if non-nil, zero value otherwise.

### GetImportBatchIdOk

`func (o *Contact) GetImportBatchIdOk() (*string, bool)`

GetImportBatchIdOk returns a tuple with the ImportBatchId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetImportBatchId

`func (o *Contact) SetImportBatchId(v string)`

SetImportBatchId sets ImportBatchId field to given value.

### HasImportBatchId

`func (o *Contact) HasImportBatchId() bool`

HasImportBatchId returns a boolean if a field has been set.

### GetAge

`func (o *Contact) GetAge() int32`

GetAge returns the Age field if non-nil, zero value otherwise.

### GetAgeOk

`func (o *Contact) GetAgeOk() (*int32, bool)`

GetAgeOk returns a tuple with the Age field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAge

`func (o *Contact) SetAge(v int32)`

SetAge sets Age field to given value.

### HasAge

`func (o *Contact) HasAge() bool`

HasAge returns a boolean if a field has been set.

### GetGivenConsents

`func (o *Contact) GetGivenConsents() ContactAllOfGivenConsents`

GetGivenConsents returns the GivenConsents field if non-nil, zero value otherwise.

### GetGivenConsentsOk

`func (o *Contact) GetGivenConsentsOk() (*ContactAllOfGivenConsents, bool)`

GetGivenConsentsOk returns a tuple with the GivenConsents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGivenConsents

`func (o *Contact) SetGivenConsents(v ContactAllOfGivenConsents)`

SetGivenConsents sets GivenConsents field to given value.

### HasGivenConsents

`func (o *Contact) HasGivenConsents() bool`

HasGivenConsents returns a boolean if a field has been set.

### GetConsentKeys

`func (o *Contact) GetConsentKeys() []string`

GetConsentKeys returns the ConsentKeys field if non-nil, zero value otherwise.

### GetConsentKeysOk

`func (o *Contact) GetConsentKeysOk() (*[]string, bool)`

GetConsentKeysOk returns a tuple with the ConsentKeys field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConsentKeys

`func (o *Contact) SetConsentKeys(v []string)`

SetConsentKeys sets ConsentKeys field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


