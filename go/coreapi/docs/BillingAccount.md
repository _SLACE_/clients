# BillingAccount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**Type** | Pointer to **string** |  | [optional] 
**TaxId** | Pointer to **string** |  | [optional] 
**ContactInfo** | Pointer to [**BillingContactInfo**](BillingContactInfo.md) |  | [optional] 
**BillingAddress** | Pointer to [**ContactAddress**](ContactAddress.md) |  | [optional] 
**Id** | **string** |  | 

## Methods

### NewBillingAccount

`func NewBillingAccount(name string, id string, ) *BillingAccount`

NewBillingAccount instantiates a new BillingAccount object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBillingAccountWithDefaults

`func NewBillingAccountWithDefaults() *BillingAccount`

NewBillingAccountWithDefaults instantiates a new BillingAccount object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *BillingAccount) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *BillingAccount) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *BillingAccount) SetName(v string)`

SetName sets Name field to given value.


### GetType

`func (o *BillingAccount) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *BillingAccount) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *BillingAccount) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *BillingAccount) HasType() bool`

HasType returns a boolean if a field has been set.

### GetTaxId

`func (o *BillingAccount) GetTaxId() string`

GetTaxId returns the TaxId field if non-nil, zero value otherwise.

### GetTaxIdOk

`func (o *BillingAccount) GetTaxIdOk() (*string, bool)`

GetTaxIdOk returns a tuple with the TaxId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTaxId

`func (o *BillingAccount) SetTaxId(v string)`

SetTaxId sets TaxId field to given value.

### HasTaxId

`func (o *BillingAccount) HasTaxId() bool`

HasTaxId returns a boolean if a field has been set.

### GetContactInfo

`func (o *BillingAccount) GetContactInfo() BillingContactInfo`

GetContactInfo returns the ContactInfo field if non-nil, zero value otherwise.

### GetContactInfoOk

`func (o *BillingAccount) GetContactInfoOk() (*BillingContactInfo, bool)`

GetContactInfoOk returns a tuple with the ContactInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactInfo

`func (o *BillingAccount) SetContactInfo(v BillingContactInfo)`

SetContactInfo sets ContactInfo field to given value.

### HasContactInfo

`func (o *BillingAccount) HasContactInfo() bool`

HasContactInfo returns a boolean if a field has been set.

### GetBillingAddress

`func (o *BillingAccount) GetBillingAddress() ContactAddress`

GetBillingAddress returns the BillingAddress field if non-nil, zero value otherwise.

### GetBillingAddressOk

`func (o *BillingAccount) GetBillingAddressOk() (*ContactAddress, bool)`

GetBillingAddressOk returns a tuple with the BillingAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingAddress

`func (o *BillingAccount) SetBillingAddress(v ContactAddress)`

SetBillingAddress sets BillingAddress field to given value.

### HasBillingAddress

`func (o *BillingAccount) HasBillingAddress() bool`

HasBillingAddress returns a boolean if a field has been set.

### GetId

`func (o *BillingAccount) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *BillingAccount) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *BillingAccount) SetId(v string)`

SetId sets Id field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


