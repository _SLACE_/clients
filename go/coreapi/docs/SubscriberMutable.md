# SubscriberMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Email** | **string** |  | 
**MutedTypes** | Pointer to **[]string** |  | [optional] 

## Methods

### NewSubscriberMutable

`func NewSubscriberMutable(email string, ) *SubscriberMutable`

NewSubscriberMutable instantiates a new SubscriberMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSubscriberMutableWithDefaults

`func NewSubscriberMutableWithDefaults() *SubscriberMutable`

NewSubscriberMutableWithDefaults instantiates a new SubscriberMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetEmail

`func (o *SubscriberMutable) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *SubscriberMutable) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *SubscriberMutable) SetEmail(v string)`

SetEmail sets Email field to given value.


### GetMutedTypes

`func (o *SubscriberMutable) GetMutedTypes() []string`

GetMutedTypes returns the MutedTypes field if non-nil, zero value otherwise.

### GetMutedTypesOk

`func (o *SubscriberMutable) GetMutedTypesOk() (*[]string, bool)`

GetMutedTypesOk returns a tuple with the MutedTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMutedTypes

`func (o *SubscriberMutable) SetMutedTypes(v []string)`

SetMutedTypes sets MutedTypes field to given value.

### HasMutedTypes

`func (o *SubscriberMutable) HasMutedTypes() bool`

HasMutedTypes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


