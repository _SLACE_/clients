# DeleteExternalIdRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SystemName** | Pointer to **string** |  | [optional] 

## Methods

### NewDeleteExternalIdRequest

`func NewDeleteExternalIdRequest() *DeleteExternalIdRequest`

NewDeleteExternalIdRequest instantiates a new DeleteExternalIdRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDeleteExternalIdRequestWithDefaults

`func NewDeleteExternalIdRequestWithDefaults() *DeleteExternalIdRequest`

NewDeleteExternalIdRequestWithDefaults instantiates a new DeleteExternalIdRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSystemName

`func (o *DeleteExternalIdRequest) GetSystemName() string`

GetSystemName returns the SystemName field if non-nil, zero value otherwise.

### GetSystemNameOk

`func (o *DeleteExternalIdRequest) GetSystemNameOk() (*string, bool)`

GetSystemNameOk returns a tuple with the SystemName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSystemName

`func (o *DeleteExternalIdRequest) SetSystemName(v string)`

SetSystemName sets SystemName field to given value.

### HasSystemName

`func (o *DeleteExternalIdRequest) HasSystemName() bool`

HasSystemName returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


