# GetCampaignSendoutStarts200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | Pointer to [**GetCampaignSendoutStarts200ResponseResults**](GetCampaignSendoutStarts200ResponseResults.md) |  | [optional] 
**Total** | Pointer to **int32** |  | [optional] 

## Methods

### NewGetCampaignSendoutStarts200Response

`func NewGetCampaignSendoutStarts200Response() *GetCampaignSendoutStarts200Response`

NewGetCampaignSendoutStarts200Response instantiates a new GetCampaignSendoutStarts200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCampaignSendoutStarts200ResponseWithDefaults

`func NewGetCampaignSendoutStarts200ResponseWithDefaults() *GetCampaignSendoutStarts200Response`

NewGetCampaignSendoutStarts200ResponseWithDefaults instantiates a new GetCampaignSendoutStarts200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *GetCampaignSendoutStarts200Response) GetResults() GetCampaignSendoutStarts200ResponseResults`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *GetCampaignSendoutStarts200Response) GetResultsOk() (*GetCampaignSendoutStarts200ResponseResults, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *GetCampaignSendoutStarts200Response) SetResults(v GetCampaignSendoutStarts200ResponseResults)`

SetResults sets Results field to given value.

### HasResults

`func (o *GetCampaignSendoutStarts200Response) HasResults() bool`

HasResults returns a boolean if a field has been set.

### GetTotal

`func (o *GetCampaignSendoutStarts200Response) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *GetCampaignSendoutStarts200Response) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *GetCampaignSendoutStarts200Response) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *GetCampaignSendoutStarts200Response) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


