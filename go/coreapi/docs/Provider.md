# Provider

## Enum


* `MESSAGE_PIPE` (value: `"MessagePipe"`)

* `UIB` (value: `"UIB"`)

* `TELEGRAM` (value: `"Telegram"`)

* `WEBCHAT` (value: `"Webchat"`)

* `FACEBOOK` (value: `"Facebook"`)

* `VIBER` (value: `"Viber"`)

* `TENIOS` (value: `"Tenios"`)

* `GOOGLE` (value: `"Google"`)

* `_360DIALOG` (value: `"360dialog"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


