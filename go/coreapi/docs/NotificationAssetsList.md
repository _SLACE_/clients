# NotificationAssetsList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | Pointer to [**[]NotificationAsset**](NotificationAsset.md) |  | [optional] 
**Total** | Pointer to **int32** |  | [optional] 

## Methods

### NewNotificationAssetsList

`func NewNotificationAssetsList() *NotificationAssetsList`

NewNotificationAssetsList instantiates a new NotificationAssetsList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNotificationAssetsListWithDefaults

`func NewNotificationAssetsListWithDefaults() *NotificationAssetsList`

NewNotificationAssetsListWithDefaults instantiates a new NotificationAssetsList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *NotificationAssetsList) GetResults() []NotificationAsset`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *NotificationAssetsList) GetResultsOk() (*[]NotificationAsset, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *NotificationAssetsList) SetResults(v []NotificationAsset)`

SetResults sets Results field to given value.

### HasResults

`func (o *NotificationAssetsList) HasResults() bool`

HasResults returns a boolean if a field has been set.

### GetTotal

`func (o *NotificationAssetsList) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *NotificationAssetsList) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *NotificationAssetsList) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *NotificationAssetsList) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


