# NotificationFrequency

## Enum


* `INSTANT` (value: `"instant"`)

* `HOURLY` (value: `"hourly"`)

* `DAILY` (value: `"daily"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


