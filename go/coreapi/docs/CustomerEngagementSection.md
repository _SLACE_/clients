# CustomerEngagementSection

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BeforeDays** | Pointer to **int32** |  | [optional] 
**AfterDays** | Pointer to **int32** |  | [optional] 
**ActivityTypes** | **[]string** |  | 
**LowerThreshold** | **int32** |  | 
**UpperThreshold** | Pointer to **int32** |  | [optional] 

## Methods

### NewCustomerEngagementSection

`func NewCustomerEngagementSection(activityTypes []string, lowerThreshold int32, ) *CustomerEngagementSection`

NewCustomerEngagementSection instantiates a new CustomerEngagementSection object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCustomerEngagementSectionWithDefaults

`func NewCustomerEngagementSectionWithDefaults() *CustomerEngagementSection`

NewCustomerEngagementSectionWithDefaults instantiates a new CustomerEngagementSection object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetBeforeDays

`func (o *CustomerEngagementSection) GetBeforeDays() int32`

GetBeforeDays returns the BeforeDays field if non-nil, zero value otherwise.

### GetBeforeDaysOk

`func (o *CustomerEngagementSection) GetBeforeDaysOk() (*int32, bool)`

GetBeforeDaysOk returns a tuple with the BeforeDays field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBeforeDays

`func (o *CustomerEngagementSection) SetBeforeDays(v int32)`

SetBeforeDays sets BeforeDays field to given value.

### HasBeforeDays

`func (o *CustomerEngagementSection) HasBeforeDays() bool`

HasBeforeDays returns a boolean if a field has been set.

### GetAfterDays

`func (o *CustomerEngagementSection) GetAfterDays() int32`

GetAfterDays returns the AfterDays field if non-nil, zero value otherwise.

### GetAfterDaysOk

`func (o *CustomerEngagementSection) GetAfterDaysOk() (*int32, bool)`

GetAfterDaysOk returns a tuple with the AfterDays field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAfterDays

`func (o *CustomerEngagementSection) SetAfterDays(v int32)`

SetAfterDays sets AfterDays field to given value.

### HasAfterDays

`func (o *CustomerEngagementSection) HasAfterDays() bool`

HasAfterDays returns a boolean if a field has been set.

### GetActivityTypes

`func (o *CustomerEngagementSection) GetActivityTypes() []string`

GetActivityTypes returns the ActivityTypes field if non-nil, zero value otherwise.

### GetActivityTypesOk

`func (o *CustomerEngagementSection) GetActivityTypesOk() (*[]string, bool)`

GetActivityTypesOk returns a tuple with the ActivityTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActivityTypes

`func (o *CustomerEngagementSection) SetActivityTypes(v []string)`

SetActivityTypes sets ActivityTypes field to given value.


### GetLowerThreshold

`func (o *CustomerEngagementSection) GetLowerThreshold() int32`

GetLowerThreshold returns the LowerThreshold field if non-nil, zero value otherwise.

### GetLowerThresholdOk

`func (o *CustomerEngagementSection) GetLowerThresholdOk() (*int32, bool)`

GetLowerThresholdOk returns a tuple with the LowerThreshold field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLowerThreshold

`func (o *CustomerEngagementSection) SetLowerThreshold(v int32)`

SetLowerThreshold sets LowerThreshold field to given value.


### GetUpperThreshold

`func (o *CustomerEngagementSection) GetUpperThreshold() int32`

GetUpperThreshold returns the UpperThreshold field if non-nil, zero value otherwise.

### GetUpperThresholdOk

`func (o *CustomerEngagementSection) GetUpperThresholdOk() (*int32, bool)`

GetUpperThresholdOk returns a tuple with the UpperThreshold field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpperThreshold

`func (o *CustomerEngagementSection) SetUpperThreshold(v int32)`

SetUpperThreshold sets UpperThreshold field to given value.

### HasUpperThreshold

`func (o *CustomerEngagementSection) HasUpperThreshold() bool`

HasUpperThreshold returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


