# NotificationCategory

## Enum


* `NotificationCategoryBilling` (value: `"billing"`)

* `NotificationCategoryConversation` (value: `"conversation"`)

* `NotificationCategoryContact` (value: `"contact"`)

* `NotificationCategoryTask` (value: `"task"`)

* `NotificationCategorySystem` (value: `"system"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


