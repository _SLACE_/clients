# WebhookEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**TransactionId** | **string** |  | 
**Type** | [**WebhookEventType**](WebhookEventType.md) |  | 
**ChannelId** | **string** |  | 
**Contact** | Pointer to [**WebhookEventContact**](WebhookEventContact.md) |  | [optional] 
**ComChannel** | Pointer to [**WebhookEventComChannel**](WebhookEventComChannel.md) |  | [optional] 
**Conversation** | Pointer to [**WebhookEventConversation**](WebhookEventConversation.md) |  | [optional] 
**Gateway** | Pointer to [**WebhookEventGateway**](WebhookEventGateway.md) |  | [optional] 
**Script** | Pointer to [**WebhookEventScript**](WebhookEventScript.md) |  | [optional] 
**MessageStatus** | Pointer to [**WebhookEventMessageStatus**](WebhookEventMessageStatus.md) |  | [optional] 
**Message** | Pointer to [**WebhookEventMessage**](WebhookEventMessage.md) |  | [optional] 
**Timestamp** | **time.Time** |  | 

## Methods

### NewWebhookEvent

`func NewWebhookEvent(id string, transactionId string, type_ WebhookEventType, channelId string, timestamp time.Time, ) *WebhookEvent`

NewWebhookEvent instantiates a new WebhookEvent object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookEventWithDefaults

`func NewWebhookEventWithDefaults() *WebhookEvent`

NewWebhookEventWithDefaults instantiates a new WebhookEvent object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WebhookEvent) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WebhookEvent) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WebhookEvent) SetId(v string)`

SetId sets Id field to given value.


### GetTransactionId

`func (o *WebhookEvent) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *WebhookEvent) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *WebhookEvent) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetType

`func (o *WebhookEvent) GetType() WebhookEventType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WebhookEvent) GetTypeOk() (*WebhookEventType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WebhookEvent) SetType(v WebhookEventType)`

SetType sets Type field to given value.


### GetChannelId

`func (o *WebhookEvent) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *WebhookEvent) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *WebhookEvent) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetContact

`func (o *WebhookEvent) GetContact() WebhookEventContact`

GetContact returns the Contact field if non-nil, zero value otherwise.

### GetContactOk

`func (o *WebhookEvent) GetContactOk() (*WebhookEventContact, bool)`

GetContactOk returns a tuple with the Contact field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContact

`func (o *WebhookEvent) SetContact(v WebhookEventContact)`

SetContact sets Contact field to given value.

### HasContact

`func (o *WebhookEvent) HasContact() bool`

HasContact returns a boolean if a field has been set.

### GetComChannel

`func (o *WebhookEvent) GetComChannel() WebhookEventComChannel`

GetComChannel returns the ComChannel field if non-nil, zero value otherwise.

### GetComChannelOk

`func (o *WebhookEvent) GetComChannelOk() (*WebhookEventComChannel, bool)`

GetComChannelOk returns a tuple with the ComChannel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComChannel

`func (o *WebhookEvent) SetComChannel(v WebhookEventComChannel)`

SetComChannel sets ComChannel field to given value.

### HasComChannel

`func (o *WebhookEvent) HasComChannel() bool`

HasComChannel returns a boolean if a field has been set.

### GetConversation

`func (o *WebhookEvent) GetConversation() WebhookEventConversation`

GetConversation returns the Conversation field if non-nil, zero value otherwise.

### GetConversationOk

`func (o *WebhookEvent) GetConversationOk() (*WebhookEventConversation, bool)`

GetConversationOk returns a tuple with the Conversation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversation

`func (o *WebhookEvent) SetConversation(v WebhookEventConversation)`

SetConversation sets Conversation field to given value.

### HasConversation

`func (o *WebhookEvent) HasConversation() bool`

HasConversation returns a boolean if a field has been set.

### GetGateway

`func (o *WebhookEvent) GetGateway() WebhookEventGateway`

GetGateway returns the Gateway field if non-nil, zero value otherwise.

### GetGatewayOk

`func (o *WebhookEvent) GetGatewayOk() (*WebhookEventGateway, bool)`

GetGatewayOk returns a tuple with the Gateway field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGateway

`func (o *WebhookEvent) SetGateway(v WebhookEventGateway)`

SetGateway sets Gateway field to given value.

### HasGateway

`func (o *WebhookEvent) HasGateway() bool`

HasGateway returns a boolean if a field has been set.

### GetScript

`func (o *WebhookEvent) GetScript() WebhookEventScript`

GetScript returns the Script field if non-nil, zero value otherwise.

### GetScriptOk

`func (o *WebhookEvent) GetScriptOk() (*WebhookEventScript, bool)`

GetScriptOk returns a tuple with the Script field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScript

`func (o *WebhookEvent) SetScript(v WebhookEventScript)`

SetScript sets Script field to given value.

### HasScript

`func (o *WebhookEvent) HasScript() bool`

HasScript returns a boolean if a field has been set.

### GetMessageStatus

`func (o *WebhookEvent) GetMessageStatus() WebhookEventMessageStatus`

GetMessageStatus returns the MessageStatus field if non-nil, zero value otherwise.

### GetMessageStatusOk

`func (o *WebhookEvent) GetMessageStatusOk() (*WebhookEventMessageStatus, bool)`

GetMessageStatusOk returns a tuple with the MessageStatus field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageStatus

`func (o *WebhookEvent) SetMessageStatus(v WebhookEventMessageStatus)`

SetMessageStatus sets MessageStatus field to given value.

### HasMessageStatus

`func (o *WebhookEvent) HasMessageStatus() bool`

HasMessageStatus returns a boolean if a field has been set.

### GetMessage

`func (o *WebhookEvent) GetMessage() WebhookEventMessage`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *WebhookEvent) GetMessageOk() (*WebhookEventMessage, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *WebhookEvent) SetMessage(v WebhookEventMessage)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *WebhookEvent) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetTimestamp

`func (o *WebhookEvent) GetTimestamp() time.Time`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *WebhookEvent) GetTimestampOk() (*time.Time, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *WebhookEvent) SetTimestamp(v time.Time)`

SetTimestamp sets Timestamp field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


