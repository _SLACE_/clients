# NotificationsCounts

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Info** | **int32** |  | 
**Warning** | **int32** |  | 
**Critical** | **int32** |  | 

## Methods

### NewNotificationsCounts

`func NewNotificationsCounts(info int32, warning int32, critical int32, ) *NotificationsCounts`

NewNotificationsCounts instantiates a new NotificationsCounts object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNotificationsCountsWithDefaults

`func NewNotificationsCountsWithDefaults() *NotificationsCounts`

NewNotificationsCountsWithDefaults instantiates a new NotificationsCounts object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInfo

`func (o *NotificationsCounts) GetInfo() int32`

GetInfo returns the Info field if non-nil, zero value otherwise.

### GetInfoOk

`func (o *NotificationsCounts) GetInfoOk() (*int32, bool)`

GetInfoOk returns a tuple with the Info field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInfo

`func (o *NotificationsCounts) SetInfo(v int32)`

SetInfo sets Info field to given value.


### GetWarning

`func (o *NotificationsCounts) GetWarning() int32`

GetWarning returns the Warning field if non-nil, zero value otherwise.

### GetWarningOk

`func (o *NotificationsCounts) GetWarningOk() (*int32, bool)`

GetWarningOk returns a tuple with the Warning field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWarning

`func (o *NotificationsCounts) SetWarning(v int32)`

SetWarning sets Warning field to given value.


### GetCritical

`func (o *NotificationsCounts) GetCritical() int32`

GetCritical returns the Critical field if non-nil, zero value otherwise.

### GetCriticalOk

`func (o *NotificationsCounts) GetCriticalOk() (*int32, bool)`

GetCriticalOk returns a tuple with the Critical field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCritical

`func (o *NotificationsCounts) SetCritical(v int32)`

SetCritical sets Critical field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


