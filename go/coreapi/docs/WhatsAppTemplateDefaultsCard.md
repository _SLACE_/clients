# WhatsAppTemplateDefaultsCard

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CardIndex** | Pointer to **int64** |  | [optional] 
**Components** | Pointer to [**[]WhatsAppTemplateDefaultsComponent**](WhatsAppTemplateDefaultsComponent.md) |  | [optional] 

## Methods

### NewWhatsAppTemplateDefaultsCard

`func NewWhatsAppTemplateDefaultsCard() *WhatsAppTemplateDefaultsCard`

NewWhatsAppTemplateDefaultsCard instantiates a new WhatsAppTemplateDefaultsCard object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateDefaultsCardWithDefaults

`func NewWhatsAppTemplateDefaultsCardWithDefaults() *WhatsAppTemplateDefaultsCard`

NewWhatsAppTemplateDefaultsCardWithDefaults instantiates a new WhatsAppTemplateDefaultsCard object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCardIndex

`func (o *WhatsAppTemplateDefaultsCard) GetCardIndex() int64`

GetCardIndex returns the CardIndex field if non-nil, zero value otherwise.

### GetCardIndexOk

`func (o *WhatsAppTemplateDefaultsCard) GetCardIndexOk() (*int64, bool)`

GetCardIndexOk returns a tuple with the CardIndex field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCardIndex

`func (o *WhatsAppTemplateDefaultsCard) SetCardIndex(v int64)`

SetCardIndex sets CardIndex field to given value.

### HasCardIndex

`func (o *WhatsAppTemplateDefaultsCard) HasCardIndex() bool`

HasCardIndex returns a boolean if a field has been set.

### GetComponents

`func (o *WhatsAppTemplateDefaultsCard) GetComponents() []WhatsAppTemplateDefaultsComponent`

GetComponents returns the Components field if non-nil, zero value otherwise.

### GetComponentsOk

`func (o *WhatsAppTemplateDefaultsCard) GetComponentsOk() (*[]WhatsAppTemplateDefaultsComponent, bool)`

GetComponentsOk returns a tuple with the Components field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComponents

`func (o *WhatsAppTemplateDefaultsCard) SetComponents(v []WhatsAppTemplateDefaultsComponent)`

SetComponents sets Components field to given value.

### HasComponents

`func (o *WhatsAppTemplateDefaultsCard) HasComponents() bool`

HasComponents returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


