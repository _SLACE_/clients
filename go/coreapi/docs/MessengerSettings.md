# MessengerSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**Description** | Pointer to **string** |  | [optional] 
**ShortDescription** | Pointer to **string** |  | [optional] 
**ProfilePhoto** | Pointer to [**MessengerSettingsProfilePhoto**](MessengerSettingsProfilePhoto.md) |  | [optional] 
**IceBreakers** | Pointer to [**[]IceBreaker**](IceBreaker.md) | Available only in &#x60;instagram&#x60; gateway type. | [optional] 
**Greeting** | Pointer to [**[]LocalizedText**](LocalizedText.md) | Available only in &#x60;FacebookMessenger&#x60; gateway type | [optional] 
**GetStarted** | Pointer to **string** |  | [optional] 
**MessengerUrl** | Pointer to **string** |  | [optional] 
**ProfileUrl** | Pointer to **string** |  | [optional] 
**BusinessProfile** | Pointer to [**MessengerBusinessProfile**](MessengerBusinessProfile.md) |  | [optional] 

## Methods

### NewMessengerSettings

`func NewMessengerSettings() *MessengerSettings`

NewMessengerSettings instantiates a new MessengerSettings object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessengerSettingsWithDefaults

`func NewMessengerSettingsWithDefaults() *MessengerSettings`

NewMessengerSettingsWithDefaults instantiates a new MessengerSettings object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *MessengerSettings) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *MessengerSettings) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *MessengerSettings) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *MessengerSettings) HasName() bool`

HasName returns a boolean if a field has been set.

### GetDescription

`func (o *MessengerSettings) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *MessengerSettings) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *MessengerSettings) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *MessengerSettings) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetShortDescription

`func (o *MessengerSettings) GetShortDescription() string`

GetShortDescription returns the ShortDescription field if non-nil, zero value otherwise.

### GetShortDescriptionOk

`func (o *MessengerSettings) GetShortDescriptionOk() (*string, bool)`

GetShortDescriptionOk returns a tuple with the ShortDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetShortDescription

`func (o *MessengerSettings) SetShortDescription(v string)`

SetShortDescription sets ShortDescription field to given value.

### HasShortDescription

`func (o *MessengerSettings) HasShortDescription() bool`

HasShortDescription returns a boolean if a field has been set.

### GetProfilePhoto

`func (o *MessengerSettings) GetProfilePhoto() MessengerSettingsProfilePhoto`

GetProfilePhoto returns the ProfilePhoto field if non-nil, zero value otherwise.

### GetProfilePhotoOk

`func (o *MessengerSettings) GetProfilePhotoOk() (*MessengerSettingsProfilePhoto, bool)`

GetProfilePhotoOk returns a tuple with the ProfilePhoto field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProfilePhoto

`func (o *MessengerSettings) SetProfilePhoto(v MessengerSettingsProfilePhoto)`

SetProfilePhoto sets ProfilePhoto field to given value.

### HasProfilePhoto

`func (o *MessengerSettings) HasProfilePhoto() bool`

HasProfilePhoto returns a boolean if a field has been set.

### GetIceBreakers

`func (o *MessengerSettings) GetIceBreakers() []IceBreaker`

GetIceBreakers returns the IceBreakers field if non-nil, zero value otherwise.

### GetIceBreakersOk

`func (o *MessengerSettings) GetIceBreakersOk() (*[]IceBreaker, bool)`

GetIceBreakersOk returns a tuple with the IceBreakers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIceBreakers

`func (o *MessengerSettings) SetIceBreakers(v []IceBreaker)`

SetIceBreakers sets IceBreakers field to given value.

### HasIceBreakers

`func (o *MessengerSettings) HasIceBreakers() bool`

HasIceBreakers returns a boolean if a field has been set.

### GetGreeting

`func (o *MessengerSettings) GetGreeting() []LocalizedText`

GetGreeting returns the Greeting field if non-nil, zero value otherwise.

### GetGreetingOk

`func (o *MessengerSettings) GetGreetingOk() (*[]LocalizedText, bool)`

GetGreetingOk returns a tuple with the Greeting field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGreeting

`func (o *MessengerSettings) SetGreeting(v []LocalizedText)`

SetGreeting sets Greeting field to given value.

### HasGreeting

`func (o *MessengerSettings) HasGreeting() bool`

HasGreeting returns a boolean if a field has been set.

### GetGetStarted

`func (o *MessengerSettings) GetGetStarted() string`

GetGetStarted returns the GetStarted field if non-nil, zero value otherwise.

### GetGetStartedOk

`func (o *MessengerSettings) GetGetStartedOk() (*string, bool)`

GetGetStartedOk returns a tuple with the GetStarted field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGetStarted

`func (o *MessengerSettings) SetGetStarted(v string)`

SetGetStarted sets GetStarted field to given value.

### HasGetStarted

`func (o *MessengerSettings) HasGetStarted() bool`

HasGetStarted returns a boolean if a field has been set.

### GetMessengerUrl

`func (o *MessengerSettings) GetMessengerUrl() string`

GetMessengerUrl returns the MessengerUrl field if non-nil, zero value otherwise.

### GetMessengerUrlOk

`func (o *MessengerSettings) GetMessengerUrlOk() (*string, bool)`

GetMessengerUrlOk returns a tuple with the MessengerUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerUrl

`func (o *MessengerSettings) SetMessengerUrl(v string)`

SetMessengerUrl sets MessengerUrl field to given value.

### HasMessengerUrl

`func (o *MessengerSettings) HasMessengerUrl() bool`

HasMessengerUrl returns a boolean if a field has been set.

### GetProfileUrl

`func (o *MessengerSettings) GetProfileUrl() string`

GetProfileUrl returns the ProfileUrl field if non-nil, zero value otherwise.

### GetProfileUrlOk

`func (o *MessengerSettings) GetProfileUrlOk() (*string, bool)`

GetProfileUrlOk returns a tuple with the ProfileUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProfileUrl

`func (o *MessengerSettings) SetProfileUrl(v string)`

SetProfileUrl sets ProfileUrl field to given value.

### HasProfileUrl

`func (o *MessengerSettings) HasProfileUrl() bool`

HasProfileUrl returns a boolean if a field has been set.

### GetBusinessProfile

`func (o *MessengerSettings) GetBusinessProfile() MessengerBusinessProfile`

GetBusinessProfile returns the BusinessProfile field if non-nil, zero value otherwise.

### GetBusinessProfileOk

`func (o *MessengerSettings) GetBusinessProfileOk() (*MessengerBusinessProfile, bool)`

GetBusinessProfileOk returns a tuple with the BusinessProfile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBusinessProfile

`func (o *MessengerSettings) SetBusinessProfile(v MessengerBusinessProfile)`

SetBusinessProfile sets BusinessProfile field to given value.

### HasBusinessProfile

`func (o *MessengerSettings) HasBusinessProfile() bool`

HasBusinessProfile returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


