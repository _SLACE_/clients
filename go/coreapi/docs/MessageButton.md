# MessageButton

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | [**MessageButtonType**](MessageButtonType.md) |  | 
**Text** | **string** |  | 
**Payload** | Pointer to **string** |  | [optional] 
**AutoShortLink** | Pointer to **bool** |  | [optional] 
**ExpiredUrl** | Pointer to **string** |  | [optional] 
**Ttl** | Pointer to **int32** |  | [optional] 

## Methods

### NewMessageButton

`func NewMessageButton(type_ MessageButtonType, text string, ) *MessageButton`

NewMessageButton instantiates a new MessageButton object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageButtonWithDefaults

`func NewMessageButtonWithDefaults() *MessageButton`

NewMessageButtonWithDefaults instantiates a new MessageButton object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *MessageButton) GetType() MessageButtonType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MessageButton) GetTypeOk() (*MessageButtonType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MessageButton) SetType(v MessageButtonType)`

SetType sets Type field to given value.


### GetText

`func (o *MessageButton) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *MessageButton) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *MessageButton) SetText(v string)`

SetText sets Text field to given value.


### GetPayload

`func (o *MessageButton) GetPayload() string`

GetPayload returns the Payload field if non-nil, zero value otherwise.

### GetPayloadOk

`func (o *MessageButton) GetPayloadOk() (*string, bool)`

GetPayloadOk returns a tuple with the Payload field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayload

`func (o *MessageButton) SetPayload(v string)`

SetPayload sets Payload field to given value.

### HasPayload

`func (o *MessageButton) HasPayload() bool`

HasPayload returns a boolean if a field has been set.

### GetAutoShortLink

`func (o *MessageButton) GetAutoShortLink() bool`

GetAutoShortLink returns the AutoShortLink field if non-nil, zero value otherwise.

### GetAutoShortLinkOk

`func (o *MessageButton) GetAutoShortLinkOk() (*bool, bool)`

GetAutoShortLinkOk returns a tuple with the AutoShortLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAutoShortLink

`func (o *MessageButton) SetAutoShortLink(v bool)`

SetAutoShortLink sets AutoShortLink field to given value.

### HasAutoShortLink

`func (o *MessageButton) HasAutoShortLink() bool`

HasAutoShortLink returns a boolean if a field has been set.

### GetExpiredUrl

`func (o *MessageButton) GetExpiredUrl() string`

GetExpiredUrl returns the ExpiredUrl field if non-nil, zero value otherwise.

### GetExpiredUrlOk

`func (o *MessageButton) GetExpiredUrlOk() (*string, bool)`

GetExpiredUrlOk returns a tuple with the ExpiredUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiredUrl

`func (o *MessageButton) SetExpiredUrl(v string)`

SetExpiredUrl sets ExpiredUrl field to given value.

### HasExpiredUrl

`func (o *MessageButton) HasExpiredUrl() bool`

HasExpiredUrl returns a boolean if a field has been set.

### GetTtl

`func (o *MessageButton) GetTtl() int32`

GetTtl returns the Ttl field if non-nil, zero value otherwise.

### GetTtlOk

`func (o *MessageButton) GetTtlOk() (*int32, bool)`

GetTtlOk returns a tuple with the Ttl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTtl

`func (o *MessageButton) SetTtl(v int32)`

SetTtl sets Ttl field to given value.

### HasTtl

`func (o *MessageButton) HasTtl() bool`

HasTtl returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


