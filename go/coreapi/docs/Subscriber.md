# Subscriber

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**UserId** | **string** |  | 
**OrganizationId** | **string** |  | 
**Email** | **string** |  | 
**Language** | [**SystemLanguage**](SystemLanguage.md) |  | 
**MutedTypes** | Pointer to **[]string** |  | [optional] 

## Methods

### NewSubscriber

`func NewSubscriber(userId string, organizationId string, email string, language SystemLanguage, ) *Subscriber`

NewSubscriber instantiates a new Subscriber object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSubscriberWithDefaults

`func NewSubscriberWithDefaults() *Subscriber`

NewSubscriberWithDefaults instantiates a new Subscriber object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUserId

`func (o *Subscriber) GetUserId() string`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *Subscriber) GetUserIdOk() (*string, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *Subscriber) SetUserId(v string)`

SetUserId sets UserId field to given value.


### GetOrganizationId

`func (o *Subscriber) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *Subscriber) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *Subscriber) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetEmail

`func (o *Subscriber) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *Subscriber) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *Subscriber) SetEmail(v string)`

SetEmail sets Email field to given value.


### GetLanguage

`func (o *Subscriber) GetLanguage() SystemLanguage`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *Subscriber) GetLanguageOk() (*SystemLanguage, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *Subscriber) SetLanguage(v SystemLanguage)`

SetLanguage sets Language field to given value.


### GetMutedTypes

`func (o *Subscriber) GetMutedTypes() []string`

GetMutedTypes returns the MutedTypes field if non-nil, zero value otherwise.

### GetMutedTypesOk

`func (o *Subscriber) GetMutedTypesOk() (*[]string, bool)`

GetMutedTypesOk returns a tuple with the MutedTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMutedTypes

`func (o *Subscriber) SetMutedTypes(v []string)`

SetMutedTypes sets MutedTypes field to given value.

### HasMutedTypes

`func (o *Subscriber) HasMutedTypes() bool`

HasMutedTypes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


