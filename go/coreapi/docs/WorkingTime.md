# WorkingTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**From** | [**HourMinute**](HourMinute.md) |  | 
**To** | [**HourMinute**](HourMinute.md) |  | 

## Methods

### NewWorkingTime

`func NewWorkingTime(from HourMinute, to HourMinute, ) *WorkingTime`

NewWorkingTime instantiates a new WorkingTime object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWorkingTimeWithDefaults

`func NewWorkingTimeWithDefaults() *WorkingTime`

NewWorkingTimeWithDefaults instantiates a new WorkingTime object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFrom

`func (o *WorkingTime) GetFrom() HourMinute`

GetFrom returns the From field if non-nil, zero value otherwise.

### GetFromOk

`func (o *WorkingTime) GetFromOk() (*HourMinute, bool)`

GetFromOk returns a tuple with the From field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFrom

`func (o *WorkingTime) SetFrom(v HourMinute)`

SetFrom sets From field to given value.


### GetTo

`func (o *WorkingTime) GetTo() HourMinute`

GetTo returns the To field if non-nil, zero value otherwise.

### GetToOk

`func (o *WorkingTime) GetToOk() (*HourMinute, bool)`

GetToOk returns a tuple with the To field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTo

`func (o *WorkingTime) SetTo(v HourMinute)`

SetTo sets To field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


