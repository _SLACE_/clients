# CampaignSendoutRescheduleData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SendAt** | **time.Time** |  | 

## Methods

### NewCampaignSendoutRescheduleData

`func NewCampaignSendoutRescheduleData(sendAt time.Time, ) *CampaignSendoutRescheduleData`

NewCampaignSendoutRescheduleData instantiates a new CampaignSendoutRescheduleData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignSendoutRescheduleDataWithDefaults

`func NewCampaignSendoutRescheduleDataWithDefaults() *CampaignSendoutRescheduleData`

NewCampaignSendoutRescheduleDataWithDefaults instantiates a new CampaignSendoutRescheduleData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSendAt

`func (o *CampaignSendoutRescheduleData) GetSendAt() time.Time`

GetSendAt returns the SendAt field if non-nil, zero value otherwise.

### GetSendAtOk

`func (o *CampaignSendoutRescheduleData) GetSendAtOk() (*time.Time, bool)`

GetSendAtOk returns a tuple with the SendAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSendAt

`func (o *CampaignSendoutRescheduleData) SetSendAt(v time.Time)`

SetSendAt sets SendAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


