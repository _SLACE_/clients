# OrganizationUsersList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | Pointer to [**[]OrganizationUser**](OrganizationUser.md) |  | [optional] 
**Total** | Pointer to **int32** |  | [optional] 

## Methods

### NewOrganizationUsersList

`func NewOrganizationUsersList() *OrganizationUsersList`

NewOrganizationUsersList instantiates a new OrganizationUsersList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrganizationUsersListWithDefaults

`func NewOrganizationUsersListWithDefaults() *OrganizationUsersList`

NewOrganizationUsersListWithDefaults instantiates a new OrganizationUsersList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *OrganizationUsersList) GetResults() []OrganizationUser`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *OrganizationUsersList) GetResultsOk() (*[]OrganizationUser, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *OrganizationUsersList) SetResults(v []OrganizationUser)`

SetResults sets Results field to given value.

### HasResults

`func (o *OrganizationUsersList) HasResults() bool`

HasResults returns a boolean if a field has been set.

### GetTotal

`func (o *OrganizationUsersList) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *OrganizationUsersList) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *OrganizationUsersList) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *OrganizationUsersList) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


