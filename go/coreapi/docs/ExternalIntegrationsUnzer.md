# ExternalIntegrationsUnzer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PrivateKey** | Pointer to **string** |  | [optional] 
**PublicKey** | Pointer to **string** |  | [optional] 
**WebhookId** | Pointer to **string** |  | [optional] 

## Methods

### NewExternalIntegrationsUnzer

`func NewExternalIntegrationsUnzer() *ExternalIntegrationsUnzer`

NewExternalIntegrationsUnzer instantiates a new ExternalIntegrationsUnzer object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExternalIntegrationsUnzerWithDefaults

`func NewExternalIntegrationsUnzerWithDefaults() *ExternalIntegrationsUnzer`

NewExternalIntegrationsUnzerWithDefaults instantiates a new ExternalIntegrationsUnzer object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPrivateKey

`func (o *ExternalIntegrationsUnzer) GetPrivateKey() string`

GetPrivateKey returns the PrivateKey field if non-nil, zero value otherwise.

### GetPrivateKeyOk

`func (o *ExternalIntegrationsUnzer) GetPrivateKeyOk() (*string, bool)`

GetPrivateKeyOk returns a tuple with the PrivateKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrivateKey

`func (o *ExternalIntegrationsUnzer) SetPrivateKey(v string)`

SetPrivateKey sets PrivateKey field to given value.

### HasPrivateKey

`func (o *ExternalIntegrationsUnzer) HasPrivateKey() bool`

HasPrivateKey returns a boolean if a field has been set.

### GetPublicKey

`func (o *ExternalIntegrationsUnzer) GetPublicKey() string`

GetPublicKey returns the PublicKey field if non-nil, zero value otherwise.

### GetPublicKeyOk

`func (o *ExternalIntegrationsUnzer) GetPublicKeyOk() (*string, bool)`

GetPublicKeyOk returns a tuple with the PublicKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPublicKey

`func (o *ExternalIntegrationsUnzer) SetPublicKey(v string)`

SetPublicKey sets PublicKey field to given value.

### HasPublicKey

`func (o *ExternalIntegrationsUnzer) HasPublicKey() bool`

HasPublicKey returns a boolean if a field has been set.

### GetWebhookId

`func (o *ExternalIntegrationsUnzer) GetWebhookId() string`

GetWebhookId returns the WebhookId field if non-nil, zero value otherwise.

### GetWebhookIdOk

`func (o *ExternalIntegrationsUnzer) GetWebhookIdOk() (*string, bool)`

GetWebhookIdOk returns a tuple with the WebhookId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWebhookId

`func (o *ExternalIntegrationsUnzer) SetWebhookId(v string)`

SetWebhookId sets WebhookId field to given value.

### HasWebhookId

`func (o *ExternalIntegrationsUnzer) HasWebhookId() bool`

HasWebhookId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


