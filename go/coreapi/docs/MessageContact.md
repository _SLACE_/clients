# MessageContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Vcard** | Pointer to **string** |  | [optional] 
**Name** | Pointer to [**MessageContactName**](MessageContactName.md) |  | [optional] 
**Org** | Pointer to [**MessageContactOrg**](MessageContactOrg.md) |  | [optional] 
**Birthday** | Pointer to **string** |  | [optional] 
**Photo** | Pointer to [**MessageContactPhoto**](MessageContactPhoto.md) |  | [optional] 
**Phones** | Pointer to [**[]MessageContactPhone**](MessageContactPhone.md) |  | [optional] 
**Addresses** | Pointer to [**[]MessageContactAddress**](MessageContactAddress.md) |  | [optional] 
**Emails** | Pointer to [**[]MessageContactEmail**](MessageContactEmail.md) |  | [optional] 
**Urls** | Pointer to [**[]MessageContactUrl**](MessageContactUrl.md) |  | [optional] 
**Ims** | Pointer to [**[]MessageContactIM**](MessageContactIM.md) |  | [optional] 

## Methods

### NewMessageContact

`func NewMessageContact() *MessageContact`

NewMessageContact instantiates a new MessageContact object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageContactWithDefaults

`func NewMessageContactWithDefaults() *MessageContact`

NewMessageContactWithDefaults instantiates a new MessageContact object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVcard

`func (o *MessageContact) GetVcard() string`

GetVcard returns the Vcard field if non-nil, zero value otherwise.

### GetVcardOk

`func (o *MessageContact) GetVcardOk() (*string, bool)`

GetVcardOk returns a tuple with the Vcard field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVcard

`func (o *MessageContact) SetVcard(v string)`

SetVcard sets Vcard field to given value.

### HasVcard

`func (o *MessageContact) HasVcard() bool`

HasVcard returns a boolean if a field has been set.

### GetName

`func (o *MessageContact) GetName() MessageContactName`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *MessageContact) GetNameOk() (*MessageContactName, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *MessageContact) SetName(v MessageContactName)`

SetName sets Name field to given value.

### HasName

`func (o *MessageContact) HasName() bool`

HasName returns a boolean if a field has been set.

### GetOrg

`func (o *MessageContact) GetOrg() MessageContactOrg`

GetOrg returns the Org field if non-nil, zero value otherwise.

### GetOrgOk

`func (o *MessageContact) GetOrgOk() (*MessageContactOrg, bool)`

GetOrgOk returns a tuple with the Org field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrg

`func (o *MessageContact) SetOrg(v MessageContactOrg)`

SetOrg sets Org field to given value.

### HasOrg

`func (o *MessageContact) HasOrg() bool`

HasOrg returns a boolean if a field has been set.

### GetBirthday

`func (o *MessageContact) GetBirthday() string`

GetBirthday returns the Birthday field if non-nil, zero value otherwise.

### GetBirthdayOk

`func (o *MessageContact) GetBirthdayOk() (*string, bool)`

GetBirthdayOk returns a tuple with the Birthday field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBirthday

`func (o *MessageContact) SetBirthday(v string)`

SetBirthday sets Birthday field to given value.

### HasBirthday

`func (o *MessageContact) HasBirthday() bool`

HasBirthday returns a boolean if a field has been set.

### GetPhoto

`func (o *MessageContact) GetPhoto() MessageContactPhoto`

GetPhoto returns the Photo field if non-nil, zero value otherwise.

### GetPhotoOk

`func (o *MessageContact) GetPhotoOk() (*MessageContactPhoto, bool)`

GetPhotoOk returns a tuple with the Photo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhoto

`func (o *MessageContact) SetPhoto(v MessageContactPhoto)`

SetPhoto sets Photo field to given value.

### HasPhoto

`func (o *MessageContact) HasPhoto() bool`

HasPhoto returns a boolean if a field has been set.

### GetPhones

`func (o *MessageContact) GetPhones() []MessageContactPhone`

GetPhones returns the Phones field if non-nil, zero value otherwise.

### GetPhonesOk

`func (o *MessageContact) GetPhonesOk() (*[]MessageContactPhone, bool)`

GetPhonesOk returns a tuple with the Phones field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhones

`func (o *MessageContact) SetPhones(v []MessageContactPhone)`

SetPhones sets Phones field to given value.

### HasPhones

`func (o *MessageContact) HasPhones() bool`

HasPhones returns a boolean if a field has been set.

### GetAddresses

`func (o *MessageContact) GetAddresses() []MessageContactAddress`

GetAddresses returns the Addresses field if non-nil, zero value otherwise.

### GetAddressesOk

`func (o *MessageContact) GetAddressesOk() (*[]MessageContactAddress, bool)`

GetAddressesOk returns a tuple with the Addresses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddresses

`func (o *MessageContact) SetAddresses(v []MessageContactAddress)`

SetAddresses sets Addresses field to given value.

### HasAddresses

`func (o *MessageContact) HasAddresses() bool`

HasAddresses returns a boolean if a field has been set.

### GetEmails

`func (o *MessageContact) GetEmails() []MessageContactEmail`

GetEmails returns the Emails field if non-nil, zero value otherwise.

### GetEmailsOk

`func (o *MessageContact) GetEmailsOk() (*[]MessageContactEmail, bool)`

GetEmailsOk returns a tuple with the Emails field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmails

`func (o *MessageContact) SetEmails(v []MessageContactEmail)`

SetEmails sets Emails field to given value.

### HasEmails

`func (o *MessageContact) HasEmails() bool`

HasEmails returns a boolean if a field has been set.

### GetUrls

`func (o *MessageContact) GetUrls() []MessageContactUrl`

GetUrls returns the Urls field if non-nil, zero value otherwise.

### GetUrlsOk

`func (o *MessageContact) GetUrlsOk() (*[]MessageContactUrl, bool)`

GetUrlsOk returns a tuple with the Urls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrls

`func (o *MessageContact) SetUrls(v []MessageContactUrl)`

SetUrls sets Urls field to given value.

### HasUrls

`func (o *MessageContact) HasUrls() bool`

HasUrls returns a boolean if a field has been set.

### GetIms

`func (o *MessageContact) GetIms() []MessageContactIM`

GetIms returns the Ims field if non-nil, zero value otherwise.

### GetImsOk

`func (o *MessageContact) GetImsOk() (*[]MessageContactIM, bool)`

GetImsOk returns a tuple with the Ims field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIms

`func (o *MessageContact) SetIms(v []MessageContactIM)`

SetIms sets Ims field to given value.

### HasIms

`func (o *MessageContact) HasIms() bool`

HasIms returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


