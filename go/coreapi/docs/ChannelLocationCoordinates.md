# ChannelLocationCoordinates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Latitude** | Pointer to **string** |  | [optional] 
**Longitude** | Pointer to **string** |  | [optional] 

## Methods

### NewChannelLocationCoordinates

`func NewChannelLocationCoordinates() *ChannelLocationCoordinates`

NewChannelLocationCoordinates instantiates a new ChannelLocationCoordinates object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChannelLocationCoordinatesWithDefaults

`func NewChannelLocationCoordinatesWithDefaults() *ChannelLocationCoordinates`

NewChannelLocationCoordinatesWithDefaults instantiates a new ChannelLocationCoordinates object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLatitude

`func (o *ChannelLocationCoordinates) GetLatitude() string`

GetLatitude returns the Latitude field if non-nil, zero value otherwise.

### GetLatitudeOk

`func (o *ChannelLocationCoordinates) GetLatitudeOk() (*string, bool)`

GetLatitudeOk returns a tuple with the Latitude field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLatitude

`func (o *ChannelLocationCoordinates) SetLatitude(v string)`

SetLatitude sets Latitude field to given value.

### HasLatitude

`func (o *ChannelLocationCoordinates) HasLatitude() bool`

HasLatitude returns a boolean if a field has been set.

### GetLongitude

`func (o *ChannelLocationCoordinates) GetLongitude() string`

GetLongitude returns the Longitude field if non-nil, zero value otherwise.

### GetLongitudeOk

`func (o *ChannelLocationCoordinates) GetLongitudeOk() (*string, bool)`

GetLongitudeOk returns a tuple with the Longitude field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLongitude

`func (o *ChannelLocationCoordinates) SetLongitude(v string)`

SetLongitude sets Longitude field to given value.

### HasLongitude

`func (o *ChannelLocationCoordinates) HasLongitude() bool`

HasLongitude returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


