# NotificationAssetCreatable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to **string** |  | [optional] 
**Translations** | Pointer to [**[]NotificationAssetTranslation**](NotificationAssetTranslation.md) |  | [optional] 

## Methods

### NewNotificationAssetCreatable

`func NewNotificationAssetCreatable() *NotificationAssetCreatable`

NewNotificationAssetCreatable instantiates a new NotificationAssetCreatable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNotificationAssetCreatableWithDefaults

`func NewNotificationAssetCreatableWithDefaults() *NotificationAssetCreatable`

NewNotificationAssetCreatableWithDefaults instantiates a new NotificationAssetCreatable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *NotificationAssetCreatable) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *NotificationAssetCreatable) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *NotificationAssetCreatable) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *NotificationAssetCreatable) HasType() bool`

HasType returns a boolean if a field has been set.

### GetTranslations

`func (o *NotificationAssetCreatable) GetTranslations() []NotificationAssetTranslation`

GetTranslations returns the Translations field if non-nil, zero value otherwise.

### GetTranslationsOk

`func (o *NotificationAssetCreatable) GetTranslationsOk() (*[]NotificationAssetTranslation, bool)`

GetTranslationsOk returns a tuple with the Translations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTranslations

`func (o *NotificationAssetCreatable) SetTranslations(v []NotificationAssetTranslation)`

SetTranslations sets Translations field to given value.

### HasTranslations

`func (o *NotificationAssetCreatable) HasTranslations() bool`

HasTranslations returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


