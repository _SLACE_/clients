# ChannelMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OrganizationId** | **string** |  | 
**Name** | **string** |  | 
**Timezone** | Pointer to **string** | requires valid time zone, eg: Europe/Berlin | [optional] 
**DefaultLanguage** | Pointer to **string** |  | [optional] 
**AttributionEnabled** | Pointer to **bool** |  | [optional] 
**StopKeywords** | **[]string** |  | 
**FallbackLanguage** | Pointer to **string** |  | [optional] 
**DefaultDialog360Tier** | Pointer to [**Dialog360Tier**](Dialog360Tier.md) |  | [optional] 

## Methods

### NewChannelMutable

`func NewChannelMutable(organizationId string, name string, stopKeywords []string, ) *ChannelMutable`

NewChannelMutable instantiates a new ChannelMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChannelMutableWithDefaults

`func NewChannelMutableWithDefaults() *ChannelMutable`

NewChannelMutableWithDefaults instantiates a new ChannelMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOrganizationId

`func (o *ChannelMutable) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *ChannelMutable) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *ChannelMutable) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetName

`func (o *ChannelMutable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ChannelMutable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ChannelMutable) SetName(v string)`

SetName sets Name field to given value.


### GetTimezone

`func (o *ChannelMutable) GetTimezone() string`

GetTimezone returns the Timezone field if non-nil, zero value otherwise.

### GetTimezoneOk

`func (o *ChannelMutable) GetTimezoneOk() (*string, bool)`

GetTimezoneOk returns a tuple with the Timezone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimezone

`func (o *ChannelMutable) SetTimezone(v string)`

SetTimezone sets Timezone field to given value.

### HasTimezone

`func (o *ChannelMutable) HasTimezone() bool`

HasTimezone returns a boolean if a field has been set.

### GetDefaultLanguage

`func (o *ChannelMutable) GetDefaultLanguage() string`

GetDefaultLanguage returns the DefaultLanguage field if non-nil, zero value otherwise.

### GetDefaultLanguageOk

`func (o *ChannelMutable) GetDefaultLanguageOk() (*string, bool)`

GetDefaultLanguageOk returns a tuple with the DefaultLanguage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultLanguage

`func (o *ChannelMutable) SetDefaultLanguage(v string)`

SetDefaultLanguage sets DefaultLanguage field to given value.

### HasDefaultLanguage

`func (o *ChannelMutable) HasDefaultLanguage() bool`

HasDefaultLanguage returns a boolean if a field has been set.

### GetAttributionEnabled

`func (o *ChannelMutable) GetAttributionEnabled() bool`

GetAttributionEnabled returns the AttributionEnabled field if non-nil, zero value otherwise.

### GetAttributionEnabledOk

`func (o *ChannelMutable) GetAttributionEnabledOk() (*bool, bool)`

GetAttributionEnabledOk returns a tuple with the AttributionEnabled field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributionEnabled

`func (o *ChannelMutable) SetAttributionEnabled(v bool)`

SetAttributionEnabled sets AttributionEnabled field to given value.

### HasAttributionEnabled

`func (o *ChannelMutable) HasAttributionEnabled() bool`

HasAttributionEnabled returns a boolean if a field has been set.

### GetStopKeywords

`func (o *ChannelMutable) GetStopKeywords() []string`

GetStopKeywords returns the StopKeywords field if non-nil, zero value otherwise.

### GetStopKeywordsOk

`func (o *ChannelMutable) GetStopKeywordsOk() (*[]string, bool)`

GetStopKeywordsOk returns a tuple with the StopKeywords field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStopKeywords

`func (o *ChannelMutable) SetStopKeywords(v []string)`

SetStopKeywords sets StopKeywords field to given value.


### GetFallbackLanguage

`func (o *ChannelMutable) GetFallbackLanguage() string`

GetFallbackLanguage returns the FallbackLanguage field if non-nil, zero value otherwise.

### GetFallbackLanguageOk

`func (o *ChannelMutable) GetFallbackLanguageOk() (*string, bool)`

GetFallbackLanguageOk returns a tuple with the FallbackLanguage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFallbackLanguage

`func (o *ChannelMutable) SetFallbackLanguage(v string)`

SetFallbackLanguage sets FallbackLanguage field to given value.

### HasFallbackLanguage

`func (o *ChannelMutable) HasFallbackLanguage() bool`

HasFallbackLanguage returns a boolean if a field has been set.

### GetDefaultDialog360Tier

`func (o *ChannelMutable) GetDefaultDialog360Tier() Dialog360Tier`

GetDefaultDialog360Tier returns the DefaultDialog360Tier field if non-nil, zero value otherwise.

### GetDefaultDialog360TierOk

`func (o *ChannelMutable) GetDefaultDialog360TierOk() (*Dialog360Tier, bool)`

GetDefaultDialog360TierOk returns a tuple with the DefaultDialog360Tier field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultDialog360Tier

`func (o *ChannelMutable) SetDefaultDialog360Tier(v Dialog360Tier)`

SetDefaultDialog360Tier sets DefaultDialog360Tier field to given value.

### HasDefaultDialog360Tier

`func (o *ChannelMutable) HasDefaultDialog360Tier() bool`

HasDefaultDialog360Tier returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


