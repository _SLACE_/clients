# ObjectPermission

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ObjectId** | **string** |  | 
**ObjectPermission** | **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | 
**Permissions** | [**ObjectPermissionPermissions**](ObjectPermissionPermissions.md) |  | 
**Permission** | [**Permission**](Permission.md) |  | 

## Methods

### NewObjectPermission

`func NewObjectPermission(objectId string, objectPermission int32, permissions ObjectPermissionPermissions, permission Permission, ) *ObjectPermission`

NewObjectPermission instantiates a new ObjectPermission object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewObjectPermissionWithDefaults

`func NewObjectPermissionWithDefaults() *ObjectPermission`

NewObjectPermissionWithDefaults instantiates a new ObjectPermission object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetObjectId

`func (o *ObjectPermission) GetObjectId() string`

GetObjectId returns the ObjectId field if non-nil, zero value otherwise.

### GetObjectIdOk

`func (o *ObjectPermission) GetObjectIdOk() (*string, bool)`

GetObjectIdOk returns a tuple with the ObjectId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetObjectId

`func (o *ObjectPermission) SetObjectId(v string)`

SetObjectId sets ObjectId field to given value.


### GetObjectPermission

`func (o *ObjectPermission) GetObjectPermission() int32`

GetObjectPermission returns the ObjectPermission field if non-nil, zero value otherwise.

### GetObjectPermissionOk

`func (o *ObjectPermission) GetObjectPermissionOk() (*int32, bool)`

GetObjectPermissionOk returns a tuple with the ObjectPermission field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetObjectPermission

`func (o *ObjectPermission) SetObjectPermission(v int32)`

SetObjectPermission sets ObjectPermission field to given value.


### GetPermissions

`func (o *ObjectPermission) GetPermissions() ObjectPermissionPermissions`

GetPermissions returns the Permissions field if non-nil, zero value otherwise.

### GetPermissionsOk

`func (o *ObjectPermission) GetPermissionsOk() (*ObjectPermissionPermissions, bool)`

GetPermissionsOk returns a tuple with the Permissions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermissions

`func (o *ObjectPermission) SetPermissions(v ObjectPermissionPermissions)`

SetPermissions sets Permissions field to given value.


### GetPermission

`func (o *ObjectPermission) GetPermission() Permission`

GetPermission returns the Permission field if non-nil, zero value otherwise.

### GetPermissionOk

`func (o *ObjectPermission) GetPermissionOk() (*Permission, bool)`

GetPermissionOk returns a tuple with the Permission field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermission

`func (o *ObjectPermission) SetPermission(v Permission)`

SetPermission sets Permission field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


