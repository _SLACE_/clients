# ExternalSystem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**UserLink** | Pointer to **string** |  | [optional] 
**UserIdPattern** | Pointer to **string** |  | [optional] 

## Methods

### NewExternalSystem

`func NewExternalSystem() *ExternalSystem`

NewExternalSystem instantiates a new ExternalSystem object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExternalSystemWithDefaults

`func NewExternalSystemWithDefaults() *ExternalSystem`

NewExternalSystemWithDefaults instantiates a new ExternalSystem object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ExternalSystem) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ExternalSystem) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ExternalSystem) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ExternalSystem) HasName() bool`

HasName returns a boolean if a field has been set.

### GetUserLink

`func (o *ExternalSystem) GetUserLink() string`

GetUserLink returns the UserLink field if non-nil, zero value otherwise.

### GetUserLinkOk

`func (o *ExternalSystem) GetUserLinkOk() (*string, bool)`

GetUserLinkOk returns a tuple with the UserLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserLink

`func (o *ExternalSystem) SetUserLink(v string)`

SetUserLink sets UserLink field to given value.

### HasUserLink

`func (o *ExternalSystem) HasUserLink() bool`

HasUserLink returns a boolean if a field has been set.

### GetUserIdPattern

`func (o *ExternalSystem) GetUserIdPattern() string`

GetUserIdPattern returns the UserIdPattern field if non-nil, zero value otherwise.

### GetUserIdPatternOk

`func (o *ExternalSystem) GetUserIdPatternOk() (*string, bool)`

GetUserIdPatternOk returns a tuple with the UserIdPattern field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserIdPattern

`func (o *ExternalSystem) SetUserIdPattern(v string)`

SetUserIdPattern sets UserIdPattern field to given value.

### HasUserIdPattern

`func (o *ExternalSystem) HasUserIdPattern() bool`

HasUserIdPattern returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


