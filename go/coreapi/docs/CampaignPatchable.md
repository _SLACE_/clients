# CampaignPatchable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TargetChannels** | Pointer to [**[]CampaignPatchableTargetChannelsInner**](CampaignPatchableTargetChannelsInner.md) |  | [optional] 
**TargetContacts** | Pointer to [**[]CampaignPatchableTargetContactsInner**](CampaignPatchableTargetContactsInner.md) |  | [optional] 

## Methods

### NewCampaignPatchable

`func NewCampaignPatchable() *CampaignPatchable`

NewCampaignPatchable instantiates a new CampaignPatchable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignPatchableWithDefaults

`func NewCampaignPatchableWithDefaults() *CampaignPatchable`

NewCampaignPatchableWithDefaults instantiates a new CampaignPatchable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTargetChannels

`func (o *CampaignPatchable) GetTargetChannels() []CampaignPatchableTargetChannelsInner`

GetTargetChannels returns the TargetChannels field if non-nil, zero value otherwise.

### GetTargetChannelsOk

`func (o *CampaignPatchable) GetTargetChannelsOk() (*[]CampaignPatchableTargetChannelsInner, bool)`

GetTargetChannelsOk returns a tuple with the TargetChannels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetChannels

`func (o *CampaignPatchable) SetTargetChannels(v []CampaignPatchableTargetChannelsInner)`

SetTargetChannels sets TargetChannels field to given value.

### HasTargetChannels

`func (o *CampaignPatchable) HasTargetChannels() bool`

HasTargetChannels returns a boolean if a field has been set.

### GetTargetContacts

`func (o *CampaignPatchable) GetTargetContacts() []CampaignPatchableTargetContactsInner`

GetTargetContacts returns the TargetContacts field if non-nil, zero value otherwise.

### GetTargetContactsOk

`func (o *CampaignPatchable) GetTargetContactsOk() (*[]CampaignPatchableTargetContactsInner, bool)`

GetTargetContactsOk returns a tuple with the TargetContacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetContacts

`func (o *CampaignPatchable) SetTargetContacts(v []CampaignPatchableTargetContactsInner)`

SetTargetContacts sets TargetContacts field to given value.

### HasTargetContacts

`func (o *CampaignPatchable) HasTargetContacts() bool`

HasTargetContacts returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


