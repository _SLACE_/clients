# \UserProfileAPI

All URIs are relative to *https://api.slace.com/core*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddUserContactSegment**](UserProfileAPI.md#AddUserContactSegment) | **Post** /me/organizations/{oid}/contacts/{cid}/segments | Add user contact segment
[**AddWebPushSubscription**](UserProfileAPI.md#AddWebPushSubscription) | **Post** /me/web-push-subscriptions | Add new web push subscription
[**AssignUserMessengerToContact**](UserProfileAPI.md#AssignUserMessengerToContact) | **Post** /me/organizations/{oid}/contacts/{cid}/assign/{mid} | Assign user messenger to contact
[**CreateUserContact**](UserProfileAPI.md#CreateUserContact) | **Post** /me/organizations/{oid}/contacts | Create user contact
[**CreateUserMessenger**](UserProfileAPI.md#CreateUserMessenger) | **Post** /me/messengers | Create user messenger
[**DeleteUserContact**](UserProfileAPI.md#DeleteUserContact) | **Delete** /me/organizations/{oid}/contacts/{cid} | Delete user contact
[**DeleteUserMessenger**](UserProfileAPI.md#DeleteUserMessenger) | **Delete** /me/messengers/{mid} | Delete user messenger
[**GetMe**](UserProfileAPI.md#GetMe) | **Get** /me | Get token user
[**GetMeEntrypoints**](UserProfileAPI.md#GetMeEntrypoints) | **Get** /me/entrypoints | Get user entrypoints
[**GetSupportOrganizationID**](UserProfileAPI.md#GetSupportOrganizationID) | **Get** /me/support | Get support organization ID
[**GetUserMessengers**](UserProfileAPI.md#GetUserMessengers) | **Get** /me/messengers | Get user messengers
[**GetWidgetToken**](UserProfileAPI.md#GetWidgetToken) | **Get** /me/widget-token | Get token for support widget
[**ListUserContacts**](UserProfileAPI.md#ListUserContacts) | **Get** /me/organizations/{oid}/contacts | List user contacts
[**PostMeEntrypoints**](UserProfileAPI.md#PostMeEntrypoints) | **Post** /me/entrypoints | Create user entrypoints
[**RemoveWebPushSubscription**](UserProfileAPI.md#RemoveWebPushSubscription) | **Delete** /me/web-push-subscriptions | Remove web push subscription



## AddUserContactSegment

> AddUserContactSegment(ctx, oid, cid).Segment(segment).Execute()

Add user contact segment

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    oid := "oid_example" // string | 
    cid := "cid_example" // string | 
    segment := *openapiclient.NewSegment("SubType_example", "Key_example") // Segment |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.UserProfileAPI.AddUserContactSegment(context.Background(), oid, cid).Segment(segment).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.AddUserContactSegment``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** |  | 
**cid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddUserContactSegmentRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **segment** | [**Segment**](Segment.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddWebPushSubscription

> GenericResponse AddWebPushSubscription(ctx).WebPushSubscriptionData(webPushSubscriptionData).Execute()

Add new web push subscription



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    webPushSubscriptionData := *openapiclient.NewWebPushSubscriptionData("Endpoint_example", *openapiclient.NewWebPushSubscriptionDataKeys("Auth_example", "P256dh_example")) // WebPushSubscriptionData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UserProfileAPI.AddWebPushSubscription(context.Background()).WebPushSubscriptionData(webPushSubscriptionData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.AddWebPushSubscription``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddWebPushSubscription`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `UserProfileAPI.AddWebPushSubscription`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiAddWebPushSubscriptionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webPushSubscriptionData** | [**WebPushSubscriptionData**](WebPushSubscriptionData.md) |  | 

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AssignUserMessengerToContact

> AssignUserMessengerToContact(ctx, oid, cid, mid).Execute()

Assign user messenger to contact



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    oid := "oid_example" // string | 
    cid := "cid_example" // string | 
    mid := "mid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.UserProfileAPI.AssignUserMessengerToContact(context.Background(), oid, cid, mid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.AssignUserMessengerToContact``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** |  | 
**cid** | **string** |  | 
**mid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAssignUserMessengerToContactRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateUserContact

> Contact CreateUserContact(ctx, oid).ContactCreatable(contactCreatable).Execute()

Create user contact



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    oid := "oid_example" // string | 
    contactCreatable := *openapiclient.NewContactCreatable() // ContactCreatable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UserProfileAPI.CreateUserContact(context.Background(), oid).ContactCreatable(contactCreatable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.CreateUserContact``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateUserContact`: Contact
    fmt.Fprintf(os.Stdout, "Response from `UserProfileAPI.CreateUserContact`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateUserContactRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **contactCreatable** | [**ContactCreatable**](ContactCreatable.md) |  | 

### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateUserMessenger

> UserMessenger CreateUserMessenger(ctx).UserMessengerCreateable(userMessengerCreateable).Execute()

Create user messenger

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    userMessengerCreateable := *openapiclient.NewUserMessengerCreateable("MessengerId_example", "MessengerType_example", "Purpose_example") // UserMessengerCreateable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UserProfileAPI.CreateUserMessenger(context.Background()).UserMessengerCreateable(userMessengerCreateable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.CreateUserMessenger``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateUserMessenger`: UserMessenger
    fmt.Fprintf(os.Stdout, "Response from `UserProfileAPI.CreateUserMessenger`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateUserMessengerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userMessengerCreateable** | [**UserMessengerCreateable**](UserMessengerCreateable.md) |  | 

### Return type

[**UserMessenger**](UserMessenger.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteUserContact

> DeleteUserContact(ctx, oid, cid).Execute()

Delete user contact

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    oid := "oid_example" // string | 
    cid := "cid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.UserProfileAPI.DeleteUserContact(context.Background(), oid, cid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.DeleteUserContact``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** |  | 
**cid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteUserContactRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteUserMessenger

> DeleteUserMessenger(ctx, mid).Execute()

Delete user messenger

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    mid := "mid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.UserProfileAPI.DeleteUserMessenger(context.Background(), mid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.DeleteUserMessenger``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**mid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteUserMessengerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMe

> User GetMe(ctx).Execute()

Get token user



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UserProfileAPI.GetMe(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.GetMe``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMe`: User
    fmt.Fprintf(os.Stdout, "Response from `UserProfileAPI.GetMe`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetMeRequest struct via the builder pattern


### Return type

[**User**](User.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMeEntrypoints

> []GetMeEntrypoints200ResponseInner GetMeEntrypoints(ctx).Execute()

Get user entrypoints

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UserProfileAPI.GetMeEntrypoints(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.GetMeEntrypoints``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMeEntrypoints`: []GetMeEntrypoints200ResponseInner
    fmt.Fprintf(os.Stdout, "Response from `UserProfileAPI.GetMeEntrypoints`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetMeEntrypointsRequest struct via the builder pattern


### Return type

[**[]GetMeEntrypoints200ResponseInner**](GetMeEntrypoints200ResponseInner.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetSupportOrganizationID

> GetSupportOrganizationID200Response GetSupportOrganizationID(ctx).Execute()

Get support organization ID



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UserProfileAPI.GetSupportOrganizationID(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.GetSupportOrganizationID``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetSupportOrganizationID`: GetSupportOrganizationID200Response
    fmt.Fprintf(os.Stdout, "Response from `UserProfileAPI.GetSupportOrganizationID`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetSupportOrganizationIDRequest struct via the builder pattern


### Return type

[**GetSupportOrganizationID200Response**](GetSupportOrganizationID200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetUserMessengers

> []UserMessenger GetUserMessengers(ctx).Execute()

Get user messengers

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UserProfileAPI.GetUserMessengers(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.GetUserMessengers``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetUserMessengers`: []UserMessenger
    fmt.Fprintf(os.Stdout, "Response from `UserProfileAPI.GetUserMessengers`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetUserMessengersRequest struct via the builder pattern


### Return type

[**[]UserMessenger**](UserMessenger.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetWidgetToken

> GetWidgetToken(ctx).WidgetTokenResponse(widgetTokenResponse).Execute()

Get token for support widget

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    widgetTokenResponse := *openapiclient.NewWidgetTokenResponse("Token_example") // WidgetTokenResponse |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.UserProfileAPI.GetWidgetToken(context.Background()).WidgetTokenResponse(widgetTokenResponse).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.GetWidgetToken``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetWidgetTokenRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **widgetTokenResponse** | [**WidgetTokenResponse**](WidgetTokenResponse.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListUserContacts

> []Contact ListUserContacts(ctx, oid).Execute()

List user contacts

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    oid := "oid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UserProfileAPI.ListUserContacts(context.Background(), oid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.ListUserContacts``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListUserContacts`: []Contact
    fmt.Fprintf(os.Stdout, "Response from `UserProfileAPI.ListUserContacts`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListUserContactsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostMeEntrypoints

> PostMeEntrypoints200Response PostMeEntrypoints(ctx).PostMeEntrypointsRequest(postMeEntrypointsRequest).Execute()

Create user entrypoints

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    postMeEntrypointsRequest := *openapiclient.NewPostMeEntrypointsRequest() // PostMeEntrypointsRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UserProfileAPI.PostMeEntrypoints(context.Background()).PostMeEntrypointsRequest(postMeEntrypointsRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.PostMeEntrypoints``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PostMeEntrypoints`: PostMeEntrypoints200Response
    fmt.Fprintf(os.Stdout, "Response from `UserProfileAPI.PostMeEntrypoints`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiPostMeEntrypointsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **postMeEntrypointsRequest** | [**PostMeEntrypointsRequest**](PostMeEntrypointsRequest.md) |  | 

### Return type

[**PostMeEntrypoints200Response**](PostMeEntrypoints200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RemoveWebPushSubscription

> GenericResponse RemoveWebPushSubscription(ctx).WebPushSubscriptionData(webPushSubscriptionData).Execute()

Remove web push subscription



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    webPushSubscriptionData := *openapiclient.NewWebPushSubscriptionData("Endpoint_example", *openapiclient.NewWebPushSubscriptionDataKeys("Auth_example", "P256dh_example")) // WebPushSubscriptionData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UserProfileAPI.RemoveWebPushSubscription(context.Background()).WebPushSubscriptionData(webPushSubscriptionData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UserProfileAPI.RemoveWebPushSubscription``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RemoveWebPushSubscription`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `UserProfileAPI.RemoveWebPushSubscription`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiRemoveWebPushSubscriptionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webPushSubscriptionData** | [**WebPushSubscriptionData**](WebPushSubscriptionData.md) |  | 

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

