# ChannelLocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Type** | **string** |  | 
**TagName** | **string** |  | 
**ExternalName** | **string** |  | 
**OrganizationId** | **string** |  | 
**Organization** | Pointer to [**Organization**](Organization.md) |  | [optional] 
**Channel** | Pointer to [**Channel**](Channel.md) |  | [optional] 
**Name** | **string** |  | 
**ImageUrl** | Pointer to **string** |  | [optional] 
**CreatedAt** | **string** |  | 
**UpdatedAt** | **string** |  | 
**ChannelId** | Pointer to **string** |  | [optional] 
**Address** | Pointer to [**ContactAddress**](ContactAddress.md) |  | [optional] 
**ExternalReferences** | Pointer to **map[string]interface{}** |  | [optional] 
**Default** | Pointer to **bool** |  | [optional] 
**Coordinates** | Pointer to [**ChannelLocationCoordinates**](ChannelLocationCoordinates.md) |  | [optional] 
**Billing** | Pointer to **bool** |  | [optional] 
**Scope** | **string** |  | 

## Methods

### NewChannelLocation

`func NewChannelLocation(id string, type_ string, tagName string, externalName string, organizationId string, name string, createdAt string, updatedAt string, scope string, ) *ChannelLocation`

NewChannelLocation instantiates a new ChannelLocation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChannelLocationWithDefaults

`func NewChannelLocationWithDefaults() *ChannelLocation`

NewChannelLocationWithDefaults instantiates a new ChannelLocation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ChannelLocation) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ChannelLocation) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ChannelLocation) SetId(v string)`

SetId sets Id field to given value.


### GetType

`func (o *ChannelLocation) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ChannelLocation) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ChannelLocation) SetType(v string)`

SetType sets Type field to given value.


### GetTagName

`func (o *ChannelLocation) GetTagName() string`

GetTagName returns the TagName field if non-nil, zero value otherwise.

### GetTagNameOk

`func (o *ChannelLocation) GetTagNameOk() (*string, bool)`

GetTagNameOk returns a tuple with the TagName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTagName

`func (o *ChannelLocation) SetTagName(v string)`

SetTagName sets TagName field to given value.


### GetExternalName

`func (o *ChannelLocation) GetExternalName() string`

GetExternalName returns the ExternalName field if non-nil, zero value otherwise.

### GetExternalNameOk

`func (o *ChannelLocation) GetExternalNameOk() (*string, bool)`

GetExternalNameOk returns a tuple with the ExternalName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalName

`func (o *ChannelLocation) SetExternalName(v string)`

SetExternalName sets ExternalName field to given value.


### GetOrganizationId

`func (o *ChannelLocation) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *ChannelLocation) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *ChannelLocation) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetOrganization

`func (o *ChannelLocation) GetOrganization() Organization`

GetOrganization returns the Organization field if non-nil, zero value otherwise.

### GetOrganizationOk

`func (o *ChannelLocation) GetOrganizationOk() (*Organization, bool)`

GetOrganizationOk returns a tuple with the Organization field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganization

`func (o *ChannelLocation) SetOrganization(v Organization)`

SetOrganization sets Organization field to given value.

### HasOrganization

`func (o *ChannelLocation) HasOrganization() bool`

HasOrganization returns a boolean if a field has been set.

### GetChannel

`func (o *ChannelLocation) GetChannel() Channel`

GetChannel returns the Channel field if non-nil, zero value otherwise.

### GetChannelOk

`func (o *ChannelLocation) GetChannelOk() (*Channel, bool)`

GetChannelOk returns a tuple with the Channel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannel

`func (o *ChannelLocation) SetChannel(v Channel)`

SetChannel sets Channel field to given value.

### HasChannel

`func (o *ChannelLocation) HasChannel() bool`

HasChannel returns a boolean if a field has been set.

### GetName

`func (o *ChannelLocation) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ChannelLocation) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ChannelLocation) SetName(v string)`

SetName sets Name field to given value.


### GetImageUrl

`func (o *ChannelLocation) GetImageUrl() string`

GetImageUrl returns the ImageUrl field if non-nil, zero value otherwise.

### GetImageUrlOk

`func (o *ChannelLocation) GetImageUrlOk() (*string, bool)`

GetImageUrlOk returns a tuple with the ImageUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetImageUrl

`func (o *ChannelLocation) SetImageUrl(v string)`

SetImageUrl sets ImageUrl field to given value.

### HasImageUrl

`func (o *ChannelLocation) HasImageUrl() bool`

HasImageUrl returns a boolean if a field has been set.

### GetCreatedAt

`func (o *ChannelLocation) GetCreatedAt() string`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *ChannelLocation) GetCreatedAtOk() (*string, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *ChannelLocation) SetCreatedAt(v string)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *ChannelLocation) GetUpdatedAt() string`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *ChannelLocation) GetUpdatedAtOk() (*string, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *ChannelLocation) SetUpdatedAt(v string)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetChannelId

`func (o *ChannelLocation) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *ChannelLocation) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *ChannelLocation) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *ChannelLocation) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetAddress

`func (o *ChannelLocation) GetAddress() ContactAddress`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *ChannelLocation) GetAddressOk() (*ContactAddress, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *ChannelLocation) SetAddress(v ContactAddress)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *ChannelLocation) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetExternalReferences

`func (o *ChannelLocation) GetExternalReferences() map[string]interface{}`

GetExternalReferences returns the ExternalReferences field if non-nil, zero value otherwise.

### GetExternalReferencesOk

`func (o *ChannelLocation) GetExternalReferencesOk() (*map[string]interface{}, bool)`

GetExternalReferencesOk returns a tuple with the ExternalReferences field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalReferences

`func (o *ChannelLocation) SetExternalReferences(v map[string]interface{})`

SetExternalReferences sets ExternalReferences field to given value.

### HasExternalReferences

`func (o *ChannelLocation) HasExternalReferences() bool`

HasExternalReferences returns a boolean if a field has been set.

### GetDefault

`func (o *ChannelLocation) GetDefault() bool`

GetDefault returns the Default field if non-nil, zero value otherwise.

### GetDefaultOk

`func (o *ChannelLocation) GetDefaultOk() (*bool, bool)`

GetDefaultOk returns a tuple with the Default field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefault

`func (o *ChannelLocation) SetDefault(v bool)`

SetDefault sets Default field to given value.

### HasDefault

`func (o *ChannelLocation) HasDefault() bool`

HasDefault returns a boolean if a field has been set.

### GetCoordinates

`func (o *ChannelLocation) GetCoordinates() ChannelLocationCoordinates`

GetCoordinates returns the Coordinates field if non-nil, zero value otherwise.

### GetCoordinatesOk

`func (o *ChannelLocation) GetCoordinatesOk() (*ChannelLocationCoordinates, bool)`

GetCoordinatesOk returns a tuple with the Coordinates field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCoordinates

`func (o *ChannelLocation) SetCoordinates(v ChannelLocationCoordinates)`

SetCoordinates sets Coordinates field to given value.

### HasCoordinates

`func (o *ChannelLocation) HasCoordinates() bool`

HasCoordinates returns a boolean if a field has been set.

### GetBilling

`func (o *ChannelLocation) GetBilling() bool`

GetBilling returns the Billing field if non-nil, zero value otherwise.

### GetBillingOk

`func (o *ChannelLocation) GetBillingOk() (*bool, bool)`

GetBillingOk returns a tuple with the Billing field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBilling

`func (o *ChannelLocation) SetBilling(v bool)`

SetBilling sets Billing field to given value.

### HasBilling

`func (o *ChannelLocation) HasBilling() bool`

HasBilling returns a boolean if a field has been set.

### GetScope

`func (o *ChannelLocation) GetScope() string`

GetScope returns the Scope field if non-nil, zero value otherwise.

### GetScopeOk

`func (o *ChannelLocation) GetScopeOk() (*string, bool)`

GetScopeOk returns a tuple with the Scope field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScope

`func (o *ChannelLocation) SetScope(v string)`

SetScope sets Scope field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


