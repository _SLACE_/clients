# CampaignSendoutSplitInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Amount** | Pointer to **int32** |  | [optional] 
**Status** | Pointer to **string** |  | [optional] 

## Methods

### NewCampaignSendoutSplitInner

`func NewCampaignSendoutSplitInner() *CampaignSendoutSplitInner`

NewCampaignSendoutSplitInner instantiates a new CampaignSendoutSplitInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignSendoutSplitInnerWithDefaults

`func NewCampaignSendoutSplitInnerWithDefaults() *CampaignSendoutSplitInner`

NewCampaignSendoutSplitInnerWithDefaults instantiates a new CampaignSendoutSplitInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAmount

`func (o *CampaignSendoutSplitInner) GetAmount() int32`

GetAmount returns the Amount field if non-nil, zero value otherwise.

### GetAmountOk

`func (o *CampaignSendoutSplitInner) GetAmountOk() (*int32, bool)`

GetAmountOk returns a tuple with the Amount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAmount

`func (o *CampaignSendoutSplitInner) SetAmount(v int32)`

SetAmount sets Amount field to given value.

### HasAmount

`func (o *CampaignSendoutSplitInner) HasAmount() bool`

HasAmount returns a boolean if a field has been set.

### GetStatus

`func (o *CampaignSendoutSplitInner) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *CampaignSendoutSplitInner) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *CampaignSendoutSplitInner) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *CampaignSendoutSplitInner) HasStatus() bool`

HasStatus returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


