# StampCard

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CampaignName** | **string** |  | 
**Points** | **int32** |  | 
**RewardName** | **string** |  | 
**StampIconUrl** | Pointer to **string** |  | [optional] 
**RewardIconUrl** | Pointer to **string** |  | [optional] 

## Methods

### NewStampCard

`func NewStampCard(campaignName string, points int32, rewardName string, ) *StampCard`

NewStampCard instantiates a new StampCard object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStampCardWithDefaults

`func NewStampCardWithDefaults() *StampCard`

NewStampCardWithDefaults instantiates a new StampCard object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCampaignName

`func (o *StampCard) GetCampaignName() string`

GetCampaignName returns the CampaignName field if non-nil, zero value otherwise.

### GetCampaignNameOk

`func (o *StampCard) GetCampaignNameOk() (*string, bool)`

GetCampaignNameOk returns a tuple with the CampaignName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCampaignName

`func (o *StampCard) SetCampaignName(v string)`

SetCampaignName sets CampaignName field to given value.


### GetPoints

`func (o *StampCard) GetPoints() int32`

GetPoints returns the Points field if non-nil, zero value otherwise.

### GetPointsOk

`func (o *StampCard) GetPointsOk() (*int32, bool)`

GetPointsOk returns a tuple with the Points field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPoints

`func (o *StampCard) SetPoints(v int32)`

SetPoints sets Points field to given value.


### GetRewardName

`func (o *StampCard) GetRewardName() string`

GetRewardName returns the RewardName field if non-nil, zero value otherwise.

### GetRewardNameOk

`func (o *StampCard) GetRewardNameOk() (*string, bool)`

GetRewardNameOk returns a tuple with the RewardName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRewardName

`func (o *StampCard) SetRewardName(v string)`

SetRewardName sets RewardName field to given value.


### GetStampIconUrl

`func (o *StampCard) GetStampIconUrl() string`

GetStampIconUrl returns the StampIconUrl field if non-nil, zero value otherwise.

### GetStampIconUrlOk

`func (o *StampCard) GetStampIconUrlOk() (*string, bool)`

GetStampIconUrlOk returns a tuple with the StampIconUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStampIconUrl

`func (o *StampCard) SetStampIconUrl(v string)`

SetStampIconUrl sets StampIconUrl field to given value.

### HasStampIconUrl

`func (o *StampCard) HasStampIconUrl() bool`

HasStampIconUrl returns a boolean if a field has been set.

### GetRewardIconUrl

`func (o *StampCard) GetRewardIconUrl() string`

GetRewardIconUrl returns the RewardIconUrl field if non-nil, zero value otherwise.

### GetRewardIconUrlOk

`func (o *StampCard) GetRewardIconUrlOk() (*string, bool)`

GetRewardIconUrlOk returns a tuple with the RewardIconUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRewardIconUrl

`func (o *StampCard) SetRewardIconUrl(v string)`

SetRewardIconUrl sets RewardIconUrl field to given value.

### HasRewardIconUrl

`func (o *StampCard) HasRewardIconUrl() bool`

HasRewardIconUrl returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


