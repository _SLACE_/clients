# CampaignPatchableTargetContactsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Language** | Pointer to **string** |  | [optional] 
**Id** | **string** |  | 

## Methods

### NewCampaignPatchableTargetContactsInner

`func NewCampaignPatchableTargetContactsInner(id string, ) *CampaignPatchableTargetContactsInner`

NewCampaignPatchableTargetContactsInner instantiates a new CampaignPatchableTargetContactsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignPatchableTargetContactsInnerWithDefaults

`func NewCampaignPatchableTargetContactsInnerWithDefaults() *CampaignPatchableTargetContactsInner`

NewCampaignPatchableTargetContactsInnerWithDefaults instantiates a new CampaignPatchableTargetContactsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLanguage

`func (o *CampaignPatchableTargetContactsInner) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *CampaignPatchableTargetContactsInner) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *CampaignPatchableTargetContactsInner) SetLanguage(v string)`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *CampaignPatchableTargetContactsInner) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.

### GetId

`func (o *CampaignPatchableTargetContactsInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CampaignPatchableTargetContactsInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CampaignPatchableTargetContactsInner) SetId(v string)`

SetId sets Id field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


