# TemplateType

## Enum


* `TEMPLATE_TYPE_INTERNAL` (value: `"internal"`)

* `TEMPLATE_TYPE_WHATS_APP` (value: `"whatsapp"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


