# \GatewaysAPI

All URIs are relative to *https://api.slace.com/core*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateDraftGateway**](GatewaysAPI.md#CreateDraftGateway) | **Post** /gateways/draft | Create draft gateway
[**CreateGateway**](GatewaysAPI.md#CreateGateway) | **Post** /gateways | Create gateway
[**DeleteGateway**](GatewaysAPI.md#DeleteGateway) | **Delete** /gateways/{gateway_id} | Delete gateway
[**GetGateway**](GatewaysAPI.md#GetGateway) | **Get** /gateways/{gateway_id} | Get gateway
[**GetGatewaysConnections**](GatewaysAPI.md#GetGatewaysConnections) | **Get** /gateways/connections | Get gateways connections with 360 dialog hub
[**GetMessengerSettings**](GatewaysAPI.md#GetMessengerSettings) | **Get** /gateways/{gateway_id}/settings | Get messenger settings
[**ListGateways**](GatewaysAPI.md#ListGateways) | **Get** /gateways | List gateways
[**PatchGateway**](GatewaysAPI.md#PatchGateway) | **Patch** /gateways/{gateway_id} | Patch gateway
[**UpdateGateway**](GatewaysAPI.md#UpdateGateway) | **Put** /gateways/{gateway_id} | Update gateway
[**UpdateMessengerSettings**](GatewaysAPI.md#UpdateMessengerSettings) | **Patch** /gateways/{gateway_id}/settings | Update messenger settings



## CreateDraftGateway

> Gateway CreateDraftGateway(ctx).GatewayDraftCreateData(gatewayDraftCreateData).Execute()

Create draft gateway



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    gatewayDraftCreateData := *openapiclient.NewGatewayDraftCreateData("ChannelId_example", openapiclient.Messenger("WhatsApp"), openapiclient.Provider("MessagePipe")) // GatewayDraftCreateData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GatewaysAPI.CreateDraftGateway(context.Background()).GatewayDraftCreateData(gatewayDraftCreateData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GatewaysAPI.CreateDraftGateway``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateDraftGateway`: Gateway
    fmt.Fprintf(os.Stdout, "Response from `GatewaysAPI.CreateDraftGateway`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateDraftGatewayRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gatewayDraftCreateData** | [**GatewayDraftCreateData**](GatewayDraftCreateData.md) |  | 

### Return type

[**Gateway**](Gateway.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/xml, multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateGateway

> Gateway CreateGateway(ctx).GatewayCreateData(gatewayCreateData).Execute()

Create gateway



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    gatewayCreateData := *openapiclient.NewGatewayCreateData("ChannelId_example", "Name_example", "Maid_example", openapiclient.Messenger("WhatsApp"), openapiclient.Provider("MessagePipe"), "AuthKey_example", "AuthSecret_example") // GatewayCreateData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GatewaysAPI.CreateGateway(context.Background()).GatewayCreateData(gatewayCreateData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GatewaysAPI.CreateGateway``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateGateway`: Gateway
    fmt.Fprintf(os.Stdout, "Response from `GatewaysAPI.CreateGateway`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateGatewayRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gatewayCreateData** | [**GatewayCreateData**](GatewayCreateData.md) |  | 

### Return type

[**Gateway**](Gateway.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteGateway

> DeleteGateway(ctx, gatewayId).Execute()

Delete gateway



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.GatewaysAPI.DeleteGateway(context.Background(), gatewayId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GatewaysAPI.DeleteGateway``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteGatewayRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetGateway

> Gateway GetGateway(ctx, gatewayId).Execute()

Get gateway



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GatewaysAPI.GetGateway(context.Background(), gatewayId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GatewaysAPI.GetGateway``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetGateway`: Gateway
    fmt.Fprintf(os.Stdout, "Response from `GatewaysAPI.GetGateway`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetGatewayRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Gateway**](Gateway.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetGatewaysConnections

> []GetGatewaysConnections200ResponseInner GetGatewaysConnections(ctx).ExternalClientId(externalClientId).ExternalIds(externalIds).Execute()

Get gateways connections with 360 dialog hub

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    externalClientId := "externalClientId_example" // string |  (optional)
    externalIds := "externalIds_example" // string | In 360 hub case it is \"channels\" param (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GatewaysAPI.GetGatewaysConnections(context.Background()).ExternalClientId(externalClientId).ExternalIds(externalIds).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GatewaysAPI.GetGatewaysConnections``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetGatewaysConnections`: []GetGatewaysConnections200ResponseInner
    fmt.Fprintf(os.Stdout, "Response from `GatewaysAPI.GetGatewaysConnections`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetGatewaysConnectionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externalClientId** | **string** |  | 
 **externalIds** | **string** | In 360 hub case it is \&quot;channels\&quot; param | 

### Return type

[**[]GetGatewaysConnections200ResponseInner**](GetGatewaysConnections200ResponseInner.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMessengerSettings

> MessengerSettings GetMessengerSettings(ctx, gatewayId).Execute()

Get messenger settings



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GatewaysAPI.GetMessengerSettings(context.Background(), gatewayId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GatewaysAPI.GetMessengerSettings``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMessengerSettings`: MessengerSettings
    fmt.Fprintf(os.Stdout, "Response from `GatewaysAPI.GetMessengerSettings`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetMessengerSettingsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**MessengerSettings**](MessengerSettings.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListGateways

> GatewaysList ListGateways(ctx).Type_(type_).CustomerId(customerId).OrganizationId(organizationId).ChannelId(channelId).Search(search).Sort(sort).Limit(limit).Offset(offset).Execute()

List gateways



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    type_ := "type__example" // string |  (optional)
    customerId := "customerId_example" // string |  (optional)
    organizationId := "organizationId_example" // string |  (optional)
    channelId := "channelId_example" // string |  (optional)
    search := "search_example" // string |  (optional)
    sort := "sort_example" // string |  (optional)
    limit := int32(56) // int32 |  (optional)
    offset := int32(56) // int32 |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GatewaysAPI.ListGateways(context.Background()).Type_(type_).CustomerId(customerId).OrganizationId(organizationId).ChannelId(channelId).Search(search).Sort(sort).Limit(limit).Offset(offset).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GatewaysAPI.ListGateways``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListGateways`: GatewaysList
    fmt.Fprintf(os.Stdout, "Response from `GatewaysAPI.ListGateways`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListGatewaysRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type_** | **string** |  | 
 **customerId** | **string** |  | 
 **organizationId** | **string** |  | 
 **channelId** | **string** |  | 
 **search** | **string** |  | 
 **sort** | **string** |  | 
 **limit** | **int32** |  | 
 **offset** | **int32** |  | 

### Return type

[**GatewaysList**](GatewaysList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PatchGateway

> Gateway PatchGateway(ctx, gatewayId).GatewayPatchData(gatewayPatchData).Execute()

Patch gateway



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    gatewayPatchData := *openapiclient.NewGatewayPatchData() // GatewayPatchData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GatewaysAPI.PatchGateway(context.Background(), gatewayId).GatewayPatchData(gatewayPatchData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GatewaysAPI.PatchGateway``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PatchGateway`: Gateway
    fmt.Fprintf(os.Stdout, "Response from `GatewaysAPI.PatchGateway`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPatchGatewayRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **gatewayPatchData** | [**GatewayPatchData**](GatewayPatchData.md) |  | 

### Return type

[**Gateway**](Gateway.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateGateway

> Gateway UpdateGateway(ctx, gatewayId).GatewayEditData(gatewayEditData).Execute()

Update gateway



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    gatewayEditData := *openapiclient.NewGatewayEditData("ChannelId_example", "Name_example") // GatewayEditData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GatewaysAPI.UpdateGateway(context.Background(), gatewayId).GatewayEditData(gatewayEditData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GatewaysAPI.UpdateGateway``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateGateway`: Gateway
    fmt.Fprintf(os.Stdout, "Response from `GatewaysAPI.UpdateGateway`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateGatewayRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **gatewayEditData** | [**GatewayEditData**](GatewayEditData.md) |  | 

### Return type

[**Gateway**](Gateway.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateMessengerSettings

> MessengerSettings UpdateMessengerSettings(ctx, gatewayId).MessengerSettings(messengerSettings).Execute()

Update messenger settings



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    messengerSettings := *openapiclient.NewMessengerSettings() // MessengerSettings |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GatewaysAPI.UpdateMessengerSettings(context.Background(), gatewayId).MessengerSettings(messengerSettings).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GatewaysAPI.UpdateMessengerSettings``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateMessengerSettings`: MessengerSettings
    fmt.Fprintf(os.Stdout, "Response from `GatewaysAPI.UpdateMessengerSettings`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateMessengerSettingsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **messengerSettings** | [**MessengerSettings**](MessengerSettings.md) |  | 

### Return type

[**MessengerSettings**](MessengerSettings.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

