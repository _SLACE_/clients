# WhatsAppTemplateMessageComponentParametersInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Text** | Pointer to **string** |  | [optional] 
**Payload** | Pointer to **string** | For buttons only | [optional] 
**Currency** | Pointer to [**WhatsAppTemplateMessageComponentParametersInnerCurrency**](WhatsAppTemplateMessageComponentParametersInnerCurrency.md) |  | [optional] 
**DateTime** | Pointer to [**WhatsAppTemplateMessageComponentParametersInnerDateTime**](WhatsAppTemplateMessageComponentParametersInnerDateTime.md) |  | [optional] 
**Image** | Pointer to [**WhatsAppTemplateMessageMedia**](WhatsAppTemplateMessageMedia.md) |  | [optional] 
**Video** | Pointer to [**WhatsAppTemplateMessageMedia**](WhatsAppTemplateMessageMedia.md) |  | [optional] 
**Document** | Pointer to [**WhatsAppTemplateMessageMedia**](WhatsAppTemplateMessageMedia.md) |  | [optional] 

## Methods

### NewWhatsAppTemplateMessageComponentParametersInner

`func NewWhatsAppTemplateMessageComponentParametersInner(type_ string, ) *WhatsAppTemplateMessageComponentParametersInner`

NewWhatsAppTemplateMessageComponentParametersInner instantiates a new WhatsAppTemplateMessageComponentParametersInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMessageComponentParametersInnerWithDefaults

`func NewWhatsAppTemplateMessageComponentParametersInnerWithDefaults() *WhatsAppTemplateMessageComponentParametersInner`

NewWhatsAppTemplateMessageComponentParametersInnerWithDefaults instantiates a new WhatsAppTemplateMessageComponentParametersInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WhatsAppTemplateMessageComponentParametersInner) SetType(v string)`

SetType sets Type field to given value.


### GetText

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *WhatsAppTemplateMessageComponentParametersInner) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *WhatsAppTemplateMessageComponentParametersInner) HasText() bool`

HasText returns a boolean if a field has been set.

### GetPayload

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetPayload() string`

GetPayload returns the Payload field if non-nil, zero value otherwise.

### GetPayloadOk

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetPayloadOk() (*string, bool)`

GetPayloadOk returns a tuple with the Payload field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayload

`func (o *WhatsAppTemplateMessageComponentParametersInner) SetPayload(v string)`

SetPayload sets Payload field to given value.

### HasPayload

`func (o *WhatsAppTemplateMessageComponentParametersInner) HasPayload() bool`

HasPayload returns a boolean if a field has been set.

### GetCurrency

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetCurrency() WhatsAppTemplateMessageComponentParametersInnerCurrency`

GetCurrency returns the Currency field if non-nil, zero value otherwise.

### GetCurrencyOk

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetCurrencyOk() (*WhatsAppTemplateMessageComponentParametersInnerCurrency, bool)`

GetCurrencyOk returns a tuple with the Currency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrency

`func (o *WhatsAppTemplateMessageComponentParametersInner) SetCurrency(v WhatsAppTemplateMessageComponentParametersInnerCurrency)`

SetCurrency sets Currency field to given value.

### HasCurrency

`func (o *WhatsAppTemplateMessageComponentParametersInner) HasCurrency() bool`

HasCurrency returns a boolean if a field has been set.

### GetDateTime

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetDateTime() WhatsAppTemplateMessageComponentParametersInnerDateTime`

GetDateTime returns the DateTime field if non-nil, zero value otherwise.

### GetDateTimeOk

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetDateTimeOk() (*WhatsAppTemplateMessageComponentParametersInnerDateTime, bool)`

GetDateTimeOk returns a tuple with the DateTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateTime

`func (o *WhatsAppTemplateMessageComponentParametersInner) SetDateTime(v WhatsAppTemplateMessageComponentParametersInnerDateTime)`

SetDateTime sets DateTime field to given value.

### HasDateTime

`func (o *WhatsAppTemplateMessageComponentParametersInner) HasDateTime() bool`

HasDateTime returns a boolean if a field has been set.

### GetImage

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetImage() WhatsAppTemplateMessageMedia`

GetImage returns the Image field if non-nil, zero value otherwise.

### GetImageOk

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetImageOk() (*WhatsAppTemplateMessageMedia, bool)`

GetImageOk returns a tuple with the Image field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetImage

`func (o *WhatsAppTemplateMessageComponentParametersInner) SetImage(v WhatsAppTemplateMessageMedia)`

SetImage sets Image field to given value.

### HasImage

`func (o *WhatsAppTemplateMessageComponentParametersInner) HasImage() bool`

HasImage returns a boolean if a field has been set.

### GetVideo

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetVideo() WhatsAppTemplateMessageMedia`

GetVideo returns the Video field if non-nil, zero value otherwise.

### GetVideoOk

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetVideoOk() (*WhatsAppTemplateMessageMedia, bool)`

GetVideoOk returns a tuple with the Video field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideo

`func (o *WhatsAppTemplateMessageComponentParametersInner) SetVideo(v WhatsAppTemplateMessageMedia)`

SetVideo sets Video field to given value.

### HasVideo

`func (o *WhatsAppTemplateMessageComponentParametersInner) HasVideo() bool`

HasVideo returns a boolean if a field has been set.

### GetDocument

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetDocument() WhatsAppTemplateMessageMedia`

GetDocument returns the Document field if non-nil, zero value otherwise.

### GetDocumentOk

`func (o *WhatsAppTemplateMessageComponentParametersInner) GetDocumentOk() (*WhatsAppTemplateMessageMedia, bool)`

GetDocumentOk returns a tuple with the Document field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDocument

`func (o *WhatsAppTemplateMessageComponentParametersInner) SetDocument(v WhatsAppTemplateMessageMedia)`

SetDocument sets Document field to given value.

### HasDocument

`func (o *WhatsAppTemplateMessageComponentParametersInner) HasDocument() bool`

HasDocument returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


