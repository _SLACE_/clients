# GetSupportOrganizationID200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OrganizationId** | **string** |  | 

## Methods

### NewGetSupportOrganizationID200Response

`func NewGetSupportOrganizationID200Response(organizationId string, ) *GetSupportOrganizationID200Response`

NewGetSupportOrganizationID200Response instantiates a new GetSupportOrganizationID200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetSupportOrganizationID200ResponseWithDefaults

`func NewGetSupportOrganizationID200ResponseWithDefaults() *GetSupportOrganizationID200Response`

NewGetSupportOrganizationID200ResponseWithDefaults instantiates a new GetSupportOrganizationID200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOrganizationId

`func (o *GetSupportOrganizationID200Response) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *GetSupportOrganizationID200Response) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *GetSupportOrganizationID200Response) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


