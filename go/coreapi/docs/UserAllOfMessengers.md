# UserAllOfMessengers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**UserId** | Pointer to **string** |  | [optional] 
**MessengerId** | Pointer to **string** |  | [optional] 
**MessengerType** | Pointer to **string** |  | [optional] 
**ProfileName** | Pointer to **string** |  | [optional] 
**Purpose** | Pointer to **string** |  | [optional] 
**Verified** | Pointer to **bool** |  | [optional] 
**VerifiedAt** | Pointer to **time.Time** |  | [optional] 
**CreatedAt** | Pointer to **time.Time** |  | [optional] 
**UpdatedAt** | Pointer to **time.Time** |  | [optional] 

## Methods

### NewUserAllOfMessengers

`func NewUserAllOfMessengers() *UserAllOfMessengers`

NewUserAllOfMessengers instantiates a new UserAllOfMessengers object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUserAllOfMessengersWithDefaults

`func NewUserAllOfMessengersWithDefaults() *UserAllOfMessengers`

NewUserAllOfMessengersWithDefaults instantiates a new UserAllOfMessengers object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *UserAllOfMessengers) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *UserAllOfMessengers) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *UserAllOfMessengers) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *UserAllOfMessengers) HasId() bool`

HasId returns a boolean if a field has been set.

### GetUserId

`func (o *UserAllOfMessengers) GetUserId() string`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *UserAllOfMessengers) GetUserIdOk() (*string, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *UserAllOfMessengers) SetUserId(v string)`

SetUserId sets UserId field to given value.

### HasUserId

`func (o *UserAllOfMessengers) HasUserId() bool`

HasUserId returns a boolean if a field has been set.

### GetMessengerId

`func (o *UserAllOfMessengers) GetMessengerId() string`

GetMessengerId returns the MessengerId field if non-nil, zero value otherwise.

### GetMessengerIdOk

`func (o *UserAllOfMessengers) GetMessengerIdOk() (*string, bool)`

GetMessengerIdOk returns a tuple with the MessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerId

`func (o *UserAllOfMessengers) SetMessengerId(v string)`

SetMessengerId sets MessengerId field to given value.

### HasMessengerId

`func (o *UserAllOfMessengers) HasMessengerId() bool`

HasMessengerId returns a boolean if a field has been set.

### GetMessengerType

`func (o *UserAllOfMessengers) GetMessengerType() string`

GetMessengerType returns the MessengerType field if non-nil, zero value otherwise.

### GetMessengerTypeOk

`func (o *UserAllOfMessengers) GetMessengerTypeOk() (*string, bool)`

GetMessengerTypeOk returns a tuple with the MessengerType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerType

`func (o *UserAllOfMessengers) SetMessengerType(v string)`

SetMessengerType sets MessengerType field to given value.

### HasMessengerType

`func (o *UserAllOfMessengers) HasMessengerType() bool`

HasMessengerType returns a boolean if a field has been set.

### GetProfileName

`func (o *UserAllOfMessengers) GetProfileName() string`

GetProfileName returns the ProfileName field if non-nil, zero value otherwise.

### GetProfileNameOk

`func (o *UserAllOfMessengers) GetProfileNameOk() (*string, bool)`

GetProfileNameOk returns a tuple with the ProfileName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProfileName

`func (o *UserAllOfMessengers) SetProfileName(v string)`

SetProfileName sets ProfileName field to given value.

### HasProfileName

`func (o *UserAllOfMessengers) HasProfileName() bool`

HasProfileName returns a boolean if a field has been set.

### GetPurpose

`func (o *UserAllOfMessengers) GetPurpose() string`

GetPurpose returns the Purpose field if non-nil, zero value otherwise.

### GetPurposeOk

`func (o *UserAllOfMessengers) GetPurposeOk() (*string, bool)`

GetPurposeOk returns a tuple with the Purpose field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPurpose

`func (o *UserAllOfMessengers) SetPurpose(v string)`

SetPurpose sets Purpose field to given value.

### HasPurpose

`func (o *UserAllOfMessengers) HasPurpose() bool`

HasPurpose returns a boolean if a field has been set.

### GetVerified

`func (o *UserAllOfMessengers) GetVerified() bool`

GetVerified returns the Verified field if non-nil, zero value otherwise.

### GetVerifiedOk

`func (o *UserAllOfMessengers) GetVerifiedOk() (*bool, bool)`

GetVerifiedOk returns a tuple with the Verified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVerified

`func (o *UserAllOfMessengers) SetVerified(v bool)`

SetVerified sets Verified field to given value.

### HasVerified

`func (o *UserAllOfMessengers) HasVerified() bool`

HasVerified returns a boolean if a field has been set.

### GetVerifiedAt

`func (o *UserAllOfMessengers) GetVerifiedAt() time.Time`

GetVerifiedAt returns the VerifiedAt field if non-nil, zero value otherwise.

### GetVerifiedAtOk

`func (o *UserAllOfMessengers) GetVerifiedAtOk() (*time.Time, bool)`

GetVerifiedAtOk returns a tuple with the VerifiedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVerifiedAt

`func (o *UserAllOfMessengers) SetVerifiedAt(v time.Time)`

SetVerifiedAt sets VerifiedAt field to given value.

### HasVerifiedAt

`func (o *UserAllOfMessengers) HasVerifiedAt() bool`

HasVerifiedAt returns a boolean if a field has been set.

### GetCreatedAt

`func (o *UserAllOfMessengers) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *UserAllOfMessengers) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *UserAllOfMessengers) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.

### HasCreatedAt

`func (o *UserAllOfMessengers) HasCreatedAt() bool`

HasCreatedAt returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *UserAllOfMessengers) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *UserAllOfMessengers) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *UserAllOfMessengers) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *UserAllOfMessengers) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


