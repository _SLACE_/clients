# EstimationResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Total** | **int32** |  | 
**TotalCostCents** | **int32** |  | 

## Methods

### NewEstimationResult

`func NewEstimationResult(total int32, totalCostCents int32, ) *EstimationResult`

NewEstimationResult instantiates a new EstimationResult object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewEstimationResultWithDefaults

`func NewEstimationResultWithDefaults() *EstimationResult`

NewEstimationResultWithDefaults instantiates a new EstimationResult object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotal

`func (o *EstimationResult) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *EstimationResult) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *EstimationResult) SetTotal(v int32)`

SetTotal sets Total field to given value.


### GetTotalCostCents

`func (o *EstimationResult) GetTotalCostCents() int32`

GetTotalCostCents returns the TotalCostCents field if non-nil, zero value otherwise.

### GetTotalCostCentsOk

`func (o *EstimationResult) GetTotalCostCentsOk() (*int32, bool)`

GetTotalCostCentsOk returns a tuple with the TotalCostCents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalCostCents

`func (o *EstimationResult) SetTotalCostCents(v int32)`

SetTotalCostCents sets TotalCostCents field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


