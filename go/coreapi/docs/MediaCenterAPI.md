# \MediaCenterAPI

All URIs are relative to *https://api.slace.com/core*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateMediaCenterFile**](MediaCenterAPI.md#CreateMediaCenterFile) | **Post** /channels/{channel_id}/mediafiles | Create media file
[**CreateMediaCenterFileVariant**](MediaCenterAPI.md#CreateMediaCenterFileVariant) | **Post** /channels/{channel_id}/mediafiles/variants | Create media file variant
[**GetChannelMediaCenterFile**](MediaCenterAPI.md#GetChannelMediaCenterFile) | **Get** /channels/{channel_id}/mediafiles/{id} | Get media center file
[**GetChannelMediaCenterFiles**](MediaCenterAPI.md#GetChannelMediaCenterFiles) | **Get** /channels/{channel_id}/mediafiles | Get media center files
[**GetMediaCenterFileVaariant**](MediaCenterAPI.md#GetMediaCenterFileVaariant) | **Get** /channels/{channel_id}/mediafiles/variants/{variant_id} | Get a Channel Mediafile Variant
[**GetMediaCenterFileVariants**](MediaCenterAPI.md#GetMediaCenterFileVariants) | **Get** /mediafiles/variants | 
[**GetMediaFileVariantByKey**](MediaCenterAPI.md#GetMediaFileVariantByKey) | **Get** /channels/{channel_id}/mediafiles/keys/{file_key}/variants/keys/{variant_key} | Get media center variant by key
[**GetMediaFileVariantDefault**](MediaCenterAPI.md#GetMediaFileVariantDefault) | **Get** /channels/{channel_id}/mediafiles/keys/{file_key}/variants/default | Get media center default variant
[**UpdateMediaCenterFile**](MediaCenterAPI.md#UpdateMediaCenterFile) | **Put** /channels/{channel_id}/mediafiles/{id} | Update media center file
[**UpdateMediaCenterFileVariant**](MediaCenterAPI.md#UpdateMediaCenterFileVariant) | **Post** /channels/{channel_id}/mediafiles/variants/{variant_id} | Update media file variant



## CreateMediaCenterFile

> MediaCenterFile CreateMediaCenterFile(ctx, channelId).MediaCenterFileCreate(mediaCenterFileCreate).Execute()

Create media file

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 
    mediaCenterFileCreate := *openapiclient.NewMediaCenterFileCreate("Key_example") // MediaCenterFileCreate |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaCenterAPI.CreateMediaCenterFile(context.Background(), channelId).MediaCenterFileCreate(mediaCenterFileCreate).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaCenterAPI.CreateMediaCenterFile``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateMediaCenterFile`: MediaCenterFile
    fmt.Fprintf(os.Stdout, "Response from `MediaCenterAPI.CreateMediaCenterFile`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateMediaCenterFileRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **mediaCenterFileCreate** | [**MediaCenterFileCreate**](MediaCenterFileCreate.md) |  | 

### Return type

[**MediaCenterFile**](MediaCenterFile.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateMediaCenterFileVariant

> MediaCenterFileVariant CreateMediaCenterFileVariant(ctx, channelId).MediaCenterFileVariantCreate(mediaCenterFileVariantCreate).Execute()

Create media file variant

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 
    mediaCenterFileVariantCreate := *openapiclient.NewMediaCenterFileVariantCreate() // MediaCenterFileVariantCreate |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaCenterAPI.CreateMediaCenterFileVariant(context.Background(), channelId).MediaCenterFileVariantCreate(mediaCenterFileVariantCreate).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaCenterAPI.CreateMediaCenterFileVariant``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateMediaCenterFileVariant`: MediaCenterFileVariant
    fmt.Fprintf(os.Stdout, "Response from `MediaCenterAPI.CreateMediaCenterFileVariant`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateMediaCenterFileVariantRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **mediaCenterFileVariantCreate** | [**MediaCenterFileVariantCreate**](MediaCenterFileVariantCreate.md) |  | 

### Return type

[**MediaCenterFileVariant**](MediaCenterFileVariant.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChannelMediaCenterFile

> MediaCenterFile GetChannelMediaCenterFile(ctx, channelId, id).Execute()

Get media center file



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaCenterAPI.GetChannelMediaCenterFile(context.Background(), channelId, id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaCenterAPI.GetChannelMediaCenterFile``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChannelMediaCenterFile`: MediaCenterFile
    fmt.Fprintf(os.Stdout, "Response from `MediaCenterAPI.GetChannelMediaCenterFile`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChannelMediaCenterFileRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**MediaCenterFile**](MediaCenterFile.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChannelMediaCenterFiles

> GetChannelMediaCenterFiles200Response GetChannelMediaCenterFiles(ctx, channelId).Key(key).Search(search).Execute()

Get media center files



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 
    key := "key_example" // string |  (optional)
    search := "search_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaCenterAPI.GetChannelMediaCenterFiles(context.Background(), channelId).Key(key).Search(search).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaCenterAPI.GetChannelMediaCenterFiles``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChannelMediaCenterFiles`: GetChannelMediaCenterFiles200Response
    fmt.Fprintf(os.Stdout, "Response from `MediaCenterAPI.GetChannelMediaCenterFiles`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChannelMediaCenterFilesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **key** | **string** |  | 
 **search** | **string** |  | 

### Return type

[**GetChannelMediaCenterFiles200Response**](GetChannelMediaCenterFiles200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMediaCenterFileVaariant

> MediaCenterFileVariant GetMediaCenterFileVaariant(ctx, channelId, variantId).Execute()

Get a Channel Mediafile Variant

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 
    variantId := "variantId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaCenterAPI.GetMediaCenterFileVaariant(context.Background(), channelId, variantId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaCenterAPI.GetMediaCenterFileVaariant``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMediaCenterFileVaariant`: MediaCenterFileVariant
    fmt.Fprintf(os.Stdout, "Response from `MediaCenterAPI.GetMediaCenterFileVaariant`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**variantId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetMediaCenterFileVaariantRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**MediaCenterFileVariant**](MediaCenterFileVariant.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMediaCenterFileVariants

> GetMediaCenterFileVariants200Response GetMediaCenterFileVariants(ctx).Type_(type_).Search(search).OrganizationId(organizationId).ChannelId(channelId).Default_(default_).External(external).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    type_ := "type__example" // string |  (optional)
    search := "search_example" // string |  (optional)
    organizationId := "organizationId_example" // string |  (optional)
    channelId := "channelId_example" // string |  (optional)
    default_ := "default__example" // string |  (optional)
    external := "external_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaCenterAPI.GetMediaCenterFileVariants(context.Background()).Type_(type_).Search(search).OrganizationId(organizationId).ChannelId(channelId).Default_(default_).External(external).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaCenterAPI.GetMediaCenterFileVariants``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMediaCenterFileVariants`: GetMediaCenterFileVariants200Response
    fmt.Fprintf(os.Stdout, "Response from `MediaCenterAPI.GetMediaCenterFileVariants`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetMediaCenterFileVariantsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type_** | **string** |  | 
 **search** | **string** |  | 
 **organizationId** | **string** |  | 
 **channelId** | **string** |  | 
 **default_** | **string** |  | 
 **external** | **string** |  | 

### Return type

[**GetMediaCenterFileVariants200Response**](GetMediaCenterFileVariants200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMediaFileVariantByKey

> MediaCenterFileVariant GetMediaFileVariantByKey(ctx, channelId, fileKey, variantKey).Execute()

Get media center variant by key



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 
    fileKey := "fileKey_example" // string | 
    variantKey := "variantKey_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaCenterAPI.GetMediaFileVariantByKey(context.Background(), channelId, fileKey, variantKey).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaCenterAPI.GetMediaFileVariantByKey``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMediaFileVariantByKey`: MediaCenterFileVariant
    fmt.Fprintf(os.Stdout, "Response from `MediaCenterAPI.GetMediaFileVariantByKey`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**fileKey** | **string** |  | 
**variantKey** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetMediaFileVariantByKeyRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**MediaCenterFileVariant**](MediaCenterFileVariant.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMediaFileVariantDefault

> MediaCenterFileVariant GetMediaFileVariantDefault(ctx, channelId, fileKey).Execute()

Get media center default variant



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 
    fileKey := "fileKey_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaCenterAPI.GetMediaFileVariantDefault(context.Background(), channelId, fileKey).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaCenterAPI.GetMediaFileVariantDefault``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMediaFileVariantDefault`: MediaCenterFileVariant
    fmt.Fprintf(os.Stdout, "Response from `MediaCenterAPI.GetMediaFileVariantDefault`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**fileKey** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetMediaFileVariantDefaultRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**MediaCenterFileVariant**](MediaCenterFileVariant.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateMediaCenterFile

> MediaCenterFile UpdateMediaCenterFile(ctx, channelId, id).MediaCenterFileUpdate(mediaCenterFileUpdate).Execute()

Update media center file

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 
    mediaCenterFileUpdate := *openapiclient.NewMediaCenterFileUpdate() // MediaCenterFileUpdate |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaCenterAPI.UpdateMediaCenterFile(context.Background(), channelId, id).MediaCenterFileUpdate(mediaCenterFileUpdate).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaCenterAPI.UpdateMediaCenterFile``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateMediaCenterFile`: MediaCenterFile
    fmt.Fprintf(os.Stdout, "Response from `MediaCenterAPI.UpdateMediaCenterFile`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateMediaCenterFileRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **mediaCenterFileUpdate** | [**MediaCenterFileUpdate**](MediaCenterFileUpdate.md) |  | 

### Return type

[**MediaCenterFile**](MediaCenterFile.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateMediaCenterFileVariant

> MediaCenterFileVariant UpdateMediaCenterFileVariant(ctx, channelId, variantId).UpdateMediaCenterFileVariantRequest(updateMediaCenterFileVariantRequest).Execute()

Update media file variant



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/coreapi"
)

func main() {
    channelId := "channelId_example" // string | 
    variantId := "variantId_example" // string | 
    updateMediaCenterFileVariantRequest := *openapiclient.NewUpdateMediaCenterFileVariantRequest() // UpdateMediaCenterFileVariantRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaCenterAPI.UpdateMediaCenterFileVariant(context.Background(), channelId, variantId).UpdateMediaCenterFileVariantRequest(updateMediaCenterFileVariantRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaCenterAPI.UpdateMediaCenterFileVariant``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateMediaCenterFileVariant`: MediaCenterFileVariant
    fmt.Fprintf(os.Stdout, "Response from `MediaCenterAPI.UpdateMediaCenterFileVariant`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**variantId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateMediaCenterFileVariantRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **updateMediaCenterFileVariantRequest** | [**UpdateMediaCenterFileVariantRequest**](UpdateMediaCenterFileVariantRequest.md) |  | 

### Return type

[**MediaCenterFileVariant**](MediaCenterFileVariant.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json, multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

