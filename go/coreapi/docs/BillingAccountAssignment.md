# BillingAccountAssignment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountId** | **string** |  | 
**Account** | Pointer to [**BillingAccount**](BillingAccount.md) |  | [optional] 
**OrganizationId** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**Current** | Pointer to **bool** | true if entry is currently is in use with selected list | [optional] 
**InteractionId** | Pointer to **string** |  | [optional] 
**StartingAt** | Pointer to **time.Time** | If not specified, now will be used | [optional] 

## Methods

### NewBillingAccountAssignment

`func NewBillingAccountAssignment(accountId string, ) *BillingAccountAssignment`

NewBillingAccountAssignment instantiates a new BillingAccountAssignment object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBillingAccountAssignmentWithDefaults

`func NewBillingAccountAssignmentWithDefaults() *BillingAccountAssignment`

NewBillingAccountAssignmentWithDefaults instantiates a new BillingAccountAssignment object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAccountId

`func (o *BillingAccountAssignment) GetAccountId() string`

GetAccountId returns the AccountId field if non-nil, zero value otherwise.

### GetAccountIdOk

`func (o *BillingAccountAssignment) GetAccountIdOk() (*string, bool)`

GetAccountIdOk returns a tuple with the AccountId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAccountId

`func (o *BillingAccountAssignment) SetAccountId(v string)`

SetAccountId sets AccountId field to given value.


### GetAccount

`func (o *BillingAccountAssignment) GetAccount() BillingAccount`

GetAccount returns the Account field if non-nil, zero value otherwise.

### GetAccountOk

`func (o *BillingAccountAssignment) GetAccountOk() (*BillingAccount, bool)`

GetAccountOk returns a tuple with the Account field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAccount

`func (o *BillingAccountAssignment) SetAccount(v BillingAccount)`

SetAccount sets Account field to given value.

### HasAccount

`func (o *BillingAccountAssignment) HasAccount() bool`

HasAccount returns a boolean if a field has been set.

### GetOrganizationId

`func (o *BillingAccountAssignment) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *BillingAccountAssignment) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *BillingAccountAssignment) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *BillingAccountAssignment) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetChannelId

`func (o *BillingAccountAssignment) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *BillingAccountAssignment) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *BillingAccountAssignment) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *BillingAccountAssignment) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetCurrent

`func (o *BillingAccountAssignment) GetCurrent() bool`

GetCurrent returns the Current field if non-nil, zero value otherwise.

### GetCurrentOk

`func (o *BillingAccountAssignment) GetCurrentOk() (*bool, bool)`

GetCurrentOk returns a tuple with the Current field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrent

`func (o *BillingAccountAssignment) SetCurrent(v bool)`

SetCurrent sets Current field to given value.

### HasCurrent

`func (o *BillingAccountAssignment) HasCurrent() bool`

HasCurrent returns a boolean if a field has been set.

### GetInteractionId

`func (o *BillingAccountAssignment) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *BillingAccountAssignment) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *BillingAccountAssignment) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *BillingAccountAssignment) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetStartingAt

`func (o *BillingAccountAssignment) GetStartingAt() time.Time`

GetStartingAt returns the StartingAt field if non-nil, zero value otherwise.

### GetStartingAtOk

`func (o *BillingAccountAssignment) GetStartingAtOk() (*time.Time, bool)`

GetStartingAtOk returns a tuple with the StartingAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartingAt

`func (o *BillingAccountAssignment) SetStartingAt(v time.Time)`

SetStartingAt sets StartingAt field to given value.

### HasStartingAt

`func (o *BillingAccountAssignment) HasStartingAt() bool`

HasStartingAt returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


