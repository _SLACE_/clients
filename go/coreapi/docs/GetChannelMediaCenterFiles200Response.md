# GetChannelMediaCenterFiles200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | Pointer to [**[]MediaCenterFile**](MediaCenterFile.md) |  | [optional] 
**Total** | Pointer to **int32** |  | [optional] 

## Methods

### NewGetChannelMediaCenterFiles200Response

`func NewGetChannelMediaCenterFiles200Response() *GetChannelMediaCenterFiles200Response`

NewGetChannelMediaCenterFiles200Response instantiates a new GetChannelMediaCenterFiles200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetChannelMediaCenterFiles200ResponseWithDefaults

`func NewGetChannelMediaCenterFiles200ResponseWithDefaults() *GetChannelMediaCenterFiles200Response`

NewGetChannelMediaCenterFiles200ResponseWithDefaults instantiates a new GetChannelMediaCenterFiles200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *GetChannelMediaCenterFiles200Response) GetResults() []MediaCenterFile`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *GetChannelMediaCenterFiles200Response) GetResultsOk() (*[]MediaCenterFile, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *GetChannelMediaCenterFiles200Response) SetResults(v []MediaCenterFile)`

SetResults sets Results field to given value.

### HasResults

`func (o *GetChannelMediaCenterFiles200Response) HasResults() bool`

HasResults returns a boolean if a field has been set.

### GetTotal

`func (o *GetChannelMediaCenterFiles200Response) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *GetChannelMediaCenterFiles200Response) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *GetChannelMediaCenterFiles200Response) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *GetChannelMediaCenterFiles200Response) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


