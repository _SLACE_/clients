# OrganizationMutableLanguageSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Default** | Pointer to **string** |  | [optional] 
**Supported** | Pointer to **[]string** |  | [optional] 
**Fallback** | Pointer to **string** |  | [optional] 

## Methods

### NewOrganizationMutableLanguageSettings

`func NewOrganizationMutableLanguageSettings() *OrganizationMutableLanguageSettings`

NewOrganizationMutableLanguageSettings instantiates a new OrganizationMutableLanguageSettings object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrganizationMutableLanguageSettingsWithDefaults

`func NewOrganizationMutableLanguageSettingsWithDefaults() *OrganizationMutableLanguageSettings`

NewOrganizationMutableLanguageSettingsWithDefaults instantiates a new OrganizationMutableLanguageSettings object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDefault

`func (o *OrganizationMutableLanguageSettings) GetDefault() string`

GetDefault returns the Default field if non-nil, zero value otherwise.

### GetDefaultOk

`func (o *OrganizationMutableLanguageSettings) GetDefaultOk() (*string, bool)`

GetDefaultOk returns a tuple with the Default field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefault

`func (o *OrganizationMutableLanguageSettings) SetDefault(v string)`

SetDefault sets Default field to given value.

### HasDefault

`func (o *OrganizationMutableLanguageSettings) HasDefault() bool`

HasDefault returns a boolean if a field has been set.

### GetSupported

`func (o *OrganizationMutableLanguageSettings) GetSupported() []string`

GetSupported returns the Supported field if non-nil, zero value otherwise.

### GetSupportedOk

`func (o *OrganizationMutableLanguageSettings) GetSupportedOk() (*[]string, bool)`

GetSupportedOk returns a tuple with the Supported field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSupported

`func (o *OrganizationMutableLanguageSettings) SetSupported(v []string)`

SetSupported sets Supported field to given value.

### HasSupported

`func (o *OrganizationMutableLanguageSettings) HasSupported() bool`

HasSupported returns a boolean if a field has been set.

### GetFallback

`func (o *OrganizationMutableLanguageSettings) GetFallback() string`

GetFallback returns the Fallback field if non-nil, zero value otherwise.

### GetFallbackOk

`func (o *OrganizationMutableLanguageSettings) GetFallbackOk() (*string, bool)`

GetFallbackOk returns a tuple with the Fallback field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFallback

`func (o *OrganizationMutableLanguageSettings) SetFallback(v string)`

SetFallback sets Fallback field to given value.

### HasFallback

`func (o *OrganizationMutableLanguageSettings) HasFallback() bool`

HasFallback returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


