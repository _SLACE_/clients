# BillingAccountAssignmentList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]BillingAccountAssignment**](BillingAccountAssignment.md) |  | 
**Total** | **int32** |  | 

## Methods

### NewBillingAccountAssignmentList

`func NewBillingAccountAssignmentList(results []BillingAccountAssignment, total int32, ) *BillingAccountAssignmentList`

NewBillingAccountAssignmentList instantiates a new BillingAccountAssignmentList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBillingAccountAssignmentListWithDefaults

`func NewBillingAccountAssignmentListWithDefaults() *BillingAccountAssignmentList`

NewBillingAccountAssignmentListWithDefaults instantiates a new BillingAccountAssignmentList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *BillingAccountAssignmentList) GetResults() []BillingAccountAssignment`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *BillingAccountAssignmentList) GetResultsOk() (*[]BillingAccountAssignment, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *BillingAccountAssignmentList) SetResults(v []BillingAccountAssignment)`

SetResults sets Results field to given value.


### GetTotal

`func (o *BillingAccountAssignmentList) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *BillingAccountAssignmentList) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *BillingAccountAssignmentList) SetTotal(v int32)`

SetTotal sets Total field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


