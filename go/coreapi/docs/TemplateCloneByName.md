# TemplateCloneByName

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**GatewayId** | Pointer to **string** |  | [optional] 

## Methods

### NewTemplateCloneByName

`func NewTemplateCloneByName() *TemplateCloneByName`

NewTemplateCloneByName instantiates a new TemplateCloneByName object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTemplateCloneByNameWithDefaults

`func NewTemplateCloneByNameWithDefaults() *TemplateCloneByName`

NewTemplateCloneByNameWithDefaults instantiates a new TemplateCloneByName object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *TemplateCloneByName) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *TemplateCloneByName) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *TemplateCloneByName) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *TemplateCloneByName) HasName() bool`

HasName returns a boolean if a field has been set.

### GetGatewayId

`func (o *TemplateCloneByName) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *TemplateCloneByName) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *TemplateCloneByName) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.

### HasGatewayId

`func (o *TemplateCloneByName) HasGatewayId() bool`

HasGatewayId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


