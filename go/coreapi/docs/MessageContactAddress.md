# MessageContactAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Street** | Pointer to **string** |  | [optional] 
**City** | Pointer to **string** |  | [optional] 
**State** | Pointer to **string** |  | [optional] 
**Zip** | Pointer to **string** |  | [optional] 
**Country** | Pointer to **string** |  | [optional] 
**CountryCode** | Pointer to **string** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageContactAddress

`func NewMessageContactAddress() *MessageContactAddress`

NewMessageContactAddress instantiates a new MessageContactAddress object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageContactAddressWithDefaults

`func NewMessageContactAddressWithDefaults() *MessageContactAddress`

NewMessageContactAddressWithDefaults instantiates a new MessageContactAddress object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStreet

`func (o *MessageContactAddress) GetStreet() string`

GetStreet returns the Street field if non-nil, zero value otherwise.

### GetStreetOk

`func (o *MessageContactAddress) GetStreetOk() (*string, bool)`

GetStreetOk returns a tuple with the Street field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreet

`func (o *MessageContactAddress) SetStreet(v string)`

SetStreet sets Street field to given value.

### HasStreet

`func (o *MessageContactAddress) HasStreet() bool`

HasStreet returns a boolean if a field has been set.

### GetCity

`func (o *MessageContactAddress) GetCity() string`

GetCity returns the City field if non-nil, zero value otherwise.

### GetCityOk

`func (o *MessageContactAddress) GetCityOk() (*string, bool)`

GetCityOk returns a tuple with the City field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCity

`func (o *MessageContactAddress) SetCity(v string)`

SetCity sets City field to given value.

### HasCity

`func (o *MessageContactAddress) HasCity() bool`

HasCity returns a boolean if a field has been set.

### GetState

`func (o *MessageContactAddress) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *MessageContactAddress) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *MessageContactAddress) SetState(v string)`

SetState sets State field to given value.

### HasState

`func (o *MessageContactAddress) HasState() bool`

HasState returns a boolean if a field has been set.

### GetZip

`func (o *MessageContactAddress) GetZip() string`

GetZip returns the Zip field if non-nil, zero value otherwise.

### GetZipOk

`func (o *MessageContactAddress) GetZipOk() (*string, bool)`

GetZipOk returns a tuple with the Zip field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetZip

`func (o *MessageContactAddress) SetZip(v string)`

SetZip sets Zip field to given value.

### HasZip

`func (o *MessageContactAddress) HasZip() bool`

HasZip returns a boolean if a field has been set.

### GetCountry

`func (o *MessageContactAddress) GetCountry() string`

GetCountry returns the Country field if non-nil, zero value otherwise.

### GetCountryOk

`func (o *MessageContactAddress) GetCountryOk() (*string, bool)`

GetCountryOk returns a tuple with the Country field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountry

`func (o *MessageContactAddress) SetCountry(v string)`

SetCountry sets Country field to given value.

### HasCountry

`func (o *MessageContactAddress) HasCountry() bool`

HasCountry returns a boolean if a field has been set.

### GetCountryCode

`func (o *MessageContactAddress) GetCountryCode() string`

GetCountryCode returns the CountryCode field if non-nil, zero value otherwise.

### GetCountryCodeOk

`func (o *MessageContactAddress) GetCountryCodeOk() (*string, bool)`

GetCountryCodeOk returns a tuple with the CountryCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountryCode

`func (o *MessageContactAddress) SetCountryCode(v string)`

SetCountryCode sets CountryCode field to given value.

### HasCountryCode

`func (o *MessageContactAddress) HasCountryCode() bool`

HasCountryCode returns a boolean if a field has been set.

### GetType

`func (o *MessageContactAddress) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MessageContactAddress) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MessageContactAddress) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *MessageContactAddress) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


