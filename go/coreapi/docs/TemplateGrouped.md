# TemplateGrouped

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OrganizationId** | **string** |  | 
**OrganizationName** | **string** |  | 
**GatewayId** | **string** |  | 
**GatewayName** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**ChannelName** | Pointer to **string** |  | [optional] 
**Name** | **string** |  | 
**Category** | Pointer to **string** |  | [optional] 
**Type** | **string** |  | 
**Templates** | [**[]TemplateGroupedTemplatesInner**](TemplateGroupedTemplatesInner.md) |  | 
**CreatedAt** | **time.Time** | MIN(created_at) from templates group | 
**UpdatedAt** | **time.Time** | MAX(updated_at) from templates group | 

## Methods

### NewTemplateGrouped

`func NewTemplateGrouped(organizationId string, organizationName string, gatewayId string, name string, type_ string, templates []TemplateGroupedTemplatesInner, createdAt time.Time, updatedAt time.Time, ) *TemplateGrouped`

NewTemplateGrouped instantiates a new TemplateGrouped object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTemplateGroupedWithDefaults

`func NewTemplateGroupedWithDefaults() *TemplateGrouped`

NewTemplateGroupedWithDefaults instantiates a new TemplateGrouped object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOrganizationId

`func (o *TemplateGrouped) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *TemplateGrouped) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *TemplateGrouped) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetOrganizationName

`func (o *TemplateGrouped) GetOrganizationName() string`

GetOrganizationName returns the OrganizationName field if non-nil, zero value otherwise.

### GetOrganizationNameOk

`func (o *TemplateGrouped) GetOrganizationNameOk() (*string, bool)`

GetOrganizationNameOk returns a tuple with the OrganizationName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationName

`func (o *TemplateGrouped) SetOrganizationName(v string)`

SetOrganizationName sets OrganizationName field to given value.


### GetGatewayId

`func (o *TemplateGrouped) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *TemplateGrouped) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *TemplateGrouped) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.


### GetGatewayName

`func (o *TemplateGrouped) GetGatewayName() string`

GetGatewayName returns the GatewayName field if non-nil, zero value otherwise.

### GetGatewayNameOk

`func (o *TemplateGrouped) GetGatewayNameOk() (*string, bool)`

GetGatewayNameOk returns a tuple with the GatewayName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayName

`func (o *TemplateGrouped) SetGatewayName(v string)`

SetGatewayName sets GatewayName field to given value.

### HasGatewayName

`func (o *TemplateGrouped) HasGatewayName() bool`

HasGatewayName returns a boolean if a field has been set.

### GetChannelId

`func (o *TemplateGrouped) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *TemplateGrouped) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *TemplateGrouped) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *TemplateGrouped) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetChannelName

`func (o *TemplateGrouped) GetChannelName() string`

GetChannelName returns the ChannelName field if non-nil, zero value otherwise.

### GetChannelNameOk

`func (o *TemplateGrouped) GetChannelNameOk() (*string, bool)`

GetChannelNameOk returns a tuple with the ChannelName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelName

`func (o *TemplateGrouped) SetChannelName(v string)`

SetChannelName sets ChannelName field to given value.

### HasChannelName

`func (o *TemplateGrouped) HasChannelName() bool`

HasChannelName returns a boolean if a field has been set.

### GetName

`func (o *TemplateGrouped) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *TemplateGrouped) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *TemplateGrouped) SetName(v string)`

SetName sets Name field to given value.


### GetCategory

`func (o *TemplateGrouped) GetCategory() string`

GetCategory returns the Category field if non-nil, zero value otherwise.

### GetCategoryOk

`func (o *TemplateGrouped) GetCategoryOk() (*string, bool)`

GetCategoryOk returns a tuple with the Category field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCategory

`func (o *TemplateGrouped) SetCategory(v string)`

SetCategory sets Category field to given value.

### HasCategory

`func (o *TemplateGrouped) HasCategory() bool`

HasCategory returns a boolean if a field has been set.

### GetType

`func (o *TemplateGrouped) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *TemplateGrouped) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *TemplateGrouped) SetType(v string)`

SetType sets Type field to given value.


### GetTemplates

`func (o *TemplateGrouped) GetTemplates() []TemplateGroupedTemplatesInner`

GetTemplates returns the Templates field if non-nil, zero value otherwise.

### GetTemplatesOk

`func (o *TemplateGrouped) GetTemplatesOk() (*[]TemplateGroupedTemplatesInner, bool)`

GetTemplatesOk returns a tuple with the Templates field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplates

`func (o *TemplateGrouped) SetTemplates(v []TemplateGroupedTemplatesInner)`

SetTemplates sets Templates field to given value.


### GetCreatedAt

`func (o *TemplateGrouped) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *TemplateGrouped) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *TemplateGrouped) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *TemplateGrouped) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *TemplateGrouped) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *TemplateGrouped) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


