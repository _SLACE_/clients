# CampaignsSettings200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DataSources** | **map[string]interface{}** |  | 
**DataTypeOperators** | **map[string]interface{}** |  | 

## Methods

### NewCampaignsSettings200Response

`func NewCampaignsSettings200Response(dataSources map[string]interface{}, dataTypeOperators map[string]interface{}, ) *CampaignsSettings200Response`

NewCampaignsSettings200Response instantiates a new CampaignsSettings200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignsSettings200ResponseWithDefaults

`func NewCampaignsSettings200ResponseWithDefaults() *CampaignsSettings200Response`

NewCampaignsSettings200ResponseWithDefaults instantiates a new CampaignsSettings200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDataSources

`func (o *CampaignsSettings200Response) GetDataSources() map[string]interface{}`

GetDataSources returns the DataSources field if non-nil, zero value otherwise.

### GetDataSourcesOk

`func (o *CampaignsSettings200Response) GetDataSourcesOk() (*map[string]interface{}, bool)`

GetDataSourcesOk returns a tuple with the DataSources field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataSources

`func (o *CampaignsSettings200Response) SetDataSources(v map[string]interface{})`

SetDataSources sets DataSources field to given value.


### GetDataTypeOperators

`func (o *CampaignsSettings200Response) GetDataTypeOperators() map[string]interface{}`

GetDataTypeOperators returns the DataTypeOperators field if non-nil, zero value otherwise.

### GetDataTypeOperatorsOk

`func (o *CampaignsSettings200Response) GetDataTypeOperatorsOk() (*map[string]interface{}, bool)`

GetDataTypeOperatorsOk returns a tuple with the DataTypeOperators field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataTypeOperators

`func (o *CampaignsSettings200Response) SetDataTypeOperators(v map[string]interface{})`

SetDataTypeOperators sets DataTypeOperators field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


