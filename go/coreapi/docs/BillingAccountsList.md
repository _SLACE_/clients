# BillingAccountsList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]BillingAccount**](BillingAccount.md) |  | 
**Total** | **int32** |  | 

## Methods

### NewBillingAccountsList

`func NewBillingAccountsList(results []BillingAccount, total int32, ) *BillingAccountsList`

NewBillingAccountsList instantiates a new BillingAccountsList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBillingAccountsListWithDefaults

`func NewBillingAccountsListWithDefaults() *BillingAccountsList`

NewBillingAccountsListWithDefaults instantiates a new BillingAccountsList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *BillingAccountsList) GetResults() []BillingAccount`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *BillingAccountsList) GetResultsOk() (*[]BillingAccount, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *BillingAccountsList) SetResults(v []BillingAccount)`

SetResults sets Results field to given value.


### GetTotal

`func (o *BillingAccountsList) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *BillingAccountsList) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *BillingAccountsList) SetTotal(v int32)`

SetTotal sets Total field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


