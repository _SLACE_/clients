# ContactAllOfGivenConsents

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LastGivenAt** | Pointer to **time.Time** |  | [optional] [readonly] 
**Version** | Pointer to **int32** |  | [optional] [readonly] 

## Methods

### NewContactAllOfGivenConsents

`func NewContactAllOfGivenConsents() *ContactAllOfGivenConsents`

NewContactAllOfGivenConsents instantiates a new ContactAllOfGivenConsents object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactAllOfGivenConsentsWithDefaults

`func NewContactAllOfGivenConsentsWithDefaults() *ContactAllOfGivenConsents`

NewContactAllOfGivenConsentsWithDefaults instantiates a new ContactAllOfGivenConsents object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLastGivenAt

`func (o *ContactAllOfGivenConsents) GetLastGivenAt() time.Time`

GetLastGivenAt returns the LastGivenAt field if non-nil, zero value otherwise.

### GetLastGivenAtOk

`func (o *ContactAllOfGivenConsents) GetLastGivenAtOk() (*time.Time, bool)`

GetLastGivenAtOk returns a tuple with the LastGivenAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastGivenAt

`func (o *ContactAllOfGivenConsents) SetLastGivenAt(v time.Time)`

SetLastGivenAt sets LastGivenAt field to given value.

### HasLastGivenAt

`func (o *ContactAllOfGivenConsents) HasLastGivenAt() bool`

HasLastGivenAt returns a boolean if a field has been set.

### GetVersion

`func (o *ContactAllOfGivenConsents) GetVersion() int32`

GetVersion returns the Version field if non-nil, zero value otherwise.

### GetVersionOk

`func (o *ContactAllOfGivenConsents) GetVersionOk() (*int32, bool)`

GetVersionOk returns a tuple with the Version field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersion

`func (o *ContactAllOfGivenConsents) SetVersion(v int32)`

SetVersion sets Version field to given value.

### HasVersion

`func (o *ContactAllOfGivenConsents) HasVersion() bool`

HasVersion returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


