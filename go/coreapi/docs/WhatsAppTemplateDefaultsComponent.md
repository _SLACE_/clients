# WhatsAppTemplateDefaultsComponent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | [**WhatsAppTemplateComponentType**](WhatsAppTemplateComponentType.md) |  | 
**FileId** | Pointer to **string** |  | [optional] 
**FileName** | Pointer to **string** |  | [optional] 
**TextParams** | Pointer to **[]string** |  | [optional] 
**Buttons** | Pointer to [**[]WhatsAppTemplateDefaultsButton**](WhatsAppTemplateDefaultsButton.md) | Both &#x60;ttl&#x60; &amp; &#x60;expired_url&#x60; are usefull only for short links generated when sending template, which means only for URL buttons with &#x60;https://slace.me&#x60; or &#x60;https://dev.slace.me&#x60; prefix  | [optional] 
**Cards** | Pointer to [**[]WhatsAppTemplateDefaultsCard**](WhatsAppTemplateDefaultsCard.md) |  | [optional] 

## Methods

### NewWhatsAppTemplateDefaultsComponent

`func NewWhatsAppTemplateDefaultsComponent(type_ WhatsAppTemplateComponentType, ) *WhatsAppTemplateDefaultsComponent`

NewWhatsAppTemplateDefaultsComponent instantiates a new WhatsAppTemplateDefaultsComponent object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateDefaultsComponentWithDefaults

`func NewWhatsAppTemplateDefaultsComponentWithDefaults() *WhatsAppTemplateDefaultsComponent`

NewWhatsAppTemplateDefaultsComponentWithDefaults instantiates a new WhatsAppTemplateDefaultsComponent object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *WhatsAppTemplateDefaultsComponent) GetType() WhatsAppTemplateComponentType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WhatsAppTemplateDefaultsComponent) GetTypeOk() (*WhatsAppTemplateComponentType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WhatsAppTemplateDefaultsComponent) SetType(v WhatsAppTemplateComponentType)`

SetType sets Type field to given value.


### GetFileId

`func (o *WhatsAppTemplateDefaultsComponent) GetFileId() string`

GetFileId returns the FileId field if non-nil, zero value otherwise.

### GetFileIdOk

`func (o *WhatsAppTemplateDefaultsComponent) GetFileIdOk() (*string, bool)`

GetFileIdOk returns a tuple with the FileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFileId

`func (o *WhatsAppTemplateDefaultsComponent) SetFileId(v string)`

SetFileId sets FileId field to given value.

### HasFileId

`func (o *WhatsAppTemplateDefaultsComponent) HasFileId() bool`

HasFileId returns a boolean if a field has been set.

### GetFileName

`func (o *WhatsAppTemplateDefaultsComponent) GetFileName() string`

GetFileName returns the FileName field if non-nil, zero value otherwise.

### GetFileNameOk

`func (o *WhatsAppTemplateDefaultsComponent) GetFileNameOk() (*string, bool)`

GetFileNameOk returns a tuple with the FileName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFileName

`func (o *WhatsAppTemplateDefaultsComponent) SetFileName(v string)`

SetFileName sets FileName field to given value.

### HasFileName

`func (o *WhatsAppTemplateDefaultsComponent) HasFileName() bool`

HasFileName returns a boolean if a field has been set.

### GetTextParams

`func (o *WhatsAppTemplateDefaultsComponent) GetTextParams() []string`

GetTextParams returns the TextParams field if non-nil, zero value otherwise.

### GetTextParamsOk

`func (o *WhatsAppTemplateDefaultsComponent) GetTextParamsOk() (*[]string, bool)`

GetTextParamsOk returns a tuple with the TextParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTextParams

`func (o *WhatsAppTemplateDefaultsComponent) SetTextParams(v []string)`

SetTextParams sets TextParams field to given value.

### HasTextParams

`func (o *WhatsAppTemplateDefaultsComponent) HasTextParams() bool`

HasTextParams returns a boolean if a field has been set.

### GetButtons

`func (o *WhatsAppTemplateDefaultsComponent) GetButtons() []WhatsAppTemplateDefaultsButton`

GetButtons returns the Buttons field if non-nil, zero value otherwise.

### GetButtonsOk

`func (o *WhatsAppTemplateDefaultsComponent) GetButtonsOk() (*[]WhatsAppTemplateDefaultsButton, bool)`

GetButtonsOk returns a tuple with the Buttons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtons

`func (o *WhatsAppTemplateDefaultsComponent) SetButtons(v []WhatsAppTemplateDefaultsButton)`

SetButtons sets Buttons field to given value.

### HasButtons

`func (o *WhatsAppTemplateDefaultsComponent) HasButtons() bool`

HasButtons returns a boolean if a field has been set.

### GetCards

`func (o *WhatsAppTemplateDefaultsComponent) GetCards() []WhatsAppTemplateDefaultsCard`

GetCards returns the Cards field if non-nil, zero value otherwise.

### GetCardsOk

`func (o *WhatsAppTemplateDefaultsComponent) GetCardsOk() (*[]WhatsAppTemplateDefaultsCard, bool)`

GetCardsOk returns a tuple with the Cards field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCards

`func (o *WhatsAppTemplateDefaultsComponent) SetCards(v []WhatsAppTemplateDefaultsCard)`

SetCards sets Cards field to given value.

### HasCards

`func (o *WhatsAppTemplateDefaultsComponent) HasCards() bool`

HasCards returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


