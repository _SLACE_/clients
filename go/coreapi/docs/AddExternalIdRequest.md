# AddExternalIdRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SystemName** | Pointer to **string** |  | [optional] 
**UserLink** | Pointer to **string** |  | [optional] 

## Methods

### NewAddExternalIdRequest

`func NewAddExternalIdRequest() *AddExternalIdRequest`

NewAddExternalIdRequest instantiates a new AddExternalIdRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAddExternalIdRequestWithDefaults

`func NewAddExternalIdRequestWithDefaults() *AddExternalIdRequest`

NewAddExternalIdRequestWithDefaults instantiates a new AddExternalIdRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSystemName

`func (o *AddExternalIdRequest) GetSystemName() string`

GetSystemName returns the SystemName field if non-nil, zero value otherwise.

### GetSystemNameOk

`func (o *AddExternalIdRequest) GetSystemNameOk() (*string, bool)`

GetSystemNameOk returns a tuple with the SystemName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSystemName

`func (o *AddExternalIdRequest) SetSystemName(v string)`

SetSystemName sets SystemName field to given value.

### HasSystemName

`func (o *AddExternalIdRequest) HasSystemName() bool`

HasSystemName returns a boolean if a field has been set.

### GetUserLink

`func (o *AddExternalIdRequest) GetUserLink() string`

GetUserLink returns the UserLink field if non-nil, zero value otherwise.

### GetUserLinkOk

`func (o *AddExternalIdRequest) GetUserLinkOk() (*string, bool)`

GetUserLinkOk returns a tuple with the UserLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserLink

`func (o *AddExternalIdRequest) SetUserLink(v string)`

SetUserLink sets UserLink field to given value.

### HasUserLink

`func (o *AddExternalIdRequest) HasUserLink() bool`

HasUserLink returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


