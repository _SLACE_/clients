# MediaCenterFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**ChannelId** | **string** |  | 
**OrganizationId** | **string** |  | 
**Key** | **string** |  | 
**DefaultVariantKey** | Pointer to **string** |  | [optional] 
**MediaFileVariants** | Pointer to [**[]MediaCenterFileVariant**](MediaCenterFileVariant.md) |  | [optional] 
**UpdatedAt** | **time.Time** |  | 
**CreatedAt** | **time.Time** |  | 
**OrganizationWide** | **bool** | File and all its variants is visible for whole organization | 
**Public** | **bool** |  | 

## Methods

### NewMediaCenterFile

`func NewMediaCenterFile(id string, channelId string, organizationId string, key string, updatedAt time.Time, createdAt time.Time, organizationWide bool, public bool, ) *MediaCenterFile`

NewMediaCenterFile instantiates a new MediaCenterFile object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMediaCenterFileWithDefaults

`func NewMediaCenterFileWithDefaults() *MediaCenterFile`

NewMediaCenterFileWithDefaults instantiates a new MediaCenterFile object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *MediaCenterFile) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *MediaCenterFile) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *MediaCenterFile) SetId(v string)`

SetId sets Id field to given value.


### GetChannelId

`func (o *MediaCenterFile) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *MediaCenterFile) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *MediaCenterFile) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetOrganizationId

`func (o *MediaCenterFile) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *MediaCenterFile) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *MediaCenterFile) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetKey

`func (o *MediaCenterFile) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *MediaCenterFile) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *MediaCenterFile) SetKey(v string)`

SetKey sets Key field to given value.


### GetDefaultVariantKey

`func (o *MediaCenterFile) GetDefaultVariantKey() string`

GetDefaultVariantKey returns the DefaultVariantKey field if non-nil, zero value otherwise.

### GetDefaultVariantKeyOk

`func (o *MediaCenterFile) GetDefaultVariantKeyOk() (*string, bool)`

GetDefaultVariantKeyOk returns a tuple with the DefaultVariantKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultVariantKey

`func (o *MediaCenterFile) SetDefaultVariantKey(v string)`

SetDefaultVariantKey sets DefaultVariantKey field to given value.

### HasDefaultVariantKey

`func (o *MediaCenterFile) HasDefaultVariantKey() bool`

HasDefaultVariantKey returns a boolean if a field has been set.

### GetMediaFileVariants

`func (o *MediaCenterFile) GetMediaFileVariants() []MediaCenterFileVariant`

GetMediaFileVariants returns the MediaFileVariants field if non-nil, zero value otherwise.

### GetMediaFileVariantsOk

`func (o *MediaCenterFile) GetMediaFileVariantsOk() (*[]MediaCenterFileVariant, bool)`

GetMediaFileVariantsOk returns a tuple with the MediaFileVariants field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaFileVariants

`func (o *MediaCenterFile) SetMediaFileVariants(v []MediaCenterFileVariant)`

SetMediaFileVariants sets MediaFileVariants field to given value.

### HasMediaFileVariants

`func (o *MediaCenterFile) HasMediaFileVariants() bool`

HasMediaFileVariants returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *MediaCenterFile) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *MediaCenterFile) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *MediaCenterFile) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetCreatedAt

`func (o *MediaCenterFile) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *MediaCenterFile) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *MediaCenterFile) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetOrganizationWide

`func (o *MediaCenterFile) GetOrganizationWide() bool`

GetOrganizationWide returns the OrganizationWide field if non-nil, zero value otherwise.

### GetOrganizationWideOk

`func (o *MediaCenterFile) GetOrganizationWideOk() (*bool, bool)`

GetOrganizationWideOk returns a tuple with the OrganizationWide field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationWide

`func (o *MediaCenterFile) SetOrganizationWide(v bool)`

SetOrganizationWide sets OrganizationWide field to given value.


### GetPublic

`func (o *MediaCenterFile) GetPublic() bool`

GetPublic returns the Public field if non-nil, zero value otherwise.

### GetPublicOk

`func (o *MediaCenterFile) GetPublicOk() (*bool, bool)`

GetPublicOk returns a tuple with the Public field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPublic

`func (o *MediaCenterFile) SetPublic(v bool)`

SetPublic sets Public field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


