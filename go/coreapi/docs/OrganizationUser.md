# OrganizationUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Email** | **string** |  | 
**FirstName** | Pointer to **string** |  | [optional] 
**LastName** | Pointer to **string** |  | [optional] 
**Company** | Pointer to **string** |  | [optional] 
**Department** | Pointer to **string** |  | [optional] 
**JobTitle** | Pointer to **string** |  | [optional] 
**Info** | Pointer to **string** |  | [optional] 
**Status** | **string** |  | 
**Roles** | Pointer to **[]string** |  | [optional] 
**LastLoginAt** | Pointer to **string** |  | [optional] 
**LoginCount** | Pointer to **int32** |  | [optional] 
**Permissions** | Pointer to [**OrganizationPermissions**](OrganizationPermissions.md) |  | [optional] 
**ExternalIds** | Pointer to **map[string]interface{}** |  | [optional] 
**ExternalLinks** | Pointer to **map[string]interface{}** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 

## Methods

### NewOrganizationUser

`func NewOrganizationUser(id string, email string, status string, ) *OrganizationUser`

NewOrganizationUser instantiates a new OrganizationUser object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrganizationUserWithDefaults

`func NewOrganizationUserWithDefaults() *OrganizationUser`

NewOrganizationUserWithDefaults instantiates a new OrganizationUser object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *OrganizationUser) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *OrganizationUser) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *OrganizationUser) SetId(v string)`

SetId sets Id field to given value.


### GetEmail

`func (o *OrganizationUser) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *OrganizationUser) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *OrganizationUser) SetEmail(v string)`

SetEmail sets Email field to given value.


### GetFirstName

`func (o *OrganizationUser) GetFirstName() string`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *OrganizationUser) GetFirstNameOk() (*string, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *OrganizationUser) SetFirstName(v string)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *OrganizationUser) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *OrganizationUser) GetLastName() string`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *OrganizationUser) GetLastNameOk() (*string, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *OrganizationUser) SetLastName(v string)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *OrganizationUser) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetCompany

`func (o *OrganizationUser) GetCompany() string`

GetCompany returns the Company field if non-nil, zero value otherwise.

### GetCompanyOk

`func (o *OrganizationUser) GetCompanyOk() (*string, bool)`

GetCompanyOk returns a tuple with the Company field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompany

`func (o *OrganizationUser) SetCompany(v string)`

SetCompany sets Company field to given value.

### HasCompany

`func (o *OrganizationUser) HasCompany() bool`

HasCompany returns a boolean if a field has been set.

### GetDepartment

`func (o *OrganizationUser) GetDepartment() string`

GetDepartment returns the Department field if non-nil, zero value otherwise.

### GetDepartmentOk

`func (o *OrganizationUser) GetDepartmentOk() (*string, bool)`

GetDepartmentOk returns a tuple with the Department field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDepartment

`func (o *OrganizationUser) SetDepartment(v string)`

SetDepartment sets Department field to given value.

### HasDepartment

`func (o *OrganizationUser) HasDepartment() bool`

HasDepartment returns a boolean if a field has been set.

### GetJobTitle

`func (o *OrganizationUser) GetJobTitle() string`

GetJobTitle returns the JobTitle field if non-nil, zero value otherwise.

### GetJobTitleOk

`func (o *OrganizationUser) GetJobTitleOk() (*string, bool)`

GetJobTitleOk returns a tuple with the JobTitle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetJobTitle

`func (o *OrganizationUser) SetJobTitle(v string)`

SetJobTitle sets JobTitle field to given value.

### HasJobTitle

`func (o *OrganizationUser) HasJobTitle() bool`

HasJobTitle returns a boolean if a field has been set.

### GetInfo

`func (o *OrganizationUser) GetInfo() string`

GetInfo returns the Info field if non-nil, zero value otherwise.

### GetInfoOk

`func (o *OrganizationUser) GetInfoOk() (*string, bool)`

GetInfoOk returns a tuple with the Info field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInfo

`func (o *OrganizationUser) SetInfo(v string)`

SetInfo sets Info field to given value.

### HasInfo

`func (o *OrganizationUser) HasInfo() bool`

HasInfo returns a boolean if a field has been set.

### GetStatus

`func (o *OrganizationUser) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *OrganizationUser) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *OrganizationUser) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetRoles

`func (o *OrganizationUser) GetRoles() []string`

GetRoles returns the Roles field if non-nil, zero value otherwise.

### GetRolesOk

`func (o *OrganizationUser) GetRolesOk() (*[]string, bool)`

GetRolesOk returns a tuple with the Roles field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRoles

`func (o *OrganizationUser) SetRoles(v []string)`

SetRoles sets Roles field to given value.

### HasRoles

`func (o *OrganizationUser) HasRoles() bool`

HasRoles returns a boolean if a field has been set.

### GetLastLoginAt

`func (o *OrganizationUser) GetLastLoginAt() string`

GetLastLoginAt returns the LastLoginAt field if non-nil, zero value otherwise.

### GetLastLoginAtOk

`func (o *OrganizationUser) GetLastLoginAtOk() (*string, bool)`

GetLastLoginAtOk returns a tuple with the LastLoginAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastLoginAt

`func (o *OrganizationUser) SetLastLoginAt(v string)`

SetLastLoginAt sets LastLoginAt field to given value.

### HasLastLoginAt

`func (o *OrganizationUser) HasLastLoginAt() bool`

HasLastLoginAt returns a boolean if a field has been set.

### GetLoginCount

`func (o *OrganizationUser) GetLoginCount() int32`

GetLoginCount returns the LoginCount field if non-nil, zero value otherwise.

### GetLoginCountOk

`func (o *OrganizationUser) GetLoginCountOk() (*int32, bool)`

GetLoginCountOk returns a tuple with the LoginCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLoginCount

`func (o *OrganizationUser) SetLoginCount(v int32)`

SetLoginCount sets LoginCount field to given value.

### HasLoginCount

`func (o *OrganizationUser) HasLoginCount() bool`

HasLoginCount returns a boolean if a field has been set.

### GetPermissions

`func (o *OrganizationUser) GetPermissions() OrganizationPermissions`

GetPermissions returns the Permissions field if non-nil, zero value otherwise.

### GetPermissionsOk

`func (o *OrganizationUser) GetPermissionsOk() (*OrganizationPermissions, bool)`

GetPermissionsOk returns a tuple with the Permissions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPermissions

`func (o *OrganizationUser) SetPermissions(v OrganizationPermissions)`

SetPermissions sets Permissions field to given value.

### HasPermissions

`func (o *OrganizationUser) HasPermissions() bool`

HasPermissions returns a boolean if a field has been set.

### GetExternalIds

`func (o *OrganizationUser) GetExternalIds() map[string]interface{}`

GetExternalIds returns the ExternalIds field if non-nil, zero value otherwise.

### GetExternalIdsOk

`func (o *OrganizationUser) GetExternalIdsOk() (*map[string]interface{}, bool)`

GetExternalIdsOk returns a tuple with the ExternalIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalIds

`func (o *OrganizationUser) SetExternalIds(v map[string]interface{})`

SetExternalIds sets ExternalIds field to given value.

### HasExternalIds

`func (o *OrganizationUser) HasExternalIds() bool`

HasExternalIds returns a boolean if a field has been set.

### GetExternalLinks

`func (o *OrganizationUser) GetExternalLinks() map[string]interface{}`

GetExternalLinks returns the ExternalLinks field if non-nil, zero value otherwise.

### GetExternalLinksOk

`func (o *OrganizationUser) GetExternalLinksOk() (*map[string]interface{}, bool)`

GetExternalLinksOk returns a tuple with the ExternalLinks field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalLinks

`func (o *OrganizationUser) SetExternalLinks(v map[string]interface{})`

SetExternalLinks sets ExternalLinks field to given value.

### HasExternalLinks

`func (o *OrganizationUser) HasExternalLinks() bool`

HasExternalLinks returns a boolean if a field has been set.

### GetTags

`func (o *OrganizationUser) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *OrganizationUser) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *OrganizationUser) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *OrganizationUser) HasTags() bool`

HasTags returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


