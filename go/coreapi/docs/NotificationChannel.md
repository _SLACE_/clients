# NotificationChannel

## Enum


* `NotificationChannelEmail` (value: `"email"`)

* `NotificationChannelBrowser` (value: `"browser"`)

* `NotificationChannelTelegram` (value: `"telegram"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


