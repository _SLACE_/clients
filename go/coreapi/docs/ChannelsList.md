# ChannelsList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]Channel**](Channel.md) |  | 
**Total** | **int32** |  | 

## Methods

### NewChannelsList

`func NewChannelsList(results []Channel, total int32, ) *ChannelsList`

NewChannelsList instantiates a new ChannelsList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChannelsListWithDefaults

`func NewChannelsListWithDefaults() *ChannelsList`

NewChannelsListWithDefaults instantiates a new ChannelsList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *ChannelsList) GetResults() []Channel`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *ChannelsList) GetResultsOk() (*[]Channel, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *ChannelsList) SetResults(v []Channel)`

SetResults sets Results field to given value.


### GetTotal

`func (o *ChannelsList) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *ChannelsList) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *ChannelsList) SetTotal(v int32)`

SetTotal sets Total field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


