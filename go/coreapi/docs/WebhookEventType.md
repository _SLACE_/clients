# WebhookEventType

## Enum


* `MessageSent` (value: `"message_sent"`)

* `MessageReceived` (value: `"message_received"`)

* `MessageReceivedAttributed` (value: `"message_received_attributed"`)

* `MessageDeleted` (value: `"message_deleted"`)

* `MessageEdited` (value: `"message_edited"`)

* `MessageSendingError` (value: `"message_sending_error"`)

* `MessageStatusChanged` (value: `"message_status_changed"`)

* `MessageMediaSynchronized` (value: `"message_media_synchronized"`)

* `ConversationOptedOut` (value: `"conversation_opted_out"`)

* `ConversationOptedIn` (value: `"conversation_opted_in"`)

* `ContactComChannelAttached` (value: `"contact_com_channel_attached"`)

* `ContactComChannelDetached` (value: `"contact_com_channel_detached"`)

* `ScriptStarted` (value: `"script_started"`)

* `ScriptFinished` (value: `"script_finished"`)

* `ContactUpdated` (value: `"contact_updated"`)

* `ContactCreated` (value: `"contact_created"`)

* `ContactDeleted` (value: `"contact_deleted"`)

* `RequestLocation` (value: `"request_location"`)

* `ConversationAttachedToTransaction` (value: `"conversation_attached_to_transaction"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


