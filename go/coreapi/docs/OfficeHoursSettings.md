# OfficeHoursSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | Pointer to **bool** |  | [optional] 
**MessageWhenClosed** | Pointer to **string** |  | [optional] 
**MessageWhenOpened** | Pointer to **string** |  | [optional] 
**Monday** | Pointer to [**[]WorkingTime**](WorkingTime.md) |  | [optional] 
**Tuesday** | Pointer to [**[]WorkingTime**](WorkingTime.md) |  | [optional] 
**Wednesday** | Pointer to [**[]WorkingTime**](WorkingTime.md) |  | [optional] 
**Thursday** | Pointer to [**[]WorkingTime**](WorkingTime.md) |  | [optional] 
**Friday** | Pointer to [**[]WorkingTime**](WorkingTime.md) |  | [optional] 
**Saturday** | Pointer to [**[]WorkingTime**](WorkingTime.md) |  | [optional] 
**Sunday** | Pointer to [**[]WorkingTime**](WorkingTime.md) |  | [optional] 

## Methods

### NewOfficeHoursSettings

`func NewOfficeHoursSettings() *OfficeHoursSettings`

NewOfficeHoursSettings instantiates a new OfficeHoursSettings object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOfficeHoursSettingsWithDefaults

`func NewOfficeHoursSettingsWithDefaults() *OfficeHoursSettings`

NewOfficeHoursSettingsWithDefaults instantiates a new OfficeHoursSettings object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetEnabled

`func (o *OfficeHoursSettings) GetEnabled() bool`

GetEnabled returns the Enabled field if non-nil, zero value otherwise.

### GetEnabledOk

`func (o *OfficeHoursSettings) GetEnabledOk() (*bool, bool)`

GetEnabledOk returns a tuple with the Enabled field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEnabled

`func (o *OfficeHoursSettings) SetEnabled(v bool)`

SetEnabled sets Enabled field to given value.

### HasEnabled

`func (o *OfficeHoursSettings) HasEnabled() bool`

HasEnabled returns a boolean if a field has been set.

### GetMessageWhenClosed

`func (o *OfficeHoursSettings) GetMessageWhenClosed() string`

GetMessageWhenClosed returns the MessageWhenClosed field if non-nil, zero value otherwise.

### GetMessageWhenClosedOk

`func (o *OfficeHoursSettings) GetMessageWhenClosedOk() (*string, bool)`

GetMessageWhenClosedOk returns a tuple with the MessageWhenClosed field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageWhenClosed

`func (o *OfficeHoursSettings) SetMessageWhenClosed(v string)`

SetMessageWhenClosed sets MessageWhenClosed field to given value.

### HasMessageWhenClosed

`func (o *OfficeHoursSettings) HasMessageWhenClosed() bool`

HasMessageWhenClosed returns a boolean if a field has been set.

### GetMessageWhenOpened

`func (o *OfficeHoursSettings) GetMessageWhenOpened() string`

GetMessageWhenOpened returns the MessageWhenOpened field if non-nil, zero value otherwise.

### GetMessageWhenOpenedOk

`func (o *OfficeHoursSettings) GetMessageWhenOpenedOk() (*string, bool)`

GetMessageWhenOpenedOk returns a tuple with the MessageWhenOpened field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageWhenOpened

`func (o *OfficeHoursSettings) SetMessageWhenOpened(v string)`

SetMessageWhenOpened sets MessageWhenOpened field to given value.

### HasMessageWhenOpened

`func (o *OfficeHoursSettings) HasMessageWhenOpened() bool`

HasMessageWhenOpened returns a boolean if a field has been set.

### GetMonday

`func (o *OfficeHoursSettings) GetMonday() []WorkingTime`

GetMonday returns the Monday field if non-nil, zero value otherwise.

### GetMondayOk

`func (o *OfficeHoursSettings) GetMondayOk() (*[]WorkingTime, bool)`

GetMondayOk returns a tuple with the Monday field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMonday

`func (o *OfficeHoursSettings) SetMonday(v []WorkingTime)`

SetMonday sets Monday field to given value.

### HasMonday

`func (o *OfficeHoursSettings) HasMonday() bool`

HasMonday returns a boolean if a field has been set.

### GetTuesday

`func (o *OfficeHoursSettings) GetTuesday() []WorkingTime`

GetTuesday returns the Tuesday field if non-nil, zero value otherwise.

### GetTuesdayOk

`func (o *OfficeHoursSettings) GetTuesdayOk() (*[]WorkingTime, bool)`

GetTuesdayOk returns a tuple with the Tuesday field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTuesday

`func (o *OfficeHoursSettings) SetTuesday(v []WorkingTime)`

SetTuesday sets Tuesday field to given value.

### HasTuesday

`func (o *OfficeHoursSettings) HasTuesday() bool`

HasTuesday returns a boolean if a field has been set.

### GetWednesday

`func (o *OfficeHoursSettings) GetWednesday() []WorkingTime`

GetWednesday returns the Wednesday field if non-nil, zero value otherwise.

### GetWednesdayOk

`func (o *OfficeHoursSettings) GetWednesdayOk() (*[]WorkingTime, bool)`

GetWednesdayOk returns a tuple with the Wednesday field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWednesday

`func (o *OfficeHoursSettings) SetWednesday(v []WorkingTime)`

SetWednesday sets Wednesday field to given value.

### HasWednesday

`func (o *OfficeHoursSettings) HasWednesday() bool`

HasWednesday returns a boolean if a field has been set.

### GetThursday

`func (o *OfficeHoursSettings) GetThursday() []WorkingTime`

GetThursday returns the Thursday field if non-nil, zero value otherwise.

### GetThursdayOk

`func (o *OfficeHoursSettings) GetThursdayOk() (*[]WorkingTime, bool)`

GetThursdayOk returns a tuple with the Thursday field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetThursday

`func (o *OfficeHoursSettings) SetThursday(v []WorkingTime)`

SetThursday sets Thursday field to given value.

### HasThursday

`func (o *OfficeHoursSettings) HasThursday() bool`

HasThursday returns a boolean if a field has been set.

### GetFriday

`func (o *OfficeHoursSettings) GetFriday() []WorkingTime`

GetFriday returns the Friday field if non-nil, zero value otherwise.

### GetFridayOk

`func (o *OfficeHoursSettings) GetFridayOk() (*[]WorkingTime, bool)`

GetFridayOk returns a tuple with the Friday field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFriday

`func (o *OfficeHoursSettings) SetFriday(v []WorkingTime)`

SetFriday sets Friday field to given value.

### HasFriday

`func (o *OfficeHoursSettings) HasFriday() bool`

HasFriday returns a boolean if a field has been set.

### GetSaturday

`func (o *OfficeHoursSettings) GetSaturday() []WorkingTime`

GetSaturday returns the Saturday field if non-nil, zero value otherwise.

### GetSaturdayOk

`func (o *OfficeHoursSettings) GetSaturdayOk() (*[]WorkingTime, bool)`

GetSaturdayOk returns a tuple with the Saturday field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSaturday

`func (o *OfficeHoursSettings) SetSaturday(v []WorkingTime)`

SetSaturday sets Saturday field to given value.

### HasSaturday

`func (o *OfficeHoursSettings) HasSaturday() bool`

HasSaturday returns a boolean if a field has been set.

### GetSunday

`func (o *OfficeHoursSettings) GetSunday() []WorkingTime`

GetSunday returns the Sunday field if non-nil, zero value otherwise.

### GetSundayOk

`func (o *OfficeHoursSettings) GetSundayOk() (*[]WorkingTime, bool)`

GetSundayOk returns a tuple with the Sunday field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSunday

`func (o *OfficeHoursSettings) SetSunday(v []WorkingTime)`

SetSunday sets Sunday field to given value.

### HasSunday

`func (o *OfficeHoursSettings) HasSunday() bool`

HasSunday returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


