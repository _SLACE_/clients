# CreateServiceUserRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**OrganizationIds** | **[]string** |  | 

## Methods

### NewCreateServiceUserRequest

`func NewCreateServiceUserRequest(name string, organizationIds []string, ) *CreateServiceUserRequest`

NewCreateServiceUserRequest instantiates a new CreateServiceUserRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateServiceUserRequestWithDefaults

`func NewCreateServiceUserRequestWithDefaults() *CreateServiceUserRequest`

NewCreateServiceUserRequestWithDefaults instantiates a new CreateServiceUserRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *CreateServiceUserRequest) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CreateServiceUserRequest) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CreateServiceUserRequest) SetName(v string)`

SetName sets Name field to given value.


### GetOrganizationIds

`func (o *CreateServiceUserRequest) GetOrganizationIds() []string`

GetOrganizationIds returns the OrganizationIds field if non-nil, zero value otherwise.

### GetOrganizationIdsOk

`func (o *CreateServiceUserRequest) GetOrganizationIdsOk() (*[]string, bool)`

GetOrganizationIdsOk returns a tuple with the OrganizationIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationIds

`func (o *CreateServiceUserRequest) SetOrganizationIds(v []string)`

SetOrganizationIds sets OrganizationIds field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


