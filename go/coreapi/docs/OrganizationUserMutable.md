# OrganizationUserMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FirstName** | Pointer to **string** |  | [optional] 
**LastName** | Pointer to **string** |  | [optional] 
**Company** | Pointer to **string** |  | [optional] 
**Department** | Pointer to **string** |  | [optional] 
**JobTitle** | Pointer to **string** |  | [optional] 
**Info** | Pointer to **string** |  | [optional] 
**ExternalIds** | Pointer to **map[string]interface{}** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 

## Methods

### NewOrganizationUserMutable

`func NewOrganizationUserMutable() *OrganizationUserMutable`

NewOrganizationUserMutable instantiates a new OrganizationUserMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrganizationUserMutableWithDefaults

`func NewOrganizationUserMutableWithDefaults() *OrganizationUserMutable`

NewOrganizationUserMutableWithDefaults instantiates a new OrganizationUserMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFirstName

`func (o *OrganizationUserMutable) GetFirstName() string`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *OrganizationUserMutable) GetFirstNameOk() (*string, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *OrganizationUserMutable) SetFirstName(v string)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *OrganizationUserMutable) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *OrganizationUserMutable) GetLastName() string`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *OrganizationUserMutable) GetLastNameOk() (*string, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *OrganizationUserMutable) SetLastName(v string)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *OrganizationUserMutable) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetCompany

`func (o *OrganizationUserMutable) GetCompany() string`

GetCompany returns the Company field if non-nil, zero value otherwise.

### GetCompanyOk

`func (o *OrganizationUserMutable) GetCompanyOk() (*string, bool)`

GetCompanyOk returns a tuple with the Company field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompany

`func (o *OrganizationUserMutable) SetCompany(v string)`

SetCompany sets Company field to given value.

### HasCompany

`func (o *OrganizationUserMutable) HasCompany() bool`

HasCompany returns a boolean if a field has been set.

### GetDepartment

`func (o *OrganizationUserMutable) GetDepartment() string`

GetDepartment returns the Department field if non-nil, zero value otherwise.

### GetDepartmentOk

`func (o *OrganizationUserMutable) GetDepartmentOk() (*string, bool)`

GetDepartmentOk returns a tuple with the Department field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDepartment

`func (o *OrganizationUserMutable) SetDepartment(v string)`

SetDepartment sets Department field to given value.

### HasDepartment

`func (o *OrganizationUserMutable) HasDepartment() bool`

HasDepartment returns a boolean if a field has been set.

### GetJobTitle

`func (o *OrganizationUserMutable) GetJobTitle() string`

GetJobTitle returns the JobTitle field if non-nil, zero value otherwise.

### GetJobTitleOk

`func (o *OrganizationUserMutable) GetJobTitleOk() (*string, bool)`

GetJobTitleOk returns a tuple with the JobTitle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetJobTitle

`func (o *OrganizationUserMutable) SetJobTitle(v string)`

SetJobTitle sets JobTitle field to given value.

### HasJobTitle

`func (o *OrganizationUserMutable) HasJobTitle() bool`

HasJobTitle returns a boolean if a field has been set.

### GetInfo

`func (o *OrganizationUserMutable) GetInfo() string`

GetInfo returns the Info field if non-nil, zero value otherwise.

### GetInfoOk

`func (o *OrganizationUserMutable) GetInfoOk() (*string, bool)`

GetInfoOk returns a tuple with the Info field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInfo

`func (o *OrganizationUserMutable) SetInfo(v string)`

SetInfo sets Info field to given value.

### HasInfo

`func (o *OrganizationUserMutable) HasInfo() bool`

HasInfo returns a boolean if a field has been set.

### GetExternalIds

`func (o *OrganizationUserMutable) GetExternalIds() map[string]interface{}`

GetExternalIds returns the ExternalIds field if non-nil, zero value otherwise.

### GetExternalIdsOk

`func (o *OrganizationUserMutable) GetExternalIdsOk() (*map[string]interface{}, bool)`

GetExternalIdsOk returns a tuple with the ExternalIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalIds

`func (o *OrganizationUserMutable) SetExternalIds(v map[string]interface{})`

SetExternalIds sets ExternalIds field to given value.

### HasExternalIds

`func (o *OrganizationUserMutable) HasExternalIds() bool`

HasExternalIds returns a boolean if a field has been set.

### GetTags

`func (o *OrganizationUserMutable) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *OrganizationUserMutable) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *OrganizationUserMutable) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *OrganizationUserMutable) HasTags() bool`

HasTags returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


