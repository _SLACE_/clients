# ObjectPermissionPermissions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Contacts** | Pointer to **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | [optional] 
**Conversations** | Pointer to **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | [optional] 
**Events** | Pointer to **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | [optional] 
**Campaigns** | Pointer to **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | [optional] 
**Locations** | Pointer to **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | [optional] 
**Users** | Pointer to **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | [optional] 
**Templates** | Pointer to **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | [optional] 
**Organizations** | Pointer to **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | [optional] 
**Gateways** | Pointer to **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | [optional] 
**Channels** | Pointer to **int32** | Available permissions list:   PermissionDenied  Permission &#x3D; 0    PermissionRead    Permission &#x3D; 1     PermissionWrite   Permission &#x3D; 2    PermissionExecute Permission &#x3D; 4    PermissionDelete  Permission &#x3D; 8   Permissions can be combined | [optional] 

## Methods

### NewObjectPermissionPermissions

`func NewObjectPermissionPermissions() *ObjectPermissionPermissions`

NewObjectPermissionPermissions instantiates a new ObjectPermissionPermissions object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewObjectPermissionPermissionsWithDefaults

`func NewObjectPermissionPermissionsWithDefaults() *ObjectPermissionPermissions`

NewObjectPermissionPermissionsWithDefaults instantiates a new ObjectPermissionPermissions object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContacts

`func (o *ObjectPermissionPermissions) GetContacts() int32`

GetContacts returns the Contacts field if non-nil, zero value otherwise.

### GetContactsOk

`func (o *ObjectPermissionPermissions) GetContactsOk() (*int32, bool)`

GetContactsOk returns a tuple with the Contacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContacts

`func (o *ObjectPermissionPermissions) SetContacts(v int32)`

SetContacts sets Contacts field to given value.

### HasContacts

`func (o *ObjectPermissionPermissions) HasContacts() bool`

HasContacts returns a boolean if a field has been set.

### GetConversations

`func (o *ObjectPermissionPermissions) GetConversations() int32`

GetConversations returns the Conversations field if non-nil, zero value otherwise.

### GetConversationsOk

`func (o *ObjectPermissionPermissions) GetConversationsOk() (*int32, bool)`

GetConversationsOk returns a tuple with the Conversations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversations

`func (o *ObjectPermissionPermissions) SetConversations(v int32)`

SetConversations sets Conversations field to given value.

### HasConversations

`func (o *ObjectPermissionPermissions) HasConversations() bool`

HasConversations returns a boolean if a field has been set.

### GetEvents

`func (o *ObjectPermissionPermissions) GetEvents() int32`

GetEvents returns the Events field if non-nil, zero value otherwise.

### GetEventsOk

`func (o *ObjectPermissionPermissions) GetEventsOk() (*int32, bool)`

GetEventsOk returns a tuple with the Events field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvents

`func (o *ObjectPermissionPermissions) SetEvents(v int32)`

SetEvents sets Events field to given value.

### HasEvents

`func (o *ObjectPermissionPermissions) HasEvents() bool`

HasEvents returns a boolean if a field has been set.

### GetCampaigns

`func (o *ObjectPermissionPermissions) GetCampaigns() int32`

GetCampaigns returns the Campaigns field if non-nil, zero value otherwise.

### GetCampaignsOk

`func (o *ObjectPermissionPermissions) GetCampaignsOk() (*int32, bool)`

GetCampaignsOk returns a tuple with the Campaigns field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCampaigns

`func (o *ObjectPermissionPermissions) SetCampaigns(v int32)`

SetCampaigns sets Campaigns field to given value.

### HasCampaigns

`func (o *ObjectPermissionPermissions) HasCampaigns() bool`

HasCampaigns returns a boolean if a field has been set.

### GetLocations

`func (o *ObjectPermissionPermissions) GetLocations() int32`

GetLocations returns the Locations field if non-nil, zero value otherwise.

### GetLocationsOk

`func (o *ObjectPermissionPermissions) GetLocationsOk() (*int32, bool)`

GetLocationsOk returns a tuple with the Locations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocations

`func (o *ObjectPermissionPermissions) SetLocations(v int32)`

SetLocations sets Locations field to given value.

### HasLocations

`func (o *ObjectPermissionPermissions) HasLocations() bool`

HasLocations returns a boolean if a field has been set.

### GetUsers

`func (o *ObjectPermissionPermissions) GetUsers() int32`

GetUsers returns the Users field if non-nil, zero value otherwise.

### GetUsersOk

`func (o *ObjectPermissionPermissions) GetUsersOk() (*int32, bool)`

GetUsersOk returns a tuple with the Users field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsers

`func (o *ObjectPermissionPermissions) SetUsers(v int32)`

SetUsers sets Users field to given value.

### HasUsers

`func (o *ObjectPermissionPermissions) HasUsers() bool`

HasUsers returns a boolean if a field has been set.

### GetTemplates

`func (o *ObjectPermissionPermissions) GetTemplates() int32`

GetTemplates returns the Templates field if non-nil, zero value otherwise.

### GetTemplatesOk

`func (o *ObjectPermissionPermissions) GetTemplatesOk() (*int32, bool)`

GetTemplatesOk returns a tuple with the Templates field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplates

`func (o *ObjectPermissionPermissions) SetTemplates(v int32)`

SetTemplates sets Templates field to given value.

### HasTemplates

`func (o *ObjectPermissionPermissions) HasTemplates() bool`

HasTemplates returns a boolean if a field has been set.

### GetOrganizations

`func (o *ObjectPermissionPermissions) GetOrganizations() int32`

GetOrganizations returns the Organizations field if non-nil, zero value otherwise.

### GetOrganizationsOk

`func (o *ObjectPermissionPermissions) GetOrganizationsOk() (*int32, bool)`

GetOrganizationsOk returns a tuple with the Organizations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizations

`func (o *ObjectPermissionPermissions) SetOrganizations(v int32)`

SetOrganizations sets Organizations field to given value.

### HasOrganizations

`func (o *ObjectPermissionPermissions) HasOrganizations() bool`

HasOrganizations returns a boolean if a field has been set.

### GetGateways

`func (o *ObjectPermissionPermissions) GetGateways() int32`

GetGateways returns the Gateways field if non-nil, zero value otherwise.

### GetGatewaysOk

`func (o *ObjectPermissionPermissions) GetGatewaysOk() (*int32, bool)`

GetGatewaysOk returns a tuple with the Gateways field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGateways

`func (o *ObjectPermissionPermissions) SetGateways(v int32)`

SetGateways sets Gateways field to given value.

### HasGateways

`func (o *ObjectPermissionPermissions) HasGateways() bool`

HasGateways returns a boolean if a field has been set.

### GetChannels

`func (o *ObjectPermissionPermissions) GetChannels() int32`

GetChannels returns the Channels field if non-nil, zero value otherwise.

### GetChannelsOk

`func (o *ObjectPermissionPermissions) GetChannelsOk() (*int32, bool)`

GetChannelsOk returns a tuple with the Channels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannels

`func (o *ObjectPermissionPermissions) SetChannels(v int32)`

SetChannels sets Channels field to given value.

### HasChannels

`func (o *ObjectPermissionPermissions) HasChannels() bool`

HasChannels returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


