# CheckProgress200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Progress** | Pointer to **int32** |  | [optional] 
**Total** | Pointer to **int32** |  | [optional] 

## Methods

### NewCheckProgress200Response

`func NewCheckProgress200Response() *CheckProgress200Response`

NewCheckProgress200Response instantiates a new CheckProgress200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCheckProgress200ResponseWithDefaults

`func NewCheckProgress200ResponseWithDefaults() *CheckProgress200Response`

NewCheckProgress200ResponseWithDefaults instantiates a new CheckProgress200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetProgress

`func (o *CheckProgress200Response) GetProgress() int32`

GetProgress returns the Progress field if non-nil, zero value otherwise.

### GetProgressOk

`func (o *CheckProgress200Response) GetProgressOk() (*int32, bool)`

GetProgressOk returns a tuple with the Progress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProgress

`func (o *CheckProgress200Response) SetProgress(v int32)`

SetProgress sets Progress field to given value.

### HasProgress

`func (o *CheckProgress200Response) HasProgress() bool`

HasProgress returns a boolean if a field has been set.

### GetTotal

`func (o *CheckProgress200Response) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *CheckProgress200Response) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *CheckProgress200Response) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *CheckProgress200Response) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


