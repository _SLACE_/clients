# MessageCard

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Text** | **string** |  | 
**Buttons** | [**[]MessageButton**](MessageButton.md) |  | 

## Methods

### NewMessageCard

`func NewMessageCard(text string, buttons []MessageButton, ) *MessageCard`

NewMessageCard instantiates a new MessageCard object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageCardWithDefaults

`func NewMessageCardWithDefaults() *MessageCard`

NewMessageCardWithDefaults instantiates a new MessageCard object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetText

`func (o *MessageCard) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *MessageCard) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *MessageCard) SetText(v string)`

SetText sets Text field to given value.


### GetButtons

`func (o *MessageCard) GetButtons() []MessageButton`

GetButtons returns the Buttons field if non-nil, zero value otherwise.

### GetButtonsOk

`func (o *MessageCard) GetButtonsOk() (*[]MessageButton, bool)`

GetButtonsOk returns a tuple with the Buttons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtons

`func (o *MessageCard) SetButtons(v []MessageButton)`

SetButtons sets Buttons field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


