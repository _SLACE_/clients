# TemplatesCloneResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NewTemplates** | Pointer to [**[]ClonedTemplate**](ClonedTemplate.md) |  | [optional] 
**Issues** | Pointer to **[]string** | Returned when templates were cloned but there are some issues requiring manual intervention | [optional] 
**Success** | **bool** |  | 

## Methods

### NewTemplatesCloneResult

`func NewTemplatesCloneResult(success bool, ) *TemplatesCloneResult`

NewTemplatesCloneResult instantiates a new TemplatesCloneResult object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTemplatesCloneResultWithDefaults

`func NewTemplatesCloneResultWithDefaults() *TemplatesCloneResult`

NewTemplatesCloneResultWithDefaults instantiates a new TemplatesCloneResult object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNewTemplates

`func (o *TemplatesCloneResult) GetNewTemplates() []ClonedTemplate`

GetNewTemplates returns the NewTemplates field if non-nil, zero value otherwise.

### GetNewTemplatesOk

`func (o *TemplatesCloneResult) GetNewTemplatesOk() (*[]ClonedTemplate, bool)`

GetNewTemplatesOk returns a tuple with the NewTemplates field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewTemplates

`func (o *TemplatesCloneResult) SetNewTemplates(v []ClonedTemplate)`

SetNewTemplates sets NewTemplates field to given value.

### HasNewTemplates

`func (o *TemplatesCloneResult) HasNewTemplates() bool`

HasNewTemplates returns a boolean if a field has been set.

### GetIssues

`func (o *TemplatesCloneResult) GetIssues() []string`

GetIssues returns the Issues field if non-nil, zero value otherwise.

### GetIssuesOk

`func (o *TemplatesCloneResult) GetIssuesOk() (*[]string, bool)`

GetIssuesOk returns a tuple with the Issues field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssues

`func (o *TemplatesCloneResult) SetIssues(v []string)`

SetIssues sets Issues field to given value.

### HasIssues

`func (o *TemplatesCloneResult) HasIssues() bool`

HasIssues returns a boolean if a field has been set.

### GetSuccess

`func (o *TemplatesCloneResult) GetSuccess() bool`

GetSuccess returns the Success field if non-nil, zero value otherwise.

### GetSuccessOk

`func (o *TemplatesCloneResult) GetSuccessOk() (*bool, bool)`

GetSuccessOk returns a tuple with the Success field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSuccess

`func (o *TemplatesCloneResult) SetSuccess(v bool)`

SetSuccess sets Success field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


