# WebhookEventContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Name** | Pointer to **string** |  | [optional] 
**FirstName** | Pointer to **string** |  | [optional] 
**LastName** | Pointer to **string** |  | [optional] 
**Timezone** | Pointer to **string** |  | [optional] 
**Email** | Pointer to **string** |  | [optional] 
**Language** | Pointer to **string** |  | [optional] 
**Mobile** | Pointer to **string** |  | [optional] 
**MobileVerified** | Pointer to **bool** |  | [optional] 
**Address** | Pointer to [**WebhookEventContactAddress**](WebhookEventContactAddress.md) |  | [optional] 
**Attributes** | Pointer to **map[string]interface{}** |  | [optional] 

## Methods

### NewWebhookEventContact

`func NewWebhookEventContact(id string, ) *WebhookEventContact`

NewWebhookEventContact instantiates a new WebhookEventContact object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookEventContactWithDefaults

`func NewWebhookEventContactWithDefaults() *WebhookEventContact`

NewWebhookEventContactWithDefaults instantiates a new WebhookEventContact object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WebhookEventContact) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WebhookEventContact) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WebhookEventContact) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *WebhookEventContact) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *WebhookEventContact) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *WebhookEventContact) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *WebhookEventContact) HasName() bool`

HasName returns a boolean if a field has been set.

### GetFirstName

`func (o *WebhookEventContact) GetFirstName() string`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *WebhookEventContact) GetFirstNameOk() (*string, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *WebhookEventContact) SetFirstName(v string)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *WebhookEventContact) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *WebhookEventContact) GetLastName() string`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *WebhookEventContact) GetLastNameOk() (*string, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *WebhookEventContact) SetLastName(v string)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *WebhookEventContact) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetTimezone

`func (o *WebhookEventContact) GetTimezone() string`

GetTimezone returns the Timezone field if non-nil, zero value otherwise.

### GetTimezoneOk

`func (o *WebhookEventContact) GetTimezoneOk() (*string, bool)`

GetTimezoneOk returns a tuple with the Timezone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimezone

`func (o *WebhookEventContact) SetTimezone(v string)`

SetTimezone sets Timezone field to given value.

### HasTimezone

`func (o *WebhookEventContact) HasTimezone() bool`

HasTimezone returns a boolean if a field has been set.

### GetEmail

`func (o *WebhookEventContact) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *WebhookEventContact) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *WebhookEventContact) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *WebhookEventContact) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetLanguage

`func (o *WebhookEventContact) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *WebhookEventContact) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *WebhookEventContact) SetLanguage(v string)`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *WebhookEventContact) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.

### GetMobile

`func (o *WebhookEventContact) GetMobile() string`

GetMobile returns the Mobile field if non-nil, zero value otherwise.

### GetMobileOk

`func (o *WebhookEventContact) GetMobileOk() (*string, bool)`

GetMobileOk returns a tuple with the Mobile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobile

`func (o *WebhookEventContact) SetMobile(v string)`

SetMobile sets Mobile field to given value.

### HasMobile

`func (o *WebhookEventContact) HasMobile() bool`

HasMobile returns a boolean if a field has been set.

### GetMobileVerified

`func (o *WebhookEventContact) GetMobileVerified() bool`

GetMobileVerified returns the MobileVerified field if non-nil, zero value otherwise.

### GetMobileVerifiedOk

`func (o *WebhookEventContact) GetMobileVerifiedOk() (*bool, bool)`

GetMobileVerifiedOk returns a tuple with the MobileVerified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobileVerified

`func (o *WebhookEventContact) SetMobileVerified(v bool)`

SetMobileVerified sets MobileVerified field to given value.

### HasMobileVerified

`func (o *WebhookEventContact) HasMobileVerified() bool`

HasMobileVerified returns a boolean if a field has been set.

### GetAddress

`func (o *WebhookEventContact) GetAddress() WebhookEventContactAddress`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *WebhookEventContact) GetAddressOk() (*WebhookEventContactAddress, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *WebhookEventContact) SetAddress(v WebhookEventContactAddress)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *WebhookEventContact) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetAttributes

`func (o *WebhookEventContact) GetAttributes() map[string]interface{}`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *WebhookEventContact) GetAttributesOk() (*map[string]interface{}, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *WebhookEventContact) SetAttributes(v map[string]interface{})`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *WebhookEventContact) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


