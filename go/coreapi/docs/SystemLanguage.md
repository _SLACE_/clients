# SystemLanguage

## Enum


* `LanguageGerman` (value: `"de"`)

* `LanguageEnglish` (value: `"en"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


