# ExecuteForGroupRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelId** | Pointer to **string** |  | [optional] 
**SelectedIds** | Pointer to **[]string** |  | [optional] 
**SelectionMode** | Pointer to **string** |  | [optional] 

## Methods

### NewExecuteForGroupRequest

`func NewExecuteForGroupRequest() *ExecuteForGroupRequest`

NewExecuteForGroupRequest instantiates a new ExecuteForGroupRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExecuteForGroupRequestWithDefaults

`func NewExecuteForGroupRequestWithDefaults() *ExecuteForGroupRequest`

NewExecuteForGroupRequestWithDefaults instantiates a new ExecuteForGroupRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetChannelId

`func (o *ExecuteForGroupRequest) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *ExecuteForGroupRequest) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *ExecuteForGroupRequest) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *ExecuteForGroupRequest) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetSelectedIds

`func (o *ExecuteForGroupRequest) GetSelectedIds() []string`

GetSelectedIds returns the SelectedIds field if non-nil, zero value otherwise.

### GetSelectedIdsOk

`func (o *ExecuteForGroupRequest) GetSelectedIdsOk() (*[]string, bool)`

GetSelectedIdsOk returns a tuple with the SelectedIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelectedIds

`func (o *ExecuteForGroupRequest) SetSelectedIds(v []string)`

SetSelectedIds sets SelectedIds field to given value.

### HasSelectedIds

`func (o *ExecuteForGroupRequest) HasSelectedIds() bool`

HasSelectedIds returns a boolean if a field has been set.

### GetSelectionMode

`func (o *ExecuteForGroupRequest) GetSelectionMode() string`

GetSelectionMode returns the SelectionMode field if non-nil, zero value otherwise.

### GetSelectionModeOk

`func (o *ExecuteForGroupRequest) GetSelectionModeOk() (*string, bool)`

GetSelectionModeOk returns a tuple with the SelectionMode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelectionMode

`func (o *ExecuteForGroupRequest) SetSelectionMode(v string)`

SetSelectionMode sets SelectionMode field to given value.

### HasSelectionMode

`func (o *ExecuteForGroupRequest) HasSelectionMode() bool`

HasSelectionMode returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


