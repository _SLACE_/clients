# UpdateGatewayActivityRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Acvive** | Pointer to **bool** |  | [optional] 

## Methods

### NewUpdateGatewayActivityRequest

`func NewUpdateGatewayActivityRequest() *UpdateGatewayActivityRequest`

NewUpdateGatewayActivityRequest instantiates a new UpdateGatewayActivityRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateGatewayActivityRequestWithDefaults

`func NewUpdateGatewayActivityRequestWithDefaults() *UpdateGatewayActivityRequest`

NewUpdateGatewayActivityRequestWithDefaults instantiates a new UpdateGatewayActivityRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAcvive

`func (o *UpdateGatewayActivityRequest) GetAcvive() bool`

GetAcvive returns the Acvive field if non-nil, zero value otherwise.

### GetAcviveOk

`func (o *UpdateGatewayActivityRequest) GetAcviveOk() (*bool, bool)`

GetAcviveOk returns a tuple with the Acvive field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAcvive

`func (o *UpdateGatewayActivityRequest) SetAcvive(v bool)`

SetAcvive sets Acvive field to given value.

### HasAcvive

`func (o *UpdateGatewayActivityRequest) HasAcvive() bool`

HasAcvive returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


