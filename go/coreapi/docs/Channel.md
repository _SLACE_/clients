# Channel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Name** | **string** |  | 
**Timezone** | Pointer to **string** |  | [optional] 
**DefaultLanguage** | Pointer to **string** |  | [optional] 
**Readonly** | **bool** |  | 
**AttributionEnabled** | Pointer to **bool** |  | [optional] 
**StopKeywords** | **[]string** |  | 
**Tags** | Pointer to [**[]ChannelTagsInner**](ChannelTagsInner.md) |  | [optional] 
**Customer** | [**Customer**](Customer.md) |  | 
**Organization** | Pointer to [**Organization**](Organization.md) |  | [optional] 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 
**FallbackLanguage** | Pointer to **string** |  | [optional] 
**DefaultDialog360Tier** | Pointer to [**Dialog360Tier**](Dialog360Tier.md) |  | [optional] 
**Dialog360Tier** | Pointer to [**Dialog360Tier**](Dialog360Tier.md) |  | [optional] 

## Methods

### NewChannel

`func NewChannel(id string, name string, readonly bool, stopKeywords []string, customer Customer, createdAt time.Time, updatedAt time.Time, ) *Channel`

NewChannel instantiates a new Channel object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChannelWithDefaults

`func NewChannelWithDefaults() *Channel`

NewChannelWithDefaults instantiates a new Channel object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Channel) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Channel) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Channel) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *Channel) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Channel) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Channel) SetName(v string)`

SetName sets Name field to given value.


### GetTimezone

`func (o *Channel) GetTimezone() string`

GetTimezone returns the Timezone field if non-nil, zero value otherwise.

### GetTimezoneOk

`func (o *Channel) GetTimezoneOk() (*string, bool)`

GetTimezoneOk returns a tuple with the Timezone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimezone

`func (o *Channel) SetTimezone(v string)`

SetTimezone sets Timezone field to given value.

### HasTimezone

`func (o *Channel) HasTimezone() bool`

HasTimezone returns a boolean if a field has been set.

### GetDefaultLanguage

`func (o *Channel) GetDefaultLanguage() string`

GetDefaultLanguage returns the DefaultLanguage field if non-nil, zero value otherwise.

### GetDefaultLanguageOk

`func (o *Channel) GetDefaultLanguageOk() (*string, bool)`

GetDefaultLanguageOk returns a tuple with the DefaultLanguage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultLanguage

`func (o *Channel) SetDefaultLanguage(v string)`

SetDefaultLanguage sets DefaultLanguage field to given value.

### HasDefaultLanguage

`func (o *Channel) HasDefaultLanguage() bool`

HasDefaultLanguage returns a boolean if a field has been set.

### GetReadonly

`func (o *Channel) GetReadonly() bool`

GetReadonly returns the Readonly field if non-nil, zero value otherwise.

### GetReadonlyOk

`func (o *Channel) GetReadonlyOk() (*bool, bool)`

GetReadonlyOk returns a tuple with the Readonly field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReadonly

`func (o *Channel) SetReadonly(v bool)`

SetReadonly sets Readonly field to given value.


### GetAttributionEnabled

`func (o *Channel) GetAttributionEnabled() bool`

GetAttributionEnabled returns the AttributionEnabled field if non-nil, zero value otherwise.

### GetAttributionEnabledOk

`func (o *Channel) GetAttributionEnabledOk() (*bool, bool)`

GetAttributionEnabledOk returns a tuple with the AttributionEnabled field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributionEnabled

`func (o *Channel) SetAttributionEnabled(v bool)`

SetAttributionEnabled sets AttributionEnabled field to given value.

### HasAttributionEnabled

`func (o *Channel) HasAttributionEnabled() bool`

HasAttributionEnabled returns a boolean if a field has been set.

### GetStopKeywords

`func (o *Channel) GetStopKeywords() []string`

GetStopKeywords returns the StopKeywords field if non-nil, zero value otherwise.

### GetStopKeywordsOk

`func (o *Channel) GetStopKeywordsOk() (*[]string, bool)`

GetStopKeywordsOk returns a tuple with the StopKeywords field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStopKeywords

`func (o *Channel) SetStopKeywords(v []string)`

SetStopKeywords sets StopKeywords field to given value.


### GetTags

`func (o *Channel) GetTags() []ChannelTagsInner`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *Channel) GetTagsOk() (*[]ChannelTagsInner, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *Channel) SetTags(v []ChannelTagsInner)`

SetTags sets Tags field to given value.

### HasTags

`func (o *Channel) HasTags() bool`

HasTags returns a boolean if a field has been set.

### GetCustomer

`func (o *Channel) GetCustomer() Customer`

GetCustomer returns the Customer field if non-nil, zero value otherwise.

### GetCustomerOk

`func (o *Channel) GetCustomerOk() (*Customer, bool)`

GetCustomerOk returns a tuple with the Customer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomer

`func (o *Channel) SetCustomer(v Customer)`

SetCustomer sets Customer field to given value.


### GetOrganization

`func (o *Channel) GetOrganization() Organization`

GetOrganization returns the Organization field if non-nil, zero value otherwise.

### GetOrganizationOk

`func (o *Channel) GetOrganizationOk() (*Organization, bool)`

GetOrganizationOk returns a tuple with the Organization field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganization

`func (o *Channel) SetOrganization(v Organization)`

SetOrganization sets Organization field to given value.

### HasOrganization

`func (o *Channel) HasOrganization() bool`

HasOrganization returns a boolean if a field has been set.

### GetCreatedAt

`func (o *Channel) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Channel) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Channel) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *Channel) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Channel) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Channel) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetFallbackLanguage

`func (o *Channel) GetFallbackLanguage() string`

GetFallbackLanguage returns the FallbackLanguage field if non-nil, zero value otherwise.

### GetFallbackLanguageOk

`func (o *Channel) GetFallbackLanguageOk() (*string, bool)`

GetFallbackLanguageOk returns a tuple with the FallbackLanguage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFallbackLanguage

`func (o *Channel) SetFallbackLanguage(v string)`

SetFallbackLanguage sets FallbackLanguage field to given value.

### HasFallbackLanguage

`func (o *Channel) HasFallbackLanguage() bool`

HasFallbackLanguage returns a boolean if a field has been set.

### GetDefaultDialog360Tier

`func (o *Channel) GetDefaultDialog360Tier() Dialog360Tier`

GetDefaultDialog360Tier returns the DefaultDialog360Tier field if non-nil, zero value otherwise.

### GetDefaultDialog360TierOk

`func (o *Channel) GetDefaultDialog360TierOk() (*Dialog360Tier, bool)`

GetDefaultDialog360TierOk returns a tuple with the DefaultDialog360Tier field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultDialog360Tier

`func (o *Channel) SetDefaultDialog360Tier(v Dialog360Tier)`

SetDefaultDialog360Tier sets DefaultDialog360Tier field to given value.

### HasDefaultDialog360Tier

`func (o *Channel) HasDefaultDialog360Tier() bool`

HasDefaultDialog360Tier returns a boolean if a field has been set.

### GetDialog360Tier

`func (o *Channel) GetDialog360Tier() Dialog360Tier`

GetDialog360Tier returns the Dialog360Tier field if non-nil, zero value otherwise.

### GetDialog360TierOk

`func (o *Channel) GetDialog360TierOk() (*Dialog360Tier, bool)`

GetDialog360TierOk returns a tuple with the Dialog360Tier field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDialog360Tier

`func (o *Channel) SetDialog360Tier(v Dialog360Tier)`

SetDialog360Tier sets Dialog360Tier field to given value.

### HasDialog360Tier

`func (o *Channel) HasDialog360Tier() bool`

HasDialog360Tier returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


