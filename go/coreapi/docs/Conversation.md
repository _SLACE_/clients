# Conversation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**ChannelId** | **string** |  | 
**GatewayId** | **string** |  | 
**ComChannel** | [**CommunicationChannel**](CommunicationChannel.md) |  | 
**LastMessage** | Pointer to [**ConversationMessage**](ConversationMessage.md) |  | [optional] 
**Labels** | Pointer to **[]string** |  | [optional] 
**CustomData** | Pointer to [**ConversationCustomData**](ConversationCustomData.md) |  | [optional] 
**OptedIn** | **bool** |  | 
**Blocked** | **bool** |  | 
**BlockedInfo** | Pointer to **string** |  | [optional] 
**OptOutType** | Pointer to **string** |  | [optional] 
**CreatedAt** | **time.Time** |  | 
**UpdateAt** | **time.Time** |  | 
**LastSentMsgAt** | Pointer to **time.Time** |  | [optional] 
**LastReceivedMsgAt** | Pointer to **time.Time** |  | [optional] 
**LastMsgAt** | Pointer to **time.Time** |  | [optional] 
**OptedInAt** | Pointer to **time.Time** |  | [optional] 
**OptedOutAt** | Pointer to **time.Time** |  | [optional] 
**ReadAt** | Pointer to **time.Time** |  | [optional] 
**AgentRequested** | **bool** |  | 
**AgentRequestedType** | Pointer to **string** |  | [optional] 
**AgentTaken** | **bool** | When true it means that human agent took over this conversation  | 
**NewMessages** | **int32** |  | 

## Methods

### NewConversation

`func NewConversation(id string, channelId string, gatewayId string, comChannel CommunicationChannel, optedIn bool, blocked bool, createdAt time.Time, updateAt time.Time, agentRequested bool, agentTaken bool, newMessages int32, ) *Conversation`

NewConversation instantiates a new Conversation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConversationWithDefaults

`func NewConversationWithDefaults() *Conversation`

NewConversationWithDefaults instantiates a new Conversation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Conversation) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Conversation) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Conversation) SetId(v string)`

SetId sets Id field to given value.


### GetChannelId

`func (o *Conversation) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *Conversation) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *Conversation) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetGatewayId

`func (o *Conversation) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *Conversation) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *Conversation) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.


### GetComChannel

`func (o *Conversation) GetComChannel() CommunicationChannel`

GetComChannel returns the ComChannel field if non-nil, zero value otherwise.

### GetComChannelOk

`func (o *Conversation) GetComChannelOk() (*CommunicationChannel, bool)`

GetComChannelOk returns a tuple with the ComChannel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComChannel

`func (o *Conversation) SetComChannel(v CommunicationChannel)`

SetComChannel sets ComChannel field to given value.


### GetLastMessage

`func (o *Conversation) GetLastMessage() ConversationMessage`

GetLastMessage returns the LastMessage field if non-nil, zero value otherwise.

### GetLastMessageOk

`func (o *Conversation) GetLastMessageOk() (*ConversationMessage, bool)`

GetLastMessageOk returns a tuple with the LastMessage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastMessage

`func (o *Conversation) SetLastMessage(v ConversationMessage)`

SetLastMessage sets LastMessage field to given value.

### HasLastMessage

`func (o *Conversation) HasLastMessage() bool`

HasLastMessage returns a boolean if a field has been set.

### GetLabels

`func (o *Conversation) GetLabels() []string`

GetLabels returns the Labels field if non-nil, zero value otherwise.

### GetLabelsOk

`func (o *Conversation) GetLabelsOk() (*[]string, bool)`

GetLabelsOk returns a tuple with the Labels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabels

`func (o *Conversation) SetLabels(v []string)`

SetLabels sets Labels field to given value.

### HasLabels

`func (o *Conversation) HasLabels() bool`

HasLabels returns a boolean if a field has been set.

### GetCustomData

`func (o *Conversation) GetCustomData() ConversationCustomData`

GetCustomData returns the CustomData field if non-nil, zero value otherwise.

### GetCustomDataOk

`func (o *Conversation) GetCustomDataOk() (*ConversationCustomData, bool)`

GetCustomDataOk returns a tuple with the CustomData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomData

`func (o *Conversation) SetCustomData(v ConversationCustomData)`

SetCustomData sets CustomData field to given value.

### HasCustomData

`func (o *Conversation) HasCustomData() bool`

HasCustomData returns a boolean if a field has been set.

### GetOptedIn

`func (o *Conversation) GetOptedIn() bool`

GetOptedIn returns the OptedIn field if non-nil, zero value otherwise.

### GetOptedInOk

`func (o *Conversation) GetOptedInOk() (*bool, bool)`

GetOptedInOk returns a tuple with the OptedIn field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptedIn

`func (o *Conversation) SetOptedIn(v bool)`

SetOptedIn sets OptedIn field to given value.


### GetBlocked

`func (o *Conversation) GetBlocked() bool`

GetBlocked returns the Blocked field if non-nil, zero value otherwise.

### GetBlockedOk

`func (o *Conversation) GetBlockedOk() (*bool, bool)`

GetBlockedOk returns a tuple with the Blocked field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlocked

`func (o *Conversation) SetBlocked(v bool)`

SetBlocked sets Blocked field to given value.


### GetBlockedInfo

`func (o *Conversation) GetBlockedInfo() string`

GetBlockedInfo returns the BlockedInfo field if non-nil, zero value otherwise.

### GetBlockedInfoOk

`func (o *Conversation) GetBlockedInfoOk() (*string, bool)`

GetBlockedInfoOk returns a tuple with the BlockedInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockedInfo

`func (o *Conversation) SetBlockedInfo(v string)`

SetBlockedInfo sets BlockedInfo field to given value.

### HasBlockedInfo

`func (o *Conversation) HasBlockedInfo() bool`

HasBlockedInfo returns a boolean if a field has been set.

### GetOptOutType

`func (o *Conversation) GetOptOutType() string`

GetOptOutType returns the OptOutType field if non-nil, zero value otherwise.

### GetOptOutTypeOk

`func (o *Conversation) GetOptOutTypeOk() (*string, bool)`

GetOptOutTypeOk returns a tuple with the OptOutType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptOutType

`func (o *Conversation) SetOptOutType(v string)`

SetOptOutType sets OptOutType field to given value.

### HasOptOutType

`func (o *Conversation) HasOptOutType() bool`

HasOptOutType returns a boolean if a field has been set.

### GetCreatedAt

`func (o *Conversation) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Conversation) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Conversation) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdateAt

`func (o *Conversation) GetUpdateAt() time.Time`

GetUpdateAt returns the UpdateAt field if non-nil, zero value otherwise.

### GetUpdateAtOk

`func (o *Conversation) GetUpdateAtOk() (*time.Time, bool)`

GetUpdateAtOk returns a tuple with the UpdateAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdateAt

`func (o *Conversation) SetUpdateAt(v time.Time)`

SetUpdateAt sets UpdateAt field to given value.


### GetLastSentMsgAt

`func (o *Conversation) GetLastSentMsgAt() time.Time`

GetLastSentMsgAt returns the LastSentMsgAt field if non-nil, zero value otherwise.

### GetLastSentMsgAtOk

`func (o *Conversation) GetLastSentMsgAtOk() (*time.Time, bool)`

GetLastSentMsgAtOk returns a tuple with the LastSentMsgAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastSentMsgAt

`func (o *Conversation) SetLastSentMsgAt(v time.Time)`

SetLastSentMsgAt sets LastSentMsgAt field to given value.

### HasLastSentMsgAt

`func (o *Conversation) HasLastSentMsgAt() bool`

HasLastSentMsgAt returns a boolean if a field has been set.

### GetLastReceivedMsgAt

`func (o *Conversation) GetLastReceivedMsgAt() time.Time`

GetLastReceivedMsgAt returns the LastReceivedMsgAt field if non-nil, zero value otherwise.

### GetLastReceivedMsgAtOk

`func (o *Conversation) GetLastReceivedMsgAtOk() (*time.Time, bool)`

GetLastReceivedMsgAtOk returns a tuple with the LastReceivedMsgAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastReceivedMsgAt

`func (o *Conversation) SetLastReceivedMsgAt(v time.Time)`

SetLastReceivedMsgAt sets LastReceivedMsgAt field to given value.

### HasLastReceivedMsgAt

`func (o *Conversation) HasLastReceivedMsgAt() bool`

HasLastReceivedMsgAt returns a boolean if a field has been set.

### GetLastMsgAt

`func (o *Conversation) GetLastMsgAt() time.Time`

GetLastMsgAt returns the LastMsgAt field if non-nil, zero value otherwise.

### GetLastMsgAtOk

`func (o *Conversation) GetLastMsgAtOk() (*time.Time, bool)`

GetLastMsgAtOk returns a tuple with the LastMsgAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastMsgAt

`func (o *Conversation) SetLastMsgAt(v time.Time)`

SetLastMsgAt sets LastMsgAt field to given value.

### HasLastMsgAt

`func (o *Conversation) HasLastMsgAt() bool`

HasLastMsgAt returns a boolean if a field has been set.

### GetOptedInAt

`func (o *Conversation) GetOptedInAt() time.Time`

GetOptedInAt returns the OptedInAt field if non-nil, zero value otherwise.

### GetOptedInAtOk

`func (o *Conversation) GetOptedInAtOk() (*time.Time, bool)`

GetOptedInAtOk returns a tuple with the OptedInAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptedInAt

`func (o *Conversation) SetOptedInAt(v time.Time)`

SetOptedInAt sets OptedInAt field to given value.

### HasOptedInAt

`func (o *Conversation) HasOptedInAt() bool`

HasOptedInAt returns a boolean if a field has been set.

### GetOptedOutAt

`func (o *Conversation) GetOptedOutAt() time.Time`

GetOptedOutAt returns the OptedOutAt field if non-nil, zero value otherwise.

### GetOptedOutAtOk

`func (o *Conversation) GetOptedOutAtOk() (*time.Time, bool)`

GetOptedOutAtOk returns a tuple with the OptedOutAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptedOutAt

`func (o *Conversation) SetOptedOutAt(v time.Time)`

SetOptedOutAt sets OptedOutAt field to given value.

### HasOptedOutAt

`func (o *Conversation) HasOptedOutAt() bool`

HasOptedOutAt returns a boolean if a field has been set.

### GetReadAt

`func (o *Conversation) GetReadAt() time.Time`

GetReadAt returns the ReadAt field if non-nil, zero value otherwise.

### GetReadAtOk

`func (o *Conversation) GetReadAtOk() (*time.Time, bool)`

GetReadAtOk returns a tuple with the ReadAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReadAt

`func (o *Conversation) SetReadAt(v time.Time)`

SetReadAt sets ReadAt field to given value.

### HasReadAt

`func (o *Conversation) HasReadAt() bool`

HasReadAt returns a boolean if a field has been set.

### GetAgentRequested

`func (o *Conversation) GetAgentRequested() bool`

GetAgentRequested returns the AgentRequested field if non-nil, zero value otherwise.

### GetAgentRequestedOk

`func (o *Conversation) GetAgentRequestedOk() (*bool, bool)`

GetAgentRequestedOk returns a tuple with the AgentRequested field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAgentRequested

`func (o *Conversation) SetAgentRequested(v bool)`

SetAgentRequested sets AgentRequested field to given value.


### GetAgentRequestedType

`func (o *Conversation) GetAgentRequestedType() string`

GetAgentRequestedType returns the AgentRequestedType field if non-nil, zero value otherwise.

### GetAgentRequestedTypeOk

`func (o *Conversation) GetAgentRequestedTypeOk() (*string, bool)`

GetAgentRequestedTypeOk returns a tuple with the AgentRequestedType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAgentRequestedType

`func (o *Conversation) SetAgentRequestedType(v string)`

SetAgentRequestedType sets AgentRequestedType field to given value.

### HasAgentRequestedType

`func (o *Conversation) HasAgentRequestedType() bool`

HasAgentRequestedType returns a boolean if a field has been set.

### GetAgentTaken

`func (o *Conversation) GetAgentTaken() bool`

GetAgentTaken returns the AgentTaken field if non-nil, zero value otherwise.

### GetAgentTakenOk

`func (o *Conversation) GetAgentTakenOk() (*bool, bool)`

GetAgentTakenOk returns a tuple with the AgentTaken field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAgentTaken

`func (o *Conversation) SetAgentTaken(v bool)`

SetAgentTaken sets AgentTaken field to given value.


### GetNewMessages

`func (o *Conversation) GetNewMessages() int32`

GetNewMessages returns the NewMessages field if non-nil, zero value otherwise.

### GetNewMessagesOk

`func (o *Conversation) GetNewMessagesOk() (*int32, bool)`

GetNewMessagesOk returns a tuple with the NewMessages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewMessages

`func (o *Conversation) SetNewMessages(v int32)`

SetNewMessages sets NewMessages field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


