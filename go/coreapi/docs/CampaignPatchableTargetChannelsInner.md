# CampaignPatchableTargetChannelsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelId** | **string** |  | 
**InteractionId** | **string** |  | 
**Main** | Pointer to **bool** |  | [optional] 

## Methods

### NewCampaignPatchableTargetChannelsInner

`func NewCampaignPatchableTargetChannelsInner(channelId string, interactionId string, ) *CampaignPatchableTargetChannelsInner`

NewCampaignPatchableTargetChannelsInner instantiates a new CampaignPatchableTargetChannelsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignPatchableTargetChannelsInnerWithDefaults

`func NewCampaignPatchableTargetChannelsInnerWithDefaults() *CampaignPatchableTargetChannelsInner`

NewCampaignPatchableTargetChannelsInnerWithDefaults instantiates a new CampaignPatchableTargetChannelsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetChannelId

`func (o *CampaignPatchableTargetChannelsInner) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CampaignPatchableTargetChannelsInner) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CampaignPatchableTargetChannelsInner) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetInteractionId

`func (o *CampaignPatchableTargetChannelsInner) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *CampaignPatchableTargetChannelsInner) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *CampaignPatchableTargetChannelsInner) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.


### GetMain

`func (o *CampaignPatchableTargetChannelsInner) GetMain() bool`

GetMain returns the Main field if non-nil, zero value otherwise.

### GetMainOk

`func (o *CampaignPatchableTargetChannelsInner) GetMainOk() (*bool, bool)`

GetMainOk returns a tuple with the Main field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMain

`func (o *CampaignPatchableTargetChannelsInner) SetMain(v bool)`

SetMain sets Main field to given value.

### HasMain

`func (o *CampaignPatchableTargetChannelsInner) HasMain() bool`

HasMain returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


