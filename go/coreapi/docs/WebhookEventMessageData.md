# WebhookEventMessageData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Location** | Pointer to [**WebhookEventMessageDataLocation**](WebhookEventMessageDataLocation.md) |  | [optional] 
**Media** | Pointer to [**[]WebhookEventMedia**](WebhookEventMedia.md) |  | [optional] 
**Error** | Pointer to [**WebhookEventMessageDataError**](WebhookEventMessageDataError.md) |  | [optional] 
**Contacts** | Pointer to [**[]MessageContact**](MessageContact.md) |  | [optional] 
**Template** | Pointer to [**WebhookEventMessageDataTemplate**](WebhookEventMessageDataTemplate.md) |  | [optional] 
**Header** | Pointer to [**MessageHeader**](MessageHeader.md) |  | [optional] 
**Footer** | Pointer to [**MessageFooter**](MessageFooter.md) |  | [optional] 
**Buttons** | Pointer to [**[]MessageButton**](MessageButton.md) |  | [optional] 
**Items** | Pointer to [**List**](List.md) |  | [optional] 

## Methods

### NewWebhookEventMessageData

`func NewWebhookEventMessageData() *WebhookEventMessageData`

NewWebhookEventMessageData instantiates a new WebhookEventMessageData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookEventMessageDataWithDefaults

`func NewWebhookEventMessageDataWithDefaults() *WebhookEventMessageData`

NewWebhookEventMessageDataWithDefaults instantiates a new WebhookEventMessageData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLocation

`func (o *WebhookEventMessageData) GetLocation() WebhookEventMessageDataLocation`

GetLocation returns the Location field if non-nil, zero value otherwise.

### GetLocationOk

`func (o *WebhookEventMessageData) GetLocationOk() (*WebhookEventMessageDataLocation, bool)`

GetLocationOk returns a tuple with the Location field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocation

`func (o *WebhookEventMessageData) SetLocation(v WebhookEventMessageDataLocation)`

SetLocation sets Location field to given value.

### HasLocation

`func (o *WebhookEventMessageData) HasLocation() bool`

HasLocation returns a boolean if a field has been set.

### GetMedia

`func (o *WebhookEventMessageData) GetMedia() []WebhookEventMedia`

GetMedia returns the Media field if non-nil, zero value otherwise.

### GetMediaOk

`func (o *WebhookEventMessageData) GetMediaOk() (*[]WebhookEventMedia, bool)`

GetMediaOk returns a tuple with the Media field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMedia

`func (o *WebhookEventMessageData) SetMedia(v []WebhookEventMedia)`

SetMedia sets Media field to given value.

### HasMedia

`func (o *WebhookEventMessageData) HasMedia() bool`

HasMedia returns a boolean if a field has been set.

### GetError

`func (o *WebhookEventMessageData) GetError() WebhookEventMessageDataError`

GetError returns the Error field if non-nil, zero value otherwise.

### GetErrorOk

`func (o *WebhookEventMessageData) GetErrorOk() (*WebhookEventMessageDataError, bool)`

GetErrorOk returns a tuple with the Error field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetError

`func (o *WebhookEventMessageData) SetError(v WebhookEventMessageDataError)`

SetError sets Error field to given value.

### HasError

`func (o *WebhookEventMessageData) HasError() bool`

HasError returns a boolean if a field has been set.

### GetContacts

`func (o *WebhookEventMessageData) GetContacts() []MessageContact`

GetContacts returns the Contacts field if non-nil, zero value otherwise.

### GetContactsOk

`func (o *WebhookEventMessageData) GetContactsOk() (*[]MessageContact, bool)`

GetContactsOk returns a tuple with the Contacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContacts

`func (o *WebhookEventMessageData) SetContacts(v []MessageContact)`

SetContacts sets Contacts field to given value.

### HasContacts

`func (o *WebhookEventMessageData) HasContacts() bool`

HasContacts returns a boolean if a field has been set.

### GetTemplate

`func (o *WebhookEventMessageData) GetTemplate() WebhookEventMessageDataTemplate`

GetTemplate returns the Template field if non-nil, zero value otherwise.

### GetTemplateOk

`func (o *WebhookEventMessageData) GetTemplateOk() (*WebhookEventMessageDataTemplate, bool)`

GetTemplateOk returns a tuple with the Template field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplate

`func (o *WebhookEventMessageData) SetTemplate(v WebhookEventMessageDataTemplate)`

SetTemplate sets Template field to given value.

### HasTemplate

`func (o *WebhookEventMessageData) HasTemplate() bool`

HasTemplate returns a boolean if a field has been set.

### GetHeader

`func (o *WebhookEventMessageData) GetHeader() MessageHeader`

GetHeader returns the Header field if non-nil, zero value otherwise.

### GetHeaderOk

`func (o *WebhookEventMessageData) GetHeaderOk() (*MessageHeader, bool)`

GetHeaderOk returns a tuple with the Header field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeader

`func (o *WebhookEventMessageData) SetHeader(v MessageHeader)`

SetHeader sets Header field to given value.

### HasHeader

`func (o *WebhookEventMessageData) HasHeader() bool`

HasHeader returns a boolean if a field has been set.

### GetFooter

`func (o *WebhookEventMessageData) GetFooter() MessageFooter`

GetFooter returns the Footer field if non-nil, zero value otherwise.

### GetFooterOk

`func (o *WebhookEventMessageData) GetFooterOk() (*MessageFooter, bool)`

GetFooterOk returns a tuple with the Footer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFooter

`func (o *WebhookEventMessageData) SetFooter(v MessageFooter)`

SetFooter sets Footer field to given value.

### HasFooter

`func (o *WebhookEventMessageData) HasFooter() bool`

HasFooter returns a boolean if a field has been set.

### GetButtons

`func (o *WebhookEventMessageData) GetButtons() []MessageButton`

GetButtons returns the Buttons field if non-nil, zero value otherwise.

### GetButtonsOk

`func (o *WebhookEventMessageData) GetButtonsOk() (*[]MessageButton, bool)`

GetButtonsOk returns a tuple with the Buttons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtons

`func (o *WebhookEventMessageData) SetButtons(v []MessageButton)`

SetButtons sets Buttons field to given value.

### HasButtons

`func (o *WebhookEventMessageData) HasButtons() bool`

HasButtons returns a boolean if a field has been set.

### GetItems

`func (o *WebhookEventMessageData) GetItems() List`

GetItems returns the Items field if non-nil, zero value otherwise.

### GetItemsOk

`func (o *WebhookEventMessageData) GetItemsOk() (*List, bool)`

GetItemsOk returns a tuple with the Items field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItems

`func (o *WebhookEventMessageData) SetItems(v List)`

SetItems sets Items field to given value.

### HasItems

`func (o *WebhookEventMessageData) HasItems() bool`

HasItems returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


