# CampaignCondition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Operator** | **string** |  | 
**DataType** | Pointer to **string** | required unless operator: or, and | [optional] 
**Left** | Pointer to [**ConditionExpression**](ConditionExpression.md) |  | [optional] 
**Right** | Pointer to [**ConditionExpression**](ConditionExpression.md) |  | [optional] 
**Conditions** | Pointer to [**[]CampaignCondition**](CampaignCondition.md) |  | [optional] 

## Methods

### NewCampaignCondition

`func NewCampaignCondition(operator string, ) *CampaignCondition`

NewCampaignCondition instantiates a new CampaignCondition object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignConditionWithDefaults

`func NewCampaignConditionWithDefaults() *CampaignCondition`

NewCampaignConditionWithDefaults instantiates a new CampaignCondition object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOperator

`func (o *CampaignCondition) GetOperator() string`

GetOperator returns the Operator field if non-nil, zero value otherwise.

### GetOperatorOk

`func (o *CampaignCondition) GetOperatorOk() (*string, bool)`

GetOperatorOk returns a tuple with the Operator field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOperator

`func (o *CampaignCondition) SetOperator(v string)`

SetOperator sets Operator field to given value.


### GetDataType

`func (o *CampaignCondition) GetDataType() string`

GetDataType returns the DataType field if non-nil, zero value otherwise.

### GetDataTypeOk

`func (o *CampaignCondition) GetDataTypeOk() (*string, bool)`

GetDataTypeOk returns a tuple with the DataType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataType

`func (o *CampaignCondition) SetDataType(v string)`

SetDataType sets DataType field to given value.

### HasDataType

`func (o *CampaignCondition) HasDataType() bool`

HasDataType returns a boolean if a field has been set.

### GetLeft

`func (o *CampaignCondition) GetLeft() ConditionExpression`

GetLeft returns the Left field if non-nil, zero value otherwise.

### GetLeftOk

`func (o *CampaignCondition) GetLeftOk() (*ConditionExpression, bool)`

GetLeftOk returns a tuple with the Left field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLeft

`func (o *CampaignCondition) SetLeft(v ConditionExpression)`

SetLeft sets Left field to given value.

### HasLeft

`func (o *CampaignCondition) HasLeft() bool`

HasLeft returns a boolean if a field has been set.

### GetRight

`func (o *CampaignCondition) GetRight() ConditionExpression`

GetRight returns the Right field if non-nil, zero value otherwise.

### GetRightOk

`func (o *CampaignCondition) GetRightOk() (*ConditionExpression, bool)`

GetRightOk returns a tuple with the Right field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRight

`func (o *CampaignCondition) SetRight(v ConditionExpression)`

SetRight sets Right field to given value.

### HasRight

`func (o *CampaignCondition) HasRight() bool`

HasRight returns a boolean if a field has been set.

### GetConditions

`func (o *CampaignCondition) GetConditions() []CampaignCondition`

GetConditions returns the Conditions field if non-nil, zero value otherwise.

### GetConditionsOk

`func (o *CampaignCondition) GetConditionsOk() (*[]CampaignCondition, bool)`

GetConditionsOk returns a tuple with the Conditions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConditions

`func (o *CampaignCondition) SetConditions(v []CampaignCondition)`

SetConditions sets Conditions field to given value.

### HasConditions

`func (o *CampaignCondition) HasConditions() bool`

HasConditions returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


