# TemplateGroupedTemplatesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Status** | **string** |  | 
**Language** | **string** |  | 
**RejectedReason** | Pointer to **string** |  | [optional] 

## Methods

### NewTemplateGroupedTemplatesInner

`func NewTemplateGroupedTemplatesInner(id string, status string, language string, ) *TemplateGroupedTemplatesInner`

NewTemplateGroupedTemplatesInner instantiates a new TemplateGroupedTemplatesInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTemplateGroupedTemplatesInnerWithDefaults

`func NewTemplateGroupedTemplatesInnerWithDefaults() *TemplateGroupedTemplatesInner`

NewTemplateGroupedTemplatesInnerWithDefaults instantiates a new TemplateGroupedTemplatesInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *TemplateGroupedTemplatesInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *TemplateGroupedTemplatesInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *TemplateGroupedTemplatesInner) SetId(v string)`

SetId sets Id field to given value.


### GetStatus

`func (o *TemplateGroupedTemplatesInner) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *TemplateGroupedTemplatesInner) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *TemplateGroupedTemplatesInner) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetLanguage

`func (o *TemplateGroupedTemplatesInner) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *TemplateGroupedTemplatesInner) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *TemplateGroupedTemplatesInner) SetLanguage(v string)`

SetLanguage sets Language field to given value.


### GetRejectedReason

`func (o *TemplateGroupedTemplatesInner) GetRejectedReason() string`

GetRejectedReason returns the RejectedReason field if non-nil, zero value otherwise.

### GetRejectedReasonOk

`func (o *TemplateGroupedTemplatesInner) GetRejectedReasonOk() (*string, bool)`

GetRejectedReasonOk returns a tuple with the RejectedReason field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRejectedReason

`func (o *TemplateGroupedTemplatesInner) SetRejectedReason(v string)`

SetRejectedReason sets RejectedReason field to given value.

### HasRejectedReason

`func (o *TemplateGroupedTemplatesInner) HasRejectedReason() bool`

HasRejectedReason returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


