# MediaCenterTranslation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Language** | **string** |  | 
**Text** | **string** |  | 

## Methods

### NewMediaCenterTranslation

`func NewMediaCenterTranslation(language string, text string, ) *MediaCenterTranslation`

NewMediaCenterTranslation instantiates a new MediaCenterTranslation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMediaCenterTranslationWithDefaults

`func NewMediaCenterTranslationWithDefaults() *MediaCenterTranslation`

NewMediaCenterTranslationWithDefaults instantiates a new MediaCenterTranslation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLanguage

`func (o *MediaCenterTranslation) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *MediaCenterTranslation) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *MediaCenterTranslation) SetLanguage(v string)`

SetLanguage sets Language field to given value.


### GetText

`func (o *MediaCenterTranslation) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *MediaCenterTranslation) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *MediaCenterTranslation) SetText(v string)`

SetText sets Text field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


