# TriggerTestRunRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContactId** | **string** |  | 
**ConversationIds** | Pointer to **[]string** |  | [optional] 
**Mode** | Pointer to **string** |  | [optional] 
**Messengers** | Pointer to **[]string** |  | [optional] 

## Methods

### NewTriggerTestRunRequest

`func NewTriggerTestRunRequest(contactId string, ) *TriggerTestRunRequest`

NewTriggerTestRunRequest instantiates a new TriggerTestRunRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTriggerTestRunRequestWithDefaults

`func NewTriggerTestRunRequestWithDefaults() *TriggerTestRunRequest`

NewTriggerTestRunRequestWithDefaults instantiates a new TriggerTestRunRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContactId

`func (o *TriggerTestRunRequest) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *TriggerTestRunRequest) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *TriggerTestRunRequest) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetConversationIds

`func (o *TriggerTestRunRequest) GetConversationIds() []string`

GetConversationIds returns the ConversationIds field if non-nil, zero value otherwise.

### GetConversationIdsOk

`func (o *TriggerTestRunRequest) GetConversationIdsOk() (*[]string, bool)`

GetConversationIdsOk returns a tuple with the ConversationIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationIds

`func (o *TriggerTestRunRequest) SetConversationIds(v []string)`

SetConversationIds sets ConversationIds field to given value.

### HasConversationIds

`func (o *TriggerTestRunRequest) HasConversationIds() bool`

HasConversationIds returns a boolean if a field has been set.

### GetMode

`func (o *TriggerTestRunRequest) GetMode() string`

GetMode returns the Mode field if non-nil, zero value otherwise.

### GetModeOk

`func (o *TriggerTestRunRequest) GetModeOk() (*string, bool)`

GetModeOk returns a tuple with the Mode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMode

`func (o *TriggerTestRunRequest) SetMode(v string)`

SetMode sets Mode field to given value.

### HasMode

`func (o *TriggerTestRunRequest) HasMode() bool`

HasMode returns a boolean if a field has been set.

### GetMessengers

`func (o *TriggerTestRunRequest) GetMessengers() []string`

GetMessengers returns the Messengers field if non-nil, zero value otherwise.

### GetMessengersOk

`func (o *TriggerTestRunRequest) GetMessengersOk() (*[]string, bool)`

GetMessengersOk returns a tuple with the Messengers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengers

`func (o *TriggerTestRunRequest) SetMessengers(v []string)`

SetMessengers sets Messengers field to given value.

### HasMessengers

`func (o *TriggerTestRunRequest) HasMessengers() bool`

HasMessengers returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


