# UserMessengerCreateable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MessengerId** | **string** |  | 
**MessengerType** | **string** |  | 
**Purpose** | **string** |  | 
**Password** | Pointer to **string** |  | [optional] 

## Methods

### NewUserMessengerCreateable

`func NewUserMessengerCreateable(messengerId string, messengerType string, purpose string, ) *UserMessengerCreateable`

NewUserMessengerCreateable instantiates a new UserMessengerCreateable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUserMessengerCreateableWithDefaults

`func NewUserMessengerCreateableWithDefaults() *UserMessengerCreateable`

NewUserMessengerCreateableWithDefaults instantiates a new UserMessengerCreateable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessengerId

`func (o *UserMessengerCreateable) GetMessengerId() string`

GetMessengerId returns the MessengerId field if non-nil, zero value otherwise.

### GetMessengerIdOk

`func (o *UserMessengerCreateable) GetMessengerIdOk() (*string, bool)`

GetMessengerIdOk returns a tuple with the MessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerId

`func (o *UserMessengerCreateable) SetMessengerId(v string)`

SetMessengerId sets MessengerId field to given value.


### GetMessengerType

`func (o *UserMessengerCreateable) GetMessengerType() string`

GetMessengerType returns the MessengerType field if non-nil, zero value otherwise.

### GetMessengerTypeOk

`func (o *UserMessengerCreateable) GetMessengerTypeOk() (*string, bool)`

GetMessengerTypeOk returns a tuple with the MessengerType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerType

`func (o *UserMessengerCreateable) SetMessengerType(v string)`

SetMessengerType sets MessengerType field to given value.


### GetPurpose

`func (o *UserMessengerCreateable) GetPurpose() string`

GetPurpose returns the Purpose field if non-nil, zero value otherwise.

### GetPurposeOk

`func (o *UserMessengerCreateable) GetPurposeOk() (*string, bool)`

GetPurposeOk returns a tuple with the Purpose field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPurpose

`func (o *UserMessengerCreateable) SetPurpose(v string)`

SetPurpose sets Purpose field to given value.


### GetPassword

`func (o *UserMessengerCreateable) GetPassword() string`

GetPassword returns the Password field if non-nil, zero value otherwise.

### GetPasswordOk

`func (o *UserMessengerCreateable) GetPasswordOk() (*string, bool)`

GetPasswordOk returns a tuple with the Password field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPassword

`func (o *UserMessengerCreateable) SetPassword(v string)`

SetPassword sets Password field to given value.

### HasPassword

`func (o *UserMessengerCreateable) HasPassword() bool`

HasPassword returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


