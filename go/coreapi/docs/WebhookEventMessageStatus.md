# WebhookEventMessageStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Status** | [**MessageStatus**](MessageStatus.md) |  | 

## Methods

### NewWebhookEventMessageStatus

`func NewWebhookEventMessageStatus(id string, status MessageStatus, ) *WebhookEventMessageStatus`

NewWebhookEventMessageStatus instantiates a new WebhookEventMessageStatus object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookEventMessageStatusWithDefaults

`func NewWebhookEventMessageStatusWithDefaults() *WebhookEventMessageStatus`

NewWebhookEventMessageStatusWithDefaults instantiates a new WebhookEventMessageStatus object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WebhookEventMessageStatus) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WebhookEventMessageStatus) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WebhookEventMessageStatus) SetId(v string)`

SetId sets Id field to given value.


### GetStatus

`func (o *WebhookEventMessageStatus) GetStatus() MessageStatus`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *WebhookEventMessageStatus) GetStatusOk() (*MessageStatus, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *WebhookEventMessageStatus) SetStatus(v MessageStatus)`

SetStatus sets Status field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


