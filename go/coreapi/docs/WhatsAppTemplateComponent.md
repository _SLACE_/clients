# WhatsAppTemplateComponent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | [**WhatsAppTemplateComponentType**](WhatsAppTemplateComponentType.md) |  | 
**Format** | Pointer to [**WhatsAppTemplateHeaderFormat**](WhatsAppTemplateHeaderFormat.md) |  | [optional] 
**Text** | Pointer to **string** |  | [optional] 
**Example** | Pointer to [**WhatsAppTemplateComponentExample**](WhatsAppTemplateComponentExample.md) |  | [optional] 
**Buttons** | Pointer to [**[]WhatsAppTemplateButton**](WhatsAppTemplateButton.md) |  | [optional] 
**Cards** | Pointer to [**[]WhatsAppTemplateCard**](WhatsAppTemplateCard.md) |  | [optional] 

## Methods

### NewWhatsAppTemplateComponent

`func NewWhatsAppTemplateComponent(type_ WhatsAppTemplateComponentType, ) *WhatsAppTemplateComponent`

NewWhatsAppTemplateComponent instantiates a new WhatsAppTemplateComponent object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateComponentWithDefaults

`func NewWhatsAppTemplateComponentWithDefaults() *WhatsAppTemplateComponent`

NewWhatsAppTemplateComponentWithDefaults instantiates a new WhatsAppTemplateComponent object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *WhatsAppTemplateComponent) GetType() WhatsAppTemplateComponentType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WhatsAppTemplateComponent) GetTypeOk() (*WhatsAppTemplateComponentType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WhatsAppTemplateComponent) SetType(v WhatsAppTemplateComponentType)`

SetType sets Type field to given value.


### GetFormat

`func (o *WhatsAppTemplateComponent) GetFormat() WhatsAppTemplateHeaderFormat`

GetFormat returns the Format field if non-nil, zero value otherwise.

### GetFormatOk

`func (o *WhatsAppTemplateComponent) GetFormatOk() (*WhatsAppTemplateHeaderFormat, bool)`

GetFormatOk returns a tuple with the Format field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormat

`func (o *WhatsAppTemplateComponent) SetFormat(v WhatsAppTemplateHeaderFormat)`

SetFormat sets Format field to given value.

### HasFormat

`func (o *WhatsAppTemplateComponent) HasFormat() bool`

HasFormat returns a boolean if a field has been set.

### GetText

`func (o *WhatsAppTemplateComponent) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *WhatsAppTemplateComponent) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *WhatsAppTemplateComponent) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *WhatsAppTemplateComponent) HasText() bool`

HasText returns a boolean if a field has been set.

### GetExample

`func (o *WhatsAppTemplateComponent) GetExample() WhatsAppTemplateComponentExample`

GetExample returns the Example field if non-nil, zero value otherwise.

### GetExampleOk

`func (o *WhatsAppTemplateComponent) GetExampleOk() (*WhatsAppTemplateComponentExample, bool)`

GetExampleOk returns a tuple with the Example field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExample

`func (o *WhatsAppTemplateComponent) SetExample(v WhatsAppTemplateComponentExample)`

SetExample sets Example field to given value.

### HasExample

`func (o *WhatsAppTemplateComponent) HasExample() bool`

HasExample returns a boolean if a field has been set.

### GetButtons

`func (o *WhatsAppTemplateComponent) GetButtons() []WhatsAppTemplateButton`

GetButtons returns the Buttons field if non-nil, zero value otherwise.

### GetButtonsOk

`func (o *WhatsAppTemplateComponent) GetButtonsOk() (*[]WhatsAppTemplateButton, bool)`

GetButtonsOk returns a tuple with the Buttons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtons

`func (o *WhatsAppTemplateComponent) SetButtons(v []WhatsAppTemplateButton)`

SetButtons sets Buttons field to given value.

### HasButtons

`func (o *WhatsAppTemplateComponent) HasButtons() bool`

HasButtons returns a boolean if a field has been set.

### GetCards

`func (o *WhatsAppTemplateComponent) GetCards() []WhatsAppTemplateCard`

GetCards returns the Cards field if non-nil, zero value otherwise.

### GetCardsOk

`func (o *WhatsAppTemplateComponent) GetCardsOk() (*[]WhatsAppTemplateCard, bool)`

GetCardsOk returns a tuple with the Cards field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCards

`func (o *WhatsAppTemplateComponent) SetCards(v []WhatsAppTemplateCard)`

SetCards sets Cards field to given value.

### HasCards

`func (o *WhatsAppTemplateComponent) HasCards() bool`

HasCards returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


