# ContactStrictAttributeValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Methods

### NewContactStrictAttributeValue

`func NewContactStrictAttributeValue() *ContactStrictAttributeValue`

NewContactStrictAttributeValue instantiates a new ContactStrictAttributeValue object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactStrictAttributeValueWithDefaults

`func NewContactStrictAttributeValueWithDefaults() *ContactStrictAttributeValue`

NewContactStrictAttributeValueWithDefaults instantiates a new ContactStrictAttributeValue object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


