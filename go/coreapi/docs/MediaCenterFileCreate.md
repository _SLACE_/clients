# MediaCenterFileCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Key** | **string** |  | 
**OrganizationWide** | Pointer to **bool** |  | [optional] 
**Public** | Pointer to **bool** |  | [optional] 

## Methods

### NewMediaCenterFileCreate

`func NewMediaCenterFileCreate(key string, ) *MediaCenterFileCreate`

NewMediaCenterFileCreate instantiates a new MediaCenterFileCreate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMediaCenterFileCreateWithDefaults

`func NewMediaCenterFileCreateWithDefaults() *MediaCenterFileCreate`

NewMediaCenterFileCreateWithDefaults instantiates a new MediaCenterFileCreate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetKey

`func (o *MediaCenterFileCreate) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *MediaCenterFileCreate) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *MediaCenterFileCreate) SetKey(v string)`

SetKey sets Key field to given value.


### GetOrganizationWide

`func (o *MediaCenterFileCreate) GetOrganizationWide() bool`

GetOrganizationWide returns the OrganizationWide field if non-nil, zero value otherwise.

### GetOrganizationWideOk

`func (o *MediaCenterFileCreate) GetOrganizationWideOk() (*bool, bool)`

GetOrganizationWideOk returns a tuple with the OrganizationWide field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationWide

`func (o *MediaCenterFileCreate) SetOrganizationWide(v bool)`

SetOrganizationWide sets OrganizationWide field to given value.

### HasOrganizationWide

`func (o *MediaCenterFileCreate) HasOrganizationWide() bool`

HasOrganizationWide returns a boolean if a field has been set.

### GetPublic

`func (o *MediaCenterFileCreate) GetPublic() bool`

GetPublic returns the Public field if non-nil, zero value otherwise.

### GetPublicOk

`func (o *MediaCenterFileCreate) GetPublicOk() (*bool, bool)`

GetPublicOk returns a tuple with the Public field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPublic

`func (o *MediaCenterFileCreate) SetPublic(v bool)`

SetPublic sets Public field to given value.

### HasPublic

`func (o *MediaCenterFileCreate) HasPublic() bool`

HasPublic returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


