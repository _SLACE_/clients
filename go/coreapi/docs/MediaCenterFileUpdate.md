# MediaCenterFileUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OrganizationWide** | Pointer to **bool** |  | [optional] 
**Public** | Pointer to **bool** |  | [optional] 

## Methods

### NewMediaCenterFileUpdate

`func NewMediaCenterFileUpdate() *MediaCenterFileUpdate`

NewMediaCenterFileUpdate instantiates a new MediaCenterFileUpdate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMediaCenterFileUpdateWithDefaults

`func NewMediaCenterFileUpdateWithDefaults() *MediaCenterFileUpdate`

NewMediaCenterFileUpdateWithDefaults instantiates a new MediaCenterFileUpdate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOrganizationWide

`func (o *MediaCenterFileUpdate) GetOrganizationWide() bool`

GetOrganizationWide returns the OrganizationWide field if non-nil, zero value otherwise.

### GetOrganizationWideOk

`func (o *MediaCenterFileUpdate) GetOrganizationWideOk() (*bool, bool)`

GetOrganizationWideOk returns a tuple with the OrganizationWide field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationWide

`func (o *MediaCenterFileUpdate) SetOrganizationWide(v bool)`

SetOrganizationWide sets OrganizationWide field to given value.

### HasOrganizationWide

`func (o *MediaCenterFileUpdate) HasOrganizationWide() bool`

HasOrganizationWide returns a boolean if a field has been set.

### GetPublic

`func (o *MediaCenterFileUpdate) GetPublic() bool`

GetPublic returns the Public field if non-nil, zero value otherwise.

### GetPublicOk

`func (o *MediaCenterFileUpdate) GetPublicOk() (*bool, bool)`

GetPublicOk returns a tuple with the Public field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPublic

`func (o *MediaCenterFileUpdate) SetPublic(v bool)`

SetPublic sets Public field to given value.

### HasPublic

`func (o *MediaCenterFileUpdate) HasPublic() bool`

HasPublic returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


