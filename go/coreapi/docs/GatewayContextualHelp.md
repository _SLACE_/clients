# GatewayContextualHelp

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Var360Status** | Pointer to **string** |  | [optional] [default to "new_client"]
**Var360ClientId** | Pointer to **string** |  | [optional] 
**Var360ExternalId** | Pointer to **string** |  | [optional] 
**Var360Namespace** | Pointer to **string** |  | [optional] 
**Maid** | Pointer to **string** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**Email** | Pointer to **string** |  | [optional] 

## Methods

### NewGatewayContextualHelp

`func NewGatewayContextualHelp() *GatewayContextualHelp`

NewGatewayContextualHelp instantiates a new GatewayContextualHelp object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGatewayContextualHelpWithDefaults

`func NewGatewayContextualHelpWithDefaults() *GatewayContextualHelp`

NewGatewayContextualHelpWithDefaults instantiates a new GatewayContextualHelp object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVar360Status

`func (o *GatewayContextualHelp) GetVar360Status() string`

GetVar360Status returns the Var360Status field if non-nil, zero value otherwise.

### GetVar360StatusOk

`func (o *GatewayContextualHelp) GetVar360StatusOk() (*string, bool)`

GetVar360StatusOk returns a tuple with the Var360Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVar360Status

`func (o *GatewayContextualHelp) SetVar360Status(v string)`

SetVar360Status sets Var360Status field to given value.

### HasVar360Status

`func (o *GatewayContextualHelp) HasVar360Status() bool`

HasVar360Status returns a boolean if a field has been set.

### GetVar360ClientId

`func (o *GatewayContextualHelp) GetVar360ClientId() string`

GetVar360ClientId returns the Var360ClientId field if non-nil, zero value otherwise.

### GetVar360ClientIdOk

`func (o *GatewayContextualHelp) GetVar360ClientIdOk() (*string, bool)`

GetVar360ClientIdOk returns a tuple with the Var360ClientId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVar360ClientId

`func (o *GatewayContextualHelp) SetVar360ClientId(v string)`

SetVar360ClientId sets Var360ClientId field to given value.

### HasVar360ClientId

`func (o *GatewayContextualHelp) HasVar360ClientId() bool`

HasVar360ClientId returns a boolean if a field has been set.

### GetVar360ExternalId

`func (o *GatewayContextualHelp) GetVar360ExternalId() string`

GetVar360ExternalId returns the Var360ExternalId field if non-nil, zero value otherwise.

### GetVar360ExternalIdOk

`func (o *GatewayContextualHelp) GetVar360ExternalIdOk() (*string, bool)`

GetVar360ExternalIdOk returns a tuple with the Var360ExternalId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVar360ExternalId

`func (o *GatewayContextualHelp) SetVar360ExternalId(v string)`

SetVar360ExternalId sets Var360ExternalId field to given value.

### HasVar360ExternalId

`func (o *GatewayContextualHelp) HasVar360ExternalId() bool`

HasVar360ExternalId returns a boolean if a field has been set.

### GetVar360Namespace

`func (o *GatewayContextualHelp) GetVar360Namespace() string`

GetVar360Namespace returns the Var360Namespace field if non-nil, zero value otherwise.

### GetVar360NamespaceOk

`func (o *GatewayContextualHelp) GetVar360NamespaceOk() (*string, bool)`

GetVar360NamespaceOk returns a tuple with the Var360Namespace field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVar360Namespace

`func (o *GatewayContextualHelp) SetVar360Namespace(v string)`

SetVar360Namespace sets Var360Namespace field to given value.

### HasVar360Namespace

`func (o *GatewayContextualHelp) HasVar360Namespace() bool`

HasVar360Namespace returns a boolean if a field has been set.

### GetMaid

`func (o *GatewayContextualHelp) GetMaid() string`

GetMaid returns the Maid field if non-nil, zero value otherwise.

### GetMaidOk

`func (o *GatewayContextualHelp) GetMaidOk() (*string, bool)`

GetMaidOk returns a tuple with the Maid field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaid

`func (o *GatewayContextualHelp) SetMaid(v string)`

SetMaid sets Maid field to given value.

### HasMaid

`func (o *GatewayContextualHelp) HasMaid() bool`

HasMaid returns a boolean if a field has been set.

### GetName

`func (o *GatewayContextualHelp) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *GatewayContextualHelp) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *GatewayContextualHelp) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *GatewayContextualHelp) HasName() bool`

HasName returns a boolean if a field has been set.

### GetEmail

`func (o *GatewayContextualHelp) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *GatewayContextualHelp) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *GatewayContextualHelp) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *GatewayContextualHelp) HasEmail() bool`

HasEmail returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


