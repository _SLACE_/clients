# WebhookEventConversation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Labels** | Pointer to **[]string** |  | [optional] 
**Blocked** | **bool** |  | 
**BlockedInfo** | Pointer to **string** |  | [optional] 
**CustomData** | Pointer to [**WebhookEventConversationCustomData**](WebhookEventConversationCustomData.md) |  | [optional] 

## Methods

### NewWebhookEventConversation

`func NewWebhookEventConversation(id string, blocked bool, ) *WebhookEventConversation`

NewWebhookEventConversation instantiates a new WebhookEventConversation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookEventConversationWithDefaults

`func NewWebhookEventConversationWithDefaults() *WebhookEventConversation`

NewWebhookEventConversationWithDefaults instantiates a new WebhookEventConversation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WebhookEventConversation) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WebhookEventConversation) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WebhookEventConversation) SetId(v string)`

SetId sets Id field to given value.


### GetLabels

`func (o *WebhookEventConversation) GetLabels() []string`

GetLabels returns the Labels field if non-nil, zero value otherwise.

### GetLabelsOk

`func (o *WebhookEventConversation) GetLabelsOk() (*[]string, bool)`

GetLabelsOk returns a tuple with the Labels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabels

`func (o *WebhookEventConversation) SetLabels(v []string)`

SetLabels sets Labels field to given value.

### HasLabels

`func (o *WebhookEventConversation) HasLabels() bool`

HasLabels returns a boolean if a field has been set.

### GetBlocked

`func (o *WebhookEventConversation) GetBlocked() bool`

GetBlocked returns the Blocked field if non-nil, zero value otherwise.

### GetBlockedOk

`func (o *WebhookEventConversation) GetBlockedOk() (*bool, bool)`

GetBlockedOk returns a tuple with the Blocked field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlocked

`func (o *WebhookEventConversation) SetBlocked(v bool)`

SetBlocked sets Blocked field to given value.


### GetBlockedInfo

`func (o *WebhookEventConversation) GetBlockedInfo() string`

GetBlockedInfo returns the BlockedInfo field if non-nil, zero value otherwise.

### GetBlockedInfoOk

`func (o *WebhookEventConversation) GetBlockedInfoOk() (*string, bool)`

GetBlockedInfoOk returns a tuple with the BlockedInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockedInfo

`func (o *WebhookEventConversation) SetBlockedInfo(v string)`

SetBlockedInfo sets BlockedInfo field to given value.

### HasBlockedInfo

`func (o *WebhookEventConversation) HasBlockedInfo() bool`

HasBlockedInfo returns a boolean if a field has been set.

### GetCustomData

`func (o *WebhookEventConversation) GetCustomData() WebhookEventConversationCustomData`

GetCustomData returns the CustomData field if non-nil, zero value otherwise.

### GetCustomDataOk

`func (o *WebhookEventConversation) GetCustomDataOk() (*WebhookEventConversationCustomData, bool)`

GetCustomDataOk returns a tuple with the CustomData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomData

`func (o *WebhookEventConversation) SetCustomData(v WebhookEventConversationCustomData)`

SetCustomData sets CustomData field to given value.

### HasCustomData

`func (o *WebhookEventConversation) HasCustomData() bool`

HasCustomData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


