# PostMeEntrypointsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Purpose** | Pointer to **string** |  | [optional] 

## Methods

### NewPostMeEntrypointsRequest

`func NewPostMeEntrypointsRequest() *PostMeEntrypointsRequest`

NewPostMeEntrypointsRequest instantiates a new PostMeEntrypointsRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPostMeEntrypointsRequestWithDefaults

`func NewPostMeEntrypointsRequestWithDefaults() *PostMeEntrypointsRequest`

NewPostMeEntrypointsRequestWithDefaults instantiates a new PostMeEntrypointsRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPurpose

`func (o *PostMeEntrypointsRequest) GetPurpose() string`

GetPurpose returns the Purpose field if non-nil, zero value otherwise.

### GetPurposeOk

`func (o *PostMeEntrypointsRequest) GetPurposeOk() (*string, bool)`

GetPurposeOk returns a tuple with the Purpose field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPurpose

`func (o *PostMeEntrypointsRequest) SetPurpose(v string)`

SetPurpose sets Purpose field to given value.

### HasPurpose

`func (o *PostMeEntrypointsRequest) HasPurpose() bool`

HasPurpose returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


