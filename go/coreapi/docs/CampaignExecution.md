# CampaignExecution

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**CampaignId** | Pointer to **string** |  | [optional] 
**Status** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**ContactId** | Pointer to **string** |  | [optional] 

## Methods

### NewCampaignExecution

`func NewCampaignExecution() *CampaignExecution`

NewCampaignExecution instantiates a new CampaignExecution object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignExecutionWithDefaults

`func NewCampaignExecutionWithDefaults() *CampaignExecution`

NewCampaignExecutionWithDefaults instantiates a new CampaignExecution object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CampaignExecution) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CampaignExecution) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CampaignExecution) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *CampaignExecution) HasId() bool`

HasId returns a boolean if a field has been set.

### GetCampaignId

`func (o *CampaignExecution) GetCampaignId() string`

GetCampaignId returns the CampaignId field if non-nil, zero value otherwise.

### GetCampaignIdOk

`func (o *CampaignExecution) GetCampaignIdOk() (*string, bool)`

GetCampaignIdOk returns a tuple with the CampaignId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCampaignId

`func (o *CampaignExecution) SetCampaignId(v string)`

SetCampaignId sets CampaignId field to given value.

### HasCampaignId

`func (o *CampaignExecution) HasCampaignId() bool`

HasCampaignId returns a boolean if a field has been set.

### GetStatus

`func (o *CampaignExecution) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *CampaignExecution) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *CampaignExecution) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *CampaignExecution) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetChannelId

`func (o *CampaignExecution) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CampaignExecution) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CampaignExecution) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *CampaignExecution) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetContactId

`func (o *CampaignExecution) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *CampaignExecution) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *CampaignExecution) SetContactId(v string)`

SetContactId sets ContactId field to given value.

### HasContactId

`func (o *CampaignExecution) HasContactId() bool`

HasContactId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


