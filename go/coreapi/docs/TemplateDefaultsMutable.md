# TemplateDefaultsMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WhatsappDefaults** | Pointer to [**WhatsAppTemplateDefaults**](WhatsAppTemplateDefaults.md) |  | [optional] 
**Visibility** | Pointer to [**TemplateVisibility**](TemplateVisibility.md) |  | [optional] 

## Methods

### NewTemplateDefaultsMutable

`func NewTemplateDefaultsMutable() *TemplateDefaultsMutable`

NewTemplateDefaultsMutable instantiates a new TemplateDefaultsMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTemplateDefaultsMutableWithDefaults

`func NewTemplateDefaultsMutableWithDefaults() *TemplateDefaultsMutable`

NewTemplateDefaultsMutableWithDefaults instantiates a new TemplateDefaultsMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWhatsappDefaults

`func (o *TemplateDefaultsMutable) GetWhatsappDefaults() WhatsAppTemplateDefaults`

GetWhatsappDefaults returns the WhatsappDefaults field if non-nil, zero value otherwise.

### GetWhatsappDefaultsOk

`func (o *TemplateDefaultsMutable) GetWhatsappDefaultsOk() (*WhatsAppTemplateDefaults, bool)`

GetWhatsappDefaultsOk returns a tuple with the WhatsappDefaults field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWhatsappDefaults

`func (o *TemplateDefaultsMutable) SetWhatsappDefaults(v WhatsAppTemplateDefaults)`

SetWhatsappDefaults sets WhatsappDefaults field to given value.

### HasWhatsappDefaults

`func (o *TemplateDefaultsMutable) HasWhatsappDefaults() bool`

HasWhatsappDefaults returns a boolean if a field has been set.

### GetVisibility

`func (o *TemplateDefaultsMutable) GetVisibility() TemplateVisibility`

GetVisibility returns the Visibility field if non-nil, zero value otherwise.

### GetVisibilityOk

`func (o *TemplateDefaultsMutable) GetVisibilityOk() (*TemplateVisibility, bool)`

GetVisibilityOk returns a tuple with the Visibility field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVisibility

`func (o *TemplateDefaultsMutable) SetVisibility(v TemplateVisibility)`

SetVisibility sets Visibility field to given value.

### HasVisibility

`func (o *TemplateDefaultsMutable) HasVisibility() bool`

HasVisibility returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


