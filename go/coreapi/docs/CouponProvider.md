# CouponProvider

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StampCards** | Pointer to [**[]StampCard**](StampCard.md) |  | [optional] 

## Methods

### NewCouponProvider

`func NewCouponProvider() *CouponProvider`

NewCouponProvider instantiates a new CouponProvider object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCouponProviderWithDefaults

`func NewCouponProviderWithDefaults() *CouponProvider`

NewCouponProviderWithDefaults instantiates a new CouponProvider object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStampCards

`func (o *CouponProvider) GetStampCards() []StampCard`

GetStampCards returns the StampCards field if non-nil, zero value otherwise.

### GetStampCardsOk

`func (o *CouponProvider) GetStampCardsOk() (*[]StampCard, bool)`

GetStampCardsOk returns a tuple with the StampCards field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStampCards

`func (o *CouponProvider) SetStampCards(v []StampCard)`

SetStampCards sets StampCards field to given value.

### HasStampCards

`func (o *CouponProvider) HasStampCards() bool`

HasStampCards returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


