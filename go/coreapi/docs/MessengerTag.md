# MessengerTag

## Enum


* `MESSENGER_TAG_NONE` (value: `"NONE"`)

* `MESSENGER_TAG_ACCOUNT_UPDATE` (value: `"ACCOUNT_UPDATE"`)

* `MESSENGER_TAG_CONFIRMED_EVENT_UPDATE` (value: `"CONFIRMED_EVENT_UPDATE"`)

* `MESSENGER_TAG_POST_PURCHASE_UPDATE` (value: `"POST_PURCHASE_UPDATE"`)

* `MESSENGER_TAG_HUMAN_AGENT` (value: `"HUMAN_AGENT"`)

* `MESSENGER_TAG_CUSTOMER_FEEDBACK` (value: `"CUSTOMER_FEEDBACK"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


