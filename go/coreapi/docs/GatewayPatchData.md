# GatewayPatchData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AuthKey** | Pointer to **string** |  | [optional] 
**AuthSecret** | Pointer to **string** |  | [optional] 

## Methods

### NewGatewayPatchData

`func NewGatewayPatchData() *GatewayPatchData`

NewGatewayPatchData instantiates a new GatewayPatchData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGatewayPatchDataWithDefaults

`func NewGatewayPatchDataWithDefaults() *GatewayPatchData`

NewGatewayPatchDataWithDefaults instantiates a new GatewayPatchData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAuthKey

`func (o *GatewayPatchData) GetAuthKey() string`

GetAuthKey returns the AuthKey field if non-nil, zero value otherwise.

### GetAuthKeyOk

`func (o *GatewayPatchData) GetAuthKeyOk() (*string, bool)`

GetAuthKeyOk returns a tuple with the AuthKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthKey

`func (o *GatewayPatchData) SetAuthKey(v string)`

SetAuthKey sets AuthKey field to given value.

### HasAuthKey

`func (o *GatewayPatchData) HasAuthKey() bool`

HasAuthKey returns a boolean if a field has been set.

### GetAuthSecret

`func (o *GatewayPatchData) GetAuthSecret() string`

GetAuthSecret returns the AuthSecret field if non-nil, zero value otherwise.

### GetAuthSecretOk

`func (o *GatewayPatchData) GetAuthSecretOk() (*string, bool)`

GetAuthSecretOk returns a tuple with the AuthSecret field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthSecret

`func (o *GatewayPatchData) SetAuthSecret(v string)`

SetAuthSecret sets AuthSecret field to given value.

### HasAuthSecret

`func (o *GatewayPatchData) HasAuthSecret() bool`

HasAuthSecret returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


