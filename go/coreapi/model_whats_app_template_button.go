/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the WhatsAppTemplateButton type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &WhatsAppTemplateButton{}

// WhatsAppTemplateButton struct for WhatsAppTemplateButton
type WhatsAppTemplateButton struct {
	Type WhatsAppTemplateButtonType `json:"type"`
	Text string `json:"text"`
	Url *string `json:"url,omitempty"`
	PhoneNumber *string `json:"phone_number,omitempty"`
	Example []string `json:"example,omitempty"`
}

// NewWhatsAppTemplateButton instantiates a new WhatsAppTemplateButton object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewWhatsAppTemplateButton(type_ WhatsAppTemplateButtonType, text string) *WhatsAppTemplateButton {
	this := WhatsAppTemplateButton{}
	this.Type = type_
	this.Text = text
	return &this
}

// NewWhatsAppTemplateButtonWithDefaults instantiates a new WhatsAppTemplateButton object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewWhatsAppTemplateButtonWithDefaults() *WhatsAppTemplateButton {
	this := WhatsAppTemplateButton{}
	return &this
}

// GetType returns the Type field value
func (o *WhatsAppTemplateButton) GetType() WhatsAppTemplateButtonType {
	if o == nil {
		var ret WhatsAppTemplateButtonType
		return ret
	}

	return o.Type
}

// GetTypeOk returns a tuple with the Type field value
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateButton) GetTypeOk() (*WhatsAppTemplateButtonType, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Type, true
}

// SetType sets field value
func (o *WhatsAppTemplateButton) SetType(v WhatsAppTemplateButtonType) {
	o.Type = v
}

// GetText returns the Text field value
func (o *WhatsAppTemplateButton) GetText() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Text
}

// GetTextOk returns a tuple with the Text field value
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateButton) GetTextOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Text, true
}

// SetText sets field value
func (o *WhatsAppTemplateButton) SetText(v string) {
	o.Text = v
}

// GetUrl returns the Url field value if set, zero value otherwise.
func (o *WhatsAppTemplateButton) GetUrl() string {
	if o == nil || IsNil(o.Url) {
		var ret string
		return ret
	}
	return *o.Url
}

// GetUrlOk returns a tuple with the Url field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateButton) GetUrlOk() (*string, bool) {
	if o == nil || IsNil(o.Url) {
		return nil, false
	}
	return o.Url, true
}

// HasUrl returns a boolean if a field has been set.
func (o *WhatsAppTemplateButton) HasUrl() bool {
	if o != nil && !IsNil(o.Url) {
		return true
	}

	return false
}

// SetUrl gets a reference to the given string and assigns it to the Url field.
func (o *WhatsAppTemplateButton) SetUrl(v string) {
	o.Url = &v
}

// GetPhoneNumber returns the PhoneNumber field value if set, zero value otherwise.
func (o *WhatsAppTemplateButton) GetPhoneNumber() string {
	if o == nil || IsNil(o.PhoneNumber) {
		var ret string
		return ret
	}
	return *o.PhoneNumber
}

// GetPhoneNumberOk returns a tuple with the PhoneNumber field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateButton) GetPhoneNumberOk() (*string, bool) {
	if o == nil || IsNil(o.PhoneNumber) {
		return nil, false
	}
	return o.PhoneNumber, true
}

// HasPhoneNumber returns a boolean if a field has been set.
func (o *WhatsAppTemplateButton) HasPhoneNumber() bool {
	if o != nil && !IsNil(o.PhoneNumber) {
		return true
	}

	return false
}

// SetPhoneNumber gets a reference to the given string and assigns it to the PhoneNumber field.
func (o *WhatsAppTemplateButton) SetPhoneNumber(v string) {
	o.PhoneNumber = &v
}

// GetExample returns the Example field value if set, zero value otherwise.
func (o *WhatsAppTemplateButton) GetExample() []string {
	if o == nil || IsNil(o.Example) {
		var ret []string
		return ret
	}
	return o.Example
}

// GetExampleOk returns a tuple with the Example field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateButton) GetExampleOk() ([]string, bool) {
	if o == nil || IsNil(o.Example) {
		return nil, false
	}
	return o.Example, true
}

// HasExample returns a boolean if a field has been set.
func (o *WhatsAppTemplateButton) HasExample() bool {
	if o != nil && !IsNil(o.Example) {
		return true
	}

	return false
}

// SetExample gets a reference to the given []string and assigns it to the Example field.
func (o *WhatsAppTemplateButton) SetExample(v []string) {
	o.Example = v
}

func (o WhatsAppTemplateButton) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o WhatsAppTemplateButton) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["type"] = o.Type
	toSerialize["text"] = o.Text
	if !IsNil(o.Url) {
		toSerialize["url"] = o.Url
	}
	if !IsNil(o.PhoneNumber) {
		toSerialize["phone_number"] = o.PhoneNumber
	}
	if !IsNil(o.Example) {
		toSerialize["example"] = o.Example
	}
	return toSerialize, nil
}

type NullableWhatsAppTemplateButton struct {
	value *WhatsAppTemplateButton
	isSet bool
}

func (v NullableWhatsAppTemplateButton) Get() *WhatsAppTemplateButton {
	return v.value
}

func (v *NullableWhatsAppTemplateButton) Set(val *WhatsAppTemplateButton) {
	v.value = val
	v.isSet = true
}

func (v NullableWhatsAppTemplateButton) IsSet() bool {
	return v.isSet
}

func (v *NullableWhatsAppTemplateButton) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableWhatsAppTemplateButton(val *WhatsAppTemplateButton) *NullableWhatsAppTemplateButton {
	return &NullableWhatsAppTemplateButton{value: val, isSet: true}
}

func (v NullableWhatsAppTemplateButton) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableWhatsAppTemplateButton) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


