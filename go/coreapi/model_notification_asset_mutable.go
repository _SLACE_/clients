/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the NotificationAssetMutable type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &NotificationAssetMutable{}

// NotificationAssetMutable struct for NotificationAssetMutable
type NotificationAssetMutable struct {
	Translations []NotificationAssetTranslation `json:"translations,omitempty"`
}

// NewNotificationAssetMutable instantiates a new NotificationAssetMutable object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewNotificationAssetMutable() *NotificationAssetMutable {
	this := NotificationAssetMutable{}
	return &this
}

// NewNotificationAssetMutableWithDefaults instantiates a new NotificationAssetMutable object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewNotificationAssetMutableWithDefaults() *NotificationAssetMutable {
	this := NotificationAssetMutable{}
	return &this
}

// GetTranslations returns the Translations field value if set, zero value otherwise.
func (o *NotificationAssetMutable) GetTranslations() []NotificationAssetTranslation {
	if o == nil || IsNil(o.Translations) {
		var ret []NotificationAssetTranslation
		return ret
	}
	return o.Translations
}

// GetTranslationsOk returns a tuple with the Translations field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NotificationAssetMutable) GetTranslationsOk() ([]NotificationAssetTranslation, bool) {
	if o == nil || IsNil(o.Translations) {
		return nil, false
	}
	return o.Translations, true
}

// HasTranslations returns a boolean if a field has been set.
func (o *NotificationAssetMutable) HasTranslations() bool {
	if o != nil && !IsNil(o.Translations) {
		return true
	}

	return false
}

// SetTranslations gets a reference to the given []NotificationAssetTranslation and assigns it to the Translations field.
func (o *NotificationAssetMutable) SetTranslations(v []NotificationAssetTranslation) {
	o.Translations = v
}

func (o NotificationAssetMutable) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o NotificationAssetMutable) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Translations) {
		toSerialize["translations"] = o.Translations
	}
	return toSerialize, nil
}

type NullableNotificationAssetMutable struct {
	value *NotificationAssetMutable
	isSet bool
}

func (v NullableNotificationAssetMutable) Get() *NotificationAssetMutable {
	return v.value
}

func (v *NullableNotificationAssetMutable) Set(val *NotificationAssetMutable) {
	v.value = val
	v.isSet = true
}

func (v NullableNotificationAssetMutable) IsSet() bool {
	return v.isSet
}

func (v *NullableNotificationAssetMutable) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableNotificationAssetMutable(val *NotificationAssetMutable) *NullableNotificationAssetMutable {
	return &NullableNotificationAssetMutable{value: val, isSet: true}
}

func (v NullableNotificationAssetMutable) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableNotificationAssetMutable) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


