/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the ObjectPermissionPermissions type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ObjectPermissionPermissions{}

// ObjectPermissionPermissions struct for ObjectPermissionPermissions
type ObjectPermissionPermissions struct {
	// Available permissions list:   PermissionDenied  Permission = 0    PermissionRead    Permission = 1     PermissionWrite   Permission = 2    PermissionExecute Permission = 4    PermissionDelete  Permission = 8   Permissions can be combined
	Contacts *int32 `json:"contacts,omitempty"`
	// Available permissions list:   PermissionDenied  Permission = 0    PermissionRead    Permission = 1     PermissionWrite   Permission = 2    PermissionExecute Permission = 4    PermissionDelete  Permission = 8   Permissions can be combined
	Conversations *int32 `json:"conversations,omitempty"`
	// Available permissions list:   PermissionDenied  Permission = 0    PermissionRead    Permission = 1     PermissionWrite   Permission = 2    PermissionExecute Permission = 4    PermissionDelete  Permission = 8   Permissions can be combined
	Events *int32 `json:"events,omitempty"`
	// Available permissions list:   PermissionDenied  Permission = 0    PermissionRead    Permission = 1     PermissionWrite   Permission = 2    PermissionExecute Permission = 4    PermissionDelete  Permission = 8   Permissions can be combined
	Campaigns *int32 `json:"campaigns,omitempty"`
	// Available permissions list:   PermissionDenied  Permission = 0    PermissionRead    Permission = 1     PermissionWrite   Permission = 2    PermissionExecute Permission = 4    PermissionDelete  Permission = 8   Permissions can be combined
	Locations *int32 `json:"locations,omitempty"`
	// Available permissions list:   PermissionDenied  Permission = 0    PermissionRead    Permission = 1     PermissionWrite   Permission = 2    PermissionExecute Permission = 4    PermissionDelete  Permission = 8   Permissions can be combined
	Users *int32 `json:"users,omitempty"`
	// Available permissions list:   PermissionDenied  Permission = 0    PermissionRead    Permission = 1     PermissionWrite   Permission = 2    PermissionExecute Permission = 4    PermissionDelete  Permission = 8   Permissions can be combined
	Templates *int32 `json:"templates,omitempty"`
	// Available permissions list:   PermissionDenied  Permission = 0    PermissionRead    Permission = 1     PermissionWrite   Permission = 2    PermissionExecute Permission = 4    PermissionDelete  Permission = 8   Permissions can be combined
	Organizations *int32 `json:"organizations,omitempty"`
	// Available permissions list:   PermissionDenied  Permission = 0    PermissionRead    Permission = 1     PermissionWrite   Permission = 2    PermissionExecute Permission = 4    PermissionDelete  Permission = 8   Permissions can be combined
	Gateways *int32 `json:"gateways,omitempty"`
	// Available permissions list:   PermissionDenied  Permission = 0    PermissionRead    Permission = 1     PermissionWrite   Permission = 2    PermissionExecute Permission = 4    PermissionDelete  Permission = 8   Permissions can be combined
	Channels *int32 `json:"channels,omitempty"`
}

// NewObjectPermissionPermissions instantiates a new ObjectPermissionPermissions object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewObjectPermissionPermissions() *ObjectPermissionPermissions {
	this := ObjectPermissionPermissions{}
	return &this
}

// NewObjectPermissionPermissionsWithDefaults instantiates a new ObjectPermissionPermissions object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewObjectPermissionPermissionsWithDefaults() *ObjectPermissionPermissions {
	this := ObjectPermissionPermissions{}
	return &this
}

// GetContacts returns the Contacts field value if set, zero value otherwise.
func (o *ObjectPermissionPermissions) GetContacts() int32 {
	if o == nil || IsNil(o.Contacts) {
		var ret int32
		return ret
	}
	return *o.Contacts
}

// GetContactsOk returns a tuple with the Contacts field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ObjectPermissionPermissions) GetContactsOk() (*int32, bool) {
	if o == nil || IsNil(o.Contacts) {
		return nil, false
	}
	return o.Contacts, true
}

// HasContacts returns a boolean if a field has been set.
func (o *ObjectPermissionPermissions) HasContacts() bool {
	if o != nil && !IsNil(o.Contacts) {
		return true
	}

	return false
}

// SetContacts gets a reference to the given int32 and assigns it to the Contacts field.
func (o *ObjectPermissionPermissions) SetContacts(v int32) {
	o.Contacts = &v
}

// GetConversations returns the Conversations field value if set, zero value otherwise.
func (o *ObjectPermissionPermissions) GetConversations() int32 {
	if o == nil || IsNil(o.Conversations) {
		var ret int32
		return ret
	}
	return *o.Conversations
}

// GetConversationsOk returns a tuple with the Conversations field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ObjectPermissionPermissions) GetConversationsOk() (*int32, bool) {
	if o == nil || IsNil(o.Conversations) {
		return nil, false
	}
	return o.Conversations, true
}

// HasConversations returns a boolean if a field has been set.
func (o *ObjectPermissionPermissions) HasConversations() bool {
	if o != nil && !IsNil(o.Conversations) {
		return true
	}

	return false
}

// SetConversations gets a reference to the given int32 and assigns it to the Conversations field.
func (o *ObjectPermissionPermissions) SetConversations(v int32) {
	o.Conversations = &v
}

// GetEvents returns the Events field value if set, zero value otherwise.
func (o *ObjectPermissionPermissions) GetEvents() int32 {
	if o == nil || IsNil(o.Events) {
		var ret int32
		return ret
	}
	return *o.Events
}

// GetEventsOk returns a tuple with the Events field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ObjectPermissionPermissions) GetEventsOk() (*int32, bool) {
	if o == nil || IsNil(o.Events) {
		return nil, false
	}
	return o.Events, true
}

// HasEvents returns a boolean if a field has been set.
func (o *ObjectPermissionPermissions) HasEvents() bool {
	if o != nil && !IsNil(o.Events) {
		return true
	}

	return false
}

// SetEvents gets a reference to the given int32 and assigns it to the Events field.
func (o *ObjectPermissionPermissions) SetEvents(v int32) {
	o.Events = &v
}

// GetCampaigns returns the Campaigns field value if set, zero value otherwise.
func (o *ObjectPermissionPermissions) GetCampaigns() int32 {
	if o == nil || IsNil(o.Campaigns) {
		var ret int32
		return ret
	}
	return *o.Campaigns
}

// GetCampaignsOk returns a tuple with the Campaigns field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ObjectPermissionPermissions) GetCampaignsOk() (*int32, bool) {
	if o == nil || IsNil(o.Campaigns) {
		return nil, false
	}
	return o.Campaigns, true
}

// HasCampaigns returns a boolean if a field has been set.
func (o *ObjectPermissionPermissions) HasCampaigns() bool {
	if o != nil && !IsNil(o.Campaigns) {
		return true
	}

	return false
}

// SetCampaigns gets a reference to the given int32 and assigns it to the Campaigns field.
func (o *ObjectPermissionPermissions) SetCampaigns(v int32) {
	o.Campaigns = &v
}

// GetLocations returns the Locations field value if set, zero value otherwise.
func (o *ObjectPermissionPermissions) GetLocations() int32 {
	if o == nil || IsNil(o.Locations) {
		var ret int32
		return ret
	}
	return *o.Locations
}

// GetLocationsOk returns a tuple with the Locations field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ObjectPermissionPermissions) GetLocationsOk() (*int32, bool) {
	if o == nil || IsNil(o.Locations) {
		return nil, false
	}
	return o.Locations, true
}

// HasLocations returns a boolean if a field has been set.
func (o *ObjectPermissionPermissions) HasLocations() bool {
	if o != nil && !IsNil(o.Locations) {
		return true
	}

	return false
}

// SetLocations gets a reference to the given int32 and assigns it to the Locations field.
func (o *ObjectPermissionPermissions) SetLocations(v int32) {
	o.Locations = &v
}

// GetUsers returns the Users field value if set, zero value otherwise.
func (o *ObjectPermissionPermissions) GetUsers() int32 {
	if o == nil || IsNil(o.Users) {
		var ret int32
		return ret
	}
	return *o.Users
}

// GetUsersOk returns a tuple with the Users field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ObjectPermissionPermissions) GetUsersOk() (*int32, bool) {
	if o == nil || IsNil(o.Users) {
		return nil, false
	}
	return o.Users, true
}

// HasUsers returns a boolean if a field has been set.
func (o *ObjectPermissionPermissions) HasUsers() bool {
	if o != nil && !IsNil(o.Users) {
		return true
	}

	return false
}

// SetUsers gets a reference to the given int32 and assigns it to the Users field.
func (o *ObjectPermissionPermissions) SetUsers(v int32) {
	o.Users = &v
}

// GetTemplates returns the Templates field value if set, zero value otherwise.
func (o *ObjectPermissionPermissions) GetTemplates() int32 {
	if o == nil || IsNil(o.Templates) {
		var ret int32
		return ret
	}
	return *o.Templates
}

// GetTemplatesOk returns a tuple with the Templates field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ObjectPermissionPermissions) GetTemplatesOk() (*int32, bool) {
	if o == nil || IsNil(o.Templates) {
		return nil, false
	}
	return o.Templates, true
}

// HasTemplates returns a boolean if a field has been set.
func (o *ObjectPermissionPermissions) HasTemplates() bool {
	if o != nil && !IsNil(o.Templates) {
		return true
	}

	return false
}

// SetTemplates gets a reference to the given int32 and assigns it to the Templates field.
func (o *ObjectPermissionPermissions) SetTemplates(v int32) {
	o.Templates = &v
}

// GetOrganizations returns the Organizations field value if set, zero value otherwise.
func (o *ObjectPermissionPermissions) GetOrganizations() int32 {
	if o == nil || IsNil(o.Organizations) {
		var ret int32
		return ret
	}
	return *o.Organizations
}

// GetOrganizationsOk returns a tuple with the Organizations field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ObjectPermissionPermissions) GetOrganizationsOk() (*int32, bool) {
	if o == nil || IsNil(o.Organizations) {
		return nil, false
	}
	return o.Organizations, true
}

// HasOrganizations returns a boolean if a field has been set.
func (o *ObjectPermissionPermissions) HasOrganizations() bool {
	if o != nil && !IsNil(o.Organizations) {
		return true
	}

	return false
}

// SetOrganizations gets a reference to the given int32 and assigns it to the Organizations field.
func (o *ObjectPermissionPermissions) SetOrganizations(v int32) {
	o.Organizations = &v
}

// GetGateways returns the Gateways field value if set, zero value otherwise.
func (o *ObjectPermissionPermissions) GetGateways() int32 {
	if o == nil || IsNil(o.Gateways) {
		var ret int32
		return ret
	}
	return *o.Gateways
}

// GetGatewaysOk returns a tuple with the Gateways field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ObjectPermissionPermissions) GetGatewaysOk() (*int32, bool) {
	if o == nil || IsNil(o.Gateways) {
		return nil, false
	}
	return o.Gateways, true
}

// HasGateways returns a boolean if a field has been set.
func (o *ObjectPermissionPermissions) HasGateways() bool {
	if o != nil && !IsNil(o.Gateways) {
		return true
	}

	return false
}

// SetGateways gets a reference to the given int32 and assigns it to the Gateways field.
func (o *ObjectPermissionPermissions) SetGateways(v int32) {
	o.Gateways = &v
}

// GetChannels returns the Channels field value if set, zero value otherwise.
func (o *ObjectPermissionPermissions) GetChannels() int32 {
	if o == nil || IsNil(o.Channels) {
		var ret int32
		return ret
	}
	return *o.Channels
}

// GetChannelsOk returns a tuple with the Channels field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ObjectPermissionPermissions) GetChannelsOk() (*int32, bool) {
	if o == nil || IsNil(o.Channels) {
		return nil, false
	}
	return o.Channels, true
}

// HasChannels returns a boolean if a field has been set.
func (o *ObjectPermissionPermissions) HasChannels() bool {
	if o != nil && !IsNil(o.Channels) {
		return true
	}

	return false
}

// SetChannels gets a reference to the given int32 and assigns it to the Channels field.
func (o *ObjectPermissionPermissions) SetChannels(v int32) {
	o.Channels = &v
}

func (o ObjectPermissionPermissions) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ObjectPermissionPermissions) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Contacts) {
		toSerialize["contacts"] = o.Contacts
	}
	if !IsNil(o.Conversations) {
		toSerialize["conversations"] = o.Conversations
	}
	if !IsNil(o.Events) {
		toSerialize["events"] = o.Events
	}
	if !IsNil(o.Campaigns) {
		toSerialize["campaigns"] = o.Campaigns
	}
	if !IsNil(o.Locations) {
		toSerialize["locations"] = o.Locations
	}
	if !IsNil(o.Users) {
		toSerialize["users"] = o.Users
	}
	if !IsNil(o.Templates) {
		toSerialize["templates"] = o.Templates
	}
	if !IsNil(o.Organizations) {
		toSerialize["organizations"] = o.Organizations
	}
	if !IsNil(o.Gateways) {
		toSerialize["gateways"] = o.Gateways
	}
	if !IsNil(o.Channels) {
		toSerialize["channels"] = o.Channels
	}
	return toSerialize, nil
}

type NullableObjectPermissionPermissions struct {
	value *ObjectPermissionPermissions
	isSet bool
}

func (v NullableObjectPermissionPermissions) Get() *ObjectPermissionPermissions {
	return v.value
}

func (v *NullableObjectPermissionPermissions) Set(val *ObjectPermissionPermissions) {
	v.value = val
	v.isSet = true
}

func (v NullableObjectPermissionPermissions) IsSet() bool {
	return v.isSet
}

func (v *NullableObjectPermissionPermissions) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableObjectPermissionPermissions(val *ObjectPermissionPermissions) *NullableObjectPermissionPermissions {
	return &NullableObjectPermissionPermissions{value: val, isSet: true}
}

func (v NullableObjectPermissionPermissions) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableObjectPermissionPermissions) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


