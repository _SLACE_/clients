/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the WebPushSubscriptionData type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &WebPushSubscriptionData{}

// WebPushSubscriptionData Standard subscription payload from browsers
type WebPushSubscriptionData struct {
	Endpoint string `json:"endpoint"`
	Keys WebPushSubscriptionDataKeys `json:"keys"`
}

// NewWebPushSubscriptionData instantiates a new WebPushSubscriptionData object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewWebPushSubscriptionData(endpoint string, keys WebPushSubscriptionDataKeys) *WebPushSubscriptionData {
	this := WebPushSubscriptionData{}
	this.Endpoint = endpoint
	this.Keys = keys
	return &this
}

// NewWebPushSubscriptionDataWithDefaults instantiates a new WebPushSubscriptionData object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewWebPushSubscriptionDataWithDefaults() *WebPushSubscriptionData {
	this := WebPushSubscriptionData{}
	return &this
}

// GetEndpoint returns the Endpoint field value
func (o *WebPushSubscriptionData) GetEndpoint() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Endpoint
}

// GetEndpointOk returns a tuple with the Endpoint field value
// and a boolean to check if the value has been set.
func (o *WebPushSubscriptionData) GetEndpointOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Endpoint, true
}

// SetEndpoint sets field value
func (o *WebPushSubscriptionData) SetEndpoint(v string) {
	o.Endpoint = v
}

// GetKeys returns the Keys field value
func (o *WebPushSubscriptionData) GetKeys() WebPushSubscriptionDataKeys {
	if o == nil {
		var ret WebPushSubscriptionDataKeys
		return ret
	}

	return o.Keys
}

// GetKeysOk returns a tuple with the Keys field value
// and a boolean to check if the value has been set.
func (o *WebPushSubscriptionData) GetKeysOk() (*WebPushSubscriptionDataKeys, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Keys, true
}

// SetKeys sets field value
func (o *WebPushSubscriptionData) SetKeys(v WebPushSubscriptionDataKeys) {
	o.Keys = v
}

func (o WebPushSubscriptionData) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o WebPushSubscriptionData) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["endpoint"] = o.Endpoint
	toSerialize["keys"] = o.Keys
	return toSerialize, nil
}

type NullableWebPushSubscriptionData struct {
	value *WebPushSubscriptionData
	isSet bool
}

func (v NullableWebPushSubscriptionData) Get() *WebPushSubscriptionData {
	return v.value
}

func (v *NullableWebPushSubscriptionData) Set(val *WebPushSubscriptionData) {
	v.value = val
	v.isSet = true
}

func (v NullableWebPushSubscriptionData) IsSet() bool {
	return v.isSet
}

func (v *NullableWebPushSubscriptionData) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableWebPushSubscriptionData(val *WebPushSubscriptionData) *NullableWebPushSubscriptionData {
	return &NullableWebPushSubscriptionData{value: val, isSet: true}
}

func (v NullableWebPushSubscriptionData) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableWebPushSubscriptionData) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


