/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the TokenClaims type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &TokenClaims{}

// TokenClaims struct for TokenClaims
type TokenClaims struct {
	// Role
	R string `json:"r"`
	// Version
	V int32 `json:"v"`
	// Type
	T string `json:"t"`
	// Service Account ID
	Sid *string `json:"sid,omitempty"`
}

// NewTokenClaims instantiates a new TokenClaims object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewTokenClaims(r string, v int32, t string) *TokenClaims {
	this := TokenClaims{}
	this.R = r
	this.V = v
	this.T = t
	return &this
}

// NewTokenClaimsWithDefaults instantiates a new TokenClaims object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewTokenClaimsWithDefaults() *TokenClaims {
	this := TokenClaims{}
	return &this
}

// GetR returns the R field value
func (o *TokenClaims) GetR() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.R
}

// GetROk returns a tuple with the R field value
// and a boolean to check if the value has been set.
func (o *TokenClaims) GetROk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.R, true
}

// SetR sets field value
func (o *TokenClaims) SetR(v string) {
	o.R = v
}

// GetV returns the V field value
func (o *TokenClaims) GetV() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.V
}

// GetVOk returns a tuple with the V field value
// and a boolean to check if the value has been set.
func (o *TokenClaims) GetVOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.V, true
}

// SetV sets field value
func (o *TokenClaims) SetV(v int32) {
	o.V = v
}

// GetT returns the T field value
func (o *TokenClaims) GetT() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.T
}

// GetTOk returns a tuple with the T field value
// and a boolean to check if the value has been set.
func (o *TokenClaims) GetTOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.T, true
}

// SetT sets field value
func (o *TokenClaims) SetT(v string) {
	o.T = v
}

// GetSid returns the Sid field value if set, zero value otherwise.
func (o *TokenClaims) GetSid() string {
	if o == nil || IsNil(o.Sid) {
		var ret string
		return ret
	}
	return *o.Sid
}

// GetSidOk returns a tuple with the Sid field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *TokenClaims) GetSidOk() (*string, bool) {
	if o == nil || IsNil(o.Sid) {
		return nil, false
	}
	return o.Sid, true
}

// HasSid returns a boolean if a field has been set.
func (o *TokenClaims) HasSid() bool {
	if o != nil && !IsNil(o.Sid) {
		return true
	}

	return false
}

// SetSid gets a reference to the given string and assigns it to the Sid field.
func (o *TokenClaims) SetSid(v string) {
	o.Sid = &v
}

func (o TokenClaims) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o TokenClaims) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["r"] = o.R
	toSerialize["v"] = o.V
	toSerialize["t"] = o.T
	if !IsNil(o.Sid) {
		toSerialize["sid"] = o.Sid
	}
	return toSerialize, nil
}

type NullableTokenClaims struct {
	value *TokenClaims
	isSet bool
}

func (v NullableTokenClaims) Get() *TokenClaims {
	return v.value
}

func (v *NullableTokenClaims) Set(val *TokenClaims) {
	v.value = val
	v.isSet = true
}

func (v NullableTokenClaims) IsSet() bool {
	return v.isSet
}

func (v *NullableTokenClaims) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableTokenClaims(val *TokenClaims) *NullableTokenClaims {
	return &NullableTokenClaims{value: val, isSet: true}
}

func (v NullableTokenClaims) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableTokenClaims) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


