/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
	"strings"
)


// GatewaysAPIService GatewaysAPI service
type GatewaysAPIService service

type ApiCreateDraftGatewayRequest struct {
	ctx context.Context
	ApiService *GatewaysAPIService
	gatewayDraftCreateData *GatewayDraftCreateData
}

// 
func (r ApiCreateDraftGatewayRequest) GatewayDraftCreateData(gatewayDraftCreateData GatewayDraftCreateData) ApiCreateDraftGatewayRequest {
	r.gatewayDraftCreateData = &gatewayDraftCreateData
	return r
}

func (r ApiCreateDraftGatewayRequest) Execute() (*Gateway, *http.Response, error) {
	return r.ApiService.CreateDraftGatewayExecute(r)
}

/*
CreateDraftGateway Create draft gateway

Create new draft gateway

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiCreateDraftGatewayRequest
*/
func (a *GatewaysAPIService) CreateDraftGateway(ctx context.Context) ApiCreateDraftGatewayRequest {
	return ApiCreateDraftGatewayRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return Gateway
func (a *GatewaysAPIService) CreateDraftGatewayExecute(r ApiCreateDraftGatewayRequest) (*Gateway, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodPost
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *Gateway
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "GatewaysAPIService.CreateDraftGateway")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/gateways/draft"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/xml", "multipart/form-data"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.gatewayDraftCreateData
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiCreateGatewayRequest struct {
	ctx context.Context
	ApiService *GatewaysAPIService
	gatewayCreateData *GatewayCreateData
}

// 
func (r ApiCreateGatewayRequest) GatewayCreateData(gatewayCreateData GatewayCreateData) ApiCreateGatewayRequest {
	r.gatewayCreateData = &gatewayCreateData
	return r
}

func (r ApiCreateGatewayRequest) Execute() (*Gateway, *http.Response, error) {
	return r.ApiService.CreateGatewayExecute(r)
}

/*
CreateGateway Create gateway

Create new gateway

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiCreateGatewayRequest
*/
func (a *GatewaysAPIService) CreateGateway(ctx context.Context) ApiCreateGatewayRequest {
	return ApiCreateGatewayRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return Gateway
func (a *GatewaysAPIService) CreateGatewayExecute(r ApiCreateGatewayRequest) (*Gateway, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodPost
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *Gateway
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "GatewaysAPIService.CreateGateway")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/gateways"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.gatewayCreateData
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiDeleteGatewayRequest struct {
	ctx context.Context
	ApiService *GatewaysAPIService
	gatewayId string
}

func (r ApiDeleteGatewayRequest) Execute() (*http.Response, error) {
	return r.ApiService.DeleteGatewayExecute(r)
}

/*
DeleteGateway Delete gateway

Delete gateway

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param gatewayId
 @return ApiDeleteGatewayRequest
*/
func (a *GatewaysAPIService) DeleteGateway(ctx context.Context, gatewayId string) ApiDeleteGatewayRequest {
	return ApiDeleteGatewayRequest{
		ApiService: a,
		ctx: ctx,
		gatewayId: gatewayId,
	}
}

// Execute executes the request
func (a *GatewaysAPIService) DeleteGatewayExecute(r ApiDeleteGatewayRequest) (*http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodDelete
		localVarPostBody     interface{}
		formFiles            []formFile
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "GatewaysAPIService.DeleteGateway")
	if err != nil {
		return nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/gateways/{gateway_id}"
	localVarPath = strings.Replace(localVarPath, "{"+"gateway_id"+"}", url.PathEscape(parameterValueToString(r.gatewayId, "gatewayId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiGetGatewayRequest struct {
	ctx context.Context
	ApiService *GatewaysAPIService
	gatewayId string
}

func (r ApiGetGatewayRequest) Execute() (*Gateway, *http.Response, error) {
	return r.ApiService.GetGatewayExecute(r)
}

/*
GetGateway Get gateway

Get single gateway

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param gatewayId
 @return ApiGetGatewayRequest
*/
func (a *GatewaysAPIService) GetGateway(ctx context.Context, gatewayId string) ApiGetGatewayRequest {
	return ApiGetGatewayRequest{
		ApiService: a,
		ctx: ctx,
		gatewayId: gatewayId,
	}
}

// Execute executes the request
//  @return Gateway
func (a *GatewaysAPIService) GetGatewayExecute(r ApiGetGatewayRequest) (*Gateway, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodGet
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *Gateway
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "GatewaysAPIService.GetGateway")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/gateways/{gateway_id}"
	localVarPath = strings.Replace(localVarPath, "{"+"gateway_id"+"}", url.PathEscape(parameterValueToString(r.gatewayId, "gatewayId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetGatewaysConnectionsRequest struct {
	ctx context.Context
	ApiService *GatewaysAPIService
	externalClientId *string
	externalIds *string
}

func (r ApiGetGatewaysConnectionsRequest) ExternalClientId(externalClientId string) ApiGetGatewaysConnectionsRequest {
	r.externalClientId = &externalClientId
	return r
}

// In 360 hub case it is \&quot;channels\&quot; param
func (r ApiGetGatewaysConnectionsRequest) ExternalIds(externalIds string) ApiGetGatewaysConnectionsRequest {
	r.externalIds = &externalIds
	return r
}

func (r ApiGetGatewaysConnectionsRequest) Execute() ([]GetGatewaysConnections200ResponseInner, *http.Response, error) {
	return r.ApiService.GetGatewaysConnectionsExecute(r)
}

/*
GetGatewaysConnections Get gateways connections with 360 dialog hub

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiGetGatewaysConnectionsRequest
*/
func (a *GatewaysAPIService) GetGatewaysConnections(ctx context.Context) ApiGetGatewaysConnectionsRequest {
	return ApiGetGatewaysConnectionsRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return []GetGatewaysConnections200ResponseInner
func (a *GatewaysAPIService) GetGatewaysConnectionsExecute(r ApiGetGatewaysConnectionsRequest) ([]GetGatewaysConnections200ResponseInner, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodGet
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  []GetGatewaysConnections200ResponseInner
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "GatewaysAPIService.GetGatewaysConnections")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/gateways/connections"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if r.externalClientId != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "external_client_id", r.externalClientId, "")
	}
	if r.externalIds != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "external_ids", r.externalIds, "")
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetMessengerSettingsRequest struct {
	ctx context.Context
	ApiService *GatewaysAPIService
	gatewayId string
}

func (r ApiGetMessengerSettingsRequest) Execute() (*MessengerSettings, *http.Response, error) {
	return r.ApiService.GetMessengerSettingsExecute(r)
}

/*
GetMessengerSettings Get messenger settings

Get messenger settings

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param gatewayId
 @return ApiGetMessengerSettingsRequest
*/
func (a *GatewaysAPIService) GetMessengerSettings(ctx context.Context, gatewayId string) ApiGetMessengerSettingsRequest {
	return ApiGetMessengerSettingsRequest{
		ApiService: a,
		ctx: ctx,
		gatewayId: gatewayId,
	}
}

// Execute executes the request
//  @return MessengerSettings
func (a *GatewaysAPIService) GetMessengerSettingsExecute(r ApiGetMessengerSettingsRequest) (*MessengerSettings, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodGet
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *MessengerSettings
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "GatewaysAPIService.GetMessengerSettings")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/gateways/{gateway_id}/settings"
	localVarPath = strings.Replace(localVarPath, "{"+"gateway_id"+"}", url.PathEscape(parameterValueToString(r.gatewayId, "gatewayId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiListGatewaysRequest struct {
	ctx context.Context
	ApiService *GatewaysAPIService
	type_ *string
	customerId *string
	organizationId *string
	channelId *string
	search *string
	sort *string
	limit *int32
	offset *int32
}

func (r ApiListGatewaysRequest) Type_(type_ string) ApiListGatewaysRequest {
	r.type_ = &type_
	return r
}

func (r ApiListGatewaysRequest) CustomerId(customerId string) ApiListGatewaysRequest {
	r.customerId = &customerId
	return r
}

func (r ApiListGatewaysRequest) OrganizationId(organizationId string) ApiListGatewaysRequest {
	r.organizationId = &organizationId
	return r
}

func (r ApiListGatewaysRequest) ChannelId(channelId string) ApiListGatewaysRequest {
	r.channelId = &channelId
	return r
}

func (r ApiListGatewaysRequest) Search(search string) ApiListGatewaysRequest {
	r.search = &search
	return r
}

func (r ApiListGatewaysRequest) Sort(sort string) ApiListGatewaysRequest {
	r.sort = &sort
	return r
}

func (r ApiListGatewaysRequest) Limit(limit int32) ApiListGatewaysRequest {
	r.limit = &limit
	return r
}

func (r ApiListGatewaysRequest) Offset(offset int32) ApiListGatewaysRequest {
	r.offset = &offset
	return r
}

func (r ApiListGatewaysRequest) Execute() (*GatewaysList, *http.Response, error) {
	return r.ApiService.ListGatewaysExecute(r)
}

/*
ListGateways List gateways

List gateways

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return ApiListGatewaysRequest
*/
func (a *GatewaysAPIService) ListGateways(ctx context.Context) ApiListGatewaysRequest {
	return ApiListGatewaysRequest{
		ApiService: a,
		ctx: ctx,
	}
}

// Execute executes the request
//  @return GatewaysList
func (a *GatewaysAPIService) ListGatewaysExecute(r ApiListGatewaysRequest) (*GatewaysList, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodGet
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *GatewaysList
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "GatewaysAPIService.ListGateways")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/gateways"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if r.type_ != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "type", r.type_, "")
	}
	if r.customerId != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "customer_id", r.customerId, "")
	}
	if r.organizationId != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "organization_id", r.organizationId, "")
	}
	if r.channelId != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "channel_id", r.channelId, "")
	}
	if r.search != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "search", r.search, "")
	}
	if r.sort != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "sort", r.sort, "")
	}
	if r.limit != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "limit", r.limit, "")
	}
	if r.offset != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "offset", r.offset, "")
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiPatchGatewayRequest struct {
	ctx context.Context
	ApiService *GatewaysAPIService
	gatewayId string
	gatewayPatchData *GatewayPatchData
}

func (r ApiPatchGatewayRequest) GatewayPatchData(gatewayPatchData GatewayPatchData) ApiPatchGatewayRequest {
	r.gatewayPatchData = &gatewayPatchData
	return r
}

func (r ApiPatchGatewayRequest) Execute() (*Gateway, *http.Response, error) {
	return r.ApiService.PatchGatewayExecute(r)
}

/*
PatchGateway Patch gateway

Patrtial gateway update

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param gatewayId
 @return ApiPatchGatewayRequest
*/
func (a *GatewaysAPIService) PatchGateway(ctx context.Context, gatewayId string) ApiPatchGatewayRequest {
	return ApiPatchGatewayRequest{
		ApiService: a,
		ctx: ctx,
		gatewayId: gatewayId,
	}
}

// Execute executes the request
//  @return Gateway
func (a *GatewaysAPIService) PatchGatewayExecute(r ApiPatchGatewayRequest) (*Gateway, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodPatch
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *Gateway
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "GatewaysAPIService.PatchGateway")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/gateways/{gateway_id}"
	localVarPath = strings.Replace(localVarPath, "{"+"gateway_id"+"}", url.PathEscape(parameterValueToString(r.gatewayId, "gatewayId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.gatewayPatchData
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiUpdateGatewayRequest struct {
	ctx context.Context
	ApiService *GatewaysAPIService
	gatewayId string
	gatewayEditData *GatewayEditData
}

func (r ApiUpdateGatewayRequest) GatewayEditData(gatewayEditData GatewayEditData) ApiUpdateGatewayRequest {
	r.gatewayEditData = &gatewayEditData
	return r
}

func (r ApiUpdateGatewayRequest) Execute() (*Gateway, *http.Response, error) {
	return r.ApiService.UpdateGatewayExecute(r)
}

/*
UpdateGateway Update gateway

Update gateway

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param gatewayId
 @return ApiUpdateGatewayRequest
*/
func (a *GatewaysAPIService) UpdateGateway(ctx context.Context, gatewayId string) ApiUpdateGatewayRequest {
	return ApiUpdateGatewayRequest{
		ApiService: a,
		ctx: ctx,
		gatewayId: gatewayId,
	}
}

// Execute executes the request
//  @return Gateway
func (a *GatewaysAPIService) UpdateGatewayExecute(r ApiUpdateGatewayRequest) (*Gateway, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodPut
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *Gateway
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "GatewaysAPIService.UpdateGateway")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/gateways/{gateway_id}"
	localVarPath = strings.Replace(localVarPath, "{"+"gateway_id"+"}", url.PathEscape(parameterValueToString(r.gatewayId, "gatewayId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.gatewayEditData
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiUpdateMessengerSettingsRequest struct {
	ctx context.Context
	ApiService *GatewaysAPIService
	gatewayId string
	messengerSettings *MessengerSettings
}

func (r ApiUpdateMessengerSettingsRequest) MessengerSettings(messengerSettings MessengerSettings) ApiUpdateMessengerSettingsRequest {
	r.messengerSettings = &messengerSettings
	return r
}

func (r ApiUpdateMessengerSettingsRequest) Execute() (*MessengerSettings, *http.Response, error) {
	return r.ApiService.UpdateMessengerSettingsExecute(r)
}

/*
UpdateMessengerSettings Update messenger settings

Update messenger settings

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param gatewayId
 @return ApiUpdateMessengerSettingsRequest
*/
func (a *GatewaysAPIService) UpdateMessengerSettings(ctx context.Context, gatewayId string) ApiUpdateMessengerSettingsRequest {
	return ApiUpdateMessengerSettingsRequest{
		ApiService: a,
		ctx: ctx,
		gatewayId: gatewayId,
	}
}

// Execute executes the request
//  @return MessengerSettings
func (a *GatewaysAPIService) UpdateMessengerSettingsExecute(r ApiUpdateMessengerSettingsRequest) (*MessengerSettings, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodPatch
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *MessengerSettings
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "GatewaysAPIService.UpdateMessengerSettings")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/gateways/{gateway_id}/settings"
	localVarPath = strings.Replace(localVarPath, "{"+"gateway_id"+"}", url.PathEscape(parameterValueToString(r.gatewayId, "gatewayId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.messengerSettings
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
