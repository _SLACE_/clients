/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
	"time"
)

// checks if the MediaCenterFile type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &MediaCenterFile{}

// MediaCenterFile struct for MediaCenterFile
type MediaCenterFile struct {
	Id string `json:"id"`
	ChannelId string `json:"channel_id"`
	OrganizationId string `json:"organization_id"`
	Key string `json:"key"`
	DefaultVariantKey *string `json:"default_variant_key,omitempty"`
	MediaFileVariants []MediaCenterFileVariant `json:"media_file_variants,omitempty"`
	UpdatedAt time.Time `json:"updated_at"`
	CreatedAt time.Time `json:"created_at"`
	// File and all its variants is visible for whole organization
	OrganizationWide bool `json:"organization_wide"`
	Public bool `json:"public"`
}

// NewMediaCenterFile instantiates a new MediaCenterFile object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMediaCenterFile(id string, channelId string, organizationId string, key string, updatedAt time.Time, createdAt time.Time, organizationWide bool, public bool) *MediaCenterFile {
	this := MediaCenterFile{}
	this.Id = id
	this.ChannelId = channelId
	this.OrganizationId = organizationId
	this.Key = key
	this.UpdatedAt = updatedAt
	this.CreatedAt = createdAt
	this.OrganizationWide = organizationWide
	this.Public = public
	return &this
}

// NewMediaCenterFileWithDefaults instantiates a new MediaCenterFile object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMediaCenterFileWithDefaults() *MediaCenterFile {
	this := MediaCenterFile{}
	return &this
}

// GetId returns the Id field value
func (o *MediaCenterFile) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *MediaCenterFile) GetIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *MediaCenterFile) SetId(v string) {
	o.Id = v
}

// GetChannelId returns the ChannelId field value
func (o *MediaCenterFile) GetChannelId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ChannelId
}

// GetChannelIdOk returns a tuple with the ChannelId field value
// and a boolean to check if the value has been set.
func (o *MediaCenterFile) GetChannelIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ChannelId, true
}

// SetChannelId sets field value
func (o *MediaCenterFile) SetChannelId(v string) {
	o.ChannelId = v
}

// GetOrganizationId returns the OrganizationId field value
func (o *MediaCenterFile) GetOrganizationId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.OrganizationId
}

// GetOrganizationIdOk returns a tuple with the OrganizationId field value
// and a boolean to check if the value has been set.
func (o *MediaCenterFile) GetOrganizationIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.OrganizationId, true
}

// SetOrganizationId sets field value
func (o *MediaCenterFile) SetOrganizationId(v string) {
	o.OrganizationId = v
}

// GetKey returns the Key field value
func (o *MediaCenterFile) GetKey() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Key
}

// GetKeyOk returns a tuple with the Key field value
// and a boolean to check if the value has been set.
func (o *MediaCenterFile) GetKeyOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Key, true
}

// SetKey sets field value
func (o *MediaCenterFile) SetKey(v string) {
	o.Key = v
}

// GetDefaultVariantKey returns the DefaultVariantKey field value if set, zero value otherwise.
func (o *MediaCenterFile) GetDefaultVariantKey() string {
	if o == nil || IsNil(o.DefaultVariantKey) {
		var ret string
		return ret
	}
	return *o.DefaultVariantKey
}

// GetDefaultVariantKeyOk returns a tuple with the DefaultVariantKey field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MediaCenterFile) GetDefaultVariantKeyOk() (*string, bool) {
	if o == nil || IsNil(o.DefaultVariantKey) {
		return nil, false
	}
	return o.DefaultVariantKey, true
}

// HasDefaultVariantKey returns a boolean if a field has been set.
func (o *MediaCenterFile) HasDefaultVariantKey() bool {
	if o != nil && !IsNil(o.DefaultVariantKey) {
		return true
	}

	return false
}

// SetDefaultVariantKey gets a reference to the given string and assigns it to the DefaultVariantKey field.
func (o *MediaCenterFile) SetDefaultVariantKey(v string) {
	o.DefaultVariantKey = &v
}

// GetMediaFileVariants returns the MediaFileVariants field value if set, zero value otherwise.
func (o *MediaCenterFile) GetMediaFileVariants() []MediaCenterFileVariant {
	if o == nil || IsNil(o.MediaFileVariants) {
		var ret []MediaCenterFileVariant
		return ret
	}
	return o.MediaFileVariants
}

// GetMediaFileVariantsOk returns a tuple with the MediaFileVariants field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MediaCenterFile) GetMediaFileVariantsOk() ([]MediaCenterFileVariant, bool) {
	if o == nil || IsNil(o.MediaFileVariants) {
		return nil, false
	}
	return o.MediaFileVariants, true
}

// HasMediaFileVariants returns a boolean if a field has been set.
func (o *MediaCenterFile) HasMediaFileVariants() bool {
	if o != nil && !IsNil(o.MediaFileVariants) {
		return true
	}

	return false
}

// SetMediaFileVariants gets a reference to the given []MediaCenterFileVariant and assigns it to the MediaFileVariants field.
func (o *MediaCenterFile) SetMediaFileVariants(v []MediaCenterFileVariant) {
	o.MediaFileVariants = v
}

// GetUpdatedAt returns the UpdatedAt field value
func (o *MediaCenterFile) GetUpdatedAt() time.Time {
	if o == nil {
		var ret time.Time
		return ret
	}

	return o.UpdatedAt
}

// GetUpdatedAtOk returns a tuple with the UpdatedAt field value
// and a boolean to check if the value has been set.
func (o *MediaCenterFile) GetUpdatedAtOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return &o.UpdatedAt, true
}

// SetUpdatedAt sets field value
func (o *MediaCenterFile) SetUpdatedAt(v time.Time) {
	o.UpdatedAt = v
}

// GetCreatedAt returns the CreatedAt field value
func (o *MediaCenterFile) GetCreatedAt() time.Time {
	if o == nil {
		var ret time.Time
		return ret
	}

	return o.CreatedAt
}

// GetCreatedAtOk returns a tuple with the CreatedAt field value
// and a boolean to check if the value has been set.
func (o *MediaCenterFile) GetCreatedAtOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return &o.CreatedAt, true
}

// SetCreatedAt sets field value
func (o *MediaCenterFile) SetCreatedAt(v time.Time) {
	o.CreatedAt = v
}

// GetOrganizationWide returns the OrganizationWide field value
func (o *MediaCenterFile) GetOrganizationWide() bool {
	if o == nil {
		var ret bool
		return ret
	}

	return o.OrganizationWide
}

// GetOrganizationWideOk returns a tuple with the OrganizationWide field value
// and a boolean to check if the value has been set.
func (o *MediaCenterFile) GetOrganizationWideOk() (*bool, bool) {
	if o == nil {
		return nil, false
	}
	return &o.OrganizationWide, true
}

// SetOrganizationWide sets field value
func (o *MediaCenterFile) SetOrganizationWide(v bool) {
	o.OrganizationWide = v
}

// GetPublic returns the Public field value
func (o *MediaCenterFile) GetPublic() bool {
	if o == nil {
		var ret bool
		return ret
	}

	return o.Public
}

// GetPublicOk returns a tuple with the Public field value
// and a boolean to check if the value has been set.
func (o *MediaCenterFile) GetPublicOk() (*bool, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Public, true
}

// SetPublic sets field value
func (o *MediaCenterFile) SetPublic(v bool) {
	o.Public = v
}

func (o MediaCenterFile) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o MediaCenterFile) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["id"] = o.Id
	toSerialize["channel_id"] = o.ChannelId
	toSerialize["organization_id"] = o.OrganizationId
	toSerialize["key"] = o.Key
	if !IsNil(o.DefaultVariantKey) {
		toSerialize["default_variant_key"] = o.DefaultVariantKey
	}
	if !IsNil(o.MediaFileVariants) {
		toSerialize["media_file_variants"] = o.MediaFileVariants
	}
	toSerialize["updated_at"] = o.UpdatedAt
	toSerialize["created_at"] = o.CreatedAt
	toSerialize["organization_wide"] = o.OrganizationWide
	toSerialize["public"] = o.Public
	return toSerialize, nil
}

type NullableMediaCenterFile struct {
	value *MediaCenterFile
	isSet bool
}

func (v NullableMediaCenterFile) Get() *MediaCenterFile {
	return v.value
}

func (v *NullableMediaCenterFile) Set(val *MediaCenterFile) {
	v.value = val
	v.isSet = true
}

func (v NullableMediaCenterFile) IsSet() bool {
	return v.isSet
}

func (v *NullableMediaCenterFile) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMediaCenterFile(val *MediaCenterFile) *NullableMediaCenterFile {
	return &NullableMediaCenterFile{value: val, isSet: true}
}

func (v NullableMediaCenterFile) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMediaCenterFile) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


