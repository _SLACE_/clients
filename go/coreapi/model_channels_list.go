/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the ChannelsList type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ChannelsList{}

// ChannelsList struct for ChannelsList
type ChannelsList struct {
	Results []Channel `json:"results"`
	Total int32 `json:"total"`
}

// NewChannelsList instantiates a new ChannelsList object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewChannelsList(results []Channel, total int32) *ChannelsList {
	this := ChannelsList{}
	this.Results = results
	this.Total = total
	return &this
}

// NewChannelsListWithDefaults instantiates a new ChannelsList object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewChannelsListWithDefaults() *ChannelsList {
	this := ChannelsList{}
	return &this
}

// GetResults returns the Results field value
func (o *ChannelsList) GetResults() []Channel {
	if o == nil {
		var ret []Channel
		return ret
	}

	return o.Results
}

// GetResultsOk returns a tuple with the Results field value
// and a boolean to check if the value has been set.
func (o *ChannelsList) GetResultsOk() ([]Channel, bool) {
	if o == nil {
		return nil, false
	}
	return o.Results, true
}

// SetResults sets field value
func (o *ChannelsList) SetResults(v []Channel) {
	o.Results = v
}

// GetTotal returns the Total field value
func (o *ChannelsList) GetTotal() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.Total
}

// GetTotalOk returns a tuple with the Total field value
// and a boolean to check if the value has been set.
func (o *ChannelsList) GetTotalOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Total, true
}

// SetTotal sets field value
func (o *ChannelsList) SetTotal(v int32) {
	o.Total = v
}

func (o ChannelsList) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ChannelsList) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["results"] = o.Results
	toSerialize["total"] = o.Total
	return toSerialize, nil
}

type NullableChannelsList struct {
	value *ChannelsList
	isSet bool
}

func (v NullableChannelsList) Get() *ChannelsList {
	return v.value
}

func (v *NullableChannelsList) Set(val *ChannelsList) {
	v.value = val
	v.isSet = true
}

func (v NullableChannelsList) IsSet() bool {
	return v.isSet
}

func (v *NullableChannelsList) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableChannelsList(val *ChannelsList) *NullableChannelsList {
	return &NullableChannelsList{value: val, isSet: true}
}

func (v NullableChannelsList) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableChannelsList) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


