/*
Core

Core API is used to manage resources around organisation like auth, set webhooks per channels, manage campaigns.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package coreapi

import (
	"encoding/json"
)

// checks if the OrganizationMutableLanguageSettings type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &OrganizationMutableLanguageSettings{}

// OrganizationMutableLanguageSettings struct for OrganizationMutableLanguageSettings
type OrganizationMutableLanguageSettings struct {
	Default *string `json:"default,omitempty"`
	Supported []string `json:"supported,omitempty"`
	Fallback *string `json:"fallback,omitempty"`
}

// NewOrganizationMutableLanguageSettings instantiates a new OrganizationMutableLanguageSettings object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewOrganizationMutableLanguageSettings() *OrganizationMutableLanguageSettings {
	this := OrganizationMutableLanguageSettings{}
	return &this
}

// NewOrganizationMutableLanguageSettingsWithDefaults instantiates a new OrganizationMutableLanguageSettings object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewOrganizationMutableLanguageSettingsWithDefaults() *OrganizationMutableLanguageSettings {
	this := OrganizationMutableLanguageSettings{}
	return &this
}

// GetDefault returns the Default field value if set, zero value otherwise.
func (o *OrganizationMutableLanguageSettings) GetDefault() string {
	if o == nil || IsNil(o.Default) {
		var ret string
		return ret
	}
	return *o.Default
}

// GetDefaultOk returns a tuple with the Default field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *OrganizationMutableLanguageSettings) GetDefaultOk() (*string, bool) {
	if o == nil || IsNil(o.Default) {
		return nil, false
	}
	return o.Default, true
}

// HasDefault returns a boolean if a field has been set.
func (o *OrganizationMutableLanguageSettings) HasDefault() bool {
	if o != nil && !IsNil(o.Default) {
		return true
	}

	return false
}

// SetDefault gets a reference to the given string and assigns it to the Default field.
func (o *OrganizationMutableLanguageSettings) SetDefault(v string) {
	o.Default = &v
}

// GetSupported returns the Supported field value if set, zero value otherwise.
func (o *OrganizationMutableLanguageSettings) GetSupported() []string {
	if o == nil || IsNil(o.Supported) {
		var ret []string
		return ret
	}
	return o.Supported
}

// GetSupportedOk returns a tuple with the Supported field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *OrganizationMutableLanguageSettings) GetSupportedOk() ([]string, bool) {
	if o == nil || IsNil(o.Supported) {
		return nil, false
	}
	return o.Supported, true
}

// HasSupported returns a boolean if a field has been set.
func (o *OrganizationMutableLanguageSettings) HasSupported() bool {
	if o != nil && !IsNil(o.Supported) {
		return true
	}

	return false
}

// SetSupported gets a reference to the given []string and assigns it to the Supported field.
func (o *OrganizationMutableLanguageSettings) SetSupported(v []string) {
	o.Supported = v
}

// GetFallback returns the Fallback field value if set, zero value otherwise.
func (o *OrganizationMutableLanguageSettings) GetFallback() string {
	if o == nil || IsNil(o.Fallback) {
		var ret string
		return ret
	}
	return *o.Fallback
}

// GetFallbackOk returns a tuple with the Fallback field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *OrganizationMutableLanguageSettings) GetFallbackOk() (*string, bool) {
	if o == nil || IsNil(o.Fallback) {
		return nil, false
	}
	return o.Fallback, true
}

// HasFallback returns a boolean if a field has been set.
func (o *OrganizationMutableLanguageSettings) HasFallback() bool {
	if o != nil && !IsNil(o.Fallback) {
		return true
	}

	return false
}

// SetFallback gets a reference to the given string and assigns it to the Fallback field.
func (o *OrganizationMutableLanguageSettings) SetFallback(v string) {
	o.Fallback = &v
}

func (o OrganizationMutableLanguageSettings) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o OrganizationMutableLanguageSettings) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Default) {
		toSerialize["default"] = o.Default
	}
	if !IsNil(o.Supported) {
		toSerialize["supported"] = o.Supported
	}
	if !IsNil(o.Fallback) {
		toSerialize["fallback"] = o.Fallback
	}
	return toSerialize, nil
}

type NullableOrganizationMutableLanguageSettings struct {
	value *OrganizationMutableLanguageSettings
	isSet bool
}

func (v NullableOrganizationMutableLanguageSettings) Get() *OrganizationMutableLanguageSettings {
	return v.value
}

func (v *NullableOrganizationMutableLanguageSettings) Set(val *OrganizationMutableLanguageSettings) {
	v.value = val
	v.isSet = true
}

func (v NullableOrganizationMutableLanguageSettings) IsSet() bool {
	return v.isSet
}

func (v *NullableOrganizationMutableLanguageSettings) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableOrganizationMutableLanguageSettings(val *OrganizationMutableLanguageSettings) *NullableOrganizationMutableLanguageSettings {
	return &NullableOrganizationMutableLanguageSettings{value: val, isSet: true}
}

func (v NullableOrganizationMutableLanguageSettings) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableOrganizationMutableLanguageSettings) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


