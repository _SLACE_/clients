# StartScriptRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConversationId** | **string** |  | 
**TransactionId** | Pointer to **string** |  | [optional] 
**Language** | Pointer to **string** |  | [optional] 
**Reference** | Pointer to **string** |  | [optional] 
**MessageTags** | Pointer to **[]string** |  | [optional] 
**StaticData** | Pointer to **map[string]interface{}** |  | [optional] 
**DynamicData** | Pointer to **map[string]interface{}** |  | [optional] 
**InteractionId** | Pointer to **string** |  | [optional] 

## Methods

### NewStartScriptRequest

`func NewStartScriptRequest(conversationId string, ) *StartScriptRequest`

NewStartScriptRequest instantiates a new StartScriptRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStartScriptRequestWithDefaults

`func NewStartScriptRequestWithDefaults() *StartScriptRequest`

NewStartScriptRequestWithDefaults instantiates a new StartScriptRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetConversationId

`func (o *StartScriptRequest) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *StartScriptRequest) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *StartScriptRequest) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetTransactionId

`func (o *StartScriptRequest) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *StartScriptRequest) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *StartScriptRequest) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.

### HasTransactionId

`func (o *StartScriptRequest) HasTransactionId() bool`

HasTransactionId returns a boolean if a field has been set.

### GetLanguage

`func (o *StartScriptRequest) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *StartScriptRequest) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *StartScriptRequest) SetLanguage(v string)`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *StartScriptRequest) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.

### GetReference

`func (o *StartScriptRequest) GetReference() string`

GetReference returns the Reference field if non-nil, zero value otherwise.

### GetReferenceOk

`func (o *StartScriptRequest) GetReferenceOk() (*string, bool)`

GetReferenceOk returns a tuple with the Reference field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReference

`func (o *StartScriptRequest) SetReference(v string)`

SetReference sets Reference field to given value.

### HasReference

`func (o *StartScriptRequest) HasReference() bool`

HasReference returns a boolean if a field has been set.

### GetMessageTags

`func (o *StartScriptRequest) GetMessageTags() []string`

GetMessageTags returns the MessageTags field if non-nil, zero value otherwise.

### GetMessageTagsOk

`func (o *StartScriptRequest) GetMessageTagsOk() (*[]string, bool)`

GetMessageTagsOk returns a tuple with the MessageTags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageTags

`func (o *StartScriptRequest) SetMessageTags(v []string)`

SetMessageTags sets MessageTags field to given value.

### HasMessageTags

`func (o *StartScriptRequest) HasMessageTags() bool`

HasMessageTags returns a boolean if a field has been set.

### GetStaticData

`func (o *StartScriptRequest) GetStaticData() map[string]interface{}`

GetStaticData returns the StaticData field if non-nil, zero value otherwise.

### GetStaticDataOk

`func (o *StartScriptRequest) GetStaticDataOk() (*map[string]interface{}, bool)`

GetStaticDataOk returns a tuple with the StaticData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStaticData

`func (o *StartScriptRequest) SetStaticData(v map[string]interface{})`

SetStaticData sets StaticData field to given value.

### HasStaticData

`func (o *StartScriptRequest) HasStaticData() bool`

HasStaticData returns a boolean if a field has been set.

### GetDynamicData

`func (o *StartScriptRequest) GetDynamicData() map[string]interface{}`

GetDynamicData returns the DynamicData field if non-nil, zero value otherwise.

### GetDynamicDataOk

`func (o *StartScriptRequest) GetDynamicDataOk() (*map[string]interface{}, bool)`

GetDynamicDataOk returns a tuple with the DynamicData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDynamicData

`func (o *StartScriptRequest) SetDynamicData(v map[string]interface{})`

SetDynamicData sets DynamicData field to given value.

### HasDynamicData

`func (o *StartScriptRequest) HasDynamicData() bool`

HasDynamicData returns a boolean if a field has been set.

### GetInteractionId

`func (o *StartScriptRequest) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *StartScriptRequest) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *StartScriptRequest) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *StartScriptRequest) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


