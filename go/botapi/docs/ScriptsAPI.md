# \ScriptsAPI

All URIs are relative to *https://bot.slace.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**StartScript**](ScriptsAPI.md#StartScript) | **Post** /api/channels/{channel_id}/scripts/{script_id}/start | Start script



## StartScript

> StartScript(ctx, channelId, scriptId).StartScriptRequest(startScriptRequest).Execute()

Start script



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapi"
)

func main() {
    channelId := "channelId_example" // string | 
    scriptId := "scriptId_example" // string | 
    startScriptRequest := *openapiclient.NewStartScriptRequest("ConversationId_example") // StartScriptRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ScriptsAPI.StartScript(context.Background(), channelId, scriptId).StartScriptRequest(startScriptRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScriptsAPI.StartScript``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**scriptId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStartScriptRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **startScriptRequest** | [**StartScriptRequest**](StartScriptRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

