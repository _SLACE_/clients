# AvailableActionsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Unzer** | **[]string** |  | 
**Paypal** | **[]string** |  | 
**Custom** | **[]string** |  | 

## Methods

### NewAvailableActionsResponse

`func NewAvailableActionsResponse(unzer []string, paypal []string, custom []string, ) *AvailableActionsResponse`

NewAvailableActionsResponse instantiates a new AvailableActionsResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAvailableActionsResponseWithDefaults

`func NewAvailableActionsResponseWithDefaults() *AvailableActionsResponse`

NewAvailableActionsResponseWithDefaults instantiates a new AvailableActionsResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUnzer

`func (o *AvailableActionsResponse) GetUnzer() []string`

GetUnzer returns the Unzer field if non-nil, zero value otherwise.

### GetUnzerOk

`func (o *AvailableActionsResponse) GetUnzerOk() (*[]string, bool)`

GetUnzerOk returns a tuple with the Unzer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnzer

`func (o *AvailableActionsResponse) SetUnzer(v []string)`

SetUnzer sets Unzer field to given value.


### GetPaypal

`func (o *AvailableActionsResponse) GetPaypal() []string`

GetPaypal returns the Paypal field if non-nil, zero value otherwise.

### GetPaypalOk

`func (o *AvailableActionsResponse) GetPaypalOk() (*[]string, bool)`

GetPaypalOk returns a tuple with the Paypal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPaypal

`func (o *AvailableActionsResponse) SetPaypal(v []string)`

SetPaypal sets Paypal field to given value.


### GetCustom

`func (o *AvailableActionsResponse) GetCustom() []string`

GetCustom returns the Custom field if non-nil, zero value otherwise.

### GetCustomOk

`func (o *AvailableActionsResponse) GetCustomOk() (*[]string, bool)`

GetCustomOk returns a tuple with the Custom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustom

`func (o *AvailableActionsResponse) SetCustom(v []string)`

SetCustom sets Custom field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


