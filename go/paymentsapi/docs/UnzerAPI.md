# \UnzerAPI

All URIs are relative to *https://payments.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateUnzerConfiguration**](UnzerAPI.md#CreateUnzerConfiguration) | **Post** /integrations/unzer/organizations/{oid}/configurations | Create Unzer configuration
[**DeleteUnzerConfiguration**](UnzerAPI.md#DeleteUnzerConfiguration) | **Delete** /integrations/unzer/organizations/{oid}/configurations | Delete Unzer configuration
[**GetUnzerConfiguration**](UnzerAPI.md#GetUnzerConfiguration) | **Get** /integrations/unzer/organizations/{oid}/configurations | Get Unzer configuration



## CreateUnzerConfiguration

> UnzerConfiguration CreateUnzerConfiguration(ctx, oid).CreateUnzerConfigRequest(createUnzerConfigRequest).Execute()

Create Unzer configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/paymentsapi"
)

func main() {
    oid := "oid_example" // string | Organization ID
    createUnzerConfigRequest := *openapiclient.NewCreateUnzerConfigRequest("PublicKey_example", "PrivateKey_example", "ApiUrl_example") // CreateUnzerConfigRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UnzerAPI.CreateUnzerConfiguration(context.Background(), oid).CreateUnzerConfigRequest(createUnzerConfigRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UnzerAPI.CreateUnzerConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateUnzerConfiguration`: UnzerConfiguration
    fmt.Fprintf(os.Stdout, "Response from `UnzerAPI.CreateUnzerConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** | Organization ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateUnzerConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **createUnzerConfigRequest** | [**CreateUnzerConfigRequest**](CreateUnzerConfigRequest.md) |  | 

### Return type

[**UnzerConfiguration**](UnzerConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteUnzerConfiguration

> DeleteUnzerConfiguration(ctx, oid).Execute()

Delete Unzer configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/paymentsapi"
)

func main() {
    oid := "oid_example" // string | Organization ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.UnzerAPI.DeleteUnzerConfiguration(context.Background(), oid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UnzerAPI.DeleteUnzerConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** | Organization ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteUnzerConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetUnzerConfiguration

> UnzerConfiguration GetUnzerConfiguration(ctx, oid).Execute()

Get Unzer configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/paymentsapi"
)

func main() {
    oid := "oid_example" // string | Organization ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.UnzerAPI.GetUnzerConfiguration(context.Background(), oid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `UnzerAPI.GetUnzerConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetUnzerConfiguration`: UnzerConfiguration
    fmt.Fprintf(os.Stdout, "Response from `UnzerAPI.GetUnzerConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** | Organization ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetUnzerConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**UnzerConfiguration**](UnzerConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

