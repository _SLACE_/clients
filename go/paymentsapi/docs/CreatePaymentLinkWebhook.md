# CreatePaymentLinkWebhook

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TransactionId** | **string** |  | 
**ChannelId** | **string** |  | 
**ConversationId** | **string** |  | 
**ContactId** | **string** |  | 
**Attributes** | Pointer to **map[string]interface{}** |  | [optional] 
**PaymentLink** | **string** |  | 
**PaymentId** | **string** |  | 

## Methods

### NewCreatePaymentLinkWebhook

`func NewCreatePaymentLinkWebhook(transactionId string, channelId string, conversationId string, contactId string, paymentLink string, paymentId string, ) *CreatePaymentLinkWebhook`

NewCreatePaymentLinkWebhook instantiates a new CreatePaymentLinkWebhook object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreatePaymentLinkWebhookWithDefaults

`func NewCreatePaymentLinkWebhookWithDefaults() *CreatePaymentLinkWebhook`

NewCreatePaymentLinkWebhookWithDefaults instantiates a new CreatePaymentLinkWebhook object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTransactionId

`func (o *CreatePaymentLinkWebhook) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *CreatePaymentLinkWebhook) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *CreatePaymentLinkWebhook) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetChannelId

`func (o *CreatePaymentLinkWebhook) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CreatePaymentLinkWebhook) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CreatePaymentLinkWebhook) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetConversationId

`func (o *CreatePaymentLinkWebhook) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *CreatePaymentLinkWebhook) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *CreatePaymentLinkWebhook) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetContactId

`func (o *CreatePaymentLinkWebhook) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *CreatePaymentLinkWebhook) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *CreatePaymentLinkWebhook) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetAttributes

`func (o *CreatePaymentLinkWebhook) GetAttributes() map[string]interface{}`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *CreatePaymentLinkWebhook) GetAttributesOk() (*map[string]interface{}, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *CreatePaymentLinkWebhook) SetAttributes(v map[string]interface{})`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *CreatePaymentLinkWebhook) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetPaymentLink

`func (o *CreatePaymentLinkWebhook) GetPaymentLink() string`

GetPaymentLink returns the PaymentLink field if non-nil, zero value otherwise.

### GetPaymentLinkOk

`func (o *CreatePaymentLinkWebhook) GetPaymentLinkOk() (*string, bool)`

GetPaymentLinkOk returns a tuple with the PaymentLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPaymentLink

`func (o *CreatePaymentLinkWebhook) SetPaymentLink(v string)`

SetPaymentLink sets PaymentLink field to given value.


### GetPaymentId

`func (o *CreatePaymentLinkWebhook) GetPaymentId() string`

GetPaymentId returns the PaymentId field if non-nil, zero value otherwise.

### GetPaymentIdOk

`func (o *CreatePaymentLinkWebhook) GetPaymentIdOk() (*string, bool)`

GetPaymentIdOk returns a tuple with the PaymentId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPaymentId

`func (o *CreatePaymentLinkWebhook) SetPaymentId(v string)`

SetPaymentId sets PaymentId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


