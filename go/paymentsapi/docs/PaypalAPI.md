# \PaypalAPI

All URIs are relative to *https://payments.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreatePaypalConfiguration**](PaypalAPI.md#CreatePaypalConfiguration) | **Post** /integrations/paypal/organizations/{oid}/configurations | Create Paypal configuration
[**DeletePaypalConfiguration**](PaypalAPI.md#DeletePaypalConfiguration) | **Delete** /integrations/paypal/organizations/{oid}/configurations | Delete Paypal configuration
[**GetPaypalConfiguration**](PaypalAPI.md#GetPaypalConfiguration) | **Get** /integrations/paypal/organizations/{oid}/configurations | Get Paypal configuration



## CreatePaypalConfiguration

> PaypalConfiguration CreatePaypalConfiguration(ctx, oid).CreatePaypalConfigRequest(createPaypalConfigRequest).Execute()

Create Paypal configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/paymentsapi"
)

func main() {
    oid := "oid_example" // string | 
    createPaypalConfigRequest := *openapiclient.NewCreatePaypalConfigRequest("ApiUrl_example", "ClientId_example", "ClientSecret_example") // CreatePaypalConfigRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.PaypalAPI.CreatePaypalConfiguration(context.Background(), oid).CreatePaypalConfigRequest(createPaypalConfigRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PaypalAPI.CreatePaypalConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreatePaypalConfiguration`: PaypalConfiguration
    fmt.Fprintf(os.Stdout, "Response from `PaypalAPI.CreatePaypalConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreatePaypalConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **createPaypalConfigRequest** | [**CreatePaypalConfigRequest**](CreatePaypalConfigRequest.md) |  | 

### Return type

[**PaypalConfiguration**](PaypalConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeletePaypalConfiguration

> DeletePaypalConfiguration(ctx, oid).Execute()

Delete Paypal configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/paymentsapi"
)

func main() {
    oid := "oid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.PaypalAPI.DeletePaypalConfiguration(context.Background(), oid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PaypalAPI.DeletePaypalConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeletePaypalConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetPaypalConfiguration

> PaypalConfiguration GetPaypalConfiguration(ctx, oid).Execute()

Get Paypal configuration

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/paymentsapi"
)

func main() {
    oid := "oid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.PaypalAPI.GetPaypalConfiguration(context.Background(), oid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `PaypalAPI.GetPaypalConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetPaypalConfiguration`: PaypalConfiguration
    fmt.Fprintf(os.Stdout, "Response from `PaypalAPI.GetPaypalConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetPaypalConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**PaypalConfiguration**](PaypalConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

