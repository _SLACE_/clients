# CreatePaypalConfigRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ApiUrl** | **string** |  | 
**ClientId** | **string** |  | 
**ClientSecret** | **string** |  | 

## Methods

### NewCreatePaypalConfigRequest

`func NewCreatePaypalConfigRequest(apiUrl string, clientId string, clientSecret string, ) *CreatePaypalConfigRequest`

NewCreatePaypalConfigRequest instantiates a new CreatePaypalConfigRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreatePaypalConfigRequestWithDefaults

`func NewCreatePaypalConfigRequestWithDefaults() *CreatePaypalConfigRequest`

NewCreatePaypalConfigRequestWithDefaults instantiates a new CreatePaypalConfigRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetApiUrl

`func (o *CreatePaypalConfigRequest) GetApiUrl() string`

GetApiUrl returns the ApiUrl field if non-nil, zero value otherwise.

### GetApiUrlOk

`func (o *CreatePaypalConfigRequest) GetApiUrlOk() (*string, bool)`

GetApiUrlOk returns a tuple with the ApiUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetApiUrl

`func (o *CreatePaypalConfigRequest) SetApiUrl(v string)`

SetApiUrl sets ApiUrl field to given value.


### GetClientId

`func (o *CreatePaypalConfigRequest) GetClientId() string`

GetClientId returns the ClientId field if non-nil, zero value otherwise.

### GetClientIdOk

`func (o *CreatePaypalConfigRequest) GetClientIdOk() (*string, bool)`

GetClientIdOk returns a tuple with the ClientId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetClientId

`func (o *CreatePaypalConfigRequest) SetClientId(v string)`

SetClientId sets ClientId field to given value.


### GetClientSecret

`func (o *CreatePaypalConfigRequest) GetClientSecret() string`

GetClientSecret returns the ClientSecret field if non-nil, zero value otherwise.

### GetClientSecretOk

`func (o *CreatePaypalConfigRequest) GetClientSecretOk() (*string, bool)`

GetClientSecretOk returns a tuple with the ClientSecret field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetClientSecret

`func (o *CreatePaypalConfigRequest) SetClientSecret(v string)`

SetClientSecret sets ClientSecret field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


