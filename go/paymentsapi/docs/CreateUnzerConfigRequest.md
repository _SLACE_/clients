# CreateUnzerConfigRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PublicKey** | **string** |  | 
**PrivateKey** | **string** |  | 
**ApiUrl** | **string** |  | 

## Methods

### NewCreateUnzerConfigRequest

`func NewCreateUnzerConfigRequest(publicKey string, privateKey string, apiUrl string, ) *CreateUnzerConfigRequest`

NewCreateUnzerConfigRequest instantiates a new CreateUnzerConfigRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateUnzerConfigRequestWithDefaults

`func NewCreateUnzerConfigRequestWithDefaults() *CreateUnzerConfigRequest`

NewCreateUnzerConfigRequestWithDefaults instantiates a new CreateUnzerConfigRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPublicKey

`func (o *CreateUnzerConfigRequest) GetPublicKey() string`

GetPublicKey returns the PublicKey field if non-nil, zero value otherwise.

### GetPublicKeyOk

`func (o *CreateUnzerConfigRequest) GetPublicKeyOk() (*string, bool)`

GetPublicKeyOk returns a tuple with the PublicKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPublicKey

`func (o *CreateUnzerConfigRequest) SetPublicKey(v string)`

SetPublicKey sets PublicKey field to given value.


### GetPrivateKey

`func (o *CreateUnzerConfigRequest) GetPrivateKey() string`

GetPrivateKey returns the PrivateKey field if non-nil, zero value otherwise.

### GetPrivateKeyOk

`func (o *CreateUnzerConfigRequest) GetPrivateKeyOk() (*string, bool)`

GetPrivateKeyOk returns a tuple with the PrivateKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrivateKey

`func (o *CreateUnzerConfigRequest) SetPrivateKey(v string)`

SetPrivateKey sets PrivateKey field to given value.


### GetApiUrl

`func (o *CreateUnzerConfigRequest) GetApiUrl() string`

GetApiUrl returns the ApiUrl field if non-nil, zero value otherwise.

### GetApiUrlOk

`func (o *CreateUnzerConfigRequest) GetApiUrlOk() (*string, bool)`

GetApiUrlOk returns a tuple with the ApiUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetApiUrl

`func (o *CreateUnzerConfigRequest) SetApiUrl(v string)`

SetApiUrl sets ApiUrl field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


