# Milestone

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**ViralLoopId** | **string** |  | 
**Name** | **string** |  | 
**CompleteThreshold** | **float32** |  | 
**ReferralRewardEach** | Pointer to [**RewardConfig**](RewardConfig.md) |  | [optional] 
**ReferralRewardMilestoneCompleted** | Pointer to [**RewardConfig**](RewardConfig.md) |  | [optional] 
**FriendReward** | Pointer to [**RewardConfig**](RewardConfig.md) |  | [optional] 
**Attributes** | Pointer to **map[string]string** |  | [optional] 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 

## Methods

### NewMilestone

`func NewMilestone(id string, viralLoopId string, name string, completeThreshold float32, createdAt time.Time, updatedAt time.Time, ) *Milestone`

NewMilestone instantiates a new Milestone object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMilestoneWithDefaults

`func NewMilestoneWithDefaults() *Milestone`

NewMilestoneWithDefaults instantiates a new Milestone object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Milestone) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Milestone) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Milestone) SetId(v string)`

SetId sets Id field to given value.


### GetViralLoopId

`func (o *Milestone) GetViralLoopId() string`

GetViralLoopId returns the ViralLoopId field if non-nil, zero value otherwise.

### GetViralLoopIdOk

`func (o *Milestone) GetViralLoopIdOk() (*string, bool)`

GetViralLoopIdOk returns a tuple with the ViralLoopId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetViralLoopId

`func (o *Milestone) SetViralLoopId(v string)`

SetViralLoopId sets ViralLoopId field to given value.


### GetName

`func (o *Milestone) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Milestone) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Milestone) SetName(v string)`

SetName sets Name field to given value.


### GetCompleteThreshold

`func (o *Milestone) GetCompleteThreshold() float32`

GetCompleteThreshold returns the CompleteThreshold field if non-nil, zero value otherwise.

### GetCompleteThresholdOk

`func (o *Milestone) GetCompleteThresholdOk() (*float32, bool)`

GetCompleteThresholdOk returns a tuple with the CompleteThreshold field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompleteThreshold

`func (o *Milestone) SetCompleteThreshold(v float32)`

SetCompleteThreshold sets CompleteThreshold field to given value.


### GetReferralRewardEach

`func (o *Milestone) GetReferralRewardEach() RewardConfig`

GetReferralRewardEach returns the ReferralRewardEach field if non-nil, zero value otherwise.

### GetReferralRewardEachOk

`func (o *Milestone) GetReferralRewardEachOk() (*RewardConfig, bool)`

GetReferralRewardEachOk returns a tuple with the ReferralRewardEach field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferralRewardEach

`func (o *Milestone) SetReferralRewardEach(v RewardConfig)`

SetReferralRewardEach sets ReferralRewardEach field to given value.

### HasReferralRewardEach

`func (o *Milestone) HasReferralRewardEach() bool`

HasReferralRewardEach returns a boolean if a field has been set.

### GetReferralRewardMilestoneCompleted

`func (o *Milestone) GetReferralRewardMilestoneCompleted() RewardConfig`

GetReferralRewardMilestoneCompleted returns the ReferralRewardMilestoneCompleted field if non-nil, zero value otherwise.

### GetReferralRewardMilestoneCompletedOk

`func (o *Milestone) GetReferralRewardMilestoneCompletedOk() (*RewardConfig, bool)`

GetReferralRewardMilestoneCompletedOk returns a tuple with the ReferralRewardMilestoneCompleted field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferralRewardMilestoneCompleted

`func (o *Milestone) SetReferralRewardMilestoneCompleted(v RewardConfig)`

SetReferralRewardMilestoneCompleted sets ReferralRewardMilestoneCompleted field to given value.

### HasReferralRewardMilestoneCompleted

`func (o *Milestone) HasReferralRewardMilestoneCompleted() bool`

HasReferralRewardMilestoneCompleted returns a boolean if a field has been set.

### GetFriendReward

`func (o *Milestone) GetFriendReward() RewardConfig`

GetFriendReward returns the FriendReward field if non-nil, zero value otherwise.

### GetFriendRewardOk

`func (o *Milestone) GetFriendRewardOk() (*RewardConfig, bool)`

GetFriendRewardOk returns a tuple with the FriendReward field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFriendReward

`func (o *Milestone) SetFriendReward(v RewardConfig)`

SetFriendReward sets FriendReward field to given value.

### HasFriendReward

`func (o *Milestone) HasFriendReward() bool`

HasFriendReward returns a boolean if a field has been set.

### GetAttributes

`func (o *Milestone) GetAttributes() map[string]string`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *Milestone) GetAttributesOk() (*map[string]string, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *Milestone) SetAttributes(v map[string]string)`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *Milestone) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetCreatedAt

`func (o *Milestone) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Milestone) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Milestone) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *Milestone) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Milestone) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Milestone) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


