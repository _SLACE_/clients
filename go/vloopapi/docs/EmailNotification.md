# EmailNotification

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Subject** | **string** |  | 
**Body** | **string** |  | 
**Footer** | **string** |  | 

## Methods

### NewEmailNotification

`func NewEmailNotification(subject string, body string, footer string, ) *EmailNotification`

NewEmailNotification instantiates a new EmailNotification object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewEmailNotificationWithDefaults

`func NewEmailNotificationWithDefaults() *EmailNotification`

NewEmailNotificationWithDefaults instantiates a new EmailNotification object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSubject

`func (o *EmailNotification) GetSubject() string`

GetSubject returns the Subject field if non-nil, zero value otherwise.

### GetSubjectOk

`func (o *EmailNotification) GetSubjectOk() (*string, bool)`

GetSubjectOk returns a tuple with the Subject field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSubject

`func (o *EmailNotification) SetSubject(v string)`

SetSubject sets Subject field to given value.


### GetBody

`func (o *EmailNotification) GetBody() string`

GetBody returns the Body field if non-nil, zero value otherwise.

### GetBodyOk

`func (o *EmailNotification) GetBodyOk() (*string, bool)`

GetBodyOk returns a tuple with the Body field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBody

`func (o *EmailNotification) SetBody(v string)`

SetBody sets Body field to given value.


### GetFooter

`func (o *EmailNotification) GetFooter() string`

GetFooter returns the Footer field if non-nil, zero value otherwise.

### GetFooterOk

`func (o *EmailNotification) GetFooterOk() (*string, bool)`

GetFooterOk returns a tuple with the Footer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFooter

`func (o *EmailNotification) SetFooter(v string)`

SetFooter sets Footer field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


