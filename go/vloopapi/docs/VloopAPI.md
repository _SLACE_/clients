# \VloopAPI

All URIs are relative to *https://vloop.slace.com/v1/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CompleteFriend**](VloopAPI.md#CompleteFriend) | **Post** /viral-loops/{id}/friends/complete | Complete friend
[**CreateFriend**](VloopAPI.md#CreateFriend) | **Post** /viral-loops/{id}/friends | Create friend
[**CreateMilestone**](VloopAPI.md#CreateMilestone) | **Post** /viral-loops/{id}/milestones | Create milestone
[**CreateReferral**](VloopAPI.md#CreateReferral) | **Post** /viral-loops/{id}/referrals | Create referral
[**CreateViralLoop**](VloopAPI.md#CreateViralLoop) | **Post** /viral-loops | Create Viral Loop
[**DeleteMilestone**](VloopAPI.md#DeleteMilestone) | **Delete** /viral-loops/{id}/milestones/{mid} | Delete milestone
[**DeleteViralLoop**](VloopAPI.md#DeleteViralLoop) | **Delete** /viral-loops/{id} | Delete Viral Loop
[**FindViralLoop**](VloopAPI.md#FindViralLoop) | **Get** /viral-loops/{id} | Find Viral Loop by ID
[**ListViralLoops**](VloopAPI.md#ListViralLoops) | **Get** /viral-loops | List Viral Loops
[**UpdateMilestone**](VloopAPI.md#UpdateMilestone) | **Put** /viral-loops/{id}/milestones/{mid} | Update milestone
[**UpdateViralLoop**](VloopAPI.md#UpdateViralLoop) | **Put** /viral-loops/{id} | Update Viral Loop
[**Webhook**](VloopAPI.md#Webhook) | **Post** /webhook | Webhook



## CompleteFriend

> CompleteFriend(ctx, id).CompleteFriend(completeFriend).Execute()

Complete friend



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/vloopapi"
)

func main() {
    id := "id_example" // string | 
    completeFriend := *openapiclient.NewCompleteFriend("FriendId_example", "FriendContactId_example") // CompleteFriend |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.VloopAPI.CompleteFriend(context.Background(), id).CompleteFriend(completeFriend).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VloopAPI.CompleteFriend``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCompleteFriendRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **completeFriend** | [**CompleteFriend**](CompleteFriend.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateFriend

> Friend CreateFriend(ctx, orgId, id, channelId).Friend(friend).Execute()

Create friend

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/vloopapi"
)

func main() {
    orgId := "orgId_example" // string | 
    id := "id_example" // string | 
    channelId := "channelId_example" // string | 
    friend := *openapiclient.NewFriend("Id_example", "ViralLoopId_example", "ReferralId_example", "ContactId_example", "ConversationId_example", "ChannelId_example", "Status_example", time.Now(), time.Now(), "Name_example", "TransactionId_example", "OrganizationId_example", "GatewayMessengerId_example", "ReferralActivityId_example", "MessengerId_example") // Friend |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.VloopAPI.CreateFriend(context.Background(), orgId, id, channelId).Friend(friend).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VloopAPI.CreateFriend``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateFriend`: Friend
    fmt.Fprintf(os.Stdout, "Response from `VloopAPI.CreateFriend`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**id** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateFriendRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **friend** | [**Friend**](Friend.md) |  | 

### Return type

[**Friend**](Friend.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateMilestone

> Milestone CreateMilestone(ctx, orgId, id, channelId).Milestone(milestone).Execute()

Create milestone



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/vloopapi"
)

func main() {
    orgId := "orgId_example" // string | 
    id := "id_example" // string | 
    channelId := "channelId_example" // string | 
    milestone := *openapiclient.NewMilestone("Id_example", "ViralLoopId_example", "Name_example", float32(123), time.Now(), time.Now()) // Milestone |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.VloopAPI.CreateMilestone(context.Background(), orgId, id, channelId).Milestone(milestone).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VloopAPI.CreateMilestone``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateMilestone`: Milestone
    fmt.Fprintf(os.Stdout, "Response from `VloopAPI.CreateMilestone`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**id** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateMilestoneRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **milestone** | [**Milestone**](Milestone.md) |  | 

### Return type

[**Milestone**](Milestone.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateReferral

> Referral CreateReferral(ctx, orgId, id, channelId).Referral(referral).Execute()

Create referral



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/vloopapi"
)

func main() {
    orgId := "orgId_example" // string | 
    id := "id_example" // string | 
    channelId := "channelId_example" // string | 
    referral := *openapiclient.NewReferral("Id_example", "ViralLoopId_example", "ContactId_example", "ConversationId_example", "ChannelId_example", "ReferralLink_example", time.Now(), time.Now(), "Name_example", "TransactionId_example", "OrganizationId_example", "GatewayMessengerId_example", "GatewayMessengerType_example", "MessengerId_example", false) // Referral |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.VloopAPI.CreateReferral(context.Background(), orgId, id, channelId).Referral(referral).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VloopAPI.CreateReferral``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateReferral`: Referral
    fmt.Fprintf(os.Stdout, "Response from `VloopAPI.CreateReferral`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**id** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateReferralRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **referral** | [**Referral**](Referral.md) |  | 

### Return type

[**Referral**](Referral.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateViralLoop

> ViralLoop CreateViralLoop(ctx).OrganizationId(organizationId).ChannelId(channelId).Limit(limit).ViralLoop(viralLoop).Execute()

Create Viral Loop

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/vloopapi"
)

func main() {
    organizationId := "organizationId_example" // string |  (optional)
    channelId := "channelId_example" // string |  (optional)
    limit := int32(56) // int32 |  (optional)
    viralLoop := *openapiclient.NewViralLoop("Id_example", "OrganizationId_example", "Type_example", "Name_example", time.Now(), time.Now(), "ChannelId_example") // ViralLoop |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.VloopAPI.CreateViralLoop(context.Background()).OrganizationId(organizationId).ChannelId(channelId).Limit(limit).ViralLoop(viralLoop).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VloopAPI.CreateViralLoop``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateViralLoop`: ViralLoop
    fmt.Fprintf(os.Stdout, "Response from `VloopAPI.CreateViralLoop`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateViralLoopRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | **string** |  | 
 **channelId** | **string** |  | 
 **limit** | **int32** |  | 
 **viralLoop** | [**ViralLoop**](ViralLoop.md) |  | 

### Return type

[**ViralLoop**](ViralLoop.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteMilestone

> DeleteMilestone(ctx, orgId, id, mid, channelId).Execute()

Delete milestone



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/vloopapi"
)

func main() {
    orgId := "orgId_example" // string | 
    id := "id_example" // string | 
    mid := "mid_example" // string | 
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.VloopAPI.DeleteMilestone(context.Background(), orgId, id, mid, channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VloopAPI.DeleteMilestone``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**id** | **string** |  | 
**mid** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteMilestoneRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------





### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteViralLoop

> DeleteViralLoop(ctx, orgId, id, channelId).Execute()

Delete Viral Loop



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/vloopapi"
)

func main() {
    orgId := "orgId_example" // string | 
    id := "id_example" // string | 
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.VloopAPI.DeleteViralLoop(context.Background(), orgId, id, channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VloopAPI.DeleteViralLoop``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**id** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteViralLoopRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## FindViralLoop

> ViralLoop FindViralLoop(ctx, orgId, id, channelId).Execute()

Find Viral Loop by ID



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/vloopapi"
)

func main() {
    orgId := "orgId_example" // string | 
    id := "id_example" // string | 
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.VloopAPI.FindViralLoop(context.Background(), orgId, id, channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VloopAPI.FindViralLoop``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `FindViralLoop`: ViralLoop
    fmt.Fprintf(os.Stdout, "Response from `VloopAPI.FindViralLoop`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**id** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiFindViralLoopRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**ViralLoop**](ViralLoop.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListViralLoops

> ListViralLoops200Response ListViralLoops(ctx).OrganizationId(organizationId).ChannelId(channelId).Limit(limit).Execute()

List Viral Loops



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/vloopapi"
)

func main() {
    organizationId := "organizationId_example" // string |  (optional)
    channelId := "channelId_example" // string |  (optional)
    limit := int32(56) // int32 |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.VloopAPI.ListViralLoops(context.Background()).OrganizationId(organizationId).ChannelId(channelId).Limit(limit).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VloopAPI.ListViralLoops``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListViralLoops`: ListViralLoops200Response
    fmt.Fprintf(os.Stdout, "Response from `VloopAPI.ListViralLoops`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListViralLoopsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | **string** |  | 
 **channelId** | **string** |  | 
 **limit** | **int32** |  | 

### Return type

[**ListViralLoops200Response**](ListViralLoops200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateMilestone

> Milestone UpdateMilestone(ctx, orgId, id, mid, channelId).Milestone(milestone).Execute()

Update milestone



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/vloopapi"
)

func main() {
    orgId := "orgId_example" // string | 
    id := "id_example" // string | 
    mid := "mid_example" // string | 
    channelId := "channelId_example" // string | 
    milestone := *openapiclient.NewMilestone("Id_example", "ViralLoopId_example", "Name_example", float32(123), time.Now(), time.Now()) // Milestone |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.VloopAPI.UpdateMilestone(context.Background(), orgId, id, mid, channelId).Milestone(milestone).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VloopAPI.UpdateMilestone``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateMilestone`: Milestone
    fmt.Fprintf(os.Stdout, "Response from `VloopAPI.UpdateMilestone`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**id** | **string** |  | 
**mid** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateMilestoneRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **milestone** | [**Milestone**](Milestone.md) |  | 

### Return type

[**Milestone**](Milestone.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateViralLoop

> ViralLoop UpdateViralLoop(ctx, orgId, id, channelId).ViralLoop(viralLoop).Execute()

Update Viral Loop



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/vloopapi"
)

func main() {
    orgId := "orgId_example" // string | 
    id := "id_example" // string | 
    channelId := "channelId_example" // string | 
    viralLoop := *openapiclient.NewViralLoop("Id_example", "OrganizationId_example", "Type_example", "Name_example", time.Now(), time.Now(), "ChannelId_example") // ViralLoop |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.VloopAPI.UpdateViralLoop(context.Background(), orgId, id, channelId).ViralLoop(viralLoop).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VloopAPI.UpdateViralLoop``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateViralLoop`: ViralLoop
    fmt.Fprintf(os.Stdout, "Response from `VloopAPI.UpdateViralLoop`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**id** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateViralLoopRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **viralLoop** | [**ViralLoop**](ViralLoop.md) |  | 

### Return type

[**ViralLoop**](ViralLoop.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## Webhook

> Webhook(ctx).WebhookRequest(webhookRequest).Execute()

Webhook



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/vloopapi"
)

func main() {
    webhookRequest := *openapiclient.NewWebhookRequest("TransactionId_example", "Type_example") // WebhookRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.VloopAPI.Webhook(context.Background()).WebhookRequest(webhookRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `VloopAPI.Webhook``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiWebhookRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookRequest** | [**WebhookRequest**](WebhookRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

