# ViralLoop

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**OrganizationId** | **string** |  | 
**Type** | **string** |  | 
**Name** | **string** |  | 
**Milestones** | Pointer to [**[]Milestone**](Milestone.md) |  | [optional] 
**ForwardScriptId** | Pointer to **string** |  | [optional] 
**FriendScriptId** | Pointer to **string** |  | [optional] 
**Attributes** | Pointer to **map[string]string** |  | [optional] 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 
**ChannelId** | **string** |  | 
**ReferralLinkAttributedEmailNotification** | Pointer to [**EmailNotification**](EmailNotification.md) |  | [optional] 
**ForwardLinkHash** | Pointer to **string** |  | [optional] 
**InteractionId** | Pointer to **string** |  | [optional] 
**LinkHash** | Pointer to **string** |  | [optional] 
**ReferralLinkAttributedScriptId** | Pointer to **string** |  | [optional] 

## Methods

### NewViralLoop

`func NewViralLoop(id string, organizationId string, type_ string, name string, createdAt time.Time, updatedAt time.Time, channelId string, ) *ViralLoop`

NewViralLoop instantiates a new ViralLoop object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewViralLoopWithDefaults

`func NewViralLoopWithDefaults() *ViralLoop`

NewViralLoopWithDefaults instantiates a new ViralLoop object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ViralLoop) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ViralLoop) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ViralLoop) SetId(v string)`

SetId sets Id field to given value.


### GetOrganizationId

`func (o *ViralLoop) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *ViralLoop) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *ViralLoop) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetType

`func (o *ViralLoop) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ViralLoop) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ViralLoop) SetType(v string)`

SetType sets Type field to given value.


### GetName

`func (o *ViralLoop) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ViralLoop) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ViralLoop) SetName(v string)`

SetName sets Name field to given value.


### GetMilestones

`func (o *ViralLoop) GetMilestones() []Milestone`

GetMilestones returns the Milestones field if non-nil, zero value otherwise.

### GetMilestonesOk

`func (o *ViralLoop) GetMilestonesOk() (*[]Milestone, bool)`

GetMilestonesOk returns a tuple with the Milestones field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMilestones

`func (o *ViralLoop) SetMilestones(v []Milestone)`

SetMilestones sets Milestones field to given value.

### HasMilestones

`func (o *ViralLoop) HasMilestones() bool`

HasMilestones returns a boolean if a field has been set.

### GetForwardScriptId

`func (o *ViralLoop) GetForwardScriptId() string`

GetForwardScriptId returns the ForwardScriptId field if non-nil, zero value otherwise.

### GetForwardScriptIdOk

`func (o *ViralLoop) GetForwardScriptIdOk() (*string, bool)`

GetForwardScriptIdOk returns a tuple with the ForwardScriptId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetForwardScriptId

`func (o *ViralLoop) SetForwardScriptId(v string)`

SetForwardScriptId sets ForwardScriptId field to given value.

### HasForwardScriptId

`func (o *ViralLoop) HasForwardScriptId() bool`

HasForwardScriptId returns a boolean if a field has been set.

### GetFriendScriptId

`func (o *ViralLoop) GetFriendScriptId() string`

GetFriendScriptId returns the FriendScriptId field if non-nil, zero value otherwise.

### GetFriendScriptIdOk

`func (o *ViralLoop) GetFriendScriptIdOk() (*string, bool)`

GetFriendScriptIdOk returns a tuple with the FriendScriptId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFriendScriptId

`func (o *ViralLoop) SetFriendScriptId(v string)`

SetFriendScriptId sets FriendScriptId field to given value.

### HasFriendScriptId

`func (o *ViralLoop) HasFriendScriptId() bool`

HasFriendScriptId returns a boolean if a field has been set.

### GetAttributes

`func (o *ViralLoop) GetAttributes() map[string]string`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *ViralLoop) GetAttributesOk() (*map[string]string, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *ViralLoop) SetAttributes(v map[string]string)`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *ViralLoop) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetCreatedAt

`func (o *ViralLoop) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *ViralLoop) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *ViralLoop) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *ViralLoop) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *ViralLoop) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *ViralLoop) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetChannelId

`func (o *ViralLoop) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *ViralLoop) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *ViralLoop) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetReferralLinkAttributedEmailNotification

`func (o *ViralLoop) GetReferralLinkAttributedEmailNotification() EmailNotification`

GetReferralLinkAttributedEmailNotification returns the ReferralLinkAttributedEmailNotification field if non-nil, zero value otherwise.

### GetReferralLinkAttributedEmailNotificationOk

`func (o *ViralLoop) GetReferralLinkAttributedEmailNotificationOk() (*EmailNotification, bool)`

GetReferralLinkAttributedEmailNotificationOk returns a tuple with the ReferralLinkAttributedEmailNotification field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferralLinkAttributedEmailNotification

`func (o *ViralLoop) SetReferralLinkAttributedEmailNotification(v EmailNotification)`

SetReferralLinkAttributedEmailNotification sets ReferralLinkAttributedEmailNotification field to given value.

### HasReferralLinkAttributedEmailNotification

`func (o *ViralLoop) HasReferralLinkAttributedEmailNotification() bool`

HasReferralLinkAttributedEmailNotification returns a boolean if a field has been set.

### GetForwardLinkHash

`func (o *ViralLoop) GetForwardLinkHash() string`

GetForwardLinkHash returns the ForwardLinkHash field if non-nil, zero value otherwise.

### GetForwardLinkHashOk

`func (o *ViralLoop) GetForwardLinkHashOk() (*string, bool)`

GetForwardLinkHashOk returns a tuple with the ForwardLinkHash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetForwardLinkHash

`func (o *ViralLoop) SetForwardLinkHash(v string)`

SetForwardLinkHash sets ForwardLinkHash field to given value.

### HasForwardLinkHash

`func (o *ViralLoop) HasForwardLinkHash() bool`

HasForwardLinkHash returns a boolean if a field has been set.

### GetInteractionId

`func (o *ViralLoop) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *ViralLoop) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *ViralLoop) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *ViralLoop) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetLinkHash

`func (o *ViralLoop) GetLinkHash() string`

GetLinkHash returns the LinkHash field if non-nil, zero value otherwise.

### GetLinkHashOk

`func (o *ViralLoop) GetLinkHashOk() (*string, bool)`

GetLinkHashOk returns a tuple with the LinkHash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLinkHash

`func (o *ViralLoop) SetLinkHash(v string)`

SetLinkHash sets LinkHash field to given value.

### HasLinkHash

`func (o *ViralLoop) HasLinkHash() bool`

HasLinkHash returns a boolean if a field has been set.

### GetReferralLinkAttributedScriptId

`func (o *ViralLoop) GetReferralLinkAttributedScriptId() string`

GetReferralLinkAttributedScriptId returns the ReferralLinkAttributedScriptId field if non-nil, zero value otherwise.

### GetReferralLinkAttributedScriptIdOk

`func (o *ViralLoop) GetReferralLinkAttributedScriptIdOk() (*string, bool)`

GetReferralLinkAttributedScriptIdOk returns a tuple with the ReferralLinkAttributedScriptId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferralLinkAttributedScriptId

`func (o *ViralLoop) SetReferralLinkAttributedScriptId(v string)`

SetReferralLinkAttributedScriptId sets ReferralLinkAttributedScriptId field to given value.

### HasReferralLinkAttributedScriptId

`func (o *ViralLoop) HasReferralLinkAttributedScriptId() bool`

HasReferralLinkAttributedScriptId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


