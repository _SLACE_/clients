# Referral

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**ViralLoopId** | **string** |  | 
**ContactId** | **string** |  | 
**ConversationId** | **string** |  | 
**ChannelId** | **string** |  | 
**CompletedMilestones** | Pointer to [**[]CompletedMilestone**](CompletedMilestone.md) |  | [optional] 
**CreatedFriends** | Pointer to [**[]Friend**](Friend.md) |  | [optional] 
**CompletedFriends** | Pointer to [**[]Friend**](Friend.md) |  | [optional] 
**Rewards** | Pointer to [**[]Reward**](Reward.md) |  | [optional] 
**ReferralLink** | **string** |  | 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 
**Attributes** | Pointer to **map[string]string** |  | [optional] 
**Name** | **string** |  | 
**TransactionId** | **string** |  | 
**OrganizationId** | **string** |  | 
**GatewayMessengerId** | **string** |  | 
**GatewayMessengerType** | **string** |  | 
**Email** | Pointer to **string** |  | [optional] 
**MessengerId** | **string** |  | 
**ForceNewLink** | **bool** |  | 

## Methods

### NewReferral

`func NewReferral(id string, viralLoopId string, contactId string, conversationId string, channelId string, referralLink string, createdAt time.Time, updatedAt time.Time, name string, transactionId string, organizationId string, gatewayMessengerId string, gatewayMessengerType string, messengerId string, forceNewLink bool, ) *Referral`

NewReferral instantiates a new Referral object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReferralWithDefaults

`func NewReferralWithDefaults() *Referral`

NewReferralWithDefaults instantiates a new Referral object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Referral) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Referral) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Referral) SetId(v string)`

SetId sets Id field to given value.


### GetViralLoopId

`func (o *Referral) GetViralLoopId() string`

GetViralLoopId returns the ViralLoopId field if non-nil, zero value otherwise.

### GetViralLoopIdOk

`func (o *Referral) GetViralLoopIdOk() (*string, bool)`

GetViralLoopIdOk returns a tuple with the ViralLoopId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetViralLoopId

`func (o *Referral) SetViralLoopId(v string)`

SetViralLoopId sets ViralLoopId field to given value.


### GetContactId

`func (o *Referral) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *Referral) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *Referral) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetConversationId

`func (o *Referral) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *Referral) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *Referral) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetChannelId

`func (o *Referral) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *Referral) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *Referral) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetCompletedMilestones

`func (o *Referral) GetCompletedMilestones() []CompletedMilestone`

GetCompletedMilestones returns the CompletedMilestones field if non-nil, zero value otherwise.

### GetCompletedMilestonesOk

`func (o *Referral) GetCompletedMilestonesOk() (*[]CompletedMilestone, bool)`

GetCompletedMilestonesOk returns a tuple with the CompletedMilestones field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompletedMilestones

`func (o *Referral) SetCompletedMilestones(v []CompletedMilestone)`

SetCompletedMilestones sets CompletedMilestones field to given value.

### HasCompletedMilestones

`func (o *Referral) HasCompletedMilestones() bool`

HasCompletedMilestones returns a boolean if a field has been set.

### GetCreatedFriends

`func (o *Referral) GetCreatedFriends() []Friend`

GetCreatedFriends returns the CreatedFriends field if non-nil, zero value otherwise.

### GetCreatedFriendsOk

`func (o *Referral) GetCreatedFriendsOk() (*[]Friend, bool)`

GetCreatedFriendsOk returns a tuple with the CreatedFriends field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedFriends

`func (o *Referral) SetCreatedFriends(v []Friend)`

SetCreatedFriends sets CreatedFriends field to given value.

### HasCreatedFriends

`func (o *Referral) HasCreatedFriends() bool`

HasCreatedFriends returns a boolean if a field has been set.

### GetCompletedFriends

`func (o *Referral) GetCompletedFriends() []Friend`

GetCompletedFriends returns the CompletedFriends field if non-nil, zero value otherwise.

### GetCompletedFriendsOk

`func (o *Referral) GetCompletedFriendsOk() (*[]Friend, bool)`

GetCompletedFriendsOk returns a tuple with the CompletedFriends field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompletedFriends

`func (o *Referral) SetCompletedFriends(v []Friend)`

SetCompletedFriends sets CompletedFriends field to given value.

### HasCompletedFriends

`func (o *Referral) HasCompletedFriends() bool`

HasCompletedFriends returns a boolean if a field has been set.

### GetRewards

`func (o *Referral) GetRewards() []Reward`

GetRewards returns the Rewards field if non-nil, zero value otherwise.

### GetRewardsOk

`func (o *Referral) GetRewardsOk() (*[]Reward, bool)`

GetRewardsOk returns a tuple with the Rewards field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRewards

`func (o *Referral) SetRewards(v []Reward)`

SetRewards sets Rewards field to given value.

### HasRewards

`func (o *Referral) HasRewards() bool`

HasRewards returns a boolean if a field has been set.

### GetReferralLink

`func (o *Referral) GetReferralLink() string`

GetReferralLink returns the ReferralLink field if non-nil, zero value otherwise.

### GetReferralLinkOk

`func (o *Referral) GetReferralLinkOk() (*string, bool)`

GetReferralLinkOk returns a tuple with the ReferralLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferralLink

`func (o *Referral) SetReferralLink(v string)`

SetReferralLink sets ReferralLink field to given value.


### GetCreatedAt

`func (o *Referral) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Referral) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Referral) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *Referral) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Referral) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Referral) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetAttributes

`func (o *Referral) GetAttributes() map[string]string`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *Referral) GetAttributesOk() (*map[string]string, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *Referral) SetAttributes(v map[string]string)`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *Referral) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetName

`func (o *Referral) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Referral) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Referral) SetName(v string)`

SetName sets Name field to given value.


### GetTransactionId

`func (o *Referral) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *Referral) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *Referral) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetOrganizationId

`func (o *Referral) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *Referral) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *Referral) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetGatewayMessengerId

`func (o *Referral) GetGatewayMessengerId() string`

GetGatewayMessengerId returns the GatewayMessengerId field if non-nil, zero value otherwise.

### GetGatewayMessengerIdOk

`func (o *Referral) GetGatewayMessengerIdOk() (*string, bool)`

GetGatewayMessengerIdOk returns a tuple with the GatewayMessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayMessengerId

`func (o *Referral) SetGatewayMessengerId(v string)`

SetGatewayMessengerId sets GatewayMessengerId field to given value.


### GetGatewayMessengerType

`func (o *Referral) GetGatewayMessengerType() string`

GetGatewayMessengerType returns the GatewayMessengerType field if non-nil, zero value otherwise.

### GetGatewayMessengerTypeOk

`func (o *Referral) GetGatewayMessengerTypeOk() (*string, bool)`

GetGatewayMessengerTypeOk returns a tuple with the GatewayMessengerType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayMessengerType

`func (o *Referral) SetGatewayMessengerType(v string)`

SetGatewayMessengerType sets GatewayMessengerType field to given value.


### GetEmail

`func (o *Referral) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *Referral) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *Referral) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *Referral) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetMessengerId

`func (o *Referral) GetMessengerId() string`

GetMessengerId returns the MessengerId field if non-nil, zero value otherwise.

### GetMessengerIdOk

`func (o *Referral) GetMessengerIdOk() (*string, bool)`

GetMessengerIdOk returns a tuple with the MessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerId

`func (o *Referral) SetMessengerId(v string)`

SetMessengerId sets MessengerId field to given value.


### GetForceNewLink

`func (o *Referral) GetForceNewLink() bool`

GetForceNewLink returns the ForceNewLink field if non-nil, zero value otherwise.

### GetForceNewLinkOk

`func (o *Referral) GetForceNewLinkOk() (*bool, bool)`

GetForceNewLinkOk returns a tuple with the ForceNewLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetForceNewLink

`func (o *Referral) SetForceNewLink(v bool)`

SetForceNewLink sets ForceNewLink field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


