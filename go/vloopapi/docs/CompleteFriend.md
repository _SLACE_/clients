# CompleteFriend

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Attributes** | Pointer to **map[string]string** |  | [optional] 
**FriendId** | **string** | required if no friend_contact_id | 
**FriendContactId** | **string** | required if no friend_id | 

## Methods

### NewCompleteFriend

`func NewCompleteFriend(friendId string, friendContactId string, ) *CompleteFriend`

NewCompleteFriend instantiates a new CompleteFriend object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCompleteFriendWithDefaults

`func NewCompleteFriendWithDefaults() *CompleteFriend`

NewCompleteFriendWithDefaults instantiates a new CompleteFriend object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAttributes

`func (o *CompleteFriend) GetAttributes() map[string]string`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *CompleteFriend) GetAttributesOk() (*map[string]string, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *CompleteFriend) SetAttributes(v map[string]string)`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *CompleteFriend) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetFriendId

`func (o *CompleteFriend) GetFriendId() string`

GetFriendId returns the FriendId field if non-nil, zero value otherwise.

### GetFriendIdOk

`func (o *CompleteFriend) GetFriendIdOk() (*string, bool)`

GetFriendIdOk returns a tuple with the FriendId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFriendId

`func (o *CompleteFriend) SetFriendId(v string)`

SetFriendId sets FriendId field to given value.


### GetFriendContactId

`func (o *CompleteFriend) GetFriendContactId() string`

GetFriendContactId returns the FriendContactId field if non-nil, zero value otherwise.

### GetFriendContactIdOk

`func (o *CompleteFriend) GetFriendContactIdOk() (*string, bool)`

GetFriendContactIdOk returns a tuple with the FriendContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFriendContactId

`func (o *CompleteFriend) SetFriendContactId(v string)`

SetFriendContactId sets FriendContactId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


