# RewardConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ScriptId** | Pointer to **string** |  | [optional] 
**EmailNotification** | Pointer to [**EmailNotification**](EmailNotification.md) |  | [optional] 

## Methods

### NewRewardConfig

`func NewRewardConfig() *RewardConfig`

NewRewardConfig instantiates a new RewardConfig object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRewardConfigWithDefaults

`func NewRewardConfigWithDefaults() *RewardConfig`

NewRewardConfigWithDefaults instantiates a new RewardConfig object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetScriptId

`func (o *RewardConfig) GetScriptId() string`

GetScriptId returns the ScriptId field if non-nil, zero value otherwise.

### GetScriptIdOk

`func (o *RewardConfig) GetScriptIdOk() (*string, bool)`

GetScriptIdOk returns a tuple with the ScriptId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScriptId

`func (o *RewardConfig) SetScriptId(v string)`

SetScriptId sets ScriptId field to given value.

### HasScriptId

`func (o *RewardConfig) HasScriptId() bool`

HasScriptId returns a boolean if a field has been set.

### GetEmailNotification

`func (o *RewardConfig) GetEmailNotification() EmailNotification`

GetEmailNotification returns the EmailNotification field if non-nil, zero value otherwise.

### GetEmailNotificationOk

`func (o *RewardConfig) GetEmailNotificationOk() (*EmailNotification, bool)`

GetEmailNotificationOk returns a tuple with the EmailNotification field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmailNotification

`func (o *RewardConfig) SetEmailNotification(v EmailNotification)`

SetEmailNotification sets EmailNotification field to given value.

### HasEmailNotification

`func (o *RewardConfig) HasEmailNotification() bool`

HasEmailNotification returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


