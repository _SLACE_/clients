# Friend

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**ViralLoopId** | **string** |  | 
**ReferralId** | **string** |  | 
**ContactId** | **string** |  | 
**ConversationId** | **string** |  | 
**ChannelId** | **string** |  | 
**Rewards** | Pointer to [**[]Reward**](Reward.md) |  | [optional] 
**Status** | **string** |  | 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 
**Attributes** | Pointer to **map[string]string** |  | [optional] 
**Name** | **string** |  | 
**TransactionId** | **string** |  | 
**OrganizationId** | **string** |  | 
**GatewayMessengerId** | **string** |  | 
**Email** | Pointer to **string** |  | [optional] 
**ReferralActivityId** | **string** |  | 
**MessengerId** | **string** |  | 

## Methods

### NewFriend

`func NewFriend(id string, viralLoopId string, referralId string, contactId string, conversationId string, channelId string, status string, createdAt time.Time, updatedAt time.Time, name string, transactionId string, organizationId string, gatewayMessengerId string, referralActivityId string, messengerId string, ) *Friend`

NewFriend instantiates a new Friend object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFriendWithDefaults

`func NewFriendWithDefaults() *Friend`

NewFriendWithDefaults instantiates a new Friend object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Friend) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Friend) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Friend) SetId(v string)`

SetId sets Id field to given value.


### GetViralLoopId

`func (o *Friend) GetViralLoopId() string`

GetViralLoopId returns the ViralLoopId field if non-nil, zero value otherwise.

### GetViralLoopIdOk

`func (o *Friend) GetViralLoopIdOk() (*string, bool)`

GetViralLoopIdOk returns a tuple with the ViralLoopId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetViralLoopId

`func (o *Friend) SetViralLoopId(v string)`

SetViralLoopId sets ViralLoopId field to given value.


### GetReferralId

`func (o *Friend) GetReferralId() string`

GetReferralId returns the ReferralId field if non-nil, zero value otherwise.

### GetReferralIdOk

`func (o *Friend) GetReferralIdOk() (*string, bool)`

GetReferralIdOk returns a tuple with the ReferralId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferralId

`func (o *Friend) SetReferralId(v string)`

SetReferralId sets ReferralId field to given value.


### GetContactId

`func (o *Friend) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *Friend) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *Friend) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetConversationId

`func (o *Friend) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *Friend) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *Friend) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetChannelId

`func (o *Friend) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *Friend) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *Friend) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetRewards

`func (o *Friend) GetRewards() []Reward`

GetRewards returns the Rewards field if non-nil, zero value otherwise.

### GetRewardsOk

`func (o *Friend) GetRewardsOk() (*[]Reward, bool)`

GetRewardsOk returns a tuple with the Rewards field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRewards

`func (o *Friend) SetRewards(v []Reward)`

SetRewards sets Rewards field to given value.

### HasRewards

`func (o *Friend) HasRewards() bool`

HasRewards returns a boolean if a field has been set.

### GetStatus

`func (o *Friend) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *Friend) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *Friend) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetCreatedAt

`func (o *Friend) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Friend) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Friend) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *Friend) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Friend) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Friend) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetAttributes

`func (o *Friend) GetAttributes() map[string]string`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *Friend) GetAttributesOk() (*map[string]string, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *Friend) SetAttributes(v map[string]string)`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *Friend) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetName

`func (o *Friend) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Friend) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Friend) SetName(v string)`

SetName sets Name field to given value.


### GetTransactionId

`func (o *Friend) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *Friend) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *Friend) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetOrganizationId

`func (o *Friend) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *Friend) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *Friend) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetGatewayMessengerId

`func (o *Friend) GetGatewayMessengerId() string`

GetGatewayMessengerId returns the GatewayMessengerId field if non-nil, zero value otherwise.

### GetGatewayMessengerIdOk

`func (o *Friend) GetGatewayMessengerIdOk() (*string, bool)`

GetGatewayMessengerIdOk returns a tuple with the GatewayMessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayMessengerId

`func (o *Friend) SetGatewayMessengerId(v string)`

SetGatewayMessengerId sets GatewayMessengerId field to given value.


### GetEmail

`func (o *Friend) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *Friend) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *Friend) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *Friend) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetReferralActivityId

`func (o *Friend) GetReferralActivityId() string`

GetReferralActivityId returns the ReferralActivityId field if non-nil, zero value otherwise.

### GetReferralActivityIdOk

`func (o *Friend) GetReferralActivityIdOk() (*string, bool)`

GetReferralActivityIdOk returns a tuple with the ReferralActivityId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferralActivityId

`func (o *Friend) SetReferralActivityId(v string)`

SetReferralActivityId sets ReferralActivityId field to given value.


### GetMessengerId

`func (o *Friend) GetMessengerId() string`

GetMessengerId returns the MessengerId field if non-nil, zero value otherwise.

### GetMessengerIdOk

`func (o *Friend) GetMessengerIdOk() (*string, bool)`

GetMessengerIdOk returns a tuple with the MessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerId

`func (o *Friend) SetMessengerId(v string)`

SetMessengerId sets MessengerId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


