# ListViralLoops200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | Pointer to [**[]ViralLoop**](ViralLoop.md) |  | [optional] 
**Total** | Pointer to **int32** |  | [optional] 

## Methods

### NewListViralLoops200Response

`func NewListViralLoops200Response() *ListViralLoops200Response`

NewListViralLoops200Response instantiates a new ListViralLoops200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListViralLoops200ResponseWithDefaults

`func NewListViralLoops200ResponseWithDefaults() *ListViralLoops200Response`

NewListViralLoops200ResponseWithDefaults instantiates a new ListViralLoops200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *ListViralLoops200Response) GetResults() []ViralLoop`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *ListViralLoops200Response) GetResultsOk() (*[]ViralLoop, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *ListViralLoops200Response) SetResults(v []ViralLoop)`

SetResults sets Results field to given value.

### HasResults

`func (o *ListViralLoops200Response) HasResults() bool`

HasResults returns a boolean if a field has been set.

### GetTotal

`func (o *ListViralLoops200Response) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *ListViralLoops200Response) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *ListViralLoops200Response) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *ListViralLoops200Response) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


