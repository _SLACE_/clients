# \EntryPointsAPI

All URIs are relative to *https://ep.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateEntryPoint**](EntryPointsAPI.md#CreateEntryPoint) | **Post** /management/entry-points | Create Entry Point
[**CreateSimpleEntryPoint**](EntryPointsAPI.md#CreateSimpleEntryPoint) | **Post** /channels/{channel_id}/entry-points | Create Entry Point
[**DeleteBatchesJobWithRelatedData**](EntryPointsAPI.md#DeleteBatchesJobWithRelatedData) | **Delete** /management/entry-points/jobs/{job_id} | Delete batches job with related data
[**DeleteEntryPoint**](EntryPointsAPI.md#DeleteEntryPoint) | **Delete** /management/entry-points/{id} | Delete Entry Point
[**FileRefreshJob**](EntryPointsAPI.md#FileRefreshJob) | **Post** /management/entry-points/jobs/{jobId}/file-refresh | Refresh batches job file
[**GetEntryPoint**](EntryPointsAPI.md#GetEntryPoint) | **Get** /management/entry-points/{id} | Get Entry Point
[**ListEntryPoints**](EntryPointsAPI.md#ListEntryPoints) | **Get** /management/entry-points | List Entry Points
[**PostEntryPointsOrgId**](EntryPointsAPI.md#PostEntryPointsOrgId) | **Post** /entry-points/{orgId} | Create entry point admin
[**UpdateEntryPoint**](EntryPointsAPI.md#UpdateEntryPoint) | **Put** /management/entry-points/{id} | Update Entry Point
[**UpdateExistingEntryPoint**](EntryPointsAPI.md#UpdateExistingEntryPoint) | **Patch** /channels/{channel_id}/entry-points/{id} | Update Entry Point
[**UpdateInteractionAssignment**](EntryPointsAPI.md#UpdateInteractionAssignment) | **Post** /management/entry-points/assignment/interaction | Assign entry points to another interaction



## CreateEntryPoint

> AdminEntryPoint CreateEntryPoint(ctx).AdminEntryPointNewData(adminEntryPointNewData).Execute()

Create Entry Point

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    adminEntryPointNewData := *openapiclient.NewAdminEntryPointNewData(openapiclient.AdminEntryPointType("campaign"), "OrganizationId_example") // AdminEntryPointNewData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.EntryPointsAPI.CreateEntryPoint(context.Background()).AdminEntryPointNewData(adminEntryPointNewData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EntryPointsAPI.CreateEntryPoint``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateEntryPoint`: AdminEntryPoint
    fmt.Fprintf(os.Stdout, "Response from `EntryPointsAPI.CreateEntryPoint`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateEntryPointRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **adminEntryPointNewData** | [**AdminEntryPointNewData**](AdminEntryPointNewData.md) |  | 

### Return type

[**AdminEntryPoint**](AdminEntryPoint.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateSimpleEntryPoint

> EntryPoint CreateSimpleEntryPoint(ctx, channelId).EntryPointData(entryPointData).Execute()

Create Entry Point



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    channelId := "channelId_example" // string | 
    entryPointData := *openapiclient.NewEntryPointData() // EntryPointData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.EntryPointsAPI.CreateSimpleEntryPoint(context.Background(), channelId).EntryPointData(entryPointData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EntryPointsAPI.CreateSimpleEntryPoint``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateSimpleEntryPoint`: EntryPoint
    fmt.Fprintf(os.Stdout, "Response from `EntryPointsAPI.CreateSimpleEntryPoint`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateSimpleEntryPointRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **entryPointData** | [**EntryPointData**](EntryPointData.md) |  | 

### Return type

[**EntryPoint**](EntryPoint.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteBatchesJobWithRelatedData

> DeleteBatchesJobWithRelatedData(ctx, jobId).Execute()

Delete batches job with related data

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    jobId := "jobId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.EntryPointsAPI.DeleteBatchesJobWithRelatedData(context.Background(), jobId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EntryPointsAPI.DeleteBatchesJobWithRelatedData``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**jobId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteBatchesJobWithRelatedDataRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteEntryPoint

> GenericResponse DeleteEntryPoint(ctx, id).Execute()

Delete Entry Point

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.EntryPointsAPI.DeleteEntryPoint(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EntryPointsAPI.DeleteEntryPoint``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteEntryPoint`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `EntryPointsAPI.DeleteEntryPoint`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteEntryPointRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## FileRefreshJob

> FileRefreshJob(ctx, jobId).Execute()

Refresh batches job file

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    jobId := "jobId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.EntryPointsAPI.FileRefreshJob(context.Background(), jobId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EntryPointsAPI.FileRefreshJob``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**jobId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiFileRefreshJobRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetEntryPoint

> AdminEntryPoint GetEntryPoint(ctx, id).Execute()

Get Entry Point



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.EntryPointsAPI.GetEntryPoint(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EntryPointsAPI.GetEntryPoint``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetEntryPoint`: AdminEntryPoint
    fmt.Fprintf(os.Stdout, "Response from `EntryPointsAPI.GetEntryPoint`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetEntryPointRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**AdminEntryPoint**](AdminEntryPoint.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListEntryPoints

> AdminEntryPointsList ListEntryPoints(ctx).Id(id).Type_(type_).Types(types).OrganizationId(organizationId).ChannelId(channelId).OrderedBatchId(orderedBatchId).Url(url).MessageLinkHash(messageLinkHash).InteractionId(interactionId).Search(search).Offset(offset).Limit(limit).Sort(sort).BatchesJobId(batchesJobId).Execute()

List Entry Points

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    id := "id_example" // string |  (optional)
    type_ := "type__example" // string |  (optional)
    types := []string{"Inner_example"} // []string |  (optional)
    organizationId := "organizationId_example" // string |  (optional)
    channelId := "channelId_example" // string |  (optional)
    orderedBatchId := "orderedBatchId_example" // string |  (optional)
    url := "url_example" // string |  (optional)
    messageLinkHash := "messageLinkHash_example" // string |  (optional)
    interactionId := "interactionId_example" // string |  (optional)
    search := "search_example" // string |  (optional)
    offset := int32(56) // int32 |  (optional)
    limit := int32(56) // int32 |  (optional)
    sort := "sort_example" // string |  (optional)
    batchesJobId := "batchesJobId_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.EntryPointsAPI.ListEntryPoints(context.Background()).Id(id).Type_(type_).Types(types).OrganizationId(organizationId).ChannelId(channelId).OrderedBatchId(orderedBatchId).Url(url).MessageLinkHash(messageLinkHash).InteractionId(interactionId).Search(search).Offset(offset).Limit(limit).Sort(sort).BatchesJobId(batchesJobId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EntryPointsAPI.ListEntryPoints``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListEntryPoints`: AdminEntryPointsList
    fmt.Fprintf(os.Stdout, "Response from `EntryPointsAPI.ListEntryPoints`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListEntryPointsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string** |  | 
 **type_** | **string** |  | 
 **types** | **[]string** |  | 
 **organizationId** | **string** |  | 
 **channelId** | **string** |  | 
 **orderedBatchId** | **string** |  | 
 **url** | **string** |  | 
 **messageLinkHash** | **string** |  | 
 **interactionId** | **string** |  | 
 **search** | **string** |  | 
 **offset** | **int32** |  | 
 **limit** | **int32** |  | 
 **sort** | **string** |  | 
 **batchesJobId** | **string** |  | 

### Return type

[**AdminEntryPointsList**](AdminEntryPointsList.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostEntryPointsOrgId

> EntryPoint PostEntryPointsOrgId(ctx, orgId).EntryPointData(entryPointData).Execute()

Create entry point admin



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    orgId := "orgId_example" // string | 
    entryPointData := *openapiclient.NewEntryPointData() // EntryPointData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.EntryPointsAPI.PostEntryPointsOrgId(context.Background(), orgId).EntryPointData(entryPointData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EntryPointsAPI.PostEntryPointsOrgId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PostEntryPointsOrgId`: EntryPoint
    fmt.Fprintf(os.Stdout, "Response from `EntryPointsAPI.PostEntryPointsOrgId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPostEntryPointsOrgIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **entryPointData** | [**EntryPointData**](EntryPointData.md) |  | 

### Return type

[**EntryPoint**](EntryPoint.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateEntryPoint

> EntryPoint UpdateEntryPoint(ctx, id).AdminEntryPointMutable(adminEntryPointMutable).Execute()

Update Entry Point

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    id := "id_example" // string | 
    adminEntryPointMutable := *openapiclient.NewAdminEntryPointMutable(openapiclient.AdminEntryPointType("campaign"), "OrganizationId_example") // AdminEntryPointMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.EntryPointsAPI.UpdateEntryPoint(context.Background(), id).AdminEntryPointMutable(adminEntryPointMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EntryPointsAPI.UpdateEntryPoint``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateEntryPoint`: EntryPoint
    fmt.Fprintf(os.Stdout, "Response from `EntryPointsAPI.UpdateEntryPoint`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateEntryPointRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **adminEntryPointMutable** | [**AdminEntryPointMutable**](AdminEntryPointMutable.md) |  | 

### Return type

[**EntryPoint**](EntryPoint.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateExistingEntryPoint

> EntryPoint UpdateExistingEntryPoint(ctx, channelId, id).UpdateEntryPointRequest(updateEntryPointRequest).Execute()

Update Entry Point



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 
    updateEntryPointRequest := *openapiclient.NewUpdateEntryPointRequest("ChannelId_example") // UpdateEntryPointRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.EntryPointsAPI.UpdateExistingEntryPoint(context.Background(), channelId, id).UpdateEntryPointRequest(updateEntryPointRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EntryPointsAPI.UpdateExistingEntryPoint``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateExistingEntryPoint`: EntryPoint
    fmt.Fprintf(os.Stdout, "Response from `EntryPointsAPI.UpdateExistingEntryPoint`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateExistingEntryPointRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **updateEntryPointRequest** | [**UpdateEntryPointRequest**](UpdateEntryPointRequest.md) |  | 

### Return type

[**EntryPoint**](EntryPoint.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateInteractionAssignment

> UpdateInteractionAssignment(ctx).InteractionAssignmentRequest(interactionAssignmentRequest).Execute()

Assign entry points to another interaction



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    interactionAssignmentRequest := *openapiclient.NewInteractionAssignmentRequest("ChannelId_example", "FromInteractionId_example", "ToInteractionId_example") // InteractionAssignmentRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.EntryPointsAPI.UpdateInteractionAssignment(context.Background()).InteractionAssignmentRequest(interactionAssignmentRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `EntryPointsAPI.UpdateInteractionAssignment``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUpdateInteractionAssignmentRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **interactionAssignmentRequest** | [**InteractionAssignmentRequest**](InteractionAssignmentRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

