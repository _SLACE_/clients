# EntryPointType

## Enum


* `QR` (value: `"qr"`)

* `NFC` (value: `"nfc"`)

* `LINK` (value: `"link"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


