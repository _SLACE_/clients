# AdminEntryPointType

## Enum


* `CAMPAIGN` (value: `"campaign"`)

* `EXTERNAL` (value: `"external"`)

* `INTERACTION` (value: `"interaction"`)

* `UNKNOWN` (value: `"unknown"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


