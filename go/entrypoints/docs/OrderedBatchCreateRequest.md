# OrderedBatchCreateRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to **string** |  | [optional] 
**OrganizationId** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**Count** | **int32** |  | 
**MessageLinkHash** | Pointer to **string** |  | [optional] 
**InteractionId** | Pointer to **string** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**BatchId** | Pointer to **string** |  | [optional] 
**CustomerExperienceId** | Pointer to **string** |  | [optional] 

## Methods

### NewOrderedBatchCreateRequest

`func NewOrderedBatchCreateRequest(count int32, ) *OrderedBatchCreateRequest`

NewOrderedBatchCreateRequest instantiates a new OrderedBatchCreateRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrderedBatchCreateRequestWithDefaults

`func NewOrderedBatchCreateRequestWithDefaults() *OrderedBatchCreateRequest`

NewOrderedBatchCreateRequestWithDefaults instantiates a new OrderedBatchCreateRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *OrderedBatchCreateRequest) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *OrderedBatchCreateRequest) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *OrderedBatchCreateRequest) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *OrderedBatchCreateRequest) HasType() bool`

HasType returns a boolean if a field has been set.

### GetOrganizationId

`func (o *OrderedBatchCreateRequest) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *OrderedBatchCreateRequest) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *OrderedBatchCreateRequest) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *OrderedBatchCreateRequest) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetChannelId

`func (o *OrderedBatchCreateRequest) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *OrderedBatchCreateRequest) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *OrderedBatchCreateRequest) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *OrderedBatchCreateRequest) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetCount

`func (o *OrderedBatchCreateRequest) GetCount() int32`

GetCount returns the Count field if non-nil, zero value otherwise.

### GetCountOk

`func (o *OrderedBatchCreateRequest) GetCountOk() (*int32, bool)`

GetCountOk returns a tuple with the Count field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCount

`func (o *OrderedBatchCreateRequest) SetCount(v int32)`

SetCount sets Count field to given value.


### GetMessageLinkHash

`func (o *OrderedBatchCreateRequest) GetMessageLinkHash() string`

GetMessageLinkHash returns the MessageLinkHash field if non-nil, zero value otherwise.

### GetMessageLinkHashOk

`func (o *OrderedBatchCreateRequest) GetMessageLinkHashOk() (*string, bool)`

GetMessageLinkHashOk returns a tuple with the MessageLinkHash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageLinkHash

`func (o *OrderedBatchCreateRequest) SetMessageLinkHash(v string)`

SetMessageLinkHash sets MessageLinkHash field to given value.

### HasMessageLinkHash

`func (o *OrderedBatchCreateRequest) HasMessageLinkHash() bool`

HasMessageLinkHash returns a boolean if a field has been set.

### GetInteractionId

`func (o *OrderedBatchCreateRequest) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *OrderedBatchCreateRequest) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *OrderedBatchCreateRequest) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *OrderedBatchCreateRequest) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetName

`func (o *OrderedBatchCreateRequest) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *OrderedBatchCreateRequest) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *OrderedBatchCreateRequest) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *OrderedBatchCreateRequest) HasName() bool`

HasName returns a boolean if a field has been set.

### GetBatchId

`func (o *OrderedBatchCreateRequest) GetBatchId() string`

GetBatchId returns the BatchId field if non-nil, zero value otherwise.

### GetBatchIdOk

`func (o *OrderedBatchCreateRequest) GetBatchIdOk() (*string, bool)`

GetBatchIdOk returns a tuple with the BatchId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBatchId

`func (o *OrderedBatchCreateRequest) SetBatchId(v string)`

SetBatchId sets BatchId field to given value.

### HasBatchId

`func (o *OrderedBatchCreateRequest) HasBatchId() bool`

HasBatchId returns a boolean if a field has been set.

### GetCustomerExperienceId

`func (o *OrderedBatchCreateRequest) GetCustomerExperienceId() string`

GetCustomerExperienceId returns the CustomerExperienceId field if non-nil, zero value otherwise.

### GetCustomerExperienceIdOk

`func (o *OrderedBatchCreateRequest) GetCustomerExperienceIdOk() (*string, bool)`

GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerExperienceId

`func (o *OrderedBatchCreateRequest) SetCustomerExperienceId(v string)`

SetCustomerExperienceId sets CustomerExperienceId field to given value.

### HasCustomerExperienceId

`func (o *OrderedBatchCreateRequest) HasCustomerExperienceId() bool`

HasCustomerExperienceId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


