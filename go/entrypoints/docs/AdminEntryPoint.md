# AdminEntryPoint

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Type** | [**AdminEntryPointType**](AdminEntryPointType.md) |  | 
**OrganizationId** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**InteractionId** | Pointer to **string** |  | [optional] 
**MessageLinkHash** | Pointer to **string** |  | [optional] 
**OrderedBatchId** | Pointer to **string** |  | [optional] 
**Url** | Pointer to **string** |  | [optional] 
**UrlHash** | Pointer to **string** |  | [optional] 
**ExpiredUrl** | Pointer to **string** |  | [optional] 
**Ttl** | **int32** |  | 
**Data** | Pointer to **map[string]string** |  | [optional] 
**Params** | Pointer to **map[string]string** |  | [optional] 
**CreatedAt** | **time.Time** |  | 
**CustomerExperienceId** | Pointer to **string** | Any text without &#39;.&#39; characters. Preferred over interaction_id if keeping the reference to the latest version of interaction is required. | [optional] 

## Methods

### NewAdminEntryPoint

`func NewAdminEntryPoint(id string, type_ AdminEntryPointType, ttl int32, createdAt time.Time, ) *AdminEntryPoint`

NewAdminEntryPoint instantiates a new AdminEntryPoint object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAdminEntryPointWithDefaults

`func NewAdminEntryPointWithDefaults() *AdminEntryPoint`

NewAdminEntryPointWithDefaults instantiates a new AdminEntryPoint object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *AdminEntryPoint) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *AdminEntryPoint) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *AdminEntryPoint) SetId(v string)`

SetId sets Id field to given value.


### GetType

`func (o *AdminEntryPoint) GetType() AdminEntryPointType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *AdminEntryPoint) GetTypeOk() (*AdminEntryPointType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *AdminEntryPoint) SetType(v AdminEntryPointType)`

SetType sets Type field to given value.


### GetOrganizationId

`func (o *AdminEntryPoint) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *AdminEntryPoint) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *AdminEntryPoint) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *AdminEntryPoint) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetChannelId

`func (o *AdminEntryPoint) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *AdminEntryPoint) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *AdminEntryPoint) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *AdminEntryPoint) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetInteractionId

`func (o *AdminEntryPoint) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *AdminEntryPoint) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *AdminEntryPoint) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *AdminEntryPoint) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetMessageLinkHash

`func (o *AdminEntryPoint) GetMessageLinkHash() string`

GetMessageLinkHash returns the MessageLinkHash field if non-nil, zero value otherwise.

### GetMessageLinkHashOk

`func (o *AdminEntryPoint) GetMessageLinkHashOk() (*string, bool)`

GetMessageLinkHashOk returns a tuple with the MessageLinkHash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageLinkHash

`func (o *AdminEntryPoint) SetMessageLinkHash(v string)`

SetMessageLinkHash sets MessageLinkHash field to given value.

### HasMessageLinkHash

`func (o *AdminEntryPoint) HasMessageLinkHash() bool`

HasMessageLinkHash returns a boolean if a field has been set.

### GetOrderedBatchId

`func (o *AdminEntryPoint) GetOrderedBatchId() string`

GetOrderedBatchId returns the OrderedBatchId field if non-nil, zero value otherwise.

### GetOrderedBatchIdOk

`func (o *AdminEntryPoint) GetOrderedBatchIdOk() (*string, bool)`

GetOrderedBatchIdOk returns a tuple with the OrderedBatchId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderedBatchId

`func (o *AdminEntryPoint) SetOrderedBatchId(v string)`

SetOrderedBatchId sets OrderedBatchId field to given value.

### HasOrderedBatchId

`func (o *AdminEntryPoint) HasOrderedBatchId() bool`

HasOrderedBatchId returns a boolean if a field has been set.

### GetUrl

`func (o *AdminEntryPoint) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *AdminEntryPoint) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *AdminEntryPoint) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *AdminEntryPoint) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetUrlHash

`func (o *AdminEntryPoint) GetUrlHash() string`

GetUrlHash returns the UrlHash field if non-nil, zero value otherwise.

### GetUrlHashOk

`func (o *AdminEntryPoint) GetUrlHashOk() (*string, bool)`

GetUrlHashOk returns a tuple with the UrlHash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrlHash

`func (o *AdminEntryPoint) SetUrlHash(v string)`

SetUrlHash sets UrlHash field to given value.

### HasUrlHash

`func (o *AdminEntryPoint) HasUrlHash() bool`

HasUrlHash returns a boolean if a field has been set.

### GetExpiredUrl

`func (o *AdminEntryPoint) GetExpiredUrl() string`

GetExpiredUrl returns the ExpiredUrl field if non-nil, zero value otherwise.

### GetExpiredUrlOk

`func (o *AdminEntryPoint) GetExpiredUrlOk() (*string, bool)`

GetExpiredUrlOk returns a tuple with the ExpiredUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiredUrl

`func (o *AdminEntryPoint) SetExpiredUrl(v string)`

SetExpiredUrl sets ExpiredUrl field to given value.

### HasExpiredUrl

`func (o *AdminEntryPoint) HasExpiredUrl() bool`

HasExpiredUrl returns a boolean if a field has been set.

### GetTtl

`func (o *AdminEntryPoint) GetTtl() int32`

GetTtl returns the Ttl field if non-nil, zero value otherwise.

### GetTtlOk

`func (o *AdminEntryPoint) GetTtlOk() (*int32, bool)`

GetTtlOk returns a tuple with the Ttl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTtl

`func (o *AdminEntryPoint) SetTtl(v int32)`

SetTtl sets Ttl field to given value.


### GetData

`func (o *AdminEntryPoint) GetData() map[string]string`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *AdminEntryPoint) GetDataOk() (*map[string]string, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *AdminEntryPoint) SetData(v map[string]string)`

SetData sets Data field to given value.

### HasData

`func (o *AdminEntryPoint) HasData() bool`

HasData returns a boolean if a field has been set.

### GetParams

`func (o *AdminEntryPoint) GetParams() map[string]string`

GetParams returns the Params field if non-nil, zero value otherwise.

### GetParamsOk

`func (o *AdminEntryPoint) GetParamsOk() (*map[string]string, bool)`

GetParamsOk returns a tuple with the Params field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParams

`func (o *AdminEntryPoint) SetParams(v map[string]string)`

SetParams sets Params field to given value.

### HasParams

`func (o *AdminEntryPoint) HasParams() bool`

HasParams returns a boolean if a field has been set.

### GetCreatedAt

`func (o *AdminEntryPoint) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *AdminEntryPoint) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *AdminEntryPoint) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetCustomerExperienceId

`func (o *AdminEntryPoint) GetCustomerExperienceId() string`

GetCustomerExperienceId returns the CustomerExperienceId field if non-nil, zero value otherwise.

### GetCustomerExperienceIdOk

`func (o *AdminEntryPoint) GetCustomerExperienceIdOk() (*string, bool)`

GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerExperienceId

`func (o *AdminEntryPoint) SetCustomerExperienceId(v string)`

SetCustomerExperienceId sets CustomerExperienceId field to given value.

### HasCustomerExperienceId

`func (o *AdminEntryPoint) HasCustomerExperienceId() bool`

HasCustomerExperienceId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


