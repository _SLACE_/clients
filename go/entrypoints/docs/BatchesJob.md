# BatchesJob

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**OrderedBatchId** | Pointer to **string** |  | [optional] 
**OrderedBatch** | Pointer to **map[string]interface{}** |  | [optional] 
**MediaId** | Pointer to **string** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 
**Status** | Pointer to **string** |  | [optional] 
**Progress** | Pointer to **string** |  | [optional] 
**FailedReason** | Pointer to **string** |  | [optional] 
**BatchType** | Pointer to **string** |  | [optional] 
**CreatedAt** | Pointer to **time.Time** |  | [optional] 
**UpdatedAt** | Pointer to **time.Time** |  | [optional] 

## Methods

### NewBatchesJob

`func NewBatchesJob() *BatchesJob`

NewBatchesJob instantiates a new BatchesJob object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBatchesJobWithDefaults

`func NewBatchesJobWithDefaults() *BatchesJob`

NewBatchesJobWithDefaults instantiates a new BatchesJob object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *BatchesJob) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *BatchesJob) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *BatchesJob) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *BatchesJob) HasId() bool`

HasId returns a boolean if a field has been set.

### GetOrderedBatchId

`func (o *BatchesJob) GetOrderedBatchId() string`

GetOrderedBatchId returns the OrderedBatchId field if non-nil, zero value otherwise.

### GetOrderedBatchIdOk

`func (o *BatchesJob) GetOrderedBatchIdOk() (*string, bool)`

GetOrderedBatchIdOk returns a tuple with the OrderedBatchId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderedBatchId

`func (o *BatchesJob) SetOrderedBatchId(v string)`

SetOrderedBatchId sets OrderedBatchId field to given value.

### HasOrderedBatchId

`func (o *BatchesJob) HasOrderedBatchId() bool`

HasOrderedBatchId returns a boolean if a field has been set.

### GetOrderedBatch

`func (o *BatchesJob) GetOrderedBatch() map[string]interface{}`

GetOrderedBatch returns the OrderedBatch field if non-nil, zero value otherwise.

### GetOrderedBatchOk

`func (o *BatchesJob) GetOrderedBatchOk() (*map[string]interface{}, bool)`

GetOrderedBatchOk returns a tuple with the OrderedBatch field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderedBatch

`func (o *BatchesJob) SetOrderedBatch(v map[string]interface{})`

SetOrderedBatch sets OrderedBatch field to given value.

### HasOrderedBatch

`func (o *BatchesJob) HasOrderedBatch() bool`

HasOrderedBatch returns a boolean if a field has been set.

### GetMediaId

`func (o *BatchesJob) GetMediaId() string`

GetMediaId returns the MediaId field if non-nil, zero value otherwise.

### GetMediaIdOk

`func (o *BatchesJob) GetMediaIdOk() (*string, bool)`

GetMediaIdOk returns a tuple with the MediaId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaId

`func (o *BatchesJob) SetMediaId(v string)`

SetMediaId sets MediaId field to given value.

### HasMediaId

`func (o *BatchesJob) HasMediaId() bool`

HasMediaId returns a boolean if a field has been set.

### GetType

`func (o *BatchesJob) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *BatchesJob) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *BatchesJob) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *BatchesJob) HasType() bool`

HasType returns a boolean if a field has been set.

### GetStatus

`func (o *BatchesJob) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *BatchesJob) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *BatchesJob) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *BatchesJob) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetProgress

`func (o *BatchesJob) GetProgress() string`

GetProgress returns the Progress field if non-nil, zero value otherwise.

### GetProgressOk

`func (o *BatchesJob) GetProgressOk() (*string, bool)`

GetProgressOk returns a tuple with the Progress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProgress

`func (o *BatchesJob) SetProgress(v string)`

SetProgress sets Progress field to given value.

### HasProgress

`func (o *BatchesJob) HasProgress() bool`

HasProgress returns a boolean if a field has been set.

### GetFailedReason

`func (o *BatchesJob) GetFailedReason() string`

GetFailedReason returns the FailedReason field if non-nil, zero value otherwise.

### GetFailedReasonOk

`func (o *BatchesJob) GetFailedReasonOk() (*string, bool)`

GetFailedReasonOk returns a tuple with the FailedReason field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFailedReason

`func (o *BatchesJob) SetFailedReason(v string)`

SetFailedReason sets FailedReason field to given value.

### HasFailedReason

`func (o *BatchesJob) HasFailedReason() bool`

HasFailedReason returns a boolean if a field has been set.

### GetBatchType

`func (o *BatchesJob) GetBatchType() string`

GetBatchType returns the BatchType field if non-nil, zero value otherwise.

### GetBatchTypeOk

`func (o *BatchesJob) GetBatchTypeOk() (*string, bool)`

GetBatchTypeOk returns a tuple with the BatchType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBatchType

`func (o *BatchesJob) SetBatchType(v string)`

SetBatchType sets BatchType field to given value.

### HasBatchType

`func (o *BatchesJob) HasBatchType() bool`

HasBatchType returns a boolean if a field has been set.

### GetCreatedAt

`func (o *BatchesJob) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *BatchesJob) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *BatchesJob) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.

### HasCreatedAt

`func (o *BatchesJob) HasCreatedAt() bool`

HasCreatedAt returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *BatchesJob) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *BatchesJob) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *BatchesJob) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *BatchesJob) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


