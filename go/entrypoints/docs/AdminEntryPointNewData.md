# AdminEntryPointNewData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | [**AdminEntryPointType**](AdminEntryPointType.md) |  | 
**OrganizationId** | **string** |  | 
**ChannelId** | Pointer to **string** | required if type !&#x3D; external | [optional] 
**InteractionId** | Pointer to **string** | required if type &#x3D;&#x3D; interaction | [optional] 
**Url** | Pointer to **string** | required if type &#x3D;&#x3D; external | [optional] 
**MessageLinkHash** | Pointer to **string** | required if type &#x3D;&#x3D; campaign | [optional] 
**ExpiredUrl** | Pointer to **string** | Optional url to which entry point will redirect when expired | [optional] 
**Ttl** | Pointer to **int32** | time to live in seconds | [optional] 
**Params** | Pointer to **map[string]string** |  | [optional] 
**Data** | Pointer to **map[string]string** |  | [optional] 
**CustomerExperienceId** | Pointer to **string** | Any text without &#39;.&#39; characters. Preferred over interaction_id if keeping the reference to the latest version of interaction is required. | [optional] 
**Hash** | Pointer to **string** | Custom entry point hash | [optional] 
**HashPrefix** | Pointer to **string** |  | [optional] 
**HashLength** | Pointer to **int32** |  | [optional] 

## Methods

### NewAdminEntryPointNewData

`func NewAdminEntryPointNewData(type_ AdminEntryPointType, organizationId string, ) *AdminEntryPointNewData`

NewAdminEntryPointNewData instantiates a new AdminEntryPointNewData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAdminEntryPointNewDataWithDefaults

`func NewAdminEntryPointNewDataWithDefaults() *AdminEntryPointNewData`

NewAdminEntryPointNewDataWithDefaults instantiates a new AdminEntryPointNewData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *AdminEntryPointNewData) GetType() AdminEntryPointType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *AdminEntryPointNewData) GetTypeOk() (*AdminEntryPointType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *AdminEntryPointNewData) SetType(v AdminEntryPointType)`

SetType sets Type field to given value.


### GetOrganizationId

`func (o *AdminEntryPointNewData) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *AdminEntryPointNewData) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *AdminEntryPointNewData) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetChannelId

`func (o *AdminEntryPointNewData) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *AdminEntryPointNewData) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *AdminEntryPointNewData) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *AdminEntryPointNewData) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetInteractionId

`func (o *AdminEntryPointNewData) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *AdminEntryPointNewData) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *AdminEntryPointNewData) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *AdminEntryPointNewData) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetUrl

`func (o *AdminEntryPointNewData) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *AdminEntryPointNewData) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *AdminEntryPointNewData) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *AdminEntryPointNewData) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetMessageLinkHash

`func (o *AdminEntryPointNewData) GetMessageLinkHash() string`

GetMessageLinkHash returns the MessageLinkHash field if non-nil, zero value otherwise.

### GetMessageLinkHashOk

`func (o *AdminEntryPointNewData) GetMessageLinkHashOk() (*string, bool)`

GetMessageLinkHashOk returns a tuple with the MessageLinkHash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageLinkHash

`func (o *AdminEntryPointNewData) SetMessageLinkHash(v string)`

SetMessageLinkHash sets MessageLinkHash field to given value.

### HasMessageLinkHash

`func (o *AdminEntryPointNewData) HasMessageLinkHash() bool`

HasMessageLinkHash returns a boolean if a field has been set.

### GetExpiredUrl

`func (o *AdminEntryPointNewData) GetExpiredUrl() string`

GetExpiredUrl returns the ExpiredUrl field if non-nil, zero value otherwise.

### GetExpiredUrlOk

`func (o *AdminEntryPointNewData) GetExpiredUrlOk() (*string, bool)`

GetExpiredUrlOk returns a tuple with the ExpiredUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiredUrl

`func (o *AdminEntryPointNewData) SetExpiredUrl(v string)`

SetExpiredUrl sets ExpiredUrl field to given value.

### HasExpiredUrl

`func (o *AdminEntryPointNewData) HasExpiredUrl() bool`

HasExpiredUrl returns a boolean if a field has been set.

### GetTtl

`func (o *AdminEntryPointNewData) GetTtl() int32`

GetTtl returns the Ttl field if non-nil, zero value otherwise.

### GetTtlOk

`func (o *AdminEntryPointNewData) GetTtlOk() (*int32, bool)`

GetTtlOk returns a tuple with the Ttl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTtl

`func (o *AdminEntryPointNewData) SetTtl(v int32)`

SetTtl sets Ttl field to given value.

### HasTtl

`func (o *AdminEntryPointNewData) HasTtl() bool`

HasTtl returns a boolean if a field has been set.

### GetParams

`func (o *AdminEntryPointNewData) GetParams() map[string]string`

GetParams returns the Params field if non-nil, zero value otherwise.

### GetParamsOk

`func (o *AdminEntryPointNewData) GetParamsOk() (*map[string]string, bool)`

GetParamsOk returns a tuple with the Params field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParams

`func (o *AdminEntryPointNewData) SetParams(v map[string]string)`

SetParams sets Params field to given value.

### HasParams

`func (o *AdminEntryPointNewData) HasParams() bool`

HasParams returns a boolean if a field has been set.

### GetData

`func (o *AdminEntryPointNewData) GetData() map[string]string`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *AdminEntryPointNewData) GetDataOk() (*map[string]string, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *AdminEntryPointNewData) SetData(v map[string]string)`

SetData sets Data field to given value.

### HasData

`func (o *AdminEntryPointNewData) HasData() bool`

HasData returns a boolean if a field has been set.

### GetCustomerExperienceId

`func (o *AdminEntryPointNewData) GetCustomerExperienceId() string`

GetCustomerExperienceId returns the CustomerExperienceId field if non-nil, zero value otherwise.

### GetCustomerExperienceIdOk

`func (o *AdminEntryPointNewData) GetCustomerExperienceIdOk() (*string, bool)`

GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerExperienceId

`func (o *AdminEntryPointNewData) SetCustomerExperienceId(v string)`

SetCustomerExperienceId sets CustomerExperienceId field to given value.

### HasCustomerExperienceId

`func (o *AdminEntryPointNewData) HasCustomerExperienceId() bool`

HasCustomerExperienceId returns a boolean if a field has been set.

### GetHash

`func (o *AdminEntryPointNewData) GetHash() string`

GetHash returns the Hash field if non-nil, zero value otherwise.

### GetHashOk

`func (o *AdminEntryPointNewData) GetHashOk() (*string, bool)`

GetHashOk returns a tuple with the Hash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHash

`func (o *AdminEntryPointNewData) SetHash(v string)`

SetHash sets Hash field to given value.

### HasHash

`func (o *AdminEntryPointNewData) HasHash() bool`

HasHash returns a boolean if a field has been set.

### GetHashPrefix

`func (o *AdminEntryPointNewData) GetHashPrefix() string`

GetHashPrefix returns the HashPrefix field if non-nil, zero value otherwise.

### GetHashPrefixOk

`func (o *AdminEntryPointNewData) GetHashPrefixOk() (*string, bool)`

GetHashPrefixOk returns a tuple with the HashPrefix field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHashPrefix

`func (o *AdminEntryPointNewData) SetHashPrefix(v string)`

SetHashPrefix sets HashPrefix field to given value.

### HasHashPrefix

`func (o *AdminEntryPointNewData) HasHashPrefix() bool`

HasHashPrefix returns a boolean if a field has been set.

### GetHashLength

`func (o *AdminEntryPointNewData) GetHashLength() int32`

GetHashLength returns the HashLength field if non-nil, zero value otherwise.

### GetHashLengthOk

`func (o *AdminEntryPointNewData) GetHashLengthOk() (*int32, bool)`

GetHashLengthOk returns a tuple with the HashLength field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHashLength

`func (o *AdminEntryPointNewData) SetHashLength(v int32)`

SetHashLength sets HashLength field to given value.

### HasHashLength

`func (o *AdminEntryPointNewData) HasHashLength() bool`

HasHashLength returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


