# \OrderedBatchesAPI

All URIs are relative to *https://ep.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateOrderedBatch**](OrderedBatchesAPI.md#CreateOrderedBatch) | **Post** /management/ordered-batch | Create Ordered Batch
[**ListOrderedBatches**](OrderedBatchesAPI.md#ListOrderedBatches) | **Get** /management/ordered-batch | List Ordered Batches
[**OrderBatchesV2**](OrderedBatchesAPI.md#OrderBatchesV2) | **Post** /management/ordered-batch-v2 | Order batches v2
[**PostManagementEntryPointsAssignmentV2**](OrderedBatchesAPI.md#PostManagementEntryPointsAssignmentV2) | **Post** /management/entry-points/assignment-v2 | Ordered batches assignment v2
[**PostManagementEntryPointsDataImportV2**](OrderedBatchesAPI.md#PostManagementEntryPointsDataImportV2) | **Post** /management/entry-points/data-import-v2 | Data import ordered batches v2



## CreateOrderedBatch

> OrderedBatch CreateOrderedBatch(ctx).OrderedBatchCreateRequest(orderedBatchCreateRequest).Execute()

Create Ordered Batch

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    orderedBatchCreateRequest := *openapiclient.NewOrderedBatchCreateRequest(int32(123)) // OrderedBatchCreateRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrderedBatchesAPI.CreateOrderedBatch(context.Background()).OrderedBatchCreateRequest(orderedBatchCreateRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrderedBatchesAPI.CreateOrderedBatch``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateOrderedBatch`: OrderedBatch
    fmt.Fprintf(os.Stdout, "Response from `OrderedBatchesAPI.CreateOrderedBatch`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateOrderedBatchRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderedBatchCreateRequest** | [**OrderedBatchCreateRequest**](OrderedBatchCreateRequest.md) |  | 

### Return type

[**OrderedBatch**](OrderedBatch.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListOrderedBatches

> []OrderedBatch ListOrderedBatches(ctx).Execute()

List Ordered Batches

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrderedBatchesAPI.ListOrderedBatches(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrderedBatchesAPI.ListOrderedBatches``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListOrderedBatches`: []OrderedBatch
    fmt.Fprintf(os.Stdout, "Response from `OrderedBatchesAPI.ListOrderedBatches`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiListOrderedBatchesRequest struct via the builder pattern


### Return type

[**[]OrderedBatch**](OrderedBatch.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## OrderBatchesV2

> BatchesJob OrderBatchesV2(ctx).OrderBatchesV2Request(orderBatchesV2Request).Execute()

Order batches v2

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    orderBatchesV2Request := *openapiclient.NewOrderBatchesV2Request() // OrderBatchesV2Request |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.OrderedBatchesAPI.OrderBatchesV2(context.Background()).OrderBatchesV2Request(orderBatchesV2Request).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrderedBatchesAPI.OrderBatchesV2``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `OrderBatchesV2`: BatchesJob
    fmt.Fprintf(os.Stdout, "Response from `OrderedBatchesAPI.OrderBatchesV2`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiOrderBatchesV2Request struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderBatchesV2Request** | [**OrderBatchesV2Request**](OrderBatchesV2Request.md) |  | 

### Return type

[**BatchesJob**](BatchesJob.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostManagementEntryPointsAssignmentV2

> PostManagementEntryPointsAssignmentV2(ctx).Execute()

Ordered batches assignment v2

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.OrderedBatchesAPI.PostManagementEntryPointsAssignmentV2(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrderedBatchesAPI.PostManagementEntryPointsAssignmentV2``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiPostManagementEntryPointsAssignmentV2Request struct via the builder pattern


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostManagementEntryPointsDataImportV2

> PostManagementEntryPointsDataImportV2(ctx).Execute()

Data import ordered batches v2

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.OrderedBatchesAPI.PostManagementEntryPointsDataImportV2(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `OrderedBatchesAPI.PostManagementEntryPointsDataImportV2``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiPostManagementEntryPointsDataImportV2Request struct via the builder pattern


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

