# OrderBatchesV2Request

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to **string** |  | [optional] 
**OrganizationId** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**Count** | Pointer to **int32** |  | [optional] 
**Size** | Pointer to **string** |  | [optional] 
**MessageLinkHash** | Pointer to **string** |  | [optional] 
**InteractionId** | Pointer to **string** |  | [optional] 
**CustomerExperienceId** | Pointer to **string** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**BatchId** | Pointer to **string** |  | [optional] 

## Methods

### NewOrderBatchesV2Request

`func NewOrderBatchesV2Request() *OrderBatchesV2Request`

NewOrderBatchesV2Request instantiates a new OrderBatchesV2Request object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOrderBatchesV2RequestWithDefaults

`func NewOrderBatchesV2RequestWithDefaults() *OrderBatchesV2Request`

NewOrderBatchesV2RequestWithDefaults instantiates a new OrderBatchesV2Request object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *OrderBatchesV2Request) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *OrderBatchesV2Request) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *OrderBatchesV2Request) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *OrderBatchesV2Request) HasType() bool`

HasType returns a boolean if a field has been set.

### GetOrganizationId

`func (o *OrderBatchesV2Request) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *OrderBatchesV2Request) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *OrderBatchesV2Request) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *OrderBatchesV2Request) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetChannelId

`func (o *OrderBatchesV2Request) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *OrderBatchesV2Request) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *OrderBatchesV2Request) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *OrderBatchesV2Request) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetCount

`func (o *OrderBatchesV2Request) GetCount() int32`

GetCount returns the Count field if non-nil, zero value otherwise.

### GetCountOk

`func (o *OrderBatchesV2Request) GetCountOk() (*int32, bool)`

GetCountOk returns a tuple with the Count field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCount

`func (o *OrderBatchesV2Request) SetCount(v int32)`

SetCount sets Count field to given value.

### HasCount

`func (o *OrderBatchesV2Request) HasCount() bool`

HasCount returns a boolean if a field has been set.

### GetSize

`func (o *OrderBatchesV2Request) GetSize() string`

GetSize returns the Size field if non-nil, zero value otherwise.

### GetSizeOk

`func (o *OrderBatchesV2Request) GetSizeOk() (*string, bool)`

GetSizeOk returns a tuple with the Size field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSize

`func (o *OrderBatchesV2Request) SetSize(v string)`

SetSize sets Size field to given value.

### HasSize

`func (o *OrderBatchesV2Request) HasSize() bool`

HasSize returns a boolean if a field has been set.

### GetMessageLinkHash

`func (o *OrderBatchesV2Request) GetMessageLinkHash() string`

GetMessageLinkHash returns the MessageLinkHash field if non-nil, zero value otherwise.

### GetMessageLinkHashOk

`func (o *OrderBatchesV2Request) GetMessageLinkHashOk() (*string, bool)`

GetMessageLinkHashOk returns a tuple with the MessageLinkHash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageLinkHash

`func (o *OrderBatchesV2Request) SetMessageLinkHash(v string)`

SetMessageLinkHash sets MessageLinkHash field to given value.

### HasMessageLinkHash

`func (o *OrderBatchesV2Request) HasMessageLinkHash() bool`

HasMessageLinkHash returns a boolean if a field has been set.

### GetInteractionId

`func (o *OrderBatchesV2Request) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *OrderBatchesV2Request) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *OrderBatchesV2Request) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *OrderBatchesV2Request) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetCustomerExperienceId

`func (o *OrderBatchesV2Request) GetCustomerExperienceId() string`

GetCustomerExperienceId returns the CustomerExperienceId field if non-nil, zero value otherwise.

### GetCustomerExperienceIdOk

`func (o *OrderBatchesV2Request) GetCustomerExperienceIdOk() (*string, bool)`

GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerExperienceId

`func (o *OrderBatchesV2Request) SetCustomerExperienceId(v string)`

SetCustomerExperienceId sets CustomerExperienceId field to given value.

### HasCustomerExperienceId

`func (o *OrderBatchesV2Request) HasCustomerExperienceId() bool`

HasCustomerExperienceId returns a boolean if a field has been set.

### GetName

`func (o *OrderBatchesV2Request) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *OrderBatchesV2Request) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *OrderBatchesV2Request) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *OrderBatchesV2Request) HasName() bool`

HasName returns a boolean if a field has been set.

### GetBatchId

`func (o *OrderBatchesV2Request) GetBatchId() string`

GetBatchId returns the BatchId field if non-nil, zero value otherwise.

### GetBatchIdOk

`func (o *OrderBatchesV2Request) GetBatchIdOk() (*string, bool)`

GetBatchIdOk returns a tuple with the BatchId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBatchId

`func (o *OrderBatchesV2Request) SetBatchId(v string)`

SetBatchId sets BatchId field to given value.

### HasBatchId

`func (o *OrderBatchesV2Request) HasBatchId() bool`

HasBatchId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


