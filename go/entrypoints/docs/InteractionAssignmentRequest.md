# InteractionAssignmentRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelId** | **string** |  | 
**FromInteractionId** | **string** |  | 
**ToInteractionId** | **string** |  | 

## Methods

### NewInteractionAssignmentRequest

`func NewInteractionAssignmentRequest(channelId string, fromInteractionId string, toInteractionId string, ) *InteractionAssignmentRequest`

NewInteractionAssignmentRequest instantiates a new InteractionAssignmentRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInteractionAssignmentRequestWithDefaults

`func NewInteractionAssignmentRequestWithDefaults() *InteractionAssignmentRequest`

NewInteractionAssignmentRequestWithDefaults instantiates a new InteractionAssignmentRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetChannelId

`func (o *InteractionAssignmentRequest) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *InteractionAssignmentRequest) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *InteractionAssignmentRequest) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetFromInteractionId

`func (o *InteractionAssignmentRequest) GetFromInteractionId() string`

GetFromInteractionId returns the FromInteractionId field if non-nil, zero value otherwise.

### GetFromInteractionIdOk

`func (o *InteractionAssignmentRequest) GetFromInteractionIdOk() (*string, bool)`

GetFromInteractionIdOk returns a tuple with the FromInteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFromInteractionId

`func (o *InteractionAssignmentRequest) SetFromInteractionId(v string)`

SetFromInteractionId sets FromInteractionId field to given value.


### GetToInteractionId

`func (o *InteractionAssignmentRequest) GetToInteractionId() string`

GetToInteractionId returns the ToInteractionId field if non-nil, zero value otherwise.

### GetToInteractionIdOk

`func (o *InteractionAssignmentRequest) GetToInteractionIdOk() (*string, bool)`

GetToInteractionIdOk returns a tuple with the ToInteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetToInteractionId

`func (o *InteractionAssignmentRequest) SetToInteractionId(v string)`

SetToInteractionId sets ToInteractionId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


