# \DefaultAPI

All URIs are relative to *https://ep.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ExportEntrypoints**](DefaultAPI.md#ExportEntrypoints) | **Get** /management/entry-points/export | Export pre-filtered EntryPoints



## ExportEntrypoints

> ExportEntrypoints(ctx).Search(search).OrganizationId(organizationId).ChannelId(channelId).Types(types).Sort(sort).Execute()

Export pre-filtered EntryPoints

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    search := "search_example" // string |  (optional)
    organizationId := "organizationId_example" // string |  (optional)
    channelId := "channelId_example" // string |  (optional)
    types := "types_example" // string |  (optional)
    sort := "sort_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.DefaultAPI.ExportEntrypoints(context.Background()).Search(search).OrganizationId(organizationId).ChannelId(channelId).Types(types).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.ExportEntrypoints``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiExportEntrypointsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **string** |  | 
 **organizationId** | **string** |  | 
 **channelId** | **string** |  | 
 **types** | **string** |  | 
 **sort** | **string** |  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

