# \BatchesJobsAPI

All URIs are relative to *https://ep.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetManagementEntryPointsJobs**](BatchesJobsAPI.md#GetManagementEntryPointsJobs) | **Get** /management/entry-points/jobs | Get batches jobs list
[**PostManagementEntryPointsJobIdFileGenerationRetry**](BatchesJobsAPI.md#PostManagementEntryPointsJobIdFileGenerationRetry) | **Post** /management/entry-points/{job_id}/file-generation-retry | Retry job file generation
[**PostManagementEntryPointsJobsJobIdDownload**](BatchesJobsAPI.md#PostManagementEntryPointsJobsJobIdDownload) | **Post** /management/entry-points/jobs/{job_id}/download | Download ordered batch file from job



## GetManagementEntryPointsJobs

> GetManagementEntryPointsJobs200Response GetManagementEntryPointsJobs(ctx).Execute()

Get batches jobs list

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BatchesJobsAPI.GetManagementEntryPointsJobs(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BatchesJobsAPI.GetManagementEntryPointsJobs``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetManagementEntryPointsJobs`: GetManagementEntryPointsJobs200Response
    fmt.Fprintf(os.Stdout, "Response from `BatchesJobsAPI.GetManagementEntryPointsJobs`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetManagementEntryPointsJobsRequest struct via the builder pattern


### Return type

[**GetManagementEntryPointsJobs200Response**](GetManagementEntryPointsJobs200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostManagementEntryPointsJobIdFileGenerationRetry

> PostManagementEntryPointsJobIdFileGenerationRetry(ctx, jobId).Execute()

Retry job file generation

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    jobId := "jobId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.BatchesJobsAPI.PostManagementEntryPointsJobIdFileGenerationRetry(context.Background(), jobId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BatchesJobsAPI.PostManagementEntryPointsJobIdFileGenerationRetry``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**jobId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPostManagementEntryPointsJobIdFileGenerationRetryRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostManagementEntryPointsJobsJobIdDownload

> PostManagementEntryPointsJobsJobIdDownload(ctx, jobId).Execute()

Download ordered batch file from job

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func main() {
    jobId := "jobId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.BatchesJobsAPI.PostManagementEntryPointsJobsJobIdDownload(context.Background(), jobId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BatchesJobsAPI.PostManagementEntryPointsJobsJobIdDownload``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**jobId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPostManagementEntryPointsJobsJobIdDownloadRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

