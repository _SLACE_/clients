# UpdateEntryPointRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelId** | **string** |  | 
**InteractionId** | Pointer to **string** |  | [optional] 
**MessageLinkHash** | Pointer to **string** |  | [optional] 
**DynamicData** | Pointer to **map[string]interface{}** |  | [optional] 
**StaticData** | Pointer to **map[string]interface{}** |  | [optional] 

## Methods

### NewUpdateEntryPointRequest

`func NewUpdateEntryPointRequest(channelId string, ) *UpdateEntryPointRequest`

NewUpdateEntryPointRequest instantiates a new UpdateEntryPointRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateEntryPointRequestWithDefaults

`func NewUpdateEntryPointRequestWithDefaults() *UpdateEntryPointRequest`

NewUpdateEntryPointRequestWithDefaults instantiates a new UpdateEntryPointRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetChannelId

`func (o *UpdateEntryPointRequest) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *UpdateEntryPointRequest) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *UpdateEntryPointRequest) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetInteractionId

`func (o *UpdateEntryPointRequest) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *UpdateEntryPointRequest) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *UpdateEntryPointRequest) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *UpdateEntryPointRequest) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetMessageLinkHash

`func (o *UpdateEntryPointRequest) GetMessageLinkHash() string`

GetMessageLinkHash returns the MessageLinkHash field if non-nil, zero value otherwise.

### GetMessageLinkHashOk

`func (o *UpdateEntryPointRequest) GetMessageLinkHashOk() (*string, bool)`

GetMessageLinkHashOk returns a tuple with the MessageLinkHash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageLinkHash

`func (o *UpdateEntryPointRequest) SetMessageLinkHash(v string)`

SetMessageLinkHash sets MessageLinkHash field to given value.

### HasMessageLinkHash

`func (o *UpdateEntryPointRequest) HasMessageLinkHash() bool`

HasMessageLinkHash returns a boolean if a field has been set.

### GetDynamicData

`func (o *UpdateEntryPointRequest) GetDynamicData() map[string]interface{}`

GetDynamicData returns the DynamicData field if non-nil, zero value otherwise.

### GetDynamicDataOk

`func (o *UpdateEntryPointRequest) GetDynamicDataOk() (*map[string]interface{}, bool)`

GetDynamicDataOk returns a tuple with the DynamicData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDynamicData

`func (o *UpdateEntryPointRequest) SetDynamicData(v map[string]interface{})`

SetDynamicData sets DynamicData field to given value.

### HasDynamicData

`func (o *UpdateEntryPointRequest) HasDynamicData() bool`

HasDynamicData returns a boolean if a field has been set.

### GetStaticData

`func (o *UpdateEntryPointRequest) GetStaticData() map[string]interface{}`

GetStaticData returns the StaticData field if non-nil, zero value otherwise.

### GetStaticDataOk

`func (o *UpdateEntryPointRequest) GetStaticDataOk() (*map[string]interface{}, bool)`

GetStaticDataOk returns a tuple with the StaticData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStaticData

`func (o *UpdateEntryPointRequest) SetStaticData(v map[string]interface{})`

SetStaticData sets StaticData field to given value.

### HasStaticData

`func (o *UpdateEntryPointRequest) HasStaticData() bool`

HasStaticData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


