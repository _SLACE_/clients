# AdminEntryPointMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | [**AdminEntryPointType**](AdminEntryPointType.md) |  | 
**OrganizationId** | **string** |  | 
**ChannelId** | Pointer to **string** | required if type !&#x3D; external | [optional] 
**InteractionId** | Pointer to **string** | required if type &#x3D;&#x3D; interaction | [optional] 
**Url** | Pointer to **string** | required if type &#x3D;&#x3D; external | [optional] 
**MessageLinkHash** | Pointer to **string** | required if type &#x3D;&#x3D; campaign | [optional] 
**ExpiredUrl** | Pointer to **string** | Optional url to which entry point will redirect when expired | [optional] 
**Ttl** | Pointer to **int32** | time to live in seconds | [optional] 
**Params** | Pointer to **map[string]string** |  | [optional] 
**Data** | Pointer to **map[string]string** |  | [optional] 
**CustomerExperienceId** | Pointer to **string** | Any text without &#39;.&#39; characters. Preferred over interaction_id if keeping the reference to the latest version of interaction is required. | [optional] 

## Methods

### NewAdminEntryPointMutable

`func NewAdminEntryPointMutable(type_ AdminEntryPointType, organizationId string, ) *AdminEntryPointMutable`

NewAdminEntryPointMutable instantiates a new AdminEntryPointMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAdminEntryPointMutableWithDefaults

`func NewAdminEntryPointMutableWithDefaults() *AdminEntryPointMutable`

NewAdminEntryPointMutableWithDefaults instantiates a new AdminEntryPointMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *AdminEntryPointMutable) GetType() AdminEntryPointType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *AdminEntryPointMutable) GetTypeOk() (*AdminEntryPointType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *AdminEntryPointMutable) SetType(v AdminEntryPointType)`

SetType sets Type field to given value.


### GetOrganizationId

`func (o *AdminEntryPointMutable) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *AdminEntryPointMutable) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *AdminEntryPointMutable) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetChannelId

`func (o *AdminEntryPointMutable) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *AdminEntryPointMutable) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *AdminEntryPointMutable) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *AdminEntryPointMutable) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetInteractionId

`func (o *AdminEntryPointMutable) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *AdminEntryPointMutable) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *AdminEntryPointMutable) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *AdminEntryPointMutable) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetUrl

`func (o *AdminEntryPointMutable) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *AdminEntryPointMutable) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *AdminEntryPointMutable) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *AdminEntryPointMutable) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetMessageLinkHash

`func (o *AdminEntryPointMutable) GetMessageLinkHash() string`

GetMessageLinkHash returns the MessageLinkHash field if non-nil, zero value otherwise.

### GetMessageLinkHashOk

`func (o *AdminEntryPointMutable) GetMessageLinkHashOk() (*string, bool)`

GetMessageLinkHashOk returns a tuple with the MessageLinkHash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageLinkHash

`func (o *AdminEntryPointMutable) SetMessageLinkHash(v string)`

SetMessageLinkHash sets MessageLinkHash field to given value.

### HasMessageLinkHash

`func (o *AdminEntryPointMutable) HasMessageLinkHash() bool`

HasMessageLinkHash returns a boolean if a field has been set.

### GetExpiredUrl

`func (o *AdminEntryPointMutable) GetExpiredUrl() string`

GetExpiredUrl returns the ExpiredUrl field if non-nil, zero value otherwise.

### GetExpiredUrlOk

`func (o *AdminEntryPointMutable) GetExpiredUrlOk() (*string, bool)`

GetExpiredUrlOk returns a tuple with the ExpiredUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiredUrl

`func (o *AdminEntryPointMutable) SetExpiredUrl(v string)`

SetExpiredUrl sets ExpiredUrl field to given value.

### HasExpiredUrl

`func (o *AdminEntryPointMutable) HasExpiredUrl() bool`

HasExpiredUrl returns a boolean if a field has been set.

### GetTtl

`func (o *AdminEntryPointMutable) GetTtl() int32`

GetTtl returns the Ttl field if non-nil, zero value otherwise.

### GetTtlOk

`func (o *AdminEntryPointMutable) GetTtlOk() (*int32, bool)`

GetTtlOk returns a tuple with the Ttl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTtl

`func (o *AdminEntryPointMutable) SetTtl(v int32)`

SetTtl sets Ttl field to given value.

### HasTtl

`func (o *AdminEntryPointMutable) HasTtl() bool`

HasTtl returns a boolean if a field has been set.

### GetParams

`func (o *AdminEntryPointMutable) GetParams() map[string]string`

GetParams returns the Params field if non-nil, zero value otherwise.

### GetParamsOk

`func (o *AdminEntryPointMutable) GetParamsOk() (*map[string]string, bool)`

GetParamsOk returns a tuple with the Params field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParams

`func (o *AdminEntryPointMutable) SetParams(v map[string]string)`

SetParams sets Params field to given value.

### HasParams

`func (o *AdminEntryPointMutable) HasParams() bool`

HasParams returns a boolean if a field has been set.

### GetData

`func (o *AdminEntryPointMutable) GetData() map[string]string`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *AdminEntryPointMutable) GetDataOk() (*map[string]string, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *AdminEntryPointMutable) SetData(v map[string]string)`

SetData sets Data field to given value.

### HasData

`func (o *AdminEntryPointMutable) HasData() bool`

HasData returns a boolean if a field has been set.

### GetCustomerExperienceId

`func (o *AdminEntryPointMutable) GetCustomerExperienceId() string`

GetCustomerExperienceId returns the CustomerExperienceId field if non-nil, zero value otherwise.

### GetCustomerExperienceIdOk

`func (o *AdminEntryPointMutable) GetCustomerExperienceIdOk() (*string, bool)`

GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerExperienceId

`func (o *AdminEntryPointMutable) SetCustomerExperienceId(v string)`

SetCustomerExperienceId sets CustomerExperienceId field to given value.

### HasCustomerExperienceId

`func (o *AdminEntryPointMutable) HasCustomerExperienceId() bool`

HasCustomerExperienceId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


