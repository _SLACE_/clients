# EntryPoint

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | [**EntryPointType**](EntryPointType.md) |  | 
**Base64Image** | Pointer to **string** | Base64 encoded image with qr code  | [optional] 
**Url** | **string** |  | 

## Methods

### NewEntryPoint

`func NewEntryPoint(type_ EntryPointType, url string, ) *EntryPoint`

NewEntryPoint instantiates a new EntryPoint object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewEntryPointWithDefaults

`func NewEntryPointWithDefaults() *EntryPoint`

NewEntryPointWithDefaults instantiates a new EntryPoint object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *EntryPoint) GetType() EntryPointType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *EntryPoint) GetTypeOk() (*EntryPointType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *EntryPoint) SetType(v EntryPointType)`

SetType sets Type field to given value.


### GetBase64Image

`func (o *EntryPoint) GetBase64Image() string`

GetBase64Image returns the Base64Image field if non-nil, zero value otherwise.

### GetBase64ImageOk

`func (o *EntryPoint) GetBase64ImageOk() (*string, bool)`

GetBase64ImageOk returns a tuple with the Base64Image field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBase64Image

`func (o *EntryPoint) SetBase64Image(v string)`

SetBase64Image sets Base64Image field to given value.

### HasBase64Image

`func (o *EntryPoint) HasBase64Image() bool`

HasBase64Image returns a boolean if a field has been set.

### GetUrl

`func (o *EntryPoint) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *EntryPoint) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *EntryPoint) SetUrl(v string)`

SetUrl sets Url field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


