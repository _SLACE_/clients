# EntryPointData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to [**EntryPointType**](EntryPointType.md) |  | [optional] 
**Hash** | Pointer to **string** | Custom identifier to create user friendly urls. If provided has to be unique globally. If not set unique id will be generated. | [optional] 
**MessageLinkHash** | Pointer to **string** |  | [optional] 
**InteractionId** | Pointer to **string** |  | [optional] 
**RedirectUrl** | Pointer to **string** | Some external url for which short link will be created | [optional] 
**ExpiredUrl** | Pointer to **string** | Optional url to which entry point will redirect when expired | [optional] 
**Ttl** | Pointer to **int32** | Time to live in seconds | [optional] 
**DynamicData** | Pointer to **map[string]string** | Dynamic data ius similar to static_data but it can be also overwritten by url query parameters. This structure is then copied to contact transaction related to entry point (for example after QR code scan) | [optional] 
**StaticData** | Pointer to **map[string]string** | Custom data attached to entry point. This data is copied to every transaction when user scans and interacts messenger. | [optional] 
**CustomerExperienceId** | Pointer to **string** | Any text without &#39;.&#39; characters. Preferred over interaction_id if keeping the reference to the latest version of interaction is required. | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 

## Methods

### NewEntryPointData

`func NewEntryPointData() *EntryPointData`

NewEntryPointData instantiates a new EntryPointData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewEntryPointDataWithDefaults

`func NewEntryPointDataWithDefaults() *EntryPointData`

NewEntryPointDataWithDefaults instantiates a new EntryPointData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *EntryPointData) GetType() EntryPointType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *EntryPointData) GetTypeOk() (*EntryPointType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *EntryPointData) SetType(v EntryPointType)`

SetType sets Type field to given value.

### HasType

`func (o *EntryPointData) HasType() bool`

HasType returns a boolean if a field has been set.

### GetHash

`func (o *EntryPointData) GetHash() string`

GetHash returns the Hash field if non-nil, zero value otherwise.

### GetHashOk

`func (o *EntryPointData) GetHashOk() (*string, bool)`

GetHashOk returns a tuple with the Hash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHash

`func (o *EntryPointData) SetHash(v string)`

SetHash sets Hash field to given value.

### HasHash

`func (o *EntryPointData) HasHash() bool`

HasHash returns a boolean if a field has been set.

### GetMessageLinkHash

`func (o *EntryPointData) GetMessageLinkHash() string`

GetMessageLinkHash returns the MessageLinkHash field if non-nil, zero value otherwise.

### GetMessageLinkHashOk

`func (o *EntryPointData) GetMessageLinkHashOk() (*string, bool)`

GetMessageLinkHashOk returns a tuple with the MessageLinkHash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageLinkHash

`func (o *EntryPointData) SetMessageLinkHash(v string)`

SetMessageLinkHash sets MessageLinkHash field to given value.

### HasMessageLinkHash

`func (o *EntryPointData) HasMessageLinkHash() bool`

HasMessageLinkHash returns a boolean if a field has been set.

### GetInteractionId

`func (o *EntryPointData) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *EntryPointData) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *EntryPointData) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *EntryPointData) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetRedirectUrl

`func (o *EntryPointData) GetRedirectUrl() string`

GetRedirectUrl returns the RedirectUrl field if non-nil, zero value otherwise.

### GetRedirectUrlOk

`func (o *EntryPointData) GetRedirectUrlOk() (*string, bool)`

GetRedirectUrlOk returns a tuple with the RedirectUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRedirectUrl

`func (o *EntryPointData) SetRedirectUrl(v string)`

SetRedirectUrl sets RedirectUrl field to given value.

### HasRedirectUrl

`func (o *EntryPointData) HasRedirectUrl() bool`

HasRedirectUrl returns a boolean if a field has been set.

### GetExpiredUrl

`func (o *EntryPointData) GetExpiredUrl() string`

GetExpiredUrl returns the ExpiredUrl field if non-nil, zero value otherwise.

### GetExpiredUrlOk

`func (o *EntryPointData) GetExpiredUrlOk() (*string, bool)`

GetExpiredUrlOk returns a tuple with the ExpiredUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiredUrl

`func (o *EntryPointData) SetExpiredUrl(v string)`

SetExpiredUrl sets ExpiredUrl field to given value.

### HasExpiredUrl

`func (o *EntryPointData) HasExpiredUrl() bool`

HasExpiredUrl returns a boolean if a field has been set.

### GetTtl

`func (o *EntryPointData) GetTtl() int32`

GetTtl returns the Ttl field if non-nil, zero value otherwise.

### GetTtlOk

`func (o *EntryPointData) GetTtlOk() (*int32, bool)`

GetTtlOk returns a tuple with the Ttl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTtl

`func (o *EntryPointData) SetTtl(v int32)`

SetTtl sets Ttl field to given value.

### HasTtl

`func (o *EntryPointData) HasTtl() bool`

HasTtl returns a boolean if a field has been set.

### GetDynamicData

`func (o *EntryPointData) GetDynamicData() map[string]string`

GetDynamicData returns the DynamicData field if non-nil, zero value otherwise.

### GetDynamicDataOk

`func (o *EntryPointData) GetDynamicDataOk() (*map[string]string, bool)`

GetDynamicDataOk returns a tuple with the DynamicData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDynamicData

`func (o *EntryPointData) SetDynamicData(v map[string]string)`

SetDynamicData sets DynamicData field to given value.

### HasDynamicData

`func (o *EntryPointData) HasDynamicData() bool`

HasDynamicData returns a boolean if a field has been set.

### GetStaticData

`func (o *EntryPointData) GetStaticData() map[string]string`

GetStaticData returns the StaticData field if non-nil, zero value otherwise.

### GetStaticDataOk

`func (o *EntryPointData) GetStaticDataOk() (*map[string]string, bool)`

GetStaticDataOk returns a tuple with the StaticData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStaticData

`func (o *EntryPointData) SetStaticData(v map[string]string)`

SetStaticData sets StaticData field to given value.

### HasStaticData

`func (o *EntryPointData) HasStaticData() bool`

HasStaticData returns a boolean if a field has been set.

### GetCustomerExperienceId

`func (o *EntryPointData) GetCustomerExperienceId() string`

GetCustomerExperienceId returns the CustomerExperienceId field if non-nil, zero value otherwise.

### GetCustomerExperienceIdOk

`func (o *EntryPointData) GetCustomerExperienceIdOk() (*string, bool)`

GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerExperienceId

`func (o *EntryPointData) SetCustomerExperienceId(v string)`

SetCustomerExperienceId sets CustomerExperienceId field to given value.

### HasCustomerExperienceId

`func (o *EntryPointData) HasCustomerExperienceId() bool`

HasCustomerExperienceId returns a boolean if a field has been set.

### GetChannelId

`func (o *EntryPointData) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *EntryPointData) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *EntryPointData) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *EntryPointData) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


