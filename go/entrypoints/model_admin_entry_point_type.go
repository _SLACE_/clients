/*
EntryPoints

## Overview  EntryPoint is one of the ways to start interaction flow. It can be represented as QR code, link (https://slace.me/... URL) or NFC tag.  ## Data  EntryPoints contain two types of data:   - **static** - *\"internal\" data storage which can be updated only via editing the EntryPoint* (directly or via Interaction) - **dynamic** - *public data which can be altered also via query parameters of the EP link. Should NOT be used for secrets or critical data* because it can be overwritten by the end user.  Both data containers are copied to transaction created when EntryPoint is entered. This data is used primarily for personalization or flow control of the Interaction.  ## EP Batches  EntryPoints can be batch-created (superadmin feature) and then [assigned](https://slace.stoplight.io/docs/stoplight-docs-core/altekzonwbrq9-imports-uploads#assignment-xlsx) to specific Interactions.  > EntryPoints in a batch have random hashes but it's possible to mass-create EPs using the assignment feature (only superadmins). If a value from **Link hash** column is not found it will be auto-created. This allows to create EntryPoints following different hash pattern (e.g. with common prefix).  EntryPoints are re-assignable meaning that they can be re-used for other Interactions even in another Channel or Organization by another [assignment import](https://slace.stoplight.io/docs/stoplight-docs-core/altekzonwbrq9-imports-uploads#assignment-xlsx).  ## Redirects  There is a special type of EntryPoint which does not start a SLACE Interaction but redirects to a configured URL.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package entrypoints

import (
	"encoding/json"
	"fmt"
)

// AdminEntryPointType the model 'AdminEntryPointType'
type AdminEntryPointType string

// List of AdminEntryPointType
const (
	CAMPAIGN AdminEntryPointType = "campaign"
	EXTERNAL AdminEntryPointType = "external"
	INTERACTION AdminEntryPointType = "interaction"
	UNKNOWN AdminEntryPointType = "unknown"
)

// All allowed values of AdminEntryPointType enum
var AllowedAdminEntryPointTypeEnumValues = []AdminEntryPointType{
	"campaign",
	"external",
	"interaction",
	"unknown",
}

func (v *AdminEntryPointType) UnmarshalJSON(src []byte) error {
	var value string
	err := json.Unmarshal(src, &value)
	if err != nil {
		return err
	}
	enumTypeValue := AdminEntryPointType(value)
	for _, existing := range AllowedAdminEntryPointTypeEnumValues {
		if existing == enumTypeValue {
			*v = enumTypeValue
			return nil
		}
	}

	return fmt.Errorf("%+v is not a valid AdminEntryPointType", value)
}

// NewAdminEntryPointTypeFromValue returns a pointer to a valid AdminEntryPointType
// for the value passed as argument, or an error if the value passed is not allowed by the enum
func NewAdminEntryPointTypeFromValue(v string) (*AdminEntryPointType, error) {
	ev := AdminEntryPointType(v)
	if ev.IsValid() {
		return &ev, nil
	} else {
		return nil, fmt.Errorf("invalid value '%v' for AdminEntryPointType: valid values are %v", v, AllowedAdminEntryPointTypeEnumValues)
	}
}

// IsValid return true if the value is valid for the enum, false otherwise
func (v AdminEntryPointType) IsValid() bool {
	for _, existing := range AllowedAdminEntryPointTypeEnumValues {
		if existing == v {
			return true
		}
	}
	return false
}

// Ptr returns reference to AdminEntryPointType value
func (v AdminEntryPointType) Ptr() *AdminEntryPointType {
	return &v
}

type NullableAdminEntryPointType struct {
	value *AdminEntryPointType
	isSet bool
}

func (v NullableAdminEntryPointType) Get() *AdminEntryPointType {
	return v.value
}

func (v *NullableAdminEntryPointType) Set(val *AdminEntryPointType) {
	v.value = val
	v.isSet = true
}

func (v NullableAdminEntryPointType) IsSet() bool {
	return v.isSet
}

func (v *NullableAdminEntryPointType) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAdminEntryPointType(val *AdminEntryPointType) *NullableAdminEntryPointType {
	return &NullableAdminEntryPointType{value: val, isSet: true}
}

func (v NullableAdminEntryPointType) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAdminEntryPointType) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}

