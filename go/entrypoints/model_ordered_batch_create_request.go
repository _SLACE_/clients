/*
EntryPoints

## Overview  EntryPoint is one of the ways to start interaction flow. It can be represented as QR code, link (https://slace.me/... URL) or NFC tag.  ## Data  EntryPoints contain two types of data:   - **static** - *\"internal\" data storage which can be updated only via editing the EntryPoint* (directly or via Interaction) - **dynamic** - *public data which can be altered also via query parameters of the EP link. Should NOT be used for secrets or critical data* because it can be overwritten by the end user.  Both data containers are copied to transaction created when EntryPoint is entered. This data is used primarily for personalization or flow control of the Interaction.  ## EP Batches  EntryPoints can be batch-created (superadmin feature) and then [assigned](https://slace.stoplight.io/docs/stoplight-docs-core/altekzonwbrq9-imports-uploads#assignment-xlsx) to specific Interactions.  > EntryPoints in a batch have random hashes but it's possible to mass-create EPs using the assignment feature (only superadmins). If a value from **Link hash** column is not found it will be auto-created. This allows to create EntryPoints following different hash pattern (e.g. with common prefix).  EntryPoints are re-assignable meaning that they can be re-used for other Interactions even in another Channel or Organization by another [assignment import](https://slace.stoplight.io/docs/stoplight-docs-core/altekzonwbrq9-imports-uploads#assignment-xlsx).  ## Redirects  There is a special type of EntryPoint which does not start a SLACE Interaction but redirects to a configured URL.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package entrypoints

import (
	"encoding/json"
)

// checks if the OrderedBatchCreateRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &OrderedBatchCreateRequest{}

// OrderedBatchCreateRequest struct for OrderedBatchCreateRequest
type OrderedBatchCreateRequest struct {
	Type *string `json:"type,omitempty"`
	OrganizationId *string `json:"organization_id,omitempty"`
	ChannelId *string `json:"channel_id,omitempty"`
	Count int32 `json:"count"`
	MessageLinkHash *string `json:"message_link_hash,omitempty"`
	InteractionId *string `json:"interaction_id,omitempty"`
	Name *string `json:"name,omitempty"`
	BatchId *string `json:"batch_id,omitempty"`
	CustomerExperienceId *string `json:"customer_experience_id,omitempty"`
}

// NewOrderedBatchCreateRequest instantiates a new OrderedBatchCreateRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewOrderedBatchCreateRequest(count int32) *OrderedBatchCreateRequest {
	this := OrderedBatchCreateRequest{}
	this.Count = count
	return &this
}

// NewOrderedBatchCreateRequestWithDefaults instantiates a new OrderedBatchCreateRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewOrderedBatchCreateRequestWithDefaults() *OrderedBatchCreateRequest {
	this := OrderedBatchCreateRequest{}
	return &this
}

// GetType returns the Type field value if set, zero value otherwise.
func (o *OrderedBatchCreateRequest) GetType() string {
	if o == nil || IsNil(o.Type) {
		var ret string
		return ret
	}
	return *o.Type
}

// GetTypeOk returns a tuple with the Type field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *OrderedBatchCreateRequest) GetTypeOk() (*string, bool) {
	if o == nil || IsNil(o.Type) {
		return nil, false
	}
	return o.Type, true
}

// HasType returns a boolean if a field has been set.
func (o *OrderedBatchCreateRequest) HasType() bool {
	if o != nil && !IsNil(o.Type) {
		return true
	}

	return false
}

// SetType gets a reference to the given string and assigns it to the Type field.
func (o *OrderedBatchCreateRequest) SetType(v string) {
	o.Type = &v
}

// GetOrganizationId returns the OrganizationId field value if set, zero value otherwise.
func (o *OrderedBatchCreateRequest) GetOrganizationId() string {
	if o == nil || IsNil(o.OrganizationId) {
		var ret string
		return ret
	}
	return *o.OrganizationId
}

// GetOrganizationIdOk returns a tuple with the OrganizationId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *OrderedBatchCreateRequest) GetOrganizationIdOk() (*string, bool) {
	if o == nil || IsNil(o.OrganizationId) {
		return nil, false
	}
	return o.OrganizationId, true
}

// HasOrganizationId returns a boolean if a field has been set.
func (o *OrderedBatchCreateRequest) HasOrganizationId() bool {
	if o != nil && !IsNil(o.OrganizationId) {
		return true
	}

	return false
}

// SetOrganizationId gets a reference to the given string and assigns it to the OrganizationId field.
func (o *OrderedBatchCreateRequest) SetOrganizationId(v string) {
	o.OrganizationId = &v
}

// GetChannelId returns the ChannelId field value if set, zero value otherwise.
func (o *OrderedBatchCreateRequest) GetChannelId() string {
	if o == nil || IsNil(o.ChannelId) {
		var ret string
		return ret
	}
	return *o.ChannelId
}

// GetChannelIdOk returns a tuple with the ChannelId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *OrderedBatchCreateRequest) GetChannelIdOk() (*string, bool) {
	if o == nil || IsNil(o.ChannelId) {
		return nil, false
	}
	return o.ChannelId, true
}

// HasChannelId returns a boolean if a field has been set.
func (o *OrderedBatchCreateRequest) HasChannelId() bool {
	if o != nil && !IsNil(o.ChannelId) {
		return true
	}

	return false
}

// SetChannelId gets a reference to the given string and assigns it to the ChannelId field.
func (o *OrderedBatchCreateRequest) SetChannelId(v string) {
	o.ChannelId = &v
}

// GetCount returns the Count field value
func (o *OrderedBatchCreateRequest) GetCount() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.Count
}

// GetCountOk returns a tuple with the Count field value
// and a boolean to check if the value has been set.
func (o *OrderedBatchCreateRequest) GetCountOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Count, true
}

// SetCount sets field value
func (o *OrderedBatchCreateRequest) SetCount(v int32) {
	o.Count = v
}

// GetMessageLinkHash returns the MessageLinkHash field value if set, zero value otherwise.
func (o *OrderedBatchCreateRequest) GetMessageLinkHash() string {
	if o == nil || IsNil(o.MessageLinkHash) {
		var ret string
		return ret
	}
	return *o.MessageLinkHash
}

// GetMessageLinkHashOk returns a tuple with the MessageLinkHash field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *OrderedBatchCreateRequest) GetMessageLinkHashOk() (*string, bool) {
	if o == nil || IsNil(o.MessageLinkHash) {
		return nil, false
	}
	return o.MessageLinkHash, true
}

// HasMessageLinkHash returns a boolean if a field has been set.
func (o *OrderedBatchCreateRequest) HasMessageLinkHash() bool {
	if o != nil && !IsNil(o.MessageLinkHash) {
		return true
	}

	return false
}

// SetMessageLinkHash gets a reference to the given string and assigns it to the MessageLinkHash field.
func (o *OrderedBatchCreateRequest) SetMessageLinkHash(v string) {
	o.MessageLinkHash = &v
}

// GetInteractionId returns the InteractionId field value if set, zero value otherwise.
func (o *OrderedBatchCreateRequest) GetInteractionId() string {
	if o == nil || IsNil(o.InteractionId) {
		var ret string
		return ret
	}
	return *o.InteractionId
}

// GetInteractionIdOk returns a tuple with the InteractionId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *OrderedBatchCreateRequest) GetInteractionIdOk() (*string, bool) {
	if o == nil || IsNil(o.InteractionId) {
		return nil, false
	}
	return o.InteractionId, true
}

// HasInteractionId returns a boolean if a field has been set.
func (o *OrderedBatchCreateRequest) HasInteractionId() bool {
	if o != nil && !IsNil(o.InteractionId) {
		return true
	}

	return false
}

// SetInteractionId gets a reference to the given string and assigns it to the InteractionId field.
func (o *OrderedBatchCreateRequest) SetInteractionId(v string) {
	o.InteractionId = &v
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *OrderedBatchCreateRequest) GetName() string {
	if o == nil || IsNil(o.Name) {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *OrderedBatchCreateRequest) GetNameOk() (*string, bool) {
	if o == nil || IsNil(o.Name) {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *OrderedBatchCreateRequest) HasName() bool {
	if o != nil && !IsNil(o.Name) {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *OrderedBatchCreateRequest) SetName(v string) {
	o.Name = &v
}

// GetBatchId returns the BatchId field value if set, zero value otherwise.
func (o *OrderedBatchCreateRequest) GetBatchId() string {
	if o == nil || IsNil(o.BatchId) {
		var ret string
		return ret
	}
	return *o.BatchId
}

// GetBatchIdOk returns a tuple with the BatchId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *OrderedBatchCreateRequest) GetBatchIdOk() (*string, bool) {
	if o == nil || IsNil(o.BatchId) {
		return nil, false
	}
	return o.BatchId, true
}

// HasBatchId returns a boolean if a field has been set.
func (o *OrderedBatchCreateRequest) HasBatchId() bool {
	if o != nil && !IsNil(o.BatchId) {
		return true
	}

	return false
}

// SetBatchId gets a reference to the given string and assigns it to the BatchId field.
func (o *OrderedBatchCreateRequest) SetBatchId(v string) {
	o.BatchId = &v
}

// GetCustomerExperienceId returns the CustomerExperienceId field value if set, zero value otherwise.
func (o *OrderedBatchCreateRequest) GetCustomerExperienceId() string {
	if o == nil || IsNil(o.CustomerExperienceId) {
		var ret string
		return ret
	}
	return *o.CustomerExperienceId
}

// GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *OrderedBatchCreateRequest) GetCustomerExperienceIdOk() (*string, bool) {
	if o == nil || IsNil(o.CustomerExperienceId) {
		return nil, false
	}
	return o.CustomerExperienceId, true
}

// HasCustomerExperienceId returns a boolean if a field has been set.
func (o *OrderedBatchCreateRequest) HasCustomerExperienceId() bool {
	if o != nil && !IsNil(o.CustomerExperienceId) {
		return true
	}

	return false
}

// SetCustomerExperienceId gets a reference to the given string and assigns it to the CustomerExperienceId field.
func (o *OrderedBatchCreateRequest) SetCustomerExperienceId(v string) {
	o.CustomerExperienceId = &v
}

func (o OrderedBatchCreateRequest) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o OrderedBatchCreateRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Type) {
		toSerialize["type"] = o.Type
	}
	if !IsNil(o.OrganizationId) {
		toSerialize["organization_id"] = o.OrganizationId
	}
	if !IsNil(o.ChannelId) {
		toSerialize["channel_id"] = o.ChannelId
	}
	toSerialize["count"] = o.Count
	if !IsNil(o.MessageLinkHash) {
		toSerialize["message_link_hash"] = o.MessageLinkHash
	}
	if !IsNil(o.InteractionId) {
		toSerialize["interaction_id"] = o.InteractionId
	}
	if !IsNil(o.Name) {
		toSerialize["name"] = o.Name
	}
	if !IsNil(o.BatchId) {
		toSerialize["batch_id"] = o.BatchId
	}
	if !IsNil(o.CustomerExperienceId) {
		toSerialize["customer_experience_id"] = o.CustomerExperienceId
	}
	return toSerialize, nil
}

type NullableOrderedBatchCreateRequest struct {
	value *OrderedBatchCreateRequest
	isSet bool
}

func (v NullableOrderedBatchCreateRequest) Get() *OrderedBatchCreateRequest {
	return v.value
}

func (v *NullableOrderedBatchCreateRequest) Set(val *OrderedBatchCreateRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableOrderedBatchCreateRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableOrderedBatchCreateRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableOrderedBatchCreateRequest(val *OrderedBatchCreateRequest) *NullableOrderedBatchCreateRequest {
	return &NullableOrderedBatchCreateRequest{value: val, isSet: true}
}

func (v NullableOrderedBatchCreateRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableOrderedBatchCreateRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


