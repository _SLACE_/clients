/*
EntryPoints

Testing BatchesJobsAPIService

*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech);

package entrypoints

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
	openapiclient "bitbucket.org/_SLACE_/clients/go/entrypoints"
)

func Test_entrypoints_BatchesJobsAPIService(t *testing.T) {

	configuration := openapiclient.NewConfiguration()
	apiClient := openapiclient.NewAPIClient(configuration)

	t.Run("Test BatchesJobsAPIService GetManagementEntryPointsJobs", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		resp, httpRes, err := apiClient.BatchesJobsAPI.GetManagementEntryPointsJobs(context.Background()).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test BatchesJobsAPIService PostManagementEntryPointsJobIdFileGenerationRetry", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var jobId string

		httpRes, err := apiClient.BatchesJobsAPI.PostManagementEntryPointsJobIdFileGenerationRetry(context.Background(), jobId).Execute()

		require.Nil(t, err)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test BatchesJobsAPIService PostManagementEntryPointsJobsJobIdDownload", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var jobId string

		httpRes, err := apiClient.BatchesJobsAPI.PostManagementEntryPointsJobsJobIdDownload(context.Background(), jobId).Execute()

		require.Nil(t, err)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

}
