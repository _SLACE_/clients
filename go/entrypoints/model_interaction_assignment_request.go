/*
EntryPoints

## Overview  EntryPoint is one of the ways to start interaction flow. It can be represented as QR code, link (https://slace.me/... URL) or NFC tag.  ## Data  EntryPoints contain two types of data:   - **static** - *\"internal\" data storage which can be updated only via editing the EntryPoint* (directly or via Interaction) - **dynamic** - *public data which can be altered also via query parameters of the EP link. Should NOT be used for secrets or critical data* because it can be overwritten by the end user.  Both data containers are copied to transaction created when EntryPoint is entered. This data is used primarily for personalization or flow control of the Interaction.  ## EP Batches  EntryPoints can be batch-created (superadmin feature) and then [assigned](https://slace.stoplight.io/docs/stoplight-docs-core/altekzonwbrq9-imports-uploads#assignment-xlsx) to specific Interactions.  > EntryPoints in a batch have random hashes but it's possible to mass-create EPs using the assignment feature (only superadmins). If a value from **Link hash** column is not found it will be auto-created. This allows to create EntryPoints following different hash pattern (e.g. with common prefix).  EntryPoints are re-assignable meaning that they can be re-used for other Interactions even in another Channel or Organization by another [assignment import](https://slace.stoplight.io/docs/stoplight-docs-core/altekzonwbrq9-imports-uploads#assignment-xlsx).  ## Redirects  There is a special type of EntryPoint which does not start a SLACE Interaction but redirects to a configured URL.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package entrypoints

import (
	"encoding/json"
)

// checks if the InteractionAssignmentRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &InteractionAssignmentRequest{}

// InteractionAssignmentRequest struct for InteractionAssignmentRequest
type InteractionAssignmentRequest struct {
	ChannelId string `json:"channel_id"`
	FromInteractionId string `json:"from_interaction_id"`
	ToInteractionId string `json:"to_interaction_id"`
}

// NewInteractionAssignmentRequest instantiates a new InteractionAssignmentRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewInteractionAssignmentRequest(channelId string, fromInteractionId string, toInteractionId string) *InteractionAssignmentRequest {
	this := InteractionAssignmentRequest{}
	this.ChannelId = channelId
	this.FromInteractionId = fromInteractionId
	this.ToInteractionId = toInteractionId
	return &this
}

// NewInteractionAssignmentRequestWithDefaults instantiates a new InteractionAssignmentRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewInteractionAssignmentRequestWithDefaults() *InteractionAssignmentRequest {
	this := InteractionAssignmentRequest{}
	return &this
}

// GetChannelId returns the ChannelId field value
func (o *InteractionAssignmentRequest) GetChannelId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ChannelId
}

// GetChannelIdOk returns a tuple with the ChannelId field value
// and a boolean to check if the value has been set.
func (o *InteractionAssignmentRequest) GetChannelIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ChannelId, true
}

// SetChannelId sets field value
func (o *InteractionAssignmentRequest) SetChannelId(v string) {
	o.ChannelId = v
}

// GetFromInteractionId returns the FromInteractionId field value
func (o *InteractionAssignmentRequest) GetFromInteractionId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.FromInteractionId
}

// GetFromInteractionIdOk returns a tuple with the FromInteractionId field value
// and a boolean to check if the value has been set.
func (o *InteractionAssignmentRequest) GetFromInteractionIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.FromInteractionId, true
}

// SetFromInteractionId sets field value
func (o *InteractionAssignmentRequest) SetFromInteractionId(v string) {
	o.FromInteractionId = v
}

// GetToInteractionId returns the ToInteractionId field value
func (o *InteractionAssignmentRequest) GetToInteractionId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ToInteractionId
}

// GetToInteractionIdOk returns a tuple with the ToInteractionId field value
// and a boolean to check if the value has been set.
func (o *InteractionAssignmentRequest) GetToInteractionIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ToInteractionId, true
}

// SetToInteractionId sets field value
func (o *InteractionAssignmentRequest) SetToInteractionId(v string) {
	o.ToInteractionId = v
}

func (o InteractionAssignmentRequest) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o InteractionAssignmentRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["channel_id"] = o.ChannelId
	toSerialize["from_interaction_id"] = o.FromInteractionId
	toSerialize["to_interaction_id"] = o.ToInteractionId
	return toSerialize, nil
}

type NullableInteractionAssignmentRequest struct {
	value *InteractionAssignmentRequest
	isSet bool
}

func (v NullableInteractionAssignmentRequest) Get() *InteractionAssignmentRequest {
	return v.value
}

func (v *NullableInteractionAssignmentRequest) Set(val *InteractionAssignmentRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableInteractionAssignmentRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableInteractionAssignmentRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableInteractionAssignmentRequest(val *InteractionAssignmentRequest) *NullableInteractionAssignmentRequest {
	return &NullableInteractionAssignmentRequest{value: val, isSet: true}
}

func (v NullableInteractionAssignmentRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableInteractionAssignmentRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


