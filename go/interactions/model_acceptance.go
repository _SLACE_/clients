/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
)

// checks if the Acceptance type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &Acceptance{}

// Acceptance struct for Acceptance
type Acceptance struct {
	Id *string `json:"id,omitempty"`
	OrganizationId *string `json:"organization_id,omitempty"`
	ChannelId *string `json:"channel_id,omitempty"`
	ScPermissionId *string `json:"sc_permission_id,omitempty"`
	RolloutId *string `json:"rollout_id,omitempty"`
	AcceptedAt *string `json:"accepted_at,omitempty"`
	AcceptedBy *string `json:"accepted_by,omitempty"`
	CreatedAt *string `json:"created_at,omitempty"`
	UpdatedAt *string `json:"updated_at,omitempty"`
}

// NewAcceptance instantiates a new Acceptance object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAcceptance() *Acceptance {
	this := Acceptance{}
	return &this
}

// NewAcceptanceWithDefaults instantiates a new Acceptance object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAcceptanceWithDefaults() *Acceptance {
	this := Acceptance{}
	return &this
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *Acceptance) GetId() string {
	if o == nil || IsNil(o.Id) {
		var ret string
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Acceptance) GetIdOk() (*string, bool) {
	if o == nil || IsNil(o.Id) {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *Acceptance) HasId() bool {
	if o != nil && !IsNil(o.Id) {
		return true
	}

	return false
}

// SetId gets a reference to the given string and assigns it to the Id field.
func (o *Acceptance) SetId(v string) {
	o.Id = &v
}

// GetOrganizationId returns the OrganizationId field value if set, zero value otherwise.
func (o *Acceptance) GetOrganizationId() string {
	if o == nil || IsNil(o.OrganizationId) {
		var ret string
		return ret
	}
	return *o.OrganizationId
}

// GetOrganizationIdOk returns a tuple with the OrganizationId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Acceptance) GetOrganizationIdOk() (*string, bool) {
	if o == nil || IsNil(o.OrganizationId) {
		return nil, false
	}
	return o.OrganizationId, true
}

// HasOrganizationId returns a boolean if a field has been set.
func (o *Acceptance) HasOrganizationId() bool {
	if o != nil && !IsNil(o.OrganizationId) {
		return true
	}

	return false
}

// SetOrganizationId gets a reference to the given string and assigns it to the OrganizationId field.
func (o *Acceptance) SetOrganizationId(v string) {
	o.OrganizationId = &v
}

// GetChannelId returns the ChannelId field value if set, zero value otherwise.
func (o *Acceptance) GetChannelId() string {
	if o == nil || IsNil(o.ChannelId) {
		var ret string
		return ret
	}
	return *o.ChannelId
}

// GetChannelIdOk returns a tuple with the ChannelId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Acceptance) GetChannelIdOk() (*string, bool) {
	if o == nil || IsNil(o.ChannelId) {
		return nil, false
	}
	return o.ChannelId, true
}

// HasChannelId returns a boolean if a field has been set.
func (o *Acceptance) HasChannelId() bool {
	if o != nil && !IsNil(o.ChannelId) {
		return true
	}

	return false
}

// SetChannelId gets a reference to the given string and assigns it to the ChannelId field.
func (o *Acceptance) SetChannelId(v string) {
	o.ChannelId = &v
}

// GetScPermissionId returns the ScPermissionId field value if set, zero value otherwise.
func (o *Acceptance) GetScPermissionId() string {
	if o == nil || IsNil(o.ScPermissionId) {
		var ret string
		return ret
	}
	return *o.ScPermissionId
}

// GetScPermissionIdOk returns a tuple with the ScPermissionId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Acceptance) GetScPermissionIdOk() (*string, bool) {
	if o == nil || IsNil(o.ScPermissionId) {
		return nil, false
	}
	return o.ScPermissionId, true
}

// HasScPermissionId returns a boolean if a field has been set.
func (o *Acceptance) HasScPermissionId() bool {
	if o != nil && !IsNil(o.ScPermissionId) {
		return true
	}

	return false
}

// SetScPermissionId gets a reference to the given string and assigns it to the ScPermissionId field.
func (o *Acceptance) SetScPermissionId(v string) {
	o.ScPermissionId = &v
}

// GetRolloutId returns the RolloutId field value if set, zero value otherwise.
func (o *Acceptance) GetRolloutId() string {
	if o == nil || IsNil(o.RolloutId) {
		var ret string
		return ret
	}
	return *o.RolloutId
}

// GetRolloutIdOk returns a tuple with the RolloutId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Acceptance) GetRolloutIdOk() (*string, bool) {
	if o == nil || IsNil(o.RolloutId) {
		return nil, false
	}
	return o.RolloutId, true
}

// HasRolloutId returns a boolean if a field has been set.
func (o *Acceptance) HasRolloutId() bool {
	if o != nil && !IsNil(o.RolloutId) {
		return true
	}

	return false
}

// SetRolloutId gets a reference to the given string and assigns it to the RolloutId field.
func (o *Acceptance) SetRolloutId(v string) {
	o.RolloutId = &v
}

// GetAcceptedAt returns the AcceptedAt field value if set, zero value otherwise.
func (o *Acceptance) GetAcceptedAt() string {
	if o == nil || IsNil(o.AcceptedAt) {
		var ret string
		return ret
	}
	return *o.AcceptedAt
}

// GetAcceptedAtOk returns a tuple with the AcceptedAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Acceptance) GetAcceptedAtOk() (*string, bool) {
	if o == nil || IsNil(o.AcceptedAt) {
		return nil, false
	}
	return o.AcceptedAt, true
}

// HasAcceptedAt returns a boolean if a field has been set.
func (o *Acceptance) HasAcceptedAt() bool {
	if o != nil && !IsNil(o.AcceptedAt) {
		return true
	}

	return false
}

// SetAcceptedAt gets a reference to the given string and assigns it to the AcceptedAt field.
func (o *Acceptance) SetAcceptedAt(v string) {
	o.AcceptedAt = &v
}

// GetAcceptedBy returns the AcceptedBy field value if set, zero value otherwise.
func (o *Acceptance) GetAcceptedBy() string {
	if o == nil || IsNil(o.AcceptedBy) {
		var ret string
		return ret
	}
	return *o.AcceptedBy
}

// GetAcceptedByOk returns a tuple with the AcceptedBy field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Acceptance) GetAcceptedByOk() (*string, bool) {
	if o == nil || IsNil(o.AcceptedBy) {
		return nil, false
	}
	return o.AcceptedBy, true
}

// HasAcceptedBy returns a boolean if a field has been set.
func (o *Acceptance) HasAcceptedBy() bool {
	if o != nil && !IsNil(o.AcceptedBy) {
		return true
	}

	return false
}

// SetAcceptedBy gets a reference to the given string and assigns it to the AcceptedBy field.
func (o *Acceptance) SetAcceptedBy(v string) {
	o.AcceptedBy = &v
}

// GetCreatedAt returns the CreatedAt field value if set, zero value otherwise.
func (o *Acceptance) GetCreatedAt() string {
	if o == nil || IsNil(o.CreatedAt) {
		var ret string
		return ret
	}
	return *o.CreatedAt
}

// GetCreatedAtOk returns a tuple with the CreatedAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Acceptance) GetCreatedAtOk() (*string, bool) {
	if o == nil || IsNil(o.CreatedAt) {
		return nil, false
	}
	return o.CreatedAt, true
}

// HasCreatedAt returns a boolean if a field has been set.
func (o *Acceptance) HasCreatedAt() bool {
	if o != nil && !IsNil(o.CreatedAt) {
		return true
	}

	return false
}

// SetCreatedAt gets a reference to the given string and assigns it to the CreatedAt field.
func (o *Acceptance) SetCreatedAt(v string) {
	o.CreatedAt = &v
}

// GetUpdatedAt returns the UpdatedAt field value if set, zero value otherwise.
func (o *Acceptance) GetUpdatedAt() string {
	if o == nil || IsNil(o.UpdatedAt) {
		var ret string
		return ret
	}
	return *o.UpdatedAt
}

// GetUpdatedAtOk returns a tuple with the UpdatedAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Acceptance) GetUpdatedAtOk() (*string, bool) {
	if o == nil || IsNil(o.UpdatedAt) {
		return nil, false
	}
	return o.UpdatedAt, true
}

// HasUpdatedAt returns a boolean if a field has been set.
func (o *Acceptance) HasUpdatedAt() bool {
	if o != nil && !IsNil(o.UpdatedAt) {
		return true
	}

	return false
}

// SetUpdatedAt gets a reference to the given string and assigns it to the UpdatedAt field.
func (o *Acceptance) SetUpdatedAt(v string) {
	o.UpdatedAt = &v
}

func (o Acceptance) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o Acceptance) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Id) {
		toSerialize["id"] = o.Id
	}
	if !IsNil(o.OrganizationId) {
		toSerialize["organization_id"] = o.OrganizationId
	}
	if !IsNil(o.ChannelId) {
		toSerialize["channel_id"] = o.ChannelId
	}
	if !IsNil(o.ScPermissionId) {
		toSerialize["sc_permission_id"] = o.ScPermissionId
	}
	if !IsNil(o.RolloutId) {
		toSerialize["rollout_id"] = o.RolloutId
	}
	if !IsNil(o.AcceptedAt) {
		toSerialize["accepted_at"] = o.AcceptedAt
	}
	if !IsNil(o.AcceptedBy) {
		toSerialize["accepted_by"] = o.AcceptedBy
	}
	if !IsNil(o.CreatedAt) {
		toSerialize["created_at"] = o.CreatedAt
	}
	if !IsNil(o.UpdatedAt) {
		toSerialize["updated_at"] = o.UpdatedAt
	}
	return toSerialize, nil
}

type NullableAcceptance struct {
	value *Acceptance
	isSet bool
}

func (v NullableAcceptance) Get() *Acceptance {
	return v.value
}

func (v *NullableAcceptance) Set(val *Acceptance) {
	v.value = val
	v.isSet = true
}

func (v NullableAcceptance) IsSet() bool {
	return v.isSet
}

func (v *NullableAcceptance) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAcceptance(val *Acceptance) *NullableAcceptance {
	return &NullableAcceptance{value: val, isSet: true}
}

func (v NullableAcceptance) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAcceptance) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


