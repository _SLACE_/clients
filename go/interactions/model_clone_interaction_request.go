/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
)

// checks if the CloneInteractionRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &CloneInteractionRequest{}

// CloneInteractionRequest struct for CloneInteractionRequest
type CloneInteractionRequest struct {
	Targets []CloneTarget `json:"targets,omitempty"`
}

// NewCloneInteractionRequest instantiates a new CloneInteractionRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCloneInteractionRequest() *CloneInteractionRequest {
	this := CloneInteractionRequest{}
	return &this
}

// NewCloneInteractionRequestWithDefaults instantiates a new CloneInteractionRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCloneInteractionRequestWithDefaults() *CloneInteractionRequest {
	this := CloneInteractionRequest{}
	return &this
}

// GetTargets returns the Targets field value if set, zero value otherwise.
func (o *CloneInteractionRequest) GetTargets() []CloneTarget {
	if o == nil || IsNil(o.Targets) {
		var ret []CloneTarget
		return ret
	}
	return o.Targets
}

// GetTargetsOk returns a tuple with the Targets field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CloneInteractionRequest) GetTargetsOk() ([]CloneTarget, bool) {
	if o == nil || IsNil(o.Targets) {
		return nil, false
	}
	return o.Targets, true
}

// HasTargets returns a boolean if a field has been set.
func (o *CloneInteractionRequest) HasTargets() bool {
	if o != nil && !IsNil(o.Targets) {
		return true
	}

	return false
}

// SetTargets gets a reference to the given []CloneTarget and assigns it to the Targets field.
func (o *CloneInteractionRequest) SetTargets(v []CloneTarget) {
	o.Targets = v
}

func (o CloneInteractionRequest) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o CloneInteractionRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Targets) {
		toSerialize["targets"] = o.Targets
	}
	return toSerialize, nil
}

type NullableCloneInteractionRequest struct {
	value *CloneInteractionRequest
	isSet bool
}

func (v NullableCloneInteractionRequest) Get() *CloneInteractionRequest {
	return v.value
}

func (v *NullableCloneInteractionRequest) Set(val *CloneInteractionRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableCloneInteractionRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableCloneInteractionRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCloneInteractionRequest(val *CloneInteractionRequest) *NullableCloneInteractionRequest {
	return &NullableCloneInteractionRequest{value: val, isSet: true}
}

func (v NullableCloneInteractionRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCloneInteractionRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


