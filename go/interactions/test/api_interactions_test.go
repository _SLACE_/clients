/*
Interactions

Testing InteractionsAPIService

*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech);

package interactions

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
	openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func Test_interactions_InteractionsAPIService(t *testing.T) {

	configuration := openapiclient.NewConfiguration()
	apiClient := openapiclient.NewAPIClient(configuration)

	t.Run("Test InteractionsAPIService CloneInteraction", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var id string

		resp, httpRes, err := apiClient.InteractionsAPI.CloneInteraction(context.Background(), channelId, id).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService CreateGlobalRollout", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var interactionId string
		var rolloutId string

		resp, httpRes, err := apiClient.InteractionsAPI.CreateGlobalRollout(context.Background(), channelId, interactionId, rolloutId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService CreateInteraction", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string

		resp, httpRes, err := apiClient.InteractionsAPI.CreateInteraction(context.Background(), channelId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService CreateRollout", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var id string

		resp, httpRes, err := apiClient.InteractionsAPI.CreateRollout(context.Background(), channelId, id).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService CreateScPermissions", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var orgId string

		resp, httpRes, err := apiClient.InteractionsAPI.CreateScPermissions(context.Background(), orgId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService DeleteInteraction", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var id string
		var channelId string

		resp, httpRes, err := apiClient.InteractionsAPI.DeleteInteraction(context.Background(), id, channelId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService DeleteRolloutStatus", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var rolloutId string
		var statusId string

		httpRes, err := apiClient.InteractionsAPI.DeleteRolloutStatus(context.Background(), channelId, rolloutId, statusId).Execute()

		require.Nil(t, err)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService DownloadScPermissionAccepted", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var orgId string
		var acceptanceId string

		resp, httpRes, err := apiClient.InteractionsAPI.DownloadScPermissionAccepted(context.Background(), orgId, acceptanceId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService GetAcceptances", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var orgId string

		resp, httpRes, err := apiClient.InteractionsAPI.GetAcceptances(context.Background(), orgId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService GetChannelSettings", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string

		resp, httpRes, err := apiClient.InteractionsAPI.GetChannelSettings(context.Background(), channelId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService GetChannelsChannelIdInteractionsInteractionIdRolloutConnections", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var interactionId string

		resp, httpRes, err := apiClient.InteractionsAPI.GetChannelsChannelIdInteractionsInteractionIdRolloutConnections(context.Background(), channelId, interactionId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService GetEmbeddings", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var id string

		resp, httpRes, err := apiClient.InteractionsAPI.GetEmbeddings(context.Background(), channelId, id).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService GetInteraction", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var id string
		var channelId string

		resp, httpRes, err := apiClient.InteractionsAPI.GetInteraction(context.Background(), id, channelId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService GetInteractionRollout", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var id string

		resp, httpRes, err := apiClient.InteractionsAPI.GetInteractionRollout(context.Background(), channelId, id).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService GetRolloutVersions", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var interactionId string
		var rolloutId string

		resp, httpRes, err := apiClient.InteractionsAPI.GetRolloutVersions(context.Background(), channelId, interactionId, rolloutId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService GetScPermissions", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var orgId string

		resp, httpRes, err := apiClient.InteractionsAPI.GetScPermissions(context.Background(), orgId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService GetUniqueTags", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var orgId string

		resp, httpRes, err := apiClient.InteractionsAPI.GetUniqueTags(context.Background(), orgId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService ListInteractions", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string

		resp, httpRes, err := apiClient.InteractionsAPI.ListInteractions(context.Background(), channelId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService MarkScPermissionAccepted", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var orgId string

		httpRes, err := apiClient.InteractionsAPI.MarkScPermissionAccepted(context.Background(), orgId).Execute()

		require.Nil(t, err)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService PatchInteraction", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var id string
		var channelId string

		resp, httpRes, err := apiClient.InteractionsAPI.PatchInteraction(context.Background(), id, channelId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService PostChannelsChannelIdInteractionsIdPreview", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var id string

		httpRes, err := apiClient.InteractionsAPI.PostChannelsChannelIdInteractionsIdPreview(context.Background(), channelId, id).Execute()

		require.Nil(t, err)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchdule", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var interactionId string
		var rolloutId string

		httpRes, err := apiClient.InteractionsAPI.PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchdule(context.Background(), channelId, interactionId, rolloutId).Execute()

		require.Nil(t, err)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService PostEvent", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		resp, httpRes, err := apiClient.InteractionsAPI.PostEvent(context.Background()).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService Setinteractionsuccessor", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var id string

		resp, httpRes, err := apiClient.InteractionsAPI.Setinteractionsuccessor(context.Background(), channelId, id).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService UpdateInteraction", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var id string
		var channelId string

		resp, httpRes, err := apiClient.InteractionsAPI.UpdateInteraction(context.Background(), id, channelId).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService UpdateState", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var id string

		resp, httpRes, err := apiClient.InteractionsAPI.UpdateState(context.Background(), channelId, id).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test InteractionsAPIService ValidateInteraction", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var id string

		resp, httpRes, err := apiClient.InteractionsAPI.ValidateInteraction(context.Background(), channelId, id).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

}
