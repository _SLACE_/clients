/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"
	"strings"
	"reflect"
)


// TasksAPIService TasksAPI service
type TasksAPIService service

type ApiCreateTaskRequest struct {
	ctx context.Context
	ApiService *TasksAPIService
	organizationId string
	createTaskData *CreateTaskData
}

func (r ApiCreateTaskRequest) CreateTaskData(createTaskData CreateTaskData) ApiCreateTaskRequest {
	r.createTaskData = &createTaskData
	return r
}

func (r ApiCreateTaskRequest) Execute() (*Task, *http.Response, error) {
	return r.ApiService.CreateTaskExecute(r)
}

/*
CreateTask Create task

create task

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param organizationId
 @return ApiCreateTaskRequest
*/
func (a *TasksAPIService) CreateTask(ctx context.Context, organizationId string) ApiCreateTaskRequest {
	return ApiCreateTaskRequest{
		ApiService: a,
		ctx: ctx,
		organizationId: organizationId,
	}
}

// Execute executes the request
//  @return Task
func (a *TasksAPIService) CreateTaskExecute(r ApiCreateTaskRequest) (*Task, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodPost
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *Task
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "TasksAPIService.CreateTask")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/organizations/{organization_id}/tasks"
	localVarPath = strings.Replace(localVarPath, "{"+"organization_id"+"}", url.PathEscape(parameterValueToString(r.organizationId, "organizationId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.createTaskData
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiDeleteChannelsChannelIdInteractionsIdTasksTaskIdRequest struct {
	ctx context.Context
	ApiService *TasksAPIService
	taskId string
	organizationId string
}

func (r ApiDeleteChannelsChannelIdInteractionsIdTasksTaskIdRequest) Execute() (*http.Response, error) {
	return r.ApiService.DeleteChannelsChannelIdInteractionsIdTasksTaskIdExecute(r)
}

/*
DeleteChannelsChannelIdInteractionsIdTasksTaskId Delete task

Delete task

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param taskId
 @param organizationId
 @return ApiDeleteChannelsChannelIdInteractionsIdTasksTaskIdRequest
*/
func (a *TasksAPIService) DeleteChannelsChannelIdInteractionsIdTasksTaskId(ctx context.Context, taskId string, organizationId string) ApiDeleteChannelsChannelIdInteractionsIdTasksTaskIdRequest {
	return ApiDeleteChannelsChannelIdInteractionsIdTasksTaskIdRequest{
		ApiService: a,
		ctx: ctx,
		taskId: taskId,
		organizationId: organizationId,
	}
}

// Execute executes the request
func (a *TasksAPIService) DeleteChannelsChannelIdInteractionsIdTasksTaskIdExecute(r ApiDeleteChannelsChannelIdInteractionsIdTasksTaskIdRequest) (*http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodDelete
		localVarPostBody     interface{}
		formFiles            []formFile
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "TasksAPIService.DeleteChannelsChannelIdInteractionsIdTasksTaskId")
	if err != nil {
		return nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/organizations/{organization_id}/tasks/{task_id}"
	localVarPath = strings.Replace(localVarPath, "{"+"task_id"+"}", url.PathEscape(parameterValueToString(r.taskId, "taskId")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"organization_id"+"}", url.PathEscape(parameterValueToString(r.organizationId, "organizationId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiGetChannelsChannelIdInteractionsIdTasksTaskIdRequest struct {
	ctx context.Context
	ApiService *TasksAPIService
	taskId string
	organizationId string
}

func (r ApiGetChannelsChannelIdInteractionsIdTasksTaskIdRequest) Execute() (*Task, *http.Response, error) {
	return r.ApiService.GetChannelsChannelIdInteractionsIdTasksTaskIdExecute(r)
}

/*
GetChannelsChannelIdInteractionsIdTasksTaskId Get task

get task

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param taskId
 @param organizationId
 @return ApiGetChannelsChannelIdInteractionsIdTasksTaskIdRequest
*/
func (a *TasksAPIService) GetChannelsChannelIdInteractionsIdTasksTaskId(ctx context.Context, taskId string, organizationId string) ApiGetChannelsChannelIdInteractionsIdTasksTaskIdRequest {
	return ApiGetChannelsChannelIdInteractionsIdTasksTaskIdRequest{
		ApiService: a,
		ctx: ctx,
		taskId: taskId,
		organizationId: organizationId,
	}
}

// Execute executes the request
//  @return Task
func (a *TasksAPIService) GetChannelsChannelIdInteractionsIdTasksTaskIdExecute(r ApiGetChannelsChannelIdInteractionsIdTasksTaskIdRequest) (*Task, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodGet
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *Task
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "TasksAPIService.GetChannelsChannelIdInteractionsIdTasksTaskId")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/organizations/{organization_id}/tasks/{task_id}"
	localVarPath = strings.Replace(localVarPath, "{"+"task_id"+"}", url.PathEscape(parameterValueToString(r.taskId, "taskId")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"organization_id"+"}", url.PathEscape(parameterValueToString(r.organizationId, "organizationId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetTaskCompletionListRequest struct {
	ctx context.Context
	ApiService *TasksAPIService
	organizationId string
	interactionId *string
	campaignSendoutId *string
	blocker *bool
	done *bool
	status *string
	usages *[]string
}

func (r ApiGetTaskCompletionListRequest) InteractionId(interactionId string) ApiGetTaskCompletionListRequest {
	r.interactionId = &interactionId
	return r
}

func (r ApiGetTaskCompletionListRequest) CampaignSendoutId(campaignSendoutId string) ApiGetTaskCompletionListRequest {
	r.campaignSendoutId = &campaignSendoutId
	return r
}

func (r ApiGetTaskCompletionListRequest) Blocker(blocker bool) ApiGetTaskCompletionListRequest {
	r.blocker = &blocker
	return r
}

func (r ApiGetTaskCompletionListRequest) Done(done bool) ApiGetTaskCompletionListRequest {
	r.done = &done
	return r
}

func (r ApiGetTaskCompletionListRequest) Status(status string) ApiGetTaskCompletionListRequest {
	r.status = &status
	return r
}

func (r ApiGetTaskCompletionListRequest) Usages(usages []string) ApiGetTaskCompletionListRequest {
	r.usages = &usages
	return r
}

func (r ApiGetTaskCompletionListRequest) Execute() (*TaskCompletionList, *http.Response, error) {
	return r.ApiService.GetTaskCompletionListExecute(r)
}

/*
GetTaskCompletionList Task completion list

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param organizationId
 @return ApiGetTaskCompletionListRequest
*/
func (a *TasksAPIService) GetTaskCompletionList(ctx context.Context, organizationId string) ApiGetTaskCompletionListRequest {
	return ApiGetTaskCompletionListRequest{
		ApiService: a,
		ctx: ctx,
		organizationId: organizationId,
	}
}

// Execute executes the request
//  @return TaskCompletionList
func (a *TasksAPIService) GetTaskCompletionListExecute(r ApiGetTaskCompletionListRequest) (*TaskCompletionList, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodGet
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *TaskCompletionList
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "TasksAPIService.GetTaskCompletionList")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/organizations/{organization_id}/tasks-completions"
	localVarPath = strings.Replace(localVarPath, "{"+"organization_id"+"}", url.PathEscape(parameterValueToString(r.organizationId, "organizationId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if r.interactionId != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "interaction_id", r.interactionId, "")
	}
	if r.campaignSendoutId != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "campaign_sendout_id", r.campaignSendoutId, "")
	}
	if r.blocker != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "blocker", r.blocker, "")
	}
	if r.done != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "done", r.done, "")
	}
	if r.status != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "status", r.status, "")
	}
	if r.usages != nil {
		t := *r.usages
		if reflect.TypeOf(t).Kind() == reflect.Slice {
			s := reflect.ValueOf(t)
			for i := 0; i < s.Len(); i++ {
				parameterAddToHeaderOrQuery(localVarQueryParams, "usages[]", s.Index(i).Interface(), "multi")
			}
		} else {
			parameterAddToHeaderOrQuery(localVarQueryParams, "usages[]", t, "multi")
		}
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiGetTasksRequest struct {
	ctx context.Context
	ApiService *TasksAPIService
	organizationId string
	interactionId *string
	blocker *bool
	scriptId *string
	usages *[]string
}

// interaction_id filter
func (r ApiGetTasksRequest) InteractionId(interactionId string) ApiGetTasksRequest {
	r.interactionId = &interactionId
	return r
}

// blockers
func (r ApiGetTasksRequest) Blocker(blocker bool) ApiGetTasksRequest {
	r.blocker = &blocker
	return r
}

// script_id filter
func (r ApiGetTasksRequest) ScriptId(scriptId string) ApiGetTasksRequest {
	r.scriptId = &scriptId
	return r
}

func (r ApiGetTasksRequest) Usages(usages []string) ApiGetTasksRequest {
	r.usages = &usages
	return r
}

func (r ApiGetTasksRequest) Execute() (*GetTasksResponse, *http.Response, error) {
	return r.ApiService.GetTasksExecute(r)
}

/*
GetTasks Get tasks

get list of tasks

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param organizationId
 @return ApiGetTasksRequest
*/
func (a *TasksAPIService) GetTasks(ctx context.Context, organizationId string) ApiGetTasksRequest {
	return ApiGetTasksRequest{
		ApiService: a,
		ctx: ctx,
		organizationId: organizationId,
	}
}

// Execute executes the request
//  @return GetTasksResponse
func (a *TasksAPIService) GetTasksExecute(r ApiGetTasksRequest) (*GetTasksResponse, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodGet
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *GetTasksResponse
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "TasksAPIService.GetTasks")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/organizations/{organization_id}/tasks"
	localVarPath = strings.Replace(localVarPath, "{"+"organization_id"+"}", url.PathEscape(parameterValueToString(r.organizationId, "organizationId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if r.interactionId != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "interaction_id", r.interactionId, "")
	}
	if r.blocker != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "blocker", r.blocker, "")
	}
	if r.scriptId != nil {
		parameterAddToHeaderOrQuery(localVarQueryParams, "script_id", r.scriptId, "")
	}
	if r.usages != nil {
		t := *r.usages
		if reflect.TypeOf(t).Kind() == reflect.Slice {
			s := reflect.ValueOf(t)
			for i := 0; i < s.Len(); i++ {
				parameterAddToHeaderOrQuery(localVarQueryParams, "usages[]", s.Index(i).Interface(), "multi")
			}
		} else {
			parameterAddToHeaderOrQuery(localVarQueryParams, "usages[]", t, "multi")
		}
	}
	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}

type ApiMarkTaskAsClosedRequest struct {
	ctx context.Context
	ApiService *TasksAPIService
	organizationId string
	completionId string
	markTaskAsClosedRequest *MarkTaskAsClosedRequest
}

// 
func (r ApiMarkTaskAsClosedRequest) MarkTaskAsClosedRequest(markTaskAsClosedRequest MarkTaskAsClosedRequest) ApiMarkTaskAsClosedRequest {
	r.markTaskAsClosedRequest = &markTaskAsClosedRequest
	return r
}

func (r ApiMarkTaskAsClosedRequest) Execute() (*http.Response, error) {
	return r.ApiService.MarkTaskAsClosedExecute(r)
}

/*
MarkTaskAsClosed Mark task as closed

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param organizationId
 @param completionId
 @return ApiMarkTaskAsClosedRequest
*/
func (a *TasksAPIService) MarkTaskAsClosed(ctx context.Context, organizationId string, completionId string) ApiMarkTaskAsClosedRequest {
	return ApiMarkTaskAsClosedRequest{
		ApiService: a,
		ctx: ctx,
		organizationId: organizationId,
		completionId: completionId,
	}
}

// Execute executes the request
func (a *TasksAPIService) MarkTaskAsClosedExecute(r ApiMarkTaskAsClosedRequest) (*http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodPost
		localVarPostBody     interface{}
		formFiles            []formFile
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "TasksAPIService.MarkTaskAsClosed")
	if err != nil {
		return nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/organizations/{organization_id}/tasks-completions/{completion_id}/closed"
	localVarPath = strings.Replace(localVarPath, "{"+"organization_id"+"}", url.PathEscape(parameterValueToString(r.organizationId, "organizationId")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"completion_id"+"}", url.PathEscape(parameterValueToString(r.completionId, "completionId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.markTaskAsClosedRequest
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type ApiPutChannelsChannelIdInteractionsIdTasksTaskIdRequest struct {
	ctx context.Context
	ApiService *TasksAPIService
	taskId string
	organizationId string
	mutableTaskData *MutableTaskData
}

// 
func (r ApiPutChannelsChannelIdInteractionsIdTasksTaskIdRequest) MutableTaskData(mutableTaskData MutableTaskData) ApiPutChannelsChannelIdInteractionsIdTasksTaskIdRequest {
	r.mutableTaskData = &mutableTaskData
	return r
}

func (r ApiPutChannelsChannelIdInteractionsIdTasksTaskIdRequest) Execute() (*Task, *http.Response, error) {
	return r.ApiService.PutChannelsChannelIdInteractionsIdTasksTaskIdExecute(r)
}

/*
PutChannelsChannelIdInteractionsIdTasksTaskId Update task

 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @param taskId
 @param organizationId
 @return ApiPutChannelsChannelIdInteractionsIdTasksTaskIdRequest
*/
func (a *TasksAPIService) PutChannelsChannelIdInteractionsIdTasksTaskId(ctx context.Context, taskId string, organizationId string) ApiPutChannelsChannelIdInteractionsIdTasksTaskIdRequest {
	return ApiPutChannelsChannelIdInteractionsIdTasksTaskIdRequest{
		ApiService: a,
		ctx: ctx,
		taskId: taskId,
		organizationId: organizationId,
	}
}

// Execute executes the request
//  @return Task
func (a *TasksAPIService) PutChannelsChannelIdInteractionsIdTasksTaskIdExecute(r ApiPutChannelsChannelIdInteractionsIdTasksTaskIdRequest) (*Task, *http.Response, error) {
	var (
		localVarHTTPMethod   = http.MethodPut
		localVarPostBody     interface{}
		formFiles            []formFile
		localVarReturnValue  *Task
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "TasksAPIService.PutChannelsChannelIdInteractionsIdTasksTaskId")
	if err != nil {
		return localVarReturnValue, nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath + "/organizations/{organization_id}/tasks/{task_id}"
	localVarPath = strings.Replace(localVarPath, "{"+"task_id"+"}", url.PathEscape(parameterValueToString(r.taskId, "taskId")), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"organization_id"+"}", url.PathEscape(parameterValueToString(r.organizationId, "organizationId")), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	// body params
	localVarPostBody = r.mutableTaskData
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		return localVarReturnValue, nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil || localVarHTTPResponse == nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	localVarBody, err := io.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = io.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		return localVarReturnValue, localVarHTTPResponse, err
	}

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	err = a.client.decode(&localVarReturnValue, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
	if err != nil {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: err.Error(),
		}
		return localVarReturnValue, localVarHTTPResponse, newErr
	}

	return localVarReturnValue, localVarHTTPResponse, nil
}
