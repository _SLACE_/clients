/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
)

// checks if the CreateRolloutRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &CreateRolloutRequest{}

// CreateRolloutRequest struct for CreateRolloutRequest
type CreateRolloutRequest struct {
	ChannelIds []string `json:"channel_ids,omitempty"`
}

// NewCreateRolloutRequest instantiates a new CreateRolloutRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCreateRolloutRequest() *CreateRolloutRequest {
	this := CreateRolloutRequest{}
	return &this
}

// NewCreateRolloutRequestWithDefaults instantiates a new CreateRolloutRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCreateRolloutRequestWithDefaults() *CreateRolloutRequest {
	this := CreateRolloutRequest{}
	return &this
}

// GetChannelIds returns the ChannelIds field value if set, zero value otherwise.
func (o *CreateRolloutRequest) GetChannelIds() []string {
	if o == nil || IsNil(o.ChannelIds) {
		var ret []string
		return ret
	}
	return o.ChannelIds
}

// GetChannelIdsOk returns a tuple with the ChannelIds field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CreateRolloutRequest) GetChannelIdsOk() ([]string, bool) {
	if o == nil || IsNil(o.ChannelIds) {
		return nil, false
	}
	return o.ChannelIds, true
}

// HasChannelIds returns a boolean if a field has been set.
func (o *CreateRolloutRequest) HasChannelIds() bool {
	if o != nil && !IsNil(o.ChannelIds) {
		return true
	}

	return false
}

// SetChannelIds gets a reference to the given []string and assigns it to the ChannelIds field.
func (o *CreateRolloutRequest) SetChannelIds(v []string) {
	o.ChannelIds = v
}

func (o CreateRolloutRequest) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o CreateRolloutRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.ChannelIds) {
		toSerialize["channel_ids"] = o.ChannelIds
	}
	return toSerialize, nil
}

type NullableCreateRolloutRequest struct {
	value *CreateRolloutRequest
	isSet bool
}

func (v NullableCreateRolloutRequest) Get() *CreateRolloutRequest {
	return v.value
}

func (v *NullableCreateRolloutRequest) Set(val *CreateRolloutRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableCreateRolloutRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableCreateRolloutRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCreateRolloutRequest(val *CreateRolloutRequest) *NullableCreateRolloutRequest {
	return &NullableCreateRolloutRequest{value: val, isSet: true}
}

func (v NullableCreateRolloutRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCreateRolloutRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


