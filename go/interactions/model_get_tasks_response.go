/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
)

// checks if the GetTasksResponse type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GetTasksResponse{}

// GetTasksResponse struct for GetTasksResponse
type GetTasksResponse struct {
	Results []Task `json:"results,omitempty"`
	Total *int32 `json:"total,omitempty"`
}

// NewGetTasksResponse instantiates a new GetTasksResponse object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGetTasksResponse() *GetTasksResponse {
	this := GetTasksResponse{}
	return &this
}

// NewGetTasksResponseWithDefaults instantiates a new GetTasksResponse object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGetTasksResponseWithDefaults() *GetTasksResponse {
	this := GetTasksResponse{}
	return &this
}

// GetResults returns the Results field value if set, zero value otherwise.
func (o *GetTasksResponse) GetResults() []Task {
	if o == nil || IsNil(o.Results) {
		var ret []Task
		return ret
	}
	return o.Results
}

// GetResultsOk returns a tuple with the Results field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetTasksResponse) GetResultsOk() ([]Task, bool) {
	if o == nil || IsNil(o.Results) {
		return nil, false
	}
	return o.Results, true
}

// HasResults returns a boolean if a field has been set.
func (o *GetTasksResponse) HasResults() bool {
	if o != nil && !IsNil(o.Results) {
		return true
	}

	return false
}

// SetResults gets a reference to the given []Task and assigns it to the Results field.
func (o *GetTasksResponse) SetResults(v []Task) {
	o.Results = v
}

// GetTotal returns the Total field value if set, zero value otherwise.
func (o *GetTasksResponse) GetTotal() int32 {
	if o == nil || IsNil(o.Total) {
		var ret int32
		return ret
	}
	return *o.Total
}

// GetTotalOk returns a tuple with the Total field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetTasksResponse) GetTotalOk() (*int32, bool) {
	if o == nil || IsNil(o.Total) {
		return nil, false
	}
	return o.Total, true
}

// HasTotal returns a boolean if a field has been set.
func (o *GetTasksResponse) HasTotal() bool {
	if o != nil && !IsNil(o.Total) {
		return true
	}

	return false
}

// SetTotal gets a reference to the given int32 and assigns it to the Total field.
func (o *GetTasksResponse) SetTotal(v int32) {
	o.Total = &v
}

func (o GetTasksResponse) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GetTasksResponse) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Results) {
		toSerialize["results"] = o.Results
	}
	if !IsNil(o.Total) {
		toSerialize["total"] = o.Total
	}
	return toSerialize, nil
}

type NullableGetTasksResponse struct {
	value *GetTasksResponse
	isSet bool
}

func (v NullableGetTasksResponse) Get() *GetTasksResponse {
	return v.value
}

func (v *NullableGetTasksResponse) Set(val *GetTasksResponse) {
	v.value = val
	v.isSet = true
}

func (v NullableGetTasksResponse) IsSet() bool {
	return v.isSet
}

func (v *NullableGetTasksResponse) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGetTasksResponse(val *GetTasksResponse) *NullableGetTasksResponse {
	return &NullableGetTasksResponse{value: val, isSet: true}
}

func (v NullableGetTasksResponse) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGetTasksResponse) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


