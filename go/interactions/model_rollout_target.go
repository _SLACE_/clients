/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
)

// checks if the RolloutTarget type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &RolloutTarget{}

// RolloutTarget struct for RolloutTarget
type RolloutTarget struct {
	// Organization where rollout will be cloned
	OrganizationId string `json:"organization_id"`
	// Main channel where rollout will be done
	MainChannelId string `json:"main_channel_id"`
	// Channels where rollout will be cloned (linked)
	ChannelIds []string `json:"channel_ids,omitempty"`
}

// NewRolloutTarget instantiates a new RolloutTarget object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewRolloutTarget(organizationId string, mainChannelId string) *RolloutTarget {
	this := RolloutTarget{}
	this.OrganizationId = organizationId
	this.MainChannelId = mainChannelId
	return &this
}

// NewRolloutTargetWithDefaults instantiates a new RolloutTarget object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewRolloutTargetWithDefaults() *RolloutTarget {
	this := RolloutTarget{}
	return &this
}

// GetOrganizationId returns the OrganizationId field value
func (o *RolloutTarget) GetOrganizationId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.OrganizationId
}

// GetOrganizationIdOk returns a tuple with the OrganizationId field value
// and a boolean to check if the value has been set.
func (o *RolloutTarget) GetOrganizationIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.OrganizationId, true
}

// SetOrganizationId sets field value
func (o *RolloutTarget) SetOrganizationId(v string) {
	o.OrganizationId = v
}

// GetMainChannelId returns the MainChannelId field value
func (o *RolloutTarget) GetMainChannelId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.MainChannelId
}

// GetMainChannelIdOk returns a tuple with the MainChannelId field value
// and a boolean to check if the value has been set.
func (o *RolloutTarget) GetMainChannelIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.MainChannelId, true
}

// SetMainChannelId sets field value
func (o *RolloutTarget) SetMainChannelId(v string) {
	o.MainChannelId = v
}

// GetChannelIds returns the ChannelIds field value if set, zero value otherwise.
func (o *RolloutTarget) GetChannelIds() []string {
	if o == nil || IsNil(o.ChannelIds) {
		var ret []string
		return ret
	}
	return o.ChannelIds
}

// GetChannelIdsOk returns a tuple with the ChannelIds field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *RolloutTarget) GetChannelIdsOk() ([]string, bool) {
	if o == nil || IsNil(o.ChannelIds) {
		return nil, false
	}
	return o.ChannelIds, true
}

// HasChannelIds returns a boolean if a field has been set.
func (o *RolloutTarget) HasChannelIds() bool {
	if o != nil && !IsNil(o.ChannelIds) {
		return true
	}

	return false
}

// SetChannelIds gets a reference to the given []string and assigns it to the ChannelIds field.
func (o *RolloutTarget) SetChannelIds(v []string) {
	o.ChannelIds = v
}

func (o RolloutTarget) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o RolloutTarget) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["organization_id"] = o.OrganizationId
	toSerialize["main_channel_id"] = o.MainChannelId
	if !IsNil(o.ChannelIds) {
		toSerialize["channel_ids"] = o.ChannelIds
	}
	return toSerialize, nil
}

type NullableRolloutTarget struct {
	value *RolloutTarget
	isSet bool
}

func (v NullableRolloutTarget) Get() *RolloutTarget {
	return v.value
}

func (v *NullableRolloutTarget) Set(val *RolloutTarget) {
	v.value = val
	v.isSet = true
}

func (v NullableRolloutTarget) IsSet() bool {
	return v.isSet
}

func (v *NullableRolloutTarget) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableRolloutTarget(val *RolloutTarget) *NullableRolloutTarget {
	return &NullableRolloutTarget{value: val, isSet: true}
}

func (v NullableRolloutTarget) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableRolloutTarget) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


