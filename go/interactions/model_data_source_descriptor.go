/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
	"time"
)

// checks if the DataSourceDescriptor type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &DataSourceDescriptor{}

// DataSourceDescriptor struct for DataSourceDescriptor
type DataSourceDescriptor struct {
	Id string `json:"id"`
	GroupId *string `json:"group_id,omitempty"`
	SourceType DataSource `json:"source_type"`
	SubType string `json:"sub_type"`
	Description string `json:"description"`
	LastSyncAt time.Time `json:"last_sync_at"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Properties []DataSourceDescriptorProperty `json:"properties"`
}

// NewDataSourceDescriptor instantiates a new DataSourceDescriptor object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewDataSourceDescriptor(id string, sourceType DataSource, subType string, description string, lastSyncAt time.Time, createdAt time.Time, updatedAt time.Time, properties []DataSourceDescriptorProperty) *DataSourceDescriptor {
	this := DataSourceDescriptor{}
	this.Id = id
	this.SourceType = sourceType
	this.SubType = subType
	this.Description = description
	this.LastSyncAt = lastSyncAt
	this.CreatedAt = createdAt
	this.UpdatedAt = updatedAt
	this.Properties = properties
	return &this
}

// NewDataSourceDescriptorWithDefaults instantiates a new DataSourceDescriptor object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewDataSourceDescriptorWithDefaults() *DataSourceDescriptor {
	this := DataSourceDescriptor{}
	return &this
}

// GetId returns the Id field value
func (o *DataSourceDescriptor) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *DataSourceDescriptor) GetIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *DataSourceDescriptor) SetId(v string) {
	o.Id = v
}

// GetGroupId returns the GroupId field value if set, zero value otherwise.
func (o *DataSourceDescriptor) GetGroupId() string {
	if o == nil || IsNil(o.GroupId) {
		var ret string
		return ret
	}
	return *o.GroupId
}

// GetGroupIdOk returns a tuple with the GroupId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *DataSourceDescriptor) GetGroupIdOk() (*string, bool) {
	if o == nil || IsNil(o.GroupId) {
		return nil, false
	}
	return o.GroupId, true
}

// HasGroupId returns a boolean if a field has been set.
func (o *DataSourceDescriptor) HasGroupId() bool {
	if o != nil && !IsNil(o.GroupId) {
		return true
	}

	return false
}

// SetGroupId gets a reference to the given string and assigns it to the GroupId field.
func (o *DataSourceDescriptor) SetGroupId(v string) {
	o.GroupId = &v
}

// GetSourceType returns the SourceType field value
func (o *DataSourceDescriptor) GetSourceType() DataSource {
	if o == nil {
		var ret DataSource
		return ret
	}

	return o.SourceType
}

// GetSourceTypeOk returns a tuple with the SourceType field value
// and a boolean to check if the value has been set.
func (o *DataSourceDescriptor) GetSourceTypeOk() (*DataSource, bool) {
	if o == nil {
		return nil, false
	}
	return &o.SourceType, true
}

// SetSourceType sets field value
func (o *DataSourceDescriptor) SetSourceType(v DataSource) {
	o.SourceType = v
}

// GetSubType returns the SubType field value
func (o *DataSourceDescriptor) GetSubType() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.SubType
}

// GetSubTypeOk returns a tuple with the SubType field value
// and a boolean to check if the value has been set.
func (o *DataSourceDescriptor) GetSubTypeOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.SubType, true
}

// SetSubType sets field value
func (o *DataSourceDescriptor) SetSubType(v string) {
	o.SubType = v
}

// GetDescription returns the Description field value
func (o *DataSourceDescriptor) GetDescription() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Description
}

// GetDescriptionOk returns a tuple with the Description field value
// and a boolean to check if the value has been set.
func (o *DataSourceDescriptor) GetDescriptionOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Description, true
}

// SetDescription sets field value
func (o *DataSourceDescriptor) SetDescription(v string) {
	o.Description = v
}

// GetLastSyncAt returns the LastSyncAt field value
func (o *DataSourceDescriptor) GetLastSyncAt() time.Time {
	if o == nil {
		var ret time.Time
		return ret
	}

	return o.LastSyncAt
}

// GetLastSyncAtOk returns a tuple with the LastSyncAt field value
// and a boolean to check if the value has been set.
func (o *DataSourceDescriptor) GetLastSyncAtOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return &o.LastSyncAt, true
}

// SetLastSyncAt sets field value
func (o *DataSourceDescriptor) SetLastSyncAt(v time.Time) {
	o.LastSyncAt = v
}

// GetCreatedAt returns the CreatedAt field value
func (o *DataSourceDescriptor) GetCreatedAt() time.Time {
	if o == nil {
		var ret time.Time
		return ret
	}

	return o.CreatedAt
}

// GetCreatedAtOk returns a tuple with the CreatedAt field value
// and a boolean to check if the value has been set.
func (o *DataSourceDescriptor) GetCreatedAtOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return &o.CreatedAt, true
}

// SetCreatedAt sets field value
func (o *DataSourceDescriptor) SetCreatedAt(v time.Time) {
	o.CreatedAt = v
}

// GetUpdatedAt returns the UpdatedAt field value
func (o *DataSourceDescriptor) GetUpdatedAt() time.Time {
	if o == nil {
		var ret time.Time
		return ret
	}

	return o.UpdatedAt
}

// GetUpdatedAtOk returns a tuple with the UpdatedAt field value
// and a boolean to check if the value has been set.
func (o *DataSourceDescriptor) GetUpdatedAtOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return &o.UpdatedAt, true
}

// SetUpdatedAt sets field value
func (o *DataSourceDescriptor) SetUpdatedAt(v time.Time) {
	o.UpdatedAt = v
}

// GetProperties returns the Properties field value
func (o *DataSourceDescriptor) GetProperties() []DataSourceDescriptorProperty {
	if o == nil {
		var ret []DataSourceDescriptorProperty
		return ret
	}

	return o.Properties
}

// GetPropertiesOk returns a tuple with the Properties field value
// and a boolean to check if the value has been set.
func (o *DataSourceDescriptor) GetPropertiesOk() ([]DataSourceDescriptorProperty, bool) {
	if o == nil {
		return nil, false
	}
	return o.Properties, true
}

// SetProperties sets field value
func (o *DataSourceDescriptor) SetProperties(v []DataSourceDescriptorProperty) {
	o.Properties = v
}

func (o DataSourceDescriptor) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o DataSourceDescriptor) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["id"] = o.Id
	if !IsNil(o.GroupId) {
		toSerialize["group_id"] = o.GroupId
	}
	toSerialize["source_type"] = o.SourceType
	toSerialize["sub_type"] = o.SubType
	toSerialize["description"] = o.Description
	toSerialize["last_sync_at"] = o.LastSyncAt
	toSerialize["created_at"] = o.CreatedAt
	toSerialize["updated_at"] = o.UpdatedAt
	toSerialize["properties"] = o.Properties
	return toSerialize, nil
}

type NullableDataSourceDescriptor struct {
	value *DataSourceDescriptor
	isSet bool
}

func (v NullableDataSourceDescriptor) Get() *DataSourceDescriptor {
	return v.value
}

func (v *NullableDataSourceDescriptor) Set(val *DataSourceDescriptor) {
	v.value = val
	v.isSet = true
}

func (v NullableDataSourceDescriptor) IsSet() bool {
	return v.isSet
}

func (v *NullableDataSourceDescriptor) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableDataSourceDescriptor(val *DataSourceDescriptor) *NullableDataSourceDescriptor {
	return &NullableDataSourceDescriptor{value: val, isSet: true}
}

func (v NullableDataSourceDescriptor) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableDataSourceDescriptor) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


