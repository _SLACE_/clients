# StateUpdateRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**State** | [**State**](State.md) |  | 

## Methods

### NewStateUpdateRequest

`func NewStateUpdateRequest(state State, ) *StateUpdateRequest`

NewStateUpdateRequest instantiates a new StateUpdateRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStateUpdateRequestWithDefaults

`func NewStateUpdateRequestWithDefaults() *StateUpdateRequest`

NewStateUpdateRequestWithDefaults instantiates a new StateUpdateRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetState

`func (o *StateUpdateRequest) GetState() State`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *StateUpdateRequest) GetStateOk() (*State, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *StateUpdateRequest) SetState(v State)`

SetState sets State field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


