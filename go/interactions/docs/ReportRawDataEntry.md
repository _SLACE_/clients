# ReportRawDataEntry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Date** | **string** |  | 
**Year** | **string** |  | 
**Month** | **string** |  | 
**Day** | **string** |  | 
**Hour** | **string** |  | 
**Weekday** | **string** |  | 
**OrganizationId** | **string** |  | 
**ChannelId** | **string** |  | 
**ChannelName** | **string** |  | 
**LocationId** | **string** |  | 
**LocationName** | **string** |  | 
**Consents** | **int32** |  | 
**ChatStarted** | **int32** |  | 
**MainActions** | **int32** |  | 
**Investments** | **int32** |  | 
**Timestamp** | **time.Time** |  | 

## Methods

### NewReportRawDataEntry

`func NewReportRawDataEntry(date string, year string, month string, day string, hour string, weekday string, organizationId string, channelId string, channelName string, locationId string, locationName string, consents int32, chatStarted int32, mainActions int32, investments int32, timestamp time.Time, ) *ReportRawDataEntry`

NewReportRawDataEntry instantiates a new ReportRawDataEntry object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReportRawDataEntryWithDefaults

`func NewReportRawDataEntryWithDefaults() *ReportRawDataEntry`

NewReportRawDataEntryWithDefaults instantiates a new ReportRawDataEntry object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDate

`func (o *ReportRawDataEntry) GetDate() string`

GetDate returns the Date field if non-nil, zero value otherwise.

### GetDateOk

`func (o *ReportRawDataEntry) GetDateOk() (*string, bool)`

GetDateOk returns a tuple with the Date field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDate

`func (o *ReportRawDataEntry) SetDate(v string)`

SetDate sets Date field to given value.


### GetYear

`func (o *ReportRawDataEntry) GetYear() string`

GetYear returns the Year field if non-nil, zero value otherwise.

### GetYearOk

`func (o *ReportRawDataEntry) GetYearOk() (*string, bool)`

GetYearOk returns a tuple with the Year field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetYear

`func (o *ReportRawDataEntry) SetYear(v string)`

SetYear sets Year field to given value.


### GetMonth

`func (o *ReportRawDataEntry) GetMonth() string`

GetMonth returns the Month field if non-nil, zero value otherwise.

### GetMonthOk

`func (o *ReportRawDataEntry) GetMonthOk() (*string, bool)`

GetMonthOk returns a tuple with the Month field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMonth

`func (o *ReportRawDataEntry) SetMonth(v string)`

SetMonth sets Month field to given value.


### GetDay

`func (o *ReportRawDataEntry) GetDay() string`

GetDay returns the Day field if non-nil, zero value otherwise.

### GetDayOk

`func (o *ReportRawDataEntry) GetDayOk() (*string, bool)`

GetDayOk returns a tuple with the Day field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDay

`func (o *ReportRawDataEntry) SetDay(v string)`

SetDay sets Day field to given value.


### GetHour

`func (o *ReportRawDataEntry) GetHour() string`

GetHour returns the Hour field if non-nil, zero value otherwise.

### GetHourOk

`func (o *ReportRawDataEntry) GetHourOk() (*string, bool)`

GetHourOk returns a tuple with the Hour field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHour

`func (o *ReportRawDataEntry) SetHour(v string)`

SetHour sets Hour field to given value.


### GetWeekday

`func (o *ReportRawDataEntry) GetWeekday() string`

GetWeekday returns the Weekday field if non-nil, zero value otherwise.

### GetWeekdayOk

`func (o *ReportRawDataEntry) GetWeekdayOk() (*string, bool)`

GetWeekdayOk returns a tuple with the Weekday field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWeekday

`func (o *ReportRawDataEntry) SetWeekday(v string)`

SetWeekday sets Weekday field to given value.


### GetOrganizationId

`func (o *ReportRawDataEntry) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *ReportRawDataEntry) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *ReportRawDataEntry) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetChannelId

`func (o *ReportRawDataEntry) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *ReportRawDataEntry) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *ReportRawDataEntry) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetChannelName

`func (o *ReportRawDataEntry) GetChannelName() string`

GetChannelName returns the ChannelName field if non-nil, zero value otherwise.

### GetChannelNameOk

`func (o *ReportRawDataEntry) GetChannelNameOk() (*string, bool)`

GetChannelNameOk returns a tuple with the ChannelName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelName

`func (o *ReportRawDataEntry) SetChannelName(v string)`

SetChannelName sets ChannelName field to given value.


### GetLocationId

`func (o *ReportRawDataEntry) GetLocationId() string`

GetLocationId returns the LocationId field if non-nil, zero value otherwise.

### GetLocationIdOk

`func (o *ReportRawDataEntry) GetLocationIdOk() (*string, bool)`

GetLocationIdOk returns a tuple with the LocationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationId

`func (o *ReportRawDataEntry) SetLocationId(v string)`

SetLocationId sets LocationId field to given value.


### GetLocationName

`func (o *ReportRawDataEntry) GetLocationName() string`

GetLocationName returns the LocationName field if non-nil, zero value otherwise.

### GetLocationNameOk

`func (o *ReportRawDataEntry) GetLocationNameOk() (*string, bool)`

GetLocationNameOk returns a tuple with the LocationName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationName

`func (o *ReportRawDataEntry) SetLocationName(v string)`

SetLocationName sets LocationName field to given value.


### GetConsents

`func (o *ReportRawDataEntry) GetConsents() int32`

GetConsents returns the Consents field if non-nil, zero value otherwise.

### GetConsentsOk

`func (o *ReportRawDataEntry) GetConsentsOk() (*int32, bool)`

GetConsentsOk returns a tuple with the Consents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConsents

`func (o *ReportRawDataEntry) SetConsents(v int32)`

SetConsents sets Consents field to given value.


### GetChatStarted

`func (o *ReportRawDataEntry) GetChatStarted() int32`

GetChatStarted returns the ChatStarted field if non-nil, zero value otherwise.

### GetChatStartedOk

`func (o *ReportRawDataEntry) GetChatStartedOk() (*int32, bool)`

GetChatStartedOk returns a tuple with the ChatStarted field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChatStarted

`func (o *ReportRawDataEntry) SetChatStarted(v int32)`

SetChatStarted sets ChatStarted field to given value.


### GetMainActions

`func (o *ReportRawDataEntry) GetMainActions() int32`

GetMainActions returns the MainActions field if non-nil, zero value otherwise.

### GetMainActionsOk

`func (o *ReportRawDataEntry) GetMainActionsOk() (*int32, bool)`

GetMainActionsOk returns a tuple with the MainActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMainActions

`func (o *ReportRawDataEntry) SetMainActions(v int32)`

SetMainActions sets MainActions field to given value.


### GetInvestments

`func (o *ReportRawDataEntry) GetInvestments() int32`

GetInvestments returns the Investments field if non-nil, zero value otherwise.

### GetInvestmentsOk

`func (o *ReportRawDataEntry) GetInvestmentsOk() (*int32, bool)`

GetInvestmentsOk returns a tuple with the Investments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInvestments

`func (o *ReportRawDataEntry) SetInvestments(v int32)`

SetInvestments sets Investments field to given value.


### GetTimestamp

`func (o *ReportRawDataEntry) GetTimestamp() time.Time`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *ReportRawDataEntry) GetTimestampOk() (*time.Time, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *ReportRawDataEntry) SetTimestamp(v time.Time)`

SetTimestamp sets Timestamp field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


