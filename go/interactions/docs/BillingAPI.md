# \BillingAPI

All URIs are relative to *https://interactions.slace.dev/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateMediaBudget**](BillingAPI.md#CreateMediaBudget) | **Post** /billing/media-budgets | Create media budget
[**CreateMediaFee**](BillingAPI.md#CreateMediaFee) | **Post** /billing/media-fees | Create media fee
[**GetMediaBudget**](BillingAPI.md#GetMediaBudget) | **Get** /billing/media-budgets/{media_budget_id} | Get media budget
[**GetMediaBudgets**](BillingAPI.md#GetMediaBudgets) | **Get** /billing/media-budgets | Get media budgets
[**GetMediaFee**](BillingAPI.md#GetMediaFee) | **Get** /billing/media-fees/{media_fee_id} | Get media fee
[**GetMediaFees**](BillingAPI.md#GetMediaFees) | **Get** /billing/media-fees | Get media fees
[**UpdateMediaBudget**](BillingAPI.md#UpdateMediaBudget) | **Put** /billing/media-budgets/{media_budget_id} | Update media budget
[**UpdateMediaFee**](BillingAPI.md#UpdateMediaFee) | **Put** /billing/media-fees/{media_fee_id} | Update media fee



## CreateMediaBudget

> MediaBudget CreateMediaBudget(ctx).MainRolloutId(mainRolloutId).MediaBudget(mediaBudget).Execute()

Create media budget

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    mainRolloutId := "mainRolloutId_example" // string |  (optional)
    mediaBudget := *openapiclient.NewMediaBudget() // MediaBudget |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.CreateMediaBudget(context.Background()).MainRolloutId(mainRolloutId).MediaBudget(mediaBudget).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.CreateMediaBudget``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateMediaBudget`: MediaBudget
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.CreateMediaBudget`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateMediaBudgetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mainRolloutId** | **string** |  | 
 **mediaBudget** | [**MediaBudget**](MediaBudget.md) |  | 

### Return type

[**MediaBudget**](MediaBudget.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateMediaFee

> MediaFee CreateMediaFee(ctx).RolloutId(rolloutId).MediaFee(mediaFee).Execute()

Create media fee

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    rolloutId := "rolloutId_example" // string |  (optional)
    mediaFee := *openapiclient.NewMediaFee() // MediaFee |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.CreateMediaFee(context.Background()).RolloutId(rolloutId).MediaFee(mediaFee).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.CreateMediaFee``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateMediaFee`: MediaFee
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.CreateMediaFee`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateMediaFeeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rolloutId** | **string** |  | 
 **mediaFee** | [**MediaFee**](MediaFee.md) |  | 

### Return type

[**MediaFee**](MediaFee.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMediaBudget

> MediaBudget GetMediaBudget(ctx, mediaBudgetId).Execute()

Get media budget

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    mediaBudgetId := "mediaBudgetId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.GetMediaBudget(context.Background(), mediaBudgetId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.GetMediaBudget``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMediaBudget`: MediaBudget
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.GetMediaBudget`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**mediaBudgetId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetMediaBudgetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**MediaBudget**](MediaBudget.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMediaBudgets

> GetMediaBudgets200Response GetMediaBudgets(ctx).MainRolloutId(mainRolloutId).Execute()

Get media budgets

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    mainRolloutId := "mainRolloutId_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.GetMediaBudgets(context.Background()).MainRolloutId(mainRolloutId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.GetMediaBudgets``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMediaBudgets`: GetMediaBudgets200Response
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.GetMediaBudgets`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetMediaBudgetsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mainRolloutId** | **string** |  | 

### Return type

[**GetMediaBudgets200Response**](GetMediaBudgets200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMediaFee

> MediaFee GetMediaFee(ctx, mediaFeeId).Execute()

Get media fee

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    mediaFeeId := "mediaFeeId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.GetMediaFee(context.Background(), mediaFeeId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.GetMediaFee``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMediaFee`: MediaFee
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.GetMediaFee`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**mediaFeeId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetMediaFeeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**MediaFee**](MediaFee.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMediaFees

> GetMediaFees200Response GetMediaFees(ctx).RolloutId(rolloutId).Execute()

Get media fees

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    rolloutId := "rolloutId_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.GetMediaFees(context.Background()).RolloutId(rolloutId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.GetMediaFees``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMediaFees`: GetMediaFees200Response
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.GetMediaFees`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetMediaFeesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rolloutId** | **string** |  | 

### Return type

[**GetMediaFees200Response**](GetMediaFees200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateMediaBudget

> MediaBudget UpdateMediaBudget(ctx, mediaBudgetId).MediaBudget(mediaBudget).Execute()

Update media budget

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    mediaBudgetId := "mediaBudgetId_example" // string | 
    mediaBudget := *openapiclient.NewMediaBudget() // MediaBudget |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.UpdateMediaBudget(context.Background(), mediaBudgetId).MediaBudget(mediaBudget).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.UpdateMediaBudget``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateMediaBudget`: MediaBudget
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.UpdateMediaBudget`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**mediaBudgetId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateMediaBudgetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **mediaBudget** | [**MediaBudget**](MediaBudget.md) |  | 

### Return type

[**MediaBudget**](MediaBudget.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateMediaFee

> MediaFee UpdateMediaFee(ctx, mediaFeeId).MediaFee(mediaFee).Execute()

Update media fee

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    mediaFeeId := "mediaFeeId_example" // string | 
    mediaFee := *openapiclient.NewMediaFee() // MediaFee |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BillingAPI.UpdateMediaFee(context.Background(), mediaFeeId).MediaFee(mediaFee).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BillingAPI.UpdateMediaFee``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateMediaFee`: MediaFee
    fmt.Fprintf(os.Stdout, "Response from `BillingAPI.UpdateMediaFee`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**mediaFeeId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateMediaFeeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **mediaFee** | [**MediaFee**](MediaFee.md) |  | 

### Return type

[**MediaFee**](MediaFee.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

