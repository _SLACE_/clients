# CxTemplateInstallationMutableDataTriggersCustomDataInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InteractionId** | Pointer to **string** |  | [optional] 
**Campaign** | Pointer to [**Campaign**](Campaign.md) |  | [optional] 

## Methods

### NewCxTemplateInstallationMutableDataTriggersCustomDataInner

`func NewCxTemplateInstallationMutableDataTriggersCustomDataInner() *CxTemplateInstallationMutableDataTriggersCustomDataInner`

NewCxTemplateInstallationMutableDataTriggersCustomDataInner instantiates a new CxTemplateInstallationMutableDataTriggersCustomDataInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateInstallationMutableDataTriggersCustomDataInnerWithDefaults

`func NewCxTemplateInstallationMutableDataTriggersCustomDataInnerWithDefaults() *CxTemplateInstallationMutableDataTriggersCustomDataInner`

NewCxTemplateInstallationMutableDataTriggersCustomDataInnerWithDefaults instantiates a new CxTemplateInstallationMutableDataTriggersCustomDataInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInteractionId

`func (o *CxTemplateInstallationMutableDataTriggersCustomDataInner) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *CxTemplateInstallationMutableDataTriggersCustomDataInner) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *CxTemplateInstallationMutableDataTriggersCustomDataInner) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *CxTemplateInstallationMutableDataTriggersCustomDataInner) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetCampaign

`func (o *CxTemplateInstallationMutableDataTriggersCustomDataInner) GetCampaign() Campaign`

GetCampaign returns the Campaign field if non-nil, zero value otherwise.

### GetCampaignOk

`func (o *CxTemplateInstallationMutableDataTriggersCustomDataInner) GetCampaignOk() (*Campaign, bool)`

GetCampaignOk returns a tuple with the Campaign field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCampaign

`func (o *CxTemplateInstallationMutableDataTriggersCustomDataInner) SetCampaign(v Campaign)`

SetCampaign sets Campaign field to given value.

### HasCampaign

`func (o *CxTemplateInstallationMutableDataTriggersCustomDataInner) HasCampaign() bool`

HasCampaign returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


