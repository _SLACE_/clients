# DataSourceDescriptor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**GroupId** | Pointer to **string** |  | [optional] 
**SourceType** | [**DataSource**](DataSource.md) |  | 
**SubType** | **string** |  | 
**Description** | **string** |  | 
**LastSyncAt** | **time.Time** |  | 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 
**Properties** | [**[]DataSourceDescriptorProperty**](DataSourceDescriptorProperty.md) |  | 

## Methods

### NewDataSourceDescriptor

`func NewDataSourceDescriptor(id string, sourceType DataSource, subType string, description string, lastSyncAt time.Time, createdAt time.Time, updatedAt time.Time, properties []DataSourceDescriptorProperty, ) *DataSourceDescriptor`

NewDataSourceDescriptor instantiates a new DataSourceDescriptor object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDataSourceDescriptorWithDefaults

`func NewDataSourceDescriptorWithDefaults() *DataSourceDescriptor`

NewDataSourceDescriptorWithDefaults instantiates a new DataSourceDescriptor object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *DataSourceDescriptor) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *DataSourceDescriptor) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *DataSourceDescriptor) SetId(v string)`

SetId sets Id field to given value.


### GetGroupId

`func (o *DataSourceDescriptor) GetGroupId() string`

GetGroupId returns the GroupId field if non-nil, zero value otherwise.

### GetGroupIdOk

`func (o *DataSourceDescriptor) GetGroupIdOk() (*string, bool)`

GetGroupIdOk returns a tuple with the GroupId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGroupId

`func (o *DataSourceDescriptor) SetGroupId(v string)`

SetGroupId sets GroupId field to given value.

### HasGroupId

`func (o *DataSourceDescriptor) HasGroupId() bool`

HasGroupId returns a boolean if a field has been set.

### GetSourceType

`func (o *DataSourceDescriptor) GetSourceType() DataSource`

GetSourceType returns the SourceType field if non-nil, zero value otherwise.

### GetSourceTypeOk

`func (o *DataSourceDescriptor) GetSourceTypeOk() (*DataSource, bool)`

GetSourceTypeOk returns a tuple with the SourceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceType

`func (o *DataSourceDescriptor) SetSourceType(v DataSource)`

SetSourceType sets SourceType field to given value.


### GetSubType

`func (o *DataSourceDescriptor) GetSubType() string`

GetSubType returns the SubType field if non-nil, zero value otherwise.

### GetSubTypeOk

`func (o *DataSourceDescriptor) GetSubTypeOk() (*string, bool)`

GetSubTypeOk returns a tuple with the SubType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSubType

`func (o *DataSourceDescriptor) SetSubType(v string)`

SetSubType sets SubType field to given value.


### GetDescription

`func (o *DataSourceDescriptor) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *DataSourceDescriptor) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *DataSourceDescriptor) SetDescription(v string)`

SetDescription sets Description field to given value.


### GetLastSyncAt

`func (o *DataSourceDescriptor) GetLastSyncAt() time.Time`

GetLastSyncAt returns the LastSyncAt field if non-nil, zero value otherwise.

### GetLastSyncAtOk

`func (o *DataSourceDescriptor) GetLastSyncAtOk() (*time.Time, bool)`

GetLastSyncAtOk returns a tuple with the LastSyncAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastSyncAt

`func (o *DataSourceDescriptor) SetLastSyncAt(v time.Time)`

SetLastSyncAt sets LastSyncAt field to given value.


### GetCreatedAt

`func (o *DataSourceDescriptor) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *DataSourceDescriptor) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *DataSourceDescriptor) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *DataSourceDescriptor) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *DataSourceDescriptor) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *DataSourceDescriptor) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetProperties

`func (o *DataSourceDescriptor) GetProperties() []DataSourceDescriptorProperty`

GetProperties returns the Properties field if non-nil, zero value otherwise.

### GetPropertiesOk

`func (o *DataSourceDescriptor) GetPropertiesOk() (*[]DataSourceDescriptorProperty, bool)`

GetPropertiesOk returns a tuple with the Properties field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProperties

`func (o *DataSourceDescriptor) SetProperties(v []DataSourceDescriptorProperty)`

SetProperties sets Properties field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


