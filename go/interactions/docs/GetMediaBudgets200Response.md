# GetMediaBudgets200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Total** | Pointer to **int32** |  | [optional] 
**Results** | Pointer to [**[]MediaBudget**](MediaBudget.md) |  | [optional] 

## Methods

### NewGetMediaBudgets200Response

`func NewGetMediaBudgets200Response() *GetMediaBudgets200Response`

NewGetMediaBudgets200Response instantiates a new GetMediaBudgets200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetMediaBudgets200ResponseWithDefaults

`func NewGetMediaBudgets200ResponseWithDefaults() *GetMediaBudgets200Response`

NewGetMediaBudgets200ResponseWithDefaults instantiates a new GetMediaBudgets200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotal

`func (o *GetMediaBudgets200Response) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *GetMediaBudgets200Response) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *GetMediaBudgets200Response) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *GetMediaBudgets200Response) HasTotal() bool`

HasTotal returns a boolean if a field has been set.

### GetResults

`func (o *GetMediaBudgets200Response) GetResults() []MediaBudget`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *GetMediaBudgets200Response) GetResultsOk() (*[]MediaBudget, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *GetMediaBudgets200Response) SetResults(v []MediaBudget)`

SetResults sets Results field to given value.

### HasResults

`func (o *GetMediaBudgets200Response) HasResults() bool`

HasResults returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


