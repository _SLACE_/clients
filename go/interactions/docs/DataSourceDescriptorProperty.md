# DataSourceDescriptorProperty

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**JsonPath** | **string** |  | 
**Description** | Pointer to **string** |  | [optional] 
**Group** | Pointer to **string** |  | [optional] 
**DataType** | [**DataType**](DataType.md) |  | 

## Methods

### NewDataSourceDescriptorProperty

`func NewDataSourceDescriptorProperty(jsonPath string, dataType DataType, ) *DataSourceDescriptorProperty`

NewDataSourceDescriptorProperty instantiates a new DataSourceDescriptorProperty object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDataSourceDescriptorPropertyWithDefaults

`func NewDataSourceDescriptorPropertyWithDefaults() *DataSourceDescriptorProperty`

NewDataSourceDescriptorPropertyWithDefaults instantiates a new DataSourceDescriptorProperty object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetJsonPath

`func (o *DataSourceDescriptorProperty) GetJsonPath() string`

GetJsonPath returns the JsonPath field if non-nil, zero value otherwise.

### GetJsonPathOk

`func (o *DataSourceDescriptorProperty) GetJsonPathOk() (*string, bool)`

GetJsonPathOk returns a tuple with the JsonPath field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetJsonPath

`func (o *DataSourceDescriptorProperty) SetJsonPath(v string)`

SetJsonPath sets JsonPath field to given value.


### GetDescription

`func (o *DataSourceDescriptorProperty) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *DataSourceDescriptorProperty) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *DataSourceDescriptorProperty) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *DataSourceDescriptorProperty) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetGroup

`func (o *DataSourceDescriptorProperty) GetGroup() string`

GetGroup returns the Group field if non-nil, zero value otherwise.

### GetGroupOk

`func (o *DataSourceDescriptorProperty) GetGroupOk() (*string, bool)`

GetGroupOk returns a tuple with the Group field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGroup

`func (o *DataSourceDescriptorProperty) SetGroup(v string)`

SetGroup sets Group field to given value.

### HasGroup

`func (o *DataSourceDescriptorProperty) HasGroup() bool`

HasGroup returns a boolean if a field has been set.

### GetDataType

`func (o *DataSourceDescriptorProperty) GetDataType() DataType`

GetDataType returns the DataType field if non-nil, zero value otherwise.

### GetDataTypeOk

`func (o *DataSourceDescriptorProperty) GetDataTypeOk() (*DataType, bool)`

GetDataTypeOk returns a tuple with the DataType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataType

`func (o *DataSourceDescriptorProperty) SetDataType(v DataType)`

SetDataType sets DataType field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


