# TriggerTemplate

## Enum


* `TRIGGER_TAP_TO_INTERACT` (value: `"tap_to_interact"`)

* `TRIGGER_INCOMING_MESSAGE` (value: `"incoming_message"`)

* `TRIGGER_EVENT` (value: `"event"`)

* `TRIGGER_PARTNER_LINK` (value: `"partner_link"`)

* `TRIGGER_ENROLLMENT` (value: `"enrollment"`)

* `TRIGGER_CAMPAIGN` (value: `"campaign"`)

* `TRIGGER_AUTORESPONDER` (value: `"autoresponder"`)

* `TRIGGER_EMPTY` (value: `""`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


