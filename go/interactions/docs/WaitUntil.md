# WaitUntil

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Mode** | **string** |  | 
**Until** | **time.Time** |  | 
**AddDays** | Pointer to **int32** | Valid for mode &#x60;time&#x60; only, adds set amount of days to wait | [optional] 
**ExcludeWeekdays** | Pointer to **[]int32** | List of weekdays that wait should omit and jump to next available day.  0 - Sunday | [optional] 
**TimeZoneType** | Pointer to **string** | Which time zone should we use | [optional] 

## Methods

### NewWaitUntil

`func NewWaitUntil(mode string, until time.Time, ) *WaitUntil`

NewWaitUntil instantiates a new WaitUntil object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWaitUntilWithDefaults

`func NewWaitUntilWithDefaults() *WaitUntil`

NewWaitUntilWithDefaults instantiates a new WaitUntil object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMode

`func (o *WaitUntil) GetMode() string`

GetMode returns the Mode field if non-nil, zero value otherwise.

### GetModeOk

`func (o *WaitUntil) GetModeOk() (*string, bool)`

GetModeOk returns a tuple with the Mode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMode

`func (o *WaitUntil) SetMode(v string)`

SetMode sets Mode field to given value.


### GetUntil

`func (o *WaitUntil) GetUntil() time.Time`

GetUntil returns the Until field if non-nil, zero value otherwise.

### GetUntilOk

`func (o *WaitUntil) GetUntilOk() (*time.Time, bool)`

GetUntilOk returns a tuple with the Until field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUntil

`func (o *WaitUntil) SetUntil(v time.Time)`

SetUntil sets Until field to given value.


### GetAddDays

`func (o *WaitUntil) GetAddDays() int32`

GetAddDays returns the AddDays field if non-nil, zero value otherwise.

### GetAddDaysOk

`func (o *WaitUntil) GetAddDaysOk() (*int32, bool)`

GetAddDaysOk returns a tuple with the AddDays field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddDays

`func (o *WaitUntil) SetAddDays(v int32)`

SetAddDays sets AddDays field to given value.

### HasAddDays

`func (o *WaitUntil) HasAddDays() bool`

HasAddDays returns a boolean if a field has been set.

### GetExcludeWeekdays

`func (o *WaitUntil) GetExcludeWeekdays() []int32`

GetExcludeWeekdays returns the ExcludeWeekdays field if non-nil, zero value otherwise.

### GetExcludeWeekdaysOk

`func (o *WaitUntil) GetExcludeWeekdaysOk() (*[]int32, bool)`

GetExcludeWeekdaysOk returns a tuple with the ExcludeWeekdays field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExcludeWeekdays

`func (o *WaitUntil) SetExcludeWeekdays(v []int32)`

SetExcludeWeekdays sets ExcludeWeekdays field to given value.

### HasExcludeWeekdays

`func (o *WaitUntil) HasExcludeWeekdays() bool`

HasExcludeWeekdays returns a boolean if a field has been set.

### GetTimeZoneType

`func (o *WaitUntil) GetTimeZoneType() string`

GetTimeZoneType returns the TimeZoneType field if non-nil, zero value otherwise.

### GetTimeZoneTypeOk

`func (o *WaitUntil) GetTimeZoneTypeOk() (*string, bool)`

GetTimeZoneTypeOk returns a tuple with the TimeZoneType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimeZoneType

`func (o *WaitUntil) SetTimeZoneType(v string)`

SetTimeZoneType sets TimeZoneType field to given value.

### HasTimeZoneType

`func (o *WaitUntil) HasTimeZoneType() bool`

HasTimeZoneType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


