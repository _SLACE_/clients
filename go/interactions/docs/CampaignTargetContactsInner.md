# CampaignTargetContactsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Language** | Pointer to **string** |  | [optional] 

## Methods

### NewCampaignTargetContactsInner

`func NewCampaignTargetContactsInner() *CampaignTargetContactsInner`

NewCampaignTargetContactsInner instantiates a new CampaignTargetContactsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignTargetContactsInnerWithDefaults

`func NewCampaignTargetContactsInnerWithDefaults() *CampaignTargetContactsInner`

NewCampaignTargetContactsInnerWithDefaults instantiates a new CampaignTargetContactsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CampaignTargetContactsInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CampaignTargetContactsInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CampaignTargetContactsInner) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *CampaignTargetContactsInner) HasId() bool`

HasId returns a boolean if a field has been set.

### GetLanguage

`func (o *CampaignTargetContactsInner) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *CampaignTargetContactsInner) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *CampaignTargetContactsInner) SetLanguage(v string)`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *CampaignTargetContactsInner) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


