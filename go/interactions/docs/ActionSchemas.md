# ActionSchemas

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Input** | **map[string]interface{}** | JsonSchema with action configuration data | 
**Output** | Pointer to **map[string]interface{}** | JsonSchema with action output data | [optional] 

## Methods

### NewActionSchemas

`func NewActionSchemas(input map[string]interface{}, ) *ActionSchemas`

NewActionSchemas instantiates a new ActionSchemas object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewActionSchemasWithDefaults

`func NewActionSchemasWithDefaults() *ActionSchemas`

NewActionSchemasWithDefaults instantiates a new ActionSchemas object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInput

`func (o *ActionSchemas) GetInput() map[string]interface{}`

GetInput returns the Input field if non-nil, zero value otherwise.

### GetInputOk

`func (o *ActionSchemas) GetInputOk() (*map[string]interface{}, bool)`

GetInputOk returns a tuple with the Input field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInput

`func (o *ActionSchemas) SetInput(v map[string]interface{})`

SetInput sets Input field to given value.


### GetOutput

`func (o *ActionSchemas) GetOutput() map[string]interface{}`

GetOutput returns the Output field if non-nil, zero value otherwise.

### GetOutputOk

`func (o *ActionSchemas) GetOutputOk() (*map[string]interface{}, bool)`

GetOutputOk returns a tuple with the Output field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOutput

`func (o *ActionSchemas) SetOutput(v map[string]interface{})`

SetOutput sets Output field to given value.

### HasOutput

`func (o *ActionSchemas) HasOutput() bool`

HasOutput returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


