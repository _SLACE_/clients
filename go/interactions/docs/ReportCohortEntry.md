# ReportCohortEntry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InteractionId** | Pointer to **string** |  | [optional] 
**InteractionName** | Pointer to **string** | Available only with group by interaction_id | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**EntryPointId** | Pointer to **string** |  | [optional] 
**LocationId** | Pointer to **string** |  | [optional] 
**Messenger** | Pointer to **string** |  | [optional] 
**TriggerType** | Pointer to **string** |  | [optional] 
**EntryType** | Pointer to **string** |  | [optional] 
**Language** | Pointer to **string** |  | [optional] 
**BrowserName** | Pointer to **string** |  | [optional] 
**DeviceName** | Pointer to **string** |  | [optional] 
**CountryCode** | Pointer to **string** |  | [optional] 
**Segment** | Pointer to **string** |  | [optional] 
**Label** | Pointer to **string** |  | [optional] 
**Date** | Pointer to **string** |  | [optional] 
**WeekDay** | Pointer to **string** |  | [optional] 
**Year** | Pointer to **string** |  | [optional] 
**Month** | Pointer to **string** |  | [optional] 
**CalendarWeek** | Pointer to **string** |  | [optional] 
**Day** | Pointer to **string** |  | [optional] 
**Hour** | Pointer to **string** |  | [optional] 
**Version** | Pointer to **string** |  | [optional] 
**ContextValue** | Pointer to **string** |  | [optional] 
**Interactions** | **int32** |  | 
**UniqueContacts** | **int32** |  | 
**Consents** | **int32** |  | 
**ChatStarted** | **int32** |  | 
**MainActions** | **int32** |  | 
**Investments** | **int32** |  | 

## Methods

### NewReportCohortEntry

`func NewReportCohortEntry(interactions int32, uniqueContacts int32, consents int32, chatStarted int32, mainActions int32, investments int32, ) *ReportCohortEntry`

NewReportCohortEntry instantiates a new ReportCohortEntry object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReportCohortEntryWithDefaults

`func NewReportCohortEntryWithDefaults() *ReportCohortEntry`

NewReportCohortEntryWithDefaults instantiates a new ReportCohortEntry object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInteractionId

`func (o *ReportCohortEntry) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *ReportCohortEntry) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *ReportCohortEntry) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *ReportCohortEntry) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetInteractionName

`func (o *ReportCohortEntry) GetInteractionName() string`

GetInteractionName returns the InteractionName field if non-nil, zero value otherwise.

### GetInteractionNameOk

`func (o *ReportCohortEntry) GetInteractionNameOk() (*string, bool)`

GetInteractionNameOk returns a tuple with the InteractionName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionName

`func (o *ReportCohortEntry) SetInteractionName(v string)`

SetInteractionName sets InteractionName field to given value.

### HasInteractionName

`func (o *ReportCohortEntry) HasInteractionName() bool`

HasInteractionName returns a boolean if a field has been set.

### GetChannelId

`func (o *ReportCohortEntry) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *ReportCohortEntry) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *ReportCohortEntry) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *ReportCohortEntry) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetEntryPointId

`func (o *ReportCohortEntry) GetEntryPointId() string`

GetEntryPointId returns the EntryPointId field if non-nil, zero value otherwise.

### GetEntryPointIdOk

`func (o *ReportCohortEntry) GetEntryPointIdOk() (*string, bool)`

GetEntryPointIdOk returns a tuple with the EntryPointId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEntryPointId

`func (o *ReportCohortEntry) SetEntryPointId(v string)`

SetEntryPointId sets EntryPointId field to given value.

### HasEntryPointId

`func (o *ReportCohortEntry) HasEntryPointId() bool`

HasEntryPointId returns a boolean if a field has been set.

### GetLocationId

`func (o *ReportCohortEntry) GetLocationId() string`

GetLocationId returns the LocationId field if non-nil, zero value otherwise.

### GetLocationIdOk

`func (o *ReportCohortEntry) GetLocationIdOk() (*string, bool)`

GetLocationIdOk returns a tuple with the LocationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationId

`func (o *ReportCohortEntry) SetLocationId(v string)`

SetLocationId sets LocationId field to given value.

### HasLocationId

`func (o *ReportCohortEntry) HasLocationId() bool`

HasLocationId returns a boolean if a field has been set.

### GetMessenger

`func (o *ReportCohortEntry) GetMessenger() string`

GetMessenger returns the Messenger field if non-nil, zero value otherwise.

### GetMessengerOk

`func (o *ReportCohortEntry) GetMessengerOk() (*string, bool)`

GetMessengerOk returns a tuple with the Messenger field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessenger

`func (o *ReportCohortEntry) SetMessenger(v string)`

SetMessenger sets Messenger field to given value.

### HasMessenger

`func (o *ReportCohortEntry) HasMessenger() bool`

HasMessenger returns a boolean if a field has been set.

### GetTriggerType

`func (o *ReportCohortEntry) GetTriggerType() string`

GetTriggerType returns the TriggerType field if non-nil, zero value otherwise.

### GetTriggerTypeOk

`func (o *ReportCohortEntry) GetTriggerTypeOk() (*string, bool)`

GetTriggerTypeOk returns a tuple with the TriggerType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTriggerType

`func (o *ReportCohortEntry) SetTriggerType(v string)`

SetTriggerType sets TriggerType field to given value.

### HasTriggerType

`func (o *ReportCohortEntry) HasTriggerType() bool`

HasTriggerType returns a boolean if a field has been set.

### GetEntryType

`func (o *ReportCohortEntry) GetEntryType() string`

GetEntryType returns the EntryType field if non-nil, zero value otherwise.

### GetEntryTypeOk

`func (o *ReportCohortEntry) GetEntryTypeOk() (*string, bool)`

GetEntryTypeOk returns a tuple with the EntryType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEntryType

`func (o *ReportCohortEntry) SetEntryType(v string)`

SetEntryType sets EntryType field to given value.

### HasEntryType

`func (o *ReportCohortEntry) HasEntryType() bool`

HasEntryType returns a boolean if a field has been set.

### GetLanguage

`func (o *ReportCohortEntry) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *ReportCohortEntry) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *ReportCohortEntry) SetLanguage(v string)`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *ReportCohortEntry) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.

### GetBrowserName

`func (o *ReportCohortEntry) GetBrowserName() string`

GetBrowserName returns the BrowserName field if non-nil, zero value otherwise.

### GetBrowserNameOk

`func (o *ReportCohortEntry) GetBrowserNameOk() (*string, bool)`

GetBrowserNameOk returns a tuple with the BrowserName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBrowserName

`func (o *ReportCohortEntry) SetBrowserName(v string)`

SetBrowserName sets BrowserName field to given value.

### HasBrowserName

`func (o *ReportCohortEntry) HasBrowserName() bool`

HasBrowserName returns a boolean if a field has been set.

### GetDeviceName

`func (o *ReportCohortEntry) GetDeviceName() string`

GetDeviceName returns the DeviceName field if non-nil, zero value otherwise.

### GetDeviceNameOk

`func (o *ReportCohortEntry) GetDeviceNameOk() (*string, bool)`

GetDeviceNameOk returns a tuple with the DeviceName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDeviceName

`func (o *ReportCohortEntry) SetDeviceName(v string)`

SetDeviceName sets DeviceName field to given value.

### HasDeviceName

`func (o *ReportCohortEntry) HasDeviceName() bool`

HasDeviceName returns a boolean if a field has been set.

### GetCountryCode

`func (o *ReportCohortEntry) GetCountryCode() string`

GetCountryCode returns the CountryCode field if non-nil, zero value otherwise.

### GetCountryCodeOk

`func (o *ReportCohortEntry) GetCountryCodeOk() (*string, bool)`

GetCountryCodeOk returns a tuple with the CountryCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountryCode

`func (o *ReportCohortEntry) SetCountryCode(v string)`

SetCountryCode sets CountryCode field to given value.

### HasCountryCode

`func (o *ReportCohortEntry) HasCountryCode() bool`

HasCountryCode returns a boolean if a field has been set.

### GetSegment

`func (o *ReportCohortEntry) GetSegment() string`

GetSegment returns the Segment field if non-nil, zero value otherwise.

### GetSegmentOk

`func (o *ReportCohortEntry) GetSegmentOk() (*string, bool)`

GetSegmentOk returns a tuple with the Segment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSegment

`func (o *ReportCohortEntry) SetSegment(v string)`

SetSegment sets Segment field to given value.

### HasSegment

`func (o *ReportCohortEntry) HasSegment() bool`

HasSegment returns a boolean if a field has been set.

### GetLabel

`func (o *ReportCohortEntry) GetLabel() string`

GetLabel returns the Label field if non-nil, zero value otherwise.

### GetLabelOk

`func (o *ReportCohortEntry) GetLabelOk() (*string, bool)`

GetLabelOk returns a tuple with the Label field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabel

`func (o *ReportCohortEntry) SetLabel(v string)`

SetLabel sets Label field to given value.

### HasLabel

`func (o *ReportCohortEntry) HasLabel() bool`

HasLabel returns a boolean if a field has been set.

### GetDate

`func (o *ReportCohortEntry) GetDate() string`

GetDate returns the Date field if non-nil, zero value otherwise.

### GetDateOk

`func (o *ReportCohortEntry) GetDateOk() (*string, bool)`

GetDateOk returns a tuple with the Date field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDate

`func (o *ReportCohortEntry) SetDate(v string)`

SetDate sets Date field to given value.

### HasDate

`func (o *ReportCohortEntry) HasDate() bool`

HasDate returns a boolean if a field has been set.

### GetWeekDay

`func (o *ReportCohortEntry) GetWeekDay() string`

GetWeekDay returns the WeekDay field if non-nil, zero value otherwise.

### GetWeekDayOk

`func (o *ReportCohortEntry) GetWeekDayOk() (*string, bool)`

GetWeekDayOk returns a tuple with the WeekDay field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWeekDay

`func (o *ReportCohortEntry) SetWeekDay(v string)`

SetWeekDay sets WeekDay field to given value.

### HasWeekDay

`func (o *ReportCohortEntry) HasWeekDay() bool`

HasWeekDay returns a boolean if a field has been set.

### GetYear

`func (o *ReportCohortEntry) GetYear() string`

GetYear returns the Year field if non-nil, zero value otherwise.

### GetYearOk

`func (o *ReportCohortEntry) GetYearOk() (*string, bool)`

GetYearOk returns a tuple with the Year field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetYear

`func (o *ReportCohortEntry) SetYear(v string)`

SetYear sets Year field to given value.

### HasYear

`func (o *ReportCohortEntry) HasYear() bool`

HasYear returns a boolean if a field has been set.

### GetMonth

`func (o *ReportCohortEntry) GetMonth() string`

GetMonth returns the Month field if non-nil, zero value otherwise.

### GetMonthOk

`func (o *ReportCohortEntry) GetMonthOk() (*string, bool)`

GetMonthOk returns a tuple with the Month field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMonth

`func (o *ReportCohortEntry) SetMonth(v string)`

SetMonth sets Month field to given value.

### HasMonth

`func (o *ReportCohortEntry) HasMonth() bool`

HasMonth returns a boolean if a field has been set.

### GetCalendarWeek

`func (o *ReportCohortEntry) GetCalendarWeek() string`

GetCalendarWeek returns the CalendarWeek field if non-nil, zero value otherwise.

### GetCalendarWeekOk

`func (o *ReportCohortEntry) GetCalendarWeekOk() (*string, bool)`

GetCalendarWeekOk returns a tuple with the CalendarWeek field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCalendarWeek

`func (o *ReportCohortEntry) SetCalendarWeek(v string)`

SetCalendarWeek sets CalendarWeek field to given value.

### HasCalendarWeek

`func (o *ReportCohortEntry) HasCalendarWeek() bool`

HasCalendarWeek returns a boolean if a field has been set.

### GetDay

`func (o *ReportCohortEntry) GetDay() string`

GetDay returns the Day field if non-nil, zero value otherwise.

### GetDayOk

`func (o *ReportCohortEntry) GetDayOk() (*string, bool)`

GetDayOk returns a tuple with the Day field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDay

`func (o *ReportCohortEntry) SetDay(v string)`

SetDay sets Day field to given value.

### HasDay

`func (o *ReportCohortEntry) HasDay() bool`

HasDay returns a boolean if a field has been set.

### GetHour

`func (o *ReportCohortEntry) GetHour() string`

GetHour returns the Hour field if non-nil, zero value otherwise.

### GetHourOk

`func (o *ReportCohortEntry) GetHourOk() (*string, bool)`

GetHourOk returns a tuple with the Hour field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHour

`func (o *ReportCohortEntry) SetHour(v string)`

SetHour sets Hour field to given value.

### HasHour

`func (o *ReportCohortEntry) HasHour() bool`

HasHour returns a boolean if a field has been set.

### GetVersion

`func (o *ReportCohortEntry) GetVersion() string`

GetVersion returns the Version field if non-nil, zero value otherwise.

### GetVersionOk

`func (o *ReportCohortEntry) GetVersionOk() (*string, bool)`

GetVersionOk returns a tuple with the Version field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersion

`func (o *ReportCohortEntry) SetVersion(v string)`

SetVersion sets Version field to given value.

### HasVersion

`func (o *ReportCohortEntry) HasVersion() bool`

HasVersion returns a boolean if a field has been set.

### GetContextValue

`func (o *ReportCohortEntry) GetContextValue() string`

GetContextValue returns the ContextValue field if non-nil, zero value otherwise.

### GetContextValueOk

`func (o *ReportCohortEntry) GetContextValueOk() (*string, bool)`

GetContextValueOk returns a tuple with the ContextValue field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContextValue

`func (o *ReportCohortEntry) SetContextValue(v string)`

SetContextValue sets ContextValue field to given value.

### HasContextValue

`func (o *ReportCohortEntry) HasContextValue() bool`

HasContextValue returns a boolean if a field has been set.

### GetInteractions

`func (o *ReportCohortEntry) GetInteractions() int32`

GetInteractions returns the Interactions field if non-nil, zero value otherwise.

### GetInteractionsOk

`func (o *ReportCohortEntry) GetInteractionsOk() (*int32, bool)`

GetInteractionsOk returns a tuple with the Interactions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractions

`func (o *ReportCohortEntry) SetInteractions(v int32)`

SetInteractions sets Interactions field to given value.


### GetUniqueContacts

`func (o *ReportCohortEntry) GetUniqueContacts() int32`

GetUniqueContacts returns the UniqueContacts field if non-nil, zero value otherwise.

### GetUniqueContactsOk

`func (o *ReportCohortEntry) GetUniqueContactsOk() (*int32, bool)`

GetUniqueContactsOk returns a tuple with the UniqueContacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUniqueContacts

`func (o *ReportCohortEntry) SetUniqueContacts(v int32)`

SetUniqueContacts sets UniqueContacts field to given value.


### GetConsents

`func (o *ReportCohortEntry) GetConsents() int32`

GetConsents returns the Consents field if non-nil, zero value otherwise.

### GetConsentsOk

`func (o *ReportCohortEntry) GetConsentsOk() (*int32, bool)`

GetConsentsOk returns a tuple with the Consents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConsents

`func (o *ReportCohortEntry) SetConsents(v int32)`

SetConsents sets Consents field to given value.


### GetChatStarted

`func (o *ReportCohortEntry) GetChatStarted() int32`

GetChatStarted returns the ChatStarted field if non-nil, zero value otherwise.

### GetChatStartedOk

`func (o *ReportCohortEntry) GetChatStartedOk() (*int32, bool)`

GetChatStartedOk returns a tuple with the ChatStarted field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChatStarted

`func (o *ReportCohortEntry) SetChatStarted(v int32)`

SetChatStarted sets ChatStarted field to given value.


### GetMainActions

`func (o *ReportCohortEntry) GetMainActions() int32`

GetMainActions returns the MainActions field if non-nil, zero value otherwise.

### GetMainActionsOk

`func (o *ReportCohortEntry) GetMainActionsOk() (*int32, bool)`

GetMainActionsOk returns a tuple with the MainActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMainActions

`func (o *ReportCohortEntry) SetMainActions(v int32)`

SetMainActions sets MainActions field to given value.


### GetInvestments

`func (o *ReportCohortEntry) GetInvestments() int32`

GetInvestments returns the Investments field if non-nil, zero value otherwise.

### GetInvestmentsOk

`func (o *ReportCohortEntry) GetInvestmentsOk() (*int32, bool)`

GetInvestmentsOk returns a tuple with the Investments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInvestments

`func (o *ReportCohortEntry) SetInvestments(v int32)`

SetInvestments sets Investments field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


