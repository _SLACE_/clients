# ExportStatus

## Enum


* `EXPORT_STATUS_PENDING` (value: `"pending"`)

* `EXPORT_STATUS_READY` (value: `"ready"`)

* `EXPORT_STATUS_FAILED` (value: `"failed"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


