# HighlightMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Template** | **string** |  | 
**Metric** | [**HighlightMetric**](HighlightMetric.md) |  | 
**Frequency** | [**Frequency**](Frequency.md) |  | 
**OutsideBusinessHours** | **bool** |  | 
**Weekdays** | **[]int32** |  | 
**StartAt** | **time.Time** |  | 
**EndAt** | **time.Time** |  | 

## Methods

### NewHighlightMutable

`func NewHighlightMutable(template string, metric HighlightMetric, frequency Frequency, outsideBusinessHours bool, weekdays []int32, startAt time.Time, endAt time.Time, ) *HighlightMutable`

NewHighlightMutable instantiates a new HighlightMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewHighlightMutableWithDefaults

`func NewHighlightMutableWithDefaults() *HighlightMutable`

NewHighlightMutableWithDefaults instantiates a new HighlightMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTemplate

`func (o *HighlightMutable) GetTemplate() string`

GetTemplate returns the Template field if non-nil, zero value otherwise.

### GetTemplateOk

`func (o *HighlightMutable) GetTemplateOk() (*string, bool)`

GetTemplateOk returns a tuple with the Template field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplate

`func (o *HighlightMutable) SetTemplate(v string)`

SetTemplate sets Template field to given value.


### GetMetric

`func (o *HighlightMutable) GetMetric() HighlightMetric`

GetMetric returns the Metric field if non-nil, zero value otherwise.

### GetMetricOk

`func (o *HighlightMutable) GetMetricOk() (*HighlightMetric, bool)`

GetMetricOk returns a tuple with the Metric field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetric

`func (o *HighlightMutable) SetMetric(v HighlightMetric)`

SetMetric sets Metric field to given value.


### GetFrequency

`func (o *HighlightMutable) GetFrequency() Frequency`

GetFrequency returns the Frequency field if non-nil, zero value otherwise.

### GetFrequencyOk

`func (o *HighlightMutable) GetFrequencyOk() (*Frequency, bool)`

GetFrequencyOk returns a tuple with the Frequency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFrequency

`func (o *HighlightMutable) SetFrequency(v Frequency)`

SetFrequency sets Frequency field to given value.


### GetOutsideBusinessHours

`func (o *HighlightMutable) GetOutsideBusinessHours() bool`

GetOutsideBusinessHours returns the OutsideBusinessHours field if non-nil, zero value otherwise.

### GetOutsideBusinessHoursOk

`func (o *HighlightMutable) GetOutsideBusinessHoursOk() (*bool, bool)`

GetOutsideBusinessHoursOk returns a tuple with the OutsideBusinessHours field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOutsideBusinessHours

`func (o *HighlightMutable) SetOutsideBusinessHours(v bool)`

SetOutsideBusinessHours sets OutsideBusinessHours field to given value.


### GetWeekdays

`func (o *HighlightMutable) GetWeekdays() []int32`

GetWeekdays returns the Weekdays field if non-nil, zero value otherwise.

### GetWeekdaysOk

`func (o *HighlightMutable) GetWeekdaysOk() (*[]int32, bool)`

GetWeekdaysOk returns a tuple with the Weekdays field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWeekdays

`func (o *HighlightMutable) SetWeekdays(v []int32)`

SetWeekdays sets Weekdays field to given value.


### GetStartAt

`func (o *HighlightMutable) GetStartAt() time.Time`

GetStartAt returns the StartAt field if non-nil, zero value otherwise.

### GetStartAtOk

`func (o *HighlightMutable) GetStartAtOk() (*time.Time, bool)`

GetStartAtOk returns a tuple with the StartAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartAt

`func (o *HighlightMutable) SetStartAt(v time.Time)`

SetStartAt sets StartAt field to given value.


### GetEndAt

`func (o *HighlightMutable) GetEndAt() time.Time`

GetEndAt returns the EndAt field if non-nil, zero value otherwise.

### GetEndAtOk

`func (o *HighlightMutable) GetEndAtOk() (*time.Time, bool)`

GetEndAtOk returns a tuple with the EndAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEndAt

`func (o *HighlightMutable) SetEndAt(v time.Time)`

SetEndAt sets EndAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


