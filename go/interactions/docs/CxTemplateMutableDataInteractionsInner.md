# CxTemplateMutableDataInteractionsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**PreviewUrls** | Pointer to [**[]CxTemplateMutableDataInteractionsInnerPreviewUrlsInner**](CxTemplateMutableDataInteractionsInnerPreviewUrlsInner.md) |  | [optional] 

## Methods

### NewCxTemplateMutableDataInteractionsInner

`func NewCxTemplateMutableDataInteractionsInner() *CxTemplateMutableDataInteractionsInner`

NewCxTemplateMutableDataInteractionsInner instantiates a new CxTemplateMutableDataInteractionsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateMutableDataInteractionsInnerWithDefaults

`func NewCxTemplateMutableDataInteractionsInnerWithDefaults() *CxTemplateMutableDataInteractionsInner`

NewCxTemplateMutableDataInteractionsInnerWithDefaults instantiates a new CxTemplateMutableDataInteractionsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CxTemplateMutableDataInteractionsInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CxTemplateMutableDataInteractionsInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CxTemplateMutableDataInteractionsInner) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *CxTemplateMutableDataInteractionsInner) HasId() bool`

HasId returns a boolean if a field has been set.

### GetName

`func (o *CxTemplateMutableDataInteractionsInner) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CxTemplateMutableDataInteractionsInner) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CxTemplateMutableDataInteractionsInner) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *CxTemplateMutableDataInteractionsInner) HasName() bool`

HasName returns a boolean if a field has been set.

### GetPreviewUrls

`func (o *CxTemplateMutableDataInteractionsInner) GetPreviewUrls() []CxTemplateMutableDataInteractionsInnerPreviewUrlsInner`

GetPreviewUrls returns the PreviewUrls field if non-nil, zero value otherwise.

### GetPreviewUrlsOk

`func (o *CxTemplateMutableDataInteractionsInner) GetPreviewUrlsOk() (*[]CxTemplateMutableDataInteractionsInnerPreviewUrlsInner, bool)`

GetPreviewUrlsOk returns a tuple with the PreviewUrls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPreviewUrls

`func (o *CxTemplateMutableDataInteractionsInner) SetPreviewUrls(v []CxTemplateMutableDataInteractionsInnerPreviewUrlsInner)`

SetPreviewUrls sets PreviewUrls field to given value.

### HasPreviewUrls

`func (o *CxTemplateMutableDataInteractionsInner) HasPreviewUrls() bool`

HasPreviewUrls returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


