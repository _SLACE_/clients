# CreateGlobalRolloutRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Targets** | Pointer to [**[]RolloutTarget**](RolloutTarget.md) |  | [optional] 
**Async** | Pointer to **bool** | This should be true always. False only for test reasons | [optional] 
**ForceGlobalMedia** | Pointer to **bool** |  | [optional] 

## Methods

### NewCreateGlobalRolloutRequest

`func NewCreateGlobalRolloutRequest() *CreateGlobalRolloutRequest`

NewCreateGlobalRolloutRequest instantiates a new CreateGlobalRolloutRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateGlobalRolloutRequestWithDefaults

`func NewCreateGlobalRolloutRequestWithDefaults() *CreateGlobalRolloutRequest`

NewCreateGlobalRolloutRequestWithDefaults instantiates a new CreateGlobalRolloutRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTargets

`func (o *CreateGlobalRolloutRequest) GetTargets() []RolloutTarget`

GetTargets returns the Targets field if non-nil, zero value otherwise.

### GetTargetsOk

`func (o *CreateGlobalRolloutRequest) GetTargetsOk() (*[]RolloutTarget, bool)`

GetTargetsOk returns a tuple with the Targets field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargets

`func (o *CreateGlobalRolloutRequest) SetTargets(v []RolloutTarget)`

SetTargets sets Targets field to given value.

### HasTargets

`func (o *CreateGlobalRolloutRequest) HasTargets() bool`

HasTargets returns a boolean if a field has been set.

### GetAsync

`func (o *CreateGlobalRolloutRequest) GetAsync() bool`

GetAsync returns the Async field if non-nil, zero value otherwise.

### GetAsyncOk

`func (o *CreateGlobalRolloutRequest) GetAsyncOk() (*bool, bool)`

GetAsyncOk returns a tuple with the Async field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAsync

`func (o *CreateGlobalRolloutRequest) SetAsync(v bool)`

SetAsync sets Async field to given value.

### HasAsync

`func (o *CreateGlobalRolloutRequest) HasAsync() bool`

HasAsync returns a boolean if a field has been set.

### GetForceGlobalMedia

`func (o *CreateGlobalRolloutRequest) GetForceGlobalMedia() bool`

GetForceGlobalMedia returns the ForceGlobalMedia field if non-nil, zero value otherwise.

### GetForceGlobalMediaOk

`func (o *CreateGlobalRolloutRequest) GetForceGlobalMediaOk() (*bool, bool)`

GetForceGlobalMediaOk returns a tuple with the ForceGlobalMedia field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetForceGlobalMedia

`func (o *CreateGlobalRolloutRequest) SetForceGlobalMedia(v bool)`

SetForceGlobalMedia sets ForceGlobalMedia field to given value.

### HasForceGlobalMedia

`func (o *CreateGlobalRolloutRequest) HasForceGlobalMedia() bool`

HasForceGlobalMedia returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


