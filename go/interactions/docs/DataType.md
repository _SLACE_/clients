# DataType

## Enum


* `BOOL` (value: `"bool"`)

* `INT` (value: `"int"`)

* `FLOAT` (value: `"float"`)

* `STRING` (value: `"string"`)

* `STRING_CI` (value: `"string_ci"`)

* `TIME` (value: `"time"`)

* `ARRAY` (value: `"array"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


