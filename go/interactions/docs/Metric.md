# Metric

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**Type** | [**MetricType**](MetricType.md) |  | 
**CustomType** | Pointer to **string** |  | [optional] 
**Trigger** | [**Trigger**](Trigger.md) |  | 
**RelatedActions** | Pointer to **[]string** |  | [optional] 
**Contexts** | Pointer to [**[]MetricContext**](MetricContext.md) |  | [optional] 

## Methods

### NewMetric

`func NewMetric(name string, type_ MetricType, trigger Trigger, ) *Metric`

NewMetric instantiates a new Metric object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMetricWithDefaults

`func NewMetricWithDefaults() *Metric`

NewMetricWithDefaults instantiates a new Metric object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *Metric) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Metric) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Metric) SetName(v string)`

SetName sets Name field to given value.


### GetType

`func (o *Metric) GetType() MetricType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Metric) GetTypeOk() (*MetricType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Metric) SetType(v MetricType)`

SetType sets Type field to given value.


### GetCustomType

`func (o *Metric) GetCustomType() string`

GetCustomType returns the CustomType field if non-nil, zero value otherwise.

### GetCustomTypeOk

`func (o *Metric) GetCustomTypeOk() (*string, bool)`

GetCustomTypeOk returns a tuple with the CustomType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomType

`func (o *Metric) SetCustomType(v string)`

SetCustomType sets CustomType field to given value.

### HasCustomType

`func (o *Metric) HasCustomType() bool`

HasCustomType returns a boolean if a field has been set.

### GetTrigger

`func (o *Metric) GetTrigger() Trigger`

GetTrigger returns the Trigger field if non-nil, zero value otherwise.

### GetTriggerOk

`func (o *Metric) GetTriggerOk() (*Trigger, bool)`

GetTriggerOk returns a tuple with the Trigger field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTrigger

`func (o *Metric) SetTrigger(v Trigger)`

SetTrigger sets Trigger field to given value.


### GetRelatedActions

`func (o *Metric) GetRelatedActions() []string`

GetRelatedActions returns the RelatedActions field if non-nil, zero value otherwise.

### GetRelatedActionsOk

`func (o *Metric) GetRelatedActionsOk() (*[]string, bool)`

GetRelatedActionsOk returns a tuple with the RelatedActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRelatedActions

`func (o *Metric) SetRelatedActions(v []string)`

SetRelatedActions sets RelatedActions field to given value.

### HasRelatedActions

`func (o *Metric) HasRelatedActions() bool`

HasRelatedActions returns a boolean if a field has been set.

### GetContexts

`func (o *Metric) GetContexts() []MetricContext`

GetContexts returns the Contexts field if non-nil, zero value otherwise.

### GetContextsOk

`func (o *Metric) GetContextsOk() (*[]MetricContext, bool)`

GetContextsOk returns a tuple with the Contexts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContexts

`func (o *Metric) SetContexts(v []MetricContext)`

SetContexts sets Contexts field to given value.

### HasContexts

`func (o *Metric) HasContexts() bool`

HasContexts returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


