# InteractionsWithMetricsList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | Pointer to [**[]InteractionWithMetrics**](InteractionWithMetrics.md) |  | [optional] 
**Total** | Pointer to **int32** |  | [optional] 

## Methods

### NewInteractionsWithMetricsList

`func NewInteractionsWithMetricsList() *InteractionsWithMetricsList`

NewInteractionsWithMetricsList instantiates a new InteractionsWithMetricsList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInteractionsWithMetricsListWithDefaults

`func NewInteractionsWithMetricsListWithDefaults() *InteractionsWithMetricsList`

NewInteractionsWithMetricsListWithDefaults instantiates a new InteractionsWithMetricsList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *InteractionsWithMetricsList) GetResults() []InteractionWithMetrics`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *InteractionsWithMetricsList) GetResultsOk() (*[]InteractionWithMetrics, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *InteractionsWithMetricsList) SetResults(v []InteractionWithMetrics)`

SetResults sets Results field to given value.

### HasResults

`func (o *InteractionsWithMetricsList) HasResults() bool`

HasResults returns a boolean if a field has been set.

### GetTotal

`func (o *InteractionsWithMetricsList) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *InteractionsWithMetricsList) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *InteractionsWithMetricsList) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *InteractionsWithMetricsList) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


