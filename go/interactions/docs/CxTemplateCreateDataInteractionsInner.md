# CxTemplateCreateDataInteractionsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**PreviewUrls** | Pointer to [**[]CxTemplateCreateDataInteractionsInnerPreviewUrlsInner**](CxTemplateCreateDataInteractionsInnerPreviewUrlsInner.md) |  | [optional] 

## Methods

### NewCxTemplateCreateDataInteractionsInner

`func NewCxTemplateCreateDataInteractionsInner() *CxTemplateCreateDataInteractionsInner`

NewCxTemplateCreateDataInteractionsInner instantiates a new CxTemplateCreateDataInteractionsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateCreateDataInteractionsInnerWithDefaults

`func NewCxTemplateCreateDataInteractionsInnerWithDefaults() *CxTemplateCreateDataInteractionsInner`

NewCxTemplateCreateDataInteractionsInnerWithDefaults instantiates a new CxTemplateCreateDataInteractionsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CxTemplateCreateDataInteractionsInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CxTemplateCreateDataInteractionsInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CxTemplateCreateDataInteractionsInner) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *CxTemplateCreateDataInteractionsInner) HasId() bool`

HasId returns a boolean if a field has been set.

### GetName

`func (o *CxTemplateCreateDataInteractionsInner) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CxTemplateCreateDataInteractionsInner) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CxTemplateCreateDataInteractionsInner) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *CxTemplateCreateDataInteractionsInner) HasName() bool`

HasName returns a boolean if a field has been set.

### GetPreviewUrls

`func (o *CxTemplateCreateDataInteractionsInner) GetPreviewUrls() []CxTemplateCreateDataInteractionsInnerPreviewUrlsInner`

GetPreviewUrls returns the PreviewUrls field if non-nil, zero value otherwise.

### GetPreviewUrlsOk

`func (o *CxTemplateCreateDataInteractionsInner) GetPreviewUrlsOk() (*[]CxTemplateCreateDataInteractionsInnerPreviewUrlsInner, bool)`

GetPreviewUrlsOk returns a tuple with the PreviewUrls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPreviewUrls

`func (o *CxTemplateCreateDataInteractionsInner) SetPreviewUrls(v []CxTemplateCreateDataInteractionsInnerPreviewUrlsInner)`

SetPreviewUrls sets PreviewUrls field to given value.

### HasPreviewUrls

`func (o *CxTemplateCreateDataInteractionsInner) HasPreviewUrls() bool`

HasPreviewUrls returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


