# RolloutError

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Info** | Pointer to **string** |  | [optional] 
**Source** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 

## Methods

### NewRolloutError

`func NewRolloutError() *RolloutError`

NewRolloutError instantiates a new RolloutError object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRolloutErrorWithDefaults

`func NewRolloutErrorWithDefaults() *RolloutError`

NewRolloutErrorWithDefaults instantiates a new RolloutError object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInfo

`func (o *RolloutError) GetInfo() string`

GetInfo returns the Info field if non-nil, zero value otherwise.

### GetInfoOk

`func (o *RolloutError) GetInfoOk() (*string, bool)`

GetInfoOk returns a tuple with the Info field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInfo

`func (o *RolloutError) SetInfo(v string)`

SetInfo sets Info field to given value.

### HasInfo

`func (o *RolloutError) HasInfo() bool`

HasInfo returns a boolean if a field has been set.

### GetSource

`func (o *RolloutError) GetSource() string`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *RolloutError) GetSourceOk() (*string, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *RolloutError) SetSource(v string)`

SetSource sets Source field to given value.

### HasSource

`func (o *RolloutError) HasSource() bool`

HasSource returns a boolean if a field has been set.

### GetChannelId

`func (o *RolloutError) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *RolloutError) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *RolloutError) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *RolloutError) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetType

`func (o *RolloutError) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *RolloutError) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *RolloutError) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *RolloutError) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


