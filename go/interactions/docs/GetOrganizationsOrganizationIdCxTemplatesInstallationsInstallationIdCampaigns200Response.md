# GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | Pointer to [**[]Campaign**](Campaign.md) |  | [optional] 
**Total** | Pointer to **int32** |  | [optional] 

## Methods

### NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response

`func NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response() *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response`

NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response instantiates a new GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200ResponseWithDefaults

`func NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200ResponseWithDefaults() *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response`

NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200ResponseWithDefaults instantiates a new GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response) GetResults() []Campaign`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response) GetResultsOk() (*[]Campaign, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response) SetResults(v []Campaign)`

SetResults sets Results field to given value.

### HasResults

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response) HasResults() bool`

HasResults returns a boolean if a field has been set.

### GetTotal

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


