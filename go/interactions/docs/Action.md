# Action

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Action id **must** be unique within interaction | 
**Template** | Pointer to **string** |  | [optional] 
**Group** | Pointer to **string** |  | [optional] 
**Type** | [**ActionType**](ActionType.md) |  | 
**FlowType** | Pointer to **string** |  | [optional] 
**Split** | [**[]ActionConfig**](ActionConfig.md) |  | 
**Trigger** | Pointer to [**Trigger**](Trigger.md) |  | [optional] 
**Wait** | Pointer to [**Wait**](Wait.md) |  | [optional] 
**SuccessActions** | Pointer to [**[]Action**](Action.md) |  | [optional] 
**Branches** | Pointer to [**[]Branch**](Branch.md) | Applies to &#x60;branch&#x60; type only | [optional] 
**FailAction** | Pointer to [**Action**](Action.md) |  | [optional] 
**OutputHandlers** | Pointer to [**[]Action**](Action.md) | Available only for actions which return any output | [optional] 
**Main** | **bool** |  | 
**Investment** | **bool** |  | 
**JumpTo** | Pointer to **string** | Id of action to which we should jump on success. | [optional] 

## Methods

### NewAction

`func NewAction(id string, type_ ActionType, split []ActionConfig, main bool, investment bool, ) *Action`

NewAction instantiates a new Action object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewActionWithDefaults

`func NewActionWithDefaults() *Action`

NewActionWithDefaults instantiates a new Action object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Action) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Action) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Action) SetId(v string)`

SetId sets Id field to given value.


### GetTemplate

`func (o *Action) GetTemplate() string`

GetTemplate returns the Template field if non-nil, zero value otherwise.

### GetTemplateOk

`func (o *Action) GetTemplateOk() (*string, bool)`

GetTemplateOk returns a tuple with the Template field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplate

`func (o *Action) SetTemplate(v string)`

SetTemplate sets Template field to given value.

### HasTemplate

`func (o *Action) HasTemplate() bool`

HasTemplate returns a boolean if a field has been set.

### GetGroup

`func (o *Action) GetGroup() string`

GetGroup returns the Group field if non-nil, zero value otherwise.

### GetGroupOk

`func (o *Action) GetGroupOk() (*string, bool)`

GetGroupOk returns a tuple with the Group field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGroup

`func (o *Action) SetGroup(v string)`

SetGroup sets Group field to given value.

### HasGroup

`func (o *Action) HasGroup() bool`

HasGroup returns a boolean if a field has been set.

### GetType

`func (o *Action) GetType() ActionType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Action) GetTypeOk() (*ActionType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Action) SetType(v ActionType)`

SetType sets Type field to given value.


### GetFlowType

`func (o *Action) GetFlowType() string`

GetFlowType returns the FlowType field if non-nil, zero value otherwise.

### GetFlowTypeOk

`func (o *Action) GetFlowTypeOk() (*string, bool)`

GetFlowTypeOk returns a tuple with the FlowType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFlowType

`func (o *Action) SetFlowType(v string)`

SetFlowType sets FlowType field to given value.

### HasFlowType

`func (o *Action) HasFlowType() bool`

HasFlowType returns a boolean if a field has been set.

### GetSplit

`func (o *Action) GetSplit() []ActionConfig`

GetSplit returns the Split field if non-nil, zero value otherwise.

### GetSplitOk

`func (o *Action) GetSplitOk() (*[]ActionConfig, bool)`

GetSplitOk returns a tuple with the Split field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSplit

`func (o *Action) SetSplit(v []ActionConfig)`

SetSplit sets Split field to given value.


### GetTrigger

`func (o *Action) GetTrigger() Trigger`

GetTrigger returns the Trigger field if non-nil, zero value otherwise.

### GetTriggerOk

`func (o *Action) GetTriggerOk() (*Trigger, bool)`

GetTriggerOk returns a tuple with the Trigger field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTrigger

`func (o *Action) SetTrigger(v Trigger)`

SetTrigger sets Trigger field to given value.

### HasTrigger

`func (o *Action) HasTrigger() bool`

HasTrigger returns a boolean if a field has been set.

### GetWait

`func (o *Action) GetWait() Wait`

GetWait returns the Wait field if non-nil, zero value otherwise.

### GetWaitOk

`func (o *Action) GetWaitOk() (*Wait, bool)`

GetWaitOk returns a tuple with the Wait field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWait

`func (o *Action) SetWait(v Wait)`

SetWait sets Wait field to given value.

### HasWait

`func (o *Action) HasWait() bool`

HasWait returns a boolean if a field has been set.

### GetSuccessActions

`func (o *Action) GetSuccessActions() []Action`

GetSuccessActions returns the SuccessActions field if non-nil, zero value otherwise.

### GetSuccessActionsOk

`func (o *Action) GetSuccessActionsOk() (*[]Action, bool)`

GetSuccessActionsOk returns a tuple with the SuccessActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSuccessActions

`func (o *Action) SetSuccessActions(v []Action)`

SetSuccessActions sets SuccessActions field to given value.

### HasSuccessActions

`func (o *Action) HasSuccessActions() bool`

HasSuccessActions returns a boolean if a field has been set.

### GetBranches

`func (o *Action) GetBranches() []Branch`

GetBranches returns the Branches field if non-nil, zero value otherwise.

### GetBranchesOk

`func (o *Action) GetBranchesOk() (*[]Branch, bool)`

GetBranchesOk returns a tuple with the Branches field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBranches

`func (o *Action) SetBranches(v []Branch)`

SetBranches sets Branches field to given value.

### HasBranches

`func (o *Action) HasBranches() bool`

HasBranches returns a boolean if a field has been set.

### GetFailAction

`func (o *Action) GetFailAction() Action`

GetFailAction returns the FailAction field if non-nil, zero value otherwise.

### GetFailActionOk

`func (o *Action) GetFailActionOk() (*Action, bool)`

GetFailActionOk returns a tuple with the FailAction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFailAction

`func (o *Action) SetFailAction(v Action)`

SetFailAction sets FailAction field to given value.

### HasFailAction

`func (o *Action) HasFailAction() bool`

HasFailAction returns a boolean if a field has been set.

### GetOutputHandlers

`func (o *Action) GetOutputHandlers() []Action`

GetOutputHandlers returns the OutputHandlers field if non-nil, zero value otherwise.

### GetOutputHandlersOk

`func (o *Action) GetOutputHandlersOk() (*[]Action, bool)`

GetOutputHandlersOk returns a tuple with the OutputHandlers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOutputHandlers

`func (o *Action) SetOutputHandlers(v []Action)`

SetOutputHandlers sets OutputHandlers field to given value.

### HasOutputHandlers

`func (o *Action) HasOutputHandlers() bool`

HasOutputHandlers returns a boolean if a field has been set.

### GetMain

`func (o *Action) GetMain() bool`

GetMain returns the Main field if non-nil, zero value otherwise.

### GetMainOk

`func (o *Action) GetMainOk() (*bool, bool)`

GetMainOk returns a tuple with the Main field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMain

`func (o *Action) SetMain(v bool)`

SetMain sets Main field to given value.


### GetInvestment

`func (o *Action) GetInvestment() bool`

GetInvestment returns the Investment field if non-nil, zero value otherwise.

### GetInvestmentOk

`func (o *Action) GetInvestmentOk() (*bool, bool)`

GetInvestmentOk returns a tuple with the Investment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInvestment

`func (o *Action) SetInvestment(v bool)`

SetInvestment sets Investment field to given value.


### GetJumpTo

`func (o *Action) GetJumpTo() string`

GetJumpTo returns the JumpTo field if non-nil, zero value otherwise.

### GetJumpToOk

`func (o *Action) GetJumpToOk() (*string, bool)`

GetJumpToOk returns a tuple with the JumpTo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetJumpTo

`func (o *Action) SetJumpTo(v string)`

SetJumpTo sets JumpTo field to given value.

### HasJumpTo

`func (o *Action) HasJumpTo() bool`

HasJumpTo returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


