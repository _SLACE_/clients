# CxTemplateInstallationTriggersCustomDataInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InteractionId** | Pointer to **string** |  | [optional] 
**Campaign** | Pointer to [**Campaign**](Campaign.md) |  | [optional] 

## Methods

### NewCxTemplateInstallationTriggersCustomDataInner

`func NewCxTemplateInstallationTriggersCustomDataInner() *CxTemplateInstallationTriggersCustomDataInner`

NewCxTemplateInstallationTriggersCustomDataInner instantiates a new CxTemplateInstallationTriggersCustomDataInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateInstallationTriggersCustomDataInnerWithDefaults

`func NewCxTemplateInstallationTriggersCustomDataInnerWithDefaults() *CxTemplateInstallationTriggersCustomDataInner`

NewCxTemplateInstallationTriggersCustomDataInnerWithDefaults instantiates a new CxTemplateInstallationTriggersCustomDataInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInteractionId

`func (o *CxTemplateInstallationTriggersCustomDataInner) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *CxTemplateInstallationTriggersCustomDataInner) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *CxTemplateInstallationTriggersCustomDataInner) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *CxTemplateInstallationTriggersCustomDataInner) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetCampaign

`func (o *CxTemplateInstallationTriggersCustomDataInner) GetCampaign() Campaign`

GetCampaign returns the Campaign field if non-nil, zero value otherwise.

### GetCampaignOk

`func (o *CxTemplateInstallationTriggersCustomDataInner) GetCampaignOk() (*Campaign, bool)`

GetCampaignOk returns a tuple with the Campaign field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCampaign

`func (o *CxTemplateInstallationTriggersCustomDataInner) SetCampaign(v Campaign)`

SetCampaign sets Campaign field to given value.

### HasCampaign

`func (o *CxTemplateInstallationTriggersCustomDataInner) HasCampaign() bool`

HasCampaign returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


