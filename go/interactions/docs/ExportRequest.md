# ExportRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**Type** | Pointer to [**ExportType**](ExportType.md) |  | [optional] 
**Format** | Pointer to [**ExportFormat**](ExportFormat.md) |  | [optional] 
**Filters** | Pointer to [**ReportFilters**](ReportFilters.md) |  | [optional] 

## Methods

### NewExportRequest

`func NewExportRequest() *ExportRequest`

NewExportRequest instantiates a new ExportRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExportRequestWithDefaults

`func NewExportRequestWithDefaults() *ExportRequest`

NewExportRequestWithDefaults instantiates a new ExportRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ExportRequest) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ExportRequest) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ExportRequest) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ExportRequest) HasName() bool`

HasName returns a boolean if a field has been set.

### GetType

`func (o *ExportRequest) GetType() ExportType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ExportRequest) GetTypeOk() (*ExportType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ExportRequest) SetType(v ExportType)`

SetType sets Type field to given value.

### HasType

`func (o *ExportRequest) HasType() bool`

HasType returns a boolean if a field has been set.

### GetFormat

`func (o *ExportRequest) GetFormat() ExportFormat`

GetFormat returns the Format field if non-nil, zero value otherwise.

### GetFormatOk

`func (o *ExportRequest) GetFormatOk() (*ExportFormat, bool)`

GetFormatOk returns a tuple with the Format field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormat

`func (o *ExportRequest) SetFormat(v ExportFormat)`

SetFormat sets Format field to given value.

### HasFormat

`func (o *ExportRequest) HasFormat() bool`

HasFormat returns a boolean if a field has been set.

### GetFilters

`func (o *ExportRequest) GetFilters() ReportFilters`

GetFilters returns the Filters field if non-nil, zero value otherwise.

### GetFiltersOk

`func (o *ExportRequest) GetFiltersOk() (*ReportFilters, bool)`

GetFiltersOk returns a tuple with the Filters field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFilters

`func (o *ExportRequest) SetFilters(v ReportFilters)`

SetFilters sets Filters field to given value.

### HasFilters

`func (o *ExportRequest) HasFilters() bool`

HasFilters returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


