# Settings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TriggerTemplates** | [**[]TriggerTemplate**](TriggerTemplate.md) |  | 
**Actions** | [**[]ActionType**](ActionType.md) |  | 
**DataSources** | Pointer to [**map[string]DataSourceProperties**](DataSourceProperties.md) | It&#x60;s a map of DataSourceProperties index by DataSource | [optional] 
**DataTypeOperators** | Pointer to [**map[string][]Operator**](array.md) | It&#x60;s a map of Operator list index by DataType | [optional] 
**ActionsSchemas** | Pointer to **map[string]interface{}** | It&#x60;s a map of ActionSchemas indexed by ActionType | [optional] 

## Methods

### NewSettings

`func NewSettings(triggerTemplates []TriggerTemplate, actions []ActionType, ) *Settings`

NewSettings instantiates a new Settings object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSettingsWithDefaults

`func NewSettingsWithDefaults() *Settings`

NewSettingsWithDefaults instantiates a new Settings object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTriggerTemplates

`func (o *Settings) GetTriggerTemplates() []TriggerTemplate`

GetTriggerTemplates returns the TriggerTemplates field if non-nil, zero value otherwise.

### GetTriggerTemplatesOk

`func (o *Settings) GetTriggerTemplatesOk() (*[]TriggerTemplate, bool)`

GetTriggerTemplatesOk returns a tuple with the TriggerTemplates field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTriggerTemplates

`func (o *Settings) SetTriggerTemplates(v []TriggerTemplate)`

SetTriggerTemplates sets TriggerTemplates field to given value.


### GetActions

`func (o *Settings) GetActions() []ActionType`

GetActions returns the Actions field if non-nil, zero value otherwise.

### GetActionsOk

`func (o *Settings) GetActionsOk() (*[]ActionType, bool)`

GetActionsOk returns a tuple with the Actions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActions

`func (o *Settings) SetActions(v []ActionType)`

SetActions sets Actions field to given value.


### GetDataSources

`func (o *Settings) GetDataSources() map[string]DataSourceProperties`

GetDataSources returns the DataSources field if non-nil, zero value otherwise.

### GetDataSourcesOk

`func (o *Settings) GetDataSourcesOk() (*map[string]DataSourceProperties, bool)`

GetDataSourcesOk returns a tuple with the DataSources field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataSources

`func (o *Settings) SetDataSources(v map[string]DataSourceProperties)`

SetDataSources sets DataSources field to given value.

### HasDataSources

`func (o *Settings) HasDataSources() bool`

HasDataSources returns a boolean if a field has been set.

### GetDataTypeOperators

`func (o *Settings) GetDataTypeOperators() map[string][]Operator`

GetDataTypeOperators returns the DataTypeOperators field if non-nil, zero value otherwise.

### GetDataTypeOperatorsOk

`func (o *Settings) GetDataTypeOperatorsOk() (*map[string][]Operator, bool)`

GetDataTypeOperatorsOk returns a tuple with the DataTypeOperators field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataTypeOperators

`func (o *Settings) SetDataTypeOperators(v map[string][]Operator)`

SetDataTypeOperators sets DataTypeOperators field to given value.

### HasDataTypeOperators

`func (o *Settings) HasDataTypeOperators() bool`

HasDataTypeOperators returns a boolean if a field has been set.

### GetActionsSchemas

`func (o *Settings) GetActionsSchemas() map[string]interface{}`

GetActionsSchemas returns the ActionsSchemas field if non-nil, zero value otherwise.

### GetActionsSchemasOk

`func (o *Settings) GetActionsSchemasOk() (*map[string]interface{}, bool)`

GetActionsSchemasOk returns a tuple with the ActionsSchemas field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActionsSchemas

`func (o *Settings) SetActionsSchemas(v map[string]interface{})`

SetActionsSchemas sets ActionsSchemas field to given value.

### HasActionsSchemas

`func (o *Settings) HasActionsSchemas() bool`

HasActionsSchemas returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


