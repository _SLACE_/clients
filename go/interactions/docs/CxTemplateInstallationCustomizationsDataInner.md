# CxTemplateInstallationCustomizationsDataInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TaskId** | Pointer to **string** |  | [optional] 
**Content** | Pointer to **string** |  | [optional] 

## Methods

### NewCxTemplateInstallationCustomizationsDataInner

`func NewCxTemplateInstallationCustomizationsDataInner() *CxTemplateInstallationCustomizationsDataInner`

NewCxTemplateInstallationCustomizationsDataInner instantiates a new CxTemplateInstallationCustomizationsDataInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateInstallationCustomizationsDataInnerWithDefaults

`func NewCxTemplateInstallationCustomizationsDataInnerWithDefaults() *CxTemplateInstallationCustomizationsDataInner`

NewCxTemplateInstallationCustomizationsDataInnerWithDefaults instantiates a new CxTemplateInstallationCustomizationsDataInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTaskId

`func (o *CxTemplateInstallationCustomizationsDataInner) GetTaskId() string`

GetTaskId returns the TaskId field if non-nil, zero value otherwise.

### GetTaskIdOk

`func (o *CxTemplateInstallationCustomizationsDataInner) GetTaskIdOk() (*string, bool)`

GetTaskIdOk returns a tuple with the TaskId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTaskId

`func (o *CxTemplateInstallationCustomizationsDataInner) SetTaskId(v string)`

SetTaskId sets TaskId field to given value.

### HasTaskId

`func (o *CxTemplateInstallationCustomizationsDataInner) HasTaskId() bool`

HasTaskId returns a boolean if a field has been set.

### GetContent

`func (o *CxTemplateInstallationCustomizationsDataInner) GetContent() string`

GetContent returns the Content field if non-nil, zero value otherwise.

### GetContentOk

`func (o *CxTemplateInstallationCustomizationsDataInner) GetContentOk() (*string, bool)`

GetContentOk returns a tuple with the Content field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContent

`func (o *CxTemplateInstallationCustomizationsDataInner) SetContent(v string)`

SetContent sets Content field to given value.

### HasContent

`func (o *CxTemplateInstallationCustomizationsDataInner) HasContent() bool`

HasContent returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


