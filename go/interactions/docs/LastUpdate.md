# LastUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LastUpdateAt** | **time.Time** |  | 

## Methods

### NewLastUpdate

`func NewLastUpdate(lastUpdateAt time.Time, ) *LastUpdate`

NewLastUpdate instantiates a new LastUpdate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLastUpdateWithDefaults

`func NewLastUpdateWithDefaults() *LastUpdate`

NewLastUpdateWithDefaults instantiates a new LastUpdate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLastUpdateAt

`func (o *LastUpdate) GetLastUpdateAt() time.Time`

GetLastUpdateAt returns the LastUpdateAt field if non-nil, zero value otherwise.

### GetLastUpdateAtOk

`func (o *LastUpdate) GetLastUpdateAtOk() (*time.Time, bool)`

GetLastUpdateAtOk returns a tuple with the LastUpdateAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastUpdateAt

`func (o *LastUpdate) SetLastUpdateAt(v time.Time)`

SetLastUpdateAt sets LastUpdateAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


