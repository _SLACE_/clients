# NewEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Random string will be generated if empty | [optional] 
**Source** | **string** | Some common identifier of source (like module or app name) | 
**Type** | **string** | Event name | 
**ChannelId** | **string** |  | 
**TransactionId** | Pointer to **string** |  | [optional] 
**ConversationId** | Pointer to **string** |  | [optional] 
**ContactId** | Pointer to **string** |  | [optional] 
**ComChannelId** | Pointer to **string** |  | [optional] 
**GatewayId** | Pointer to **string** |  | [optional] 
**Timestamp** | Pointer to **time.Time** | Current time will be set if empty | [optional] 
**Body** | Pointer to **map[string]interface{}** | Key value object with custom event parameters. | [optional] 

## Methods

### NewNewEvent

`func NewNewEvent(source string, type_ string, channelId string, ) *NewEvent`

NewNewEvent instantiates a new NewEvent object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNewEventWithDefaults

`func NewNewEventWithDefaults() *NewEvent`

NewNewEventWithDefaults instantiates a new NewEvent object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *NewEvent) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *NewEvent) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *NewEvent) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *NewEvent) HasId() bool`

HasId returns a boolean if a field has been set.

### GetSource

`func (o *NewEvent) GetSource() string`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *NewEvent) GetSourceOk() (*string, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *NewEvent) SetSource(v string)`

SetSource sets Source field to given value.


### GetType

`func (o *NewEvent) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *NewEvent) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *NewEvent) SetType(v string)`

SetType sets Type field to given value.


### GetChannelId

`func (o *NewEvent) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *NewEvent) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *NewEvent) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetTransactionId

`func (o *NewEvent) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *NewEvent) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *NewEvent) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.

### HasTransactionId

`func (o *NewEvent) HasTransactionId() bool`

HasTransactionId returns a boolean if a field has been set.

### GetConversationId

`func (o *NewEvent) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *NewEvent) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *NewEvent) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.

### HasConversationId

`func (o *NewEvent) HasConversationId() bool`

HasConversationId returns a boolean if a field has been set.

### GetContactId

`func (o *NewEvent) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *NewEvent) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *NewEvent) SetContactId(v string)`

SetContactId sets ContactId field to given value.

### HasContactId

`func (o *NewEvent) HasContactId() bool`

HasContactId returns a boolean if a field has been set.

### GetComChannelId

`func (o *NewEvent) GetComChannelId() string`

GetComChannelId returns the ComChannelId field if non-nil, zero value otherwise.

### GetComChannelIdOk

`func (o *NewEvent) GetComChannelIdOk() (*string, bool)`

GetComChannelIdOk returns a tuple with the ComChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComChannelId

`func (o *NewEvent) SetComChannelId(v string)`

SetComChannelId sets ComChannelId field to given value.

### HasComChannelId

`func (o *NewEvent) HasComChannelId() bool`

HasComChannelId returns a boolean if a field has been set.

### GetGatewayId

`func (o *NewEvent) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *NewEvent) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *NewEvent) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.

### HasGatewayId

`func (o *NewEvent) HasGatewayId() bool`

HasGatewayId returns a boolean if a field has been set.

### GetTimestamp

`func (o *NewEvent) GetTimestamp() time.Time`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *NewEvent) GetTimestampOk() (*time.Time, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *NewEvent) SetTimestamp(v time.Time)`

SetTimestamp sets Timestamp field to given value.

### HasTimestamp

`func (o *NewEvent) HasTimestamp() bool`

HasTimestamp returns a boolean if a field has been set.

### GetBody

`func (o *NewEvent) GetBody() map[string]interface{}`

GetBody returns the Body field if non-nil, zero value otherwise.

### GetBodyOk

`func (o *NewEvent) GetBodyOk() (*map[string]interface{}, bool)`

GetBodyOk returns a tuple with the Body field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBody

`func (o *NewEvent) SetBody(v map[string]interface{})`

SetBody sets Body field to given value.

### HasBody

`func (o *NewEvent) HasBody() bool`

HasBody returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


