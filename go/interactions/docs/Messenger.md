# Messenger

## Enum


* `MESSENGER_WHATS_APP` (value: `"WhatsApp"`)

* `MESSENGER_TELEGRAM_BOT` (value: `"TelegramBot"`)

* `MESSENGER_WEBCHAT` (value: `"Webchat"`)

* `MESSENGER_VIBER_CHATBOTS` (value: `"ViberChatbots"`)

* `MESSENGER_FACEBOOK_MESSENGER` (value: `"FacebookMessenger"`)

* `MESSENGER_INSTAGRAM_MESSENGER` (value: `"InstagramMessenger"`)

* `MESSENGER_SMS` (value: `"SMS"`)

* `MESSENGER_GOOGLE_BM` (value: `"GoogleBM"`)

* `MESSENGER_IN_APP` (value: `"InApp"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


