# MarkScPermissionAcceptedRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RolloutId** | **string** |  | 
**ChannelId** | **string** |  | 
**ScPermissionId** | **string** |  | 
**ScOrganizationId** | **string** |  | 

## Methods

### NewMarkScPermissionAcceptedRequest

`func NewMarkScPermissionAcceptedRequest(rolloutId string, channelId string, scPermissionId string, scOrganizationId string, ) *MarkScPermissionAcceptedRequest`

NewMarkScPermissionAcceptedRequest instantiates a new MarkScPermissionAcceptedRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMarkScPermissionAcceptedRequestWithDefaults

`func NewMarkScPermissionAcceptedRequestWithDefaults() *MarkScPermissionAcceptedRequest`

NewMarkScPermissionAcceptedRequestWithDefaults instantiates a new MarkScPermissionAcceptedRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRolloutId

`func (o *MarkScPermissionAcceptedRequest) GetRolloutId() string`

GetRolloutId returns the RolloutId field if non-nil, zero value otherwise.

### GetRolloutIdOk

`func (o *MarkScPermissionAcceptedRequest) GetRolloutIdOk() (*string, bool)`

GetRolloutIdOk returns a tuple with the RolloutId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRolloutId

`func (o *MarkScPermissionAcceptedRequest) SetRolloutId(v string)`

SetRolloutId sets RolloutId field to given value.


### GetChannelId

`func (o *MarkScPermissionAcceptedRequest) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *MarkScPermissionAcceptedRequest) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *MarkScPermissionAcceptedRequest) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetScPermissionId

`func (o *MarkScPermissionAcceptedRequest) GetScPermissionId() string`

GetScPermissionId returns the ScPermissionId field if non-nil, zero value otherwise.

### GetScPermissionIdOk

`func (o *MarkScPermissionAcceptedRequest) GetScPermissionIdOk() (*string, bool)`

GetScPermissionIdOk returns a tuple with the ScPermissionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScPermissionId

`func (o *MarkScPermissionAcceptedRequest) SetScPermissionId(v string)`

SetScPermissionId sets ScPermissionId field to given value.


### GetScOrganizationId

`func (o *MarkScPermissionAcceptedRequest) GetScOrganizationId() string`

GetScOrganizationId returns the ScOrganizationId field if non-nil, zero value otherwise.

### GetScOrganizationIdOk

`func (o *MarkScPermissionAcceptedRequest) GetScOrganizationIdOk() (*string, bool)`

GetScOrganizationIdOk returns a tuple with the ScOrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScOrganizationId

`func (o *MarkScPermissionAcceptedRequest) SetScOrganizationId(v string)`

SetScOrganizationId sets ScOrganizationId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


