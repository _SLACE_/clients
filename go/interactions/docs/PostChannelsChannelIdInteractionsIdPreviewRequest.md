# PostChannelsChannelIdInteractionsIdPreviewRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConversationId** | Pointer to **string** |  | [optional] 
**Event** | Pointer to **map[string]interface{}** |  | [optional] 

## Methods

### NewPostChannelsChannelIdInteractionsIdPreviewRequest

`func NewPostChannelsChannelIdInteractionsIdPreviewRequest() *PostChannelsChannelIdInteractionsIdPreviewRequest`

NewPostChannelsChannelIdInteractionsIdPreviewRequest instantiates a new PostChannelsChannelIdInteractionsIdPreviewRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPostChannelsChannelIdInteractionsIdPreviewRequestWithDefaults

`func NewPostChannelsChannelIdInteractionsIdPreviewRequestWithDefaults() *PostChannelsChannelIdInteractionsIdPreviewRequest`

NewPostChannelsChannelIdInteractionsIdPreviewRequestWithDefaults instantiates a new PostChannelsChannelIdInteractionsIdPreviewRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetConversationId

`func (o *PostChannelsChannelIdInteractionsIdPreviewRequest) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *PostChannelsChannelIdInteractionsIdPreviewRequest) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *PostChannelsChannelIdInteractionsIdPreviewRequest) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.

### HasConversationId

`func (o *PostChannelsChannelIdInteractionsIdPreviewRequest) HasConversationId() bool`

HasConversationId returns a boolean if a field has been set.

### GetEvent

`func (o *PostChannelsChannelIdInteractionsIdPreviewRequest) GetEvent() map[string]interface{}`

GetEvent returns the Event field if non-nil, zero value otherwise.

### GetEventOk

`func (o *PostChannelsChannelIdInteractionsIdPreviewRequest) GetEventOk() (*map[string]interface{}, bool)`

GetEventOk returns a tuple with the Event field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvent

`func (o *PostChannelsChannelIdInteractionsIdPreviewRequest) SetEvent(v map[string]interface{})`

SetEvent sets Event field to given value.

### HasEvent

`func (o *PostChannelsChannelIdInteractionsIdPreviewRequest) HasEvent() bool`

HasEvent returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


