# ActionConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Percentage** | **int32** |  | 
**Data** | **map[string]interface{}** | Required data is defined by and dependant on action | 

## Methods

### NewActionConfig

`func NewActionConfig(percentage int32, data map[string]interface{}, ) *ActionConfig`

NewActionConfig instantiates a new ActionConfig object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewActionConfigWithDefaults

`func NewActionConfigWithDefaults() *ActionConfig`

NewActionConfigWithDefaults instantiates a new ActionConfig object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPercentage

`func (o *ActionConfig) GetPercentage() int32`

GetPercentage returns the Percentage field if non-nil, zero value otherwise.

### GetPercentageOk

`func (o *ActionConfig) GetPercentageOk() (*int32, bool)`

GetPercentageOk returns a tuple with the Percentage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPercentage

`func (o *ActionConfig) SetPercentage(v int32)`

SetPercentage sets Percentage field to given value.


### GetData

`func (o *ActionConfig) GetData() map[string]interface{}`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *ActionConfig) GetDataOk() (*map[string]interface{}, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *ActionConfig) SetData(v map[string]interface{})`

SetData sets Data field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


