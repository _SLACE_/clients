# HighlightSubscriberMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**State** | **string** |  | 

## Methods

### NewHighlightSubscriberMutable

`func NewHighlightSubscriberMutable(state string, ) *HighlightSubscriberMutable`

NewHighlightSubscriberMutable instantiates a new HighlightSubscriberMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewHighlightSubscriberMutableWithDefaults

`func NewHighlightSubscriberMutableWithDefaults() *HighlightSubscriberMutable`

NewHighlightSubscriberMutableWithDefaults instantiates a new HighlightSubscriberMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetState

`func (o *HighlightSubscriberMutable) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *HighlightSubscriberMutable) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *HighlightSubscriberMutable) SetState(v string)`

SetState sets State field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


