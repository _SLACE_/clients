# CxTemplateInstallation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**TemplateId** | Pointer to **string** |  | [optional] 
**Template** | Pointer to [**CxTemplate**](CxTemplate.md) |  | [optional] 
**OrganizationId** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**Status** | Pointer to **string** |  | [optional] 
**PrepareDate** | Pointer to [**CxTemplateInstallationPrepareDate**](CxTemplateInstallationPrepareDate.md) |  | [optional] 
**CustomizationsData** | Pointer to [**[]CxTemplateInstallationCustomizationsDataInner**](CxTemplateInstallationCustomizationsDataInner.md) |  | [optional] 
**WizardData** | Pointer to **map[string]interface{}** |  | [optional] 
**Errors** | Pointer to **[]string** |  | [optional] 
**CreatedAt** | Pointer to **time.Time** |  | [optional] 
**UpdatedAt** | Pointer to **time.Time** |  | [optional] 
**TriggersCustomData** | Pointer to [**[]CxTemplateInstallationTriggersCustomDataInner**](CxTemplateInstallationTriggersCustomDataInner.md) |  | [optional] 
**InteractionsCustomData** | Pointer to [**[]CxTemplateInstallationInteractionsCustomDataInner**](CxTemplateInstallationInteractionsCustomDataInner.md) |  | [optional] 

## Methods

### NewCxTemplateInstallation

`func NewCxTemplateInstallation() *CxTemplateInstallation`

NewCxTemplateInstallation instantiates a new CxTemplateInstallation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateInstallationWithDefaults

`func NewCxTemplateInstallationWithDefaults() *CxTemplateInstallation`

NewCxTemplateInstallationWithDefaults instantiates a new CxTemplateInstallation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CxTemplateInstallation) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CxTemplateInstallation) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CxTemplateInstallation) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *CxTemplateInstallation) HasId() bool`

HasId returns a boolean if a field has been set.

### GetTemplateId

`func (o *CxTemplateInstallation) GetTemplateId() string`

GetTemplateId returns the TemplateId field if non-nil, zero value otherwise.

### GetTemplateIdOk

`func (o *CxTemplateInstallation) GetTemplateIdOk() (*string, bool)`

GetTemplateIdOk returns a tuple with the TemplateId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplateId

`func (o *CxTemplateInstallation) SetTemplateId(v string)`

SetTemplateId sets TemplateId field to given value.

### HasTemplateId

`func (o *CxTemplateInstallation) HasTemplateId() bool`

HasTemplateId returns a boolean if a field has been set.

### GetTemplate

`func (o *CxTemplateInstallation) GetTemplate() CxTemplate`

GetTemplate returns the Template field if non-nil, zero value otherwise.

### GetTemplateOk

`func (o *CxTemplateInstallation) GetTemplateOk() (*CxTemplate, bool)`

GetTemplateOk returns a tuple with the Template field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplate

`func (o *CxTemplateInstallation) SetTemplate(v CxTemplate)`

SetTemplate sets Template field to given value.

### HasTemplate

`func (o *CxTemplateInstallation) HasTemplate() bool`

HasTemplate returns a boolean if a field has been set.

### GetOrganizationId

`func (o *CxTemplateInstallation) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *CxTemplateInstallation) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *CxTemplateInstallation) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *CxTemplateInstallation) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetChannelId

`func (o *CxTemplateInstallation) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CxTemplateInstallation) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CxTemplateInstallation) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *CxTemplateInstallation) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetStatus

`func (o *CxTemplateInstallation) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *CxTemplateInstallation) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *CxTemplateInstallation) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *CxTemplateInstallation) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetPrepareDate

`func (o *CxTemplateInstallation) GetPrepareDate() CxTemplateInstallationPrepareDate`

GetPrepareDate returns the PrepareDate field if non-nil, zero value otherwise.

### GetPrepareDateOk

`func (o *CxTemplateInstallation) GetPrepareDateOk() (*CxTemplateInstallationPrepareDate, bool)`

GetPrepareDateOk returns a tuple with the PrepareDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrepareDate

`func (o *CxTemplateInstallation) SetPrepareDate(v CxTemplateInstallationPrepareDate)`

SetPrepareDate sets PrepareDate field to given value.

### HasPrepareDate

`func (o *CxTemplateInstallation) HasPrepareDate() bool`

HasPrepareDate returns a boolean if a field has been set.

### GetCustomizationsData

`func (o *CxTemplateInstallation) GetCustomizationsData() []CxTemplateInstallationCustomizationsDataInner`

GetCustomizationsData returns the CustomizationsData field if non-nil, zero value otherwise.

### GetCustomizationsDataOk

`func (o *CxTemplateInstallation) GetCustomizationsDataOk() (*[]CxTemplateInstallationCustomizationsDataInner, bool)`

GetCustomizationsDataOk returns a tuple with the CustomizationsData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomizationsData

`func (o *CxTemplateInstallation) SetCustomizationsData(v []CxTemplateInstallationCustomizationsDataInner)`

SetCustomizationsData sets CustomizationsData field to given value.

### HasCustomizationsData

`func (o *CxTemplateInstallation) HasCustomizationsData() bool`

HasCustomizationsData returns a boolean if a field has been set.

### GetWizardData

`func (o *CxTemplateInstallation) GetWizardData() map[string]interface{}`

GetWizardData returns the WizardData field if non-nil, zero value otherwise.

### GetWizardDataOk

`func (o *CxTemplateInstallation) GetWizardDataOk() (*map[string]interface{}, bool)`

GetWizardDataOk returns a tuple with the WizardData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWizardData

`func (o *CxTemplateInstallation) SetWizardData(v map[string]interface{})`

SetWizardData sets WizardData field to given value.

### HasWizardData

`func (o *CxTemplateInstallation) HasWizardData() bool`

HasWizardData returns a boolean if a field has been set.

### GetErrors

`func (o *CxTemplateInstallation) GetErrors() []string`

GetErrors returns the Errors field if non-nil, zero value otherwise.

### GetErrorsOk

`func (o *CxTemplateInstallation) GetErrorsOk() (*[]string, bool)`

GetErrorsOk returns a tuple with the Errors field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrors

`func (o *CxTemplateInstallation) SetErrors(v []string)`

SetErrors sets Errors field to given value.

### HasErrors

`func (o *CxTemplateInstallation) HasErrors() bool`

HasErrors returns a boolean if a field has been set.

### GetCreatedAt

`func (o *CxTemplateInstallation) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *CxTemplateInstallation) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *CxTemplateInstallation) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.

### HasCreatedAt

`func (o *CxTemplateInstallation) HasCreatedAt() bool`

HasCreatedAt returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *CxTemplateInstallation) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *CxTemplateInstallation) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *CxTemplateInstallation) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *CxTemplateInstallation) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.

### GetTriggersCustomData

`func (o *CxTemplateInstallation) GetTriggersCustomData() []CxTemplateInstallationTriggersCustomDataInner`

GetTriggersCustomData returns the TriggersCustomData field if non-nil, zero value otherwise.

### GetTriggersCustomDataOk

`func (o *CxTemplateInstallation) GetTriggersCustomDataOk() (*[]CxTemplateInstallationTriggersCustomDataInner, bool)`

GetTriggersCustomDataOk returns a tuple with the TriggersCustomData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTriggersCustomData

`func (o *CxTemplateInstallation) SetTriggersCustomData(v []CxTemplateInstallationTriggersCustomDataInner)`

SetTriggersCustomData sets TriggersCustomData field to given value.

### HasTriggersCustomData

`func (o *CxTemplateInstallation) HasTriggersCustomData() bool`

HasTriggersCustomData returns a boolean if a field has been set.

### GetInteractionsCustomData

`func (o *CxTemplateInstallation) GetInteractionsCustomData() []CxTemplateInstallationInteractionsCustomDataInner`

GetInteractionsCustomData returns the InteractionsCustomData field if non-nil, zero value otherwise.

### GetInteractionsCustomDataOk

`func (o *CxTemplateInstallation) GetInteractionsCustomDataOk() (*[]CxTemplateInstallationInteractionsCustomDataInner, bool)`

GetInteractionsCustomDataOk returns a tuple with the InteractionsCustomData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionsCustomData

`func (o *CxTemplateInstallation) SetInteractionsCustomData(v []CxTemplateInstallationInteractionsCustomDataInner)`

SetInteractionsCustomData sets InteractionsCustomData field to given value.

### HasInteractionsCustomData

`func (o *CxTemplateInstallation) HasInteractionsCustomData() bool`

HasInteractionsCustomData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


