# ScPermission

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Name** | **string** |  | 
**OrganizationId** | **string** |  | 
**MainRolloutId** | **string** |  | 
**TermsContent** | **string** |  | 
**TermsContentHash** | **string** |  | 
**Active** | **bool** |  | 
**CreatedAt** | **string** |  | 
**UpdatedAt** | **string** |  | 
**Acceptances** | Pointer to [**[]Acceptance**](Acceptance.md) |  | [optional] 

## Methods

### NewScPermission

`func NewScPermission(id string, name string, organizationId string, mainRolloutId string, termsContent string, termsContentHash string, active bool, createdAt string, updatedAt string, ) *ScPermission`

NewScPermission instantiates a new ScPermission object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScPermissionWithDefaults

`func NewScPermissionWithDefaults() *ScPermission`

NewScPermissionWithDefaults instantiates a new ScPermission object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ScPermission) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ScPermission) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ScPermission) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *ScPermission) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ScPermission) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ScPermission) SetName(v string)`

SetName sets Name field to given value.


### GetOrganizationId

`func (o *ScPermission) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *ScPermission) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *ScPermission) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetMainRolloutId

`func (o *ScPermission) GetMainRolloutId() string`

GetMainRolloutId returns the MainRolloutId field if non-nil, zero value otherwise.

### GetMainRolloutIdOk

`func (o *ScPermission) GetMainRolloutIdOk() (*string, bool)`

GetMainRolloutIdOk returns a tuple with the MainRolloutId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMainRolloutId

`func (o *ScPermission) SetMainRolloutId(v string)`

SetMainRolloutId sets MainRolloutId field to given value.


### GetTermsContent

`func (o *ScPermission) GetTermsContent() string`

GetTermsContent returns the TermsContent field if non-nil, zero value otherwise.

### GetTermsContentOk

`func (o *ScPermission) GetTermsContentOk() (*string, bool)`

GetTermsContentOk returns a tuple with the TermsContent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTermsContent

`func (o *ScPermission) SetTermsContent(v string)`

SetTermsContent sets TermsContent field to given value.


### GetTermsContentHash

`func (o *ScPermission) GetTermsContentHash() string`

GetTermsContentHash returns the TermsContentHash field if non-nil, zero value otherwise.

### GetTermsContentHashOk

`func (o *ScPermission) GetTermsContentHashOk() (*string, bool)`

GetTermsContentHashOk returns a tuple with the TermsContentHash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTermsContentHash

`func (o *ScPermission) SetTermsContentHash(v string)`

SetTermsContentHash sets TermsContentHash field to given value.


### GetActive

`func (o *ScPermission) GetActive() bool`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *ScPermission) GetActiveOk() (*bool, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *ScPermission) SetActive(v bool)`

SetActive sets Active field to given value.


### GetCreatedAt

`func (o *ScPermission) GetCreatedAt() string`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *ScPermission) GetCreatedAtOk() (*string, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *ScPermission) SetCreatedAt(v string)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *ScPermission) GetUpdatedAt() string`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *ScPermission) GetUpdatedAtOk() (*string, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *ScPermission) SetUpdatedAt(v string)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetAcceptances

`func (o *ScPermission) GetAcceptances() []Acceptance`

GetAcceptances returns the Acceptances field if non-nil, zero value otherwise.

### GetAcceptancesOk

`func (o *ScPermission) GetAcceptancesOk() (*[]Acceptance, bool)`

GetAcceptancesOk returns a tuple with the Acceptances field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAcceptances

`func (o *ScPermission) SetAcceptances(v []Acceptance)`

SetAcceptances sets Acceptances field to given value.

### HasAcceptances

`func (o *ScPermission) HasAcceptances() bool`

HasAcceptances returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


