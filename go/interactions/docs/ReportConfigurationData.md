# ReportConfigurationData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Template** | **string** |  | 
**Frequency** | Pointer to **string** |  | [optional] 
**DateRange** | Pointer to **string** |  | [optional] 
**DateRangeFrom** | Pointer to **time.Time** |  | [optional] 
**DateRangeTo** | Pointer to **time.Time** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 
**Filters** | Pointer to **map[string]interface{}** |  | [optional] 
**Grouping** | Pointer to [**[]ReportGroupBy**](ReportGroupBy.md) |  | [optional] 
**Invisible** | Pointer to [**[]ReportGroupBy**](ReportGroupBy.md) |  | [optional] 
**Sort** | Pointer to [**[]ReportConfigurationDataSortInner**](ReportConfigurationDataSortInner.md) |  | [optional] 
**CustomData** | Pointer to **map[string]interface{}** | Data for templates other than interactions | [optional] 

## Methods

### NewReportConfigurationData

`func NewReportConfigurationData(template string, ) *ReportConfigurationData`

NewReportConfigurationData instantiates a new ReportConfigurationData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReportConfigurationDataWithDefaults

`func NewReportConfigurationDataWithDefaults() *ReportConfigurationData`

NewReportConfigurationDataWithDefaults instantiates a new ReportConfigurationData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTemplate

`func (o *ReportConfigurationData) GetTemplate() string`

GetTemplate returns the Template field if non-nil, zero value otherwise.

### GetTemplateOk

`func (o *ReportConfigurationData) GetTemplateOk() (*string, bool)`

GetTemplateOk returns a tuple with the Template field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplate

`func (o *ReportConfigurationData) SetTemplate(v string)`

SetTemplate sets Template field to given value.


### GetFrequency

`func (o *ReportConfigurationData) GetFrequency() string`

GetFrequency returns the Frequency field if non-nil, zero value otherwise.

### GetFrequencyOk

`func (o *ReportConfigurationData) GetFrequencyOk() (*string, bool)`

GetFrequencyOk returns a tuple with the Frequency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFrequency

`func (o *ReportConfigurationData) SetFrequency(v string)`

SetFrequency sets Frequency field to given value.

### HasFrequency

`func (o *ReportConfigurationData) HasFrequency() bool`

HasFrequency returns a boolean if a field has been set.

### GetDateRange

`func (o *ReportConfigurationData) GetDateRange() string`

GetDateRange returns the DateRange field if non-nil, zero value otherwise.

### GetDateRangeOk

`func (o *ReportConfigurationData) GetDateRangeOk() (*string, bool)`

GetDateRangeOk returns a tuple with the DateRange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateRange

`func (o *ReportConfigurationData) SetDateRange(v string)`

SetDateRange sets DateRange field to given value.

### HasDateRange

`func (o *ReportConfigurationData) HasDateRange() bool`

HasDateRange returns a boolean if a field has been set.

### GetDateRangeFrom

`func (o *ReportConfigurationData) GetDateRangeFrom() time.Time`

GetDateRangeFrom returns the DateRangeFrom field if non-nil, zero value otherwise.

### GetDateRangeFromOk

`func (o *ReportConfigurationData) GetDateRangeFromOk() (*time.Time, bool)`

GetDateRangeFromOk returns a tuple with the DateRangeFrom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateRangeFrom

`func (o *ReportConfigurationData) SetDateRangeFrom(v time.Time)`

SetDateRangeFrom sets DateRangeFrom field to given value.

### HasDateRangeFrom

`func (o *ReportConfigurationData) HasDateRangeFrom() bool`

HasDateRangeFrom returns a boolean if a field has been set.

### GetDateRangeTo

`func (o *ReportConfigurationData) GetDateRangeTo() time.Time`

GetDateRangeTo returns the DateRangeTo field if non-nil, zero value otherwise.

### GetDateRangeToOk

`func (o *ReportConfigurationData) GetDateRangeToOk() (*time.Time, bool)`

GetDateRangeToOk returns a tuple with the DateRangeTo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateRangeTo

`func (o *ReportConfigurationData) SetDateRangeTo(v time.Time)`

SetDateRangeTo sets DateRangeTo field to given value.

### HasDateRangeTo

`func (o *ReportConfigurationData) HasDateRangeTo() bool`

HasDateRangeTo returns a boolean if a field has been set.

### GetTags

`func (o *ReportConfigurationData) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *ReportConfigurationData) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *ReportConfigurationData) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *ReportConfigurationData) HasTags() bool`

HasTags returns a boolean if a field has been set.

### GetFilters

`func (o *ReportConfigurationData) GetFilters() map[string]interface{}`

GetFilters returns the Filters field if non-nil, zero value otherwise.

### GetFiltersOk

`func (o *ReportConfigurationData) GetFiltersOk() (*map[string]interface{}, bool)`

GetFiltersOk returns a tuple with the Filters field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFilters

`func (o *ReportConfigurationData) SetFilters(v map[string]interface{})`

SetFilters sets Filters field to given value.

### HasFilters

`func (o *ReportConfigurationData) HasFilters() bool`

HasFilters returns a boolean if a field has been set.

### GetGrouping

`func (o *ReportConfigurationData) GetGrouping() []ReportGroupBy`

GetGrouping returns the Grouping field if non-nil, zero value otherwise.

### GetGroupingOk

`func (o *ReportConfigurationData) GetGroupingOk() (*[]ReportGroupBy, bool)`

GetGroupingOk returns a tuple with the Grouping field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGrouping

`func (o *ReportConfigurationData) SetGrouping(v []ReportGroupBy)`

SetGrouping sets Grouping field to given value.

### HasGrouping

`func (o *ReportConfigurationData) HasGrouping() bool`

HasGrouping returns a boolean if a field has been set.

### GetInvisible

`func (o *ReportConfigurationData) GetInvisible() []ReportGroupBy`

GetInvisible returns the Invisible field if non-nil, zero value otherwise.

### GetInvisibleOk

`func (o *ReportConfigurationData) GetInvisibleOk() (*[]ReportGroupBy, bool)`

GetInvisibleOk returns a tuple with the Invisible field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInvisible

`func (o *ReportConfigurationData) SetInvisible(v []ReportGroupBy)`

SetInvisible sets Invisible field to given value.

### HasInvisible

`func (o *ReportConfigurationData) HasInvisible() bool`

HasInvisible returns a boolean if a field has been set.

### GetSort

`func (o *ReportConfigurationData) GetSort() []ReportConfigurationDataSortInner`

GetSort returns the Sort field if non-nil, zero value otherwise.

### GetSortOk

`func (o *ReportConfigurationData) GetSortOk() (*[]ReportConfigurationDataSortInner, bool)`

GetSortOk returns a tuple with the Sort field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSort

`func (o *ReportConfigurationData) SetSort(v []ReportConfigurationDataSortInner)`

SetSort sets Sort field to given value.

### HasSort

`func (o *ReportConfigurationData) HasSort() bool`

HasSort returns a boolean if a field has been set.

### GetCustomData

`func (o *ReportConfigurationData) GetCustomData() map[string]interface{}`

GetCustomData returns the CustomData field if non-nil, zero value otherwise.

### GetCustomDataOk

`func (o *ReportConfigurationData) GetCustomDataOk() (*map[string]interface{}, bool)`

GetCustomDataOk returns a tuple with the CustomData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomData

`func (o *ReportConfigurationData) SetCustomData(v map[string]interface{})`

SetCustomData sets CustomData field to given value.

### HasCustomData

`func (o *ReportConfigurationData) HasCustomData() bool`

HasCustomData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


