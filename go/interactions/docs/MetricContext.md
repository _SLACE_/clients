# MetricContext

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FieldName** | **string** |  | 
**ObjectType** | Pointer to **string** |  | [optional] 
**Value** | [**ExpressionValue**](ExpressionValue.md) |  | 

## Methods

### NewMetricContext

`func NewMetricContext(fieldName string, value ExpressionValue, ) *MetricContext`

NewMetricContext instantiates a new MetricContext object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMetricContextWithDefaults

`func NewMetricContextWithDefaults() *MetricContext`

NewMetricContextWithDefaults instantiates a new MetricContext object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFieldName

`func (o *MetricContext) GetFieldName() string`

GetFieldName returns the FieldName field if non-nil, zero value otherwise.

### GetFieldNameOk

`func (o *MetricContext) GetFieldNameOk() (*string, bool)`

GetFieldNameOk returns a tuple with the FieldName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFieldName

`func (o *MetricContext) SetFieldName(v string)`

SetFieldName sets FieldName field to given value.


### GetObjectType

`func (o *MetricContext) GetObjectType() string`

GetObjectType returns the ObjectType field if non-nil, zero value otherwise.

### GetObjectTypeOk

`func (o *MetricContext) GetObjectTypeOk() (*string, bool)`

GetObjectTypeOk returns a tuple with the ObjectType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetObjectType

`func (o *MetricContext) SetObjectType(v string)`

SetObjectType sets ObjectType field to given value.

### HasObjectType

`func (o *MetricContext) HasObjectType() bool`

HasObjectType returns a boolean if a field has been set.

### GetValue

`func (o *MetricContext) GetValue() ExpressionValue`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *MetricContext) GetValueOk() (*ExpressionValue, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *MetricContext) SetValue(v ExpressionValue)`

SetValue sets Value field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


