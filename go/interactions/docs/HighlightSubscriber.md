# HighlightSubscriber

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**HighlightId** | **string** |  | 
**State** | **string** |  | 
**Name** | **string** |  | 
**PhotoUrl** | Pointer to **string** |  | [optional] 
**Messenger** | [**Messenger**](Messenger.md) |  | 
**GatewayId** | Pointer to **string** |  | [optional] 
**ConversationId** | Pointer to **string** |  | [optional] 
**UserId** | Pointer to **string** |  | [optional] 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 

## Methods

### NewHighlightSubscriber

`func NewHighlightSubscriber(id string, highlightId string, state string, name string, messenger Messenger, createdAt time.Time, updatedAt time.Time, ) *HighlightSubscriber`

NewHighlightSubscriber instantiates a new HighlightSubscriber object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewHighlightSubscriberWithDefaults

`func NewHighlightSubscriberWithDefaults() *HighlightSubscriber`

NewHighlightSubscriberWithDefaults instantiates a new HighlightSubscriber object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *HighlightSubscriber) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *HighlightSubscriber) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *HighlightSubscriber) SetId(v string)`

SetId sets Id field to given value.


### GetHighlightId

`func (o *HighlightSubscriber) GetHighlightId() string`

GetHighlightId returns the HighlightId field if non-nil, zero value otherwise.

### GetHighlightIdOk

`func (o *HighlightSubscriber) GetHighlightIdOk() (*string, bool)`

GetHighlightIdOk returns a tuple with the HighlightId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHighlightId

`func (o *HighlightSubscriber) SetHighlightId(v string)`

SetHighlightId sets HighlightId field to given value.


### GetState

`func (o *HighlightSubscriber) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *HighlightSubscriber) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *HighlightSubscriber) SetState(v string)`

SetState sets State field to given value.


### GetName

`func (o *HighlightSubscriber) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *HighlightSubscriber) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *HighlightSubscriber) SetName(v string)`

SetName sets Name field to given value.


### GetPhotoUrl

`func (o *HighlightSubscriber) GetPhotoUrl() string`

GetPhotoUrl returns the PhotoUrl field if non-nil, zero value otherwise.

### GetPhotoUrlOk

`func (o *HighlightSubscriber) GetPhotoUrlOk() (*string, bool)`

GetPhotoUrlOk returns a tuple with the PhotoUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhotoUrl

`func (o *HighlightSubscriber) SetPhotoUrl(v string)`

SetPhotoUrl sets PhotoUrl field to given value.

### HasPhotoUrl

`func (o *HighlightSubscriber) HasPhotoUrl() bool`

HasPhotoUrl returns a boolean if a field has been set.

### GetMessenger

`func (o *HighlightSubscriber) GetMessenger() Messenger`

GetMessenger returns the Messenger field if non-nil, zero value otherwise.

### GetMessengerOk

`func (o *HighlightSubscriber) GetMessengerOk() (*Messenger, bool)`

GetMessengerOk returns a tuple with the Messenger field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessenger

`func (o *HighlightSubscriber) SetMessenger(v Messenger)`

SetMessenger sets Messenger field to given value.


### GetGatewayId

`func (o *HighlightSubscriber) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *HighlightSubscriber) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *HighlightSubscriber) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.

### HasGatewayId

`func (o *HighlightSubscriber) HasGatewayId() bool`

HasGatewayId returns a boolean if a field has been set.

### GetConversationId

`func (o *HighlightSubscriber) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *HighlightSubscriber) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *HighlightSubscriber) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.

### HasConversationId

`func (o *HighlightSubscriber) HasConversationId() bool`

HasConversationId returns a boolean if a field has been set.

### GetUserId

`func (o *HighlightSubscriber) GetUserId() string`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *HighlightSubscriber) GetUserIdOk() (*string, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *HighlightSubscriber) SetUserId(v string)`

SetUserId sets UserId field to given value.

### HasUserId

`func (o *HighlightSubscriber) HasUserId() bool`

HasUserId returns a boolean if a field has been set.

### GetCreatedAt

`func (o *HighlightSubscriber) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *HighlightSubscriber) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *HighlightSubscriber) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *HighlightSubscriber) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *HighlightSubscriber) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *HighlightSubscriber) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


