# CloneTarget

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OrganizationId** | **string** | Organization where interaction will be cloned | 
**ChannelId** | **string** | Channel where interaction will be cloned | 

## Methods

### NewCloneTarget

`func NewCloneTarget(organizationId string, channelId string, ) *CloneTarget`

NewCloneTarget instantiates a new CloneTarget object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCloneTargetWithDefaults

`func NewCloneTargetWithDefaults() *CloneTarget`

NewCloneTargetWithDefaults instantiates a new CloneTarget object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOrganizationId

`func (o *CloneTarget) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *CloneTarget) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *CloneTarget) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetChannelId

`func (o *CloneTarget) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CloneTarget) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CloneTarget) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


