# CxTemplateCreateDataIntegrations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VynamicCoupons** | Pointer to **bool** |  | [optional] 
**VynamicCrm** | Pointer to **bool** |  | [optional] 
**AnybillReceipt** | Pointer to **bool** |  | [optional] 
**PaypalPayments** | Pointer to **bool** |  | [optional] 
**UnzerPayments** | Pointer to **bool** |  | [optional] 
**InsignDocuments** | Pointer to **bool** |  | [optional] 
**PolyprintNfcCard** | Pointer to **bool** |  | [optional] 

## Methods

### NewCxTemplateCreateDataIntegrations

`func NewCxTemplateCreateDataIntegrations() *CxTemplateCreateDataIntegrations`

NewCxTemplateCreateDataIntegrations instantiates a new CxTemplateCreateDataIntegrations object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateCreateDataIntegrationsWithDefaults

`func NewCxTemplateCreateDataIntegrationsWithDefaults() *CxTemplateCreateDataIntegrations`

NewCxTemplateCreateDataIntegrationsWithDefaults instantiates a new CxTemplateCreateDataIntegrations object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVynamicCoupons

`func (o *CxTemplateCreateDataIntegrations) GetVynamicCoupons() bool`

GetVynamicCoupons returns the VynamicCoupons field if non-nil, zero value otherwise.

### GetVynamicCouponsOk

`func (o *CxTemplateCreateDataIntegrations) GetVynamicCouponsOk() (*bool, bool)`

GetVynamicCouponsOk returns a tuple with the VynamicCoupons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVynamicCoupons

`func (o *CxTemplateCreateDataIntegrations) SetVynamicCoupons(v bool)`

SetVynamicCoupons sets VynamicCoupons field to given value.

### HasVynamicCoupons

`func (o *CxTemplateCreateDataIntegrations) HasVynamicCoupons() bool`

HasVynamicCoupons returns a boolean if a field has been set.

### GetVynamicCrm

`func (o *CxTemplateCreateDataIntegrations) GetVynamicCrm() bool`

GetVynamicCrm returns the VynamicCrm field if non-nil, zero value otherwise.

### GetVynamicCrmOk

`func (o *CxTemplateCreateDataIntegrations) GetVynamicCrmOk() (*bool, bool)`

GetVynamicCrmOk returns a tuple with the VynamicCrm field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVynamicCrm

`func (o *CxTemplateCreateDataIntegrations) SetVynamicCrm(v bool)`

SetVynamicCrm sets VynamicCrm field to given value.

### HasVynamicCrm

`func (o *CxTemplateCreateDataIntegrations) HasVynamicCrm() bool`

HasVynamicCrm returns a boolean if a field has been set.

### GetAnybillReceipt

`func (o *CxTemplateCreateDataIntegrations) GetAnybillReceipt() bool`

GetAnybillReceipt returns the AnybillReceipt field if non-nil, zero value otherwise.

### GetAnybillReceiptOk

`func (o *CxTemplateCreateDataIntegrations) GetAnybillReceiptOk() (*bool, bool)`

GetAnybillReceiptOk returns a tuple with the AnybillReceipt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAnybillReceipt

`func (o *CxTemplateCreateDataIntegrations) SetAnybillReceipt(v bool)`

SetAnybillReceipt sets AnybillReceipt field to given value.

### HasAnybillReceipt

`func (o *CxTemplateCreateDataIntegrations) HasAnybillReceipt() bool`

HasAnybillReceipt returns a boolean if a field has been set.

### GetPaypalPayments

`func (o *CxTemplateCreateDataIntegrations) GetPaypalPayments() bool`

GetPaypalPayments returns the PaypalPayments field if non-nil, zero value otherwise.

### GetPaypalPaymentsOk

`func (o *CxTemplateCreateDataIntegrations) GetPaypalPaymentsOk() (*bool, bool)`

GetPaypalPaymentsOk returns a tuple with the PaypalPayments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPaypalPayments

`func (o *CxTemplateCreateDataIntegrations) SetPaypalPayments(v bool)`

SetPaypalPayments sets PaypalPayments field to given value.

### HasPaypalPayments

`func (o *CxTemplateCreateDataIntegrations) HasPaypalPayments() bool`

HasPaypalPayments returns a boolean if a field has been set.

### GetUnzerPayments

`func (o *CxTemplateCreateDataIntegrations) GetUnzerPayments() bool`

GetUnzerPayments returns the UnzerPayments field if non-nil, zero value otherwise.

### GetUnzerPaymentsOk

`func (o *CxTemplateCreateDataIntegrations) GetUnzerPaymentsOk() (*bool, bool)`

GetUnzerPaymentsOk returns a tuple with the UnzerPayments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnzerPayments

`func (o *CxTemplateCreateDataIntegrations) SetUnzerPayments(v bool)`

SetUnzerPayments sets UnzerPayments field to given value.

### HasUnzerPayments

`func (o *CxTemplateCreateDataIntegrations) HasUnzerPayments() bool`

HasUnzerPayments returns a boolean if a field has been set.

### GetInsignDocuments

`func (o *CxTemplateCreateDataIntegrations) GetInsignDocuments() bool`

GetInsignDocuments returns the InsignDocuments field if non-nil, zero value otherwise.

### GetInsignDocumentsOk

`func (o *CxTemplateCreateDataIntegrations) GetInsignDocumentsOk() (*bool, bool)`

GetInsignDocumentsOk returns a tuple with the InsignDocuments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInsignDocuments

`func (o *CxTemplateCreateDataIntegrations) SetInsignDocuments(v bool)`

SetInsignDocuments sets InsignDocuments field to given value.

### HasInsignDocuments

`func (o *CxTemplateCreateDataIntegrations) HasInsignDocuments() bool`

HasInsignDocuments returns a boolean if a field has been set.

### GetPolyprintNfcCard

`func (o *CxTemplateCreateDataIntegrations) GetPolyprintNfcCard() bool`

GetPolyprintNfcCard returns the PolyprintNfcCard field if non-nil, zero value otherwise.

### GetPolyprintNfcCardOk

`func (o *CxTemplateCreateDataIntegrations) GetPolyprintNfcCardOk() (*bool, bool)`

GetPolyprintNfcCardOk returns a tuple with the PolyprintNfcCard field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolyprintNfcCard

`func (o *CxTemplateCreateDataIntegrations) SetPolyprintNfcCard(v bool)`

SetPolyprintNfcCard sets PolyprintNfcCard field to given value.

### HasPolyprintNfcCard

`func (o *CxTemplateCreateDataIntegrations) HasPolyprintNfcCard() bool`

HasPolyprintNfcCard returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


