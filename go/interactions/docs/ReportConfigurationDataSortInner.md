# ReportConfigurationDataSortInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**By** | Pointer to [**ReportGroupBy**](ReportGroupBy.md) |  | [optional] 
**Dir** | Pointer to **string** |  | [optional] 

## Methods

### NewReportConfigurationDataSortInner

`func NewReportConfigurationDataSortInner() *ReportConfigurationDataSortInner`

NewReportConfigurationDataSortInner instantiates a new ReportConfigurationDataSortInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReportConfigurationDataSortInnerWithDefaults

`func NewReportConfigurationDataSortInnerWithDefaults() *ReportConfigurationDataSortInner`

NewReportConfigurationDataSortInnerWithDefaults instantiates a new ReportConfigurationDataSortInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetBy

`func (o *ReportConfigurationDataSortInner) GetBy() ReportGroupBy`

GetBy returns the By field if non-nil, zero value otherwise.

### GetByOk

`func (o *ReportConfigurationDataSortInner) GetByOk() (*ReportGroupBy, bool)`

GetByOk returns a tuple with the By field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBy

`func (o *ReportConfigurationDataSortInner) SetBy(v ReportGroupBy)`

SetBy sets By field to given value.

### HasBy

`func (o *ReportConfigurationDataSortInner) HasBy() bool`

HasBy returns a boolean if a field has been set.

### GetDir

`func (o *ReportConfigurationDataSortInner) GetDir() string`

GetDir returns the Dir field if non-nil, zero value otherwise.

### GetDirOk

`func (o *ReportConfigurationDataSortInner) GetDirOk() (*string, bool)`

GetDirOk returns a tuple with the Dir field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDir

`func (o *ReportConfigurationDataSortInner) SetDir(v string)`

SetDir sets Dir field to given value.

### HasDir

`func (o *ReportConfigurationDataSortInner) HasDir() bool`

HasDir returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


