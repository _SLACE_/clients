# RolloutTarget

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OrganizationId** | **string** | Organization where rollout will be cloned | 
**MainChannelId** | **string** | Main channel where rollout will be done | 
**ChannelIds** | Pointer to **[]string** | Channels where rollout will be cloned (linked) | [optional] 

## Methods

### NewRolloutTarget

`func NewRolloutTarget(organizationId string, mainChannelId string, ) *RolloutTarget`

NewRolloutTarget instantiates a new RolloutTarget object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRolloutTargetWithDefaults

`func NewRolloutTargetWithDefaults() *RolloutTarget`

NewRolloutTargetWithDefaults instantiates a new RolloutTarget object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOrganizationId

`func (o *RolloutTarget) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *RolloutTarget) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *RolloutTarget) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetMainChannelId

`func (o *RolloutTarget) GetMainChannelId() string`

GetMainChannelId returns the MainChannelId field if non-nil, zero value otherwise.

### GetMainChannelIdOk

`func (o *RolloutTarget) GetMainChannelIdOk() (*string, bool)`

GetMainChannelIdOk returns a tuple with the MainChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMainChannelId

`func (o *RolloutTarget) SetMainChannelId(v string)`

SetMainChannelId sets MainChannelId field to given value.


### GetChannelIds

`func (o *RolloutTarget) GetChannelIds() []string`

GetChannelIds returns the ChannelIds field if non-nil, zero value otherwise.

### GetChannelIdsOk

`func (o *RolloutTarget) GetChannelIdsOk() (*[]string, bool)`

GetChannelIdsOk returns a tuple with the ChannelIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelIds

`func (o *RolloutTarget) SetChannelIds(v []string)`

SetChannelIds sets ChannelIds field to given value.

### HasChannelIds

`func (o *RolloutTarget) HasChannelIds() bool`

HasChannelIds returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


