# GetOrganizationsOrganizationIdCxTemplates200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Resuts** | Pointer to [**[]CxTemplate**](CxTemplate.md) |  | [optional] 
**Total** | Pointer to **int32** |  | [optional] 

## Methods

### NewGetOrganizationsOrganizationIdCxTemplates200Response

`func NewGetOrganizationsOrganizationIdCxTemplates200Response() *GetOrganizationsOrganizationIdCxTemplates200Response`

NewGetOrganizationsOrganizationIdCxTemplates200Response instantiates a new GetOrganizationsOrganizationIdCxTemplates200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetOrganizationsOrganizationIdCxTemplates200ResponseWithDefaults

`func NewGetOrganizationsOrganizationIdCxTemplates200ResponseWithDefaults() *GetOrganizationsOrganizationIdCxTemplates200Response`

NewGetOrganizationsOrganizationIdCxTemplates200ResponseWithDefaults instantiates a new GetOrganizationsOrganizationIdCxTemplates200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResuts

`func (o *GetOrganizationsOrganizationIdCxTemplates200Response) GetResuts() []CxTemplate`

GetResuts returns the Resuts field if non-nil, zero value otherwise.

### GetResutsOk

`func (o *GetOrganizationsOrganizationIdCxTemplates200Response) GetResutsOk() (*[]CxTemplate, bool)`

GetResutsOk returns a tuple with the Resuts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResuts

`func (o *GetOrganizationsOrganizationIdCxTemplates200Response) SetResuts(v []CxTemplate)`

SetResuts sets Resuts field to given value.

### HasResuts

`func (o *GetOrganizationsOrganizationIdCxTemplates200Response) HasResuts() bool`

HasResuts returns a boolean if a field has been set.

### GetTotal

`func (o *GetOrganizationsOrganizationIdCxTemplates200Response) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *GetOrganizationsOrganizationIdCxTemplates200Response) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *GetOrganizationsOrganizationIdCxTemplates200Response) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *GetOrganizationsOrganizationIdCxTemplates200Response) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


