# MetricType

## Enum


* `MAIN` (value: `"main"`)

* `INVESTMENT` (value: `"investment"`)

* `CHAT_STARTED` (value: `"chat_started"`)

* `CUSTOM` (value: `"custom"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


