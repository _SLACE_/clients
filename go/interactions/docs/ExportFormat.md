# ExportFormat

## Enum


* `EXPORT_FORMAT_CSV` (value: `"csv"`)

* `EXPORT_FORMAT_XLSX` (value: `"xlsx"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


