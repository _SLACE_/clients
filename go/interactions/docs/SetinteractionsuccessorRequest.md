# SetinteractionsuccessorRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InteractionId** | Pointer to **string** | Interaction id of successor  | [optional] 
**EntryPointHandover** | Pointer to **bool** |  | [optional] 
**Minor** | Pointer to **bool** | Indicates if in versioning suscessor will be set as minor version | [optional] [default to false]

## Methods

### NewSetinteractionsuccessorRequest

`func NewSetinteractionsuccessorRequest() *SetinteractionsuccessorRequest`

NewSetinteractionsuccessorRequest instantiates a new SetinteractionsuccessorRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSetinteractionsuccessorRequestWithDefaults

`func NewSetinteractionsuccessorRequestWithDefaults() *SetinteractionsuccessorRequest`

NewSetinteractionsuccessorRequestWithDefaults instantiates a new SetinteractionsuccessorRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInteractionId

`func (o *SetinteractionsuccessorRequest) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *SetinteractionsuccessorRequest) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *SetinteractionsuccessorRequest) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *SetinteractionsuccessorRequest) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetEntryPointHandover

`func (o *SetinteractionsuccessorRequest) GetEntryPointHandover() bool`

GetEntryPointHandover returns the EntryPointHandover field if non-nil, zero value otherwise.

### GetEntryPointHandoverOk

`func (o *SetinteractionsuccessorRequest) GetEntryPointHandoverOk() (*bool, bool)`

GetEntryPointHandoverOk returns a tuple with the EntryPointHandover field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEntryPointHandover

`func (o *SetinteractionsuccessorRequest) SetEntryPointHandover(v bool)`

SetEntryPointHandover sets EntryPointHandover field to given value.

### HasEntryPointHandover

`func (o *SetinteractionsuccessorRequest) HasEntryPointHandover() bool`

HasEntryPointHandover returns a boolean if a field has been set.

### GetMinor

`func (o *SetinteractionsuccessorRequest) GetMinor() bool`

GetMinor returns the Minor field if non-nil, zero value otherwise.

### GetMinorOk

`func (o *SetinteractionsuccessorRequest) GetMinorOk() (*bool, bool)`

GetMinorOk returns a tuple with the Minor field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMinor

`func (o *SetinteractionsuccessorRequest) SetMinor(v bool)`

SetMinor sets Minor field to given value.

### HasMinor

`func (o *SetinteractionsuccessorRequest) HasMinor() bool`

HasMinor returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


