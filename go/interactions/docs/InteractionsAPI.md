# \InteractionsAPI

All URIs are relative to *https://interactions.slace.dev/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CloneInteraction**](InteractionsAPI.md#CloneInteraction) | **Post** /channels/{channel_id}/interactions/{id}/clone | Clone interaction
[**CreateGlobalRollout**](InteractionsAPI.md#CreateGlobalRollout) | **Post** /channels/{channel_id}/interactions/{interaction_id}/rollout/{rollout_id}/global | Create global rollout
[**CreateInteraction**](InteractionsAPI.md#CreateInteraction) | **Post** /channels/{channel_id}/interactions | Create interaction
[**CreateRollout**](InteractionsAPI.md#CreateRollout) | **Post** /channels/{channel_id}/interactions/{id}/rollout | Create Rollout
[**CreateScPermissions**](InteractionsAPI.md#CreateScPermissions) | **Post** /organizations/{org_id}/sc-permissions | Create SC Permissions
[**DeleteInteraction**](InteractionsAPI.md#DeleteInteraction) | **Delete** /channels/{channel_id}/interactions/{id} | Delete interaction
[**DeleteRolloutStatus**](InteractionsAPI.md#DeleteRolloutStatus) | **Delete** /channels/{channel_id}/rollouts/{rollout_id}/statuses/{status_id} | Delete rollout status
[**DownloadScPermissionAccepted**](InteractionsAPI.md#DownloadScPermissionAccepted) | **Get** /organizations/{org_id}/sc-permissions/acceptances/{acceptance_id}/download | Download accepted T&amp;C
[**GetAcceptances**](InteractionsAPI.md#GetAcceptances) | **Get** /organizations/{org_id}/sc-permissions/acceptances | Get SC Acceptances
[**GetChannelSettings**](InteractionsAPI.md#GetChannelSettings) | **Get** /channels/{channel_id}/settings | Get channel settings
[**GetChannelsChannelIdInteractionsInteractionIdRolloutConnections**](InteractionsAPI.md#GetChannelsChannelIdInteractionsInteractionIdRolloutConnections) | **Get** /channels/{channel_id}/interactions/{interaction_id}/rollout/connections | Get rollout connections
[**GetEmbeddings**](InteractionsAPI.md#GetEmbeddings) | **Get** /channels/{channel_id}/interactions/{id}/embedded-by | Get embeddings
[**GetInteraction**](InteractionsAPI.md#GetInteraction) | **Get** /channels/{channel_id}/interactions/{id} | Get interaction
[**GetInteractionRollout**](InteractionsAPI.md#GetInteractionRollout) | **Get** /channels/{channel_id}/interactions/{id}/rollout | Get Rollout
[**GetRolloutVersions**](InteractionsAPI.md#GetRolloutVersions) | **Get** /channels/{channel_id}/interactions/{interaction_id}/rollout/{rollout_id}/versions | Get Rollout Version
[**GetScPermissions**](InteractionsAPI.md#GetScPermissions) | **Get** /organizations/{org_id}/sc-permissions | Get SC Permissions
[**GetUniqueTags**](InteractionsAPI.md#GetUniqueTags) | **Get** /organizations/{org_id}/interactions/tags | Get unique tags
[**ListInteractions**](InteractionsAPI.md#ListInteractions) | **Get** /channels/{channel_id}/interactions | List interactions
[**MarkScPermissionAccepted**](InteractionsAPI.md#MarkScPermissionAccepted) | **Post** /organizations/{org_id}/sc-permissions/acceptances/accepted | Mark SC permission accepted
[**PatchInteraction**](InteractionsAPI.md#PatchInteraction) | **Patch** /channels/{channel_id}/interactions/{id} | Patch interaction
[**PostChannelsChannelIdInteractionsIdPreview**](InteractionsAPI.md#PostChannelsChannelIdInteractionsIdPreview) | **Post** /channels/{channel_id}/interactions/{id}/preview | Preview interaction
[**PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchdule**](InteractionsAPI.md#PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchdule) | **Post** /channels/{channel_id}/interactions/{interaction_id}/rollout/{rollout_id}/schedule | Schedule rollout
[**PostEvent**](InteractionsAPI.md#PostEvent) | **Post** /event | Send interaction event
[**Setinteractionsuccessor**](InteractionsAPI.md#Setinteractionsuccessor) | **Put** /channels/{channel_id}/interactions/{id}/successor | Set interaction successor
[**UpdateInteraction**](InteractionsAPI.md#UpdateInteraction) | **Put** /channels/{channel_id}/interactions/{id} | Update interaction
[**UpdateState**](InteractionsAPI.md#UpdateState) | **Put** /channels/{channel_id}/interactions/{id}/state | Update state
[**ValidateInteraction**](InteractionsAPI.md#ValidateInteraction) | **Get** /channels/{channel_id}/interactions/{id}/validate | Validate interaction



## CloneInteraction

> []CloneResponse CloneInteraction(ctx, channelId, id).CloneInteractionRequest(cloneInteractionRequest).Execute()

Clone interaction



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 
    cloneInteractionRequest := *openapiclient.NewCloneInteractionRequest() // CloneInteractionRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.CloneInteraction(context.Background(), channelId, id).CloneInteractionRequest(cloneInteractionRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.CloneInteraction``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CloneInteraction`: []CloneResponse
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.CloneInteraction`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCloneInteractionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **cloneInteractionRequest** | [**CloneInteractionRequest**](CloneInteractionRequest.md) |  | 

### Return type

[**[]CloneResponse**](CloneResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateGlobalRollout

> []RolloutResponse CreateGlobalRollout(ctx, channelId, interactionId, rolloutId).CreateGlobalRolloutRequest(createGlobalRolloutRequest).Execute()

Create global rollout

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    interactionId := "interactionId_example" // string | 
    rolloutId := "rolloutId_example" // string | 
    createGlobalRolloutRequest := *openapiclient.NewCreateGlobalRolloutRequest() // CreateGlobalRolloutRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.CreateGlobalRollout(context.Background(), channelId, interactionId, rolloutId).CreateGlobalRolloutRequest(createGlobalRolloutRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.CreateGlobalRollout``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateGlobalRollout`: []RolloutResponse
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.CreateGlobalRollout`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**interactionId** | **string** |  | 
**rolloutId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateGlobalRolloutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **createGlobalRolloutRequest** | [**CreateGlobalRolloutRequest**](CreateGlobalRolloutRequest.md) |  | 

### Return type

[**[]RolloutResponse**](RolloutResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateInteraction

> Interaction CreateInteraction(ctx, channelId).InteractionMutable(interactionMutable).Execute()

Create interaction



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    interactionMutable := *openapiclient.NewInteractionMutable("Name_example", int32(123), *openapiclient.NewTrigger(*openapiclient.NewCondition(openapiclient.Operator("and"))), []openapiclient.Action{*openapiclient.NewAction("Id_example", openapiclient.ActionType("wait"), []openapiclient.ActionConfig{*openapiclient.NewActionConfig(int32(123), map[string]interface{}(123))}, false, false)}, false, false) // InteractionMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.CreateInteraction(context.Background(), channelId).InteractionMutable(interactionMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.CreateInteraction``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateInteraction`: Interaction
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.CreateInteraction`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateInteractionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **interactionMutable** | [**InteractionMutable**](InteractionMutable.md) |  | 

### Return type

[**Interaction**](Interaction.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateRollout

> RolloutResponse CreateRollout(ctx, channelId, id).CreateRolloutRequest(createRolloutRequest).Execute()

Create Rollout

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 
    createRolloutRequest := *openapiclient.NewCreateRolloutRequest() // CreateRolloutRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.CreateRollout(context.Background(), channelId, id).CreateRolloutRequest(createRolloutRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.CreateRollout``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateRollout`: RolloutResponse
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.CreateRollout`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateRolloutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **createRolloutRequest** | [**CreateRolloutRequest**](CreateRolloutRequest.md) |  | 

### Return type

[**RolloutResponse**](RolloutResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateScPermissions

> ScPermission CreateScPermissions(ctx, orgId).ScPermissionCreateData(scPermissionCreateData).Execute()

Create SC Permissions

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    scPermissionCreateData := *openapiclient.NewScPermissionCreateData("Name_example", "MainRolloutId_example", "TermsContent_example", false) // ScPermissionCreateData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.CreateScPermissions(context.Background(), orgId).ScPermissionCreateData(scPermissionCreateData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.CreateScPermissions``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateScPermissions`: ScPermission
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.CreateScPermissions`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateScPermissionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **scPermissionCreateData** | [**ScPermissionCreateData**](ScPermissionCreateData.md) |  | 

### Return type

[**ScPermission**](ScPermission.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteInteraction

> GenericResponse DeleteInteraction(ctx, id, channelId).Execute()

Delete interaction



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    id := "id_example" // string | 
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.DeleteInteraction(context.Background(), id, channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.DeleteInteraction``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteInteraction`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.DeleteInteraction`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteInteractionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteRolloutStatus

> DeleteRolloutStatus(ctx, channelId, rolloutId, statusId).Execute()

Delete rollout status



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    rolloutId := "rolloutId_example" // string | 
    statusId := "statusId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.InteractionsAPI.DeleteRolloutStatus(context.Background(), channelId, rolloutId, statusId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.DeleteRolloutStatus``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**rolloutId** | **string** |  | 
**statusId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteRolloutStatusRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DownloadScPermissionAccepted

> map[string]interface{} DownloadScPermissionAccepted(ctx, orgId, acceptanceId).Execute()

Download accepted T&C

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    acceptanceId := "acceptanceId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.DownloadScPermissionAccepted(context.Background(), orgId, acceptanceId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.DownloadScPermissionAccepted``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DownloadScPermissionAccepted`: map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.DownloadScPermissionAccepted`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**acceptanceId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDownloadScPermissionAcceptedRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

**map[string]interface{}**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/pdf

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAcceptances

> GetAcceptances200Response GetAcceptances(ctx, orgId).RolloutId(rolloutId).Execute()

Get SC Acceptances

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    rolloutId := "rolloutId_example" // string | Get terms for rollout (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.GetAcceptances(context.Background(), orgId).RolloutId(rolloutId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.GetAcceptances``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAcceptances`: GetAcceptances200Response
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.GetAcceptances`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetAcceptancesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **rolloutId** | **string** | Get terms for rollout | 

### Return type

[**GetAcceptances200Response**](GetAcceptances200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChannelSettings

> Settings GetChannelSettings(ctx, channelId).Execute()

Get channel settings



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.GetChannelSettings(context.Background(), channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.GetChannelSettings``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChannelSettings`: Settings
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.GetChannelSettings`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChannelSettingsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Settings**](Settings.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChannelsChannelIdInteractionsInteractionIdRolloutConnections

> []RolloutResponse GetChannelsChannelIdInteractionsInteractionIdRolloutConnections(ctx, channelId, interactionId).Execute()

Get rollout connections

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    interactionId := "interactionId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.GetChannelsChannelIdInteractionsInteractionIdRolloutConnections(context.Background(), channelId, interactionId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.GetChannelsChannelIdInteractionsInteractionIdRolloutConnections``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChannelsChannelIdInteractionsInteractionIdRolloutConnections`: []RolloutResponse
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.GetChannelsChannelIdInteractionsInteractionIdRolloutConnections`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**interactionId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChannelsChannelIdInteractionsInteractionIdRolloutConnectionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**[]RolloutResponse**](RolloutResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetEmbeddings

> InteractionsList GetEmbeddings(ctx, channelId, id).WithConnections(withConnections).Execute()

Get embeddings



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 
    withConnections := true // bool | Flag if we should also return information about all interactions in SLACE Connect graph (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.GetEmbeddings(context.Background(), channelId, id).WithConnections(withConnections).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.GetEmbeddings``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetEmbeddings`: InteractionsList
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.GetEmbeddings`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetEmbeddingsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **withConnections** | **bool** | Flag if we should also return information about all interactions in SLACE Connect graph | 

### Return type

[**InteractionsList**](InteractionsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetInteraction

> Interaction GetInteraction(ctx, id, channelId).Execute()

Get interaction



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    id := "id_example" // string | 
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.GetInteraction(context.Background(), id, channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.GetInteraction``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetInteraction`: Interaction
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.GetInteraction`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetInteractionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Interaction**](Interaction.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetInteractionRollout

> RolloutResponse GetInteractionRollout(ctx, channelId, id).Execute()

Get Rollout

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.GetInteractionRollout(context.Background(), channelId, id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.GetInteractionRollout``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetInteractionRollout`: RolloutResponse
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.GetInteractionRollout`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetInteractionRolloutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**RolloutResponse**](RolloutResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetRolloutVersions

> []RolloutStatus GetRolloutVersions(ctx, channelId, interactionId, rolloutId).Execute()

Get Rollout Version



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    interactionId := "interactionId_example" // string | 
    rolloutId := "rolloutId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.GetRolloutVersions(context.Background(), channelId, interactionId, rolloutId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.GetRolloutVersions``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetRolloutVersions`: []RolloutStatus
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.GetRolloutVersions`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**interactionId** | **string** |  | 
**rolloutId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetRolloutVersionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**[]RolloutStatus**](RolloutStatus.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetScPermissions

> GetScPermissions200Response GetScPermissions(ctx, orgId).MainRolloutId(mainRolloutId).Execute()

Get SC Permissions

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    mainRolloutId := "mainRolloutId_example" // string | Get terms for rollout (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.GetScPermissions(context.Background(), orgId).MainRolloutId(mainRolloutId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.GetScPermissions``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetScPermissions`: GetScPermissions200Response
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.GetScPermissions`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetScPermissionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **mainRolloutId** | **string** | Get terms for rollout | 

### Return type

[**GetScPermissions200Response**](GetScPermissions200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetUniqueTags

> []string GetUniqueTags(ctx, orgId).ChannelIds(channelIds).Execute()

Get unique tags



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    channelIds := []string{"Inner_example"} // []string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.GetUniqueTags(context.Background(), orgId).ChannelIds(channelIds).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.GetUniqueTags``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetUniqueTags`: []string
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.GetUniqueTags`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetUniqueTagsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **channelIds** | **[]string** |  | 

### Return type

**[]string**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListInteractions

> InteractionsList ListInteractions(ctx, channelId).Name(name).Restart(restart).Ids(ids).States(states).Tags(tags).Languages(languages).Messengers(messengers).Offset(offset).Limit(limit).Sort(sort).TriggerTemplate(triggerTemplate).CxId(cxId).LiveFrom(liveFrom).LiveTo(liveTo).Execute()

List interactions



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    name := "name_example" // string |  (optional)
    restart := true // bool |  (optional)
    ids := []string{"Inner_example"} // []string |  (optional)
    states := []string{"Inner_example"} // []string |  (optional)
    tags := []string{"Inner_example"} // []string |  (optional)
    languages := []string{"Inner_example"} // []string |  (optional)
    messengers := []string{"Inner_example"} // []string |  (optional)
    offset := "offset_example" // string |  (optional)
    limit := "limit_example" // string |  (optional)
    sort := "sort_example" // string |  (optional)
    triggerTemplate := "triggerTemplate_example" // string |  (optional)
    cxId := "cxId_example" // string |  (optional)
    liveFrom := time.Now() // time.Time | Find all with `live from` greater than/equal (optional)
    liveTo := time.Now() // time.Time | Find all with `live to` less than/equal (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.ListInteractions(context.Background(), channelId).Name(name).Restart(restart).Ids(ids).States(states).Tags(tags).Languages(languages).Messengers(messengers).Offset(offset).Limit(limit).Sort(sort).TriggerTemplate(triggerTemplate).CxId(cxId).LiveFrom(liveFrom).LiveTo(liveTo).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.ListInteractions``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListInteractions`: InteractionsList
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.ListInteractions`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListInteractionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **name** | **string** |  | 
 **restart** | **bool** |  | 
 **ids** | **[]string** |  | 
 **states** | **[]string** |  | 
 **tags** | **[]string** |  | 
 **languages** | **[]string** |  | 
 **messengers** | **[]string** |  | 
 **offset** | **string** |  | 
 **limit** | **string** |  | 
 **sort** | **string** |  | 
 **triggerTemplate** | **string** |  | 
 **cxId** | **string** |  | 
 **liveFrom** | **time.Time** | Find all with &#x60;live from&#x60; greater than/equal | 
 **liveTo** | **time.Time** | Find all with &#x60;live to&#x60; less than/equal | 

### Return type

[**InteractionsList**](InteractionsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## MarkScPermissionAccepted

> MarkScPermissionAccepted(ctx, orgId).MarkScPermissionAcceptedRequest(markScPermissionAcceptedRequest).Execute()

Mark SC permission accepted

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    markScPermissionAcceptedRequest := *openapiclient.NewMarkScPermissionAcceptedRequest("RolloutId_example", "ChannelId_example", "ScPermissionId_example", "ScOrganizationId_example") // MarkScPermissionAcceptedRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.InteractionsAPI.MarkScPermissionAccepted(context.Background(), orgId).MarkScPermissionAcceptedRequest(markScPermissionAcceptedRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.MarkScPermissionAccepted``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiMarkScPermissionAcceptedRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **markScPermissionAcceptedRequest** | [**MarkScPermissionAcceptedRequest**](MarkScPermissionAcceptedRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PatchInteraction

> Interaction PatchInteraction(ctx, id, channelId).InteractionPatchable(interactionPatchable).Execute()

Patch interaction



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    id := "id_example" // string | 
    channelId := "channelId_example" // string | 
    interactionPatchable := *openapiclient.NewInteractionPatchable() // InteractionPatchable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.PatchInteraction(context.Background(), id, channelId).InteractionPatchable(interactionPatchable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.PatchInteraction``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PatchInteraction`: Interaction
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.PatchInteraction`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPatchInteractionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **interactionPatchable** | [**InteractionPatchable**](InteractionPatchable.md) |  | 

### Return type

[**Interaction**](Interaction.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostChannelsChannelIdInteractionsIdPreview

> PostChannelsChannelIdInteractionsIdPreview(ctx, channelId, id).PostChannelsChannelIdInteractionsIdPreviewRequest(postChannelsChannelIdInteractionsIdPreviewRequest).Execute()

Preview interaction

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 
    postChannelsChannelIdInteractionsIdPreviewRequest := *openapiclient.NewPostChannelsChannelIdInteractionsIdPreviewRequest() // PostChannelsChannelIdInteractionsIdPreviewRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.InteractionsAPI.PostChannelsChannelIdInteractionsIdPreview(context.Background(), channelId, id).PostChannelsChannelIdInteractionsIdPreviewRequest(postChannelsChannelIdInteractionsIdPreviewRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.PostChannelsChannelIdInteractionsIdPreview``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPostChannelsChannelIdInteractionsIdPreviewRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **postChannelsChannelIdInteractionsIdPreviewRequest** | [**PostChannelsChannelIdInteractionsIdPreviewRequest**](PostChannelsChannelIdInteractionsIdPreviewRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchdule

> PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchdule(ctx, channelId, interactionId, rolloutId).PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest(postChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest).Execute()

Schedule rollout

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    interactionId := "interactionId_example" // string | 
    rolloutId := "rolloutId_example" // string | 
    postChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest := *openapiclient.NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest() // PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.InteractionsAPI.PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchdule(context.Background(), channelId, interactionId, rolloutId).PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest(postChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchdule``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**interactionId** | **string** |  | 
**rolloutId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **postChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest** | [**PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest**](PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostEvent

> GenericResponse PostEvent(ctx).NewEvent(newEvent).Execute()

Send interaction event



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    newEvent := *openapiclient.NewNewEvent("Source_example", "Type_example", "ChannelId_example") // NewEvent |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.PostEvent(context.Background()).NewEvent(newEvent).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.PostEvent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PostEvent`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.PostEvent`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiPostEventRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **newEvent** | [**NewEvent**](NewEvent.md) |  | 

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## Setinteractionsuccessor

> Interaction Setinteractionsuccessor(ctx, channelId, id).SetinteractionsuccessorRequest(setinteractionsuccessorRequest).Execute()

Set interaction successor



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 
    setinteractionsuccessorRequest := *openapiclient.NewSetinteractionsuccessorRequest() // SetinteractionsuccessorRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.Setinteractionsuccessor(context.Background(), channelId, id).SetinteractionsuccessorRequest(setinteractionsuccessorRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.Setinteractionsuccessor``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `Setinteractionsuccessor`: Interaction
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.Setinteractionsuccessor`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiSetinteractionsuccessorRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **setinteractionsuccessorRequest** | [**SetinteractionsuccessorRequest**](SetinteractionsuccessorRequest.md) |  | 

### Return type

[**Interaction**](Interaction.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateInteraction

> Interaction UpdateInteraction(ctx, id, channelId).InteractionMutable(interactionMutable).Execute()

Update interaction



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    id := "id_example" // string | 
    channelId := "channelId_example" // string | 
    interactionMutable := *openapiclient.NewInteractionMutable("Name_example", int32(123), *openapiclient.NewTrigger(*openapiclient.NewCondition(openapiclient.Operator("and"))), []openapiclient.Action{*openapiclient.NewAction("Id_example", openapiclient.ActionType("wait"), []openapiclient.ActionConfig{*openapiclient.NewActionConfig(int32(123), map[string]interface{}(123))}, false, false)}, false, false) // InteractionMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.UpdateInteraction(context.Background(), id, channelId).InteractionMutable(interactionMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.UpdateInteraction``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateInteraction`: Interaction
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.UpdateInteraction`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateInteractionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **interactionMutable** | [**InteractionMutable**](InteractionMutable.md) |  | 

### Return type

[**Interaction**](Interaction.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateState

> Interaction UpdateState(ctx, channelId, id).StateUpdateRequest(stateUpdateRequest).Execute()

Update state

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 
    stateUpdateRequest := *openapiclient.NewStateUpdateRequest(openapiclient.State("draft")) // StateUpdateRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.UpdateState(context.Background(), channelId, id).StateUpdateRequest(stateUpdateRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.UpdateState``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateState`: Interaction
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.UpdateState`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateStateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **stateUpdateRequest** | [**StateUpdateRequest**](StateUpdateRequest.md) |  | 

### Return type

[**Interaction**](Interaction.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ValidateInteraction

> []ValidationError ValidateInteraction(ctx, channelId, id).Execute()

Validate interaction

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InteractionsAPI.ValidateInteraction(context.Background(), channelId, id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InteractionsAPI.ValidateInteraction``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ValidateInteraction`: []ValidationError
    fmt.Fprintf(os.Stdout, "Response from `InteractionsAPI.ValidateInteraction`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiValidateInteractionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**[]ValidationError**](ValidationError.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

