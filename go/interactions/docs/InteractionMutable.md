# InteractionMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**Priority** | **int32** |  | 
**ActionExecutionsLimit** | Pointer to **int32** |  | [optional] 
**Trigger** | [**Trigger**](Trigger.md) |  | 
**Actions** | [**[]Action**](Action.md) |  | 
**Restart** | **bool** |  | 
**RestartCoolDown** | Pointer to **string** |  | [optional] 
**LiveFrom** | Pointer to **time.Time** |  | [optional] 
**LiveTo** | Pointer to **time.Time** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 
**Languages** | Pointer to **[]string** |  | [optional] 
**Messengers** | Pointer to **[]string** |  | [optional] 
**TimeZone** | Pointer to **string** |  | [optional] 
**DefaultLanguage** | Pointer to **string** |  | [optional] 
**CustomerExperienceId** | Pointer to **string** |  | [optional] 
**ForceNewTransaction** | **bool** |  | 

## Methods

### NewInteractionMutable

`func NewInteractionMutable(name string, priority int32, trigger Trigger, actions []Action, restart bool, forceNewTransaction bool, ) *InteractionMutable`

NewInteractionMutable instantiates a new InteractionMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInteractionMutableWithDefaults

`func NewInteractionMutableWithDefaults() *InteractionMutable`

NewInteractionMutableWithDefaults instantiates a new InteractionMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *InteractionMutable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *InteractionMutable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *InteractionMutable) SetName(v string)`

SetName sets Name field to given value.


### GetPriority

`func (o *InteractionMutable) GetPriority() int32`

GetPriority returns the Priority field if non-nil, zero value otherwise.

### GetPriorityOk

`func (o *InteractionMutable) GetPriorityOk() (*int32, bool)`

GetPriorityOk returns a tuple with the Priority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriority

`func (o *InteractionMutable) SetPriority(v int32)`

SetPriority sets Priority field to given value.


### GetActionExecutionsLimit

`func (o *InteractionMutable) GetActionExecutionsLimit() int32`

GetActionExecutionsLimit returns the ActionExecutionsLimit field if non-nil, zero value otherwise.

### GetActionExecutionsLimitOk

`func (o *InteractionMutable) GetActionExecutionsLimitOk() (*int32, bool)`

GetActionExecutionsLimitOk returns a tuple with the ActionExecutionsLimit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActionExecutionsLimit

`func (o *InteractionMutable) SetActionExecutionsLimit(v int32)`

SetActionExecutionsLimit sets ActionExecutionsLimit field to given value.

### HasActionExecutionsLimit

`func (o *InteractionMutable) HasActionExecutionsLimit() bool`

HasActionExecutionsLimit returns a boolean if a field has been set.

### GetTrigger

`func (o *InteractionMutable) GetTrigger() Trigger`

GetTrigger returns the Trigger field if non-nil, zero value otherwise.

### GetTriggerOk

`func (o *InteractionMutable) GetTriggerOk() (*Trigger, bool)`

GetTriggerOk returns a tuple with the Trigger field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTrigger

`func (o *InteractionMutable) SetTrigger(v Trigger)`

SetTrigger sets Trigger field to given value.


### GetActions

`func (o *InteractionMutable) GetActions() []Action`

GetActions returns the Actions field if non-nil, zero value otherwise.

### GetActionsOk

`func (o *InteractionMutable) GetActionsOk() (*[]Action, bool)`

GetActionsOk returns a tuple with the Actions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActions

`func (o *InteractionMutable) SetActions(v []Action)`

SetActions sets Actions field to given value.


### GetRestart

`func (o *InteractionMutable) GetRestart() bool`

GetRestart returns the Restart field if non-nil, zero value otherwise.

### GetRestartOk

`func (o *InteractionMutable) GetRestartOk() (*bool, bool)`

GetRestartOk returns a tuple with the Restart field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRestart

`func (o *InteractionMutable) SetRestart(v bool)`

SetRestart sets Restart field to given value.


### GetRestartCoolDown

`func (o *InteractionMutable) GetRestartCoolDown() string`

GetRestartCoolDown returns the RestartCoolDown field if non-nil, zero value otherwise.

### GetRestartCoolDownOk

`func (o *InteractionMutable) GetRestartCoolDownOk() (*string, bool)`

GetRestartCoolDownOk returns a tuple with the RestartCoolDown field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRestartCoolDown

`func (o *InteractionMutable) SetRestartCoolDown(v string)`

SetRestartCoolDown sets RestartCoolDown field to given value.

### HasRestartCoolDown

`func (o *InteractionMutable) HasRestartCoolDown() bool`

HasRestartCoolDown returns a boolean if a field has been set.

### GetLiveFrom

`func (o *InteractionMutable) GetLiveFrom() time.Time`

GetLiveFrom returns the LiveFrom field if non-nil, zero value otherwise.

### GetLiveFromOk

`func (o *InteractionMutable) GetLiveFromOk() (*time.Time, bool)`

GetLiveFromOk returns a tuple with the LiveFrom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLiveFrom

`func (o *InteractionMutable) SetLiveFrom(v time.Time)`

SetLiveFrom sets LiveFrom field to given value.

### HasLiveFrom

`func (o *InteractionMutable) HasLiveFrom() bool`

HasLiveFrom returns a boolean if a field has been set.

### GetLiveTo

`func (o *InteractionMutable) GetLiveTo() time.Time`

GetLiveTo returns the LiveTo field if non-nil, zero value otherwise.

### GetLiveToOk

`func (o *InteractionMutable) GetLiveToOk() (*time.Time, bool)`

GetLiveToOk returns a tuple with the LiveTo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLiveTo

`func (o *InteractionMutable) SetLiveTo(v time.Time)`

SetLiveTo sets LiveTo field to given value.

### HasLiveTo

`func (o *InteractionMutable) HasLiveTo() bool`

HasLiveTo returns a boolean if a field has been set.

### GetTags

`func (o *InteractionMutable) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *InteractionMutable) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *InteractionMutable) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *InteractionMutable) HasTags() bool`

HasTags returns a boolean if a field has been set.

### GetLanguages

`func (o *InteractionMutable) GetLanguages() []string`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *InteractionMutable) GetLanguagesOk() (*[]string, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *InteractionMutable) SetLanguages(v []string)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *InteractionMutable) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetMessengers

`func (o *InteractionMutable) GetMessengers() []string`

GetMessengers returns the Messengers field if non-nil, zero value otherwise.

### GetMessengersOk

`func (o *InteractionMutable) GetMessengersOk() (*[]string, bool)`

GetMessengersOk returns a tuple with the Messengers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengers

`func (o *InteractionMutable) SetMessengers(v []string)`

SetMessengers sets Messengers field to given value.

### HasMessengers

`func (o *InteractionMutable) HasMessengers() bool`

HasMessengers returns a boolean if a field has been set.

### GetTimeZone

`func (o *InteractionMutable) GetTimeZone() string`

GetTimeZone returns the TimeZone field if non-nil, zero value otherwise.

### GetTimeZoneOk

`func (o *InteractionMutable) GetTimeZoneOk() (*string, bool)`

GetTimeZoneOk returns a tuple with the TimeZone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimeZone

`func (o *InteractionMutable) SetTimeZone(v string)`

SetTimeZone sets TimeZone field to given value.

### HasTimeZone

`func (o *InteractionMutable) HasTimeZone() bool`

HasTimeZone returns a boolean if a field has been set.

### GetDefaultLanguage

`func (o *InteractionMutable) GetDefaultLanguage() string`

GetDefaultLanguage returns the DefaultLanguage field if non-nil, zero value otherwise.

### GetDefaultLanguageOk

`func (o *InteractionMutable) GetDefaultLanguageOk() (*string, bool)`

GetDefaultLanguageOk returns a tuple with the DefaultLanguage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultLanguage

`func (o *InteractionMutable) SetDefaultLanguage(v string)`

SetDefaultLanguage sets DefaultLanguage field to given value.

### HasDefaultLanguage

`func (o *InteractionMutable) HasDefaultLanguage() bool`

HasDefaultLanguage returns a boolean if a field has been set.

### GetCustomerExperienceId

`func (o *InteractionMutable) GetCustomerExperienceId() string`

GetCustomerExperienceId returns the CustomerExperienceId field if non-nil, zero value otherwise.

### GetCustomerExperienceIdOk

`func (o *InteractionMutable) GetCustomerExperienceIdOk() (*string, bool)`

GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerExperienceId

`func (o *InteractionMutable) SetCustomerExperienceId(v string)`

SetCustomerExperienceId sets CustomerExperienceId field to given value.

### HasCustomerExperienceId

`func (o *InteractionMutable) HasCustomerExperienceId() bool`

HasCustomerExperienceId returns a boolean if a field has been set.

### GetForceNewTransaction

`func (o *InteractionMutable) GetForceNewTransaction() bool`

GetForceNewTransaction returns the ForceNewTransaction field if non-nil, zero value otherwise.

### GetForceNewTransactionOk

`func (o *InteractionMutable) GetForceNewTransactionOk() (*bool, bool)`

GetForceNewTransactionOk returns a tuple with the ForceNewTransaction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetForceNewTransaction

`func (o *InteractionMutable) SetForceNewTransaction(v bool)`

SetForceNewTransaction sets ForceNewTransaction field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


