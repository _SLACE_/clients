# RolloutStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**RolloutId** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**OrganizationId** | Pointer to **string** |  | [optional] 
**InteractionId** | Pointer to **string** |  | [optional] 
**Interaction** | Pointer to **map[string]interface{}** |  | [optional] 
**Version** | Pointer to **int32** |  | [optional] 
**VersionMinor** | Pointer to **int32** |  | [optional] 
**State** | Pointer to **string** |  | [optional] 
**Errors** | Pointer to [**RolloutError**](RolloutError.md) |  | [optional] 
**Main** | Pointer to **bool** |  | [optional] 
**CreatedAt** | Pointer to **string** |  | [optional] 
**UpdatedAt** | Pointer to **string** |  | [optional] 

## Methods

### NewRolloutStatus

`func NewRolloutStatus() *RolloutStatus`

NewRolloutStatus instantiates a new RolloutStatus object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRolloutStatusWithDefaults

`func NewRolloutStatusWithDefaults() *RolloutStatus`

NewRolloutStatusWithDefaults instantiates a new RolloutStatus object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *RolloutStatus) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *RolloutStatus) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *RolloutStatus) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *RolloutStatus) HasId() bool`

HasId returns a boolean if a field has been set.

### GetRolloutId

`func (o *RolloutStatus) GetRolloutId() string`

GetRolloutId returns the RolloutId field if non-nil, zero value otherwise.

### GetRolloutIdOk

`func (o *RolloutStatus) GetRolloutIdOk() (*string, bool)`

GetRolloutIdOk returns a tuple with the RolloutId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRolloutId

`func (o *RolloutStatus) SetRolloutId(v string)`

SetRolloutId sets RolloutId field to given value.

### HasRolloutId

`func (o *RolloutStatus) HasRolloutId() bool`

HasRolloutId returns a boolean if a field has been set.

### GetChannelId

`func (o *RolloutStatus) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *RolloutStatus) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *RolloutStatus) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *RolloutStatus) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetOrganizationId

`func (o *RolloutStatus) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *RolloutStatus) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *RolloutStatus) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *RolloutStatus) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetInteractionId

`func (o *RolloutStatus) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *RolloutStatus) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *RolloutStatus) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *RolloutStatus) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetInteraction

`func (o *RolloutStatus) GetInteraction() map[string]interface{}`

GetInteraction returns the Interaction field if non-nil, zero value otherwise.

### GetInteractionOk

`func (o *RolloutStatus) GetInteractionOk() (*map[string]interface{}, bool)`

GetInteractionOk returns a tuple with the Interaction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteraction

`func (o *RolloutStatus) SetInteraction(v map[string]interface{})`

SetInteraction sets Interaction field to given value.

### HasInteraction

`func (o *RolloutStatus) HasInteraction() bool`

HasInteraction returns a boolean if a field has been set.

### GetVersion

`func (o *RolloutStatus) GetVersion() int32`

GetVersion returns the Version field if non-nil, zero value otherwise.

### GetVersionOk

`func (o *RolloutStatus) GetVersionOk() (*int32, bool)`

GetVersionOk returns a tuple with the Version field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersion

`func (o *RolloutStatus) SetVersion(v int32)`

SetVersion sets Version field to given value.

### HasVersion

`func (o *RolloutStatus) HasVersion() bool`

HasVersion returns a boolean if a field has been set.

### GetVersionMinor

`func (o *RolloutStatus) GetVersionMinor() int32`

GetVersionMinor returns the VersionMinor field if non-nil, zero value otherwise.

### GetVersionMinorOk

`func (o *RolloutStatus) GetVersionMinorOk() (*int32, bool)`

GetVersionMinorOk returns a tuple with the VersionMinor field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersionMinor

`func (o *RolloutStatus) SetVersionMinor(v int32)`

SetVersionMinor sets VersionMinor field to given value.

### HasVersionMinor

`func (o *RolloutStatus) HasVersionMinor() bool`

HasVersionMinor returns a boolean if a field has been set.

### GetState

`func (o *RolloutStatus) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *RolloutStatus) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *RolloutStatus) SetState(v string)`

SetState sets State field to given value.

### HasState

`func (o *RolloutStatus) HasState() bool`

HasState returns a boolean if a field has been set.

### GetErrors

`func (o *RolloutStatus) GetErrors() RolloutError`

GetErrors returns the Errors field if non-nil, zero value otherwise.

### GetErrorsOk

`func (o *RolloutStatus) GetErrorsOk() (*RolloutError, bool)`

GetErrorsOk returns a tuple with the Errors field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrors

`func (o *RolloutStatus) SetErrors(v RolloutError)`

SetErrors sets Errors field to given value.

### HasErrors

`func (o *RolloutStatus) HasErrors() bool`

HasErrors returns a boolean if a field has been set.

### GetMain

`func (o *RolloutStatus) GetMain() bool`

GetMain returns the Main field if non-nil, zero value otherwise.

### GetMainOk

`func (o *RolloutStatus) GetMainOk() (*bool, bool)`

GetMainOk returns a tuple with the Main field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMain

`func (o *RolloutStatus) SetMain(v bool)`

SetMain sets Main field to given value.

### HasMain

`func (o *RolloutStatus) HasMain() bool`

HasMain returns a boolean if a field has been set.

### GetCreatedAt

`func (o *RolloutStatus) GetCreatedAt() string`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *RolloutStatus) GetCreatedAtOk() (*string, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *RolloutStatus) SetCreatedAt(v string)`

SetCreatedAt sets CreatedAt field to given value.

### HasCreatedAt

`func (o *RolloutStatus) HasCreatedAt() bool`

HasCreatedAt returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *RolloutStatus) GetUpdatedAt() string`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *RolloutStatus) GetUpdatedAtOk() (*string, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *RolloutStatus) SetUpdatedAt(v string)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *RolloutStatus) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


