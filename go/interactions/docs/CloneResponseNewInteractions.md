# CloneResponseNewInteractions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Interaction id | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**OrganisationId** | Pointer to **string** |  | [optional] 

## Methods

### NewCloneResponseNewInteractions

`func NewCloneResponseNewInteractions() *CloneResponseNewInteractions`

NewCloneResponseNewInteractions instantiates a new CloneResponseNewInteractions object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCloneResponseNewInteractionsWithDefaults

`func NewCloneResponseNewInteractionsWithDefaults() *CloneResponseNewInteractions`

NewCloneResponseNewInteractionsWithDefaults instantiates a new CloneResponseNewInteractions object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CloneResponseNewInteractions) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CloneResponseNewInteractions) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CloneResponseNewInteractions) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *CloneResponseNewInteractions) HasId() bool`

HasId returns a boolean if a field has been set.

### GetChannelId

`func (o *CloneResponseNewInteractions) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CloneResponseNewInteractions) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CloneResponseNewInteractions) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *CloneResponseNewInteractions) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetOrganisationId

`func (o *CloneResponseNewInteractions) GetOrganisationId() string`

GetOrganisationId returns the OrganisationId field if non-nil, zero value otherwise.

### GetOrganisationIdOk

`func (o *CloneResponseNewInteractions) GetOrganisationIdOk() (*string, bool)`

GetOrganisationIdOk returns a tuple with the OrganisationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganisationId

`func (o *CloneResponseNewInteractions) SetOrganisationId(v string)`

SetOrganisationId sets OrganisationId field to given value.

### HasOrganisationId

`func (o *CloneResponseNewInteractions) HasOrganisationId() bool`

HasOrganisationId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


