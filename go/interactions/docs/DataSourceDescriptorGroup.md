# DataSourceDescriptorGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**ParentId** | Pointer to **string** |  | [optional] 
**Parent** | Pointer to [**DataSourceDescriptorGroup**](DataSourceDescriptorGroup.md) |  | [optional] 
**Key** | Pointer to **string** | This is only set when group got created by system, in this case this kind of group can only be changed, but not removed | [optional] 
**Name** | **string** |  | 
**Description** | Pointer to **string** |  | [optional] 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 

## Methods

### NewDataSourceDescriptorGroup

`func NewDataSourceDescriptorGroup(id string, name string, createdAt time.Time, updatedAt time.Time, ) *DataSourceDescriptorGroup`

NewDataSourceDescriptorGroup instantiates a new DataSourceDescriptorGroup object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDataSourceDescriptorGroupWithDefaults

`func NewDataSourceDescriptorGroupWithDefaults() *DataSourceDescriptorGroup`

NewDataSourceDescriptorGroupWithDefaults instantiates a new DataSourceDescriptorGroup object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *DataSourceDescriptorGroup) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *DataSourceDescriptorGroup) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *DataSourceDescriptorGroup) SetId(v string)`

SetId sets Id field to given value.


### GetParentId

`func (o *DataSourceDescriptorGroup) GetParentId() string`

GetParentId returns the ParentId field if non-nil, zero value otherwise.

### GetParentIdOk

`func (o *DataSourceDescriptorGroup) GetParentIdOk() (*string, bool)`

GetParentIdOk returns a tuple with the ParentId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParentId

`func (o *DataSourceDescriptorGroup) SetParentId(v string)`

SetParentId sets ParentId field to given value.

### HasParentId

`func (o *DataSourceDescriptorGroup) HasParentId() bool`

HasParentId returns a boolean if a field has been set.

### GetParent

`func (o *DataSourceDescriptorGroup) GetParent() DataSourceDescriptorGroup`

GetParent returns the Parent field if non-nil, zero value otherwise.

### GetParentOk

`func (o *DataSourceDescriptorGroup) GetParentOk() (*DataSourceDescriptorGroup, bool)`

GetParentOk returns a tuple with the Parent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParent

`func (o *DataSourceDescriptorGroup) SetParent(v DataSourceDescriptorGroup)`

SetParent sets Parent field to given value.

### HasParent

`func (o *DataSourceDescriptorGroup) HasParent() bool`

HasParent returns a boolean if a field has been set.

### GetKey

`func (o *DataSourceDescriptorGroup) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *DataSourceDescriptorGroup) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *DataSourceDescriptorGroup) SetKey(v string)`

SetKey sets Key field to given value.

### HasKey

`func (o *DataSourceDescriptorGroup) HasKey() bool`

HasKey returns a boolean if a field has been set.

### GetName

`func (o *DataSourceDescriptorGroup) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *DataSourceDescriptorGroup) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *DataSourceDescriptorGroup) SetName(v string)`

SetName sets Name field to given value.


### GetDescription

`func (o *DataSourceDescriptorGroup) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *DataSourceDescriptorGroup) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *DataSourceDescriptorGroup) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *DataSourceDescriptorGroup) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetCreatedAt

`func (o *DataSourceDescriptorGroup) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *DataSourceDescriptorGroup) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *DataSourceDescriptorGroup) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *DataSourceDescriptorGroup) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *DataSourceDescriptorGroup) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *DataSourceDescriptorGroup) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


