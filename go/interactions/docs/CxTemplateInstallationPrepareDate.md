# CxTemplateInstallationPrepareDate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Languages** | Pointer to **[]string** |  | [optional] 
**FallbackLanguage** | Pointer to **string** |  | [optional] 
**Messengers** | Pointer to **[]string** |  | [optional] 

## Methods

### NewCxTemplateInstallationPrepareDate

`func NewCxTemplateInstallationPrepareDate() *CxTemplateInstallationPrepareDate`

NewCxTemplateInstallationPrepareDate instantiates a new CxTemplateInstallationPrepareDate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateInstallationPrepareDateWithDefaults

`func NewCxTemplateInstallationPrepareDateWithDefaults() *CxTemplateInstallationPrepareDate`

NewCxTemplateInstallationPrepareDateWithDefaults instantiates a new CxTemplateInstallationPrepareDate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLanguages

`func (o *CxTemplateInstallationPrepareDate) GetLanguages() []string`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *CxTemplateInstallationPrepareDate) GetLanguagesOk() (*[]string, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *CxTemplateInstallationPrepareDate) SetLanguages(v []string)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *CxTemplateInstallationPrepareDate) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetFallbackLanguage

`func (o *CxTemplateInstallationPrepareDate) GetFallbackLanguage() string`

GetFallbackLanguage returns the FallbackLanguage field if non-nil, zero value otherwise.

### GetFallbackLanguageOk

`func (o *CxTemplateInstallationPrepareDate) GetFallbackLanguageOk() (*string, bool)`

GetFallbackLanguageOk returns a tuple with the FallbackLanguage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFallbackLanguage

`func (o *CxTemplateInstallationPrepareDate) SetFallbackLanguage(v string)`

SetFallbackLanguage sets FallbackLanguage field to given value.

### HasFallbackLanguage

`func (o *CxTemplateInstallationPrepareDate) HasFallbackLanguage() bool`

HasFallbackLanguage returns a boolean if a field has been set.

### GetMessengers

`func (o *CxTemplateInstallationPrepareDate) GetMessengers() []string`

GetMessengers returns the Messengers field if non-nil, zero value otherwise.

### GetMessengersOk

`func (o *CxTemplateInstallationPrepareDate) GetMessengersOk() (*[]string, bool)`

GetMessengersOk returns a tuple with the Messengers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengers

`func (o *CxTemplateInstallationPrepareDate) SetMessengers(v []string)`

SetMessengers sets Messengers field to given value.

### HasMessengers

`func (o *CxTemplateInstallationPrepareDate) HasMessengers() bool`

HasMessengers returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


