# SlaceConnect

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RolloutId** | **string** |  | 
**InteractionName** | **string** |  | 
**CxId** | Pointer to **string** |  | [optional] 

## Methods

### NewSlaceConnect

`func NewSlaceConnect(rolloutId string, interactionName string, ) *SlaceConnect`

NewSlaceConnect instantiates a new SlaceConnect object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSlaceConnectWithDefaults

`func NewSlaceConnectWithDefaults() *SlaceConnect`

NewSlaceConnectWithDefaults instantiates a new SlaceConnect object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRolloutId

`func (o *SlaceConnect) GetRolloutId() string`

GetRolloutId returns the RolloutId field if non-nil, zero value otherwise.

### GetRolloutIdOk

`func (o *SlaceConnect) GetRolloutIdOk() (*string, bool)`

GetRolloutIdOk returns a tuple with the RolloutId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRolloutId

`func (o *SlaceConnect) SetRolloutId(v string)`

SetRolloutId sets RolloutId field to given value.


### GetInteractionName

`func (o *SlaceConnect) GetInteractionName() string`

GetInteractionName returns the InteractionName field if non-nil, zero value otherwise.

### GetInteractionNameOk

`func (o *SlaceConnect) GetInteractionNameOk() (*string, bool)`

GetInteractionNameOk returns a tuple with the InteractionName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionName

`func (o *SlaceConnect) SetInteractionName(v string)`

SetInteractionName sets InteractionName field to given value.


### GetCxId

`func (o *SlaceConnect) GetCxId() string`

GetCxId returns the CxId field if non-nil, zero value otherwise.

### GetCxIdOk

`func (o *SlaceConnect) GetCxIdOk() (*string, bool)`

GetCxIdOk returns a tuple with the CxId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCxId

`func (o *SlaceConnect) SetCxId(v string)`

SetCxId sets CxId field to given value.

### HasCxId

`func (o *SlaceConnect) HasCxId() bool`

HasCxId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


