# CxTemplateInstallationMutableData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PrepareData** | Pointer to [**CxTemplateInstallationMutableDataPrepareData**](CxTemplateInstallationMutableDataPrepareData.md) |  | [optional] 
**CustomizationsData** | Pointer to [**[]CxTemplateInstallationMutableDataCustomizationsDataInner**](CxTemplateInstallationMutableDataCustomizationsDataInner.md) |  | [optional] 
**WizardData** | Pointer to **map[string]interface{}** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**InteractionsCustomData** | Pointer to [**[]CxTemplateInstallationMutableDataInteractionsCustomDataInner**](CxTemplateInstallationMutableDataInteractionsCustomDataInner.md) |  | [optional] 
**TriggersCustomData** | Pointer to [**[]CxTemplateInstallationMutableDataTriggersCustomDataInner**](CxTemplateInstallationMutableDataTriggersCustomDataInner.md) |  | [optional] 

## Methods

### NewCxTemplateInstallationMutableData

`func NewCxTemplateInstallationMutableData() *CxTemplateInstallationMutableData`

NewCxTemplateInstallationMutableData instantiates a new CxTemplateInstallationMutableData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateInstallationMutableDataWithDefaults

`func NewCxTemplateInstallationMutableDataWithDefaults() *CxTemplateInstallationMutableData`

NewCxTemplateInstallationMutableDataWithDefaults instantiates a new CxTemplateInstallationMutableData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPrepareData

`func (o *CxTemplateInstallationMutableData) GetPrepareData() CxTemplateInstallationMutableDataPrepareData`

GetPrepareData returns the PrepareData field if non-nil, zero value otherwise.

### GetPrepareDataOk

`func (o *CxTemplateInstallationMutableData) GetPrepareDataOk() (*CxTemplateInstallationMutableDataPrepareData, bool)`

GetPrepareDataOk returns a tuple with the PrepareData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrepareData

`func (o *CxTemplateInstallationMutableData) SetPrepareData(v CxTemplateInstallationMutableDataPrepareData)`

SetPrepareData sets PrepareData field to given value.

### HasPrepareData

`func (o *CxTemplateInstallationMutableData) HasPrepareData() bool`

HasPrepareData returns a boolean if a field has been set.

### GetCustomizationsData

`func (o *CxTemplateInstallationMutableData) GetCustomizationsData() []CxTemplateInstallationMutableDataCustomizationsDataInner`

GetCustomizationsData returns the CustomizationsData field if non-nil, zero value otherwise.

### GetCustomizationsDataOk

`func (o *CxTemplateInstallationMutableData) GetCustomizationsDataOk() (*[]CxTemplateInstallationMutableDataCustomizationsDataInner, bool)`

GetCustomizationsDataOk returns a tuple with the CustomizationsData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomizationsData

`func (o *CxTemplateInstallationMutableData) SetCustomizationsData(v []CxTemplateInstallationMutableDataCustomizationsDataInner)`

SetCustomizationsData sets CustomizationsData field to given value.

### HasCustomizationsData

`func (o *CxTemplateInstallationMutableData) HasCustomizationsData() bool`

HasCustomizationsData returns a boolean if a field has been set.

### GetWizardData

`func (o *CxTemplateInstallationMutableData) GetWizardData() map[string]interface{}`

GetWizardData returns the WizardData field if non-nil, zero value otherwise.

### GetWizardDataOk

`func (o *CxTemplateInstallationMutableData) GetWizardDataOk() (*map[string]interface{}, bool)`

GetWizardDataOk returns a tuple with the WizardData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWizardData

`func (o *CxTemplateInstallationMutableData) SetWizardData(v map[string]interface{})`

SetWizardData sets WizardData field to given value.

### HasWizardData

`func (o *CxTemplateInstallationMutableData) HasWizardData() bool`

HasWizardData returns a boolean if a field has been set.

### GetChannelId

`func (o *CxTemplateInstallationMutableData) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CxTemplateInstallationMutableData) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CxTemplateInstallationMutableData) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *CxTemplateInstallationMutableData) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetInteractionsCustomData

`func (o *CxTemplateInstallationMutableData) GetInteractionsCustomData() []CxTemplateInstallationMutableDataInteractionsCustomDataInner`

GetInteractionsCustomData returns the InteractionsCustomData field if non-nil, zero value otherwise.

### GetInteractionsCustomDataOk

`func (o *CxTemplateInstallationMutableData) GetInteractionsCustomDataOk() (*[]CxTemplateInstallationMutableDataInteractionsCustomDataInner, bool)`

GetInteractionsCustomDataOk returns a tuple with the InteractionsCustomData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionsCustomData

`func (o *CxTemplateInstallationMutableData) SetInteractionsCustomData(v []CxTemplateInstallationMutableDataInteractionsCustomDataInner)`

SetInteractionsCustomData sets InteractionsCustomData field to given value.

### HasInteractionsCustomData

`func (o *CxTemplateInstallationMutableData) HasInteractionsCustomData() bool`

HasInteractionsCustomData returns a boolean if a field has been set.

### GetTriggersCustomData

`func (o *CxTemplateInstallationMutableData) GetTriggersCustomData() []CxTemplateInstallationMutableDataTriggersCustomDataInner`

GetTriggersCustomData returns the TriggersCustomData field if non-nil, zero value otherwise.

### GetTriggersCustomDataOk

`func (o *CxTemplateInstallationMutableData) GetTriggersCustomDataOk() (*[]CxTemplateInstallationMutableDataTriggersCustomDataInner, bool)`

GetTriggersCustomDataOk returns a tuple with the TriggersCustomData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTriggersCustomData

`func (o *CxTemplateInstallationMutableData) SetTriggersCustomData(v []CxTemplateInstallationMutableDataTriggersCustomDataInner)`

SetTriggersCustomData sets TriggersCustomData field to given value.

### HasTriggersCustomData

`func (o *CxTemplateInstallationMutableData) HasTriggersCustomData() bool`

HasTriggersCustomData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


