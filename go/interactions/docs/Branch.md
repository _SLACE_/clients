# Branch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**Condition** | Pointer to [**Condition**](Condition.md) |  | [optional] 
**Action** | Pointer to [**Action**](Action.md) |  | [optional] 
**BackgroundActions** | Pointer to [**[]Action**](Action.md) |  | [optional] 

## Methods

### NewBranch

`func NewBranch() *Branch`

NewBranch instantiates a new Branch object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBranchWithDefaults

`func NewBranchWithDefaults() *Branch`

NewBranchWithDefaults instantiates a new Branch object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *Branch) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Branch) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Branch) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *Branch) HasName() bool`

HasName returns a boolean if a field has been set.

### GetCondition

`func (o *Branch) GetCondition() Condition`

GetCondition returns the Condition field if non-nil, zero value otherwise.

### GetConditionOk

`func (o *Branch) GetConditionOk() (*Condition, bool)`

GetConditionOk returns a tuple with the Condition field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCondition

`func (o *Branch) SetCondition(v Condition)`

SetCondition sets Condition field to given value.

### HasCondition

`func (o *Branch) HasCondition() bool`

HasCondition returns a boolean if a field has been set.

### GetAction

`func (o *Branch) GetAction() Action`

GetAction returns the Action field if non-nil, zero value otherwise.

### GetActionOk

`func (o *Branch) GetActionOk() (*Action, bool)`

GetActionOk returns a tuple with the Action field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAction

`func (o *Branch) SetAction(v Action)`

SetAction sets Action field to given value.

### HasAction

`func (o *Branch) HasAction() bool`

HasAction returns a boolean if a field has been set.

### GetBackgroundActions

`func (o *Branch) GetBackgroundActions() []Action`

GetBackgroundActions returns the BackgroundActions field if non-nil, zero value otherwise.

### GetBackgroundActionsOk

`func (o *Branch) GetBackgroundActionsOk() (*[]Action, bool)`

GetBackgroundActionsOk returns a tuple with the BackgroundActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBackgroundActions

`func (o *Branch) SetBackgroundActions(v []Action)`

SetBackgroundActions sets BackgroundActions field to given value.

### HasBackgroundActions

`func (o *Branch) HasBackgroundActions() bool`

HasBackgroundActions returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


