# CreateRolloutRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelIds** | Pointer to **[]string** |  | [optional] 

## Methods

### NewCreateRolloutRequest

`func NewCreateRolloutRequest() *CreateRolloutRequest`

NewCreateRolloutRequest instantiates a new CreateRolloutRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateRolloutRequestWithDefaults

`func NewCreateRolloutRequestWithDefaults() *CreateRolloutRequest`

NewCreateRolloutRequestWithDefaults instantiates a new CreateRolloutRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetChannelIds

`func (o *CreateRolloutRequest) GetChannelIds() []string`

GetChannelIds returns the ChannelIds field if non-nil, zero value otherwise.

### GetChannelIdsOk

`func (o *CreateRolloutRequest) GetChannelIdsOk() (*[]string, bool)`

GetChannelIdsOk returns a tuple with the ChannelIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelIds

`func (o *CreateRolloutRequest) SetChannelIds(v []string)`

SetChannelIds sets ChannelIds field to given value.

### HasChannelIds

`func (o *CreateRolloutRequest) HasChannelIds() bool`

HasChannelIds returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


