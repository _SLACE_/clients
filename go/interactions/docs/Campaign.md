# Campaign

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Type** | **string** |  | 
**SubType** | Pointer to **string** |  | [optional] 
**GroupId** | Pointer to **string** |  | [optional] 
**Name** | **string** |  | 
**Status** | **string** |  | [readonly] 
**Messengers** | Pointer to **[]string** | Any of: Telegram, WhatsApp, Webchat, Facebook, Viber | [optional] 
**OrganizationId** | **string** |  | 
**TriggerConditions** | Pointer to [**[]CampaignTriggerConditionsInner**](CampaignTriggerConditionsInner.md) | DEPRECATED | [optional] 
**ExecuteAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 
**CreatedAt** | **time.Time** |  | 
**SendoutLimit** | **int32** |  | 
**TargetAll** | **bool** |  | 
**Recurring** | **bool** |  | 
**RecurringConfig** | Pointer to [**CampaignRecurringConfig**](CampaignRecurringConfig.md) |  | [optional] 
**TargetChannels** | [**[]CampaignTargetChannelsInner**](CampaignTargetChannelsInner.md) |  | 
**Strategy** | Pointer to [**CampaignStrategy**](CampaignStrategy.md) |  | [optional] 
**BudgetLimitCents** | Pointer to **int32** |  | [optional] 
**Conditions** | Pointer to [**[]CampaignCondition**](CampaignCondition.md) |  | [optional] 
**Cooldown** | **bool** |  | 
**IncludeMarketingOptOuts** | Pointer to **bool** |  | [optional] 
**TargetType** | **string** |  | 
**ConditionsDescription** | Pointer to **string** |  | [optional] 
**TargetContacts** | Pointer to [**[]CampaignTargetContactsInner**](CampaignTargetContactsInner.md) |  | [optional] 
**TargetMode** | Pointer to **string** |  | [optional] 
**TargetFilters** | Pointer to **map[string]interface{}** |  | [optional] 
**SendType** | Pointer to **string** |  | [optional] 

## Methods

### NewCampaign

`func NewCampaign(id string, type_ string, name string, status string, organizationId string, executeAt time.Time, updatedAt time.Time, createdAt time.Time, sendoutLimit int32, targetAll bool, recurring bool, targetChannels []CampaignTargetChannelsInner, cooldown bool, targetType string, ) *Campaign`

NewCampaign instantiates a new Campaign object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignWithDefaults

`func NewCampaignWithDefaults() *Campaign`

NewCampaignWithDefaults instantiates a new Campaign object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Campaign) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Campaign) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Campaign) SetId(v string)`

SetId sets Id field to given value.


### GetType

`func (o *Campaign) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Campaign) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Campaign) SetType(v string)`

SetType sets Type field to given value.


### GetSubType

`func (o *Campaign) GetSubType() string`

GetSubType returns the SubType field if non-nil, zero value otherwise.

### GetSubTypeOk

`func (o *Campaign) GetSubTypeOk() (*string, bool)`

GetSubTypeOk returns a tuple with the SubType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSubType

`func (o *Campaign) SetSubType(v string)`

SetSubType sets SubType field to given value.

### HasSubType

`func (o *Campaign) HasSubType() bool`

HasSubType returns a boolean if a field has been set.

### GetGroupId

`func (o *Campaign) GetGroupId() string`

GetGroupId returns the GroupId field if non-nil, zero value otherwise.

### GetGroupIdOk

`func (o *Campaign) GetGroupIdOk() (*string, bool)`

GetGroupIdOk returns a tuple with the GroupId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGroupId

`func (o *Campaign) SetGroupId(v string)`

SetGroupId sets GroupId field to given value.

### HasGroupId

`func (o *Campaign) HasGroupId() bool`

HasGroupId returns a boolean if a field has been set.

### GetName

`func (o *Campaign) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Campaign) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Campaign) SetName(v string)`

SetName sets Name field to given value.


### GetStatus

`func (o *Campaign) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *Campaign) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *Campaign) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetMessengers

`func (o *Campaign) GetMessengers() []string`

GetMessengers returns the Messengers field if non-nil, zero value otherwise.

### GetMessengersOk

`func (o *Campaign) GetMessengersOk() (*[]string, bool)`

GetMessengersOk returns a tuple with the Messengers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengers

`func (o *Campaign) SetMessengers(v []string)`

SetMessengers sets Messengers field to given value.

### HasMessengers

`func (o *Campaign) HasMessengers() bool`

HasMessengers returns a boolean if a field has been set.

### GetOrganizationId

`func (o *Campaign) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *Campaign) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *Campaign) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetTriggerConditions

`func (o *Campaign) GetTriggerConditions() []CampaignTriggerConditionsInner`

GetTriggerConditions returns the TriggerConditions field if non-nil, zero value otherwise.

### GetTriggerConditionsOk

`func (o *Campaign) GetTriggerConditionsOk() (*[]CampaignTriggerConditionsInner, bool)`

GetTriggerConditionsOk returns a tuple with the TriggerConditions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTriggerConditions

`func (o *Campaign) SetTriggerConditions(v []CampaignTriggerConditionsInner)`

SetTriggerConditions sets TriggerConditions field to given value.

### HasTriggerConditions

`func (o *Campaign) HasTriggerConditions() bool`

HasTriggerConditions returns a boolean if a field has been set.

### GetExecuteAt

`func (o *Campaign) GetExecuteAt() time.Time`

GetExecuteAt returns the ExecuteAt field if non-nil, zero value otherwise.

### GetExecuteAtOk

`func (o *Campaign) GetExecuteAtOk() (*time.Time, bool)`

GetExecuteAtOk returns a tuple with the ExecuteAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExecuteAt

`func (o *Campaign) SetExecuteAt(v time.Time)`

SetExecuteAt sets ExecuteAt field to given value.


### GetUpdatedAt

`func (o *Campaign) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Campaign) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Campaign) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetCreatedAt

`func (o *Campaign) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Campaign) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Campaign) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetSendoutLimit

`func (o *Campaign) GetSendoutLimit() int32`

GetSendoutLimit returns the SendoutLimit field if non-nil, zero value otherwise.

### GetSendoutLimitOk

`func (o *Campaign) GetSendoutLimitOk() (*int32, bool)`

GetSendoutLimitOk returns a tuple with the SendoutLimit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSendoutLimit

`func (o *Campaign) SetSendoutLimit(v int32)`

SetSendoutLimit sets SendoutLimit field to given value.


### GetTargetAll

`func (o *Campaign) GetTargetAll() bool`

GetTargetAll returns the TargetAll field if non-nil, zero value otherwise.

### GetTargetAllOk

`func (o *Campaign) GetTargetAllOk() (*bool, bool)`

GetTargetAllOk returns a tuple with the TargetAll field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetAll

`func (o *Campaign) SetTargetAll(v bool)`

SetTargetAll sets TargetAll field to given value.


### GetRecurring

`func (o *Campaign) GetRecurring() bool`

GetRecurring returns the Recurring field if non-nil, zero value otherwise.

### GetRecurringOk

`func (o *Campaign) GetRecurringOk() (*bool, bool)`

GetRecurringOk returns a tuple with the Recurring field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRecurring

`func (o *Campaign) SetRecurring(v bool)`

SetRecurring sets Recurring field to given value.


### GetRecurringConfig

`func (o *Campaign) GetRecurringConfig() CampaignRecurringConfig`

GetRecurringConfig returns the RecurringConfig field if non-nil, zero value otherwise.

### GetRecurringConfigOk

`func (o *Campaign) GetRecurringConfigOk() (*CampaignRecurringConfig, bool)`

GetRecurringConfigOk returns a tuple with the RecurringConfig field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRecurringConfig

`func (o *Campaign) SetRecurringConfig(v CampaignRecurringConfig)`

SetRecurringConfig sets RecurringConfig field to given value.

### HasRecurringConfig

`func (o *Campaign) HasRecurringConfig() bool`

HasRecurringConfig returns a boolean if a field has been set.

### GetTargetChannels

`func (o *Campaign) GetTargetChannels() []CampaignTargetChannelsInner`

GetTargetChannels returns the TargetChannels field if non-nil, zero value otherwise.

### GetTargetChannelsOk

`func (o *Campaign) GetTargetChannelsOk() (*[]CampaignTargetChannelsInner, bool)`

GetTargetChannelsOk returns a tuple with the TargetChannels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetChannels

`func (o *Campaign) SetTargetChannels(v []CampaignTargetChannelsInner)`

SetTargetChannels sets TargetChannels field to given value.


### GetStrategy

`func (o *Campaign) GetStrategy() CampaignStrategy`

GetStrategy returns the Strategy field if non-nil, zero value otherwise.

### GetStrategyOk

`func (o *Campaign) GetStrategyOk() (*CampaignStrategy, bool)`

GetStrategyOk returns a tuple with the Strategy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStrategy

`func (o *Campaign) SetStrategy(v CampaignStrategy)`

SetStrategy sets Strategy field to given value.

### HasStrategy

`func (o *Campaign) HasStrategy() bool`

HasStrategy returns a boolean if a field has been set.

### GetBudgetLimitCents

`func (o *Campaign) GetBudgetLimitCents() int32`

GetBudgetLimitCents returns the BudgetLimitCents field if non-nil, zero value otherwise.

### GetBudgetLimitCentsOk

`func (o *Campaign) GetBudgetLimitCentsOk() (*int32, bool)`

GetBudgetLimitCentsOk returns a tuple with the BudgetLimitCents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBudgetLimitCents

`func (o *Campaign) SetBudgetLimitCents(v int32)`

SetBudgetLimitCents sets BudgetLimitCents field to given value.

### HasBudgetLimitCents

`func (o *Campaign) HasBudgetLimitCents() bool`

HasBudgetLimitCents returns a boolean if a field has been set.

### GetConditions

`func (o *Campaign) GetConditions() []CampaignCondition`

GetConditions returns the Conditions field if non-nil, zero value otherwise.

### GetConditionsOk

`func (o *Campaign) GetConditionsOk() (*[]CampaignCondition, bool)`

GetConditionsOk returns a tuple with the Conditions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConditions

`func (o *Campaign) SetConditions(v []CampaignCondition)`

SetConditions sets Conditions field to given value.

### HasConditions

`func (o *Campaign) HasConditions() bool`

HasConditions returns a boolean if a field has been set.

### GetCooldown

`func (o *Campaign) GetCooldown() bool`

GetCooldown returns the Cooldown field if non-nil, zero value otherwise.

### GetCooldownOk

`func (o *Campaign) GetCooldownOk() (*bool, bool)`

GetCooldownOk returns a tuple with the Cooldown field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCooldown

`func (o *Campaign) SetCooldown(v bool)`

SetCooldown sets Cooldown field to given value.


### GetIncludeMarketingOptOuts

`func (o *Campaign) GetIncludeMarketingOptOuts() bool`

GetIncludeMarketingOptOuts returns the IncludeMarketingOptOuts field if non-nil, zero value otherwise.

### GetIncludeMarketingOptOutsOk

`func (o *Campaign) GetIncludeMarketingOptOutsOk() (*bool, bool)`

GetIncludeMarketingOptOutsOk returns a tuple with the IncludeMarketingOptOuts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIncludeMarketingOptOuts

`func (o *Campaign) SetIncludeMarketingOptOuts(v bool)`

SetIncludeMarketingOptOuts sets IncludeMarketingOptOuts field to given value.

### HasIncludeMarketingOptOuts

`func (o *Campaign) HasIncludeMarketingOptOuts() bool`

HasIncludeMarketingOptOuts returns a boolean if a field has been set.

### GetTargetType

`func (o *Campaign) GetTargetType() string`

GetTargetType returns the TargetType field if non-nil, zero value otherwise.

### GetTargetTypeOk

`func (o *Campaign) GetTargetTypeOk() (*string, bool)`

GetTargetTypeOk returns a tuple with the TargetType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetType

`func (o *Campaign) SetTargetType(v string)`

SetTargetType sets TargetType field to given value.


### GetConditionsDescription

`func (o *Campaign) GetConditionsDescription() string`

GetConditionsDescription returns the ConditionsDescription field if non-nil, zero value otherwise.

### GetConditionsDescriptionOk

`func (o *Campaign) GetConditionsDescriptionOk() (*string, bool)`

GetConditionsDescriptionOk returns a tuple with the ConditionsDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConditionsDescription

`func (o *Campaign) SetConditionsDescription(v string)`

SetConditionsDescription sets ConditionsDescription field to given value.

### HasConditionsDescription

`func (o *Campaign) HasConditionsDescription() bool`

HasConditionsDescription returns a boolean if a field has been set.

### GetTargetContacts

`func (o *Campaign) GetTargetContacts() []CampaignTargetContactsInner`

GetTargetContacts returns the TargetContacts field if non-nil, zero value otherwise.

### GetTargetContactsOk

`func (o *Campaign) GetTargetContactsOk() (*[]CampaignTargetContactsInner, bool)`

GetTargetContactsOk returns a tuple with the TargetContacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetContacts

`func (o *Campaign) SetTargetContacts(v []CampaignTargetContactsInner)`

SetTargetContacts sets TargetContacts field to given value.

### HasTargetContacts

`func (o *Campaign) HasTargetContacts() bool`

HasTargetContacts returns a boolean if a field has been set.

### GetTargetMode

`func (o *Campaign) GetTargetMode() string`

GetTargetMode returns the TargetMode field if non-nil, zero value otherwise.

### GetTargetModeOk

`func (o *Campaign) GetTargetModeOk() (*string, bool)`

GetTargetModeOk returns a tuple with the TargetMode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetMode

`func (o *Campaign) SetTargetMode(v string)`

SetTargetMode sets TargetMode field to given value.

### HasTargetMode

`func (o *Campaign) HasTargetMode() bool`

HasTargetMode returns a boolean if a field has been set.

### GetTargetFilters

`func (o *Campaign) GetTargetFilters() map[string]interface{}`

GetTargetFilters returns the TargetFilters field if non-nil, zero value otherwise.

### GetTargetFiltersOk

`func (o *Campaign) GetTargetFiltersOk() (*map[string]interface{}, bool)`

GetTargetFiltersOk returns a tuple with the TargetFilters field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetFilters

`func (o *Campaign) SetTargetFilters(v map[string]interface{})`

SetTargetFilters sets TargetFilters field to given value.

### HasTargetFilters

`func (o *Campaign) HasTargetFilters() bool`

HasTargetFilters returns a boolean if a field has been set.

### GetSendType

`func (o *Campaign) GetSendType() string`

GetSendType returns the SendType field if non-nil, zero value otherwise.

### GetSendTypeOk

`func (o *Campaign) GetSendTypeOk() (*string, bool)`

GetSendTypeOk returns a tuple with the SendType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSendType

`func (o *Campaign) SetSendType(v string)`

SetSendType sets SendType field to given value.

### HasSendType

`func (o *Campaign) HasSendType() bool`

HasSendType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


