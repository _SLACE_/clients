# ReportConfiguration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**OrganizationId** | Pointer to **string** |  | [optional] 
**Name** | **string** |  | 
**Scope** | [**ReportConfigurationScope**](ReportConfigurationScope.md) |  | 
**Data** | [**ReportConfigurationData**](ReportConfigurationData.md) |  | 
**Active** | **bool** |  | 
**CreatedAt** | **string** |  | 
**UpdatedAt** | **string** |  | 

## Methods

### NewReportConfiguration

`func NewReportConfiguration(id string, name string, scope ReportConfigurationScope, data ReportConfigurationData, active bool, createdAt string, updatedAt string, ) *ReportConfiguration`

NewReportConfiguration instantiates a new ReportConfiguration object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReportConfigurationWithDefaults

`func NewReportConfigurationWithDefaults() *ReportConfiguration`

NewReportConfigurationWithDefaults instantiates a new ReportConfiguration object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ReportConfiguration) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ReportConfiguration) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ReportConfiguration) SetId(v string)`

SetId sets Id field to given value.


### GetOrganizationId

`func (o *ReportConfiguration) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *ReportConfiguration) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *ReportConfiguration) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *ReportConfiguration) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetName

`func (o *ReportConfiguration) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ReportConfiguration) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ReportConfiguration) SetName(v string)`

SetName sets Name field to given value.


### GetScope

`func (o *ReportConfiguration) GetScope() ReportConfigurationScope`

GetScope returns the Scope field if non-nil, zero value otherwise.

### GetScopeOk

`func (o *ReportConfiguration) GetScopeOk() (*ReportConfigurationScope, bool)`

GetScopeOk returns a tuple with the Scope field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScope

`func (o *ReportConfiguration) SetScope(v ReportConfigurationScope)`

SetScope sets Scope field to given value.


### GetData

`func (o *ReportConfiguration) GetData() ReportConfigurationData`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *ReportConfiguration) GetDataOk() (*ReportConfigurationData, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *ReportConfiguration) SetData(v ReportConfigurationData)`

SetData sets Data field to given value.


### GetActive

`func (o *ReportConfiguration) GetActive() bool`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *ReportConfiguration) GetActiveOk() (*bool, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *ReportConfiguration) SetActive(v bool)`

SetActive sets Active field to given value.


### GetCreatedAt

`func (o *ReportConfiguration) GetCreatedAt() string`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *ReportConfiguration) GetCreatedAtOk() (*string, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *ReportConfiguration) SetCreatedAt(v string)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *ReportConfiguration) GetUpdatedAt() string`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *ReportConfiguration) GetUpdatedAtOk() (*string, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *ReportConfiguration) SetUpdatedAt(v string)`

SetUpdatedAt sets UpdatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


