# CampaignTriggerConditionsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConditionType** | **string** |  | 
**Index** | **float32** |  | 
**Key** | Pointer to **string** |  | [optional] 
**Operator** | **string** |  | 
**Values** | **[]string** |  | 
**ValueType** | **string** |  | 

## Methods

### NewCampaignTriggerConditionsInner

`func NewCampaignTriggerConditionsInner(conditionType string, index float32, operator string, values []string, valueType string, ) *CampaignTriggerConditionsInner`

NewCampaignTriggerConditionsInner instantiates a new CampaignTriggerConditionsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignTriggerConditionsInnerWithDefaults

`func NewCampaignTriggerConditionsInnerWithDefaults() *CampaignTriggerConditionsInner`

NewCampaignTriggerConditionsInnerWithDefaults instantiates a new CampaignTriggerConditionsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetConditionType

`func (o *CampaignTriggerConditionsInner) GetConditionType() string`

GetConditionType returns the ConditionType field if non-nil, zero value otherwise.

### GetConditionTypeOk

`func (o *CampaignTriggerConditionsInner) GetConditionTypeOk() (*string, bool)`

GetConditionTypeOk returns a tuple with the ConditionType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConditionType

`func (o *CampaignTriggerConditionsInner) SetConditionType(v string)`

SetConditionType sets ConditionType field to given value.


### GetIndex

`func (o *CampaignTriggerConditionsInner) GetIndex() float32`

GetIndex returns the Index field if non-nil, zero value otherwise.

### GetIndexOk

`func (o *CampaignTriggerConditionsInner) GetIndexOk() (*float32, bool)`

GetIndexOk returns a tuple with the Index field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIndex

`func (o *CampaignTriggerConditionsInner) SetIndex(v float32)`

SetIndex sets Index field to given value.


### GetKey

`func (o *CampaignTriggerConditionsInner) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *CampaignTriggerConditionsInner) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *CampaignTriggerConditionsInner) SetKey(v string)`

SetKey sets Key field to given value.

### HasKey

`func (o *CampaignTriggerConditionsInner) HasKey() bool`

HasKey returns a boolean if a field has been set.

### GetOperator

`func (o *CampaignTriggerConditionsInner) GetOperator() string`

GetOperator returns the Operator field if non-nil, zero value otherwise.

### GetOperatorOk

`func (o *CampaignTriggerConditionsInner) GetOperatorOk() (*string, bool)`

GetOperatorOk returns a tuple with the Operator field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOperator

`func (o *CampaignTriggerConditionsInner) SetOperator(v string)`

SetOperator sets Operator field to given value.


### GetValues

`func (o *CampaignTriggerConditionsInner) GetValues() []string`

GetValues returns the Values field if non-nil, zero value otherwise.

### GetValuesOk

`func (o *CampaignTriggerConditionsInner) GetValuesOk() (*[]string, bool)`

GetValuesOk returns a tuple with the Values field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValues

`func (o *CampaignTriggerConditionsInner) SetValues(v []string)`

SetValues sets Values field to given value.


### GetValueType

`func (o *CampaignTriggerConditionsInner) GetValueType() string`

GetValueType returns the ValueType field if non-nil, zero value otherwise.

### GetValueTypeOk

`func (o *CampaignTriggerConditionsInner) GetValueTypeOk() (*string, bool)`

GetValueTypeOk returns a tuple with the ValueType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValueType

`func (o *CampaignTriggerConditionsInner) SetValueType(v string)`

SetValueType sets ValueType field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


