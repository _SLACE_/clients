# MediaFee

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**RolloutId** | Pointer to **string** |  | [optional] 
**OrganizationId** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**MainRolloutId** | Pointer to **string** |  | [optional] 
**Price** | Pointer to **string** | decimal string | [optional] 
**TargetAgencyId** | Pointer to **string** |  | [optional] 
**TargetAgencyPercentage** | Pointer to **int32** |  | [optional] 
**SourceAgencyId** | Pointer to **string** |  | [optional] 
**SourceAgencyPercentage** | Pointer to **int32** |  | [optional] 

## Methods

### NewMediaFee

`func NewMediaFee() *MediaFee`

NewMediaFee instantiates a new MediaFee object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMediaFeeWithDefaults

`func NewMediaFeeWithDefaults() *MediaFee`

NewMediaFeeWithDefaults instantiates a new MediaFee object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *MediaFee) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *MediaFee) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *MediaFee) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *MediaFee) HasId() bool`

HasId returns a boolean if a field has been set.

### GetRolloutId

`func (o *MediaFee) GetRolloutId() string`

GetRolloutId returns the RolloutId field if non-nil, zero value otherwise.

### GetRolloutIdOk

`func (o *MediaFee) GetRolloutIdOk() (*string, bool)`

GetRolloutIdOk returns a tuple with the RolloutId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRolloutId

`func (o *MediaFee) SetRolloutId(v string)`

SetRolloutId sets RolloutId field to given value.

### HasRolloutId

`func (o *MediaFee) HasRolloutId() bool`

HasRolloutId returns a boolean if a field has been set.

### GetOrganizationId

`func (o *MediaFee) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *MediaFee) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *MediaFee) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *MediaFee) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetChannelId

`func (o *MediaFee) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *MediaFee) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *MediaFee) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *MediaFee) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetMainRolloutId

`func (o *MediaFee) GetMainRolloutId() string`

GetMainRolloutId returns the MainRolloutId field if non-nil, zero value otherwise.

### GetMainRolloutIdOk

`func (o *MediaFee) GetMainRolloutIdOk() (*string, bool)`

GetMainRolloutIdOk returns a tuple with the MainRolloutId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMainRolloutId

`func (o *MediaFee) SetMainRolloutId(v string)`

SetMainRolloutId sets MainRolloutId field to given value.

### HasMainRolloutId

`func (o *MediaFee) HasMainRolloutId() bool`

HasMainRolloutId returns a boolean if a field has been set.

### GetPrice

`func (o *MediaFee) GetPrice() string`

GetPrice returns the Price field if non-nil, zero value otherwise.

### GetPriceOk

`func (o *MediaFee) GetPriceOk() (*string, bool)`

GetPriceOk returns a tuple with the Price field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrice

`func (o *MediaFee) SetPrice(v string)`

SetPrice sets Price field to given value.

### HasPrice

`func (o *MediaFee) HasPrice() bool`

HasPrice returns a boolean if a field has been set.

### GetTargetAgencyId

`func (o *MediaFee) GetTargetAgencyId() string`

GetTargetAgencyId returns the TargetAgencyId field if non-nil, zero value otherwise.

### GetTargetAgencyIdOk

`func (o *MediaFee) GetTargetAgencyIdOk() (*string, bool)`

GetTargetAgencyIdOk returns a tuple with the TargetAgencyId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetAgencyId

`func (o *MediaFee) SetTargetAgencyId(v string)`

SetTargetAgencyId sets TargetAgencyId field to given value.

### HasTargetAgencyId

`func (o *MediaFee) HasTargetAgencyId() bool`

HasTargetAgencyId returns a boolean if a field has been set.

### GetTargetAgencyPercentage

`func (o *MediaFee) GetTargetAgencyPercentage() int32`

GetTargetAgencyPercentage returns the TargetAgencyPercentage field if non-nil, zero value otherwise.

### GetTargetAgencyPercentageOk

`func (o *MediaFee) GetTargetAgencyPercentageOk() (*int32, bool)`

GetTargetAgencyPercentageOk returns a tuple with the TargetAgencyPercentage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetAgencyPercentage

`func (o *MediaFee) SetTargetAgencyPercentage(v int32)`

SetTargetAgencyPercentage sets TargetAgencyPercentage field to given value.

### HasTargetAgencyPercentage

`func (o *MediaFee) HasTargetAgencyPercentage() bool`

HasTargetAgencyPercentage returns a boolean if a field has been set.

### GetSourceAgencyId

`func (o *MediaFee) GetSourceAgencyId() string`

GetSourceAgencyId returns the SourceAgencyId field if non-nil, zero value otherwise.

### GetSourceAgencyIdOk

`func (o *MediaFee) GetSourceAgencyIdOk() (*string, bool)`

GetSourceAgencyIdOk returns a tuple with the SourceAgencyId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceAgencyId

`func (o *MediaFee) SetSourceAgencyId(v string)`

SetSourceAgencyId sets SourceAgencyId field to given value.

### HasSourceAgencyId

`func (o *MediaFee) HasSourceAgencyId() bool`

HasSourceAgencyId returns a boolean if a field has been set.

### GetSourceAgencyPercentage

`func (o *MediaFee) GetSourceAgencyPercentage() int32`

GetSourceAgencyPercentage returns the SourceAgencyPercentage field if non-nil, zero value otherwise.

### GetSourceAgencyPercentageOk

`func (o *MediaFee) GetSourceAgencyPercentageOk() (*int32, bool)`

GetSourceAgencyPercentageOk returns a tuple with the SourceAgencyPercentage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceAgencyPercentage

`func (o *MediaFee) SetSourceAgencyPercentage(v int32)`

SetSourceAgencyPercentage sets SourceAgencyPercentage field to given value.

### HasSourceAgencyPercentage

`func (o *MediaFee) HasSourceAgencyPercentage() bool`

HasSourceAgencyPercentage returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


