# CxTemplateInstallationMutableDataInteractionsCustomDataInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InteractionId** | Pointer to **string** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 

## Methods

### NewCxTemplateInstallationMutableDataInteractionsCustomDataInner

`func NewCxTemplateInstallationMutableDataInteractionsCustomDataInner() *CxTemplateInstallationMutableDataInteractionsCustomDataInner`

NewCxTemplateInstallationMutableDataInteractionsCustomDataInner instantiates a new CxTemplateInstallationMutableDataInteractionsCustomDataInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateInstallationMutableDataInteractionsCustomDataInnerWithDefaults

`func NewCxTemplateInstallationMutableDataInteractionsCustomDataInnerWithDefaults() *CxTemplateInstallationMutableDataInteractionsCustomDataInner`

NewCxTemplateInstallationMutableDataInteractionsCustomDataInnerWithDefaults instantiates a new CxTemplateInstallationMutableDataInteractionsCustomDataInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInteractionId

`func (o *CxTemplateInstallationMutableDataInteractionsCustomDataInner) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *CxTemplateInstallationMutableDataInteractionsCustomDataInner) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *CxTemplateInstallationMutableDataInteractionsCustomDataInner) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *CxTemplateInstallationMutableDataInteractionsCustomDataInner) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetName

`func (o *CxTemplateInstallationMutableDataInteractionsCustomDataInner) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CxTemplateInstallationMutableDataInteractionsCustomDataInner) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CxTemplateInstallationMutableDataInteractionsCustomDataInner) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *CxTemplateInstallationMutableDataInteractionsCustomDataInner) HasName() bool`

HasName returns a boolean if a field has been set.

### GetTags

`func (o *CxTemplateInstallationMutableDataInteractionsCustomDataInner) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *CxTemplateInstallationMutableDataInteractionsCustomDataInner) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *CxTemplateInstallationMutableDataInteractionsCustomDataInner) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *CxTemplateInstallationMutableDataInteractionsCustomDataInner) HasTags() bool`

HasTags returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


