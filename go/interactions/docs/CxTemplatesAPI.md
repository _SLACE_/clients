# \CxTemplatesAPI

All URIs are relative to *https://interactions.slace.dev/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetCxTemplatesId**](CxTemplatesAPI.md#GetCxTemplatesId) | **Get** /cx-templates/{id} | Get one global
[**GetOrganizationsOrganizationIdCxTemplates**](CxTemplatesAPI.md#GetOrganizationsOrganizationIdCxTemplates) | **Get** /organizations/{organization_id}/cx-templates | Get list of organization cx templates
[**GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns**](CxTemplatesAPI.md#GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns) | **Get** /organizations/{organization_id}/cx-templates-installations/{installation_id}/campaigns | Get installation campaigns
[**GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts**](CxTemplatesAPI.md#GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts) | **Get** /organizations/{organization_id}/cx-templates-installations/{installation_id}/scripts | Get installation scripts
[**PostOrganizationsOrganizationIdCxTemplates**](CxTemplatesAPI.md#PostOrganizationsOrganizationIdCxTemplates) | **Post** /organizations/{organization_id}/cx-templates | Create cx template



## GetCxTemplatesId

> CxTemplate GetCxTemplatesId(ctx, id).Execute()

Get one global

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplatesAPI.GetCxTemplatesId(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplatesAPI.GetCxTemplatesId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCxTemplatesId`: CxTemplate
    fmt.Fprintf(os.Stdout, "Response from `CxTemplatesAPI.GetCxTemplatesId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetCxTemplatesIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**CxTemplate**](CxTemplate.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationsOrganizationIdCxTemplates

> GetOrganizationsOrganizationIdCxTemplates200Response GetOrganizationsOrganizationIdCxTemplates(ctx, organizationId).Tags(tags).Statuses(statuses).Languages(languages).Formality(formality).Global(global).Shared(shared).Execute()

Get list of organization cx templates

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    tags := []string{"Inner_example"} // []string | Tags filter (optional)
    statuses := []string{"Inner_example"} // []string |  (optional)
    languages := []string{"Inner_example"} // []string | Languages filter (optional)
    formality := "formality_example" // string |  (optional)
    global := true // bool |  (optional)
    shared := true // bool |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplatesAPI.GetOrganizationsOrganizationIdCxTemplates(context.Background(), organizationId).Tags(tags).Statuses(statuses).Languages(languages).Formality(formality).Global(global).Shared(shared).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplatesAPI.GetOrganizationsOrganizationIdCxTemplates``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationsOrganizationIdCxTemplates`: GetOrganizationsOrganizationIdCxTemplates200Response
    fmt.Fprintf(os.Stdout, "Response from `CxTemplatesAPI.GetOrganizationsOrganizationIdCxTemplates`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationsOrganizationIdCxTemplatesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **tags** | **[]string** | Tags filter | 
 **statuses** | **[]string** |  | 
 **languages** | **[]string** | Languages filter | 
 **formality** | **string** |  | 
 **global** | **bool** |  | 
 **shared** | **bool** |  | 

### Return type

[**GetOrganizationsOrganizationIdCxTemplates200Response**](GetOrganizationsOrganizationIdCxTemplates200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns

> GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns(ctx, organizationId, installationId).InteractionId(interactionId).Execute()

Get installation campaigns

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    installationId := "installationId_example" // string | 
    interactionId := "interactionId_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplatesAPI.GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns(context.Background(), organizationId, installationId).InteractionId(interactionId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplatesAPI.GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns`: GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response
    fmt.Fprintf(os.Stdout, "Response from `CxTemplatesAPI.GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**installationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaignsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **interactionId** | **string** |  | 

### Return type

[**GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response**](GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdCampaigns200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts

> GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts(ctx, organizationId, installationId).InteractionId(interactionId).Execute()

Get installation scripts

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    installationId := "installationId_example" // string | 
    interactionId := "interactionId_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplatesAPI.GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts(context.Background(), organizationId, installationId).InteractionId(interactionId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplatesAPI.GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts`: GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response
    fmt.Fprintf(os.Stdout, "Response from `CxTemplatesAPI.GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**installationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScriptsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **interactionId** | **string** |  | 

### Return type

[**GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response**](GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostOrganizationsOrganizationIdCxTemplates

> CxTemplate PostOrganizationsOrganizationIdCxTemplates(ctx, organizationId).CxTemplateCreateData(cxTemplateCreateData).Execute()

Create cx template

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    cxTemplateCreateData := *openapiclient.NewCxTemplateCreateData() // CxTemplateCreateData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplatesAPI.PostOrganizationsOrganizationIdCxTemplates(context.Background(), organizationId).CxTemplateCreateData(cxTemplateCreateData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplatesAPI.PostOrganizationsOrganizationIdCxTemplates``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PostOrganizationsOrganizationIdCxTemplates`: CxTemplate
    fmt.Fprintf(os.Stdout, "Response from `CxTemplatesAPI.PostOrganizationsOrganizationIdCxTemplates`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPostOrganizationsOrganizationIdCxTemplatesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cxTemplateCreateData** | [**CxTemplateCreateData**](CxTemplateCreateData.md) |  | 

### Return type

[**CxTemplate**](CxTemplate.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

