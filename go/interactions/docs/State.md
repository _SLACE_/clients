# State

## Enum


* `DRAFT` (value: `"draft"`)

* `IN_REVIEW` (value: `"in_review"`)

* `APPROVED` (value: `"approved"`)

* `LIVE` (value: `"live"`)

* `STOPPED` (value: `"stopped"`)

* `ARCHIVED` (value: `"archived"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


