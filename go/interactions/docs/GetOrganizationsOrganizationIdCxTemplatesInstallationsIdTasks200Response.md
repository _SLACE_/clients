# GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | Pointer to [**Task**](Task.md) |  | [optional] 
**Total** | Pointer to **string** |  | [optional] 

## Methods

### NewGetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response

`func NewGetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response() *GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response`

NewGetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response instantiates a new GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200ResponseWithDefaults

`func NewGetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200ResponseWithDefaults() *GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response`

NewGetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200ResponseWithDefaults instantiates a new GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response) GetResults() Task`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response) GetResultsOk() (*Task, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response) SetResults(v Task)`

SetResults sets Results field to given value.

### HasResults

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response) HasResults() bool`

HasResults returns a boolean if a field has been set.

### GetTotal

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response) GetTotal() string`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response) GetTotalOk() (*string, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response) SetTotal(v string)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


