# Trigger

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EventType** | Pointer to **string** |  | [optional] 
**Condition** | [**Condition**](Condition.md) |  | 
**Template** | Pointer to [**TriggerTemplate**](TriggerTemplate.md) |  | [optional] 
**InputHandlers** | Pointer to [**[]Action**](Action.md) |  | [optional] 

## Methods

### NewTrigger

`func NewTrigger(condition Condition, ) *Trigger`

NewTrigger instantiates a new Trigger object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTriggerWithDefaults

`func NewTriggerWithDefaults() *Trigger`

NewTriggerWithDefaults instantiates a new Trigger object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetEventType

`func (o *Trigger) GetEventType() string`

GetEventType returns the EventType field if non-nil, zero value otherwise.

### GetEventTypeOk

`func (o *Trigger) GetEventTypeOk() (*string, bool)`

GetEventTypeOk returns a tuple with the EventType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventType

`func (o *Trigger) SetEventType(v string)`

SetEventType sets EventType field to given value.

### HasEventType

`func (o *Trigger) HasEventType() bool`

HasEventType returns a boolean if a field has been set.

### GetCondition

`func (o *Trigger) GetCondition() Condition`

GetCondition returns the Condition field if non-nil, zero value otherwise.

### GetConditionOk

`func (o *Trigger) GetConditionOk() (*Condition, bool)`

GetConditionOk returns a tuple with the Condition field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCondition

`func (o *Trigger) SetCondition(v Condition)`

SetCondition sets Condition field to given value.


### GetTemplate

`func (o *Trigger) GetTemplate() TriggerTemplate`

GetTemplate returns the Template field if non-nil, zero value otherwise.

### GetTemplateOk

`func (o *Trigger) GetTemplateOk() (*TriggerTemplate, bool)`

GetTemplateOk returns a tuple with the Template field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplate

`func (o *Trigger) SetTemplate(v TriggerTemplate)`

SetTemplate sets Template field to given value.

### HasTemplate

`func (o *Trigger) HasTemplate() bool`

HasTemplate returns a boolean if a field has been set.

### GetInputHandlers

`func (o *Trigger) GetInputHandlers() []Action`

GetInputHandlers returns the InputHandlers field if non-nil, zero value otherwise.

### GetInputHandlersOk

`func (o *Trigger) GetInputHandlersOk() (*[]Action, bool)`

GetInputHandlersOk returns a tuple with the InputHandlers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInputHandlers

`func (o *Trigger) SetInputHandlers(v []Action)`

SetInputHandlers sets InputHandlers field to given value.

### HasInputHandlers

`func (o *Trigger) HasInputHandlers() bool`

HasInputHandlers returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


