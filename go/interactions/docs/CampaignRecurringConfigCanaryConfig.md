# CampaignRecurringConfigCanaryConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Amount** | Pointer to **int32** |  | [optional] 
**ManualResume** | Pointer to **bool** |  | [optional] 

## Methods

### NewCampaignRecurringConfigCanaryConfig

`func NewCampaignRecurringConfigCanaryConfig() *CampaignRecurringConfigCanaryConfig`

NewCampaignRecurringConfigCanaryConfig instantiates a new CampaignRecurringConfigCanaryConfig object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignRecurringConfigCanaryConfigWithDefaults

`func NewCampaignRecurringConfigCanaryConfigWithDefaults() *CampaignRecurringConfigCanaryConfig`

NewCampaignRecurringConfigCanaryConfigWithDefaults instantiates a new CampaignRecurringConfigCanaryConfig object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAmount

`func (o *CampaignRecurringConfigCanaryConfig) GetAmount() int32`

GetAmount returns the Amount field if non-nil, zero value otherwise.

### GetAmountOk

`func (o *CampaignRecurringConfigCanaryConfig) GetAmountOk() (*int32, bool)`

GetAmountOk returns a tuple with the Amount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAmount

`func (o *CampaignRecurringConfigCanaryConfig) SetAmount(v int32)`

SetAmount sets Amount field to given value.

### HasAmount

`func (o *CampaignRecurringConfigCanaryConfig) HasAmount() bool`

HasAmount returns a boolean if a field has been set.

### GetManualResume

`func (o *CampaignRecurringConfigCanaryConfig) GetManualResume() bool`

GetManualResume returns the ManualResume field if non-nil, zero value otherwise.

### GetManualResumeOk

`func (o *CampaignRecurringConfigCanaryConfig) GetManualResumeOk() (*bool, bool)`

GetManualResumeOk returns a tuple with the ManualResume field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetManualResume

`func (o *CampaignRecurringConfigCanaryConfig) SetManualResume(v bool)`

SetManualResume sets ManualResume field to given value.

### HasManualResume

`func (o *CampaignRecurringConfigCanaryConfig) HasManualResume() bool`

HasManualResume returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


