# CloneResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Success** | Pointer to **bool** |  | [optional] 
**NewInteractions** | Pointer to [**CloneResponseNewInteractions**](CloneResponseNewInteractions.md) |  | [optional] 
**Errors** | Pointer to [**[]RolloutError**](RolloutError.md) |  | [optional] 

## Methods

### NewCloneResponse

`func NewCloneResponse() *CloneResponse`

NewCloneResponse instantiates a new CloneResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCloneResponseWithDefaults

`func NewCloneResponseWithDefaults() *CloneResponse`

NewCloneResponseWithDefaults instantiates a new CloneResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSuccess

`func (o *CloneResponse) GetSuccess() bool`

GetSuccess returns the Success field if non-nil, zero value otherwise.

### GetSuccessOk

`func (o *CloneResponse) GetSuccessOk() (*bool, bool)`

GetSuccessOk returns a tuple with the Success field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSuccess

`func (o *CloneResponse) SetSuccess(v bool)`

SetSuccess sets Success field to given value.

### HasSuccess

`func (o *CloneResponse) HasSuccess() bool`

HasSuccess returns a boolean if a field has been set.

### GetNewInteractions

`func (o *CloneResponse) GetNewInteractions() CloneResponseNewInteractions`

GetNewInteractions returns the NewInteractions field if non-nil, zero value otherwise.

### GetNewInteractionsOk

`func (o *CloneResponse) GetNewInteractionsOk() (*CloneResponseNewInteractions, bool)`

GetNewInteractionsOk returns a tuple with the NewInteractions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewInteractions

`func (o *CloneResponse) SetNewInteractions(v CloneResponseNewInteractions)`

SetNewInteractions sets NewInteractions field to given value.

### HasNewInteractions

`func (o *CloneResponse) HasNewInteractions() bool`

HasNewInteractions returns a boolean if a field has been set.

### GetErrors

`func (o *CloneResponse) GetErrors() []RolloutError`

GetErrors returns the Errors field if non-nil, zero value otherwise.

### GetErrorsOk

`func (o *CloneResponse) GetErrorsOk() (*[]RolloutError, bool)`

GetErrorsOk returns a tuple with the Errors field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrors

`func (o *CloneResponse) SetErrors(v []RolloutError)`

SetErrors sets Errors field to given value.

### HasErrors

`func (o *CloneResponse) HasErrors() bool`

HasErrors returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


