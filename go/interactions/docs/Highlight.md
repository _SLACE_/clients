# Highlight

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**OrganizationId** | **string** |  | 
**ConfigurationId** | **string** |  | 
**Template** | **string** |  | 
**Metric** | [**HighlightMetric**](HighlightMetric.md) |  | 
**Frequency** | [**Frequency**](Frequency.md) |  | 
**GuestsEntryPoint** | Pointer to **string** |  | [optional] 
**OutsideBusinessHours** | **bool** |  | 
**Weekdays** | **[]int32** |  | 
**StartAt** | **time.Time** |  | 
**EndAt** | **time.Time** |  | 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 

## Methods

### NewHighlight

`func NewHighlight(id string, organizationId string, configurationId string, template string, metric HighlightMetric, frequency Frequency, outsideBusinessHours bool, weekdays []int32, startAt time.Time, endAt time.Time, createdAt time.Time, updatedAt time.Time, ) *Highlight`

NewHighlight instantiates a new Highlight object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewHighlightWithDefaults

`func NewHighlightWithDefaults() *Highlight`

NewHighlightWithDefaults instantiates a new Highlight object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Highlight) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Highlight) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Highlight) SetId(v string)`

SetId sets Id field to given value.


### GetOrganizationId

`func (o *Highlight) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *Highlight) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *Highlight) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetConfigurationId

`func (o *Highlight) GetConfigurationId() string`

GetConfigurationId returns the ConfigurationId field if non-nil, zero value otherwise.

### GetConfigurationIdOk

`func (o *Highlight) GetConfigurationIdOk() (*string, bool)`

GetConfigurationIdOk returns a tuple with the ConfigurationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfigurationId

`func (o *Highlight) SetConfigurationId(v string)`

SetConfigurationId sets ConfigurationId field to given value.


### GetTemplate

`func (o *Highlight) GetTemplate() string`

GetTemplate returns the Template field if non-nil, zero value otherwise.

### GetTemplateOk

`func (o *Highlight) GetTemplateOk() (*string, bool)`

GetTemplateOk returns a tuple with the Template field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplate

`func (o *Highlight) SetTemplate(v string)`

SetTemplate sets Template field to given value.


### GetMetric

`func (o *Highlight) GetMetric() HighlightMetric`

GetMetric returns the Metric field if non-nil, zero value otherwise.

### GetMetricOk

`func (o *Highlight) GetMetricOk() (*HighlightMetric, bool)`

GetMetricOk returns a tuple with the Metric field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetric

`func (o *Highlight) SetMetric(v HighlightMetric)`

SetMetric sets Metric field to given value.


### GetFrequency

`func (o *Highlight) GetFrequency() Frequency`

GetFrequency returns the Frequency field if non-nil, zero value otherwise.

### GetFrequencyOk

`func (o *Highlight) GetFrequencyOk() (*Frequency, bool)`

GetFrequencyOk returns a tuple with the Frequency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFrequency

`func (o *Highlight) SetFrequency(v Frequency)`

SetFrequency sets Frequency field to given value.


### GetGuestsEntryPoint

`func (o *Highlight) GetGuestsEntryPoint() string`

GetGuestsEntryPoint returns the GuestsEntryPoint field if non-nil, zero value otherwise.

### GetGuestsEntryPointOk

`func (o *Highlight) GetGuestsEntryPointOk() (*string, bool)`

GetGuestsEntryPointOk returns a tuple with the GuestsEntryPoint field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGuestsEntryPoint

`func (o *Highlight) SetGuestsEntryPoint(v string)`

SetGuestsEntryPoint sets GuestsEntryPoint field to given value.

### HasGuestsEntryPoint

`func (o *Highlight) HasGuestsEntryPoint() bool`

HasGuestsEntryPoint returns a boolean if a field has been set.

### GetOutsideBusinessHours

`func (o *Highlight) GetOutsideBusinessHours() bool`

GetOutsideBusinessHours returns the OutsideBusinessHours field if non-nil, zero value otherwise.

### GetOutsideBusinessHoursOk

`func (o *Highlight) GetOutsideBusinessHoursOk() (*bool, bool)`

GetOutsideBusinessHoursOk returns a tuple with the OutsideBusinessHours field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOutsideBusinessHours

`func (o *Highlight) SetOutsideBusinessHours(v bool)`

SetOutsideBusinessHours sets OutsideBusinessHours field to given value.


### GetWeekdays

`func (o *Highlight) GetWeekdays() []int32`

GetWeekdays returns the Weekdays field if non-nil, zero value otherwise.

### GetWeekdaysOk

`func (o *Highlight) GetWeekdaysOk() (*[]int32, bool)`

GetWeekdaysOk returns a tuple with the Weekdays field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWeekdays

`func (o *Highlight) SetWeekdays(v []int32)`

SetWeekdays sets Weekdays field to given value.


### GetStartAt

`func (o *Highlight) GetStartAt() time.Time`

GetStartAt returns the StartAt field if non-nil, zero value otherwise.

### GetStartAtOk

`func (o *Highlight) GetStartAtOk() (*time.Time, bool)`

GetStartAtOk returns a tuple with the StartAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartAt

`func (o *Highlight) SetStartAt(v time.Time)`

SetStartAt sets StartAt field to given value.


### GetEndAt

`func (o *Highlight) GetEndAt() time.Time`

GetEndAt returns the EndAt field if non-nil, zero value otherwise.

### GetEndAtOk

`func (o *Highlight) GetEndAtOk() (*time.Time, bool)`

GetEndAtOk returns a tuple with the EndAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEndAt

`func (o *Highlight) SetEndAt(v time.Time)`

SetEndAt sets EndAt field to given value.


### GetCreatedAt

`func (o *Highlight) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Highlight) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Highlight) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *Highlight) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Highlight) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Highlight) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


