# ReportCohort

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]ReportCohortEntry**](ReportCohortEntry.md) |  | 
**Total** | **int32** |  | 

## Methods

### NewReportCohort

`func NewReportCohort(results []ReportCohortEntry, total int32, ) *ReportCohort`

NewReportCohort instantiates a new ReportCohort object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReportCohortWithDefaults

`func NewReportCohortWithDefaults() *ReportCohort`

NewReportCohortWithDefaults instantiates a new ReportCohort object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *ReportCohort) GetResults() []ReportCohortEntry`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *ReportCohort) GetResultsOk() (*[]ReportCohortEntry, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *ReportCohort) SetResults(v []ReportCohortEntry)`

SetResults sets Results field to given value.


### GetTotal

`func (o *ReportCohort) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *ReportCohort) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *ReportCohort) SetTotal(v int32)`

SetTotal sets Total field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


