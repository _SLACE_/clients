# CampaignTargetChannelsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelId** | **string** |  | 
**InteractionId** | **string** |  | 
**Main** | Pointer to **bool** |  | [optional] 

## Methods

### NewCampaignTargetChannelsInner

`func NewCampaignTargetChannelsInner(channelId string, interactionId string, ) *CampaignTargetChannelsInner`

NewCampaignTargetChannelsInner instantiates a new CampaignTargetChannelsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignTargetChannelsInnerWithDefaults

`func NewCampaignTargetChannelsInnerWithDefaults() *CampaignTargetChannelsInner`

NewCampaignTargetChannelsInnerWithDefaults instantiates a new CampaignTargetChannelsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetChannelId

`func (o *CampaignTargetChannelsInner) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CampaignTargetChannelsInner) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CampaignTargetChannelsInner) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetInteractionId

`func (o *CampaignTargetChannelsInner) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *CampaignTargetChannelsInner) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *CampaignTargetChannelsInner) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.


### GetMain

`func (o *CampaignTargetChannelsInner) GetMain() bool`

GetMain returns the Main field if non-nil, zero value otherwise.

### GetMainOk

`func (o *CampaignTargetChannelsInner) GetMainOk() (*bool, bool)`

GetMainOk returns a tuple with the Main field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMain

`func (o *CampaignTargetChannelsInner) SetMain(v bool)`

SetMain sets Main field to given value.

### HasMain

`func (o *CampaignTargetChannelsInner) HasMain() bool`

HasMain returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


