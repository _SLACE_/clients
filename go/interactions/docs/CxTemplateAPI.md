# \CxTemplateAPI

All URIs are relative to *https://interactions.slace.dev/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteOrganizationsOrganizationIdCxTemplatesId**](CxTemplateAPI.md#DeleteOrganizationsOrganizationIdCxTemplatesId) | **Delete** /organizations/{organization_id}/cx-templates/{id} | Delete cx template
[**DeleteOrganizationsOrganizationIdCxTemplatesInstallationsId**](CxTemplateAPI.md#DeleteOrganizationsOrganizationIdCxTemplatesInstallationsId) | **Delete** /organizations/{organization_id}/cx-templates-installations/{installation_id} | Delete installation
[**GetOrganizationsOrganizationIdCxTemplatesGlobalList**](CxTemplateAPI.md#GetOrganizationsOrganizationIdCxTemplatesGlobalList) | **Get** /cx-templates | Get list of global cx templates
[**GetOrganizationsOrganizationIdCxTemplatesId**](CxTemplateAPI.md#GetOrganizationsOrganizationIdCxTemplatesId) | **Get** /organizations/{organization_id}/cx-templates/{id} | Get one cx template
[**GetOrganizationsOrganizationIdCxTemplatesInstallations**](CxTemplateAPI.md#GetOrganizationsOrganizationIdCxTemplatesInstallations) | **Get** /organizations/{organization_id}/cx-templates-installations | Get list of installations
[**GetOrganizationsOrganizationIdCxTemplatesInstallationsId**](CxTemplateAPI.md#GetOrganizationsOrganizationIdCxTemplatesInstallationsId) | **Get** /organizations/{organization_id}/cx-templates-installations/{installation_id} | Get installation by id
[**GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks**](CxTemplateAPI.md#GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks) | **Get** /organizations/{organization_id}/cx-templates-installations/{installation_id}/tasks | Get installation tasks
[**PostOrganizationsOrganizationIdCxTemplatesGlobalIdInstallations**](CxTemplateAPI.md#PostOrganizationsOrganizationIdCxTemplatesGlobalIdInstallations) | **Post** /cx-templates/{id}/installations | Init installation for cx template
[**PostOrganizationsOrganizationIdCxTemplatesInstallationsIdFinish**](CxTemplateAPI.md#PostOrganizationsOrganizationIdCxTemplatesInstallationsIdFinish) | **Post** /organizations/{organization_id}/cx-templates-installations/{installation_id}/finish | Finish installation
[**PutOrganizationsOrganizationIdCxTemplatesId**](CxTemplateAPI.md#PutOrganizationsOrganizationIdCxTemplatesId) | **Put** /organizations/{organization_id}/cx-templates/{id} | Update one cx template
[**PutOrganizationsOrganizationIdCxTemplatesIdStatus**](CxTemplateAPI.md#PutOrganizationsOrganizationIdCxTemplatesIdStatus) | **Put** /organizations/{organization_id}/cx-templates/{id}/status | Update status of cx template
[**PutOrganizationsOrganizationIdCxTemplatesInstallationsId**](CxTemplateAPI.md#PutOrganizationsOrganizationIdCxTemplatesInstallationsId) | **Put** /organizations/{organization_id}/cx-templates-installations/{installation_id} | Update installation



## DeleteOrganizationsOrganizationIdCxTemplatesId

> DeleteOrganizationsOrganizationIdCxTemplatesId(ctx, organizationId, id).Execute()

Delete cx template

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.CxTemplateAPI.DeleteOrganizationsOrganizationIdCxTemplatesId(context.Background(), organizationId, id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplateAPI.DeleteOrganizationsOrganizationIdCxTemplatesId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteOrganizationsOrganizationIdCxTemplatesIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteOrganizationsOrganizationIdCxTemplatesInstallationsId

> DeleteOrganizationsOrganizationIdCxTemplatesInstallationsId(ctx, organizationId, installationId).Execute()

Delete installation

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    installationId := "installationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.CxTemplateAPI.DeleteOrganizationsOrganizationIdCxTemplatesInstallationsId(context.Background(), organizationId, installationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplateAPI.DeleteOrganizationsOrganizationIdCxTemplatesInstallationsId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**installationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteOrganizationsOrganizationIdCxTemplatesInstallationsIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationsOrganizationIdCxTemplatesGlobalList

> GetOrganizationsOrganizationIdCxTemplatesGlobalList200Response GetOrganizationsOrganizationIdCxTemplatesGlobalList(ctx).Tags(tags).Languages(languages).Formality(formality).Global(global).Shared(shared).Execute()

Get list of global cx templates

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    tags := []string{"Inner_example"} // []string | Tags filter (optional)
    languages := []string{"Inner_example"} // []string | Languages filter (optional)
    formality := "formality_example" // string |  (optional)
    global := true // bool |  (optional)
    shared := true // bool |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesGlobalList(context.Background()).Tags(tags).Languages(languages).Formality(formality).Global(global).Shared(shared).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesGlobalList``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationsOrganizationIdCxTemplatesGlobalList`: GetOrganizationsOrganizationIdCxTemplatesGlobalList200Response
    fmt.Fprintf(os.Stdout, "Response from `CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesGlobalList`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationsOrganizationIdCxTemplatesGlobalListRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tags** | **[]string** | Tags filter | 
 **languages** | **[]string** | Languages filter | 
 **formality** | **string** |  | 
 **global** | **bool** |  | 
 **shared** | **bool** |  | 

### Return type

[**GetOrganizationsOrganizationIdCxTemplatesGlobalList200Response**](GetOrganizationsOrganizationIdCxTemplatesGlobalList200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationsOrganizationIdCxTemplatesId

> CxTemplate GetOrganizationsOrganizationIdCxTemplatesId(ctx, organizationId, id).Execute()

Get one cx template

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesId(context.Background(), organizationId, id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationsOrganizationIdCxTemplatesId`: CxTemplate
    fmt.Fprintf(os.Stdout, "Response from `CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationsOrganizationIdCxTemplatesIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**CxTemplate**](CxTemplate.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationsOrganizationIdCxTemplatesInstallations

> GetOrganizationsOrganizationIdCxTemplatesInstallations200Response GetOrganizationsOrganizationIdCxTemplatesInstallations(ctx, organizationId).Statuses(statuses).TemplateIds(templateIds).Execute()

Get list of installations

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    statuses := []string{"Inner_example"} // []string |  (optional)
    templateIds := []string{"Inner_example"} // []string | template ids (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesInstallations(context.Background(), organizationId).Statuses(statuses).TemplateIds(templateIds).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesInstallations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationsOrganizationIdCxTemplatesInstallations`: GetOrganizationsOrganizationIdCxTemplatesInstallations200Response
    fmt.Fprintf(os.Stdout, "Response from `CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesInstallations`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationsOrganizationIdCxTemplatesInstallationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **statuses** | **[]string** |  | 
 **templateIds** | **[]string** | template ids | 

### Return type

[**GetOrganizationsOrganizationIdCxTemplatesInstallations200Response**](GetOrganizationsOrganizationIdCxTemplatesInstallations200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationsOrganizationIdCxTemplatesInstallationsId

> CxTemplateInstallation GetOrganizationsOrganizationIdCxTemplatesInstallationsId(ctx, organizationId, installationId).Execute()

Get installation by id

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    installationId := "installationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesInstallationsId(context.Background(), organizationId, installationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesInstallationsId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationsOrganizationIdCxTemplatesInstallationsId`: CxTemplateInstallation
    fmt.Fprintf(os.Stdout, "Response from `CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesInstallationsId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**installationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationsOrganizationIdCxTemplatesInstallationsIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**CxTemplateInstallation**](CxTemplateInstallation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks

> GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks(ctx, organizationId, installationId).InteractionId(interactionId).Execute()

Get installation tasks

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    installationId := "installationId_example" // string | 
    interactionId := "interactionId_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks(context.Background(), organizationId, installationId).InteractionId(interactionId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks`: GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response
    fmt.Fprintf(os.Stdout, "Response from `CxTemplateAPI.GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**installationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasksRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **interactionId** | **string** |  | 

### Return type

[**GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response**](GetOrganizationsOrganizationIdCxTemplatesInstallationsIdTasks200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostOrganizationsOrganizationIdCxTemplatesGlobalIdInstallations

> CxTemplateInstallation PostOrganizationsOrganizationIdCxTemplatesGlobalIdInstallations(ctx, id).CxTemplateInstallationCreateData(cxTemplateInstallationCreateData).Execute()

Init installation for cx template

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    id := "id_example" // string | 
    cxTemplateInstallationCreateData := *openapiclient.NewCxTemplateInstallationCreateData() // CxTemplateInstallationCreateData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplateAPI.PostOrganizationsOrganizationIdCxTemplatesGlobalIdInstallations(context.Background(), id).CxTemplateInstallationCreateData(cxTemplateInstallationCreateData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplateAPI.PostOrganizationsOrganizationIdCxTemplatesGlobalIdInstallations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PostOrganizationsOrganizationIdCxTemplatesGlobalIdInstallations`: CxTemplateInstallation
    fmt.Fprintf(os.Stdout, "Response from `CxTemplateAPI.PostOrganizationsOrganizationIdCxTemplatesGlobalIdInstallations`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPostOrganizationsOrganizationIdCxTemplatesGlobalIdInstallationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **cxTemplateInstallationCreateData** | [**CxTemplateInstallationCreateData**](CxTemplateInstallationCreateData.md) |  | 

### Return type

[**CxTemplateInstallation**](CxTemplateInstallation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostOrganizationsOrganizationIdCxTemplatesInstallationsIdFinish

> PostOrganizationsOrganizationIdCxTemplatesInstallationsIdFinish(ctx, organizationId, installationId).Execute()

Finish installation

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    installationId := "installationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.CxTemplateAPI.PostOrganizationsOrganizationIdCxTemplatesInstallationsIdFinish(context.Background(), organizationId, installationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplateAPI.PostOrganizationsOrganizationIdCxTemplatesInstallationsIdFinish``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**installationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPostOrganizationsOrganizationIdCxTemplatesInstallationsIdFinishRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PutOrganizationsOrganizationIdCxTemplatesId

> CxTemplate PutOrganizationsOrganizationIdCxTemplatesId(ctx, organizationId, id).CxTemplateMutableData(cxTemplateMutableData).Execute()

Update one cx template

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    id := "id_example" // string | 
    cxTemplateMutableData := *openapiclient.NewCxTemplateMutableData() // CxTemplateMutableData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplateAPI.PutOrganizationsOrganizationIdCxTemplatesId(context.Background(), organizationId, id).CxTemplateMutableData(cxTemplateMutableData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplateAPI.PutOrganizationsOrganizationIdCxTemplatesId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PutOrganizationsOrganizationIdCxTemplatesId`: CxTemplate
    fmt.Fprintf(os.Stdout, "Response from `CxTemplateAPI.PutOrganizationsOrganizationIdCxTemplatesId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPutOrganizationsOrganizationIdCxTemplatesIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **cxTemplateMutableData** | [**CxTemplateMutableData**](CxTemplateMutableData.md) |  | 

### Return type

[**CxTemplate**](CxTemplate.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PutOrganizationsOrganizationIdCxTemplatesIdStatus

> CxTemplate PutOrganizationsOrganizationIdCxTemplatesIdStatus(ctx, organizationId, id).PutOrganizationsOrganizationIdCxTemplatesIdStatusRequest(putOrganizationsOrganizationIdCxTemplatesIdStatusRequest).Execute()

Update status of cx template

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    id := "id_example" // string | 
    putOrganizationsOrganizationIdCxTemplatesIdStatusRequest := *openapiclient.NewPutOrganizationsOrganizationIdCxTemplatesIdStatusRequest("Status_example") // PutOrganizationsOrganizationIdCxTemplatesIdStatusRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplateAPI.PutOrganizationsOrganizationIdCxTemplatesIdStatus(context.Background(), organizationId, id).PutOrganizationsOrganizationIdCxTemplatesIdStatusRequest(putOrganizationsOrganizationIdCxTemplatesIdStatusRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplateAPI.PutOrganizationsOrganizationIdCxTemplatesIdStatus``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PutOrganizationsOrganizationIdCxTemplatesIdStatus`: CxTemplate
    fmt.Fprintf(os.Stdout, "Response from `CxTemplateAPI.PutOrganizationsOrganizationIdCxTemplatesIdStatus`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPutOrganizationsOrganizationIdCxTemplatesIdStatusRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **putOrganizationsOrganizationIdCxTemplatesIdStatusRequest** | [**PutOrganizationsOrganizationIdCxTemplatesIdStatusRequest**](PutOrganizationsOrganizationIdCxTemplatesIdStatusRequest.md) |  | 

### Return type

[**CxTemplate**](CxTemplate.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PutOrganizationsOrganizationIdCxTemplatesInstallationsId

> CxTemplateInstallation PutOrganizationsOrganizationIdCxTemplatesInstallationsId(ctx, organizationId, installationId).CxTemplateInstallationMutableData(cxTemplateInstallationMutableData).Execute()

Update installation

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    installationId := "installationId_example" // string | 
    cxTemplateInstallationMutableData := *openapiclient.NewCxTemplateInstallationMutableData() // CxTemplateInstallationMutableData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CxTemplateAPI.PutOrganizationsOrganizationIdCxTemplatesInstallationsId(context.Background(), organizationId, installationId).CxTemplateInstallationMutableData(cxTemplateInstallationMutableData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CxTemplateAPI.PutOrganizationsOrganizationIdCxTemplatesInstallationsId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PutOrganizationsOrganizationIdCxTemplatesInstallationsId`: CxTemplateInstallation
    fmt.Fprintf(os.Stdout, "Response from `CxTemplateAPI.PutOrganizationsOrganizationIdCxTemplatesInstallationsId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**installationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPutOrganizationsOrganizationIdCxTemplatesInstallationsIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **cxTemplateInstallationMutableData** | [**CxTemplateInstallationMutableData**](CxTemplateInstallationMutableData.md) |  | 

### Return type

[**CxTemplateInstallation**](CxTemplateInstallation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

