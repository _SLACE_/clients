# GetScPermissions200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Total** | Pointer to **int32** |  | [optional] 
**Results** | Pointer to [**[]ScPermission**](ScPermission.md) |  | [optional] 

## Methods

### NewGetScPermissions200Response

`func NewGetScPermissions200Response() *GetScPermissions200Response`

NewGetScPermissions200Response instantiates a new GetScPermissions200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetScPermissions200ResponseWithDefaults

`func NewGetScPermissions200ResponseWithDefaults() *GetScPermissions200Response`

NewGetScPermissions200ResponseWithDefaults instantiates a new GetScPermissions200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotal

`func (o *GetScPermissions200Response) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *GetScPermissions200Response) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *GetScPermissions200Response) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *GetScPermissions200Response) HasTotal() bool`

HasTotal returns a boolean if a field has been set.

### GetResults

`func (o *GetScPermissions200Response) GetResults() []ScPermission`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *GetScPermissions200Response) GetResultsOk() (*[]ScPermission, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *GetScPermissions200Response) SetResults(v []ScPermission)`

SetResults sets Results field to given value.

### HasResults

`func (o *GetScPermissions200Response) HasResults() bool`

HasResults returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


