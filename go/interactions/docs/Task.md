# Task

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**OrganizationId** | **string** |  | 
**InteractionId** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**ScriptId** | Pointer to **string** |  | [optional] 
**Text** | **string** |  | 
**Title** | **string** |  | 
**Type** | **string** |  | 
**Blocker** | **bool** |  | 
**CreatedAt** | Pointer to **string** |  | [optional] 
**UpdatedAt** | Pointer to **string** |  | [optional] 
**OwnedBy** | Pointer to **string** |  | [optional] 
**DestinationLink** | Pointer to **string** |  | [optional] 
**Interval** | Pointer to **string** |  | [optional] 
**ScheduleType** | Pointer to **string** |  | [optional] 
**Action** | Pointer to **string** |  | [optional] 
**Usages** | Pointer to **[]string** |  | [optional] 
**Element** | Pointer to [**TaskElement**](TaskElement.md) |  | [optional] 
**TasksCompletions** | Pointer to [**[]TaskCompletion**](TaskCompletion.md) |  | [optional] 

## Methods

### NewTask

`func NewTask(id string, organizationId string, text string, title string, type_ string, blocker bool, ) *Task`

NewTask instantiates a new Task object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTaskWithDefaults

`func NewTaskWithDefaults() *Task`

NewTaskWithDefaults instantiates a new Task object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Task) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Task) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Task) SetId(v string)`

SetId sets Id field to given value.


### GetOrganizationId

`func (o *Task) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *Task) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *Task) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetInteractionId

`func (o *Task) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *Task) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *Task) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *Task) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetChannelId

`func (o *Task) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *Task) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *Task) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *Task) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetScriptId

`func (o *Task) GetScriptId() string`

GetScriptId returns the ScriptId field if non-nil, zero value otherwise.

### GetScriptIdOk

`func (o *Task) GetScriptIdOk() (*string, bool)`

GetScriptIdOk returns a tuple with the ScriptId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScriptId

`func (o *Task) SetScriptId(v string)`

SetScriptId sets ScriptId field to given value.

### HasScriptId

`func (o *Task) HasScriptId() bool`

HasScriptId returns a boolean if a field has been set.

### GetText

`func (o *Task) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *Task) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *Task) SetText(v string)`

SetText sets Text field to given value.


### GetTitle

`func (o *Task) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *Task) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *Task) SetTitle(v string)`

SetTitle sets Title field to given value.


### GetType

`func (o *Task) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Task) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Task) SetType(v string)`

SetType sets Type field to given value.


### GetBlocker

`func (o *Task) GetBlocker() bool`

GetBlocker returns the Blocker field if non-nil, zero value otherwise.

### GetBlockerOk

`func (o *Task) GetBlockerOk() (*bool, bool)`

GetBlockerOk returns a tuple with the Blocker field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlocker

`func (o *Task) SetBlocker(v bool)`

SetBlocker sets Blocker field to given value.


### GetCreatedAt

`func (o *Task) GetCreatedAt() string`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Task) GetCreatedAtOk() (*string, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Task) SetCreatedAt(v string)`

SetCreatedAt sets CreatedAt field to given value.

### HasCreatedAt

`func (o *Task) HasCreatedAt() bool`

HasCreatedAt returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *Task) GetUpdatedAt() string`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Task) GetUpdatedAtOk() (*string, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Task) SetUpdatedAt(v string)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *Task) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.

### GetOwnedBy

`func (o *Task) GetOwnedBy() string`

GetOwnedBy returns the OwnedBy field if non-nil, zero value otherwise.

### GetOwnedByOk

`func (o *Task) GetOwnedByOk() (*string, bool)`

GetOwnedByOk returns a tuple with the OwnedBy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOwnedBy

`func (o *Task) SetOwnedBy(v string)`

SetOwnedBy sets OwnedBy field to given value.

### HasOwnedBy

`func (o *Task) HasOwnedBy() bool`

HasOwnedBy returns a boolean if a field has been set.

### GetDestinationLink

`func (o *Task) GetDestinationLink() string`

GetDestinationLink returns the DestinationLink field if non-nil, zero value otherwise.

### GetDestinationLinkOk

`func (o *Task) GetDestinationLinkOk() (*string, bool)`

GetDestinationLinkOk returns a tuple with the DestinationLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDestinationLink

`func (o *Task) SetDestinationLink(v string)`

SetDestinationLink sets DestinationLink field to given value.

### HasDestinationLink

`func (o *Task) HasDestinationLink() bool`

HasDestinationLink returns a boolean if a field has been set.

### GetInterval

`func (o *Task) GetInterval() string`

GetInterval returns the Interval field if non-nil, zero value otherwise.

### GetIntervalOk

`func (o *Task) GetIntervalOk() (*string, bool)`

GetIntervalOk returns a tuple with the Interval field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInterval

`func (o *Task) SetInterval(v string)`

SetInterval sets Interval field to given value.

### HasInterval

`func (o *Task) HasInterval() bool`

HasInterval returns a boolean if a field has been set.

### GetScheduleType

`func (o *Task) GetScheduleType() string`

GetScheduleType returns the ScheduleType field if non-nil, zero value otherwise.

### GetScheduleTypeOk

`func (o *Task) GetScheduleTypeOk() (*string, bool)`

GetScheduleTypeOk returns a tuple with the ScheduleType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScheduleType

`func (o *Task) SetScheduleType(v string)`

SetScheduleType sets ScheduleType field to given value.

### HasScheduleType

`func (o *Task) HasScheduleType() bool`

HasScheduleType returns a boolean if a field has been set.

### GetAction

`func (o *Task) GetAction() string`

GetAction returns the Action field if non-nil, zero value otherwise.

### GetActionOk

`func (o *Task) GetActionOk() (*string, bool)`

GetActionOk returns a tuple with the Action field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAction

`func (o *Task) SetAction(v string)`

SetAction sets Action field to given value.

### HasAction

`func (o *Task) HasAction() bool`

HasAction returns a boolean if a field has been set.

### GetUsages

`func (o *Task) GetUsages() []string`

GetUsages returns the Usages field if non-nil, zero value otherwise.

### GetUsagesOk

`func (o *Task) GetUsagesOk() (*[]string, bool)`

GetUsagesOk returns a tuple with the Usages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsages

`func (o *Task) SetUsages(v []string)`

SetUsages sets Usages field to given value.

### HasUsages

`func (o *Task) HasUsages() bool`

HasUsages returns a boolean if a field has been set.

### GetElement

`func (o *Task) GetElement() TaskElement`

GetElement returns the Element field if non-nil, zero value otherwise.

### GetElementOk

`func (o *Task) GetElementOk() (*TaskElement, bool)`

GetElementOk returns a tuple with the Element field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetElement

`func (o *Task) SetElement(v TaskElement)`

SetElement sets Element field to given value.

### HasElement

`func (o *Task) HasElement() bool`

HasElement returns a boolean if a field has been set.

### GetTasksCompletions

`func (o *Task) GetTasksCompletions() []TaskCompletion`

GetTasksCompletions returns the TasksCompletions field if non-nil, zero value otherwise.

### GetTasksCompletionsOk

`func (o *Task) GetTasksCompletionsOk() (*[]TaskCompletion, bool)`

GetTasksCompletionsOk returns a tuple with the TasksCompletions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTasksCompletions

`func (o *Task) SetTasksCompletions(v []TaskCompletion)`

SetTasksCompletions sets TasksCompletions field to given value.

### HasTasksCompletions

`func (o *Task) HasTasksCompletions() bool`

HasTasksCompletions returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


