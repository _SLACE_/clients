# CxTemplateInteractionsInnerPreviewUrlsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | Pointer to **string** |  | [optional] 
**Language** | Pointer to **string** |  | [optional] 

## Methods

### NewCxTemplateInteractionsInnerPreviewUrlsInner

`func NewCxTemplateInteractionsInnerPreviewUrlsInner() *CxTemplateInteractionsInnerPreviewUrlsInner`

NewCxTemplateInteractionsInnerPreviewUrlsInner instantiates a new CxTemplateInteractionsInnerPreviewUrlsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateInteractionsInnerPreviewUrlsInnerWithDefaults

`func NewCxTemplateInteractionsInnerPreviewUrlsInnerWithDefaults() *CxTemplateInteractionsInnerPreviewUrlsInner`

NewCxTemplateInteractionsInnerPreviewUrlsInnerWithDefaults instantiates a new CxTemplateInteractionsInnerPreviewUrlsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUrl

`func (o *CxTemplateInteractionsInnerPreviewUrlsInner) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *CxTemplateInteractionsInnerPreviewUrlsInner) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *CxTemplateInteractionsInnerPreviewUrlsInner) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *CxTemplateInteractionsInnerPreviewUrlsInner) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetLanguage

`func (o *CxTemplateInteractionsInnerPreviewUrlsInner) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *CxTemplateInteractionsInnerPreviewUrlsInner) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *CxTemplateInteractionsInnerPreviewUrlsInner) SetLanguage(v string)`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *CxTemplateInteractionsInnerPreviewUrlsInner) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


