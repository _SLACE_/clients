# Operator

## Enum


* `AND` (value: `"and"`)

* `OR` (value: `"or"`)

* `EQ` (value: `"eq"`)

* `NEQ` (value: `"neq"`)

* `EMPTY` (value: `"empty"`)

* `LT` (value: `"lt"`)

* `LTE` (value: `"lte"`)

* `GT` (value: `"gt"`)

* `GTE` (value: `"gte"`)

* `CT` (value: `"ct"`)

* `ALL_OF` (value: `"allOf"`)

* `NONE_OF` (value: `"noneOf"`)

* `ONE_OF` (value: `"oneOf"`)

* `STARTS_WITH` (value: `"startsWith"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


