# CampaignRecurringConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Frequency** | **string** |  | 
**Until** | **time.Time** |  | 
**ExcludeWeekdays** | Pointer to **[]int32** |  | [optional] 
**RescheduleExcluded** | Pointer to **bool** |  | [optional] 
**FrequencyValue** | **int32** |  | 
**UntilOccurences** | Pointer to **int32** |  | [optional] 
**CanaryConfig** | Pointer to [**CampaignRecurringConfigCanaryConfig**](CampaignRecurringConfigCanaryConfig.md) |  | [optional] 

## Methods

### NewCampaignRecurringConfig

`func NewCampaignRecurringConfig(frequency string, until time.Time, frequencyValue int32, ) *CampaignRecurringConfig`

NewCampaignRecurringConfig instantiates a new CampaignRecurringConfig object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCampaignRecurringConfigWithDefaults

`func NewCampaignRecurringConfigWithDefaults() *CampaignRecurringConfig`

NewCampaignRecurringConfigWithDefaults instantiates a new CampaignRecurringConfig object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFrequency

`func (o *CampaignRecurringConfig) GetFrequency() string`

GetFrequency returns the Frequency field if non-nil, zero value otherwise.

### GetFrequencyOk

`func (o *CampaignRecurringConfig) GetFrequencyOk() (*string, bool)`

GetFrequencyOk returns a tuple with the Frequency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFrequency

`func (o *CampaignRecurringConfig) SetFrequency(v string)`

SetFrequency sets Frequency field to given value.


### GetUntil

`func (o *CampaignRecurringConfig) GetUntil() time.Time`

GetUntil returns the Until field if non-nil, zero value otherwise.

### GetUntilOk

`func (o *CampaignRecurringConfig) GetUntilOk() (*time.Time, bool)`

GetUntilOk returns a tuple with the Until field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUntil

`func (o *CampaignRecurringConfig) SetUntil(v time.Time)`

SetUntil sets Until field to given value.


### GetExcludeWeekdays

`func (o *CampaignRecurringConfig) GetExcludeWeekdays() []int32`

GetExcludeWeekdays returns the ExcludeWeekdays field if non-nil, zero value otherwise.

### GetExcludeWeekdaysOk

`func (o *CampaignRecurringConfig) GetExcludeWeekdaysOk() (*[]int32, bool)`

GetExcludeWeekdaysOk returns a tuple with the ExcludeWeekdays field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExcludeWeekdays

`func (o *CampaignRecurringConfig) SetExcludeWeekdays(v []int32)`

SetExcludeWeekdays sets ExcludeWeekdays field to given value.

### HasExcludeWeekdays

`func (o *CampaignRecurringConfig) HasExcludeWeekdays() bool`

HasExcludeWeekdays returns a boolean if a field has been set.

### GetRescheduleExcluded

`func (o *CampaignRecurringConfig) GetRescheduleExcluded() bool`

GetRescheduleExcluded returns the RescheduleExcluded field if non-nil, zero value otherwise.

### GetRescheduleExcludedOk

`func (o *CampaignRecurringConfig) GetRescheduleExcludedOk() (*bool, bool)`

GetRescheduleExcludedOk returns a tuple with the RescheduleExcluded field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRescheduleExcluded

`func (o *CampaignRecurringConfig) SetRescheduleExcluded(v bool)`

SetRescheduleExcluded sets RescheduleExcluded field to given value.

### HasRescheduleExcluded

`func (o *CampaignRecurringConfig) HasRescheduleExcluded() bool`

HasRescheduleExcluded returns a boolean if a field has been set.

### GetFrequencyValue

`func (o *CampaignRecurringConfig) GetFrequencyValue() int32`

GetFrequencyValue returns the FrequencyValue field if non-nil, zero value otherwise.

### GetFrequencyValueOk

`func (o *CampaignRecurringConfig) GetFrequencyValueOk() (*int32, bool)`

GetFrequencyValueOk returns a tuple with the FrequencyValue field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFrequencyValue

`func (o *CampaignRecurringConfig) SetFrequencyValue(v int32)`

SetFrequencyValue sets FrequencyValue field to given value.


### GetUntilOccurences

`func (o *CampaignRecurringConfig) GetUntilOccurences() int32`

GetUntilOccurences returns the UntilOccurences field if non-nil, zero value otherwise.

### GetUntilOccurencesOk

`func (o *CampaignRecurringConfig) GetUntilOccurencesOk() (*int32, bool)`

GetUntilOccurencesOk returns a tuple with the UntilOccurences field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUntilOccurences

`func (o *CampaignRecurringConfig) SetUntilOccurences(v int32)`

SetUntilOccurences sets UntilOccurences field to given value.

### HasUntilOccurences

`func (o *CampaignRecurringConfig) HasUntilOccurences() bool`

HasUntilOccurences returns a boolean if a field has been set.

### GetCanaryConfig

`func (o *CampaignRecurringConfig) GetCanaryConfig() CampaignRecurringConfigCanaryConfig`

GetCanaryConfig returns the CanaryConfig field if non-nil, zero value otherwise.

### GetCanaryConfigOk

`func (o *CampaignRecurringConfig) GetCanaryConfigOk() (*CampaignRecurringConfigCanaryConfig, bool)`

GetCanaryConfigOk returns a tuple with the CanaryConfig field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCanaryConfig

`func (o *CampaignRecurringConfig) SetCanaryConfig(v CampaignRecurringConfigCanaryConfig)`

SetCanaryConfig sets CanaryConfig field to given value.

### HasCanaryConfig

`func (o *CampaignRecurringConfig) HasCanaryConfig() bool`

HasCanaryConfig returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


