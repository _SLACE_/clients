# InteractionWithMetrics

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**ChannelId** | **string** |  | 
**OrganizationId** | **string** |  | 
**Name** | **string** |  | 
**TriggerTemplate** | **string** |  | 
**State** | [**State**](State.md) |  | 
**LiveFrom** | Pointer to **time.Time** |  | [optional] 
**LiveTo** | Pointer to **time.Time** |  | [optional] 
**Languages** | Pointer to **[]string** |  | [optional] 
**Messengers** | Pointer to **[]string** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 
**Interactions** | **int32** |  | 
**UniqueContacts** | **int32** |  | 
**Consents** | **int32** |  | 
**ChatsStarted** | **int32** |  | 
**MainActions** | **int32** |  | 
**Investments** | **int32** |  | 

## Methods

### NewInteractionWithMetrics

`func NewInteractionWithMetrics(id string, channelId string, organizationId string, name string, triggerTemplate string, state State, interactions int32, uniqueContacts int32, consents int32, chatsStarted int32, mainActions int32, investments int32, ) *InteractionWithMetrics`

NewInteractionWithMetrics instantiates a new InteractionWithMetrics object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInteractionWithMetricsWithDefaults

`func NewInteractionWithMetricsWithDefaults() *InteractionWithMetrics`

NewInteractionWithMetricsWithDefaults instantiates a new InteractionWithMetrics object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *InteractionWithMetrics) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *InteractionWithMetrics) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *InteractionWithMetrics) SetId(v string)`

SetId sets Id field to given value.


### GetChannelId

`func (o *InteractionWithMetrics) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *InteractionWithMetrics) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *InteractionWithMetrics) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetOrganizationId

`func (o *InteractionWithMetrics) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *InteractionWithMetrics) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *InteractionWithMetrics) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetName

`func (o *InteractionWithMetrics) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *InteractionWithMetrics) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *InteractionWithMetrics) SetName(v string)`

SetName sets Name field to given value.


### GetTriggerTemplate

`func (o *InteractionWithMetrics) GetTriggerTemplate() string`

GetTriggerTemplate returns the TriggerTemplate field if non-nil, zero value otherwise.

### GetTriggerTemplateOk

`func (o *InteractionWithMetrics) GetTriggerTemplateOk() (*string, bool)`

GetTriggerTemplateOk returns a tuple with the TriggerTemplate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTriggerTemplate

`func (o *InteractionWithMetrics) SetTriggerTemplate(v string)`

SetTriggerTemplate sets TriggerTemplate field to given value.


### GetState

`func (o *InteractionWithMetrics) GetState() State`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *InteractionWithMetrics) GetStateOk() (*State, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *InteractionWithMetrics) SetState(v State)`

SetState sets State field to given value.


### GetLiveFrom

`func (o *InteractionWithMetrics) GetLiveFrom() time.Time`

GetLiveFrom returns the LiveFrom field if non-nil, zero value otherwise.

### GetLiveFromOk

`func (o *InteractionWithMetrics) GetLiveFromOk() (*time.Time, bool)`

GetLiveFromOk returns a tuple with the LiveFrom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLiveFrom

`func (o *InteractionWithMetrics) SetLiveFrom(v time.Time)`

SetLiveFrom sets LiveFrom field to given value.

### HasLiveFrom

`func (o *InteractionWithMetrics) HasLiveFrom() bool`

HasLiveFrom returns a boolean if a field has been set.

### GetLiveTo

`func (o *InteractionWithMetrics) GetLiveTo() time.Time`

GetLiveTo returns the LiveTo field if non-nil, zero value otherwise.

### GetLiveToOk

`func (o *InteractionWithMetrics) GetLiveToOk() (*time.Time, bool)`

GetLiveToOk returns a tuple with the LiveTo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLiveTo

`func (o *InteractionWithMetrics) SetLiveTo(v time.Time)`

SetLiveTo sets LiveTo field to given value.

### HasLiveTo

`func (o *InteractionWithMetrics) HasLiveTo() bool`

HasLiveTo returns a boolean if a field has been set.

### GetLanguages

`func (o *InteractionWithMetrics) GetLanguages() []string`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *InteractionWithMetrics) GetLanguagesOk() (*[]string, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *InteractionWithMetrics) SetLanguages(v []string)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *InteractionWithMetrics) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetMessengers

`func (o *InteractionWithMetrics) GetMessengers() []string`

GetMessengers returns the Messengers field if non-nil, zero value otherwise.

### GetMessengersOk

`func (o *InteractionWithMetrics) GetMessengersOk() (*[]string, bool)`

GetMessengersOk returns a tuple with the Messengers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengers

`func (o *InteractionWithMetrics) SetMessengers(v []string)`

SetMessengers sets Messengers field to given value.

### HasMessengers

`func (o *InteractionWithMetrics) HasMessengers() bool`

HasMessengers returns a boolean if a field has been set.

### GetTags

`func (o *InteractionWithMetrics) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *InteractionWithMetrics) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *InteractionWithMetrics) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *InteractionWithMetrics) HasTags() bool`

HasTags returns a boolean if a field has been set.

### GetInteractions

`func (o *InteractionWithMetrics) GetInteractions() int32`

GetInteractions returns the Interactions field if non-nil, zero value otherwise.

### GetInteractionsOk

`func (o *InteractionWithMetrics) GetInteractionsOk() (*int32, bool)`

GetInteractionsOk returns a tuple with the Interactions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractions

`func (o *InteractionWithMetrics) SetInteractions(v int32)`

SetInteractions sets Interactions field to given value.


### GetUniqueContacts

`func (o *InteractionWithMetrics) GetUniqueContacts() int32`

GetUniqueContacts returns the UniqueContacts field if non-nil, zero value otherwise.

### GetUniqueContactsOk

`func (o *InteractionWithMetrics) GetUniqueContactsOk() (*int32, bool)`

GetUniqueContactsOk returns a tuple with the UniqueContacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUniqueContacts

`func (o *InteractionWithMetrics) SetUniqueContacts(v int32)`

SetUniqueContacts sets UniqueContacts field to given value.


### GetConsents

`func (o *InteractionWithMetrics) GetConsents() int32`

GetConsents returns the Consents field if non-nil, zero value otherwise.

### GetConsentsOk

`func (o *InteractionWithMetrics) GetConsentsOk() (*int32, bool)`

GetConsentsOk returns a tuple with the Consents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConsents

`func (o *InteractionWithMetrics) SetConsents(v int32)`

SetConsents sets Consents field to given value.


### GetChatsStarted

`func (o *InteractionWithMetrics) GetChatsStarted() int32`

GetChatsStarted returns the ChatsStarted field if non-nil, zero value otherwise.

### GetChatsStartedOk

`func (o *InteractionWithMetrics) GetChatsStartedOk() (*int32, bool)`

GetChatsStartedOk returns a tuple with the ChatsStarted field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChatsStarted

`func (o *InteractionWithMetrics) SetChatsStarted(v int32)`

SetChatsStarted sets ChatsStarted field to given value.


### GetMainActions

`func (o *InteractionWithMetrics) GetMainActions() int32`

GetMainActions returns the MainActions field if non-nil, zero value otherwise.

### GetMainActionsOk

`func (o *InteractionWithMetrics) GetMainActionsOk() (*int32, bool)`

GetMainActionsOk returns a tuple with the MainActions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMainActions

`func (o *InteractionWithMetrics) SetMainActions(v int32)`

SetMainActions sets MainActions field to given value.


### GetInvestments

`func (o *InteractionWithMetrics) GetInvestments() int32`

GetInvestments returns the Investments field if non-nil, zero value otherwise.

### GetInvestmentsOk

`func (o *InteractionWithMetrics) GetInvestmentsOk() (*int32, bool)`

GetInvestmentsOk returns a tuple with the Investments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInvestments

`func (o *InteractionWithMetrics) SetInvestments(v int32)`

SetInvestments sets Investments field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


