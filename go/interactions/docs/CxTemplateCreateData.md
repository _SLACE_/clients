# CxTemplateCreateData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**HeaderImageUrls** | Pointer to [**[]CxTemplateCreateDataHeaderImageUrlsInner**](CxTemplateCreateDataHeaderImageUrlsInner.md) |  | [optional] 
**Description** | Pointer to **string** |  | [optional] 
**Languages** | Pointer to **[]string** |  | [optional] 
**Integrations** | Pointer to [**CxTemplateCreateDataIntegrations**](CxTemplateCreateDataIntegrations.md) |  | [optional] 
**RequiredMessengers** | Pointer to **[]string** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 
**Interactions** | Pointer to [**[]CxTemplateCreateDataInteractionsInner**](CxTemplateCreateDataInteractionsInner.md) |  | [optional] 
**Global** | Pointer to **bool** |  | [optional] 
**Formality** | Pointer to **string** |  | [optional] 
**PreviewUrls** | Pointer to [**[]CxTemplateCreateDataPreviewUrlsInner**](CxTemplateCreateDataPreviewUrlsInner.md) |  | [optional] 

## Methods

### NewCxTemplateCreateData

`func NewCxTemplateCreateData() *CxTemplateCreateData`

NewCxTemplateCreateData instantiates a new CxTemplateCreateData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateCreateDataWithDefaults

`func NewCxTemplateCreateDataWithDefaults() *CxTemplateCreateData`

NewCxTemplateCreateDataWithDefaults instantiates a new CxTemplateCreateData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *CxTemplateCreateData) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CxTemplateCreateData) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CxTemplateCreateData) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *CxTemplateCreateData) HasName() bool`

HasName returns a boolean if a field has been set.

### GetHeaderImageUrls

`func (o *CxTemplateCreateData) GetHeaderImageUrls() []CxTemplateCreateDataHeaderImageUrlsInner`

GetHeaderImageUrls returns the HeaderImageUrls field if non-nil, zero value otherwise.

### GetHeaderImageUrlsOk

`func (o *CxTemplateCreateData) GetHeaderImageUrlsOk() (*[]CxTemplateCreateDataHeaderImageUrlsInner, bool)`

GetHeaderImageUrlsOk returns a tuple with the HeaderImageUrls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeaderImageUrls

`func (o *CxTemplateCreateData) SetHeaderImageUrls(v []CxTemplateCreateDataHeaderImageUrlsInner)`

SetHeaderImageUrls sets HeaderImageUrls field to given value.

### HasHeaderImageUrls

`func (o *CxTemplateCreateData) HasHeaderImageUrls() bool`

HasHeaderImageUrls returns a boolean if a field has been set.

### GetDescription

`func (o *CxTemplateCreateData) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *CxTemplateCreateData) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *CxTemplateCreateData) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *CxTemplateCreateData) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetLanguages

`func (o *CxTemplateCreateData) GetLanguages() []string`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *CxTemplateCreateData) GetLanguagesOk() (*[]string, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *CxTemplateCreateData) SetLanguages(v []string)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *CxTemplateCreateData) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetIntegrations

`func (o *CxTemplateCreateData) GetIntegrations() CxTemplateCreateDataIntegrations`

GetIntegrations returns the Integrations field if non-nil, zero value otherwise.

### GetIntegrationsOk

`func (o *CxTemplateCreateData) GetIntegrationsOk() (*CxTemplateCreateDataIntegrations, bool)`

GetIntegrationsOk returns a tuple with the Integrations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIntegrations

`func (o *CxTemplateCreateData) SetIntegrations(v CxTemplateCreateDataIntegrations)`

SetIntegrations sets Integrations field to given value.

### HasIntegrations

`func (o *CxTemplateCreateData) HasIntegrations() bool`

HasIntegrations returns a boolean if a field has been set.

### GetRequiredMessengers

`func (o *CxTemplateCreateData) GetRequiredMessengers() []string`

GetRequiredMessengers returns the RequiredMessengers field if non-nil, zero value otherwise.

### GetRequiredMessengersOk

`func (o *CxTemplateCreateData) GetRequiredMessengersOk() (*[]string, bool)`

GetRequiredMessengersOk returns a tuple with the RequiredMessengers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequiredMessengers

`func (o *CxTemplateCreateData) SetRequiredMessengers(v []string)`

SetRequiredMessengers sets RequiredMessengers field to given value.

### HasRequiredMessengers

`func (o *CxTemplateCreateData) HasRequiredMessengers() bool`

HasRequiredMessengers returns a boolean if a field has been set.

### GetTags

`func (o *CxTemplateCreateData) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *CxTemplateCreateData) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *CxTemplateCreateData) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *CxTemplateCreateData) HasTags() bool`

HasTags returns a boolean if a field has been set.

### GetInteractions

`func (o *CxTemplateCreateData) GetInteractions() []CxTemplateCreateDataInteractionsInner`

GetInteractions returns the Interactions field if non-nil, zero value otherwise.

### GetInteractionsOk

`func (o *CxTemplateCreateData) GetInteractionsOk() (*[]CxTemplateCreateDataInteractionsInner, bool)`

GetInteractionsOk returns a tuple with the Interactions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractions

`func (o *CxTemplateCreateData) SetInteractions(v []CxTemplateCreateDataInteractionsInner)`

SetInteractions sets Interactions field to given value.

### HasInteractions

`func (o *CxTemplateCreateData) HasInteractions() bool`

HasInteractions returns a boolean if a field has been set.

### GetGlobal

`func (o *CxTemplateCreateData) GetGlobal() bool`

GetGlobal returns the Global field if non-nil, zero value otherwise.

### GetGlobalOk

`func (o *CxTemplateCreateData) GetGlobalOk() (*bool, bool)`

GetGlobalOk returns a tuple with the Global field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGlobal

`func (o *CxTemplateCreateData) SetGlobal(v bool)`

SetGlobal sets Global field to given value.

### HasGlobal

`func (o *CxTemplateCreateData) HasGlobal() bool`

HasGlobal returns a boolean if a field has been set.

### GetFormality

`func (o *CxTemplateCreateData) GetFormality() string`

GetFormality returns the Formality field if non-nil, zero value otherwise.

### GetFormalityOk

`func (o *CxTemplateCreateData) GetFormalityOk() (*string, bool)`

GetFormalityOk returns a tuple with the Formality field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormality

`func (o *CxTemplateCreateData) SetFormality(v string)`

SetFormality sets Formality field to given value.

### HasFormality

`func (o *CxTemplateCreateData) HasFormality() bool`

HasFormality returns a boolean if a field has been set.

### GetPreviewUrls

`func (o *CxTemplateCreateData) GetPreviewUrls() []CxTemplateCreateDataPreviewUrlsInner`

GetPreviewUrls returns the PreviewUrls field if non-nil, zero value otherwise.

### GetPreviewUrlsOk

`func (o *CxTemplateCreateData) GetPreviewUrlsOk() (*[]CxTemplateCreateDataPreviewUrlsInner, bool)`

GetPreviewUrlsOk returns a tuple with the PreviewUrls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPreviewUrls

`func (o *CxTemplateCreateData) SetPreviewUrls(v []CxTemplateCreateDataPreviewUrlsInner)`

SetPreviewUrls sets PreviewUrls field to given value.

### HasPreviewUrls

`func (o *CxTemplateCreateData) HasPreviewUrls() bool`

HasPreviewUrls returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


