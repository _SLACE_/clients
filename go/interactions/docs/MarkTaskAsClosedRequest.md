# MarkTaskAsClosedRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MediaFileKey** | Pointer to **string** | If completed tasks refers to media assed, here we should get media file key | [optional] 

## Methods

### NewMarkTaskAsClosedRequest

`func NewMarkTaskAsClosedRequest() *MarkTaskAsClosedRequest`

NewMarkTaskAsClosedRequest instantiates a new MarkTaskAsClosedRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMarkTaskAsClosedRequestWithDefaults

`func NewMarkTaskAsClosedRequestWithDefaults() *MarkTaskAsClosedRequest`

NewMarkTaskAsClosedRequestWithDefaults instantiates a new MarkTaskAsClosedRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMediaFileKey

`func (o *MarkTaskAsClosedRequest) GetMediaFileKey() string`

GetMediaFileKey returns the MediaFileKey field if non-nil, zero value otherwise.

### GetMediaFileKeyOk

`func (o *MarkTaskAsClosedRequest) GetMediaFileKeyOk() (*string, bool)`

GetMediaFileKeyOk returns a tuple with the MediaFileKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaFileKey

`func (o *MarkTaskAsClosedRequest) SetMediaFileKey(v string)`

SetMediaFileKey sets MediaFileKey field to given value.

### HasMediaFileKey

`func (o *MarkTaskAsClosedRequest) HasMediaFileKey() bool`

HasMediaFileKey returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


