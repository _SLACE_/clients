# DataSourceDescriptorMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GroupId** | Pointer to **string** |  | [optional] 
**SourceType** | [**DataSource**](DataSource.md) |  | 
**SubType** | **string** |  | 
**Description** | **string** |  | 
**Properties** | [**[]DataSourceDescriptorProperty**](DataSourceDescriptorProperty.md) |  | 

## Methods

### NewDataSourceDescriptorMutable

`func NewDataSourceDescriptorMutable(sourceType DataSource, subType string, description string, properties []DataSourceDescriptorProperty, ) *DataSourceDescriptorMutable`

NewDataSourceDescriptorMutable instantiates a new DataSourceDescriptorMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDataSourceDescriptorMutableWithDefaults

`func NewDataSourceDescriptorMutableWithDefaults() *DataSourceDescriptorMutable`

NewDataSourceDescriptorMutableWithDefaults instantiates a new DataSourceDescriptorMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetGroupId

`func (o *DataSourceDescriptorMutable) GetGroupId() string`

GetGroupId returns the GroupId field if non-nil, zero value otherwise.

### GetGroupIdOk

`func (o *DataSourceDescriptorMutable) GetGroupIdOk() (*string, bool)`

GetGroupIdOk returns a tuple with the GroupId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGroupId

`func (o *DataSourceDescriptorMutable) SetGroupId(v string)`

SetGroupId sets GroupId field to given value.

### HasGroupId

`func (o *DataSourceDescriptorMutable) HasGroupId() bool`

HasGroupId returns a boolean if a field has been set.

### GetSourceType

`func (o *DataSourceDescriptorMutable) GetSourceType() DataSource`

GetSourceType returns the SourceType field if non-nil, zero value otherwise.

### GetSourceTypeOk

`func (o *DataSourceDescriptorMutable) GetSourceTypeOk() (*DataSource, bool)`

GetSourceTypeOk returns a tuple with the SourceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceType

`func (o *DataSourceDescriptorMutable) SetSourceType(v DataSource)`

SetSourceType sets SourceType field to given value.


### GetSubType

`func (o *DataSourceDescriptorMutable) GetSubType() string`

GetSubType returns the SubType field if non-nil, zero value otherwise.

### GetSubTypeOk

`func (o *DataSourceDescriptorMutable) GetSubTypeOk() (*string, bool)`

GetSubTypeOk returns a tuple with the SubType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSubType

`func (o *DataSourceDescriptorMutable) SetSubType(v string)`

SetSubType sets SubType field to given value.


### GetDescription

`func (o *DataSourceDescriptorMutable) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *DataSourceDescriptorMutable) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *DataSourceDescriptorMutable) SetDescription(v string)`

SetDescription sets Description field to given value.


### GetProperties

`func (o *DataSourceDescriptorMutable) GetProperties() []DataSourceDescriptorProperty`

GetProperties returns the Properties field if non-nil, zero value otherwise.

### GetPropertiesOk

`func (o *DataSourceDescriptorMutable) GetPropertiesOk() (*[]DataSourceDescriptorProperty, bool)`

GetPropertiesOk returns a tuple with the Properties field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProperties

`func (o *DataSourceDescriptorMutable) SetProperties(v []DataSourceDescriptorProperty)`

SetProperties sets Properties field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


