# DataSourceDescriptorGroupMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ParentId** | Pointer to **string** |  | [optional] 
**Name** | **string** |  | 
**Description** | Pointer to **string** |  | [optional] 

## Methods

### NewDataSourceDescriptorGroupMutable

`func NewDataSourceDescriptorGroupMutable(name string, ) *DataSourceDescriptorGroupMutable`

NewDataSourceDescriptorGroupMutable instantiates a new DataSourceDescriptorGroupMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDataSourceDescriptorGroupMutableWithDefaults

`func NewDataSourceDescriptorGroupMutableWithDefaults() *DataSourceDescriptorGroupMutable`

NewDataSourceDescriptorGroupMutableWithDefaults instantiates a new DataSourceDescriptorGroupMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetParentId

`func (o *DataSourceDescriptorGroupMutable) GetParentId() string`

GetParentId returns the ParentId field if non-nil, zero value otherwise.

### GetParentIdOk

`func (o *DataSourceDescriptorGroupMutable) GetParentIdOk() (*string, bool)`

GetParentIdOk returns a tuple with the ParentId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParentId

`func (o *DataSourceDescriptorGroupMutable) SetParentId(v string)`

SetParentId sets ParentId field to given value.

### HasParentId

`func (o *DataSourceDescriptorGroupMutable) HasParentId() bool`

HasParentId returns a boolean if a field has been set.

### GetName

`func (o *DataSourceDescriptorGroupMutable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *DataSourceDescriptorGroupMutable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *DataSourceDescriptorGroupMutable) SetName(v string)`

SetName sets Name field to given value.


### GetDescription

`func (o *DataSourceDescriptorGroupMutable) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *DataSourceDescriptorGroupMutable) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *DataSourceDescriptorGroupMutable) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *DataSourceDescriptorGroupMutable) HasDescription() bool`

HasDescription returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


