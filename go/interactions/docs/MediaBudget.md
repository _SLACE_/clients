# MediaBudget

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**RolloutId** | Pointer to **string** |  | [optional] 
**OrganizationId** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**Budget** | Pointer to **string** | decimal string | [optional] 

## Methods

### NewMediaBudget

`func NewMediaBudget() *MediaBudget`

NewMediaBudget instantiates a new MediaBudget object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMediaBudgetWithDefaults

`func NewMediaBudgetWithDefaults() *MediaBudget`

NewMediaBudgetWithDefaults instantiates a new MediaBudget object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *MediaBudget) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *MediaBudget) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *MediaBudget) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *MediaBudget) HasId() bool`

HasId returns a boolean if a field has been set.

### GetRolloutId

`func (o *MediaBudget) GetRolloutId() string`

GetRolloutId returns the RolloutId field if non-nil, zero value otherwise.

### GetRolloutIdOk

`func (o *MediaBudget) GetRolloutIdOk() (*string, bool)`

GetRolloutIdOk returns a tuple with the RolloutId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRolloutId

`func (o *MediaBudget) SetRolloutId(v string)`

SetRolloutId sets RolloutId field to given value.

### HasRolloutId

`func (o *MediaBudget) HasRolloutId() bool`

HasRolloutId returns a boolean if a field has been set.

### GetOrganizationId

`func (o *MediaBudget) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *MediaBudget) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *MediaBudget) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *MediaBudget) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetChannelId

`func (o *MediaBudget) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *MediaBudget) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *MediaBudget) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *MediaBudget) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetBudget

`func (o *MediaBudget) GetBudget() string`

GetBudget returns the Budget field if non-nil, zero value otherwise.

### GetBudgetOk

`func (o *MediaBudget) GetBudgetOk() (*string, bool)`

GetBudgetOk returns a tuple with the Budget field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBudget

`func (o *MediaBudget) SetBudget(v string)`

SetBudget sets Budget field to given value.

### HasBudget

`func (o *MediaBudget) HasBudget() bool`

HasBudget returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


