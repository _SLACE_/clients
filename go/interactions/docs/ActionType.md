# ActionType

## Enum


* `WAIT` (value: `"wait"`)

* `BRANCH` (value: `"branch"`)

* `MESSAGE_LINK` (value: `"message_link"`)

* `SCRIPT_START` (value: `"script_start"`)

* `SCRIPT_V2_START` (value: `"script_v2_start"`)

* `ALERT_USER` (value: `"alert_user"`)

* `STORE_CONTACT_DATA` (value: `"store_contact_data"`)

* `ACTIVITY` (value: `"activity"`)

* `CONTACT_SEGMENT` (value: `"contact_segment"`)

* `CONTACT_UPDATE` (value: `"contact_update"`)

* `CONTACT_EXTERNAL_ID` (value: `"contact_external_id"`)

* `TRANSACTION_UPDATE` (value: `"transaction_update"`)

* `VLOOP_CREATE_REFERRAL` (value: `"vloop_create_referral"`)

* `VLOOP_COMPLETE_FRIEND` (value: `"vloop_complete_friend"`)

* `SIGNATURES_CREATE_SIGNATURE` (value: `"signatures_create_signature"`)

* `SEND_EVENT` (value: `"send_event"`)

* `ENTRY_POINT` (value: `"entry_point"`)

* `ENTRY_POINT_UPDATE` (value: `"entry_point_update"`)

* `API_FETCH_DATA` (value: `"api_fetch_data"`)

* `API_FETCH_FILE` (value: `"api_fetch_file"`)

* `AI_CHATBOT` (value: `"ai_chatbot"`)

* `WHATSAPP_NOTIFICATION` (value: `"whatsapp_notification"`)

* `SMS_NOTIFICATION` (value: `"sms_notification"`)

* `CUSTOM_SYNC_CONTACT` (value: `"custom_sync_contact"`)

* `CUSTOM_CREATE_LOYALTY` (value: `"custom_create_loyalty"`)

* `CUSTOM_CHANGE_POINTS` (value: `"custom_change_points"`)

* `CUSTOM_ASSIGN_COUPON` (value: `"custom_assign_coupon"`)

* `CUSTOM_REDEEM_COUPON` (value: `"custom_redeem_coupon"`)

* `CUSTOM_GET_RECEIPT` (value: `"custom_get_receipt"`)

* `CUSTOM_CREATE_PAYMENT_LINK` (value: `"custom_create_payment_link"`)

* `CUSTOM_PAY_ANOTHER_ACCOUNT` (value: `"custom_pay_another_account"`)

* `CUSTOM_CREATE_DOCUMENT` (value: `"custom_create_document"`)

* `USER_NOTIFICATION` (value: `"user_notification"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


