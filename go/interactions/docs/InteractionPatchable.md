# InteractionPatchable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**LiveFrom** | Pointer to **string** |  | [optional] 
**LiveTo** | Pointer to **string** |  | [optional] 

## Methods

### NewInteractionPatchable

`func NewInteractionPatchable() *InteractionPatchable`

NewInteractionPatchable instantiates a new InteractionPatchable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInteractionPatchableWithDefaults

`func NewInteractionPatchableWithDefaults() *InteractionPatchable`

NewInteractionPatchableWithDefaults instantiates a new InteractionPatchable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *InteractionPatchable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *InteractionPatchable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *InteractionPatchable) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *InteractionPatchable) HasName() bool`

HasName returns a boolean if a field has been set.

### GetLiveFrom

`func (o *InteractionPatchable) GetLiveFrom() string`

GetLiveFrom returns the LiveFrom field if non-nil, zero value otherwise.

### GetLiveFromOk

`func (o *InteractionPatchable) GetLiveFromOk() (*string, bool)`

GetLiveFromOk returns a tuple with the LiveFrom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLiveFrom

`func (o *InteractionPatchable) SetLiveFrom(v string)`

SetLiveFrom sets LiveFrom field to given value.

### HasLiveFrom

`func (o *InteractionPatchable) HasLiveFrom() bool`

HasLiveFrom returns a boolean if a field has been set.

### GetLiveTo

`func (o *InteractionPatchable) GetLiveTo() string`

GetLiveTo returns the LiveTo field if non-nil, zero value otherwise.

### GetLiveToOk

`func (o *InteractionPatchable) GetLiveToOk() (*string, bool)`

GetLiveToOk returns a tuple with the LiveTo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLiveTo

`func (o *InteractionPatchable) SetLiveTo(v string)`

SetLiveTo sets LiveTo field to given value.

### HasLiveTo

`func (o *InteractionPatchable) HasLiveTo() bool`

HasLiveTo returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


