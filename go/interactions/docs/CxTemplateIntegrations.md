# CxTemplateIntegrations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VynamicCoupons** | Pointer to **bool** |  | [optional] 
**VynamicCrm** | Pointer to **bool** |  | [optional] 
**AnybillReceipt** | Pointer to **bool** |  | [optional] 
**PaypalPayments** | Pointer to **bool** |  | [optional] 
**UnzerPayments** | Pointer to **bool** |  | [optional] 
**InsignDocuments** | Pointer to **bool** |  | [optional] 
**PolyprintNfcCard** | Pointer to **bool** |  | [optional] 

## Methods

### NewCxTemplateIntegrations

`func NewCxTemplateIntegrations() *CxTemplateIntegrations`

NewCxTemplateIntegrations instantiates a new CxTemplateIntegrations object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateIntegrationsWithDefaults

`func NewCxTemplateIntegrationsWithDefaults() *CxTemplateIntegrations`

NewCxTemplateIntegrationsWithDefaults instantiates a new CxTemplateIntegrations object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVynamicCoupons

`func (o *CxTemplateIntegrations) GetVynamicCoupons() bool`

GetVynamicCoupons returns the VynamicCoupons field if non-nil, zero value otherwise.

### GetVynamicCouponsOk

`func (o *CxTemplateIntegrations) GetVynamicCouponsOk() (*bool, bool)`

GetVynamicCouponsOk returns a tuple with the VynamicCoupons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVynamicCoupons

`func (o *CxTemplateIntegrations) SetVynamicCoupons(v bool)`

SetVynamicCoupons sets VynamicCoupons field to given value.

### HasVynamicCoupons

`func (o *CxTemplateIntegrations) HasVynamicCoupons() bool`

HasVynamicCoupons returns a boolean if a field has been set.

### GetVynamicCrm

`func (o *CxTemplateIntegrations) GetVynamicCrm() bool`

GetVynamicCrm returns the VynamicCrm field if non-nil, zero value otherwise.

### GetVynamicCrmOk

`func (o *CxTemplateIntegrations) GetVynamicCrmOk() (*bool, bool)`

GetVynamicCrmOk returns a tuple with the VynamicCrm field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVynamicCrm

`func (o *CxTemplateIntegrations) SetVynamicCrm(v bool)`

SetVynamicCrm sets VynamicCrm field to given value.

### HasVynamicCrm

`func (o *CxTemplateIntegrations) HasVynamicCrm() bool`

HasVynamicCrm returns a boolean if a field has been set.

### GetAnybillReceipt

`func (o *CxTemplateIntegrations) GetAnybillReceipt() bool`

GetAnybillReceipt returns the AnybillReceipt field if non-nil, zero value otherwise.

### GetAnybillReceiptOk

`func (o *CxTemplateIntegrations) GetAnybillReceiptOk() (*bool, bool)`

GetAnybillReceiptOk returns a tuple with the AnybillReceipt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAnybillReceipt

`func (o *CxTemplateIntegrations) SetAnybillReceipt(v bool)`

SetAnybillReceipt sets AnybillReceipt field to given value.

### HasAnybillReceipt

`func (o *CxTemplateIntegrations) HasAnybillReceipt() bool`

HasAnybillReceipt returns a boolean if a field has been set.

### GetPaypalPayments

`func (o *CxTemplateIntegrations) GetPaypalPayments() bool`

GetPaypalPayments returns the PaypalPayments field if non-nil, zero value otherwise.

### GetPaypalPaymentsOk

`func (o *CxTemplateIntegrations) GetPaypalPaymentsOk() (*bool, bool)`

GetPaypalPaymentsOk returns a tuple with the PaypalPayments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPaypalPayments

`func (o *CxTemplateIntegrations) SetPaypalPayments(v bool)`

SetPaypalPayments sets PaypalPayments field to given value.

### HasPaypalPayments

`func (o *CxTemplateIntegrations) HasPaypalPayments() bool`

HasPaypalPayments returns a boolean if a field has been set.

### GetUnzerPayments

`func (o *CxTemplateIntegrations) GetUnzerPayments() bool`

GetUnzerPayments returns the UnzerPayments field if non-nil, zero value otherwise.

### GetUnzerPaymentsOk

`func (o *CxTemplateIntegrations) GetUnzerPaymentsOk() (*bool, bool)`

GetUnzerPaymentsOk returns a tuple with the UnzerPayments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnzerPayments

`func (o *CxTemplateIntegrations) SetUnzerPayments(v bool)`

SetUnzerPayments sets UnzerPayments field to given value.

### HasUnzerPayments

`func (o *CxTemplateIntegrations) HasUnzerPayments() bool`

HasUnzerPayments returns a boolean if a field has been set.

### GetInsignDocuments

`func (o *CxTemplateIntegrations) GetInsignDocuments() bool`

GetInsignDocuments returns the InsignDocuments field if non-nil, zero value otherwise.

### GetInsignDocumentsOk

`func (o *CxTemplateIntegrations) GetInsignDocumentsOk() (*bool, bool)`

GetInsignDocumentsOk returns a tuple with the InsignDocuments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInsignDocuments

`func (o *CxTemplateIntegrations) SetInsignDocuments(v bool)`

SetInsignDocuments sets InsignDocuments field to given value.

### HasInsignDocuments

`func (o *CxTemplateIntegrations) HasInsignDocuments() bool`

HasInsignDocuments returns a boolean if a field has been set.

### GetPolyprintNfcCard

`func (o *CxTemplateIntegrations) GetPolyprintNfcCard() bool`

GetPolyprintNfcCard returns the PolyprintNfcCard field if non-nil, zero value otherwise.

### GetPolyprintNfcCardOk

`func (o *CxTemplateIntegrations) GetPolyprintNfcCardOk() (*bool, bool)`

GetPolyprintNfcCardOk returns a tuple with the PolyprintNfcCard field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolyprintNfcCard

`func (o *CxTemplateIntegrations) SetPolyprintNfcCard(v bool)`

SetPolyprintNfcCard sets PolyprintNfcCard field to given value.

### HasPolyprintNfcCard

`func (o *CxTemplateIntegrations) HasPolyprintNfcCard() bool`

HasPolyprintNfcCard returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


