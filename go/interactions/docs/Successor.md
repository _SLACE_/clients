# Successor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InteractionId** | Pointer to **string** |  | [optional] 
**EntryPointHandover** | Pointer to **bool** |  | [optional] 

## Methods

### NewSuccessor

`func NewSuccessor() *Successor`

NewSuccessor instantiates a new Successor object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSuccessorWithDefaults

`func NewSuccessorWithDefaults() *Successor`

NewSuccessorWithDefaults instantiates a new Successor object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInteractionId

`func (o *Successor) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *Successor) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *Successor) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *Successor) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetEntryPointHandover

`func (o *Successor) GetEntryPointHandover() bool`

GetEntryPointHandover returns the EntryPointHandover field if non-nil, zero value otherwise.

### GetEntryPointHandoverOk

`func (o *Successor) GetEntryPointHandoverOk() (*bool, bool)`

GetEntryPointHandoverOk returns a tuple with the EntryPointHandover field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEntryPointHandover

`func (o *Successor) SetEntryPointHandover(v bool)`

SetEntryPointHandover sets EntryPointHandover field to given value.

### HasEntryPointHandover

`func (o *Successor) HasEntryPointHandover() bool`

HasEntryPointHandover returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


