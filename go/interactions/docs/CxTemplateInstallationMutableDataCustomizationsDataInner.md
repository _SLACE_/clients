# CxTemplateInstallationMutableDataCustomizationsDataInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TaskId** | Pointer to **string** |  | [optional] 
**Content** | Pointer to **string** |  | [optional] 

## Methods

### NewCxTemplateInstallationMutableDataCustomizationsDataInner

`func NewCxTemplateInstallationMutableDataCustomizationsDataInner() *CxTemplateInstallationMutableDataCustomizationsDataInner`

NewCxTemplateInstallationMutableDataCustomizationsDataInner instantiates a new CxTemplateInstallationMutableDataCustomizationsDataInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateInstallationMutableDataCustomizationsDataInnerWithDefaults

`func NewCxTemplateInstallationMutableDataCustomizationsDataInnerWithDefaults() *CxTemplateInstallationMutableDataCustomizationsDataInner`

NewCxTemplateInstallationMutableDataCustomizationsDataInnerWithDefaults instantiates a new CxTemplateInstallationMutableDataCustomizationsDataInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTaskId

`func (o *CxTemplateInstallationMutableDataCustomizationsDataInner) GetTaskId() string`

GetTaskId returns the TaskId field if non-nil, zero value otherwise.

### GetTaskIdOk

`func (o *CxTemplateInstallationMutableDataCustomizationsDataInner) GetTaskIdOk() (*string, bool)`

GetTaskIdOk returns a tuple with the TaskId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTaskId

`func (o *CxTemplateInstallationMutableDataCustomizationsDataInner) SetTaskId(v string)`

SetTaskId sets TaskId field to given value.

### HasTaskId

`func (o *CxTemplateInstallationMutableDataCustomizationsDataInner) HasTaskId() bool`

HasTaskId returns a boolean if a field has been set.

### GetContent

`func (o *CxTemplateInstallationMutableDataCustomizationsDataInner) GetContent() string`

GetContent returns the Content field if non-nil, zero value otherwise.

### GetContentOk

`func (o *CxTemplateInstallationMutableDataCustomizationsDataInner) GetContentOk() (*string, bool)`

GetContentOk returns a tuple with the Content field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContent

`func (o *CxTemplateInstallationMutableDataCustomizationsDataInner) SetContent(v string)`

SetContent sets Content field to given value.

### HasContent

`func (o *CxTemplateInstallationMutableDataCustomizationsDataInner) HasContent() bool`

HasContent returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


