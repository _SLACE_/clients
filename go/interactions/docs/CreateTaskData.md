# CreateTaskData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Text** | Pointer to **string** |  | [optional] 
**ContextMap** | Pointer to **map[string]interface{}** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** | optional | [optional] 
**InteractionId** | Pointer to **string** | only if task should belong to interaction | [optional] 
**UserId** | Pointer to **string** | only if tash should belong to user | [optional] 
**Title** | Pointer to **string** |  | [optional] 
**Blocker** | Pointer to **bool** |  | [optional] 
**ScheduleType** | Pointer to **string** |  | [optional] 
**Interval** | Pointer to **string** |  | [optional] 
**DestinationLink** | Pointer to **string** |  | [optional] 
**Deadline** | Pointer to **string** |  | [optional] 
**Action** | Pointer to **string** |  | [optional] 
**Usages** | Pointer to **[]string** |  | [optional] 
**Element** | Pointer to [**CreateTaskDataElement**](CreateTaskDataElement.md) |  | [optional] 
**ScriptId** | Pointer to **string** |  | [optional] 
**Sendout** | Pointer to [**CreateTaskDataSendout**](CreateTaskDataSendout.md) |  | [optional] 

## Methods

### NewCreateTaskData

`func NewCreateTaskData() *CreateTaskData`

NewCreateTaskData instantiates a new CreateTaskData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateTaskDataWithDefaults

`func NewCreateTaskDataWithDefaults() *CreateTaskData`

NewCreateTaskDataWithDefaults instantiates a new CreateTaskData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetText

`func (o *CreateTaskData) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *CreateTaskData) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *CreateTaskData) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *CreateTaskData) HasText() bool`

HasText returns a boolean if a field has been set.

### GetContextMap

`func (o *CreateTaskData) GetContextMap() map[string]interface{}`

GetContextMap returns the ContextMap field if non-nil, zero value otherwise.

### GetContextMapOk

`func (o *CreateTaskData) GetContextMapOk() (*map[string]interface{}, bool)`

GetContextMapOk returns a tuple with the ContextMap field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContextMap

`func (o *CreateTaskData) SetContextMap(v map[string]interface{})`

SetContextMap sets ContextMap field to given value.

### HasContextMap

`func (o *CreateTaskData) HasContextMap() bool`

HasContextMap returns a boolean if a field has been set.

### GetType

`func (o *CreateTaskData) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *CreateTaskData) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *CreateTaskData) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *CreateTaskData) HasType() bool`

HasType returns a boolean if a field has been set.

### GetChannelId

`func (o *CreateTaskData) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CreateTaskData) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CreateTaskData) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *CreateTaskData) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetInteractionId

`func (o *CreateTaskData) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *CreateTaskData) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *CreateTaskData) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *CreateTaskData) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetUserId

`func (o *CreateTaskData) GetUserId() string`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *CreateTaskData) GetUserIdOk() (*string, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *CreateTaskData) SetUserId(v string)`

SetUserId sets UserId field to given value.

### HasUserId

`func (o *CreateTaskData) HasUserId() bool`

HasUserId returns a boolean if a field has been set.

### GetTitle

`func (o *CreateTaskData) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *CreateTaskData) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *CreateTaskData) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *CreateTaskData) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetBlocker

`func (o *CreateTaskData) GetBlocker() bool`

GetBlocker returns the Blocker field if non-nil, zero value otherwise.

### GetBlockerOk

`func (o *CreateTaskData) GetBlockerOk() (*bool, bool)`

GetBlockerOk returns a tuple with the Blocker field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlocker

`func (o *CreateTaskData) SetBlocker(v bool)`

SetBlocker sets Blocker field to given value.

### HasBlocker

`func (o *CreateTaskData) HasBlocker() bool`

HasBlocker returns a boolean if a field has been set.

### GetScheduleType

`func (o *CreateTaskData) GetScheduleType() string`

GetScheduleType returns the ScheduleType field if non-nil, zero value otherwise.

### GetScheduleTypeOk

`func (o *CreateTaskData) GetScheduleTypeOk() (*string, bool)`

GetScheduleTypeOk returns a tuple with the ScheduleType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScheduleType

`func (o *CreateTaskData) SetScheduleType(v string)`

SetScheduleType sets ScheduleType field to given value.

### HasScheduleType

`func (o *CreateTaskData) HasScheduleType() bool`

HasScheduleType returns a boolean if a field has been set.

### GetInterval

`func (o *CreateTaskData) GetInterval() string`

GetInterval returns the Interval field if non-nil, zero value otherwise.

### GetIntervalOk

`func (o *CreateTaskData) GetIntervalOk() (*string, bool)`

GetIntervalOk returns a tuple with the Interval field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInterval

`func (o *CreateTaskData) SetInterval(v string)`

SetInterval sets Interval field to given value.

### HasInterval

`func (o *CreateTaskData) HasInterval() bool`

HasInterval returns a boolean if a field has been set.

### GetDestinationLink

`func (o *CreateTaskData) GetDestinationLink() string`

GetDestinationLink returns the DestinationLink field if non-nil, zero value otherwise.

### GetDestinationLinkOk

`func (o *CreateTaskData) GetDestinationLinkOk() (*string, bool)`

GetDestinationLinkOk returns a tuple with the DestinationLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDestinationLink

`func (o *CreateTaskData) SetDestinationLink(v string)`

SetDestinationLink sets DestinationLink field to given value.

### HasDestinationLink

`func (o *CreateTaskData) HasDestinationLink() bool`

HasDestinationLink returns a boolean if a field has been set.

### GetDeadline

`func (o *CreateTaskData) GetDeadline() string`

GetDeadline returns the Deadline field if non-nil, zero value otherwise.

### GetDeadlineOk

`func (o *CreateTaskData) GetDeadlineOk() (*string, bool)`

GetDeadlineOk returns a tuple with the Deadline field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDeadline

`func (o *CreateTaskData) SetDeadline(v string)`

SetDeadline sets Deadline field to given value.

### HasDeadline

`func (o *CreateTaskData) HasDeadline() bool`

HasDeadline returns a boolean if a field has been set.

### GetAction

`func (o *CreateTaskData) GetAction() string`

GetAction returns the Action field if non-nil, zero value otherwise.

### GetActionOk

`func (o *CreateTaskData) GetActionOk() (*string, bool)`

GetActionOk returns a tuple with the Action field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAction

`func (o *CreateTaskData) SetAction(v string)`

SetAction sets Action field to given value.

### HasAction

`func (o *CreateTaskData) HasAction() bool`

HasAction returns a boolean if a field has been set.

### GetUsages

`func (o *CreateTaskData) GetUsages() []string`

GetUsages returns the Usages field if non-nil, zero value otherwise.

### GetUsagesOk

`func (o *CreateTaskData) GetUsagesOk() (*[]string, bool)`

GetUsagesOk returns a tuple with the Usages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsages

`func (o *CreateTaskData) SetUsages(v []string)`

SetUsages sets Usages field to given value.

### HasUsages

`func (o *CreateTaskData) HasUsages() bool`

HasUsages returns a boolean if a field has been set.

### GetElement

`func (o *CreateTaskData) GetElement() CreateTaskDataElement`

GetElement returns the Element field if non-nil, zero value otherwise.

### GetElementOk

`func (o *CreateTaskData) GetElementOk() (*CreateTaskDataElement, bool)`

GetElementOk returns a tuple with the Element field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetElement

`func (o *CreateTaskData) SetElement(v CreateTaskDataElement)`

SetElement sets Element field to given value.

### HasElement

`func (o *CreateTaskData) HasElement() bool`

HasElement returns a boolean if a field has been set.

### GetScriptId

`func (o *CreateTaskData) GetScriptId() string`

GetScriptId returns the ScriptId field if non-nil, zero value otherwise.

### GetScriptIdOk

`func (o *CreateTaskData) GetScriptIdOk() (*string, bool)`

GetScriptIdOk returns a tuple with the ScriptId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScriptId

`func (o *CreateTaskData) SetScriptId(v string)`

SetScriptId sets ScriptId field to given value.

### HasScriptId

`func (o *CreateTaskData) HasScriptId() bool`

HasScriptId returns a boolean if a field has been set.

### GetSendout

`func (o *CreateTaskData) GetSendout() CreateTaskDataSendout`

GetSendout returns the Sendout field if non-nil, zero value otherwise.

### GetSendoutOk

`func (o *CreateTaskData) GetSendoutOk() (*CreateTaskDataSendout, bool)`

GetSendoutOk returns a tuple with the Sendout field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSendout

`func (o *CreateTaskData) SetSendout(v CreateTaskDataSendout)`

SetSendout sets Sendout field to given value.

### HasSendout

`func (o *CreateTaskData) HasSendout() bool`

HasSendout returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


