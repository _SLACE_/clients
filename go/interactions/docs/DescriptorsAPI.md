# \DescriptorsAPI

All URIs are relative to *https://interactions.slace.dev/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateDescriptor**](DescriptorsAPI.md#CreateDescriptor) | **Post** /data-sources/descriptors | Create Descriptor
[**CreateGroup**](DescriptorsAPI.md#CreateGroup) | **Post** /data-sources/groups | Create Group
[**DeleteDescriptor**](DescriptorsAPI.md#DeleteDescriptor) | **Delete** /data-sources/descriptors/{id} | Delete Descriptor
[**DeleteGroup**](DescriptorsAPI.md#DeleteGroup) | **Delete** /data-sources/groups/{id} | Delete group
[**GetDescriptorById**](DescriptorsAPI.md#GetDescriptorById) | **Get** /data-sources/descriptors/{id} | Get Descriptor By Id
[**GetGroupById**](DescriptorsAPI.md#GetGroupById) | **Get** /data-sources/groups/{id} | Get Group By Id
[**ListDescriptors**](DescriptorsAPI.md#ListDescriptors) | **Get** /data-sources/descriptors | List Descriptors
[**ListGroups**](DescriptorsAPI.md#ListGroups) | **Get** /data-sources/groups | List Groups
[**UpdateDescriptor**](DescriptorsAPI.md#UpdateDescriptor) | **Put** /data-sources/descriptors/{id} | Update descriptor
[**UpdateGroup**](DescriptorsAPI.md#UpdateGroup) | **Put** /data-sources/groups/{id} | Update group



## CreateDescriptor

> DataSourceDescriptor CreateDescriptor(ctx).DataSourceDescriptorMutable(dataSourceDescriptorMutable).Execute()

Create Descriptor

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    dataSourceDescriptorMutable := *openapiclient.NewDataSourceDescriptorMutable(openapiclient.DataSource("static"), "SubType_example", "Description_example", []openapiclient.DataSourceDescriptorProperty{*openapiclient.NewDataSourceDescriptorProperty("JsonPath_example", openapiclient.DataType("bool"))}) // DataSourceDescriptorMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DescriptorsAPI.CreateDescriptor(context.Background()).DataSourceDescriptorMutable(dataSourceDescriptorMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DescriptorsAPI.CreateDescriptor``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateDescriptor`: DataSourceDescriptor
    fmt.Fprintf(os.Stdout, "Response from `DescriptorsAPI.CreateDescriptor`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateDescriptorRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataSourceDescriptorMutable** | [**DataSourceDescriptorMutable**](DataSourceDescriptorMutable.md) |  | 

### Return type

[**DataSourceDescriptor**](DataSourceDescriptor.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateGroup

> DataSourceDescriptorGroup CreateGroup(ctx).DataSourceDescriptorGroupMutable(dataSourceDescriptorGroupMutable).Execute()

Create Group

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    dataSourceDescriptorGroupMutable := *openapiclient.NewDataSourceDescriptorGroupMutable("Name_example") // DataSourceDescriptorGroupMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DescriptorsAPI.CreateGroup(context.Background()).DataSourceDescriptorGroupMutable(dataSourceDescriptorGroupMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DescriptorsAPI.CreateGroup``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateGroup`: DataSourceDescriptorGroup
    fmt.Fprintf(os.Stdout, "Response from `DescriptorsAPI.CreateGroup`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateGroupRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataSourceDescriptorGroupMutable** | [**DataSourceDescriptorGroupMutable**](DataSourceDescriptorGroupMutable.md) |  | 

### Return type

[**DataSourceDescriptorGroup**](DataSourceDescriptorGroup.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteDescriptor

> GenericResponse DeleteDescriptor(ctx, id).Execute()

Delete Descriptor

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DescriptorsAPI.DeleteDescriptor(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DescriptorsAPI.DeleteDescriptor``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteDescriptor`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `DescriptorsAPI.DeleteDescriptor`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteDescriptorRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteGroup

> GenericResponse DeleteGroup(ctx, id).Execute()

Delete group

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DescriptorsAPI.DeleteGroup(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DescriptorsAPI.DeleteGroup``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteGroup`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `DescriptorsAPI.DeleteGroup`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteGroupRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDescriptorById

> DataSourceDescriptor GetDescriptorById(ctx, id).Execute()

Get Descriptor By Id

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DescriptorsAPI.GetDescriptorById(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DescriptorsAPI.GetDescriptorById``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetDescriptorById`: DataSourceDescriptor
    fmt.Fprintf(os.Stdout, "Response from `DescriptorsAPI.GetDescriptorById`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetDescriptorByIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**DataSourceDescriptor**](DataSourceDescriptor.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetGroupById

> DataSourceDescriptorGroup GetGroupById(ctx, id).Execute()

Get Group By Id

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DescriptorsAPI.GetGroupById(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DescriptorsAPI.GetGroupById``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetGroupById`: DataSourceDescriptorGroup
    fmt.Fprintf(os.Stdout, "Response from `DescriptorsAPI.GetGroupById`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetGroupByIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**DataSourceDescriptorGroup**](DataSourceDescriptorGroup.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListDescriptors

> []DataSourceDescriptor ListDescriptors(ctx).Execute()

List Descriptors

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DescriptorsAPI.ListDescriptors(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DescriptorsAPI.ListDescriptors``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListDescriptors`: []DataSourceDescriptor
    fmt.Fprintf(os.Stdout, "Response from `DescriptorsAPI.ListDescriptors`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiListDescriptorsRequest struct via the builder pattern


### Return type

[**[]DataSourceDescriptor**](DataSourceDescriptor.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListGroups

> []DataSourceDescriptorGroup ListGroups(ctx).Execute()

List Groups

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DescriptorsAPI.ListGroups(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DescriptorsAPI.ListGroups``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListGroups`: []DataSourceDescriptorGroup
    fmt.Fprintf(os.Stdout, "Response from `DescriptorsAPI.ListGroups`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiListGroupsRequest struct via the builder pattern


### Return type

[**[]DataSourceDescriptorGroup**](DataSourceDescriptorGroup.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateDescriptor

> DataSourceDescriptor UpdateDescriptor(ctx, id).DataSourceDescriptorMutable(dataSourceDescriptorMutable).Execute()

Update descriptor

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    id := "id_example" // string | 
    dataSourceDescriptorMutable := *openapiclient.NewDataSourceDescriptorMutable(openapiclient.DataSource("static"), "SubType_example", "Description_example", []openapiclient.DataSourceDescriptorProperty{*openapiclient.NewDataSourceDescriptorProperty("JsonPath_example", openapiclient.DataType("bool"))}) // DataSourceDescriptorMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DescriptorsAPI.UpdateDescriptor(context.Background(), id).DataSourceDescriptorMutable(dataSourceDescriptorMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DescriptorsAPI.UpdateDescriptor``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateDescriptor`: DataSourceDescriptor
    fmt.Fprintf(os.Stdout, "Response from `DescriptorsAPI.UpdateDescriptor`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateDescriptorRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **dataSourceDescriptorMutable** | [**DataSourceDescriptorMutable**](DataSourceDescriptorMutable.md) |  | 

### Return type

[**DataSourceDescriptor**](DataSourceDescriptor.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateGroup

> DataSourceDescriptorGroup UpdateGroup(ctx, id).DataSourceDescriptorGroupMutable(dataSourceDescriptorGroupMutable).Execute()

Update group

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    id := "id_example" // string | 
    dataSourceDescriptorGroupMutable := *openapiclient.NewDataSourceDescriptorGroupMutable("Name_example") // DataSourceDescriptorGroupMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DescriptorsAPI.UpdateGroup(context.Background(), id).DataSourceDescriptorGroupMutable(dataSourceDescriptorGroupMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DescriptorsAPI.UpdateGroup``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateGroup`: DataSourceDescriptorGroup
    fmt.Fprintf(os.Stdout, "Response from `DescriptorsAPI.UpdateGroup`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateGroupRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **dataSourceDescriptorGroupMutable** | [**DataSourceDescriptorGroupMutable**](DataSourceDescriptorGroupMutable.md) |  | 

### Return type

[**DataSourceDescriptorGroup**](DataSourceDescriptorGroup.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

