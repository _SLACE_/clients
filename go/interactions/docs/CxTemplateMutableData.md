# CxTemplateMutableData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**HeaderImageUrls** | Pointer to [**[]CxTemplateMutableDataHeaderImageUrlsInner**](CxTemplateMutableDataHeaderImageUrlsInner.md) |  | [optional] 
**Description** | Pointer to **string** |  | [optional] 
**Languages** | Pointer to **[]string** |  | [optional] 
**Integrations** | Pointer to [**CxTemplateMutableDataIntegrations**](CxTemplateMutableDataIntegrations.md) |  | [optional] 
**RequiredMessengers** | Pointer to **[]string** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 
**Interactions** | Pointer to [**[]CxTemplateMutableDataInteractionsInner**](CxTemplateMutableDataInteractionsInner.md) |  | [optional] 
**Global** | Pointer to **bool** |  | [optional] 
**Formality** | Pointer to **string** |  | [optional] 
**PreviewUrls** | Pointer to [**[]CxTemplateMutableDataPreviewUrlsInner**](CxTemplateMutableDataPreviewUrlsInner.md) |  | [optional] 

## Methods

### NewCxTemplateMutableData

`func NewCxTemplateMutableData() *CxTemplateMutableData`

NewCxTemplateMutableData instantiates a new CxTemplateMutableData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateMutableDataWithDefaults

`func NewCxTemplateMutableDataWithDefaults() *CxTemplateMutableData`

NewCxTemplateMutableDataWithDefaults instantiates a new CxTemplateMutableData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *CxTemplateMutableData) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CxTemplateMutableData) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CxTemplateMutableData) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *CxTemplateMutableData) HasName() bool`

HasName returns a boolean if a field has been set.

### GetHeaderImageUrls

`func (o *CxTemplateMutableData) GetHeaderImageUrls() []CxTemplateMutableDataHeaderImageUrlsInner`

GetHeaderImageUrls returns the HeaderImageUrls field if non-nil, zero value otherwise.

### GetHeaderImageUrlsOk

`func (o *CxTemplateMutableData) GetHeaderImageUrlsOk() (*[]CxTemplateMutableDataHeaderImageUrlsInner, bool)`

GetHeaderImageUrlsOk returns a tuple with the HeaderImageUrls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeaderImageUrls

`func (o *CxTemplateMutableData) SetHeaderImageUrls(v []CxTemplateMutableDataHeaderImageUrlsInner)`

SetHeaderImageUrls sets HeaderImageUrls field to given value.

### HasHeaderImageUrls

`func (o *CxTemplateMutableData) HasHeaderImageUrls() bool`

HasHeaderImageUrls returns a boolean if a field has been set.

### GetDescription

`func (o *CxTemplateMutableData) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *CxTemplateMutableData) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *CxTemplateMutableData) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *CxTemplateMutableData) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetLanguages

`func (o *CxTemplateMutableData) GetLanguages() []string`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *CxTemplateMutableData) GetLanguagesOk() (*[]string, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *CxTemplateMutableData) SetLanguages(v []string)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *CxTemplateMutableData) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetIntegrations

`func (o *CxTemplateMutableData) GetIntegrations() CxTemplateMutableDataIntegrations`

GetIntegrations returns the Integrations field if non-nil, zero value otherwise.

### GetIntegrationsOk

`func (o *CxTemplateMutableData) GetIntegrationsOk() (*CxTemplateMutableDataIntegrations, bool)`

GetIntegrationsOk returns a tuple with the Integrations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIntegrations

`func (o *CxTemplateMutableData) SetIntegrations(v CxTemplateMutableDataIntegrations)`

SetIntegrations sets Integrations field to given value.

### HasIntegrations

`func (o *CxTemplateMutableData) HasIntegrations() bool`

HasIntegrations returns a boolean if a field has been set.

### GetRequiredMessengers

`func (o *CxTemplateMutableData) GetRequiredMessengers() []string`

GetRequiredMessengers returns the RequiredMessengers field if non-nil, zero value otherwise.

### GetRequiredMessengersOk

`func (o *CxTemplateMutableData) GetRequiredMessengersOk() (*[]string, bool)`

GetRequiredMessengersOk returns a tuple with the RequiredMessengers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequiredMessengers

`func (o *CxTemplateMutableData) SetRequiredMessengers(v []string)`

SetRequiredMessengers sets RequiredMessengers field to given value.

### HasRequiredMessengers

`func (o *CxTemplateMutableData) HasRequiredMessengers() bool`

HasRequiredMessengers returns a boolean if a field has been set.

### GetTags

`func (o *CxTemplateMutableData) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *CxTemplateMutableData) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *CxTemplateMutableData) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *CxTemplateMutableData) HasTags() bool`

HasTags returns a boolean if a field has been set.

### GetInteractions

`func (o *CxTemplateMutableData) GetInteractions() []CxTemplateMutableDataInteractionsInner`

GetInteractions returns the Interactions field if non-nil, zero value otherwise.

### GetInteractionsOk

`func (o *CxTemplateMutableData) GetInteractionsOk() (*[]CxTemplateMutableDataInteractionsInner, bool)`

GetInteractionsOk returns a tuple with the Interactions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractions

`func (o *CxTemplateMutableData) SetInteractions(v []CxTemplateMutableDataInteractionsInner)`

SetInteractions sets Interactions field to given value.

### HasInteractions

`func (o *CxTemplateMutableData) HasInteractions() bool`

HasInteractions returns a boolean if a field has been set.

### GetGlobal

`func (o *CxTemplateMutableData) GetGlobal() bool`

GetGlobal returns the Global field if non-nil, zero value otherwise.

### GetGlobalOk

`func (o *CxTemplateMutableData) GetGlobalOk() (*bool, bool)`

GetGlobalOk returns a tuple with the Global field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGlobal

`func (o *CxTemplateMutableData) SetGlobal(v bool)`

SetGlobal sets Global field to given value.

### HasGlobal

`func (o *CxTemplateMutableData) HasGlobal() bool`

HasGlobal returns a boolean if a field has been set.

### GetFormality

`func (o *CxTemplateMutableData) GetFormality() string`

GetFormality returns the Formality field if non-nil, zero value otherwise.

### GetFormalityOk

`func (o *CxTemplateMutableData) GetFormalityOk() (*string, bool)`

GetFormalityOk returns a tuple with the Formality field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormality

`func (o *CxTemplateMutableData) SetFormality(v string)`

SetFormality sets Formality field to given value.

### HasFormality

`func (o *CxTemplateMutableData) HasFormality() bool`

HasFormality returns a boolean if a field has been set.

### GetPreviewUrls

`func (o *CxTemplateMutableData) GetPreviewUrls() []CxTemplateMutableDataPreviewUrlsInner`

GetPreviewUrls returns the PreviewUrls field if non-nil, zero value otherwise.

### GetPreviewUrlsOk

`func (o *CxTemplateMutableData) GetPreviewUrlsOk() (*[]CxTemplateMutableDataPreviewUrlsInner, bool)`

GetPreviewUrlsOk returns a tuple with the PreviewUrls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPreviewUrls

`func (o *CxTemplateMutableData) SetPreviewUrls(v []CxTemplateMutableDataPreviewUrlsInner)`

SetPreviewUrls sets PreviewUrls field to given value.

### HasPreviewUrls

`func (o *CxTemplateMutableData) HasPreviewUrls() bool`

HasPreviewUrls returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


