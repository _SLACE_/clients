# PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OrganizationId** | Pointer to **string** |  | [optional] 
**ChannelIds** | Pointer to **[]string** |  | [optional] 

## Methods

### NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner

`func NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner() *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner`

NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner instantiates a new PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInnerWithDefaults

`func NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInnerWithDefaults() *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner`

NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInnerWithDefaults instantiates a new PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOrganizationId

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetChannelIds

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner) GetChannelIds() []string`

GetChannelIds returns the ChannelIds field if non-nil, zero value otherwise.

### GetChannelIdsOk

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner) GetChannelIdsOk() (*[]string, bool)`

GetChannelIdsOk returns a tuple with the ChannelIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelIds

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner) SetChannelIds(v []string)`

SetChannelIds sets ChannelIds field to given value.

### HasChannelIds

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner) HasChannelIds() bool`

HasChannelIds returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


