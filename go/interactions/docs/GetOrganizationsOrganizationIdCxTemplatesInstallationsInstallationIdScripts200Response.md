# GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | Pointer to **[]map[string]interface{}** |  | [optional] 
**Total** | Pointer to **int32** |  | [optional] 

## Methods

### NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response

`func NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response() *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response`

NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response instantiates a new GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200ResponseWithDefaults

`func NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200ResponseWithDefaults() *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response`

NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200ResponseWithDefaults instantiates a new GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) GetResults() []map[string]interface{}`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) GetResultsOk() (*[]map[string]interface{}, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) SetResults(v []map[string]interface{})`

SetResults sets Results field to given value.

### HasResults

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) HasResults() bool`

HasResults returns a boolean if a field has been set.

### GetTotal

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


