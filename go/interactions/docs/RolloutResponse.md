# RolloutResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**ChannelId** | **string** |  | 
**OrganizationId** | **string** |  | 
**InteractionId** | Pointer to **string** |  | [optional] 
**RolloutStatuses** | Pointer to [**[]RolloutStatus**](RolloutStatus.md) |  | [optional] 
**Version** | **int32** |  | 
**VersionMinor** | Pointer to **int32** |  | [optional] 
**Linked** | Pointer to **bool** |  | [optional] 
**CreatedAt** | **string** |  | 
**UpdatedAt** | **string** |  | 
**Errors** | Pointer to [**[]RolloutError**](RolloutError.md) |  | [optional] 
**ScPermissionInfo** | Pointer to **map[string]interface{}** |  | [optional] 

## Methods

### NewRolloutResponse

`func NewRolloutResponse(channelId string, organizationId string, version int32, createdAt string, updatedAt string, ) *RolloutResponse`

NewRolloutResponse instantiates a new RolloutResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRolloutResponseWithDefaults

`func NewRolloutResponseWithDefaults() *RolloutResponse`

NewRolloutResponseWithDefaults instantiates a new RolloutResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *RolloutResponse) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *RolloutResponse) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *RolloutResponse) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *RolloutResponse) HasId() bool`

HasId returns a boolean if a field has been set.

### GetChannelId

`func (o *RolloutResponse) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *RolloutResponse) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *RolloutResponse) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetOrganizationId

`func (o *RolloutResponse) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *RolloutResponse) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *RolloutResponse) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetInteractionId

`func (o *RolloutResponse) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *RolloutResponse) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *RolloutResponse) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *RolloutResponse) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetRolloutStatuses

`func (o *RolloutResponse) GetRolloutStatuses() []RolloutStatus`

GetRolloutStatuses returns the RolloutStatuses field if non-nil, zero value otherwise.

### GetRolloutStatusesOk

`func (o *RolloutResponse) GetRolloutStatusesOk() (*[]RolloutStatus, bool)`

GetRolloutStatusesOk returns a tuple with the RolloutStatuses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRolloutStatuses

`func (o *RolloutResponse) SetRolloutStatuses(v []RolloutStatus)`

SetRolloutStatuses sets RolloutStatuses field to given value.

### HasRolloutStatuses

`func (o *RolloutResponse) HasRolloutStatuses() bool`

HasRolloutStatuses returns a boolean if a field has been set.

### GetVersion

`func (o *RolloutResponse) GetVersion() int32`

GetVersion returns the Version field if non-nil, zero value otherwise.

### GetVersionOk

`func (o *RolloutResponse) GetVersionOk() (*int32, bool)`

GetVersionOk returns a tuple with the Version field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersion

`func (o *RolloutResponse) SetVersion(v int32)`

SetVersion sets Version field to given value.


### GetVersionMinor

`func (o *RolloutResponse) GetVersionMinor() int32`

GetVersionMinor returns the VersionMinor field if non-nil, zero value otherwise.

### GetVersionMinorOk

`func (o *RolloutResponse) GetVersionMinorOk() (*int32, bool)`

GetVersionMinorOk returns a tuple with the VersionMinor field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersionMinor

`func (o *RolloutResponse) SetVersionMinor(v int32)`

SetVersionMinor sets VersionMinor field to given value.

### HasVersionMinor

`func (o *RolloutResponse) HasVersionMinor() bool`

HasVersionMinor returns a boolean if a field has been set.

### GetLinked

`func (o *RolloutResponse) GetLinked() bool`

GetLinked returns the Linked field if non-nil, zero value otherwise.

### GetLinkedOk

`func (o *RolloutResponse) GetLinkedOk() (*bool, bool)`

GetLinkedOk returns a tuple with the Linked field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLinked

`func (o *RolloutResponse) SetLinked(v bool)`

SetLinked sets Linked field to given value.

### HasLinked

`func (o *RolloutResponse) HasLinked() bool`

HasLinked returns a boolean if a field has been set.

### GetCreatedAt

`func (o *RolloutResponse) GetCreatedAt() string`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *RolloutResponse) GetCreatedAtOk() (*string, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *RolloutResponse) SetCreatedAt(v string)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *RolloutResponse) GetUpdatedAt() string`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *RolloutResponse) GetUpdatedAtOk() (*string, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *RolloutResponse) SetUpdatedAt(v string)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetErrors

`func (o *RolloutResponse) GetErrors() []RolloutError`

GetErrors returns the Errors field if non-nil, zero value otherwise.

### GetErrorsOk

`func (o *RolloutResponse) GetErrorsOk() (*[]RolloutError, bool)`

GetErrorsOk returns a tuple with the Errors field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrors

`func (o *RolloutResponse) SetErrors(v []RolloutError)`

SetErrors sets Errors field to given value.

### HasErrors

`func (o *RolloutResponse) HasErrors() bool`

HasErrors returns a boolean if a field has been set.

### GetScPermissionInfo

`func (o *RolloutResponse) GetScPermissionInfo() map[string]interface{}`

GetScPermissionInfo returns the ScPermissionInfo field if non-nil, zero value otherwise.

### GetScPermissionInfoOk

`func (o *RolloutResponse) GetScPermissionInfoOk() (*map[string]interface{}, bool)`

GetScPermissionInfoOk returns a tuple with the ScPermissionInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScPermissionInfo

`func (o *RolloutResponse) SetScPermissionInfo(v map[string]interface{})`

SetScPermissionInfo sets ScPermissionInfo field to given value.

### HasScPermissionInfo

`func (o *RolloutResponse) HasScPermissionInfo() bool`

HasScPermissionInfo returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


