# Acceptance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**OrganizationId** | Pointer to **string** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 
**ScPermissionId** | Pointer to **string** |  | [optional] 
**RolloutId** | Pointer to **string** |  | [optional] 
**AcceptedAt** | Pointer to **string** |  | [optional] 
**AcceptedBy** | Pointer to **string** |  | [optional] 
**CreatedAt** | Pointer to **string** |  | [optional] 
**UpdatedAt** | Pointer to **string** |  | [optional] 

## Methods

### NewAcceptance

`func NewAcceptance() *Acceptance`

NewAcceptance instantiates a new Acceptance object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAcceptanceWithDefaults

`func NewAcceptanceWithDefaults() *Acceptance`

NewAcceptanceWithDefaults instantiates a new Acceptance object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Acceptance) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Acceptance) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Acceptance) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *Acceptance) HasId() bool`

HasId returns a boolean if a field has been set.

### GetOrganizationId

`func (o *Acceptance) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *Acceptance) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *Acceptance) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *Acceptance) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetChannelId

`func (o *Acceptance) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *Acceptance) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *Acceptance) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *Acceptance) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### GetScPermissionId

`func (o *Acceptance) GetScPermissionId() string`

GetScPermissionId returns the ScPermissionId field if non-nil, zero value otherwise.

### GetScPermissionIdOk

`func (o *Acceptance) GetScPermissionIdOk() (*string, bool)`

GetScPermissionIdOk returns a tuple with the ScPermissionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScPermissionId

`func (o *Acceptance) SetScPermissionId(v string)`

SetScPermissionId sets ScPermissionId field to given value.

### HasScPermissionId

`func (o *Acceptance) HasScPermissionId() bool`

HasScPermissionId returns a boolean if a field has been set.

### GetRolloutId

`func (o *Acceptance) GetRolloutId() string`

GetRolloutId returns the RolloutId field if non-nil, zero value otherwise.

### GetRolloutIdOk

`func (o *Acceptance) GetRolloutIdOk() (*string, bool)`

GetRolloutIdOk returns a tuple with the RolloutId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRolloutId

`func (o *Acceptance) SetRolloutId(v string)`

SetRolloutId sets RolloutId field to given value.

### HasRolloutId

`func (o *Acceptance) HasRolloutId() bool`

HasRolloutId returns a boolean if a field has been set.

### GetAcceptedAt

`func (o *Acceptance) GetAcceptedAt() string`

GetAcceptedAt returns the AcceptedAt field if non-nil, zero value otherwise.

### GetAcceptedAtOk

`func (o *Acceptance) GetAcceptedAtOk() (*string, bool)`

GetAcceptedAtOk returns a tuple with the AcceptedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAcceptedAt

`func (o *Acceptance) SetAcceptedAt(v string)`

SetAcceptedAt sets AcceptedAt field to given value.

### HasAcceptedAt

`func (o *Acceptance) HasAcceptedAt() bool`

HasAcceptedAt returns a boolean if a field has been set.

### GetAcceptedBy

`func (o *Acceptance) GetAcceptedBy() string`

GetAcceptedBy returns the AcceptedBy field if non-nil, zero value otherwise.

### GetAcceptedByOk

`func (o *Acceptance) GetAcceptedByOk() (*string, bool)`

GetAcceptedByOk returns a tuple with the AcceptedBy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAcceptedBy

`func (o *Acceptance) SetAcceptedBy(v string)`

SetAcceptedBy sets AcceptedBy field to given value.

### HasAcceptedBy

`func (o *Acceptance) HasAcceptedBy() bool`

HasAcceptedBy returns a boolean if a field has been set.

### GetCreatedAt

`func (o *Acceptance) GetCreatedAt() string`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Acceptance) GetCreatedAtOk() (*string, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Acceptance) SetCreatedAt(v string)`

SetCreatedAt sets CreatedAt field to given value.

### HasCreatedAt

`func (o *Acceptance) HasCreatedAt() bool`

HasCreatedAt returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *Acceptance) GetUpdatedAt() string`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Acceptance) GetUpdatedAtOk() (*string, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Acceptance) SetUpdatedAt(v string)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *Acceptance) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


