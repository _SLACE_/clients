# CxTemplateInteractionsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**PreviewUrls** | Pointer to [**[]CxTemplateInteractionsInnerPreviewUrlsInner**](CxTemplateInteractionsInnerPreviewUrlsInner.md) |  | [optional] 
**Triggers** | Pointer to **[]string** |  | [optional] 

## Methods

### NewCxTemplateInteractionsInner

`func NewCxTemplateInteractionsInner() *CxTemplateInteractionsInner`

NewCxTemplateInteractionsInner instantiates a new CxTemplateInteractionsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateInteractionsInnerWithDefaults

`func NewCxTemplateInteractionsInnerWithDefaults() *CxTemplateInteractionsInner`

NewCxTemplateInteractionsInnerWithDefaults instantiates a new CxTemplateInteractionsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CxTemplateInteractionsInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CxTemplateInteractionsInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CxTemplateInteractionsInner) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *CxTemplateInteractionsInner) HasId() bool`

HasId returns a boolean if a field has been set.

### GetName

`func (o *CxTemplateInteractionsInner) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CxTemplateInteractionsInner) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CxTemplateInteractionsInner) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *CxTemplateInteractionsInner) HasName() bool`

HasName returns a boolean if a field has been set.

### GetPreviewUrls

`func (o *CxTemplateInteractionsInner) GetPreviewUrls() []CxTemplateInteractionsInnerPreviewUrlsInner`

GetPreviewUrls returns the PreviewUrls field if non-nil, zero value otherwise.

### GetPreviewUrlsOk

`func (o *CxTemplateInteractionsInner) GetPreviewUrlsOk() (*[]CxTemplateInteractionsInnerPreviewUrlsInner, bool)`

GetPreviewUrlsOk returns a tuple with the PreviewUrls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPreviewUrls

`func (o *CxTemplateInteractionsInner) SetPreviewUrls(v []CxTemplateInteractionsInnerPreviewUrlsInner)`

SetPreviewUrls sets PreviewUrls field to given value.

### HasPreviewUrls

`func (o *CxTemplateInteractionsInner) HasPreviewUrls() bool`

HasPreviewUrls returns a boolean if a field has been set.

### GetTriggers

`func (o *CxTemplateInteractionsInner) GetTriggers() []string`

GetTriggers returns the Triggers field if non-nil, zero value otherwise.

### GetTriggersOk

`func (o *CxTemplateInteractionsInner) GetTriggersOk() (*[]string, bool)`

GetTriggersOk returns a tuple with the Triggers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTriggers

`func (o *CxTemplateInteractionsInner) SetTriggers(v []string)`

SetTriggers sets Triggers field to given value.

### HasTriggers

`func (o *CxTemplateInteractionsInner) HasTriggers() bool`

HasTriggers returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


