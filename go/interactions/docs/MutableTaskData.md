# MutableTaskData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Text** | Pointer to **string** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 
**ContextMap** | Pointer to **map[string]interface{}** |  | [optional] 
**Title** | Pointer to **string** |  | [optional] 
**Blocker** | Pointer to **bool** |  | [optional] 
**DestinationLink** | Pointer to **string** |  | [optional] 
**Action** | Pointer to **string** |  | [optional] 
**Usages** | Pointer to **[]string** |  | [optional] 
**Element** | Pointer to [**MutableTaskDataElement**](MutableTaskDataElement.md) |  | [optional] 
**ScriptId** | Pointer to **string** |  | [optional] 

## Methods

### NewMutableTaskData

`func NewMutableTaskData() *MutableTaskData`

NewMutableTaskData instantiates a new MutableTaskData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMutableTaskDataWithDefaults

`func NewMutableTaskDataWithDefaults() *MutableTaskData`

NewMutableTaskDataWithDefaults instantiates a new MutableTaskData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetText

`func (o *MutableTaskData) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *MutableTaskData) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *MutableTaskData) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *MutableTaskData) HasText() bool`

HasText returns a boolean if a field has been set.

### GetType

`func (o *MutableTaskData) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MutableTaskData) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MutableTaskData) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *MutableTaskData) HasType() bool`

HasType returns a boolean if a field has been set.

### GetContextMap

`func (o *MutableTaskData) GetContextMap() map[string]interface{}`

GetContextMap returns the ContextMap field if non-nil, zero value otherwise.

### GetContextMapOk

`func (o *MutableTaskData) GetContextMapOk() (*map[string]interface{}, bool)`

GetContextMapOk returns a tuple with the ContextMap field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContextMap

`func (o *MutableTaskData) SetContextMap(v map[string]interface{})`

SetContextMap sets ContextMap field to given value.

### HasContextMap

`func (o *MutableTaskData) HasContextMap() bool`

HasContextMap returns a boolean if a field has been set.

### GetTitle

`func (o *MutableTaskData) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *MutableTaskData) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *MutableTaskData) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *MutableTaskData) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetBlocker

`func (o *MutableTaskData) GetBlocker() bool`

GetBlocker returns the Blocker field if non-nil, zero value otherwise.

### GetBlockerOk

`func (o *MutableTaskData) GetBlockerOk() (*bool, bool)`

GetBlockerOk returns a tuple with the Blocker field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlocker

`func (o *MutableTaskData) SetBlocker(v bool)`

SetBlocker sets Blocker field to given value.

### HasBlocker

`func (o *MutableTaskData) HasBlocker() bool`

HasBlocker returns a boolean if a field has been set.

### GetDestinationLink

`func (o *MutableTaskData) GetDestinationLink() string`

GetDestinationLink returns the DestinationLink field if non-nil, zero value otherwise.

### GetDestinationLinkOk

`func (o *MutableTaskData) GetDestinationLinkOk() (*string, bool)`

GetDestinationLinkOk returns a tuple with the DestinationLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDestinationLink

`func (o *MutableTaskData) SetDestinationLink(v string)`

SetDestinationLink sets DestinationLink field to given value.

### HasDestinationLink

`func (o *MutableTaskData) HasDestinationLink() bool`

HasDestinationLink returns a boolean if a field has been set.

### GetAction

`func (o *MutableTaskData) GetAction() string`

GetAction returns the Action field if non-nil, zero value otherwise.

### GetActionOk

`func (o *MutableTaskData) GetActionOk() (*string, bool)`

GetActionOk returns a tuple with the Action field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAction

`func (o *MutableTaskData) SetAction(v string)`

SetAction sets Action field to given value.

### HasAction

`func (o *MutableTaskData) HasAction() bool`

HasAction returns a boolean if a field has been set.

### GetUsages

`func (o *MutableTaskData) GetUsages() []string`

GetUsages returns the Usages field if non-nil, zero value otherwise.

### GetUsagesOk

`func (o *MutableTaskData) GetUsagesOk() (*[]string, bool)`

GetUsagesOk returns a tuple with the Usages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsages

`func (o *MutableTaskData) SetUsages(v []string)`

SetUsages sets Usages field to given value.

### HasUsages

`func (o *MutableTaskData) HasUsages() bool`

HasUsages returns a boolean if a field has been set.

### GetElement

`func (o *MutableTaskData) GetElement() MutableTaskDataElement`

GetElement returns the Element field if non-nil, zero value otherwise.

### GetElementOk

`func (o *MutableTaskData) GetElementOk() (*MutableTaskDataElement, bool)`

GetElementOk returns a tuple with the Element field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetElement

`func (o *MutableTaskData) SetElement(v MutableTaskDataElement)`

SetElement sets Element field to given value.

### HasElement

`func (o *MutableTaskData) HasElement() bool`

HasElement returns a boolean if a field has been set.

### GetScriptId

`func (o *MutableTaskData) GetScriptId() string`

GetScriptId returns the ScriptId field if non-nil, zero value otherwise.

### GetScriptIdOk

`func (o *MutableTaskData) GetScriptIdOk() (*string, bool)`

GetScriptIdOk returns a tuple with the ScriptId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScriptId

`func (o *MutableTaskData) SetScriptId(v string)`

SetScriptId sets ScriptId field to given value.

### HasScriptId

`func (o *MutableTaskData) HasScriptId() bool`

HasScriptId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


