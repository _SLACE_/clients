# TaskCompletion

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Status** | Pointer to **string** |  | [optional] [default to "open"]
**CampaignSendoutId** | Pointer to **string** |  | [optional] 
**CreatedAt** | Pointer to **string** |  | [optional] 
**UpdatedAt** | Pointer to **string** |  | [optional] 
**DoneAt** | Pointer to **string** |  | [optional] 
**DoneBy** | Pointer to **string** |  | [optional] 
**Deadline** | Pointer to **string** |  | [optional] 
**NextReminderAt** | Pointer to **string** |  | [optional] 
**LastReminderAt** | Pointer to **string** |  | [optional] 
**Task** | Pointer to [**Task**](Task.md) |  | [optional] 
**TaskId** | Pointer to **string** |  | [optional] 

## Methods

### NewTaskCompletion

`func NewTaskCompletion(id string, ) *TaskCompletion`

NewTaskCompletion instantiates a new TaskCompletion object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTaskCompletionWithDefaults

`func NewTaskCompletionWithDefaults() *TaskCompletion`

NewTaskCompletionWithDefaults instantiates a new TaskCompletion object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *TaskCompletion) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *TaskCompletion) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *TaskCompletion) SetId(v string)`

SetId sets Id field to given value.


### GetStatus

`func (o *TaskCompletion) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *TaskCompletion) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *TaskCompletion) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *TaskCompletion) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetCampaignSendoutId

`func (o *TaskCompletion) GetCampaignSendoutId() string`

GetCampaignSendoutId returns the CampaignSendoutId field if non-nil, zero value otherwise.

### GetCampaignSendoutIdOk

`func (o *TaskCompletion) GetCampaignSendoutIdOk() (*string, bool)`

GetCampaignSendoutIdOk returns a tuple with the CampaignSendoutId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCampaignSendoutId

`func (o *TaskCompletion) SetCampaignSendoutId(v string)`

SetCampaignSendoutId sets CampaignSendoutId field to given value.

### HasCampaignSendoutId

`func (o *TaskCompletion) HasCampaignSendoutId() bool`

HasCampaignSendoutId returns a boolean if a field has been set.

### GetCreatedAt

`func (o *TaskCompletion) GetCreatedAt() string`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *TaskCompletion) GetCreatedAtOk() (*string, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *TaskCompletion) SetCreatedAt(v string)`

SetCreatedAt sets CreatedAt field to given value.

### HasCreatedAt

`func (o *TaskCompletion) HasCreatedAt() bool`

HasCreatedAt returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *TaskCompletion) GetUpdatedAt() string`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *TaskCompletion) GetUpdatedAtOk() (*string, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *TaskCompletion) SetUpdatedAt(v string)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *TaskCompletion) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.

### GetDoneAt

`func (o *TaskCompletion) GetDoneAt() string`

GetDoneAt returns the DoneAt field if non-nil, zero value otherwise.

### GetDoneAtOk

`func (o *TaskCompletion) GetDoneAtOk() (*string, bool)`

GetDoneAtOk returns a tuple with the DoneAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDoneAt

`func (o *TaskCompletion) SetDoneAt(v string)`

SetDoneAt sets DoneAt field to given value.

### HasDoneAt

`func (o *TaskCompletion) HasDoneAt() bool`

HasDoneAt returns a boolean if a field has been set.

### GetDoneBy

`func (o *TaskCompletion) GetDoneBy() string`

GetDoneBy returns the DoneBy field if non-nil, zero value otherwise.

### GetDoneByOk

`func (o *TaskCompletion) GetDoneByOk() (*string, bool)`

GetDoneByOk returns a tuple with the DoneBy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDoneBy

`func (o *TaskCompletion) SetDoneBy(v string)`

SetDoneBy sets DoneBy field to given value.

### HasDoneBy

`func (o *TaskCompletion) HasDoneBy() bool`

HasDoneBy returns a boolean if a field has been set.

### GetDeadline

`func (o *TaskCompletion) GetDeadline() string`

GetDeadline returns the Deadline field if non-nil, zero value otherwise.

### GetDeadlineOk

`func (o *TaskCompletion) GetDeadlineOk() (*string, bool)`

GetDeadlineOk returns a tuple with the Deadline field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDeadline

`func (o *TaskCompletion) SetDeadline(v string)`

SetDeadline sets Deadline field to given value.

### HasDeadline

`func (o *TaskCompletion) HasDeadline() bool`

HasDeadline returns a boolean if a field has been set.

### GetNextReminderAt

`func (o *TaskCompletion) GetNextReminderAt() string`

GetNextReminderAt returns the NextReminderAt field if non-nil, zero value otherwise.

### GetNextReminderAtOk

`func (o *TaskCompletion) GetNextReminderAtOk() (*string, bool)`

GetNextReminderAtOk returns a tuple with the NextReminderAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNextReminderAt

`func (o *TaskCompletion) SetNextReminderAt(v string)`

SetNextReminderAt sets NextReminderAt field to given value.

### HasNextReminderAt

`func (o *TaskCompletion) HasNextReminderAt() bool`

HasNextReminderAt returns a boolean if a field has been set.

### GetLastReminderAt

`func (o *TaskCompletion) GetLastReminderAt() string`

GetLastReminderAt returns the LastReminderAt field if non-nil, zero value otherwise.

### GetLastReminderAtOk

`func (o *TaskCompletion) GetLastReminderAtOk() (*string, bool)`

GetLastReminderAtOk returns a tuple with the LastReminderAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastReminderAt

`func (o *TaskCompletion) SetLastReminderAt(v string)`

SetLastReminderAt sets LastReminderAt field to given value.

### HasLastReminderAt

`func (o *TaskCompletion) HasLastReminderAt() bool`

HasLastReminderAt returns a boolean if a field has been set.

### GetTask

`func (o *TaskCompletion) GetTask() Task`

GetTask returns the Task field if non-nil, zero value otherwise.

### GetTaskOk

`func (o *TaskCompletion) GetTaskOk() (*Task, bool)`

GetTaskOk returns a tuple with the Task field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTask

`func (o *TaskCompletion) SetTask(v Task)`

SetTask sets Task field to given value.

### HasTask

`func (o *TaskCompletion) HasTask() bool`

HasTask returns a boolean if a field has been set.

### GetTaskId

`func (o *TaskCompletion) GetTaskId() string`

GetTaskId returns the TaskId field if non-nil, zero value otherwise.

### GetTaskIdOk

`func (o *TaskCompletion) GetTaskIdOk() (*string, bool)`

GetTaskIdOk returns a tuple with the TaskId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTaskId

`func (o *TaskCompletion) SetTaskId(v string)`

SetTaskId sets TaskId field to given value.

### HasTaskId

`func (o *TaskCompletion) HasTaskId() bool`

HasTaskId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


