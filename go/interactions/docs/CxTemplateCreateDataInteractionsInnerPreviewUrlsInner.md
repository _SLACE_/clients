# CxTemplateCreateDataInteractionsInnerPreviewUrlsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | Pointer to **string** |  | [optional] 
**Language** | Pointer to **string** |  | [optional] 

## Methods

### NewCxTemplateCreateDataInteractionsInnerPreviewUrlsInner

`func NewCxTemplateCreateDataInteractionsInnerPreviewUrlsInner() *CxTemplateCreateDataInteractionsInnerPreviewUrlsInner`

NewCxTemplateCreateDataInteractionsInnerPreviewUrlsInner instantiates a new CxTemplateCreateDataInteractionsInnerPreviewUrlsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateCreateDataInteractionsInnerPreviewUrlsInnerWithDefaults

`func NewCxTemplateCreateDataInteractionsInnerPreviewUrlsInnerWithDefaults() *CxTemplateCreateDataInteractionsInnerPreviewUrlsInner`

NewCxTemplateCreateDataInteractionsInnerPreviewUrlsInnerWithDefaults instantiates a new CxTemplateCreateDataInteractionsInnerPreviewUrlsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUrl

`func (o *CxTemplateCreateDataInteractionsInnerPreviewUrlsInner) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *CxTemplateCreateDataInteractionsInnerPreviewUrlsInner) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *CxTemplateCreateDataInteractionsInnerPreviewUrlsInner) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *CxTemplateCreateDataInteractionsInnerPreviewUrlsInner) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetLanguage

`func (o *CxTemplateCreateDataInteractionsInnerPreviewUrlsInner) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *CxTemplateCreateDataInteractionsInnerPreviewUrlsInner) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *CxTemplateCreateDataInteractionsInnerPreviewUrlsInner) SetLanguage(v string)`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *CxTemplateCreateDataInteractionsInnerPreviewUrlsInner) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


