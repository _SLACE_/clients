# ReportConfigurationMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**Scope** | Pointer to [**ReportConfigurationScope**](ReportConfigurationScope.md) |  | [optional] 
**Data** | Pointer to [**ReportConfigurationData**](ReportConfigurationData.md) |  | [optional] 
**Active** | Pointer to **bool** | Applies to global &amp; organization scopes | [optional] 

## Methods

### NewReportConfigurationMutable

`func NewReportConfigurationMutable(name string, ) *ReportConfigurationMutable`

NewReportConfigurationMutable instantiates a new ReportConfigurationMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReportConfigurationMutableWithDefaults

`func NewReportConfigurationMutableWithDefaults() *ReportConfigurationMutable`

NewReportConfigurationMutableWithDefaults instantiates a new ReportConfigurationMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ReportConfigurationMutable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ReportConfigurationMutable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ReportConfigurationMutable) SetName(v string)`

SetName sets Name field to given value.


### GetScope

`func (o *ReportConfigurationMutable) GetScope() ReportConfigurationScope`

GetScope returns the Scope field if non-nil, zero value otherwise.

### GetScopeOk

`func (o *ReportConfigurationMutable) GetScopeOk() (*ReportConfigurationScope, bool)`

GetScopeOk returns a tuple with the Scope field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScope

`func (o *ReportConfigurationMutable) SetScope(v ReportConfigurationScope)`

SetScope sets Scope field to given value.

### HasScope

`func (o *ReportConfigurationMutable) HasScope() bool`

HasScope returns a boolean if a field has been set.

### GetData

`func (o *ReportConfigurationMutable) GetData() ReportConfigurationData`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *ReportConfigurationMutable) GetDataOk() (*ReportConfigurationData, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *ReportConfigurationMutable) SetData(v ReportConfigurationData)`

SetData sets Data field to given value.

### HasData

`func (o *ReportConfigurationMutable) HasData() bool`

HasData returns a boolean if a field has been set.

### GetActive

`func (o *ReportConfigurationMutable) GetActive() bool`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *ReportConfigurationMutable) GetActiveOk() (*bool, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *ReportConfigurationMutable) SetActive(v bool)`

SetActive sets Active field to given value.

### HasActive

`func (o *ReportConfigurationMutable) HasActive() bool`

HasActive returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


