# ReportFilters

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RolloutId** | Pointer to **string** | Rollout ID | [optional] 
**ResultType** | Pointer to **string** | Result Type | [optional] 
**ChannelIds** | Pointer to **[]string** | List of Channel IDs | [optional] 
**IxIds** | Pointer to **[]string** | List of Interaction IDs | [optional] 
**LocationIds** | Pointer to **[]string** | List of Location IDs | [optional] 
**EpIds** | Pointer to **[]string** | List of Entry Point IDs | [optional] 
**Messengers** | Pointer to **[]string** | List of Messengers | [optional] 
**TriggerTypes** | Pointer to **[]string** | List of Trigger Types | [optional] 
**EntryTypes** | Pointer to **[]string** | List of Entry Types | [optional] 
**Tags** | Pointer to **[]string** | List of Tags | [optional] 
**Languages** | Pointer to **[]string** | List of Languages | [optional] 
**Segments** | Pointer to **[]string** | List of Segments | [optional] 
**Labels** | Pointer to **[]string** | List of Labels | [optional] 
**DeviceBrands** | Pointer to **[]string** | List of Device Brands | [optional] 
**BrowserNames** | Pointer to **[]string** | List of Browser Names | [optional] 
**OsNames** | Pointer to **[]string** | List of Operating System Names | [optional] 
**CountryCodes** | Pointer to **[]string** | List of Country Codes | [optional] 
**Versions** | Pointer to **[]string** | List of Versions | [optional] 
**ContextFields** | Pointer to **[]string** | List of Context Fields | [optional] 
**ContextValues** | Pointer to **[]string** | List of Context Values | [optional] 
**GroupBy** | Pointer to [**[]ReportGroupBy**](ReportGroupBy.md) | Fields to Group By | [optional] 
**GroupByContextField** | Pointer to **string** | Context Field to Group By | [optional] 
**From** | Pointer to **time.Time** | Start Time | [optional] 
**To** | Pointer to **time.Time** | End Time | [optional] 
**Sort** | Pointer to **[]string** | Fields to Sort By | [optional] 
**Limit** | Pointer to **int32** | Maximum number of records to return | [optional] 
**GroupLimit** | Pointer to **int32** | Maximum number of groups to return | [optional] 
**Offset** | Pointer to **int32** | Number of records to skip | [optional] 

## Methods

### NewReportFilters

`func NewReportFilters() *ReportFilters`

NewReportFilters instantiates a new ReportFilters object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReportFiltersWithDefaults

`func NewReportFiltersWithDefaults() *ReportFilters`

NewReportFiltersWithDefaults instantiates a new ReportFilters object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRolloutId

`func (o *ReportFilters) GetRolloutId() string`

GetRolloutId returns the RolloutId field if non-nil, zero value otherwise.

### GetRolloutIdOk

`func (o *ReportFilters) GetRolloutIdOk() (*string, bool)`

GetRolloutIdOk returns a tuple with the RolloutId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRolloutId

`func (o *ReportFilters) SetRolloutId(v string)`

SetRolloutId sets RolloutId field to given value.

### HasRolloutId

`func (o *ReportFilters) HasRolloutId() bool`

HasRolloutId returns a boolean if a field has been set.

### GetResultType

`func (o *ReportFilters) GetResultType() string`

GetResultType returns the ResultType field if non-nil, zero value otherwise.

### GetResultTypeOk

`func (o *ReportFilters) GetResultTypeOk() (*string, bool)`

GetResultTypeOk returns a tuple with the ResultType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResultType

`func (o *ReportFilters) SetResultType(v string)`

SetResultType sets ResultType field to given value.

### HasResultType

`func (o *ReportFilters) HasResultType() bool`

HasResultType returns a boolean if a field has been set.

### GetChannelIds

`func (o *ReportFilters) GetChannelIds() []string`

GetChannelIds returns the ChannelIds field if non-nil, zero value otherwise.

### GetChannelIdsOk

`func (o *ReportFilters) GetChannelIdsOk() (*[]string, bool)`

GetChannelIdsOk returns a tuple with the ChannelIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelIds

`func (o *ReportFilters) SetChannelIds(v []string)`

SetChannelIds sets ChannelIds field to given value.

### HasChannelIds

`func (o *ReportFilters) HasChannelIds() bool`

HasChannelIds returns a boolean if a field has been set.

### GetIxIds

`func (o *ReportFilters) GetIxIds() []string`

GetIxIds returns the IxIds field if non-nil, zero value otherwise.

### GetIxIdsOk

`func (o *ReportFilters) GetIxIdsOk() (*[]string, bool)`

GetIxIdsOk returns a tuple with the IxIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIxIds

`func (o *ReportFilters) SetIxIds(v []string)`

SetIxIds sets IxIds field to given value.

### HasIxIds

`func (o *ReportFilters) HasIxIds() bool`

HasIxIds returns a boolean if a field has been set.

### GetLocationIds

`func (o *ReportFilters) GetLocationIds() []string`

GetLocationIds returns the LocationIds field if non-nil, zero value otherwise.

### GetLocationIdsOk

`func (o *ReportFilters) GetLocationIdsOk() (*[]string, bool)`

GetLocationIdsOk returns a tuple with the LocationIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationIds

`func (o *ReportFilters) SetLocationIds(v []string)`

SetLocationIds sets LocationIds field to given value.

### HasLocationIds

`func (o *ReportFilters) HasLocationIds() bool`

HasLocationIds returns a boolean if a field has been set.

### GetEpIds

`func (o *ReportFilters) GetEpIds() []string`

GetEpIds returns the EpIds field if non-nil, zero value otherwise.

### GetEpIdsOk

`func (o *ReportFilters) GetEpIdsOk() (*[]string, bool)`

GetEpIdsOk returns a tuple with the EpIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEpIds

`func (o *ReportFilters) SetEpIds(v []string)`

SetEpIds sets EpIds field to given value.

### HasEpIds

`func (o *ReportFilters) HasEpIds() bool`

HasEpIds returns a boolean if a field has been set.

### GetMessengers

`func (o *ReportFilters) GetMessengers() []string`

GetMessengers returns the Messengers field if non-nil, zero value otherwise.

### GetMessengersOk

`func (o *ReportFilters) GetMessengersOk() (*[]string, bool)`

GetMessengersOk returns a tuple with the Messengers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengers

`func (o *ReportFilters) SetMessengers(v []string)`

SetMessengers sets Messengers field to given value.

### HasMessengers

`func (o *ReportFilters) HasMessengers() bool`

HasMessengers returns a boolean if a field has been set.

### GetTriggerTypes

`func (o *ReportFilters) GetTriggerTypes() []string`

GetTriggerTypes returns the TriggerTypes field if non-nil, zero value otherwise.

### GetTriggerTypesOk

`func (o *ReportFilters) GetTriggerTypesOk() (*[]string, bool)`

GetTriggerTypesOk returns a tuple with the TriggerTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTriggerTypes

`func (o *ReportFilters) SetTriggerTypes(v []string)`

SetTriggerTypes sets TriggerTypes field to given value.

### HasTriggerTypes

`func (o *ReportFilters) HasTriggerTypes() bool`

HasTriggerTypes returns a boolean if a field has been set.

### GetEntryTypes

`func (o *ReportFilters) GetEntryTypes() []string`

GetEntryTypes returns the EntryTypes field if non-nil, zero value otherwise.

### GetEntryTypesOk

`func (o *ReportFilters) GetEntryTypesOk() (*[]string, bool)`

GetEntryTypesOk returns a tuple with the EntryTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEntryTypes

`func (o *ReportFilters) SetEntryTypes(v []string)`

SetEntryTypes sets EntryTypes field to given value.

### HasEntryTypes

`func (o *ReportFilters) HasEntryTypes() bool`

HasEntryTypes returns a boolean if a field has been set.

### GetTags

`func (o *ReportFilters) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *ReportFilters) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *ReportFilters) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *ReportFilters) HasTags() bool`

HasTags returns a boolean if a field has been set.

### GetLanguages

`func (o *ReportFilters) GetLanguages() []string`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *ReportFilters) GetLanguagesOk() (*[]string, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *ReportFilters) SetLanguages(v []string)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *ReportFilters) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetSegments

`func (o *ReportFilters) GetSegments() []string`

GetSegments returns the Segments field if non-nil, zero value otherwise.

### GetSegmentsOk

`func (o *ReportFilters) GetSegmentsOk() (*[]string, bool)`

GetSegmentsOk returns a tuple with the Segments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSegments

`func (o *ReportFilters) SetSegments(v []string)`

SetSegments sets Segments field to given value.

### HasSegments

`func (o *ReportFilters) HasSegments() bool`

HasSegments returns a boolean if a field has been set.

### GetLabels

`func (o *ReportFilters) GetLabels() []string`

GetLabels returns the Labels field if non-nil, zero value otherwise.

### GetLabelsOk

`func (o *ReportFilters) GetLabelsOk() (*[]string, bool)`

GetLabelsOk returns a tuple with the Labels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabels

`func (o *ReportFilters) SetLabels(v []string)`

SetLabels sets Labels field to given value.

### HasLabels

`func (o *ReportFilters) HasLabels() bool`

HasLabels returns a boolean if a field has been set.

### GetDeviceBrands

`func (o *ReportFilters) GetDeviceBrands() []string`

GetDeviceBrands returns the DeviceBrands field if non-nil, zero value otherwise.

### GetDeviceBrandsOk

`func (o *ReportFilters) GetDeviceBrandsOk() (*[]string, bool)`

GetDeviceBrandsOk returns a tuple with the DeviceBrands field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDeviceBrands

`func (o *ReportFilters) SetDeviceBrands(v []string)`

SetDeviceBrands sets DeviceBrands field to given value.

### HasDeviceBrands

`func (o *ReportFilters) HasDeviceBrands() bool`

HasDeviceBrands returns a boolean if a field has been set.

### GetBrowserNames

`func (o *ReportFilters) GetBrowserNames() []string`

GetBrowserNames returns the BrowserNames field if non-nil, zero value otherwise.

### GetBrowserNamesOk

`func (o *ReportFilters) GetBrowserNamesOk() (*[]string, bool)`

GetBrowserNamesOk returns a tuple with the BrowserNames field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBrowserNames

`func (o *ReportFilters) SetBrowserNames(v []string)`

SetBrowserNames sets BrowserNames field to given value.

### HasBrowserNames

`func (o *ReportFilters) HasBrowserNames() bool`

HasBrowserNames returns a boolean if a field has been set.

### GetOsNames

`func (o *ReportFilters) GetOsNames() []string`

GetOsNames returns the OsNames field if non-nil, zero value otherwise.

### GetOsNamesOk

`func (o *ReportFilters) GetOsNamesOk() (*[]string, bool)`

GetOsNamesOk returns a tuple with the OsNames field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOsNames

`func (o *ReportFilters) SetOsNames(v []string)`

SetOsNames sets OsNames field to given value.

### HasOsNames

`func (o *ReportFilters) HasOsNames() bool`

HasOsNames returns a boolean if a field has been set.

### GetCountryCodes

`func (o *ReportFilters) GetCountryCodes() []string`

GetCountryCodes returns the CountryCodes field if non-nil, zero value otherwise.

### GetCountryCodesOk

`func (o *ReportFilters) GetCountryCodesOk() (*[]string, bool)`

GetCountryCodesOk returns a tuple with the CountryCodes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountryCodes

`func (o *ReportFilters) SetCountryCodes(v []string)`

SetCountryCodes sets CountryCodes field to given value.

### HasCountryCodes

`func (o *ReportFilters) HasCountryCodes() bool`

HasCountryCodes returns a boolean if a field has been set.

### GetVersions

`func (o *ReportFilters) GetVersions() []string`

GetVersions returns the Versions field if non-nil, zero value otherwise.

### GetVersionsOk

`func (o *ReportFilters) GetVersionsOk() (*[]string, bool)`

GetVersionsOk returns a tuple with the Versions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVersions

`func (o *ReportFilters) SetVersions(v []string)`

SetVersions sets Versions field to given value.

### HasVersions

`func (o *ReportFilters) HasVersions() bool`

HasVersions returns a boolean if a field has been set.

### GetContextFields

`func (o *ReportFilters) GetContextFields() []string`

GetContextFields returns the ContextFields field if non-nil, zero value otherwise.

### GetContextFieldsOk

`func (o *ReportFilters) GetContextFieldsOk() (*[]string, bool)`

GetContextFieldsOk returns a tuple with the ContextFields field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContextFields

`func (o *ReportFilters) SetContextFields(v []string)`

SetContextFields sets ContextFields field to given value.

### HasContextFields

`func (o *ReportFilters) HasContextFields() bool`

HasContextFields returns a boolean if a field has been set.

### GetContextValues

`func (o *ReportFilters) GetContextValues() []string`

GetContextValues returns the ContextValues field if non-nil, zero value otherwise.

### GetContextValuesOk

`func (o *ReportFilters) GetContextValuesOk() (*[]string, bool)`

GetContextValuesOk returns a tuple with the ContextValues field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContextValues

`func (o *ReportFilters) SetContextValues(v []string)`

SetContextValues sets ContextValues field to given value.

### HasContextValues

`func (o *ReportFilters) HasContextValues() bool`

HasContextValues returns a boolean if a field has been set.

### GetGroupBy

`func (o *ReportFilters) GetGroupBy() []ReportGroupBy`

GetGroupBy returns the GroupBy field if non-nil, zero value otherwise.

### GetGroupByOk

`func (o *ReportFilters) GetGroupByOk() (*[]ReportGroupBy, bool)`

GetGroupByOk returns a tuple with the GroupBy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGroupBy

`func (o *ReportFilters) SetGroupBy(v []ReportGroupBy)`

SetGroupBy sets GroupBy field to given value.

### HasGroupBy

`func (o *ReportFilters) HasGroupBy() bool`

HasGroupBy returns a boolean if a field has been set.

### GetGroupByContextField

`func (o *ReportFilters) GetGroupByContextField() string`

GetGroupByContextField returns the GroupByContextField field if non-nil, zero value otherwise.

### GetGroupByContextFieldOk

`func (o *ReportFilters) GetGroupByContextFieldOk() (*string, bool)`

GetGroupByContextFieldOk returns a tuple with the GroupByContextField field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGroupByContextField

`func (o *ReportFilters) SetGroupByContextField(v string)`

SetGroupByContextField sets GroupByContextField field to given value.

### HasGroupByContextField

`func (o *ReportFilters) HasGroupByContextField() bool`

HasGroupByContextField returns a boolean if a field has been set.

### GetFrom

`func (o *ReportFilters) GetFrom() time.Time`

GetFrom returns the From field if non-nil, zero value otherwise.

### GetFromOk

`func (o *ReportFilters) GetFromOk() (*time.Time, bool)`

GetFromOk returns a tuple with the From field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFrom

`func (o *ReportFilters) SetFrom(v time.Time)`

SetFrom sets From field to given value.

### HasFrom

`func (o *ReportFilters) HasFrom() bool`

HasFrom returns a boolean if a field has been set.

### GetTo

`func (o *ReportFilters) GetTo() time.Time`

GetTo returns the To field if non-nil, zero value otherwise.

### GetToOk

`func (o *ReportFilters) GetToOk() (*time.Time, bool)`

GetToOk returns a tuple with the To field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTo

`func (o *ReportFilters) SetTo(v time.Time)`

SetTo sets To field to given value.

### HasTo

`func (o *ReportFilters) HasTo() bool`

HasTo returns a boolean if a field has been set.

### GetSort

`func (o *ReportFilters) GetSort() []string`

GetSort returns the Sort field if non-nil, zero value otherwise.

### GetSortOk

`func (o *ReportFilters) GetSortOk() (*[]string, bool)`

GetSortOk returns a tuple with the Sort field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSort

`func (o *ReportFilters) SetSort(v []string)`

SetSort sets Sort field to given value.

### HasSort

`func (o *ReportFilters) HasSort() bool`

HasSort returns a boolean if a field has been set.

### GetLimit

`func (o *ReportFilters) GetLimit() int32`

GetLimit returns the Limit field if non-nil, zero value otherwise.

### GetLimitOk

`func (o *ReportFilters) GetLimitOk() (*int32, bool)`

GetLimitOk returns a tuple with the Limit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLimit

`func (o *ReportFilters) SetLimit(v int32)`

SetLimit sets Limit field to given value.

### HasLimit

`func (o *ReportFilters) HasLimit() bool`

HasLimit returns a boolean if a field has been set.

### GetGroupLimit

`func (o *ReportFilters) GetGroupLimit() int32`

GetGroupLimit returns the GroupLimit field if non-nil, zero value otherwise.

### GetGroupLimitOk

`func (o *ReportFilters) GetGroupLimitOk() (*int32, bool)`

GetGroupLimitOk returns a tuple with the GroupLimit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGroupLimit

`func (o *ReportFilters) SetGroupLimit(v int32)`

SetGroupLimit sets GroupLimit field to given value.

### HasGroupLimit

`func (o *ReportFilters) HasGroupLimit() bool`

HasGroupLimit returns a boolean if a field has been set.

### GetOffset

`func (o *ReportFilters) GetOffset() int32`

GetOffset returns the Offset field if non-nil, zero value otherwise.

### GetOffsetOk

`func (o *ReportFilters) GetOffsetOk() (*int32, bool)`

GetOffsetOk returns a tuple with the Offset field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOffset

`func (o *ReportFilters) SetOffset(v int32)`

SetOffset sets Offset field to given value.

### HasOffset

`func (o *ReportFilters) HasOffset() bool`

HasOffset returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


