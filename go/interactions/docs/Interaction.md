# Interaction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**ChannelId** | **string** |  | 
**OrganizationId** | **string** |  | 
**Name** | **string** |  | 
**Priority** | **int32** |  | 
**ActionExecutionsLimit** | **int32** |  | 
**State** | [**State**](State.md) |  | 
**Trigger** | [**Trigger**](Trigger.md) |  | 
**Actions** | [**[]Action**](Action.md) |  | 
**ForceNewTransaction** | **bool** | When &#x60;true&#x60; new transaction will always be created on interaction start | 
**Restart** | **bool** |  | 
**RestartCoolDown** | Pointer to **string** |  | [optional] 
**LiveFrom** | Pointer to **time.Time** |  | [optional] 
**LiveTo** | Pointer to **time.Time** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 
**Languages** | Pointer to **[]string** |  | [optional] 
**Messengers** | Pointer to **[]string** |  | [optional] 
**Metrics** | Pointer to [**[]Metric**](Metric.md) |  | [optional] 
**TimeZone** | Pointer to **string** |  | [optional] 
**DefaultLanguage** | Pointer to **string** |  | [optional] 
**Successor** | Pointer to [**Successor**](Successor.md) |  | [optional] 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 

## Methods

### NewInteraction

`func NewInteraction(id string, channelId string, organizationId string, name string, priority int32, actionExecutionsLimit int32, state State, trigger Trigger, actions []Action, forceNewTransaction bool, restart bool, createdAt time.Time, updatedAt time.Time, ) *Interaction`

NewInteraction instantiates a new Interaction object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInteractionWithDefaults

`func NewInteractionWithDefaults() *Interaction`

NewInteractionWithDefaults instantiates a new Interaction object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Interaction) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Interaction) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Interaction) SetId(v string)`

SetId sets Id field to given value.


### GetChannelId

`func (o *Interaction) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *Interaction) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *Interaction) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetOrganizationId

`func (o *Interaction) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *Interaction) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *Interaction) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetName

`func (o *Interaction) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Interaction) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Interaction) SetName(v string)`

SetName sets Name field to given value.


### GetPriority

`func (o *Interaction) GetPriority() int32`

GetPriority returns the Priority field if non-nil, zero value otherwise.

### GetPriorityOk

`func (o *Interaction) GetPriorityOk() (*int32, bool)`

GetPriorityOk returns a tuple with the Priority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriority

`func (o *Interaction) SetPriority(v int32)`

SetPriority sets Priority field to given value.


### GetActionExecutionsLimit

`func (o *Interaction) GetActionExecutionsLimit() int32`

GetActionExecutionsLimit returns the ActionExecutionsLimit field if non-nil, zero value otherwise.

### GetActionExecutionsLimitOk

`func (o *Interaction) GetActionExecutionsLimitOk() (*int32, bool)`

GetActionExecutionsLimitOk returns a tuple with the ActionExecutionsLimit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActionExecutionsLimit

`func (o *Interaction) SetActionExecutionsLimit(v int32)`

SetActionExecutionsLimit sets ActionExecutionsLimit field to given value.


### GetState

`func (o *Interaction) GetState() State`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *Interaction) GetStateOk() (*State, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *Interaction) SetState(v State)`

SetState sets State field to given value.


### GetTrigger

`func (o *Interaction) GetTrigger() Trigger`

GetTrigger returns the Trigger field if non-nil, zero value otherwise.

### GetTriggerOk

`func (o *Interaction) GetTriggerOk() (*Trigger, bool)`

GetTriggerOk returns a tuple with the Trigger field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTrigger

`func (o *Interaction) SetTrigger(v Trigger)`

SetTrigger sets Trigger field to given value.


### GetActions

`func (o *Interaction) GetActions() []Action`

GetActions returns the Actions field if non-nil, zero value otherwise.

### GetActionsOk

`func (o *Interaction) GetActionsOk() (*[]Action, bool)`

GetActionsOk returns a tuple with the Actions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActions

`func (o *Interaction) SetActions(v []Action)`

SetActions sets Actions field to given value.


### GetForceNewTransaction

`func (o *Interaction) GetForceNewTransaction() bool`

GetForceNewTransaction returns the ForceNewTransaction field if non-nil, zero value otherwise.

### GetForceNewTransactionOk

`func (o *Interaction) GetForceNewTransactionOk() (*bool, bool)`

GetForceNewTransactionOk returns a tuple with the ForceNewTransaction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetForceNewTransaction

`func (o *Interaction) SetForceNewTransaction(v bool)`

SetForceNewTransaction sets ForceNewTransaction field to given value.


### GetRestart

`func (o *Interaction) GetRestart() bool`

GetRestart returns the Restart field if non-nil, zero value otherwise.

### GetRestartOk

`func (o *Interaction) GetRestartOk() (*bool, bool)`

GetRestartOk returns a tuple with the Restart field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRestart

`func (o *Interaction) SetRestart(v bool)`

SetRestart sets Restart field to given value.


### GetRestartCoolDown

`func (o *Interaction) GetRestartCoolDown() string`

GetRestartCoolDown returns the RestartCoolDown field if non-nil, zero value otherwise.

### GetRestartCoolDownOk

`func (o *Interaction) GetRestartCoolDownOk() (*string, bool)`

GetRestartCoolDownOk returns a tuple with the RestartCoolDown field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRestartCoolDown

`func (o *Interaction) SetRestartCoolDown(v string)`

SetRestartCoolDown sets RestartCoolDown field to given value.

### HasRestartCoolDown

`func (o *Interaction) HasRestartCoolDown() bool`

HasRestartCoolDown returns a boolean if a field has been set.

### GetLiveFrom

`func (o *Interaction) GetLiveFrom() time.Time`

GetLiveFrom returns the LiveFrom field if non-nil, zero value otherwise.

### GetLiveFromOk

`func (o *Interaction) GetLiveFromOk() (*time.Time, bool)`

GetLiveFromOk returns a tuple with the LiveFrom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLiveFrom

`func (o *Interaction) SetLiveFrom(v time.Time)`

SetLiveFrom sets LiveFrom field to given value.

### HasLiveFrom

`func (o *Interaction) HasLiveFrom() bool`

HasLiveFrom returns a boolean if a field has been set.

### GetLiveTo

`func (o *Interaction) GetLiveTo() time.Time`

GetLiveTo returns the LiveTo field if non-nil, zero value otherwise.

### GetLiveToOk

`func (o *Interaction) GetLiveToOk() (*time.Time, bool)`

GetLiveToOk returns a tuple with the LiveTo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLiveTo

`func (o *Interaction) SetLiveTo(v time.Time)`

SetLiveTo sets LiveTo field to given value.

### HasLiveTo

`func (o *Interaction) HasLiveTo() bool`

HasLiveTo returns a boolean if a field has been set.

### GetTags

`func (o *Interaction) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *Interaction) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *Interaction) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *Interaction) HasTags() bool`

HasTags returns a boolean if a field has been set.

### GetLanguages

`func (o *Interaction) GetLanguages() []string`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *Interaction) GetLanguagesOk() (*[]string, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *Interaction) SetLanguages(v []string)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *Interaction) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetMessengers

`func (o *Interaction) GetMessengers() []string`

GetMessengers returns the Messengers field if non-nil, zero value otherwise.

### GetMessengersOk

`func (o *Interaction) GetMessengersOk() (*[]string, bool)`

GetMessengersOk returns a tuple with the Messengers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengers

`func (o *Interaction) SetMessengers(v []string)`

SetMessengers sets Messengers field to given value.

### HasMessengers

`func (o *Interaction) HasMessengers() bool`

HasMessengers returns a boolean if a field has been set.

### GetMetrics

`func (o *Interaction) GetMetrics() []Metric`

GetMetrics returns the Metrics field if non-nil, zero value otherwise.

### GetMetricsOk

`func (o *Interaction) GetMetricsOk() (*[]Metric, bool)`

GetMetricsOk returns a tuple with the Metrics field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetrics

`func (o *Interaction) SetMetrics(v []Metric)`

SetMetrics sets Metrics field to given value.

### HasMetrics

`func (o *Interaction) HasMetrics() bool`

HasMetrics returns a boolean if a field has been set.

### GetTimeZone

`func (o *Interaction) GetTimeZone() string`

GetTimeZone returns the TimeZone field if non-nil, zero value otherwise.

### GetTimeZoneOk

`func (o *Interaction) GetTimeZoneOk() (*string, bool)`

GetTimeZoneOk returns a tuple with the TimeZone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimeZone

`func (o *Interaction) SetTimeZone(v string)`

SetTimeZone sets TimeZone field to given value.

### HasTimeZone

`func (o *Interaction) HasTimeZone() bool`

HasTimeZone returns a boolean if a field has been set.

### GetDefaultLanguage

`func (o *Interaction) GetDefaultLanguage() string`

GetDefaultLanguage returns the DefaultLanguage field if non-nil, zero value otherwise.

### GetDefaultLanguageOk

`func (o *Interaction) GetDefaultLanguageOk() (*string, bool)`

GetDefaultLanguageOk returns a tuple with the DefaultLanguage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultLanguage

`func (o *Interaction) SetDefaultLanguage(v string)`

SetDefaultLanguage sets DefaultLanguage field to given value.

### HasDefaultLanguage

`func (o *Interaction) HasDefaultLanguage() bool`

HasDefaultLanguage returns a boolean if a field has been set.

### GetSuccessor

`func (o *Interaction) GetSuccessor() Successor`

GetSuccessor returns the Successor field if non-nil, zero value otherwise.

### GetSuccessorOk

`func (o *Interaction) GetSuccessorOk() (*Successor, bool)`

GetSuccessorOk returns a tuple with the Successor field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSuccessor

`func (o *Interaction) SetSuccessor(v Successor)`

SetSuccessor sets Successor field to given value.

### HasSuccessor

`func (o *Interaction) HasSuccessor() bool`

HasSuccessor returns a boolean if a field has been set.

### GetCreatedAt

`func (o *Interaction) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Interaction) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Interaction) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *Interaction) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Interaction) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Interaction) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


