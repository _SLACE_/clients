# ScPermissionCreateData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**MainRolloutId** | **string** |  | 
**TermsContent** | **string** |  | 
**Active** | **bool** |  | 

## Methods

### NewScPermissionCreateData

`func NewScPermissionCreateData(name string, mainRolloutId string, termsContent string, active bool, ) *ScPermissionCreateData`

NewScPermissionCreateData instantiates a new ScPermissionCreateData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScPermissionCreateDataWithDefaults

`func NewScPermissionCreateDataWithDefaults() *ScPermissionCreateData`

NewScPermissionCreateDataWithDefaults instantiates a new ScPermissionCreateData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ScPermissionCreateData) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ScPermissionCreateData) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ScPermissionCreateData) SetName(v string)`

SetName sets Name field to given value.


### GetMainRolloutId

`func (o *ScPermissionCreateData) GetMainRolloutId() string`

GetMainRolloutId returns the MainRolloutId field if non-nil, zero value otherwise.

### GetMainRolloutIdOk

`func (o *ScPermissionCreateData) GetMainRolloutIdOk() (*string, bool)`

GetMainRolloutIdOk returns a tuple with the MainRolloutId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMainRolloutId

`func (o *ScPermissionCreateData) SetMainRolloutId(v string)`

SetMainRolloutId sets MainRolloutId field to given value.


### GetTermsContent

`func (o *ScPermissionCreateData) GetTermsContent() string`

GetTermsContent returns the TermsContent field if non-nil, zero value otherwise.

### GetTermsContentOk

`func (o *ScPermissionCreateData) GetTermsContentOk() (*string, bool)`

GetTermsContentOk returns a tuple with the TermsContent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTermsContent

`func (o *ScPermissionCreateData) SetTermsContent(v string)`

SetTermsContent sets TermsContent field to given value.


### GetActive

`func (o *ScPermissionCreateData) GetActive() bool`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *ScPermissionCreateData) GetActiveOk() (*bool, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *ScPermissionCreateData) SetActive(v bool)`

SetActive sets Active field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


