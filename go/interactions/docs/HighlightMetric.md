# HighlightMetric

## Enum


* `MAIN_GOAL` (value: `"main_goal"`)

* `INVESTMENTS` (value: `"investments"`)

* `CHATS_STARTED` (value: `"chats_started"`)

* `CONSENTS` (value: `"consents"`)

* `INTERACTIONS` (value: `"interactions"`)

* `UNIQUE_CONTACTS` (value: `"unique_contacts"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


