# DataSource

## Enum


* `STATIC` (value: `"static"`)

* `EVENT` (value: `"event"`)

* `CONTACT` (value: `"contact"`)

* `TX` (value: `"tx"`)

* `CODE` (value: `"code"`)

* `SEGMENTS` (value: `"segments"`)

* `LABELS` (value: `"labels"`)

* `OUTPUT` (value: `"output"`)

* `MOBILE_DEVICE` (value: `"mobile_device"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


