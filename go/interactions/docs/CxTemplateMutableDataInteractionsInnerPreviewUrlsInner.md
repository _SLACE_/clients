# CxTemplateMutableDataInteractionsInnerPreviewUrlsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | Pointer to **string** |  | [optional] 
**Language** | Pointer to **string** |  | [optional] 

## Methods

### NewCxTemplateMutableDataInteractionsInnerPreviewUrlsInner

`func NewCxTemplateMutableDataInteractionsInnerPreviewUrlsInner() *CxTemplateMutableDataInteractionsInnerPreviewUrlsInner`

NewCxTemplateMutableDataInteractionsInnerPreviewUrlsInner instantiates a new CxTemplateMutableDataInteractionsInnerPreviewUrlsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateMutableDataInteractionsInnerPreviewUrlsInnerWithDefaults

`func NewCxTemplateMutableDataInteractionsInnerPreviewUrlsInnerWithDefaults() *CxTemplateMutableDataInteractionsInnerPreviewUrlsInner`

NewCxTemplateMutableDataInteractionsInnerPreviewUrlsInnerWithDefaults instantiates a new CxTemplateMutableDataInteractionsInnerPreviewUrlsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUrl

`func (o *CxTemplateMutableDataInteractionsInnerPreviewUrlsInner) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *CxTemplateMutableDataInteractionsInnerPreviewUrlsInner) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *CxTemplateMutableDataInteractionsInnerPreviewUrlsInner) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *CxTemplateMutableDataInteractionsInnerPreviewUrlsInner) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetLanguage

`func (o *CxTemplateMutableDataInteractionsInnerPreviewUrlsInner) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *CxTemplateMutableDataInteractionsInnerPreviewUrlsInner) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *CxTemplateMutableDataInteractionsInnerPreviewUrlsInner) SetLanguage(v string)`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *CxTemplateMutableDataInteractionsInnerPreviewUrlsInner) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


