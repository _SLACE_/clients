# CxTemplateInstallationInteractionsCustomDataInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InteractionId** | Pointer to **string** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 

## Methods

### NewCxTemplateInstallationInteractionsCustomDataInner

`func NewCxTemplateInstallationInteractionsCustomDataInner() *CxTemplateInstallationInteractionsCustomDataInner`

NewCxTemplateInstallationInteractionsCustomDataInner instantiates a new CxTemplateInstallationInteractionsCustomDataInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateInstallationInteractionsCustomDataInnerWithDefaults

`func NewCxTemplateInstallationInteractionsCustomDataInnerWithDefaults() *CxTemplateInstallationInteractionsCustomDataInner`

NewCxTemplateInstallationInteractionsCustomDataInnerWithDefaults instantiates a new CxTemplateInstallationInteractionsCustomDataInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInteractionId

`func (o *CxTemplateInstallationInteractionsCustomDataInner) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *CxTemplateInstallationInteractionsCustomDataInner) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *CxTemplateInstallationInteractionsCustomDataInner) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *CxTemplateInstallationInteractionsCustomDataInner) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetName

`func (o *CxTemplateInstallationInteractionsCustomDataInner) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CxTemplateInstallationInteractionsCustomDataInner) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CxTemplateInstallationInteractionsCustomDataInner) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *CxTemplateInstallationInteractionsCustomDataInner) HasName() bool`

HasName returns a boolean if a field has been set.

### GetTags

`func (o *CxTemplateInstallationInteractionsCustomDataInner) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *CxTemplateInstallationInteractionsCustomDataInner) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *CxTemplateInstallationInteractionsCustomDataInner) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *CxTemplateInstallationInteractionsCustomDataInner) HasTags() bool`

HasTags returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


