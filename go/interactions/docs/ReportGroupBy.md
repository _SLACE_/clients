# ReportGroupBy

## Enum


* `GroupByInteractionId` (value: `"interaction_id"`)

* `GroupByInteractionName` (value: `"interaction_name"`)

* `GroupByChannelId` (value: `"channel_id"`)

* `GroupByChannelName` (value: `"channel_name"`)

* `GroupByEntryPointId` (value: `"entry_point_id"`)

* `GroupByLocationId` (value: `"location_id"`)

* `GroupByLocationName` (value: `"location_name"`)

* `GroupByMessenger` (value: `"messenger"`)

* `GroupByTriggerType` (value: `"trigger_type"`)

* `GroupByEntryType` (value: `"entry_type"`)

* `GroupByLanguage` (value: `"language"`)

* `GroupByBrowserName` (value: `"browser_name"`)

* `GroupByDeviceName` (value: `"device_name"`)

* `GroupByCountryCode` (value: `"country_code"`)

* `GroupBySegments` (value: `"segments"`)

* `GroupByLabels` (value: `"labels"`)

* `GroupByYear` (value: `"year"`)

* `GroupByMonth` (value: `"month"`)

* `GroupByCalendarWeek` (value: `"calendar_week"`)

* `GroupByDate` (value: `"date"`)

* `GroupByDay` (value: `"day"`)

* `GroupByHour` (value: `"hour"`)

* `GroupByWeekDay` (value: `"week_day"`)

* `GroupByVersion` (value: `"version"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


