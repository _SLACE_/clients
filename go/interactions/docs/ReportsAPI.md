# \ReportsAPI

All URIs are relative to *https://interactions.slace.dev/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateExport**](ReportsAPI.md#CreateExport) | **Post** /organizations/{org_id}/reports/exports | Create export
[**CreateHighlightConfiguration**](ReportsAPI.md#CreateHighlightConfiguration) | **Post** /organizations/{org_id}/reports/configurations/{conf_id}/highlight | Create highlight configuration
[**CreateReportConfiguration**](ReportsAPI.md#CreateReportConfiguration) | **Post** /organizations/{org_id}/reports/configurations | Create report configuration
[**DeleteHighlightConfiguration**](ReportsAPI.md#DeleteHighlightConfiguration) | **Delete** /organizations/{org_id}/reports/configurations/{conf_id}/highlight | Delete highlight configuration
[**DeleteReportConfiguration**](ReportsAPI.md#DeleteReportConfiguration) | **Delete** /organizations/{org_id}/reports/configurations/{conf_id} | Delete report configuration
[**DeleteSubscriber**](ReportsAPI.md#DeleteSubscriber) | **Delete** /organizations/{org_id}/reports/configurations/{conf_id}/highlight/subscribers/{subscriber_id} | Delete subscriber
[**GetCohortReport**](ReportsAPI.md#GetCohortReport) | **Get** /organizations/{org_id}/reports/cohort | Get cohort report
[**GetHighlightConfiguration**](ReportsAPI.md#GetHighlightConfiguration) | **Get** /organizations/{org_id}/reports/configurations/{conf_id}/highlight | Get highlight configuration
[**GetLastUpdateTime**](ReportsAPI.md#GetLastUpdateTime) | **Get** /organizations/{org_id}/reports/last-update | Get Last Update Time
[**GetRawDataReport**](ReportsAPI.md#GetRawDataReport) | **Get** /organizations/{org_id}/reports/raw | Get raw data report
[**GetSlaceConnects**](ReportsAPI.md#GetSlaceConnects) | **Get** /organizations/{org_id}/reports/slace-connects | Get slace connects
[**ListExports**](ReportsAPI.md#ListExports) | **Get** /organizations/{org_id}/reports/exports | List exports
[**ListHighlightSubscribers**](ReportsAPI.md#ListHighlightSubscribers) | **Get** /organizations/{org_id}/reports/configurations/{conf_id}/highlight/subscribers | List highlight subscribers
[**ListInteractionsWithMetrics**](ReportsAPI.md#ListInteractionsWithMetrics) | **Get** /channels/{channel_id}/interactions-with-metrics | List interactions with metrics
[**ListReportsOrganizations**](ReportsAPI.md#ListReportsOrganizations) | **Get** /organizations/{org_id}/reports/configurations | List reports configurations
[**UpdateHighlightConfiguration**](ReportsAPI.md#UpdateHighlightConfiguration) | **Put** /organizations/{org_id}/reports/configurations/{conf_id}/highlight | Update highlight configuration
[**UpdateReportConfiguration**](ReportsAPI.md#UpdateReportConfiguration) | **Put** /organizations/{org_id}/reports/configurations/{conf_id} | Update report configuration
[**UpdateSubscriber**](ReportsAPI.md#UpdateSubscriber) | **Put** /organizations/{org_id}/reports/configurations/{conf_id}/highlight/subscribers/{subscriber_id} | Update subscriber



## CreateExport

> ExportJob CreateExport(ctx, orgId).ExportRequest(exportRequest).Execute()

Create export

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    exportRequest := map[string][]openapiclient.ExportRequest{ ... } // ExportRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.CreateExport(context.Background(), orgId).ExportRequest(exportRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.CreateExport``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateExport`: ExportJob
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.CreateExport`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateExportRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **exportRequest** | [**ExportRequest**](ExportRequest.md) |  | 

### Return type

[**ExportJob**](ExportJob.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateHighlightConfiguration

> Highlight CreateHighlightConfiguration(ctx, orgId, confId).HighlightMutable(highlightMutable).Execute()

Create highlight configuration

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    confId := "confId_example" // string | 
    highlightMutable := *openapiclient.NewHighlightMutable("Template_example", openapiclient.HighlightMetric("main_goal"), openapiclient.Frequency("hourly"), false, []int32{int32(123)}, time.Now(), time.Now()) // HighlightMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.CreateHighlightConfiguration(context.Background(), orgId, confId).HighlightMutable(highlightMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.CreateHighlightConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateHighlightConfiguration`: Highlight
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.CreateHighlightConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**confId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateHighlightConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **highlightMutable** | [**HighlightMutable**](HighlightMutable.md) |  | 

### Return type

[**Highlight**](Highlight.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateReportConfiguration

> ReportConfiguration CreateReportConfiguration(ctx, orgId).ReportConfigurationMutable(reportConfigurationMutable).Execute()

Create report configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    reportConfigurationMutable := *openapiclient.NewReportConfigurationMutable("Name_example") // ReportConfigurationMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.CreateReportConfiguration(context.Background(), orgId).ReportConfigurationMutable(reportConfigurationMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.CreateReportConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateReportConfiguration`: ReportConfiguration
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.CreateReportConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateReportConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **reportConfigurationMutable** | [**ReportConfigurationMutable**](ReportConfigurationMutable.md) |  | 

### Return type

[**ReportConfiguration**](ReportConfiguration.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteHighlightConfiguration

> GenericResponse DeleteHighlightConfiguration(ctx, orgId, confId).Execute()

Delete highlight configuration

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    confId := "confId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.DeleteHighlightConfiguration(context.Background(), orgId, confId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.DeleteHighlightConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteHighlightConfiguration`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.DeleteHighlightConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**confId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteHighlightConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteReportConfiguration

> GenericResponse DeleteReportConfiguration(ctx, orgId, confId).Execute()

Delete report configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    confId := "confId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.DeleteReportConfiguration(context.Background(), orgId, confId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.DeleteReportConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteReportConfiguration`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.DeleteReportConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**confId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteReportConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteSubscriber

> GenericResponse DeleteSubscriber(ctx, orgId, confId, subscriberId).Execute()

Delete subscriber

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    confId := "confId_example" // string | 
    subscriberId := "subscriberId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.DeleteSubscriber(context.Background(), orgId, confId, subscriberId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.DeleteSubscriber``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteSubscriber`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.DeleteSubscriber`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**confId** | **string** |  | 
**subscriberId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteSubscriberRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCohortReport

> ReportCohort GetCohortReport(ctx, orgId).RolloutId(rolloutId).ResultType(resultType).ChannelIds(channelIds).IxIds(ixIds).LocationIds(locationIds).EpIds(epIds).Messengers(messengers).TriggerTypes(triggerTypes).EntryTypes(entryTypes).Tags(tags).Languages(languages).Segments(segments).Labels(labels).DeviceBrands(deviceBrands).BrowserNames(browserNames).CountryCodes(countryCodes).GroupBy(groupBy).From(from).To(to).Format(format).Sort(sort).Limit(limit).Offset(offset).ContextValues(contextValues).ContextFields(contextFields).GroupByContextField(groupByContextField).Execute()

Get cohort report

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    rolloutId := "rolloutId_example" // string | Rollout id (optional)
    resultType := "resultType_example" // string | Report results type (optional) (default to "absolute")
    channelIds := []string{"Inner_example"} // []string | Channel ids filter (optional)
    ixIds := []string{"Inner_example"} // []string | Interaction ids filter (optional)
    locationIds := []string{"Inner_example"} // []string | Location ids filter (optional)
    epIds := []string{"Inner_example"} // []string | Entry Points ids filter (optional)
    messengers := []string{"Inner_example"} // []string | Messengers filter (optional)
    triggerTypes := []string{"Inner_example"} // []string | Trigger types filter (optional)
    entryTypes := []string{"Inner_example"} // []string | Entry types filter (optional)
    tags := []string{"Inner_example"} // []string | Tags filter (optional)
    languages := []string{"Inner_example"} // []string | Languages filter (optional)
    segments := []string{"Inner_example"} // []string | Segments filter (optional)
    labels := []string{"Inner_example"} // []string | Labels filter (optional)
    deviceBrands := []string{"Inner_example"} // []string | Device brands filter (optional)
    browserNames := []string{"Inner_example"} // []string | Browser names filter (optional)
    countryCodes := []string{"Inner_example"} // []string | Country codes filter (optional)
    groupBy := []string{"Inner_example"} // []string | Group by (optional)
    from := time.Now() // time.Time | Time from (optional)
    to := time.Now() // time.Time | Time to (optional)
    format := "format_example" // string | Set to get data as xlsx or csv, instead of json (optional)
    sort := []string{"Inner_example"} // []string | List of fields to sort by, `-` prefix means descending order (optional)
    limit := int32(56) // int32 |  (optional)
    offset := int32(56) // int32 |  (optional)
    contextValues := []string{"Inner_example"} // []string | Context values filter (optional)
    contextFields := []string{"Inner_example"} // []string | Context fields filter (optional)
    groupByContextField := "groupByContextField_example" // string | Context field name for group by (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.GetCohortReport(context.Background(), orgId).RolloutId(rolloutId).ResultType(resultType).ChannelIds(channelIds).IxIds(ixIds).LocationIds(locationIds).EpIds(epIds).Messengers(messengers).TriggerTypes(triggerTypes).EntryTypes(entryTypes).Tags(tags).Languages(languages).Segments(segments).Labels(labels).DeviceBrands(deviceBrands).BrowserNames(browserNames).CountryCodes(countryCodes).GroupBy(groupBy).From(from).To(to).Format(format).Sort(sort).Limit(limit).Offset(offset).ContextValues(contextValues).ContextFields(contextFields).GroupByContextField(groupByContextField).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.GetCohortReport``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCohortReport`: ReportCohort
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.GetCohortReport`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetCohortReportRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **rolloutId** | **string** | Rollout id | 
 **resultType** | **string** | Report results type | [default to &quot;absolute&quot;]
 **channelIds** | **[]string** | Channel ids filter | 
 **ixIds** | **[]string** | Interaction ids filter | 
 **locationIds** | **[]string** | Location ids filter | 
 **epIds** | **[]string** | Entry Points ids filter | 
 **messengers** | **[]string** | Messengers filter | 
 **triggerTypes** | **[]string** | Trigger types filter | 
 **entryTypes** | **[]string** | Entry types filter | 
 **tags** | **[]string** | Tags filter | 
 **languages** | **[]string** | Languages filter | 
 **segments** | **[]string** | Segments filter | 
 **labels** | **[]string** | Labels filter | 
 **deviceBrands** | **[]string** | Device brands filter | 
 **browserNames** | **[]string** | Browser names filter | 
 **countryCodes** | **[]string** | Country codes filter | 
 **groupBy** | **[]string** | Group by | 
 **from** | **time.Time** | Time from | 
 **to** | **time.Time** | Time to | 
 **format** | **string** | Set to get data as xlsx or csv, instead of json | 
 **sort** | **[]string** | List of fields to sort by, &#x60;-&#x60; prefix means descending order | 
 **limit** | **int32** |  | 
 **offset** | **int32** |  | 
 **contextValues** | **[]string** | Context values filter | 
 **contextFields** | **[]string** | Context fields filter | 
 **groupByContextField** | **string** | Context field name for group by | 

### Return type

[**ReportCohort**](ReportCohort.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetHighlightConfiguration

> Highlight GetHighlightConfiguration(ctx, orgId, confId).Execute()

Get highlight configuration

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    confId := "confId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.GetHighlightConfiguration(context.Background(), orgId, confId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.GetHighlightConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetHighlightConfiguration`: Highlight
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.GetHighlightConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**confId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetHighlightConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Highlight**](Highlight.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetLastUpdateTime

> LastUpdate GetLastUpdateTime(ctx, orgId).Execute()

Get Last Update Time



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.GetLastUpdateTime(context.Background(), orgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.GetLastUpdateTime``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetLastUpdateTime`: LastUpdate
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.GetLastUpdateTime`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetLastUpdateTimeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**LastUpdate**](LastUpdate.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetRawDataReport

> ReportRawData GetRawDataReport(ctx, orgId).ChannelIds(channelIds).IxIds(ixIds).LocationIds(locationIds).EpIds(epIds).Messengers(messengers).TriggerTypes(triggerTypes).EntryTypes(entryTypes).Tags(tags).Languages(languages).Segments(segments).Labels(labels).DeviceBrands(deviceBrands).BrowserNames(browserNames).CountryCodes(countryCodes).From(from).To(to).Format(format).ContextValues(contextValues).ContextFields(contextFields).Execute()

Get raw data report

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    channelIds := []string{"Inner_example"} // []string | Channel ids filter (optional)
    ixIds := []string{"Inner_example"} // []string | Interaction ids filter (optional)
    locationIds := []string{"Inner_example"} // []string | Location ids filter (optional)
    epIds := []string{"Inner_example"} // []string | Entry Points ids filter (optional)
    messengers := []string{"Inner_example"} // []string | Messengers filter (optional)
    triggerTypes := []string{"Inner_example"} // []string | Trigger types filter (optional)
    entryTypes := []string{"Inner_example"} // []string | Entry types filter (optional)
    tags := []string{"Inner_example"} // []string | Tags filter (optional)
    languages := []string{"Inner_example"} // []string | Languages filter (optional)
    segments := []string{"Inner_example"} // []string | Segments filter (optional)
    labels := []string{"Inner_example"} // []string | Labels filter (optional)
    deviceBrands := []string{"Inner_example"} // []string | Device brands filter (optional)
    browserNames := []string{"Inner_example"} // []string | Browser names filter (optional)
    countryCodes := []string{"Inner_example"} // []string | Country codes filter (optional)
    from := time.Now() // time.Time | Time from (optional)
    to := time.Now() // time.Time | Time to (optional)
    format := "format_example" // string | Set to get data as xlsx or csv, instead of json (optional)
    contextValues := []string{"Inner_example"} // []string | Context values filter (optional)
    contextFields := []string{"Inner_example"} // []string | Context fields filter (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.GetRawDataReport(context.Background(), orgId).ChannelIds(channelIds).IxIds(ixIds).LocationIds(locationIds).EpIds(epIds).Messengers(messengers).TriggerTypes(triggerTypes).EntryTypes(entryTypes).Tags(tags).Languages(languages).Segments(segments).Labels(labels).DeviceBrands(deviceBrands).BrowserNames(browserNames).CountryCodes(countryCodes).From(from).To(to).Format(format).ContextValues(contextValues).ContextFields(contextFields).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.GetRawDataReport``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetRawDataReport`: ReportRawData
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.GetRawDataReport`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetRawDataReportRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **channelIds** | **[]string** | Channel ids filter | 
 **ixIds** | **[]string** | Interaction ids filter | 
 **locationIds** | **[]string** | Location ids filter | 
 **epIds** | **[]string** | Entry Points ids filter | 
 **messengers** | **[]string** | Messengers filter | 
 **triggerTypes** | **[]string** | Trigger types filter | 
 **entryTypes** | **[]string** | Entry types filter | 
 **tags** | **[]string** | Tags filter | 
 **languages** | **[]string** | Languages filter | 
 **segments** | **[]string** | Segments filter | 
 **labels** | **[]string** | Labels filter | 
 **deviceBrands** | **[]string** | Device brands filter | 
 **browserNames** | **[]string** | Browser names filter | 
 **countryCodes** | **[]string** | Country codes filter | 
 **from** | **time.Time** | Time from | 
 **to** | **time.Time** | Time to | 
 **format** | **string** | Set to get data as xlsx or csv, instead of json | 
 **contextValues** | **[]string** | Context values filter | 
 **contextFields** | **[]string** | Context fields filter | 

### Return type

[**ReportRawData**](ReportRawData.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetSlaceConnects

> []SlaceConnect GetSlaceConnects(ctx, orgId).Execute()

Get slace connects



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.GetSlaceConnects(context.Background(), orgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.GetSlaceConnects``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetSlaceConnects`: []SlaceConnect
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.GetSlaceConnects`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetSlaceConnectsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]SlaceConnect**](SlaceConnect.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListExports

> ExportJobsList ListExports(ctx, orgId).Execute()

List exports

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.ListExports(context.Background(), orgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.ListExports``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListExports`: ExportJobsList
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.ListExports`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListExportsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**ExportJobsList**](ExportJobsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListHighlightSubscribers

> []HighlightSubscriber ListHighlightSubscribers(ctx, orgId, confId).Execute()

List highlight subscribers

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    confId := "confId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.ListHighlightSubscribers(context.Background(), orgId, confId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.ListHighlightSubscribers``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListHighlightSubscribers`: []HighlightSubscriber
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.ListHighlightSubscribers`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**confId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListHighlightSubscribersRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**[]HighlightSubscriber**](HighlightSubscriber.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListInteractionsWithMetrics

> InteractionsWithMetricsList ListInteractionsWithMetrics(ctx, channelId).Name(name).Restart(restart).Ids(ids).States(states).Tags(tags).Languages(languages).Messengers(messengers).Offset(offset).Limit(limit).Sort(sort).TriggerTemplate(triggerTemplate).LiveFrom(liveFrom).LiveTo(liveTo).Execute()

List interactions with metrics



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    channelId := "channelId_example" // string | 
    name := "name_example" // string |  (optional)
    restart := true // bool |  (optional)
    ids := []string{"Inner_example"} // []string |  (optional)
    states := []string{"Inner_example"} // []string |  (optional)
    tags := []string{"Inner_example"} // []string |  (optional)
    languages := []string{"Inner_example"} // []string |  (optional)
    messengers := []string{"Inner_example"} // []string |  (optional)
    offset := "offset_example" // string |  (optional)
    limit := "limit_example" // string |  (optional)
    sort := "sort_example" // string |  (optional)
    triggerTemplate := "triggerTemplate_example" // string |  (optional)
    liveFrom := time.Now() // time.Time | Find all with `live from` greater than/equal (optional)
    liveTo := time.Now() // time.Time | Find all with `live to` less than/equal (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.ListInteractionsWithMetrics(context.Background(), channelId).Name(name).Restart(restart).Ids(ids).States(states).Tags(tags).Languages(languages).Messengers(messengers).Offset(offset).Limit(limit).Sort(sort).TriggerTemplate(triggerTemplate).LiveFrom(liveFrom).LiveTo(liveTo).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.ListInteractionsWithMetrics``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListInteractionsWithMetrics`: InteractionsWithMetricsList
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.ListInteractionsWithMetrics`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListInteractionsWithMetricsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **name** | **string** |  | 
 **restart** | **bool** |  | 
 **ids** | **[]string** |  | 
 **states** | **[]string** |  | 
 **tags** | **[]string** |  | 
 **languages** | **[]string** |  | 
 **messengers** | **[]string** |  | 
 **offset** | **string** |  | 
 **limit** | **string** |  | 
 **sort** | **string** |  | 
 **triggerTemplate** | **string** |  | 
 **liveFrom** | **time.Time** | Find all with &#x60;live from&#x60; greater than/equal | 
 **liveTo** | **time.Time** | Find all with &#x60;live to&#x60; less than/equal | 

### Return type

[**InteractionsWithMetricsList**](InteractionsWithMetricsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListReportsOrganizations

> []ReportConfiguration ListReportsOrganizations(ctx, orgId).Execute()

List reports configurations



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.ListReportsOrganizations(context.Background(), orgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.ListReportsOrganizations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListReportsOrganizations`: []ReportConfiguration
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.ListReportsOrganizations`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListReportsOrganizationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]ReportConfiguration**](ReportConfiguration.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateHighlightConfiguration

> Highlight UpdateHighlightConfiguration(ctx, orgId, confId).HighlightMutable(highlightMutable).Execute()

Update highlight configuration

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    confId := "confId_example" // string | 
    highlightMutable := *openapiclient.NewHighlightMutable("Template_example", openapiclient.HighlightMetric("main_goal"), openapiclient.Frequency("hourly"), false, []int32{int32(123)}, time.Now(), time.Now()) // HighlightMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.UpdateHighlightConfiguration(context.Background(), orgId, confId).HighlightMutable(highlightMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.UpdateHighlightConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateHighlightConfiguration`: Highlight
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.UpdateHighlightConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**confId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateHighlightConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **highlightMutable** | [**HighlightMutable**](HighlightMutable.md) |  | 

### Return type

[**Highlight**](Highlight.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateReportConfiguration

> ReportConfiguration UpdateReportConfiguration(ctx, orgId, confId).ReportConfigurationMutable(reportConfigurationMutable).Execute()

Update report configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    confId := "confId_example" // string | 
    reportConfigurationMutable := *openapiclient.NewReportConfigurationMutable("Name_example") // ReportConfigurationMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.UpdateReportConfiguration(context.Background(), orgId, confId).ReportConfigurationMutable(reportConfigurationMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.UpdateReportConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateReportConfiguration`: ReportConfiguration
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.UpdateReportConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**confId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateReportConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **reportConfigurationMutable** | [**ReportConfigurationMutable**](ReportConfigurationMutable.md) |  | 

### Return type

[**ReportConfiguration**](ReportConfiguration.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateSubscriber

> HighlightSubscriber UpdateSubscriber(ctx, orgId, confId, subscriberId).HighlightSubscriberMutable(highlightSubscriberMutable).Execute()

Update subscriber

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    orgId := "orgId_example" // string | 
    confId := "confId_example" // string | 
    subscriberId := "subscriberId_example" // string | 
    highlightSubscriberMutable := *openapiclient.NewHighlightSubscriberMutable("State_example") // HighlightSubscriberMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ReportsAPI.UpdateSubscriber(context.Background(), orgId, confId, subscriberId).HighlightSubscriberMutable(highlightSubscriberMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ReportsAPI.UpdateSubscriber``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateSubscriber`: HighlightSubscriber
    fmt.Fprintf(os.Stdout, "Response from `ReportsAPI.UpdateSubscriber`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 
**confId** | **string** |  | 
**subscriberId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateSubscriberRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **highlightSubscriberMutable** | [**HighlightSubscriberMutable**](HighlightSubscriberMutable.md) |  | 

### Return type

[**HighlightSubscriber**](HighlightSubscriber.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

