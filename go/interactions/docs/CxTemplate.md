# CxTemplate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**HeaderImageUrls** | Pointer to [**[]CxTemplateHeaderImageUrlsInner**](CxTemplateHeaderImageUrlsInner.md) |  | [optional] 
**Description** | Pointer to **string** |  | [optional] 
**Languages** | Pointer to **[]string** |  | [optional] 
**Integrations** | Pointer to [**CxTemplateIntegrations**](CxTemplateIntegrations.md) |  | [optional] 
**RequiredMessengers** | Pointer to **[]string** |  | [optional] 
**Tags** | Pointer to **[]string** |  | [optional] 
**Interactions** | Pointer to [**[]CxTemplateInteractionsInner**](CxTemplateInteractionsInner.md) |  | [optional] 
**Global** | Pointer to **bool** |  | [optional] 
**Formality** | Pointer to **string** |  | [optional] 
**PreviewUrls** | Pointer to [**[]CxTemplatePreviewUrlsInner**](CxTemplatePreviewUrlsInner.md) |  | [optional] 
**OrganizationId** | Pointer to **string** |  | [optional] 
**Id** | Pointer to **string** |  | [optional] 
**CreatedAt** | Pointer to **time.Time** |  | [optional] 
**UpdatedAt** | Pointer to **time.Time** |  | [optional] 
**Status** | Pointer to **string** |  | [optional] 

## Methods

### NewCxTemplate

`func NewCxTemplate() *CxTemplate`

NewCxTemplate instantiates a new CxTemplate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCxTemplateWithDefaults

`func NewCxTemplateWithDefaults() *CxTemplate`

NewCxTemplateWithDefaults instantiates a new CxTemplate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *CxTemplate) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CxTemplate) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CxTemplate) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *CxTemplate) HasName() bool`

HasName returns a boolean if a field has been set.

### GetHeaderImageUrls

`func (o *CxTemplate) GetHeaderImageUrls() []CxTemplateHeaderImageUrlsInner`

GetHeaderImageUrls returns the HeaderImageUrls field if non-nil, zero value otherwise.

### GetHeaderImageUrlsOk

`func (o *CxTemplate) GetHeaderImageUrlsOk() (*[]CxTemplateHeaderImageUrlsInner, bool)`

GetHeaderImageUrlsOk returns a tuple with the HeaderImageUrls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeaderImageUrls

`func (o *CxTemplate) SetHeaderImageUrls(v []CxTemplateHeaderImageUrlsInner)`

SetHeaderImageUrls sets HeaderImageUrls field to given value.

### HasHeaderImageUrls

`func (o *CxTemplate) HasHeaderImageUrls() bool`

HasHeaderImageUrls returns a boolean if a field has been set.

### GetDescription

`func (o *CxTemplate) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *CxTemplate) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *CxTemplate) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *CxTemplate) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetLanguages

`func (o *CxTemplate) GetLanguages() []string`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *CxTemplate) GetLanguagesOk() (*[]string, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *CxTemplate) SetLanguages(v []string)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *CxTemplate) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetIntegrations

`func (o *CxTemplate) GetIntegrations() CxTemplateIntegrations`

GetIntegrations returns the Integrations field if non-nil, zero value otherwise.

### GetIntegrationsOk

`func (o *CxTemplate) GetIntegrationsOk() (*CxTemplateIntegrations, bool)`

GetIntegrationsOk returns a tuple with the Integrations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIntegrations

`func (o *CxTemplate) SetIntegrations(v CxTemplateIntegrations)`

SetIntegrations sets Integrations field to given value.

### HasIntegrations

`func (o *CxTemplate) HasIntegrations() bool`

HasIntegrations returns a boolean if a field has been set.

### GetRequiredMessengers

`func (o *CxTemplate) GetRequiredMessengers() []string`

GetRequiredMessengers returns the RequiredMessengers field if non-nil, zero value otherwise.

### GetRequiredMessengersOk

`func (o *CxTemplate) GetRequiredMessengersOk() (*[]string, bool)`

GetRequiredMessengersOk returns a tuple with the RequiredMessengers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequiredMessengers

`func (o *CxTemplate) SetRequiredMessengers(v []string)`

SetRequiredMessengers sets RequiredMessengers field to given value.

### HasRequiredMessengers

`func (o *CxTemplate) HasRequiredMessengers() bool`

HasRequiredMessengers returns a boolean if a field has been set.

### GetTags

`func (o *CxTemplate) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *CxTemplate) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *CxTemplate) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *CxTemplate) HasTags() bool`

HasTags returns a boolean if a field has been set.

### GetInteractions

`func (o *CxTemplate) GetInteractions() []CxTemplateInteractionsInner`

GetInteractions returns the Interactions field if non-nil, zero value otherwise.

### GetInteractionsOk

`func (o *CxTemplate) GetInteractionsOk() (*[]CxTemplateInteractionsInner, bool)`

GetInteractionsOk returns a tuple with the Interactions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractions

`func (o *CxTemplate) SetInteractions(v []CxTemplateInteractionsInner)`

SetInteractions sets Interactions field to given value.

### HasInteractions

`func (o *CxTemplate) HasInteractions() bool`

HasInteractions returns a boolean if a field has been set.

### GetGlobal

`func (o *CxTemplate) GetGlobal() bool`

GetGlobal returns the Global field if non-nil, zero value otherwise.

### GetGlobalOk

`func (o *CxTemplate) GetGlobalOk() (*bool, bool)`

GetGlobalOk returns a tuple with the Global field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGlobal

`func (o *CxTemplate) SetGlobal(v bool)`

SetGlobal sets Global field to given value.

### HasGlobal

`func (o *CxTemplate) HasGlobal() bool`

HasGlobal returns a boolean if a field has been set.

### GetFormality

`func (o *CxTemplate) GetFormality() string`

GetFormality returns the Formality field if non-nil, zero value otherwise.

### GetFormalityOk

`func (o *CxTemplate) GetFormalityOk() (*string, bool)`

GetFormalityOk returns a tuple with the Formality field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormality

`func (o *CxTemplate) SetFormality(v string)`

SetFormality sets Formality field to given value.

### HasFormality

`func (o *CxTemplate) HasFormality() bool`

HasFormality returns a boolean if a field has been set.

### GetPreviewUrls

`func (o *CxTemplate) GetPreviewUrls() []CxTemplatePreviewUrlsInner`

GetPreviewUrls returns the PreviewUrls field if non-nil, zero value otherwise.

### GetPreviewUrlsOk

`func (o *CxTemplate) GetPreviewUrlsOk() (*[]CxTemplatePreviewUrlsInner, bool)`

GetPreviewUrlsOk returns a tuple with the PreviewUrls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPreviewUrls

`func (o *CxTemplate) SetPreviewUrls(v []CxTemplatePreviewUrlsInner)`

SetPreviewUrls sets PreviewUrls field to given value.

### HasPreviewUrls

`func (o *CxTemplate) HasPreviewUrls() bool`

HasPreviewUrls returns a boolean if a field has been set.

### GetOrganizationId

`func (o *CxTemplate) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *CxTemplate) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *CxTemplate) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *CxTemplate) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetId

`func (o *CxTemplate) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CxTemplate) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CxTemplate) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *CxTemplate) HasId() bool`

HasId returns a boolean if a field has been set.

### GetCreatedAt

`func (o *CxTemplate) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *CxTemplate) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *CxTemplate) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.

### HasCreatedAt

`func (o *CxTemplate) HasCreatedAt() bool`

HasCreatedAt returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *CxTemplate) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *CxTemplate) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *CxTemplate) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *CxTemplate) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.

### GetStatus

`func (o *CxTemplate) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *CxTemplate) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *CxTemplate) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *CxTemplate) HasStatus() bool`

HasStatus returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


