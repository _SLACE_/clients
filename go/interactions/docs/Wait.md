# Wait

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Duration** | Pointer to **string** |  | [optional] 
**Until** | Pointer to [**WaitUntil**](WaitUntil.md) |  | [optional] 
**FailOnReach** | **bool** | Whether reaching deadline means action failed or succeeded | 

## Methods

### NewWait

`func NewWait(failOnReach bool, ) *Wait`

NewWait instantiates a new Wait object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWaitWithDefaults

`func NewWaitWithDefaults() *Wait`

NewWaitWithDefaults instantiates a new Wait object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDuration

`func (o *Wait) GetDuration() string`

GetDuration returns the Duration field if non-nil, zero value otherwise.

### GetDurationOk

`func (o *Wait) GetDurationOk() (*string, bool)`

GetDurationOk returns a tuple with the Duration field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDuration

`func (o *Wait) SetDuration(v string)`

SetDuration sets Duration field to given value.

### HasDuration

`func (o *Wait) HasDuration() bool`

HasDuration returns a boolean if a field has been set.

### GetUntil

`func (o *Wait) GetUntil() WaitUntil`

GetUntil returns the Until field if non-nil, zero value otherwise.

### GetUntilOk

`func (o *Wait) GetUntilOk() (*WaitUntil, bool)`

GetUntilOk returns a tuple with the Until field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUntil

`func (o *Wait) SetUntil(v WaitUntil)`

SetUntil sets Until field to given value.

### HasUntil

`func (o *Wait) HasUntil() bool`

HasUntil returns a boolean if a field has been set.

### GetFailOnReach

`func (o *Wait) GetFailOnReach() bool`

GetFailOnReach returns the FailOnReach field if non-nil, zero value otherwise.

### GetFailOnReachOk

`func (o *Wait) GetFailOnReachOk() (*bool, bool)`

GetFailOnReachOk returns a tuple with the FailOnReach field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFailOnReach

`func (o *Wait) SetFailOnReach(v bool)`

SetFailOnReach sets FailOnReach field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


