# Condition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Operator** | [**Operator**](Operator.md) |  | 
**DataType** | Pointer to [**DataType**](DataType.md) |  | [optional] 
**Left** | Pointer to [**ExpressionValue**](ExpressionValue.md) |  | [optional] 
**Right** | Pointer to [**ExpressionValue**](ExpressionValue.md) |  | [optional] 
**Conditions** | Pointer to [**[]Condition**](Condition.md) |  | [optional] 

## Methods

### NewCondition

`func NewCondition(operator Operator, ) *Condition`

NewCondition instantiates a new Condition object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConditionWithDefaults

`func NewConditionWithDefaults() *Condition`

NewConditionWithDefaults instantiates a new Condition object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOperator

`func (o *Condition) GetOperator() Operator`

GetOperator returns the Operator field if non-nil, zero value otherwise.

### GetOperatorOk

`func (o *Condition) GetOperatorOk() (*Operator, bool)`

GetOperatorOk returns a tuple with the Operator field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOperator

`func (o *Condition) SetOperator(v Operator)`

SetOperator sets Operator field to given value.


### GetDataType

`func (o *Condition) GetDataType() DataType`

GetDataType returns the DataType field if non-nil, zero value otherwise.

### GetDataTypeOk

`func (o *Condition) GetDataTypeOk() (*DataType, bool)`

GetDataTypeOk returns a tuple with the DataType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataType

`func (o *Condition) SetDataType(v DataType)`

SetDataType sets DataType field to given value.

### HasDataType

`func (o *Condition) HasDataType() bool`

HasDataType returns a boolean if a field has been set.

### GetLeft

`func (o *Condition) GetLeft() ExpressionValue`

GetLeft returns the Left field if non-nil, zero value otherwise.

### GetLeftOk

`func (o *Condition) GetLeftOk() (*ExpressionValue, bool)`

GetLeftOk returns a tuple with the Left field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLeft

`func (o *Condition) SetLeft(v ExpressionValue)`

SetLeft sets Left field to given value.

### HasLeft

`func (o *Condition) HasLeft() bool`

HasLeft returns a boolean if a field has been set.

### GetRight

`func (o *Condition) GetRight() ExpressionValue`

GetRight returns the Right field if non-nil, zero value otherwise.

### GetRightOk

`func (o *Condition) GetRightOk() (*ExpressionValue, bool)`

GetRightOk returns a tuple with the Right field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRight

`func (o *Condition) SetRight(v ExpressionValue)`

SetRight sets Right field to given value.

### HasRight

`func (o *Condition) HasRight() bool`

HasRight returns a boolean if a field has been set.

### GetConditions

`func (o *Condition) GetConditions() []Condition`

GetConditions returns the Conditions field if non-nil, zero value otherwise.

### GetConditionsOk

`func (o *Condition) GetConditionsOk() (*[]Condition, bool)`

GetConditionsOk returns a tuple with the Conditions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConditions

`func (o *Condition) SetConditions(v []Condition)`

SetConditions sets Conditions field to given value.

### HasConditions

`func (o *Condition) HasConditions() bool`

HasConditions returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


