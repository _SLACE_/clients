# \TasksAPI

All URIs are relative to *https://interactions.slace.dev/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateTask**](TasksAPI.md#CreateTask) | **Post** /organizations/{organization_id}/tasks | Create task
[**DeleteChannelsChannelIdInteractionsIdTasksTaskId**](TasksAPI.md#DeleteChannelsChannelIdInteractionsIdTasksTaskId) | **Delete** /organizations/{organization_id}/tasks/{task_id} | Delete task
[**GetChannelsChannelIdInteractionsIdTasksTaskId**](TasksAPI.md#GetChannelsChannelIdInteractionsIdTasksTaskId) | **Get** /organizations/{organization_id}/tasks/{task_id} | Get task
[**GetTaskCompletionList**](TasksAPI.md#GetTaskCompletionList) | **Get** /organizations/{organization_id}/tasks-completions | Task completion list
[**GetTasks**](TasksAPI.md#GetTasks) | **Get** /organizations/{organization_id}/tasks | Get tasks
[**MarkTaskAsClosed**](TasksAPI.md#MarkTaskAsClosed) | **Post** /organizations/{organization_id}/tasks-completions/{completion_id}/closed | Mark task as closed
[**PutChannelsChannelIdInteractionsIdTasksTaskId**](TasksAPI.md#PutChannelsChannelIdInteractionsIdTasksTaskId) | **Put** /organizations/{organization_id}/tasks/{task_id} | Update task



## CreateTask

> Task CreateTask(ctx, organizationId).CreateTaskData(createTaskData).Execute()

Create task



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    createTaskData := *openapiclient.NewCreateTaskData() // CreateTaskData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TasksAPI.CreateTask(context.Background(), organizationId).CreateTaskData(createTaskData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TasksAPI.CreateTask``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateTask`: Task
    fmt.Fprintf(os.Stdout, "Response from `TasksAPI.CreateTask`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateTaskRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **createTaskData** | [**CreateTaskData**](CreateTaskData.md) |  | 

### Return type

[**Task**](Task.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteChannelsChannelIdInteractionsIdTasksTaskId

> DeleteChannelsChannelIdInteractionsIdTasksTaskId(ctx, taskId, organizationId).Execute()

Delete task



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    taskId := "taskId_example" // string | 
    organizationId := "organizationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.TasksAPI.DeleteChannelsChannelIdInteractionsIdTasksTaskId(context.Background(), taskId, organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TasksAPI.DeleteChannelsChannelIdInteractionsIdTasksTaskId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**taskId** | **string** |  | 
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteChannelsChannelIdInteractionsIdTasksTaskIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChannelsChannelIdInteractionsIdTasksTaskId

> Task GetChannelsChannelIdInteractionsIdTasksTaskId(ctx, taskId, organizationId).Execute()

Get task



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    taskId := "taskId_example" // string | 
    organizationId := "organizationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TasksAPI.GetChannelsChannelIdInteractionsIdTasksTaskId(context.Background(), taskId, organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TasksAPI.GetChannelsChannelIdInteractionsIdTasksTaskId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChannelsChannelIdInteractionsIdTasksTaskId`: Task
    fmt.Fprintf(os.Stdout, "Response from `TasksAPI.GetChannelsChannelIdInteractionsIdTasksTaskId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**taskId** | **string** |  | 
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChannelsChannelIdInteractionsIdTasksTaskIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Task**](Task.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTaskCompletionList

> TaskCompletionList GetTaskCompletionList(ctx, organizationId).InteractionId(interactionId).CampaignSendoutId(campaignSendoutId).Blocker(blocker).Done(done).Status(status).Usages(usages).Execute()

Task completion list

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    interactionId := "interactionId_example" // string |  (optional)
    campaignSendoutId := "campaignSendoutId_example" // string |  (optional)
    blocker := true // bool |  (optional)
    done := true // bool |  (optional)
    status := "status_example" // string |  (optional)
    usages := []string{"Inner_example"} // []string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TasksAPI.GetTaskCompletionList(context.Background(), organizationId).InteractionId(interactionId).CampaignSendoutId(campaignSendoutId).Blocker(blocker).Done(done).Status(status).Usages(usages).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TasksAPI.GetTaskCompletionList``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTaskCompletionList`: TaskCompletionList
    fmt.Fprintf(os.Stdout, "Response from `TasksAPI.GetTaskCompletionList`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTaskCompletionListRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **interactionId** | **string** |  | 
 **campaignSendoutId** | **string** |  | 
 **blocker** | **bool** |  | 
 **done** | **bool** |  | 
 **status** | **string** |  | 
 **usages** | **[]string** |  | 

### Return type

[**TaskCompletionList**](TaskCompletionList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTasks

> GetTasksResponse GetTasks(ctx, organizationId).InteractionId(interactionId).Blocker(blocker).ScriptId(scriptId).Usages(usages).Execute()

Get tasks



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    interactionId := "interactionId_example" // string | interaction_id filter (optional)
    blocker := true // bool | blockers (optional)
    scriptId := "scriptId_example" // string | script_id filter (optional)
    usages := []string{"Inner_example"} // []string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TasksAPI.GetTasks(context.Background(), organizationId).InteractionId(interactionId).Blocker(blocker).ScriptId(scriptId).Usages(usages).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TasksAPI.GetTasks``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTasks`: GetTasksResponse
    fmt.Fprintf(os.Stdout, "Response from `TasksAPI.GetTasks`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTasksRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **interactionId** | **string** | interaction_id filter | 
 **blocker** | **bool** | blockers | 
 **scriptId** | **string** | script_id filter | 
 **usages** | **[]string** |  | 

### Return type

[**GetTasksResponse**](GetTasksResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## MarkTaskAsClosed

> MarkTaskAsClosed(ctx, organizationId, completionId).MarkTaskAsClosedRequest(markTaskAsClosedRequest).Execute()

Mark task as closed

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    organizationId := "organizationId_example" // string | 
    completionId := "completionId_example" // string | 
    markTaskAsClosedRequest := *openapiclient.NewMarkTaskAsClosedRequest() // MarkTaskAsClosedRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.TasksAPI.MarkTaskAsClosed(context.Background(), organizationId, completionId).MarkTaskAsClosedRequest(markTaskAsClosedRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TasksAPI.MarkTaskAsClosed``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**completionId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiMarkTaskAsClosedRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **markTaskAsClosedRequest** | [**MarkTaskAsClosedRequest**](MarkTaskAsClosedRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PutChannelsChannelIdInteractionsIdTasksTaskId

> Task PutChannelsChannelIdInteractionsIdTasksTaskId(ctx, taskId, organizationId).MutableTaskData(mutableTaskData).Execute()

Update task

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/interactions"
)

func main() {
    taskId := "taskId_example" // string | 
    organizationId := "organizationId_example" // string | 
    mutableTaskData := *openapiclient.NewMutableTaskData() // MutableTaskData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.TasksAPI.PutChannelsChannelIdInteractionsIdTasksTaskId(context.Background(), taskId, organizationId).MutableTaskData(mutableTaskData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TasksAPI.PutChannelsChannelIdInteractionsIdTasksTaskId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PutChannelsChannelIdInteractionsIdTasksTaskId`: Task
    fmt.Fprintf(os.Stdout, "Response from `TasksAPI.PutChannelsChannelIdInteractionsIdTasksTaskId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**taskId** | **string** |  | 
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPutChannelsChannelIdInteractionsIdTasksTaskIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **mutableTaskData** | [**MutableTaskData**](MutableTaskData.md) |  | 

### Return type

[**Task**](Task.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

