# MutableTaskDataElement

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BlockId** | Pointer to **string** |  | [optional] 
**AssetId** | Pointer to **string** |  | [optional] 

## Methods

### NewMutableTaskDataElement

`func NewMutableTaskDataElement() *MutableTaskDataElement`

NewMutableTaskDataElement instantiates a new MutableTaskDataElement object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMutableTaskDataElementWithDefaults

`func NewMutableTaskDataElementWithDefaults() *MutableTaskDataElement`

NewMutableTaskDataElementWithDefaults instantiates a new MutableTaskDataElement object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetBlockId

`func (o *MutableTaskDataElement) GetBlockId() string`

GetBlockId returns the BlockId field if non-nil, zero value otherwise.

### GetBlockIdOk

`func (o *MutableTaskDataElement) GetBlockIdOk() (*string, bool)`

GetBlockIdOk returns a tuple with the BlockId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockId

`func (o *MutableTaskDataElement) SetBlockId(v string)`

SetBlockId sets BlockId field to given value.

### HasBlockId

`func (o *MutableTaskDataElement) HasBlockId() bool`

HasBlockId returns a boolean if a field has been set.

### GetAssetId

`func (o *MutableTaskDataElement) GetAssetId() string`

GetAssetId returns the AssetId field if non-nil, zero value otherwise.

### GetAssetIdOk

`func (o *MutableTaskDataElement) GetAssetIdOk() (*string, bool)`

GetAssetIdOk returns a tuple with the AssetId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssetId

`func (o *MutableTaskDataElement) SetAssetId(v string)`

SetAssetId sets AssetId field to given value.

### HasAssetId

`func (o *MutableTaskDataElement) HasAssetId() bool`

HasAssetId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


