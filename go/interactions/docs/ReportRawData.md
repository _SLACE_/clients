# ReportRawData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]ReportRawDataEntry**](ReportRawDataEntry.md) |  | 

## Methods

### NewReportRawData

`func NewReportRawData(results []ReportRawDataEntry, ) *ReportRawData`

NewReportRawData instantiates a new ReportRawData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReportRawDataWithDefaults

`func NewReportRawDataWithDefaults() *ReportRawData`

NewReportRawDataWithDefaults instantiates a new ReportRawData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *ReportRawData) GetResults() []ReportRawDataEntry`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *ReportRawData) GetResultsOk() (*[]ReportRawDataEntry, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *ReportRawData) SetResults(v []ReportRawDataEntry)`

SetResults sets Results field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


