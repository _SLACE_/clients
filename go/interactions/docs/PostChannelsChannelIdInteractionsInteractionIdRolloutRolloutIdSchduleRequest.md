# PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LiveFrom** | Pointer to **string** |  | [optional] 
**LiveTo** | Pointer to **string** |  | [optional] 
**ChannelIds** | Pointer to **[]string** | This value is optional | [optional] 
**Organizations** | Pointer to [**[]PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner**](PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner.md) | For global rollout, list of orgs and channels where rollout will be scheduled | [optional] 

## Methods

### NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest

`func NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest() *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest`

NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest instantiates a new PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestWithDefaults

`func NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestWithDefaults() *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest`

NewPostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestWithDefaults instantiates a new PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLiveFrom

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) GetLiveFrom() string`

GetLiveFrom returns the LiveFrom field if non-nil, zero value otherwise.

### GetLiveFromOk

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) GetLiveFromOk() (*string, bool)`

GetLiveFromOk returns a tuple with the LiveFrom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLiveFrom

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) SetLiveFrom(v string)`

SetLiveFrom sets LiveFrom field to given value.

### HasLiveFrom

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) HasLiveFrom() bool`

HasLiveFrom returns a boolean if a field has been set.

### GetLiveTo

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) GetLiveTo() string`

GetLiveTo returns the LiveTo field if non-nil, zero value otherwise.

### GetLiveToOk

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) GetLiveToOk() (*string, bool)`

GetLiveToOk returns a tuple with the LiveTo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLiveTo

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) SetLiveTo(v string)`

SetLiveTo sets LiveTo field to given value.

### HasLiveTo

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) HasLiveTo() bool`

HasLiveTo returns a boolean if a field has been set.

### GetChannelIds

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) GetChannelIds() []string`

GetChannelIds returns the ChannelIds field if non-nil, zero value otherwise.

### GetChannelIdsOk

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) GetChannelIdsOk() (*[]string, bool)`

GetChannelIdsOk returns a tuple with the ChannelIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelIds

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) SetChannelIds(v []string)`

SetChannelIds sets ChannelIds field to given value.

### HasChannelIds

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) HasChannelIds() bool`

HasChannelIds returns a boolean if a field has been set.

### GetOrganizations

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) GetOrganizations() []PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner`

GetOrganizations returns the Organizations field if non-nil, zero value otherwise.

### GetOrganizationsOk

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) GetOrganizationsOk() (*[]PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner, bool)`

GetOrganizationsOk returns a tuple with the Organizations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizations

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) SetOrganizations(v []PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequestOrganizationsInner)`

SetOrganizations sets Organizations field to given value.

### HasOrganizations

`func (o *PostChannelsChannelIdInteractionsInteractionIdRolloutRolloutIdSchduleRequest) HasOrganizations() bool`

HasOrganizations returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


