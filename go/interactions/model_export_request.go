/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
)

// checks if the ExportRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ExportRequest{}

// ExportRequest struct for ExportRequest
type ExportRequest struct {
	Name *string `json:"name,omitempty"`
	Type *ExportType `json:"type,omitempty"`
	Format *ExportFormat `json:"format,omitempty"`
	Filters ReportFilters `json:"filters,omitempty"`
}

// NewExportRequest instantiates a new ExportRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewExportRequest() *ExportRequest {
	this := ExportRequest{}
	return &this
}

// NewExportRequestWithDefaults instantiates a new ExportRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewExportRequestWithDefaults() *ExportRequest {
	this := ExportRequest{}
	return &this
}

// GetName returns the Name field value if set, zero value otherwise.
func (o *ExportRequest) GetName() string {
	if o == nil || IsNil(o.Name) {
		var ret string
		return ret
	}
	return *o.Name
}

// GetNameOk returns a tuple with the Name field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ExportRequest) GetNameOk() (*string, bool) {
	if o == nil || IsNil(o.Name) {
		return nil, false
	}
	return o.Name, true
}

// HasName returns a boolean if a field has been set.
func (o *ExportRequest) HasName() bool {
	if o != nil && !IsNil(o.Name) {
		return true
	}

	return false
}

// SetName gets a reference to the given string and assigns it to the Name field.
func (o *ExportRequest) SetName(v string) {
	o.Name = &v
}

// GetType returns the Type field value if set, zero value otherwise.
func (o *ExportRequest) GetType() ExportType {
	if o == nil || IsNil(o.Type) {
		var ret ExportType
		return ret
	}
	return *o.Type
}

// GetTypeOk returns a tuple with the Type field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ExportRequest) GetTypeOk() (*ExportType, bool) {
	if o == nil || IsNil(o.Type) {
		return nil, false
	}
	return o.Type, true
}

// HasType returns a boolean if a field has been set.
func (o *ExportRequest) HasType() bool {
	if o != nil && !IsNil(o.Type) {
		return true
	}

	return false
}

// SetType gets a reference to the given ExportType and assigns it to the Type field.
func (o *ExportRequest) SetType(v ExportType) {
	o.Type = &v
}

// GetFormat returns the Format field value if set, zero value otherwise.
func (o *ExportRequest) GetFormat() ExportFormat {
	if o == nil || IsNil(o.Format) {
		var ret ExportFormat
		return ret
	}
	return *o.Format
}

// GetFormatOk returns a tuple with the Format field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ExportRequest) GetFormatOk() (*ExportFormat, bool) {
	if o == nil || IsNil(o.Format) {
		return nil, false
	}
	return o.Format, true
}

// HasFormat returns a boolean if a field has been set.
func (o *ExportRequest) HasFormat() bool {
	if o != nil && !IsNil(o.Format) {
		return true
	}

	return false
}

// SetFormat gets a reference to the given ExportFormat and assigns it to the Format field.
func (o *ExportRequest) SetFormat(v ExportFormat) {
	o.Format = &v
}

// GetFilters returns the Filters field value if set, zero value otherwise.
func (o *ExportRequest) GetFilters() ReportFilters {
	if o == nil || IsNil(o.Filters) {
		var ret ReportFilters
		return ret
	}
	return o.Filters
}

// GetFiltersOk returns a tuple with the Filters field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ExportRequest) GetFiltersOk() (ReportFilters, bool) {
	if o == nil || IsNil(o.Filters) {
		return ReportFilters{}, false
	}
	return o.Filters, true
}

// HasFilters returns a boolean if a field has been set.
func (o *ExportRequest) HasFilters() bool {
	if o != nil && !IsNil(o.Filters) {
		return true
	}

	return false
}

// SetFilters gets a reference to the given ReportFilters and assigns it to the Filters field.
func (o *ExportRequest) SetFilters(v ReportFilters) {
	o.Filters = v
}

func (o ExportRequest) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ExportRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Name) {
		toSerialize["name"] = o.Name
	}
	if !IsNil(o.Type) {
		toSerialize["type"] = o.Type
	}
	if !IsNil(o.Format) {
		toSerialize["format"] = o.Format
	}
	if !IsNil(o.Filters) {
		toSerialize["filters"] = o.Filters
	}
	return toSerialize, nil
}

type NullableExportRequest struct {
	value *ExportRequest
	isSet bool
}

func (v NullableExportRequest) Get() *ExportRequest {
	return v.value
}

func (v *NullableExportRequest) Set(val *ExportRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableExportRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableExportRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableExportRequest(val *ExportRequest) *NullableExportRequest {
	return &NullableExportRequest{value: val, isSet: true}
}

func (v NullableExportRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableExportRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


