/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
)

// checks if the CxTemplatePreviewUrlsInner type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &CxTemplatePreviewUrlsInner{}

// CxTemplatePreviewUrlsInner struct for CxTemplatePreviewUrlsInner
type CxTemplatePreviewUrlsInner struct {
	Url *string `json:"url,omitempty"`
	Language *string `json:"language,omitempty"`
}

// NewCxTemplatePreviewUrlsInner instantiates a new CxTemplatePreviewUrlsInner object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCxTemplatePreviewUrlsInner() *CxTemplatePreviewUrlsInner {
	this := CxTemplatePreviewUrlsInner{}
	return &this
}

// NewCxTemplatePreviewUrlsInnerWithDefaults instantiates a new CxTemplatePreviewUrlsInner object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCxTemplatePreviewUrlsInnerWithDefaults() *CxTemplatePreviewUrlsInner {
	this := CxTemplatePreviewUrlsInner{}
	return &this
}

// GetUrl returns the Url field value if set, zero value otherwise.
func (o *CxTemplatePreviewUrlsInner) GetUrl() string {
	if o == nil || IsNil(o.Url) {
		var ret string
		return ret
	}
	return *o.Url
}

// GetUrlOk returns a tuple with the Url field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CxTemplatePreviewUrlsInner) GetUrlOk() (*string, bool) {
	if o == nil || IsNil(o.Url) {
		return nil, false
	}
	return o.Url, true
}

// HasUrl returns a boolean if a field has been set.
func (o *CxTemplatePreviewUrlsInner) HasUrl() bool {
	if o != nil && !IsNil(o.Url) {
		return true
	}

	return false
}

// SetUrl gets a reference to the given string and assigns it to the Url field.
func (o *CxTemplatePreviewUrlsInner) SetUrl(v string) {
	o.Url = &v
}

// GetLanguage returns the Language field value if set, zero value otherwise.
func (o *CxTemplatePreviewUrlsInner) GetLanguage() string {
	if o == nil || IsNil(o.Language) {
		var ret string
		return ret
	}
	return *o.Language
}

// GetLanguageOk returns a tuple with the Language field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CxTemplatePreviewUrlsInner) GetLanguageOk() (*string, bool) {
	if o == nil || IsNil(o.Language) {
		return nil, false
	}
	return o.Language, true
}

// HasLanguage returns a boolean if a field has been set.
func (o *CxTemplatePreviewUrlsInner) HasLanguage() bool {
	if o != nil && !IsNil(o.Language) {
		return true
	}

	return false
}

// SetLanguage gets a reference to the given string and assigns it to the Language field.
func (o *CxTemplatePreviewUrlsInner) SetLanguage(v string) {
	o.Language = &v
}

func (o CxTemplatePreviewUrlsInner) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o CxTemplatePreviewUrlsInner) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Url) {
		toSerialize["url"] = o.Url
	}
	if !IsNil(o.Language) {
		toSerialize["language"] = o.Language
	}
	return toSerialize, nil
}

type NullableCxTemplatePreviewUrlsInner struct {
	value *CxTemplatePreviewUrlsInner
	isSet bool
}

func (v NullableCxTemplatePreviewUrlsInner) Get() *CxTemplatePreviewUrlsInner {
	return v.value
}

func (v *NullableCxTemplatePreviewUrlsInner) Set(val *CxTemplatePreviewUrlsInner) {
	v.value = val
	v.isSet = true
}

func (v NullableCxTemplatePreviewUrlsInner) IsSet() bool {
	return v.isSet
}

func (v *NullableCxTemplatePreviewUrlsInner) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCxTemplatePreviewUrlsInner(val *CxTemplatePreviewUrlsInner) *NullableCxTemplatePreviewUrlsInner {
	return &NullableCxTemplatePreviewUrlsInner{value: val, isSet: true}
}

func (v NullableCxTemplatePreviewUrlsInner) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCxTemplatePreviewUrlsInner) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


