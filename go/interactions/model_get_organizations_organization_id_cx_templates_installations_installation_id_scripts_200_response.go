/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
)

// checks if the GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response{}

// GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response struct for GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response
type GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response struct {
	Results []map[string]interface{} `json:"results,omitempty"`
	Total *int32 `json:"total,omitempty"`
}

// NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response instantiates a new GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response() *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response {
	this := GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response{}
	return &this
}

// NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200ResponseWithDefaults instantiates a new GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200ResponseWithDefaults() *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response {
	this := GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response{}
	return &this
}

// GetResults returns the Results field value if set, zero value otherwise.
func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) GetResults() []map[string]interface{} {
	if o == nil || IsNil(o.Results) {
		var ret []map[string]interface{}
		return ret
	}
	return o.Results
}

// GetResultsOk returns a tuple with the Results field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) GetResultsOk() ([]map[string]interface{}, bool) {
	if o == nil || IsNil(o.Results) {
		return nil, false
	}
	return o.Results, true
}

// HasResults returns a boolean if a field has been set.
func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) HasResults() bool {
	if o != nil && !IsNil(o.Results) {
		return true
	}

	return false
}

// SetResults gets a reference to the given []map[string]interface{} and assigns it to the Results field.
func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) SetResults(v []map[string]interface{}) {
	o.Results = v
}

// GetTotal returns the Total field value if set, zero value otherwise.
func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) GetTotal() int32 {
	if o == nil || IsNil(o.Total) {
		var ret int32
		return ret
	}
	return *o.Total
}

// GetTotalOk returns a tuple with the Total field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) GetTotalOk() (*int32, bool) {
	if o == nil || IsNil(o.Total) {
		return nil, false
	}
	return o.Total, true
}

// HasTotal returns a boolean if a field has been set.
func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) HasTotal() bool {
	if o != nil && !IsNil(o.Total) {
		return true
	}

	return false
}

// SetTotal gets a reference to the given int32 and assigns it to the Total field.
func (o *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) SetTotal(v int32) {
	o.Total = &v
}

func (o GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Results) {
		toSerialize["results"] = o.Results
	}
	if !IsNil(o.Total) {
		toSerialize["total"] = o.Total
	}
	return toSerialize, nil
}

type NullableGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response struct {
	value *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response
	isSet bool
}

func (v NullableGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) Get() *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response {
	return v.value
}

func (v *NullableGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) Set(val *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) {
	v.value = val
	v.isSet = true
}

func (v NullableGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) IsSet() bool {
	return v.isSet
}

func (v *NullableGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response(val *GetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) *NullableGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response {
	return &NullableGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response{value: val, isSet: true}
}

func (v NullableGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGetOrganizationsOrganizationIdCxTemplatesInstallationsInstallationIdScripts200Response) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


