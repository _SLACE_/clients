/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
)

// checks if the MediaFee type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &MediaFee{}

// MediaFee struct for MediaFee
type MediaFee struct {
	Id *string `json:"id,omitempty"`
	RolloutId *string `json:"rollout_id,omitempty"`
	OrganizationId *string `json:"organization_id,omitempty"`
	ChannelId *string `json:"channel_id,omitempty"`
	MainRolloutId *string `json:"main_rollout_id,omitempty"`
	// decimal string
	Price *string `json:"price,omitempty"`
	TargetAgencyId *string `json:"target_agency_id,omitempty"`
	TargetAgencyPercentage *int32 `json:"target_agency_percentage,omitempty"`
	SourceAgencyId *string `json:"source_agency_id,omitempty"`
	SourceAgencyPercentage *int32 `json:"source_agency_percentage,omitempty"`
}

// NewMediaFee instantiates a new MediaFee object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMediaFee() *MediaFee {
	this := MediaFee{}
	return &this
}

// NewMediaFeeWithDefaults instantiates a new MediaFee object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMediaFeeWithDefaults() *MediaFee {
	this := MediaFee{}
	return &this
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *MediaFee) GetId() string {
	if o == nil || IsNil(o.Id) {
		var ret string
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MediaFee) GetIdOk() (*string, bool) {
	if o == nil || IsNil(o.Id) {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *MediaFee) HasId() bool {
	if o != nil && !IsNil(o.Id) {
		return true
	}

	return false
}

// SetId gets a reference to the given string and assigns it to the Id field.
func (o *MediaFee) SetId(v string) {
	o.Id = &v
}

// GetRolloutId returns the RolloutId field value if set, zero value otherwise.
func (o *MediaFee) GetRolloutId() string {
	if o == nil || IsNil(o.RolloutId) {
		var ret string
		return ret
	}
	return *o.RolloutId
}

// GetRolloutIdOk returns a tuple with the RolloutId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MediaFee) GetRolloutIdOk() (*string, bool) {
	if o == nil || IsNil(o.RolloutId) {
		return nil, false
	}
	return o.RolloutId, true
}

// HasRolloutId returns a boolean if a field has been set.
func (o *MediaFee) HasRolloutId() bool {
	if o != nil && !IsNil(o.RolloutId) {
		return true
	}

	return false
}

// SetRolloutId gets a reference to the given string and assigns it to the RolloutId field.
func (o *MediaFee) SetRolloutId(v string) {
	o.RolloutId = &v
}

// GetOrganizationId returns the OrganizationId field value if set, zero value otherwise.
func (o *MediaFee) GetOrganizationId() string {
	if o == nil || IsNil(o.OrganizationId) {
		var ret string
		return ret
	}
	return *o.OrganizationId
}

// GetOrganizationIdOk returns a tuple with the OrganizationId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MediaFee) GetOrganizationIdOk() (*string, bool) {
	if o == nil || IsNil(o.OrganizationId) {
		return nil, false
	}
	return o.OrganizationId, true
}

// HasOrganizationId returns a boolean if a field has been set.
func (o *MediaFee) HasOrganizationId() bool {
	if o != nil && !IsNil(o.OrganizationId) {
		return true
	}

	return false
}

// SetOrganizationId gets a reference to the given string and assigns it to the OrganizationId field.
func (o *MediaFee) SetOrganizationId(v string) {
	o.OrganizationId = &v
}

// GetChannelId returns the ChannelId field value if set, zero value otherwise.
func (o *MediaFee) GetChannelId() string {
	if o == nil || IsNil(o.ChannelId) {
		var ret string
		return ret
	}
	return *o.ChannelId
}

// GetChannelIdOk returns a tuple with the ChannelId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MediaFee) GetChannelIdOk() (*string, bool) {
	if o == nil || IsNil(o.ChannelId) {
		return nil, false
	}
	return o.ChannelId, true
}

// HasChannelId returns a boolean if a field has been set.
func (o *MediaFee) HasChannelId() bool {
	if o != nil && !IsNil(o.ChannelId) {
		return true
	}

	return false
}

// SetChannelId gets a reference to the given string and assigns it to the ChannelId field.
func (o *MediaFee) SetChannelId(v string) {
	o.ChannelId = &v
}

// GetMainRolloutId returns the MainRolloutId field value if set, zero value otherwise.
func (o *MediaFee) GetMainRolloutId() string {
	if o == nil || IsNil(o.MainRolloutId) {
		var ret string
		return ret
	}
	return *o.MainRolloutId
}

// GetMainRolloutIdOk returns a tuple with the MainRolloutId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MediaFee) GetMainRolloutIdOk() (*string, bool) {
	if o == nil || IsNil(o.MainRolloutId) {
		return nil, false
	}
	return o.MainRolloutId, true
}

// HasMainRolloutId returns a boolean if a field has been set.
func (o *MediaFee) HasMainRolloutId() bool {
	if o != nil && !IsNil(o.MainRolloutId) {
		return true
	}

	return false
}

// SetMainRolloutId gets a reference to the given string and assigns it to the MainRolloutId field.
func (o *MediaFee) SetMainRolloutId(v string) {
	o.MainRolloutId = &v
}

// GetPrice returns the Price field value if set, zero value otherwise.
func (o *MediaFee) GetPrice() string {
	if o == nil || IsNil(o.Price) {
		var ret string
		return ret
	}
	return *o.Price
}

// GetPriceOk returns a tuple with the Price field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MediaFee) GetPriceOk() (*string, bool) {
	if o == nil || IsNil(o.Price) {
		return nil, false
	}
	return o.Price, true
}

// HasPrice returns a boolean if a field has been set.
func (o *MediaFee) HasPrice() bool {
	if o != nil && !IsNil(o.Price) {
		return true
	}

	return false
}

// SetPrice gets a reference to the given string and assigns it to the Price field.
func (o *MediaFee) SetPrice(v string) {
	o.Price = &v
}

// GetTargetAgencyId returns the TargetAgencyId field value if set, zero value otherwise.
func (o *MediaFee) GetTargetAgencyId() string {
	if o == nil || IsNil(o.TargetAgencyId) {
		var ret string
		return ret
	}
	return *o.TargetAgencyId
}

// GetTargetAgencyIdOk returns a tuple with the TargetAgencyId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MediaFee) GetTargetAgencyIdOk() (*string, bool) {
	if o == nil || IsNil(o.TargetAgencyId) {
		return nil, false
	}
	return o.TargetAgencyId, true
}

// HasTargetAgencyId returns a boolean if a field has been set.
func (o *MediaFee) HasTargetAgencyId() bool {
	if o != nil && !IsNil(o.TargetAgencyId) {
		return true
	}

	return false
}

// SetTargetAgencyId gets a reference to the given string and assigns it to the TargetAgencyId field.
func (o *MediaFee) SetTargetAgencyId(v string) {
	o.TargetAgencyId = &v
}

// GetTargetAgencyPercentage returns the TargetAgencyPercentage field value if set, zero value otherwise.
func (o *MediaFee) GetTargetAgencyPercentage() int32 {
	if o == nil || IsNil(o.TargetAgencyPercentage) {
		var ret int32
		return ret
	}
	return *o.TargetAgencyPercentage
}

// GetTargetAgencyPercentageOk returns a tuple with the TargetAgencyPercentage field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MediaFee) GetTargetAgencyPercentageOk() (*int32, bool) {
	if o == nil || IsNil(o.TargetAgencyPercentage) {
		return nil, false
	}
	return o.TargetAgencyPercentage, true
}

// HasTargetAgencyPercentage returns a boolean if a field has been set.
func (o *MediaFee) HasTargetAgencyPercentage() bool {
	if o != nil && !IsNil(o.TargetAgencyPercentage) {
		return true
	}

	return false
}

// SetTargetAgencyPercentage gets a reference to the given int32 and assigns it to the TargetAgencyPercentage field.
func (o *MediaFee) SetTargetAgencyPercentage(v int32) {
	o.TargetAgencyPercentage = &v
}

// GetSourceAgencyId returns the SourceAgencyId field value if set, zero value otherwise.
func (o *MediaFee) GetSourceAgencyId() string {
	if o == nil || IsNil(o.SourceAgencyId) {
		var ret string
		return ret
	}
	return *o.SourceAgencyId
}

// GetSourceAgencyIdOk returns a tuple with the SourceAgencyId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MediaFee) GetSourceAgencyIdOk() (*string, bool) {
	if o == nil || IsNil(o.SourceAgencyId) {
		return nil, false
	}
	return o.SourceAgencyId, true
}

// HasSourceAgencyId returns a boolean if a field has been set.
func (o *MediaFee) HasSourceAgencyId() bool {
	if o != nil && !IsNil(o.SourceAgencyId) {
		return true
	}

	return false
}

// SetSourceAgencyId gets a reference to the given string and assigns it to the SourceAgencyId field.
func (o *MediaFee) SetSourceAgencyId(v string) {
	o.SourceAgencyId = &v
}

// GetSourceAgencyPercentage returns the SourceAgencyPercentage field value if set, zero value otherwise.
func (o *MediaFee) GetSourceAgencyPercentage() int32 {
	if o == nil || IsNil(o.SourceAgencyPercentage) {
		var ret int32
		return ret
	}
	return *o.SourceAgencyPercentage
}

// GetSourceAgencyPercentageOk returns a tuple with the SourceAgencyPercentage field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MediaFee) GetSourceAgencyPercentageOk() (*int32, bool) {
	if o == nil || IsNil(o.SourceAgencyPercentage) {
		return nil, false
	}
	return o.SourceAgencyPercentage, true
}

// HasSourceAgencyPercentage returns a boolean if a field has been set.
func (o *MediaFee) HasSourceAgencyPercentage() bool {
	if o != nil && !IsNil(o.SourceAgencyPercentage) {
		return true
	}

	return false
}

// SetSourceAgencyPercentage gets a reference to the given int32 and assigns it to the SourceAgencyPercentage field.
func (o *MediaFee) SetSourceAgencyPercentage(v int32) {
	o.SourceAgencyPercentage = &v
}

func (o MediaFee) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o MediaFee) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Id) {
		toSerialize["id"] = o.Id
	}
	if !IsNil(o.RolloutId) {
		toSerialize["rollout_id"] = o.RolloutId
	}
	if !IsNil(o.OrganizationId) {
		toSerialize["organization_id"] = o.OrganizationId
	}
	if !IsNil(o.ChannelId) {
		toSerialize["channel_id"] = o.ChannelId
	}
	if !IsNil(o.MainRolloutId) {
		toSerialize["main_rollout_id"] = o.MainRolloutId
	}
	if !IsNil(o.Price) {
		toSerialize["price"] = o.Price
	}
	if !IsNil(o.TargetAgencyId) {
		toSerialize["target_agency_id"] = o.TargetAgencyId
	}
	if !IsNil(o.TargetAgencyPercentage) {
		toSerialize["target_agency_percentage"] = o.TargetAgencyPercentage
	}
	if !IsNil(o.SourceAgencyId) {
		toSerialize["source_agency_id"] = o.SourceAgencyId
	}
	if !IsNil(o.SourceAgencyPercentage) {
		toSerialize["source_agency_percentage"] = o.SourceAgencyPercentage
	}
	return toSerialize, nil
}

type NullableMediaFee struct {
	value *MediaFee
	isSet bool
}

func (v NullableMediaFee) Get() *MediaFee {
	return v.value
}

func (v *NullableMediaFee) Set(val *MediaFee) {
	v.value = val
	v.isSet = true
}

func (v NullableMediaFee) IsSet() bool {
	return v.isSet
}

func (v *NullableMediaFee) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMediaFee(val *MediaFee) *NullableMediaFee {
	return &NullableMediaFee{value: val, isSet: true}
}

func (v NullableMediaFee) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMediaFee) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


