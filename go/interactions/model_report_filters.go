/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
	"time"
)

// checks if the ReportFilters type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ReportFilters{}

// ReportFilters struct for ReportFilters
type ReportFilters struct {
	// Rollout ID
	RolloutId *string `json:"rollout_id,omitempty"`
	// Result Type
	ResultType *string `json:"result_type,omitempty"`
	// List of Channel IDs
	ChannelIds []string `json:"channel_ids,omitempty"`
	// List of Interaction IDs
	IxIds []string `json:"ix_ids,omitempty"`
	// List of Location IDs
	LocationIds []string `json:"location_ids,omitempty"`
	// List of Entry Point IDs
	EpIds []string `json:"ep_ids,omitempty"`
	// List of Messengers
	Messengers []string `json:"messengers,omitempty"`
	// List of Trigger Types
	TriggerTypes []string `json:"trigger_types,omitempty"`
	// List of Entry Types
	EntryTypes []string `json:"entry_types,omitempty"`
	// List of Tags
	Tags []string `json:"tags,omitempty"`
	// List of Languages
	Languages []string `json:"languages,omitempty"`
	// List of Segments
	Segments []string `json:"segments,omitempty"`
	// List of Labels
	Labels []string `json:"labels,omitempty"`
	// List of Device Brands
	DeviceBrands []string `json:"device_brands,omitempty"`
	// List of Browser Names
	BrowserNames []string `json:"browser_names,omitempty"`
	// List of Operating System Names
	OsNames []string `json:"os_names,omitempty"`
	// List of Country Codes
	CountryCodes []string `json:"country_codes,omitempty"`
	// List of Versions
	Versions []string `json:"versions,omitempty"`
	// List of Context Fields
	ContextFields []string `json:"context_fields,omitempty"`
	// List of Context Values
	ContextValues []string `json:"context_values,omitempty"`
	// Fields to Group By
	GroupBy []ReportGroupBy `json:"group_by,omitempty"`
	// Context Field to Group By
	GroupByContextField *string `json:"group_by_context_field,omitempty"`
	// Start Time
	From *time.Time `json:"from,omitempty"`
	// End Time
	To *time.Time `json:"to,omitempty"`
	// Fields to Sort By
	Sort []string `json:"sort,omitempty"`
	// Maximum number of records to return
	Limit *int32 `json:"limit,omitempty"`
	// Maximum number of groups to return
	GroupLimit *int32 `json:"group_limit,omitempty"`
	// Number of records to skip
	Offset *int32 `json:"offset,omitempty"`
}

// NewReportFilters instantiates a new ReportFilters object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewReportFilters() *ReportFilters {
	this := ReportFilters{}
	return &this
}

// NewReportFiltersWithDefaults instantiates a new ReportFilters object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewReportFiltersWithDefaults() *ReportFilters {
	this := ReportFilters{}
	return &this
}

// GetRolloutId returns the RolloutId field value if set, zero value otherwise.
func (o *ReportFilters) GetRolloutId() string {
	if o == nil || IsNil(o.RolloutId) {
		var ret string
		return ret
	}
	return *o.RolloutId
}

// GetRolloutIdOk returns a tuple with the RolloutId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetRolloutIdOk() (*string, bool) {
	if o == nil || IsNil(o.RolloutId) {
		return nil, false
	}
	return o.RolloutId, true
}

// HasRolloutId returns a boolean if a field has been set.
func (o *ReportFilters) HasRolloutId() bool {
	if o != nil && !IsNil(o.RolloutId) {
		return true
	}

	return false
}

// SetRolloutId gets a reference to the given string and assigns it to the RolloutId field.
func (o *ReportFilters) SetRolloutId(v string) {
	o.RolloutId = &v
}

// GetResultType returns the ResultType field value if set, zero value otherwise.
func (o *ReportFilters) GetResultType() string {
	if o == nil || IsNil(o.ResultType) {
		var ret string
		return ret
	}
	return *o.ResultType
}

// GetResultTypeOk returns a tuple with the ResultType field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetResultTypeOk() (*string, bool) {
	if o == nil || IsNil(o.ResultType) {
		return nil, false
	}
	return o.ResultType, true
}

// HasResultType returns a boolean if a field has been set.
func (o *ReportFilters) HasResultType() bool {
	if o != nil && !IsNil(o.ResultType) {
		return true
	}

	return false
}

// SetResultType gets a reference to the given string and assigns it to the ResultType field.
func (o *ReportFilters) SetResultType(v string) {
	o.ResultType = &v
}

// GetChannelIds returns the ChannelIds field value if set, zero value otherwise.
func (o *ReportFilters) GetChannelIds() []string {
	if o == nil || IsNil(o.ChannelIds) {
		var ret []string
		return ret
	}
	return o.ChannelIds
}

// GetChannelIdsOk returns a tuple with the ChannelIds field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetChannelIdsOk() ([]string, bool) {
	if o == nil || IsNil(o.ChannelIds) {
		return nil, false
	}
	return o.ChannelIds, true
}

// HasChannelIds returns a boolean if a field has been set.
func (o *ReportFilters) HasChannelIds() bool {
	if o != nil && !IsNil(o.ChannelIds) {
		return true
	}

	return false
}

// SetChannelIds gets a reference to the given []string and assigns it to the ChannelIds field.
func (o *ReportFilters) SetChannelIds(v []string) {
	o.ChannelIds = v
}

// GetIxIds returns the IxIds field value if set, zero value otherwise.
func (o *ReportFilters) GetIxIds() []string {
	if o == nil || IsNil(o.IxIds) {
		var ret []string
		return ret
	}
	return o.IxIds
}

// GetIxIdsOk returns a tuple with the IxIds field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetIxIdsOk() ([]string, bool) {
	if o == nil || IsNil(o.IxIds) {
		return nil, false
	}
	return o.IxIds, true
}

// HasIxIds returns a boolean if a field has been set.
func (o *ReportFilters) HasIxIds() bool {
	if o != nil && !IsNil(o.IxIds) {
		return true
	}

	return false
}

// SetIxIds gets a reference to the given []string and assigns it to the IxIds field.
func (o *ReportFilters) SetIxIds(v []string) {
	o.IxIds = v
}

// GetLocationIds returns the LocationIds field value if set, zero value otherwise.
func (o *ReportFilters) GetLocationIds() []string {
	if o == nil || IsNil(o.LocationIds) {
		var ret []string
		return ret
	}
	return o.LocationIds
}

// GetLocationIdsOk returns a tuple with the LocationIds field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetLocationIdsOk() ([]string, bool) {
	if o == nil || IsNil(o.LocationIds) {
		return nil, false
	}
	return o.LocationIds, true
}

// HasLocationIds returns a boolean if a field has been set.
func (o *ReportFilters) HasLocationIds() bool {
	if o != nil && !IsNil(o.LocationIds) {
		return true
	}

	return false
}

// SetLocationIds gets a reference to the given []string and assigns it to the LocationIds field.
func (o *ReportFilters) SetLocationIds(v []string) {
	o.LocationIds = v
}

// GetEpIds returns the EpIds field value if set, zero value otherwise.
func (o *ReportFilters) GetEpIds() []string {
	if o == nil || IsNil(o.EpIds) {
		var ret []string
		return ret
	}
	return o.EpIds
}

// GetEpIdsOk returns a tuple with the EpIds field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetEpIdsOk() ([]string, bool) {
	if o == nil || IsNil(o.EpIds) {
		return nil, false
	}
	return o.EpIds, true
}

// HasEpIds returns a boolean if a field has been set.
func (o *ReportFilters) HasEpIds() bool {
	if o != nil && !IsNil(o.EpIds) {
		return true
	}

	return false
}

// SetEpIds gets a reference to the given []string and assigns it to the EpIds field.
func (o *ReportFilters) SetEpIds(v []string) {
	o.EpIds = v
}

// GetMessengers returns the Messengers field value if set, zero value otherwise.
func (o *ReportFilters) GetMessengers() []string {
	if o == nil || IsNil(o.Messengers) {
		var ret []string
		return ret
	}
	return o.Messengers
}

// GetMessengersOk returns a tuple with the Messengers field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetMessengersOk() ([]string, bool) {
	if o == nil || IsNil(o.Messengers) {
		return nil, false
	}
	return o.Messengers, true
}

// HasMessengers returns a boolean if a field has been set.
func (o *ReportFilters) HasMessengers() bool {
	if o != nil && !IsNil(o.Messengers) {
		return true
	}

	return false
}

// SetMessengers gets a reference to the given []string and assigns it to the Messengers field.
func (o *ReportFilters) SetMessengers(v []string) {
	o.Messengers = v
}

// GetTriggerTypes returns the TriggerTypes field value if set, zero value otherwise.
func (o *ReportFilters) GetTriggerTypes() []string {
	if o == nil || IsNil(o.TriggerTypes) {
		var ret []string
		return ret
	}
	return o.TriggerTypes
}

// GetTriggerTypesOk returns a tuple with the TriggerTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetTriggerTypesOk() ([]string, bool) {
	if o == nil || IsNil(o.TriggerTypes) {
		return nil, false
	}
	return o.TriggerTypes, true
}

// HasTriggerTypes returns a boolean if a field has been set.
func (o *ReportFilters) HasTriggerTypes() bool {
	if o != nil && !IsNil(o.TriggerTypes) {
		return true
	}

	return false
}

// SetTriggerTypes gets a reference to the given []string and assigns it to the TriggerTypes field.
func (o *ReportFilters) SetTriggerTypes(v []string) {
	o.TriggerTypes = v
}

// GetEntryTypes returns the EntryTypes field value if set, zero value otherwise.
func (o *ReportFilters) GetEntryTypes() []string {
	if o == nil || IsNil(o.EntryTypes) {
		var ret []string
		return ret
	}
	return o.EntryTypes
}

// GetEntryTypesOk returns a tuple with the EntryTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetEntryTypesOk() ([]string, bool) {
	if o == nil || IsNil(o.EntryTypes) {
		return nil, false
	}
	return o.EntryTypes, true
}

// HasEntryTypes returns a boolean if a field has been set.
func (o *ReportFilters) HasEntryTypes() bool {
	if o != nil && !IsNil(o.EntryTypes) {
		return true
	}

	return false
}

// SetEntryTypes gets a reference to the given []string and assigns it to the EntryTypes field.
func (o *ReportFilters) SetEntryTypes(v []string) {
	o.EntryTypes = v
}

// GetTags returns the Tags field value if set, zero value otherwise.
func (o *ReportFilters) GetTags() []string {
	if o == nil || IsNil(o.Tags) {
		var ret []string
		return ret
	}
	return o.Tags
}

// GetTagsOk returns a tuple with the Tags field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetTagsOk() ([]string, bool) {
	if o == nil || IsNil(o.Tags) {
		return nil, false
	}
	return o.Tags, true
}

// HasTags returns a boolean if a field has been set.
func (o *ReportFilters) HasTags() bool {
	if o != nil && !IsNil(o.Tags) {
		return true
	}

	return false
}

// SetTags gets a reference to the given []string and assigns it to the Tags field.
func (o *ReportFilters) SetTags(v []string) {
	o.Tags = v
}

// GetLanguages returns the Languages field value if set, zero value otherwise.
func (o *ReportFilters) GetLanguages() []string {
	if o == nil || IsNil(o.Languages) {
		var ret []string
		return ret
	}
	return o.Languages
}

// GetLanguagesOk returns a tuple with the Languages field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetLanguagesOk() ([]string, bool) {
	if o == nil || IsNil(o.Languages) {
		return nil, false
	}
	return o.Languages, true
}

// HasLanguages returns a boolean if a field has been set.
func (o *ReportFilters) HasLanguages() bool {
	if o != nil && !IsNil(o.Languages) {
		return true
	}

	return false
}

// SetLanguages gets a reference to the given []string and assigns it to the Languages field.
func (o *ReportFilters) SetLanguages(v []string) {
	o.Languages = v
}

// GetSegments returns the Segments field value if set, zero value otherwise.
func (o *ReportFilters) GetSegments() []string {
	if o == nil || IsNil(o.Segments) {
		var ret []string
		return ret
	}
	return o.Segments
}

// GetSegmentsOk returns a tuple with the Segments field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetSegmentsOk() ([]string, bool) {
	if o == nil || IsNil(o.Segments) {
		return nil, false
	}
	return o.Segments, true
}

// HasSegments returns a boolean if a field has been set.
func (o *ReportFilters) HasSegments() bool {
	if o != nil && !IsNil(o.Segments) {
		return true
	}

	return false
}

// SetSegments gets a reference to the given []string and assigns it to the Segments field.
func (o *ReportFilters) SetSegments(v []string) {
	o.Segments = v
}

// GetLabels returns the Labels field value if set, zero value otherwise.
func (o *ReportFilters) GetLabels() []string {
	if o == nil || IsNil(o.Labels) {
		var ret []string
		return ret
	}
	return o.Labels
}

// GetLabelsOk returns a tuple with the Labels field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetLabelsOk() ([]string, bool) {
	if o == nil || IsNil(o.Labels) {
		return nil, false
	}
	return o.Labels, true
}

// HasLabels returns a boolean if a field has been set.
func (o *ReportFilters) HasLabels() bool {
	if o != nil && !IsNil(o.Labels) {
		return true
	}

	return false
}

// SetLabels gets a reference to the given []string and assigns it to the Labels field.
func (o *ReportFilters) SetLabels(v []string) {
	o.Labels = v
}

// GetDeviceBrands returns the DeviceBrands field value if set, zero value otherwise.
func (o *ReportFilters) GetDeviceBrands() []string {
	if o == nil || IsNil(o.DeviceBrands) {
		var ret []string
		return ret
	}
	return o.DeviceBrands
}

// GetDeviceBrandsOk returns a tuple with the DeviceBrands field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetDeviceBrandsOk() ([]string, bool) {
	if o == nil || IsNil(o.DeviceBrands) {
		return nil, false
	}
	return o.DeviceBrands, true
}

// HasDeviceBrands returns a boolean if a field has been set.
func (o *ReportFilters) HasDeviceBrands() bool {
	if o != nil && !IsNil(o.DeviceBrands) {
		return true
	}

	return false
}

// SetDeviceBrands gets a reference to the given []string and assigns it to the DeviceBrands field.
func (o *ReportFilters) SetDeviceBrands(v []string) {
	o.DeviceBrands = v
}

// GetBrowserNames returns the BrowserNames field value if set, zero value otherwise.
func (o *ReportFilters) GetBrowserNames() []string {
	if o == nil || IsNil(o.BrowserNames) {
		var ret []string
		return ret
	}
	return o.BrowserNames
}

// GetBrowserNamesOk returns a tuple with the BrowserNames field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetBrowserNamesOk() ([]string, bool) {
	if o == nil || IsNil(o.BrowserNames) {
		return nil, false
	}
	return o.BrowserNames, true
}

// HasBrowserNames returns a boolean if a field has been set.
func (o *ReportFilters) HasBrowserNames() bool {
	if o != nil && !IsNil(o.BrowserNames) {
		return true
	}

	return false
}

// SetBrowserNames gets a reference to the given []string and assigns it to the BrowserNames field.
func (o *ReportFilters) SetBrowserNames(v []string) {
	o.BrowserNames = v
}

// GetOsNames returns the OsNames field value if set, zero value otherwise.
func (o *ReportFilters) GetOsNames() []string {
	if o == nil || IsNil(o.OsNames) {
		var ret []string
		return ret
	}
	return o.OsNames
}

// GetOsNamesOk returns a tuple with the OsNames field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetOsNamesOk() ([]string, bool) {
	if o == nil || IsNil(o.OsNames) {
		return nil, false
	}
	return o.OsNames, true
}

// HasOsNames returns a boolean if a field has been set.
func (o *ReportFilters) HasOsNames() bool {
	if o != nil && !IsNil(o.OsNames) {
		return true
	}

	return false
}

// SetOsNames gets a reference to the given []string and assigns it to the OsNames field.
func (o *ReportFilters) SetOsNames(v []string) {
	o.OsNames = v
}

// GetCountryCodes returns the CountryCodes field value if set, zero value otherwise.
func (o *ReportFilters) GetCountryCodes() []string {
	if o == nil || IsNil(o.CountryCodes) {
		var ret []string
		return ret
	}
	return o.CountryCodes
}

// GetCountryCodesOk returns a tuple with the CountryCodes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetCountryCodesOk() ([]string, bool) {
	if o == nil || IsNil(o.CountryCodes) {
		return nil, false
	}
	return o.CountryCodes, true
}

// HasCountryCodes returns a boolean if a field has been set.
func (o *ReportFilters) HasCountryCodes() bool {
	if o != nil && !IsNil(o.CountryCodes) {
		return true
	}

	return false
}

// SetCountryCodes gets a reference to the given []string and assigns it to the CountryCodes field.
func (o *ReportFilters) SetCountryCodes(v []string) {
	o.CountryCodes = v
}

// GetVersions returns the Versions field value if set, zero value otherwise.
func (o *ReportFilters) GetVersions() []string {
	if o == nil || IsNil(o.Versions) {
		var ret []string
		return ret
	}
	return o.Versions
}

// GetVersionsOk returns a tuple with the Versions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetVersionsOk() ([]string, bool) {
	if o == nil || IsNil(o.Versions) {
		return nil, false
	}
	return o.Versions, true
}

// HasVersions returns a boolean if a field has been set.
func (o *ReportFilters) HasVersions() bool {
	if o != nil && !IsNil(o.Versions) {
		return true
	}

	return false
}

// SetVersions gets a reference to the given []string and assigns it to the Versions field.
func (o *ReportFilters) SetVersions(v []string) {
	o.Versions = v
}

// GetContextFields returns the ContextFields field value if set, zero value otherwise.
func (o *ReportFilters) GetContextFields() []string {
	if o == nil || IsNil(o.ContextFields) {
		var ret []string
		return ret
	}
	return o.ContextFields
}

// GetContextFieldsOk returns a tuple with the ContextFields field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetContextFieldsOk() ([]string, bool) {
	if o == nil || IsNil(o.ContextFields) {
		return nil, false
	}
	return o.ContextFields, true
}

// HasContextFields returns a boolean if a field has been set.
func (o *ReportFilters) HasContextFields() bool {
	if o != nil && !IsNil(o.ContextFields) {
		return true
	}

	return false
}

// SetContextFields gets a reference to the given []string and assigns it to the ContextFields field.
func (o *ReportFilters) SetContextFields(v []string) {
	o.ContextFields = v
}

// GetContextValues returns the ContextValues field value if set, zero value otherwise.
func (o *ReportFilters) GetContextValues() []string {
	if o == nil || IsNil(o.ContextValues) {
		var ret []string
		return ret
	}
	return o.ContextValues
}

// GetContextValuesOk returns a tuple with the ContextValues field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetContextValuesOk() ([]string, bool) {
	if o == nil || IsNil(o.ContextValues) {
		return nil, false
	}
	return o.ContextValues, true
}

// HasContextValues returns a boolean if a field has been set.
func (o *ReportFilters) HasContextValues() bool {
	if o != nil && !IsNil(o.ContextValues) {
		return true
	}

	return false
}

// SetContextValues gets a reference to the given []string and assigns it to the ContextValues field.
func (o *ReportFilters) SetContextValues(v []string) {
	o.ContextValues = v
}

// GetGroupBy returns the GroupBy field value if set, zero value otherwise.
func (o *ReportFilters) GetGroupBy() []ReportGroupBy {
	if o == nil || IsNil(o.GroupBy) {
		var ret []ReportGroupBy
		return ret
	}
	return o.GroupBy
}

// GetGroupByOk returns a tuple with the GroupBy field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetGroupByOk() ([]ReportGroupBy, bool) {
	if o == nil || IsNil(o.GroupBy) {
		return nil, false
	}
	return o.GroupBy, true
}

// HasGroupBy returns a boolean if a field has been set.
func (o *ReportFilters) HasGroupBy() bool {
	if o != nil && !IsNil(o.GroupBy) {
		return true
	}

	return false
}

// SetGroupBy gets a reference to the given []ReportGroupBy and assigns it to the GroupBy field.
func (o *ReportFilters) SetGroupBy(v []ReportGroupBy) {
	o.GroupBy = v
}

// GetGroupByContextField returns the GroupByContextField field value if set, zero value otherwise.
func (o *ReportFilters) GetGroupByContextField() string {
	if o == nil || IsNil(o.GroupByContextField) {
		var ret string
		return ret
	}
	return *o.GroupByContextField
}

// GetGroupByContextFieldOk returns a tuple with the GroupByContextField field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetGroupByContextFieldOk() (*string, bool) {
	if o == nil || IsNil(o.GroupByContextField) {
		return nil, false
	}
	return o.GroupByContextField, true
}

// HasGroupByContextField returns a boolean if a field has been set.
func (o *ReportFilters) HasGroupByContextField() bool {
	if o != nil && !IsNil(o.GroupByContextField) {
		return true
	}

	return false
}

// SetGroupByContextField gets a reference to the given string and assigns it to the GroupByContextField field.
func (o *ReportFilters) SetGroupByContextField(v string) {
	o.GroupByContextField = &v
}

// GetFrom returns the From field value if set, zero value otherwise.
func (o *ReportFilters) GetFrom() time.Time {
	if o == nil || IsNil(o.From) {
		var ret time.Time
		return ret
	}
	return *o.From
}

// GetFromOk returns a tuple with the From field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetFromOk() (*time.Time, bool) {
	if o == nil || IsNil(o.From) {
		return nil, false
	}
	return o.From, true
}

// HasFrom returns a boolean if a field has been set.
func (o *ReportFilters) HasFrom() bool {
	if o != nil && !IsNil(o.From) {
		return true
	}

	return false
}

// SetFrom gets a reference to the given time.Time and assigns it to the From field.
func (o *ReportFilters) SetFrom(v time.Time) {
	o.From = &v
}

// GetTo returns the To field value if set, zero value otherwise.
func (o *ReportFilters) GetTo() time.Time {
	if o == nil || IsNil(o.To) {
		var ret time.Time
		return ret
	}
	return *o.To
}

// GetToOk returns a tuple with the To field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetToOk() (*time.Time, bool) {
	if o == nil || IsNil(o.To) {
		return nil, false
	}
	return o.To, true
}

// HasTo returns a boolean if a field has been set.
func (o *ReportFilters) HasTo() bool {
	if o != nil && !IsNil(o.To) {
		return true
	}

	return false
}

// SetTo gets a reference to the given time.Time and assigns it to the To field.
func (o *ReportFilters) SetTo(v time.Time) {
	o.To = &v
}

// GetSort returns the Sort field value if set, zero value otherwise.
func (o *ReportFilters) GetSort() []string {
	if o == nil || IsNil(o.Sort) {
		var ret []string
		return ret
	}
	return o.Sort
}

// GetSortOk returns a tuple with the Sort field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetSortOk() ([]string, bool) {
	if o == nil || IsNil(o.Sort) {
		return nil, false
	}
	return o.Sort, true
}

// HasSort returns a boolean if a field has been set.
func (o *ReportFilters) HasSort() bool {
	if o != nil && !IsNil(o.Sort) {
		return true
	}

	return false
}

// SetSort gets a reference to the given []string and assigns it to the Sort field.
func (o *ReportFilters) SetSort(v []string) {
	o.Sort = v
}

// GetLimit returns the Limit field value if set, zero value otherwise.
func (o *ReportFilters) GetLimit() int32 {
	if o == nil || IsNil(o.Limit) {
		var ret int32
		return ret
	}
	return *o.Limit
}

// GetLimitOk returns a tuple with the Limit field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetLimitOk() (*int32, bool) {
	if o == nil || IsNil(o.Limit) {
		return nil, false
	}
	return o.Limit, true
}

// HasLimit returns a boolean if a field has been set.
func (o *ReportFilters) HasLimit() bool {
	if o != nil && !IsNil(o.Limit) {
		return true
	}

	return false
}

// SetLimit gets a reference to the given int32 and assigns it to the Limit field.
func (o *ReportFilters) SetLimit(v int32) {
	o.Limit = &v
}

// GetGroupLimit returns the GroupLimit field value if set, zero value otherwise.
func (o *ReportFilters) GetGroupLimit() int32 {
	if o == nil || IsNil(o.GroupLimit) {
		var ret int32
		return ret
	}
	return *o.GroupLimit
}

// GetGroupLimitOk returns a tuple with the GroupLimit field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetGroupLimitOk() (*int32, bool) {
	if o == nil || IsNil(o.GroupLimit) {
		return nil, false
	}
	return o.GroupLimit, true
}

// HasGroupLimit returns a boolean if a field has been set.
func (o *ReportFilters) HasGroupLimit() bool {
	if o != nil && !IsNil(o.GroupLimit) {
		return true
	}

	return false
}

// SetGroupLimit gets a reference to the given int32 and assigns it to the GroupLimit field.
func (o *ReportFilters) SetGroupLimit(v int32) {
	o.GroupLimit = &v
}

// GetOffset returns the Offset field value if set, zero value otherwise.
func (o *ReportFilters) GetOffset() int32 {
	if o == nil || IsNil(o.Offset) {
		var ret int32
		return ret
	}
	return *o.Offset
}

// GetOffsetOk returns a tuple with the Offset field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ReportFilters) GetOffsetOk() (*int32, bool) {
	if o == nil || IsNil(o.Offset) {
		return nil, false
	}
	return o.Offset, true
}

// HasOffset returns a boolean if a field has been set.
func (o *ReportFilters) HasOffset() bool {
	if o != nil && !IsNil(o.Offset) {
		return true
	}

	return false
}

// SetOffset gets a reference to the given int32 and assigns it to the Offset field.
func (o *ReportFilters) SetOffset(v int32) {
	o.Offset = &v
}

func (o ReportFilters) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ReportFilters) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.RolloutId) {
		toSerialize["rollout_id"] = o.RolloutId
	}
	if !IsNil(o.ResultType) {
		toSerialize["result_type"] = o.ResultType
	}
	if !IsNil(o.ChannelIds) {
		toSerialize["channel_ids"] = o.ChannelIds
	}
	if !IsNil(o.IxIds) {
		toSerialize["ix_ids"] = o.IxIds
	}
	if !IsNil(o.LocationIds) {
		toSerialize["location_ids"] = o.LocationIds
	}
	if !IsNil(o.EpIds) {
		toSerialize["ep_ids"] = o.EpIds
	}
	if !IsNil(o.Messengers) {
		toSerialize["messengers"] = o.Messengers
	}
	if !IsNil(o.TriggerTypes) {
		toSerialize["trigger_types"] = o.TriggerTypes
	}
	if !IsNil(o.EntryTypes) {
		toSerialize["entry_types"] = o.EntryTypes
	}
	if !IsNil(o.Tags) {
		toSerialize["tags"] = o.Tags
	}
	if !IsNil(o.Languages) {
		toSerialize["languages"] = o.Languages
	}
	if !IsNil(o.Segments) {
		toSerialize["segments"] = o.Segments
	}
	if !IsNil(o.Labels) {
		toSerialize["labels"] = o.Labels
	}
	if !IsNil(o.DeviceBrands) {
		toSerialize["device_brands"] = o.DeviceBrands
	}
	if !IsNil(o.BrowserNames) {
		toSerialize["browser_names"] = o.BrowserNames
	}
	if !IsNil(o.OsNames) {
		toSerialize["os_names"] = o.OsNames
	}
	if !IsNil(o.CountryCodes) {
		toSerialize["country_codes"] = o.CountryCodes
	}
	if !IsNil(o.Versions) {
		toSerialize["versions"] = o.Versions
	}
	if !IsNil(o.ContextFields) {
		toSerialize["context_fields"] = o.ContextFields
	}
	if !IsNil(o.ContextValues) {
		toSerialize["context_values"] = o.ContextValues
	}
	if !IsNil(o.GroupBy) {
		toSerialize["group_by"] = o.GroupBy
	}
	if !IsNil(o.GroupByContextField) {
		toSerialize["group_by_context_field"] = o.GroupByContextField
	}
	if !IsNil(o.From) {
		toSerialize["from"] = o.From
	}
	if !IsNil(o.To) {
		toSerialize["to"] = o.To
	}
	if !IsNil(o.Sort) {
		toSerialize["sort"] = o.Sort
	}
	if !IsNil(o.Limit) {
		toSerialize["limit"] = o.Limit
	}
	if !IsNil(o.GroupLimit) {
		toSerialize["group_limit"] = o.GroupLimit
	}
	if !IsNil(o.Offset) {
		toSerialize["offset"] = o.Offset
	}
	return toSerialize, nil
}

type NullableReportFilters struct {
	value *ReportFilters
	isSet bool
}

func (v NullableReportFilters) Get() *ReportFilters {
	return v.value
}

func (v *NullableReportFilters) Set(val *ReportFilters) {
	v.value = val
	v.isSet = true
}

func (v NullableReportFilters) IsSet() bool {
	return v.isSet
}

func (v *NullableReportFilters) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableReportFilters(val *ReportFilters) *NullableReportFilters {
	return &NullableReportFilters{value: val, isSet: true}
}

func (v NullableReportFilters) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableReportFilters) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


