/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
)

// checks if the CloneResponse type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &CloneResponse{}

// CloneResponse struct for CloneResponse
type CloneResponse struct {
	Success *bool `json:"success,omitempty"`
	NewInteractions *CloneResponseNewInteractions `json:"new_interactions,omitempty"`
	Errors []RolloutError `json:"errors,omitempty"`
}

// NewCloneResponse instantiates a new CloneResponse object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCloneResponse() *CloneResponse {
	this := CloneResponse{}
	return &this
}

// NewCloneResponseWithDefaults instantiates a new CloneResponse object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCloneResponseWithDefaults() *CloneResponse {
	this := CloneResponse{}
	return &this
}

// GetSuccess returns the Success field value if set, zero value otherwise.
func (o *CloneResponse) GetSuccess() bool {
	if o == nil || IsNil(o.Success) {
		var ret bool
		return ret
	}
	return *o.Success
}

// GetSuccessOk returns a tuple with the Success field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CloneResponse) GetSuccessOk() (*bool, bool) {
	if o == nil || IsNil(o.Success) {
		return nil, false
	}
	return o.Success, true
}

// HasSuccess returns a boolean if a field has been set.
func (o *CloneResponse) HasSuccess() bool {
	if o != nil && !IsNil(o.Success) {
		return true
	}

	return false
}

// SetSuccess gets a reference to the given bool and assigns it to the Success field.
func (o *CloneResponse) SetSuccess(v bool) {
	o.Success = &v
}

// GetNewInteractions returns the NewInteractions field value if set, zero value otherwise.
func (o *CloneResponse) GetNewInteractions() CloneResponseNewInteractions {
	if o == nil || IsNil(o.NewInteractions) {
		var ret CloneResponseNewInteractions
		return ret
	}
	return *o.NewInteractions
}

// GetNewInteractionsOk returns a tuple with the NewInteractions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CloneResponse) GetNewInteractionsOk() (*CloneResponseNewInteractions, bool) {
	if o == nil || IsNil(o.NewInteractions) {
		return nil, false
	}
	return o.NewInteractions, true
}

// HasNewInteractions returns a boolean if a field has been set.
func (o *CloneResponse) HasNewInteractions() bool {
	if o != nil && !IsNil(o.NewInteractions) {
		return true
	}

	return false
}

// SetNewInteractions gets a reference to the given CloneResponseNewInteractions and assigns it to the NewInteractions field.
func (o *CloneResponse) SetNewInteractions(v CloneResponseNewInteractions) {
	o.NewInteractions = &v
}

// GetErrors returns the Errors field value if set, zero value otherwise.
func (o *CloneResponse) GetErrors() []RolloutError {
	if o == nil || IsNil(o.Errors) {
		var ret []RolloutError
		return ret
	}
	return o.Errors
}

// GetErrorsOk returns a tuple with the Errors field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *CloneResponse) GetErrorsOk() ([]RolloutError, bool) {
	if o == nil || IsNil(o.Errors) {
		return nil, false
	}
	return o.Errors, true
}

// HasErrors returns a boolean if a field has been set.
func (o *CloneResponse) HasErrors() bool {
	if o != nil && !IsNil(o.Errors) {
		return true
	}

	return false
}

// SetErrors gets a reference to the given []RolloutError and assigns it to the Errors field.
func (o *CloneResponse) SetErrors(v []RolloutError) {
	o.Errors = v
}

func (o CloneResponse) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o CloneResponse) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Success) {
		toSerialize["success"] = o.Success
	}
	if !IsNil(o.NewInteractions) {
		toSerialize["new_interactions"] = o.NewInteractions
	}
	if !IsNil(o.Errors) {
		toSerialize["errors"] = o.Errors
	}
	return toSerialize, nil
}

type NullableCloneResponse struct {
	value *CloneResponse
	isSet bool
}

func (v NullableCloneResponse) Get() *CloneResponse {
	return v.value
}

func (v *NullableCloneResponse) Set(val *CloneResponse) {
	v.value = val
	v.isSet = true
}

func (v NullableCloneResponse) IsSet() bool {
	return v.isSet
}

func (v *NullableCloneResponse) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCloneResponse(val *CloneResponse) *NullableCloneResponse {
	return &NullableCloneResponse{value: val, isSet: true}
}

func (v NullableCloneResponse) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCloneResponse) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


