/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
)

// checks if the DataSourceProperty type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &DataSourceProperty{}

// DataSourceProperty struct for DataSourceProperty
type DataSourceProperty struct {
	Path string `json:"path"`
	Description *string `json:"description,omitempty"`
	DataType DataType `json:"data_type"`
	Enum []string `json:"enum,omitempty"`
}

// NewDataSourceProperty instantiates a new DataSourceProperty object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewDataSourceProperty(path string, dataType DataType) *DataSourceProperty {
	this := DataSourceProperty{}
	this.Path = path
	this.DataType = dataType
	return &this
}

// NewDataSourcePropertyWithDefaults instantiates a new DataSourceProperty object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewDataSourcePropertyWithDefaults() *DataSourceProperty {
	this := DataSourceProperty{}
	return &this
}

// GetPath returns the Path field value
func (o *DataSourceProperty) GetPath() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Path
}

// GetPathOk returns a tuple with the Path field value
// and a boolean to check if the value has been set.
func (o *DataSourceProperty) GetPathOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Path, true
}

// SetPath sets field value
func (o *DataSourceProperty) SetPath(v string) {
	o.Path = v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *DataSourceProperty) GetDescription() string {
	if o == nil || IsNil(o.Description) {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *DataSourceProperty) GetDescriptionOk() (*string, bool) {
	if o == nil || IsNil(o.Description) {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *DataSourceProperty) HasDescription() bool {
	if o != nil && !IsNil(o.Description) {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *DataSourceProperty) SetDescription(v string) {
	o.Description = &v
}

// GetDataType returns the DataType field value
func (o *DataSourceProperty) GetDataType() DataType {
	if o == nil {
		var ret DataType
		return ret
	}

	return o.DataType
}

// GetDataTypeOk returns a tuple with the DataType field value
// and a boolean to check if the value has been set.
func (o *DataSourceProperty) GetDataTypeOk() (*DataType, bool) {
	if o == nil {
		return nil, false
	}
	return &o.DataType, true
}

// SetDataType sets field value
func (o *DataSourceProperty) SetDataType(v DataType) {
	o.DataType = v
}

// GetEnum returns the Enum field value if set, zero value otherwise.
func (o *DataSourceProperty) GetEnum() []string {
	if o == nil || IsNil(o.Enum) {
		var ret []string
		return ret
	}
	return o.Enum
}

// GetEnumOk returns a tuple with the Enum field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *DataSourceProperty) GetEnumOk() ([]string, bool) {
	if o == nil || IsNil(o.Enum) {
		return nil, false
	}
	return o.Enum, true
}

// HasEnum returns a boolean if a field has been set.
func (o *DataSourceProperty) HasEnum() bool {
	if o != nil && !IsNil(o.Enum) {
		return true
	}

	return false
}

// SetEnum gets a reference to the given []string and assigns it to the Enum field.
func (o *DataSourceProperty) SetEnum(v []string) {
	o.Enum = v
}

func (o DataSourceProperty) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o DataSourceProperty) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["path"] = o.Path
	if !IsNil(o.Description) {
		toSerialize["description"] = o.Description
	}
	toSerialize["data_type"] = o.DataType
	if !IsNil(o.Enum) {
		toSerialize["enum"] = o.Enum
	}
	return toSerialize, nil
}

type NullableDataSourceProperty struct {
	value *DataSourceProperty
	isSet bool
}

func (v NullableDataSourceProperty) Get() *DataSourceProperty {
	return v.value
}

func (v *NullableDataSourceProperty) Set(val *DataSourceProperty) {
	v.value = val
	v.isSet = true
}

func (v NullableDataSourceProperty) IsSet() bool {
	return v.isSet
}

func (v *NullableDataSourceProperty) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableDataSourceProperty(val *DataSourceProperty) *NullableDataSourceProperty {
	return &NullableDataSourceProperty{value: val, isSet: true}
}

func (v NullableDataSourceProperty) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableDataSourceProperty) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


