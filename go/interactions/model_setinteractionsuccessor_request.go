/*
Interactions

`Interactions are managed by our flow designer in UI, what you can control via api is how you start and run interaction flow for contact sending events.  Interaction can start in a multiple ways:  - EntryPont - when contact scans QR code ar opens entry point link  - event as trigger - this way you can start flow for single contact  ## Events Events are a data structure related to some contact action. You can use them whenever you want to start active interaction for contact but also inside interaction flow you can define points wher system should wait for custom event sent from your side.   Depending on your use case our predefined interactions might have defined events and instruction what events should be sent.  Events can be sent directly using our REST Api or triggered automatically by our integration modules.  ## Adding attributes to events Adding attributest to event can personalize communication, select branch flow based on attribute value or use them as perameters in other interaction flow actions. Use `body` property in event structure to add key-value attributes. 

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package interactions

import (
	"encoding/json"
)

// checks if the SetinteractionsuccessorRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &SetinteractionsuccessorRequest{}

// SetinteractionsuccessorRequest struct for SetinteractionsuccessorRequest
type SetinteractionsuccessorRequest struct {
	// Interaction id of successor 
	InteractionId *string `json:"interaction_id,omitempty"`
	EntryPointHandover *bool `json:"entry_point_handover,omitempty"`
	// Indicates if in versioning suscessor will be set as minor version
	Minor *bool `json:"minor,omitempty"`
}

// NewSetinteractionsuccessorRequest instantiates a new SetinteractionsuccessorRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewSetinteractionsuccessorRequest() *SetinteractionsuccessorRequest {
	this := SetinteractionsuccessorRequest{}
	var minor bool = false
	this.Minor = &minor
	return &this
}

// NewSetinteractionsuccessorRequestWithDefaults instantiates a new SetinteractionsuccessorRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewSetinteractionsuccessorRequestWithDefaults() *SetinteractionsuccessorRequest {
	this := SetinteractionsuccessorRequest{}
	var minor bool = false
	this.Minor = &minor
	return &this
}

// GetInteractionId returns the InteractionId field value if set, zero value otherwise.
func (o *SetinteractionsuccessorRequest) GetInteractionId() string {
	if o == nil || IsNil(o.InteractionId) {
		var ret string
		return ret
	}
	return *o.InteractionId
}

// GetInteractionIdOk returns a tuple with the InteractionId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SetinteractionsuccessorRequest) GetInteractionIdOk() (*string, bool) {
	if o == nil || IsNil(o.InteractionId) {
		return nil, false
	}
	return o.InteractionId, true
}

// HasInteractionId returns a boolean if a field has been set.
func (o *SetinteractionsuccessorRequest) HasInteractionId() bool {
	if o != nil && !IsNil(o.InteractionId) {
		return true
	}

	return false
}

// SetInteractionId gets a reference to the given string and assigns it to the InteractionId field.
func (o *SetinteractionsuccessorRequest) SetInteractionId(v string) {
	o.InteractionId = &v
}

// GetEntryPointHandover returns the EntryPointHandover field value if set, zero value otherwise.
func (o *SetinteractionsuccessorRequest) GetEntryPointHandover() bool {
	if o == nil || IsNil(o.EntryPointHandover) {
		var ret bool
		return ret
	}
	return *o.EntryPointHandover
}

// GetEntryPointHandoverOk returns a tuple with the EntryPointHandover field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SetinteractionsuccessorRequest) GetEntryPointHandoverOk() (*bool, bool) {
	if o == nil || IsNil(o.EntryPointHandover) {
		return nil, false
	}
	return o.EntryPointHandover, true
}

// HasEntryPointHandover returns a boolean if a field has been set.
func (o *SetinteractionsuccessorRequest) HasEntryPointHandover() bool {
	if o != nil && !IsNil(o.EntryPointHandover) {
		return true
	}

	return false
}

// SetEntryPointHandover gets a reference to the given bool and assigns it to the EntryPointHandover field.
func (o *SetinteractionsuccessorRequest) SetEntryPointHandover(v bool) {
	o.EntryPointHandover = &v
}

// GetMinor returns the Minor field value if set, zero value otherwise.
func (o *SetinteractionsuccessorRequest) GetMinor() bool {
	if o == nil || IsNil(o.Minor) {
		var ret bool
		return ret
	}
	return *o.Minor
}

// GetMinorOk returns a tuple with the Minor field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SetinteractionsuccessorRequest) GetMinorOk() (*bool, bool) {
	if o == nil || IsNil(o.Minor) {
		return nil, false
	}
	return o.Minor, true
}

// HasMinor returns a boolean if a field has been set.
func (o *SetinteractionsuccessorRequest) HasMinor() bool {
	if o != nil && !IsNil(o.Minor) {
		return true
	}

	return false
}

// SetMinor gets a reference to the given bool and assigns it to the Minor field.
func (o *SetinteractionsuccessorRequest) SetMinor(v bool) {
	o.Minor = &v
}

func (o SetinteractionsuccessorRequest) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o SetinteractionsuccessorRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.InteractionId) {
		toSerialize["interaction_id"] = o.InteractionId
	}
	if !IsNil(o.EntryPointHandover) {
		toSerialize["entry_point_handover"] = o.EntryPointHandover
	}
	if !IsNil(o.Minor) {
		toSerialize["minor"] = o.Minor
	}
	return toSerialize, nil
}

type NullableSetinteractionsuccessorRequest struct {
	value *SetinteractionsuccessorRequest
	isSet bool
}

func (v NullableSetinteractionsuccessorRequest) Get() *SetinteractionsuccessorRequest {
	return v.value
}

func (v *NullableSetinteractionsuccessorRequest) Set(val *SetinteractionsuccessorRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableSetinteractionsuccessorRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableSetinteractionsuccessorRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableSetinteractionsuccessorRequest(val *SetinteractionsuccessorRequest) *NullableSetinteractionsuccessorRequest {
	return &NullableSetinteractionsuccessorRequest{value: val, isSet: true}
}

func (v NullableSetinteractionsuccessorRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableSetinteractionsuccessorRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


