/*
polyprint.v1

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package polyprintapi

import (
	"encoding/json"
)

// checks if the CreatePrintOrder200Response type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &CreatePrintOrder200Response{}

// CreatePrintOrder200Response struct for CreatePrintOrder200Response
type CreatePrintOrder200Response struct {
	// either application/pdf or image/jpeg depending on \"mode\" 
	Mime interface{} `json:"mime,omitempty"`
	// array of binary streams
	Files interface{} `json:"files,omitempty"`
}

// NewCreatePrintOrder200Response instantiates a new CreatePrintOrder200Response object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCreatePrintOrder200Response() *CreatePrintOrder200Response {
	this := CreatePrintOrder200Response{}
	return &this
}

// NewCreatePrintOrder200ResponseWithDefaults instantiates a new CreatePrintOrder200Response object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCreatePrintOrder200ResponseWithDefaults() *CreatePrintOrder200Response {
	this := CreatePrintOrder200Response{}
	return &this
}

// GetMime returns the Mime field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *CreatePrintOrder200Response) GetMime() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.Mime
}

// GetMimeOk returns a tuple with the Mime field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *CreatePrintOrder200Response) GetMimeOk() (*interface{}, bool) {
	if o == nil || IsNil(o.Mime) {
		return nil, false
	}
	return &o.Mime, true
}

// HasMime returns a boolean if a field has been set.
func (o *CreatePrintOrder200Response) HasMime() bool {
	if o != nil && IsNil(o.Mime) {
		return true
	}

	return false
}

// SetMime gets a reference to the given interface{} and assigns it to the Mime field.
func (o *CreatePrintOrder200Response) SetMime(v interface{}) {
	o.Mime = v
}

// GetFiles returns the Files field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *CreatePrintOrder200Response) GetFiles() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.Files
}

// GetFilesOk returns a tuple with the Files field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *CreatePrintOrder200Response) GetFilesOk() (*interface{}, bool) {
	if o == nil || IsNil(o.Files) {
		return nil, false
	}
	return &o.Files, true
}

// HasFiles returns a boolean if a field has been set.
func (o *CreatePrintOrder200Response) HasFiles() bool {
	if o != nil && IsNil(o.Files) {
		return true
	}

	return false
}

// SetFiles gets a reference to the given interface{} and assigns it to the Files field.
func (o *CreatePrintOrder200Response) SetFiles(v interface{}) {
	o.Files = v
}

func (o CreatePrintOrder200Response) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o CreatePrintOrder200Response) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if o.Mime != nil {
		toSerialize["mime"] = o.Mime
	}
	if o.Files != nil {
		toSerialize["files"] = o.Files
	}
	return toSerialize, nil
}

type NullableCreatePrintOrder200Response struct {
	value *CreatePrintOrder200Response
	isSet bool
}

func (v NullableCreatePrintOrder200Response) Get() *CreatePrintOrder200Response {
	return v.value
}

func (v *NullableCreatePrintOrder200Response) Set(val *CreatePrintOrder200Response) {
	v.value = val
	v.isSet = true
}

func (v NullableCreatePrintOrder200Response) IsSet() bool {
	return v.isSet
}

func (v *NullableCreatePrintOrder200Response) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCreatePrintOrder200Response(val *CreatePrintOrder200Response) *NullableCreatePrintOrder200Response {
	return &NullableCreatePrintOrder200Response{value: val, isSet: true}
}

func (v NullableCreatePrintOrder200Response) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCreatePrintOrder200Response) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


