# CreatePrintOrderRequestProductionData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ReferenceId** | **interface{}** |  | 
**Date** | **interface{}** |  | 
**ProductId** | **interface{}** |  | 
**Quantity** | **interface{}** |  | 
**DeliveryContact** | [**PolyprintContact**](PolyprintContact.md) |  | 
**BillingContact** | [**PolyprintContact**](PolyprintContact.md) |  | 
**ExpectedDeliveryDate** | **interface{}** |  | 
**NfcCode** | Pointer to **interface{}** |  | [optional] 

## Methods

### NewCreatePrintOrderRequestProductionData

`func NewCreatePrintOrderRequestProductionData(referenceId interface{}, date interface{}, productId interface{}, quantity interface{}, deliveryContact PolyprintContact, billingContact PolyprintContact, expectedDeliveryDate interface{}, ) *CreatePrintOrderRequestProductionData`

NewCreatePrintOrderRequestProductionData instantiates a new CreatePrintOrderRequestProductionData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreatePrintOrderRequestProductionDataWithDefaults

`func NewCreatePrintOrderRequestProductionDataWithDefaults() *CreatePrintOrderRequestProductionData`

NewCreatePrintOrderRequestProductionDataWithDefaults instantiates a new CreatePrintOrderRequestProductionData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetReferenceId

`func (o *CreatePrintOrderRequestProductionData) GetReferenceId() interface{}`

GetReferenceId returns the ReferenceId field if non-nil, zero value otherwise.

### GetReferenceIdOk

`func (o *CreatePrintOrderRequestProductionData) GetReferenceIdOk() (*interface{}, bool)`

GetReferenceIdOk returns a tuple with the ReferenceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferenceId

`func (o *CreatePrintOrderRequestProductionData) SetReferenceId(v interface{})`

SetReferenceId sets ReferenceId field to given value.


### SetReferenceIdNil

`func (o *CreatePrintOrderRequestProductionData) SetReferenceIdNil(b bool)`

 SetReferenceIdNil sets the value for ReferenceId to be an explicit nil

### UnsetReferenceId
`func (o *CreatePrintOrderRequestProductionData) UnsetReferenceId()`

UnsetReferenceId ensures that no value is present for ReferenceId, not even an explicit nil
### GetDate

`func (o *CreatePrintOrderRequestProductionData) GetDate() interface{}`

GetDate returns the Date field if non-nil, zero value otherwise.

### GetDateOk

`func (o *CreatePrintOrderRequestProductionData) GetDateOk() (*interface{}, bool)`

GetDateOk returns a tuple with the Date field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDate

`func (o *CreatePrintOrderRequestProductionData) SetDate(v interface{})`

SetDate sets Date field to given value.


### SetDateNil

`func (o *CreatePrintOrderRequestProductionData) SetDateNil(b bool)`

 SetDateNil sets the value for Date to be an explicit nil

### UnsetDate
`func (o *CreatePrintOrderRequestProductionData) UnsetDate()`

UnsetDate ensures that no value is present for Date, not even an explicit nil
### GetProductId

`func (o *CreatePrintOrderRequestProductionData) GetProductId() interface{}`

GetProductId returns the ProductId field if non-nil, zero value otherwise.

### GetProductIdOk

`func (o *CreatePrintOrderRequestProductionData) GetProductIdOk() (*interface{}, bool)`

GetProductIdOk returns a tuple with the ProductId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductId

`func (o *CreatePrintOrderRequestProductionData) SetProductId(v interface{})`

SetProductId sets ProductId field to given value.


### SetProductIdNil

`func (o *CreatePrintOrderRequestProductionData) SetProductIdNil(b bool)`

 SetProductIdNil sets the value for ProductId to be an explicit nil

### UnsetProductId
`func (o *CreatePrintOrderRequestProductionData) UnsetProductId()`

UnsetProductId ensures that no value is present for ProductId, not even an explicit nil
### GetQuantity

`func (o *CreatePrintOrderRequestProductionData) GetQuantity() interface{}`

GetQuantity returns the Quantity field if non-nil, zero value otherwise.

### GetQuantityOk

`func (o *CreatePrintOrderRequestProductionData) GetQuantityOk() (*interface{}, bool)`

GetQuantityOk returns a tuple with the Quantity field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuantity

`func (o *CreatePrintOrderRequestProductionData) SetQuantity(v interface{})`

SetQuantity sets Quantity field to given value.


### SetQuantityNil

`func (o *CreatePrintOrderRequestProductionData) SetQuantityNil(b bool)`

 SetQuantityNil sets the value for Quantity to be an explicit nil

### UnsetQuantity
`func (o *CreatePrintOrderRequestProductionData) UnsetQuantity()`

UnsetQuantity ensures that no value is present for Quantity, not even an explicit nil
### GetDeliveryContact

`func (o *CreatePrintOrderRequestProductionData) GetDeliveryContact() PolyprintContact`

GetDeliveryContact returns the DeliveryContact field if non-nil, zero value otherwise.

### GetDeliveryContactOk

`func (o *CreatePrintOrderRequestProductionData) GetDeliveryContactOk() (*PolyprintContact, bool)`

GetDeliveryContactOk returns a tuple with the DeliveryContact field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDeliveryContact

`func (o *CreatePrintOrderRequestProductionData) SetDeliveryContact(v PolyprintContact)`

SetDeliveryContact sets DeliveryContact field to given value.


### GetBillingContact

`func (o *CreatePrintOrderRequestProductionData) GetBillingContact() PolyprintContact`

GetBillingContact returns the BillingContact field if non-nil, zero value otherwise.

### GetBillingContactOk

`func (o *CreatePrintOrderRequestProductionData) GetBillingContactOk() (*PolyprintContact, bool)`

GetBillingContactOk returns a tuple with the BillingContact field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillingContact

`func (o *CreatePrintOrderRequestProductionData) SetBillingContact(v PolyprintContact)`

SetBillingContact sets BillingContact field to given value.


### GetExpectedDeliveryDate

`func (o *CreatePrintOrderRequestProductionData) GetExpectedDeliveryDate() interface{}`

GetExpectedDeliveryDate returns the ExpectedDeliveryDate field if non-nil, zero value otherwise.

### GetExpectedDeliveryDateOk

`func (o *CreatePrintOrderRequestProductionData) GetExpectedDeliveryDateOk() (*interface{}, bool)`

GetExpectedDeliveryDateOk returns a tuple with the ExpectedDeliveryDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpectedDeliveryDate

`func (o *CreatePrintOrderRequestProductionData) SetExpectedDeliveryDate(v interface{})`

SetExpectedDeliveryDate sets ExpectedDeliveryDate field to given value.


### SetExpectedDeliveryDateNil

`func (o *CreatePrintOrderRequestProductionData) SetExpectedDeliveryDateNil(b bool)`

 SetExpectedDeliveryDateNil sets the value for ExpectedDeliveryDate to be an explicit nil

### UnsetExpectedDeliveryDate
`func (o *CreatePrintOrderRequestProductionData) UnsetExpectedDeliveryDate()`

UnsetExpectedDeliveryDate ensures that no value is present for ExpectedDeliveryDate, not even an explicit nil
### GetNfcCode

`func (o *CreatePrintOrderRequestProductionData) GetNfcCode() interface{}`

GetNfcCode returns the NfcCode field if non-nil, zero value otherwise.

### GetNfcCodeOk

`func (o *CreatePrintOrderRequestProductionData) GetNfcCodeOk() (*interface{}, bool)`

GetNfcCodeOk returns a tuple with the NfcCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNfcCode

`func (o *CreatePrintOrderRequestProductionData) SetNfcCode(v interface{})`

SetNfcCode sets NfcCode field to given value.

### HasNfcCode

`func (o *CreatePrintOrderRequestProductionData) HasNfcCode() bool`

HasNfcCode returns a boolean if a field has been set.

### SetNfcCodeNil

`func (o *CreatePrintOrderRequestProductionData) SetNfcCodeNil(b bool)`

 SetNfcCodeNil sets the value for NfcCode to be an explicit nil

### UnsetNfcCode
`func (o *CreatePrintOrderRequestProductionData) UnsetNfcCode()`

UnsetNfcCode ensures that no value is present for NfcCode, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


