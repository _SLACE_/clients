# CreatePrintOrderRequestPersonalizationData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FirstName** | Pointer to **interface{}** |  | [optional] 
**LastName** | Pointer to **interface{}** |  | [optional] 
**Email** | Pointer to **interface{}** |  | [optional] 
**Phone** | Pointer to **interface{}** |  | [optional] 
**JobTitle** | Pointer to **interface{}** |  | [optional] 
**Department** | Pointer to **interface{}** |  | [optional] 
**Branch** | Pointer to **interface{}** |  | [optional] 
**NfcCode** | Pointer to **interface{}** |  | [optional] 
**QrCode** | Pointer to **interface{}** |  | [optional] 
**Language** | Pointer to **interface{}** |  | [optional] 
**Custom1** | Pointer to **interface{}** |  | [optional] 
**Custom2** | Pointer to **interface{}** |  | [optional] 
**AcademicTitle** | Pointer to **interface{}** |  | [optional] 
**Organization** | Pointer to **interface{}** |  | [optional] 
**Link** | Pointer to **interface{}** |  | [optional] 
**Street** | Pointer to **interface{}** |  | [optional] 
**StreetNumber** | Pointer to **interface{}** |  | [optional] 
**DoorNumber** | Pointer to **interface{}** |  | [optional] 
**Zip** | Pointer to **interface{}** |  | [optional] 
**City** | Pointer to **interface{}** |  | [optional] 
**Country** | Pointer to **interface{}** |  | [optional] 
**SenderName** | Pointer to **interface{}** |  | [optional] 
**SenderStreet** | Pointer to **interface{}** |  | [optional] 
**SenderStreetNo** | Pointer to **interface{}** |  | [optional] 
**SenderDepartment** | Pointer to **interface{}** |  | [optional] 
**SenderBranch** | Pointer to **interface{}** |  | [optional] 
**SenderOrganization** | Pointer to **interface{}** |  | [optional] 
**SenderCity** | Pointer to **interface{}** |  | [optional] 
**SenderCountry** | Pointer to **interface{}** |  | [optional] 
**SenderZip** | Pointer to **interface{}** |  | [optional] 
**Date** | Pointer to **interface{}** |  | [optional] 

## Methods

### NewCreatePrintOrderRequestPersonalizationData

`func NewCreatePrintOrderRequestPersonalizationData() *CreatePrintOrderRequestPersonalizationData`

NewCreatePrintOrderRequestPersonalizationData instantiates a new CreatePrintOrderRequestPersonalizationData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreatePrintOrderRequestPersonalizationDataWithDefaults

`func NewCreatePrintOrderRequestPersonalizationDataWithDefaults() *CreatePrintOrderRequestPersonalizationData`

NewCreatePrintOrderRequestPersonalizationDataWithDefaults instantiates a new CreatePrintOrderRequestPersonalizationData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFirstName

`func (o *CreatePrintOrderRequestPersonalizationData) GetFirstName() interface{}`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetFirstNameOk() (*interface{}, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *CreatePrintOrderRequestPersonalizationData) SetFirstName(v interface{})`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *CreatePrintOrderRequestPersonalizationData) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### SetFirstNameNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetFirstNameNil(b bool)`

 SetFirstNameNil sets the value for FirstName to be an explicit nil

### UnsetFirstName
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetFirstName()`

UnsetFirstName ensures that no value is present for FirstName, not even an explicit nil
### GetLastName

`func (o *CreatePrintOrderRequestPersonalizationData) GetLastName() interface{}`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetLastNameOk() (*interface{}, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *CreatePrintOrderRequestPersonalizationData) SetLastName(v interface{})`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *CreatePrintOrderRequestPersonalizationData) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### SetLastNameNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetLastNameNil(b bool)`

 SetLastNameNil sets the value for LastName to be an explicit nil

### UnsetLastName
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetLastName()`

UnsetLastName ensures that no value is present for LastName, not even an explicit nil
### GetEmail

`func (o *CreatePrintOrderRequestPersonalizationData) GetEmail() interface{}`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetEmailOk() (*interface{}, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *CreatePrintOrderRequestPersonalizationData) SetEmail(v interface{})`

SetEmail sets Email field to given value.

### HasEmail

`func (o *CreatePrintOrderRequestPersonalizationData) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### SetEmailNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetEmailNil(b bool)`

 SetEmailNil sets the value for Email to be an explicit nil

### UnsetEmail
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetEmail()`

UnsetEmail ensures that no value is present for Email, not even an explicit nil
### GetPhone

`func (o *CreatePrintOrderRequestPersonalizationData) GetPhone() interface{}`

GetPhone returns the Phone field if non-nil, zero value otherwise.

### GetPhoneOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetPhoneOk() (*interface{}, bool)`

GetPhoneOk returns a tuple with the Phone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhone

`func (o *CreatePrintOrderRequestPersonalizationData) SetPhone(v interface{})`

SetPhone sets Phone field to given value.

### HasPhone

`func (o *CreatePrintOrderRequestPersonalizationData) HasPhone() bool`

HasPhone returns a boolean if a field has been set.

### SetPhoneNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetPhoneNil(b bool)`

 SetPhoneNil sets the value for Phone to be an explicit nil

### UnsetPhone
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetPhone()`

UnsetPhone ensures that no value is present for Phone, not even an explicit nil
### GetJobTitle

`func (o *CreatePrintOrderRequestPersonalizationData) GetJobTitle() interface{}`

GetJobTitle returns the JobTitle field if non-nil, zero value otherwise.

### GetJobTitleOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetJobTitleOk() (*interface{}, bool)`

GetJobTitleOk returns a tuple with the JobTitle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetJobTitle

`func (o *CreatePrintOrderRequestPersonalizationData) SetJobTitle(v interface{})`

SetJobTitle sets JobTitle field to given value.

### HasJobTitle

`func (o *CreatePrintOrderRequestPersonalizationData) HasJobTitle() bool`

HasJobTitle returns a boolean if a field has been set.

### SetJobTitleNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetJobTitleNil(b bool)`

 SetJobTitleNil sets the value for JobTitle to be an explicit nil

### UnsetJobTitle
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetJobTitle()`

UnsetJobTitle ensures that no value is present for JobTitle, not even an explicit nil
### GetDepartment

`func (o *CreatePrintOrderRequestPersonalizationData) GetDepartment() interface{}`

GetDepartment returns the Department field if non-nil, zero value otherwise.

### GetDepartmentOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetDepartmentOk() (*interface{}, bool)`

GetDepartmentOk returns a tuple with the Department field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDepartment

`func (o *CreatePrintOrderRequestPersonalizationData) SetDepartment(v interface{})`

SetDepartment sets Department field to given value.

### HasDepartment

`func (o *CreatePrintOrderRequestPersonalizationData) HasDepartment() bool`

HasDepartment returns a boolean if a field has been set.

### SetDepartmentNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetDepartmentNil(b bool)`

 SetDepartmentNil sets the value for Department to be an explicit nil

### UnsetDepartment
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetDepartment()`

UnsetDepartment ensures that no value is present for Department, not even an explicit nil
### GetBranch

`func (o *CreatePrintOrderRequestPersonalizationData) GetBranch() interface{}`

GetBranch returns the Branch field if non-nil, zero value otherwise.

### GetBranchOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetBranchOk() (*interface{}, bool)`

GetBranchOk returns a tuple with the Branch field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBranch

`func (o *CreatePrintOrderRequestPersonalizationData) SetBranch(v interface{})`

SetBranch sets Branch field to given value.

### HasBranch

`func (o *CreatePrintOrderRequestPersonalizationData) HasBranch() bool`

HasBranch returns a boolean if a field has been set.

### SetBranchNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetBranchNil(b bool)`

 SetBranchNil sets the value for Branch to be an explicit nil

### UnsetBranch
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetBranch()`

UnsetBranch ensures that no value is present for Branch, not even an explicit nil
### GetNfcCode

`func (o *CreatePrintOrderRequestPersonalizationData) GetNfcCode() interface{}`

GetNfcCode returns the NfcCode field if non-nil, zero value otherwise.

### GetNfcCodeOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetNfcCodeOk() (*interface{}, bool)`

GetNfcCodeOk returns a tuple with the NfcCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNfcCode

`func (o *CreatePrintOrderRequestPersonalizationData) SetNfcCode(v interface{})`

SetNfcCode sets NfcCode field to given value.

### HasNfcCode

`func (o *CreatePrintOrderRequestPersonalizationData) HasNfcCode() bool`

HasNfcCode returns a boolean if a field has been set.

### SetNfcCodeNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetNfcCodeNil(b bool)`

 SetNfcCodeNil sets the value for NfcCode to be an explicit nil

### UnsetNfcCode
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetNfcCode()`

UnsetNfcCode ensures that no value is present for NfcCode, not even an explicit nil
### GetQrCode

`func (o *CreatePrintOrderRequestPersonalizationData) GetQrCode() interface{}`

GetQrCode returns the QrCode field if non-nil, zero value otherwise.

### GetQrCodeOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetQrCodeOk() (*interface{}, bool)`

GetQrCodeOk returns a tuple with the QrCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQrCode

`func (o *CreatePrintOrderRequestPersonalizationData) SetQrCode(v interface{})`

SetQrCode sets QrCode field to given value.

### HasQrCode

`func (o *CreatePrintOrderRequestPersonalizationData) HasQrCode() bool`

HasQrCode returns a boolean if a field has been set.

### SetQrCodeNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetQrCodeNil(b bool)`

 SetQrCodeNil sets the value for QrCode to be an explicit nil

### UnsetQrCode
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetQrCode()`

UnsetQrCode ensures that no value is present for QrCode, not even an explicit nil
### GetLanguage

`func (o *CreatePrintOrderRequestPersonalizationData) GetLanguage() interface{}`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetLanguageOk() (*interface{}, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *CreatePrintOrderRequestPersonalizationData) SetLanguage(v interface{})`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *CreatePrintOrderRequestPersonalizationData) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.

### SetLanguageNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetLanguageNil(b bool)`

 SetLanguageNil sets the value for Language to be an explicit nil

### UnsetLanguage
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetLanguage()`

UnsetLanguage ensures that no value is present for Language, not even an explicit nil
### GetCustom1

`func (o *CreatePrintOrderRequestPersonalizationData) GetCustom1() interface{}`

GetCustom1 returns the Custom1 field if non-nil, zero value otherwise.

### GetCustom1Ok

`func (o *CreatePrintOrderRequestPersonalizationData) GetCustom1Ok() (*interface{}, bool)`

GetCustom1Ok returns a tuple with the Custom1 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustom1

`func (o *CreatePrintOrderRequestPersonalizationData) SetCustom1(v interface{})`

SetCustom1 sets Custom1 field to given value.

### HasCustom1

`func (o *CreatePrintOrderRequestPersonalizationData) HasCustom1() bool`

HasCustom1 returns a boolean if a field has been set.

### SetCustom1Nil

`func (o *CreatePrintOrderRequestPersonalizationData) SetCustom1Nil(b bool)`

 SetCustom1Nil sets the value for Custom1 to be an explicit nil

### UnsetCustom1
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetCustom1()`

UnsetCustom1 ensures that no value is present for Custom1, not even an explicit nil
### GetCustom2

`func (o *CreatePrintOrderRequestPersonalizationData) GetCustom2() interface{}`

GetCustom2 returns the Custom2 field if non-nil, zero value otherwise.

### GetCustom2Ok

`func (o *CreatePrintOrderRequestPersonalizationData) GetCustom2Ok() (*interface{}, bool)`

GetCustom2Ok returns a tuple with the Custom2 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustom2

`func (o *CreatePrintOrderRequestPersonalizationData) SetCustom2(v interface{})`

SetCustom2 sets Custom2 field to given value.

### HasCustom2

`func (o *CreatePrintOrderRequestPersonalizationData) HasCustom2() bool`

HasCustom2 returns a boolean if a field has been set.

### SetCustom2Nil

`func (o *CreatePrintOrderRequestPersonalizationData) SetCustom2Nil(b bool)`

 SetCustom2Nil sets the value for Custom2 to be an explicit nil

### UnsetCustom2
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetCustom2()`

UnsetCustom2 ensures that no value is present for Custom2, not even an explicit nil
### GetAcademicTitle

`func (o *CreatePrintOrderRequestPersonalizationData) GetAcademicTitle() interface{}`

GetAcademicTitle returns the AcademicTitle field if non-nil, zero value otherwise.

### GetAcademicTitleOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetAcademicTitleOk() (*interface{}, bool)`

GetAcademicTitleOk returns a tuple with the AcademicTitle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAcademicTitle

`func (o *CreatePrintOrderRequestPersonalizationData) SetAcademicTitle(v interface{})`

SetAcademicTitle sets AcademicTitle field to given value.

### HasAcademicTitle

`func (o *CreatePrintOrderRequestPersonalizationData) HasAcademicTitle() bool`

HasAcademicTitle returns a boolean if a field has been set.

### SetAcademicTitleNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetAcademicTitleNil(b bool)`

 SetAcademicTitleNil sets the value for AcademicTitle to be an explicit nil

### UnsetAcademicTitle
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetAcademicTitle()`

UnsetAcademicTitle ensures that no value is present for AcademicTitle, not even an explicit nil
### GetOrganization

`func (o *CreatePrintOrderRequestPersonalizationData) GetOrganization() interface{}`

GetOrganization returns the Organization field if non-nil, zero value otherwise.

### GetOrganizationOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetOrganizationOk() (*interface{}, bool)`

GetOrganizationOk returns a tuple with the Organization field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganization

`func (o *CreatePrintOrderRequestPersonalizationData) SetOrganization(v interface{})`

SetOrganization sets Organization field to given value.

### HasOrganization

`func (o *CreatePrintOrderRequestPersonalizationData) HasOrganization() bool`

HasOrganization returns a boolean if a field has been set.

### SetOrganizationNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetOrganizationNil(b bool)`

 SetOrganizationNil sets the value for Organization to be an explicit nil

### UnsetOrganization
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetOrganization()`

UnsetOrganization ensures that no value is present for Organization, not even an explicit nil
### GetLink

`func (o *CreatePrintOrderRequestPersonalizationData) GetLink() interface{}`

GetLink returns the Link field if non-nil, zero value otherwise.

### GetLinkOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetLinkOk() (*interface{}, bool)`

GetLinkOk returns a tuple with the Link field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLink

`func (o *CreatePrintOrderRequestPersonalizationData) SetLink(v interface{})`

SetLink sets Link field to given value.

### HasLink

`func (o *CreatePrintOrderRequestPersonalizationData) HasLink() bool`

HasLink returns a boolean if a field has been set.

### SetLinkNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetLinkNil(b bool)`

 SetLinkNil sets the value for Link to be an explicit nil

### UnsetLink
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetLink()`

UnsetLink ensures that no value is present for Link, not even an explicit nil
### GetStreet

`func (o *CreatePrintOrderRequestPersonalizationData) GetStreet() interface{}`

GetStreet returns the Street field if non-nil, zero value otherwise.

### GetStreetOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetStreetOk() (*interface{}, bool)`

GetStreetOk returns a tuple with the Street field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreet

`func (o *CreatePrintOrderRequestPersonalizationData) SetStreet(v interface{})`

SetStreet sets Street field to given value.

### HasStreet

`func (o *CreatePrintOrderRequestPersonalizationData) HasStreet() bool`

HasStreet returns a boolean if a field has been set.

### SetStreetNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetStreetNil(b bool)`

 SetStreetNil sets the value for Street to be an explicit nil

### UnsetStreet
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetStreet()`

UnsetStreet ensures that no value is present for Street, not even an explicit nil
### GetStreetNumber

`func (o *CreatePrintOrderRequestPersonalizationData) GetStreetNumber() interface{}`

GetStreetNumber returns the StreetNumber field if non-nil, zero value otherwise.

### GetStreetNumberOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetStreetNumberOk() (*interface{}, bool)`

GetStreetNumberOk returns a tuple with the StreetNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreetNumber

`func (o *CreatePrintOrderRequestPersonalizationData) SetStreetNumber(v interface{})`

SetStreetNumber sets StreetNumber field to given value.

### HasStreetNumber

`func (o *CreatePrintOrderRequestPersonalizationData) HasStreetNumber() bool`

HasStreetNumber returns a boolean if a field has been set.

### SetStreetNumberNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetStreetNumberNil(b bool)`

 SetStreetNumberNil sets the value for StreetNumber to be an explicit nil

### UnsetStreetNumber
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetStreetNumber()`

UnsetStreetNumber ensures that no value is present for StreetNumber, not even an explicit nil
### GetDoorNumber

`func (o *CreatePrintOrderRequestPersonalizationData) GetDoorNumber() interface{}`

GetDoorNumber returns the DoorNumber field if non-nil, zero value otherwise.

### GetDoorNumberOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetDoorNumberOk() (*interface{}, bool)`

GetDoorNumberOk returns a tuple with the DoorNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDoorNumber

`func (o *CreatePrintOrderRequestPersonalizationData) SetDoorNumber(v interface{})`

SetDoorNumber sets DoorNumber field to given value.

### HasDoorNumber

`func (o *CreatePrintOrderRequestPersonalizationData) HasDoorNumber() bool`

HasDoorNumber returns a boolean if a field has been set.

### SetDoorNumberNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetDoorNumberNil(b bool)`

 SetDoorNumberNil sets the value for DoorNumber to be an explicit nil

### UnsetDoorNumber
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetDoorNumber()`

UnsetDoorNumber ensures that no value is present for DoorNumber, not even an explicit nil
### GetZip

`func (o *CreatePrintOrderRequestPersonalizationData) GetZip() interface{}`

GetZip returns the Zip field if non-nil, zero value otherwise.

### GetZipOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetZipOk() (*interface{}, bool)`

GetZipOk returns a tuple with the Zip field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetZip

`func (o *CreatePrintOrderRequestPersonalizationData) SetZip(v interface{})`

SetZip sets Zip field to given value.

### HasZip

`func (o *CreatePrintOrderRequestPersonalizationData) HasZip() bool`

HasZip returns a boolean if a field has been set.

### SetZipNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetZipNil(b bool)`

 SetZipNil sets the value for Zip to be an explicit nil

### UnsetZip
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetZip()`

UnsetZip ensures that no value is present for Zip, not even an explicit nil
### GetCity

`func (o *CreatePrintOrderRequestPersonalizationData) GetCity() interface{}`

GetCity returns the City field if non-nil, zero value otherwise.

### GetCityOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetCityOk() (*interface{}, bool)`

GetCityOk returns a tuple with the City field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCity

`func (o *CreatePrintOrderRequestPersonalizationData) SetCity(v interface{})`

SetCity sets City field to given value.

### HasCity

`func (o *CreatePrintOrderRequestPersonalizationData) HasCity() bool`

HasCity returns a boolean if a field has been set.

### SetCityNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetCityNil(b bool)`

 SetCityNil sets the value for City to be an explicit nil

### UnsetCity
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetCity()`

UnsetCity ensures that no value is present for City, not even an explicit nil
### GetCountry

`func (o *CreatePrintOrderRequestPersonalizationData) GetCountry() interface{}`

GetCountry returns the Country field if non-nil, zero value otherwise.

### GetCountryOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetCountryOk() (*interface{}, bool)`

GetCountryOk returns a tuple with the Country field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountry

`func (o *CreatePrintOrderRequestPersonalizationData) SetCountry(v interface{})`

SetCountry sets Country field to given value.

### HasCountry

`func (o *CreatePrintOrderRequestPersonalizationData) HasCountry() bool`

HasCountry returns a boolean if a field has been set.

### SetCountryNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetCountryNil(b bool)`

 SetCountryNil sets the value for Country to be an explicit nil

### UnsetCountry
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetCountry()`

UnsetCountry ensures that no value is present for Country, not even an explicit nil
### GetSenderName

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderName() interface{}`

GetSenderName returns the SenderName field if non-nil, zero value otherwise.

### GetSenderNameOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderNameOk() (*interface{}, bool)`

GetSenderNameOk returns a tuple with the SenderName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderName

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderName(v interface{})`

SetSenderName sets SenderName field to given value.

### HasSenderName

`func (o *CreatePrintOrderRequestPersonalizationData) HasSenderName() bool`

HasSenderName returns a boolean if a field has been set.

### SetSenderNameNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderNameNil(b bool)`

 SetSenderNameNil sets the value for SenderName to be an explicit nil

### UnsetSenderName
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetSenderName()`

UnsetSenderName ensures that no value is present for SenderName, not even an explicit nil
### GetSenderStreet

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderStreet() interface{}`

GetSenderStreet returns the SenderStreet field if non-nil, zero value otherwise.

### GetSenderStreetOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderStreetOk() (*interface{}, bool)`

GetSenderStreetOk returns a tuple with the SenderStreet field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderStreet

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderStreet(v interface{})`

SetSenderStreet sets SenderStreet field to given value.

### HasSenderStreet

`func (o *CreatePrintOrderRequestPersonalizationData) HasSenderStreet() bool`

HasSenderStreet returns a boolean if a field has been set.

### SetSenderStreetNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderStreetNil(b bool)`

 SetSenderStreetNil sets the value for SenderStreet to be an explicit nil

### UnsetSenderStreet
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetSenderStreet()`

UnsetSenderStreet ensures that no value is present for SenderStreet, not even an explicit nil
### GetSenderStreetNo

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderStreetNo() interface{}`

GetSenderStreetNo returns the SenderStreetNo field if non-nil, zero value otherwise.

### GetSenderStreetNoOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderStreetNoOk() (*interface{}, bool)`

GetSenderStreetNoOk returns a tuple with the SenderStreetNo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderStreetNo

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderStreetNo(v interface{})`

SetSenderStreetNo sets SenderStreetNo field to given value.

### HasSenderStreetNo

`func (o *CreatePrintOrderRequestPersonalizationData) HasSenderStreetNo() bool`

HasSenderStreetNo returns a boolean if a field has been set.

### SetSenderStreetNoNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderStreetNoNil(b bool)`

 SetSenderStreetNoNil sets the value for SenderStreetNo to be an explicit nil

### UnsetSenderStreetNo
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetSenderStreetNo()`

UnsetSenderStreetNo ensures that no value is present for SenderStreetNo, not even an explicit nil
### GetSenderDepartment

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderDepartment() interface{}`

GetSenderDepartment returns the SenderDepartment field if non-nil, zero value otherwise.

### GetSenderDepartmentOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderDepartmentOk() (*interface{}, bool)`

GetSenderDepartmentOk returns a tuple with the SenderDepartment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderDepartment

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderDepartment(v interface{})`

SetSenderDepartment sets SenderDepartment field to given value.

### HasSenderDepartment

`func (o *CreatePrintOrderRequestPersonalizationData) HasSenderDepartment() bool`

HasSenderDepartment returns a boolean if a field has been set.

### SetSenderDepartmentNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderDepartmentNil(b bool)`

 SetSenderDepartmentNil sets the value for SenderDepartment to be an explicit nil

### UnsetSenderDepartment
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetSenderDepartment()`

UnsetSenderDepartment ensures that no value is present for SenderDepartment, not even an explicit nil
### GetSenderBranch

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderBranch() interface{}`

GetSenderBranch returns the SenderBranch field if non-nil, zero value otherwise.

### GetSenderBranchOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderBranchOk() (*interface{}, bool)`

GetSenderBranchOk returns a tuple with the SenderBranch field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderBranch

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderBranch(v interface{})`

SetSenderBranch sets SenderBranch field to given value.

### HasSenderBranch

`func (o *CreatePrintOrderRequestPersonalizationData) HasSenderBranch() bool`

HasSenderBranch returns a boolean if a field has been set.

### SetSenderBranchNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderBranchNil(b bool)`

 SetSenderBranchNil sets the value for SenderBranch to be an explicit nil

### UnsetSenderBranch
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetSenderBranch()`

UnsetSenderBranch ensures that no value is present for SenderBranch, not even an explicit nil
### GetSenderOrganization

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderOrganization() interface{}`

GetSenderOrganization returns the SenderOrganization field if non-nil, zero value otherwise.

### GetSenderOrganizationOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderOrganizationOk() (*interface{}, bool)`

GetSenderOrganizationOk returns a tuple with the SenderOrganization field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderOrganization

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderOrganization(v interface{})`

SetSenderOrganization sets SenderOrganization field to given value.

### HasSenderOrganization

`func (o *CreatePrintOrderRequestPersonalizationData) HasSenderOrganization() bool`

HasSenderOrganization returns a boolean if a field has been set.

### SetSenderOrganizationNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderOrganizationNil(b bool)`

 SetSenderOrganizationNil sets the value for SenderOrganization to be an explicit nil

### UnsetSenderOrganization
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetSenderOrganization()`

UnsetSenderOrganization ensures that no value is present for SenderOrganization, not even an explicit nil
### GetSenderCity

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderCity() interface{}`

GetSenderCity returns the SenderCity field if non-nil, zero value otherwise.

### GetSenderCityOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderCityOk() (*interface{}, bool)`

GetSenderCityOk returns a tuple with the SenderCity field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderCity

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderCity(v interface{})`

SetSenderCity sets SenderCity field to given value.

### HasSenderCity

`func (o *CreatePrintOrderRequestPersonalizationData) HasSenderCity() bool`

HasSenderCity returns a boolean if a field has been set.

### SetSenderCityNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderCityNil(b bool)`

 SetSenderCityNil sets the value for SenderCity to be an explicit nil

### UnsetSenderCity
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetSenderCity()`

UnsetSenderCity ensures that no value is present for SenderCity, not even an explicit nil
### GetSenderCountry

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderCountry() interface{}`

GetSenderCountry returns the SenderCountry field if non-nil, zero value otherwise.

### GetSenderCountryOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderCountryOk() (*interface{}, bool)`

GetSenderCountryOk returns a tuple with the SenderCountry field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderCountry

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderCountry(v interface{})`

SetSenderCountry sets SenderCountry field to given value.

### HasSenderCountry

`func (o *CreatePrintOrderRequestPersonalizationData) HasSenderCountry() bool`

HasSenderCountry returns a boolean if a field has been set.

### SetSenderCountryNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderCountryNil(b bool)`

 SetSenderCountryNil sets the value for SenderCountry to be an explicit nil

### UnsetSenderCountry
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetSenderCountry()`

UnsetSenderCountry ensures that no value is present for SenderCountry, not even an explicit nil
### GetSenderZip

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderZip() interface{}`

GetSenderZip returns the SenderZip field if non-nil, zero value otherwise.

### GetSenderZipOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetSenderZipOk() (*interface{}, bool)`

GetSenderZipOk returns a tuple with the SenderZip field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderZip

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderZip(v interface{})`

SetSenderZip sets SenderZip field to given value.

### HasSenderZip

`func (o *CreatePrintOrderRequestPersonalizationData) HasSenderZip() bool`

HasSenderZip returns a boolean if a field has been set.

### SetSenderZipNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetSenderZipNil(b bool)`

 SetSenderZipNil sets the value for SenderZip to be an explicit nil

### UnsetSenderZip
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetSenderZip()`

UnsetSenderZip ensures that no value is present for SenderZip, not even an explicit nil
### GetDate

`func (o *CreatePrintOrderRequestPersonalizationData) GetDate() interface{}`

GetDate returns the Date field if non-nil, zero value otherwise.

### GetDateOk

`func (o *CreatePrintOrderRequestPersonalizationData) GetDateOk() (*interface{}, bool)`

GetDateOk returns a tuple with the Date field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDate

`func (o *CreatePrintOrderRequestPersonalizationData) SetDate(v interface{})`

SetDate sets Date field to given value.

### HasDate

`func (o *CreatePrintOrderRequestPersonalizationData) HasDate() bool`

HasDate returns a boolean if a field has been set.

### SetDateNil

`func (o *CreatePrintOrderRequestPersonalizationData) SetDateNil(b bool)`

 SetDateNil sets the value for Date to be an explicit nil

### UnsetDate
`func (o *CreatePrintOrderRequestPersonalizationData) UnsetDate()`

UnsetDate ensures that no value is present for Date, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


