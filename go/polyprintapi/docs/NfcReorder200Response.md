# NfcReorder200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Link** | Pointer to **interface{}** |  | [optional] 

## Methods

### NewNfcReorder200Response

`func NewNfcReorder200Response() *NfcReorder200Response`

NewNfcReorder200Response instantiates a new NfcReorder200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNfcReorder200ResponseWithDefaults

`func NewNfcReorder200ResponseWithDefaults() *NfcReorder200Response`

NewNfcReorder200ResponseWithDefaults instantiates a new NfcReorder200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLink

`func (o *NfcReorder200Response) GetLink() interface{}`

GetLink returns the Link field if non-nil, zero value otherwise.

### GetLinkOk

`func (o *NfcReorder200Response) GetLinkOk() (*interface{}, bool)`

GetLinkOk returns a tuple with the Link field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLink

`func (o *NfcReorder200Response) SetLink(v interface{})`

SetLink sets Link field to given value.

### HasLink

`func (o *NfcReorder200Response) HasLink() bool`

HasLink returns a boolean if a field has been set.

### SetLinkNil

`func (o *NfcReorder200Response) SetLinkNil(b bool)`

 SetLinkNil sets the value for Link to be an explicit nil

### UnsetLink
`func (o *NfcReorder200Response) UnsetLink()`

UnsetLink ensures that no value is present for Link, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


