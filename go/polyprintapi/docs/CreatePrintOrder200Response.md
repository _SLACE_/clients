# CreatePrintOrder200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Mime** | Pointer to **interface{}** | either application/pdf or image/jpeg depending on \&quot;mode\&quot;  | [optional] 
**Files** | Pointer to **interface{}** | array of binary streams | [optional] 

## Methods

### NewCreatePrintOrder200Response

`func NewCreatePrintOrder200Response() *CreatePrintOrder200Response`

NewCreatePrintOrder200Response instantiates a new CreatePrintOrder200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreatePrintOrder200ResponseWithDefaults

`func NewCreatePrintOrder200ResponseWithDefaults() *CreatePrintOrder200Response`

NewCreatePrintOrder200ResponseWithDefaults instantiates a new CreatePrintOrder200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMime

`func (o *CreatePrintOrder200Response) GetMime() interface{}`

GetMime returns the Mime field if non-nil, zero value otherwise.

### GetMimeOk

`func (o *CreatePrintOrder200Response) GetMimeOk() (*interface{}, bool)`

GetMimeOk returns a tuple with the Mime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMime

`func (o *CreatePrintOrder200Response) SetMime(v interface{})`

SetMime sets Mime field to given value.

### HasMime

`func (o *CreatePrintOrder200Response) HasMime() bool`

HasMime returns a boolean if a field has been set.

### SetMimeNil

`func (o *CreatePrintOrder200Response) SetMimeNil(b bool)`

 SetMimeNil sets the value for Mime to be an explicit nil

### UnsetMime
`func (o *CreatePrintOrder200Response) UnsetMime()`

UnsetMime ensures that no value is present for Mime, not even an explicit nil
### GetFiles

`func (o *CreatePrintOrder200Response) GetFiles() interface{}`

GetFiles returns the Files field if non-nil, zero value otherwise.

### GetFilesOk

`func (o *CreatePrintOrder200Response) GetFilesOk() (*interface{}, bool)`

GetFilesOk returns a tuple with the Files field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFiles

`func (o *CreatePrintOrder200Response) SetFiles(v interface{})`

SetFiles sets Files field to given value.

### HasFiles

`func (o *CreatePrintOrder200Response) HasFiles() bool`

HasFiles returns a boolean if a field has been set.

### SetFilesNil

`func (o *CreatePrintOrder200Response) SetFilesNil(b bool)`

 SetFilesNil sets the value for Files to be an explicit nil

### UnsetFiles
`func (o *CreatePrintOrder200Response) UnsetFiles()`

UnsetFiles ensures that no value is present for Files, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


