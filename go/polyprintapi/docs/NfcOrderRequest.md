# NfcOrderRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Address** | Pointer to [**NfcOrderRequestAddress**](NfcOrderRequestAddress.md) |  | [optional] 
**ChannelId** | Pointer to **interface{}** |  | [optional] 
**CustomerExperienceId** | Pointer to **interface{}** |  | [optional] 
**UserId** | Pointer to **interface{}** |  | [optional] 

## Methods

### NewNfcOrderRequest

`func NewNfcOrderRequest() *NfcOrderRequest`

NewNfcOrderRequest instantiates a new NfcOrderRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNfcOrderRequestWithDefaults

`func NewNfcOrderRequestWithDefaults() *NfcOrderRequest`

NewNfcOrderRequestWithDefaults instantiates a new NfcOrderRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAddress

`func (o *NfcOrderRequest) GetAddress() NfcOrderRequestAddress`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *NfcOrderRequest) GetAddressOk() (*NfcOrderRequestAddress, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *NfcOrderRequest) SetAddress(v NfcOrderRequestAddress)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *NfcOrderRequest) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetChannelId

`func (o *NfcOrderRequest) GetChannelId() interface{}`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *NfcOrderRequest) GetChannelIdOk() (*interface{}, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *NfcOrderRequest) SetChannelId(v interface{})`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *NfcOrderRequest) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### SetChannelIdNil

`func (o *NfcOrderRequest) SetChannelIdNil(b bool)`

 SetChannelIdNil sets the value for ChannelId to be an explicit nil

### UnsetChannelId
`func (o *NfcOrderRequest) UnsetChannelId()`

UnsetChannelId ensures that no value is present for ChannelId, not even an explicit nil
### GetCustomerExperienceId

`func (o *NfcOrderRequest) GetCustomerExperienceId() interface{}`

GetCustomerExperienceId returns the CustomerExperienceId field if non-nil, zero value otherwise.

### GetCustomerExperienceIdOk

`func (o *NfcOrderRequest) GetCustomerExperienceIdOk() (*interface{}, bool)`

GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerExperienceId

`func (o *NfcOrderRequest) SetCustomerExperienceId(v interface{})`

SetCustomerExperienceId sets CustomerExperienceId field to given value.

### HasCustomerExperienceId

`func (o *NfcOrderRequest) HasCustomerExperienceId() bool`

HasCustomerExperienceId returns a boolean if a field has been set.

### SetCustomerExperienceIdNil

`func (o *NfcOrderRequest) SetCustomerExperienceIdNil(b bool)`

 SetCustomerExperienceIdNil sets the value for CustomerExperienceId to be an explicit nil

### UnsetCustomerExperienceId
`func (o *NfcOrderRequest) UnsetCustomerExperienceId()`

UnsetCustomerExperienceId ensures that no value is present for CustomerExperienceId, not even an explicit nil
### GetUserId

`func (o *NfcOrderRequest) GetUserId() interface{}`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *NfcOrderRequest) GetUserIdOk() (*interface{}, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *NfcOrderRequest) SetUserId(v interface{})`

SetUserId sets UserId field to given value.

### HasUserId

`func (o *NfcOrderRequest) HasUserId() bool`

HasUserId returns a boolean if a field has been set.

### SetUserIdNil

`func (o *NfcOrderRequest) SetUserIdNil(b bool)`

 SetUserIdNil sets the value for UserId to be an explicit nil

### UnsetUserId
`func (o *NfcOrderRequest) UnsetUserId()`

UnsetUserId ensures that no value is present for UserId, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


