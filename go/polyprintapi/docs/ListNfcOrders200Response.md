# ListNfcOrders200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Address** | Pointer to [**ListNfcOrders200ResponseAddress**](ListNfcOrders200ResponseAddress.md) |  | [optional] 
**ChannelId** | Pointer to **interface{}** |  | [optional] 
**CustomerExperienceId** | Pointer to **interface{}** |  | [optional] 
**UserId** | Pointer to **interface{}** |  | [optional] 
**Entrypoint** | Pointer to **interface{}** |  | [optional] 

## Methods

### NewListNfcOrders200Response

`func NewListNfcOrders200Response() *ListNfcOrders200Response`

NewListNfcOrders200Response instantiates a new ListNfcOrders200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListNfcOrders200ResponseWithDefaults

`func NewListNfcOrders200ResponseWithDefaults() *ListNfcOrders200Response`

NewListNfcOrders200ResponseWithDefaults instantiates a new ListNfcOrders200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAddress

`func (o *ListNfcOrders200Response) GetAddress() ListNfcOrders200ResponseAddress`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *ListNfcOrders200Response) GetAddressOk() (*ListNfcOrders200ResponseAddress, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *ListNfcOrders200Response) SetAddress(v ListNfcOrders200ResponseAddress)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *ListNfcOrders200Response) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetChannelId

`func (o *ListNfcOrders200Response) GetChannelId() interface{}`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *ListNfcOrders200Response) GetChannelIdOk() (*interface{}, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *ListNfcOrders200Response) SetChannelId(v interface{})`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *ListNfcOrders200Response) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### SetChannelIdNil

`func (o *ListNfcOrders200Response) SetChannelIdNil(b bool)`

 SetChannelIdNil sets the value for ChannelId to be an explicit nil

### UnsetChannelId
`func (o *ListNfcOrders200Response) UnsetChannelId()`

UnsetChannelId ensures that no value is present for ChannelId, not even an explicit nil
### GetCustomerExperienceId

`func (o *ListNfcOrders200Response) GetCustomerExperienceId() interface{}`

GetCustomerExperienceId returns the CustomerExperienceId field if non-nil, zero value otherwise.

### GetCustomerExperienceIdOk

`func (o *ListNfcOrders200Response) GetCustomerExperienceIdOk() (*interface{}, bool)`

GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerExperienceId

`func (o *ListNfcOrders200Response) SetCustomerExperienceId(v interface{})`

SetCustomerExperienceId sets CustomerExperienceId field to given value.

### HasCustomerExperienceId

`func (o *ListNfcOrders200Response) HasCustomerExperienceId() bool`

HasCustomerExperienceId returns a boolean if a field has been set.

### SetCustomerExperienceIdNil

`func (o *ListNfcOrders200Response) SetCustomerExperienceIdNil(b bool)`

 SetCustomerExperienceIdNil sets the value for CustomerExperienceId to be an explicit nil

### UnsetCustomerExperienceId
`func (o *ListNfcOrders200Response) UnsetCustomerExperienceId()`

UnsetCustomerExperienceId ensures that no value is present for CustomerExperienceId, not even an explicit nil
### GetUserId

`func (o *ListNfcOrders200Response) GetUserId() interface{}`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *ListNfcOrders200Response) GetUserIdOk() (*interface{}, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *ListNfcOrders200Response) SetUserId(v interface{})`

SetUserId sets UserId field to given value.

### HasUserId

`func (o *ListNfcOrders200Response) HasUserId() bool`

HasUserId returns a boolean if a field has been set.

### SetUserIdNil

`func (o *ListNfcOrders200Response) SetUserIdNil(b bool)`

 SetUserIdNil sets the value for UserId to be an explicit nil

### UnsetUserId
`func (o *ListNfcOrders200Response) UnsetUserId()`

UnsetUserId ensures that no value is present for UserId, not even an explicit nil
### GetEntrypoint

`func (o *ListNfcOrders200Response) GetEntrypoint() interface{}`

GetEntrypoint returns the Entrypoint field if non-nil, zero value otherwise.

### GetEntrypointOk

`func (o *ListNfcOrders200Response) GetEntrypointOk() (*interface{}, bool)`

GetEntrypointOk returns a tuple with the Entrypoint field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEntrypoint

`func (o *ListNfcOrders200Response) SetEntrypoint(v interface{})`

SetEntrypoint sets Entrypoint field to given value.

### HasEntrypoint

`func (o *ListNfcOrders200Response) HasEntrypoint() bool`

HasEntrypoint returns a boolean if a field has been set.

### SetEntrypointNil

`func (o *ListNfcOrders200Response) SetEntrypointNil(b bool)`

 SetEntrypointNil sets the value for Entrypoint to be an explicit nil

### UnsetEntrypoint
`func (o *ListNfcOrders200Response) UnsetEntrypoint()`

UnsetEntrypoint ensures that no value is present for Entrypoint, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


