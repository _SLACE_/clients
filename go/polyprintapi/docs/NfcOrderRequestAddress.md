# NfcOrderRequestAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CompanyName** | Pointer to **interface{}** |  | [optional] 
**Recipient** | Pointer to **interface{}** | example: Johny Bobby | [optional] 
**StreetWithHouseNumber** | Pointer to **interface{}** |  | [optional] 
**Zip** | Pointer to **interface{}** |  | [optional] 
**City** | Pointer to **interface{}** |  | [optional] 

## Methods

### NewNfcOrderRequestAddress

`func NewNfcOrderRequestAddress() *NfcOrderRequestAddress`

NewNfcOrderRequestAddress instantiates a new NfcOrderRequestAddress object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNfcOrderRequestAddressWithDefaults

`func NewNfcOrderRequestAddressWithDefaults() *NfcOrderRequestAddress`

NewNfcOrderRequestAddressWithDefaults instantiates a new NfcOrderRequestAddress object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCompanyName

`func (o *NfcOrderRequestAddress) GetCompanyName() interface{}`

GetCompanyName returns the CompanyName field if non-nil, zero value otherwise.

### GetCompanyNameOk

`func (o *NfcOrderRequestAddress) GetCompanyNameOk() (*interface{}, bool)`

GetCompanyNameOk returns a tuple with the CompanyName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompanyName

`func (o *NfcOrderRequestAddress) SetCompanyName(v interface{})`

SetCompanyName sets CompanyName field to given value.

### HasCompanyName

`func (o *NfcOrderRequestAddress) HasCompanyName() bool`

HasCompanyName returns a boolean if a field has been set.

### SetCompanyNameNil

`func (o *NfcOrderRequestAddress) SetCompanyNameNil(b bool)`

 SetCompanyNameNil sets the value for CompanyName to be an explicit nil

### UnsetCompanyName
`func (o *NfcOrderRequestAddress) UnsetCompanyName()`

UnsetCompanyName ensures that no value is present for CompanyName, not even an explicit nil
### GetRecipient

`func (o *NfcOrderRequestAddress) GetRecipient() interface{}`

GetRecipient returns the Recipient field if non-nil, zero value otherwise.

### GetRecipientOk

`func (o *NfcOrderRequestAddress) GetRecipientOk() (*interface{}, bool)`

GetRecipientOk returns a tuple with the Recipient field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRecipient

`func (o *NfcOrderRequestAddress) SetRecipient(v interface{})`

SetRecipient sets Recipient field to given value.

### HasRecipient

`func (o *NfcOrderRequestAddress) HasRecipient() bool`

HasRecipient returns a boolean if a field has been set.

### SetRecipientNil

`func (o *NfcOrderRequestAddress) SetRecipientNil(b bool)`

 SetRecipientNil sets the value for Recipient to be an explicit nil

### UnsetRecipient
`func (o *NfcOrderRequestAddress) UnsetRecipient()`

UnsetRecipient ensures that no value is present for Recipient, not even an explicit nil
### GetStreetWithHouseNumber

`func (o *NfcOrderRequestAddress) GetStreetWithHouseNumber() interface{}`

GetStreetWithHouseNumber returns the StreetWithHouseNumber field if non-nil, zero value otherwise.

### GetStreetWithHouseNumberOk

`func (o *NfcOrderRequestAddress) GetStreetWithHouseNumberOk() (*interface{}, bool)`

GetStreetWithHouseNumberOk returns a tuple with the StreetWithHouseNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreetWithHouseNumber

`func (o *NfcOrderRequestAddress) SetStreetWithHouseNumber(v interface{})`

SetStreetWithHouseNumber sets StreetWithHouseNumber field to given value.

### HasStreetWithHouseNumber

`func (o *NfcOrderRequestAddress) HasStreetWithHouseNumber() bool`

HasStreetWithHouseNumber returns a boolean if a field has been set.

### SetStreetWithHouseNumberNil

`func (o *NfcOrderRequestAddress) SetStreetWithHouseNumberNil(b bool)`

 SetStreetWithHouseNumberNil sets the value for StreetWithHouseNumber to be an explicit nil

### UnsetStreetWithHouseNumber
`func (o *NfcOrderRequestAddress) UnsetStreetWithHouseNumber()`

UnsetStreetWithHouseNumber ensures that no value is present for StreetWithHouseNumber, not even an explicit nil
### GetZip

`func (o *NfcOrderRequestAddress) GetZip() interface{}`

GetZip returns the Zip field if non-nil, zero value otherwise.

### GetZipOk

`func (o *NfcOrderRequestAddress) GetZipOk() (*interface{}, bool)`

GetZipOk returns a tuple with the Zip field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetZip

`func (o *NfcOrderRequestAddress) SetZip(v interface{})`

SetZip sets Zip field to given value.

### HasZip

`func (o *NfcOrderRequestAddress) HasZip() bool`

HasZip returns a boolean if a field has been set.

### SetZipNil

`func (o *NfcOrderRequestAddress) SetZipNil(b bool)`

 SetZipNil sets the value for Zip to be an explicit nil

### UnsetZip
`func (o *NfcOrderRequestAddress) UnsetZip()`

UnsetZip ensures that no value is present for Zip, not even an explicit nil
### GetCity

`func (o *NfcOrderRequestAddress) GetCity() interface{}`

GetCity returns the City field if non-nil, zero value otherwise.

### GetCityOk

`func (o *NfcOrderRequestAddress) GetCityOk() (*interface{}, bool)`

GetCityOk returns a tuple with the City field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCity

`func (o *NfcOrderRequestAddress) SetCity(v interface{})`

SetCity sets City field to given value.

### HasCity

`func (o *NfcOrderRequestAddress) HasCity() bool`

HasCity returns a boolean if a field has been set.

### SetCityNil

`func (o *NfcOrderRequestAddress) SetCityNil(b bool)`

 SetCityNil sets the value for City to be an explicit nil

### UnsetCity
`func (o *NfcOrderRequestAddress) UnsetCity()`

UnsetCity ensures that no value is present for City, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


