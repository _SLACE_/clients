# \DefaultAPI

All URIs are relative to *https://polyprint.slace.dev/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreatePrintOrder**](DefaultAPI.md#CreatePrintOrder) | **Post** /integrations/{organization_id}/print-orders | Create print order
[**GetUserSSOLink**](DefaultAPI.md#GetUserSSOLink) | **Post** /integrations/{organization_id}/sso-link | Get user sso link
[**IntegrationsOrganizationIdPrintOrdersGet**](DefaultAPI.md#IntegrationsOrganizationIdPrintOrdersGet) | **Get** /integrations/{organization_id}/print-orders | Get list of print orders
[**ListNfcOrders**](DefaultAPI.md#ListNfcOrders) | **Get** /integrations/{organization_id}/nfc-orders | List NFC orders for users
[**NfcOrder**](DefaultAPI.md#NfcOrder) | **Post** /integrations/{organization_id}/nfc-order | NFC Order
[**NfcReorder**](DefaultAPI.md#NfcReorder) | **Post** /integrations/{organization_id}/nfc-reorder | Reorder NFC



## CreatePrintOrder

> CreatePrintOrder200Response CreatePrintOrder(ctx, organizationId).CreatePrintOrderRequest(createPrintOrderRequest).Execute()

Create print order

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/polyprintapi"
)

func main() {
    organizationId := TODO // interface{} | 
    createPrintOrderRequest := *openapiclient.NewCreatePrintOrderRequest(interface{}(123), interface{}(123), *openapiclient.NewCreatePrintOrderRequestPersonalizationData(), interface{}(123), interface{}(123), *openapiclient.NewCreatePrintOrderRequestProductionData(interface{}(123), interface{}(123), interface{}(123), interface{}(123), *openapiclient.NewPolyprintContact(), *openapiclient.NewPolyprintContact(), interface{}(123))) // CreatePrintOrderRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultAPI.CreatePrintOrder(context.Background(), organizationId).CreatePrintOrderRequest(createPrintOrderRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.CreatePrintOrder``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreatePrintOrder`: CreatePrintOrder200Response
    fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.CreatePrintOrder`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | [**interface{}**](.md) |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreatePrintOrderRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **createPrintOrderRequest** | [**CreatePrintOrderRequest**](CreatePrintOrderRequest.md) |  | 

### Return type

[**CreatePrintOrder200Response**](CreatePrintOrder200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetUserSSOLink

> GetUserSSOLink200Response GetUserSSOLink(ctx, organizationId).Execute()

Get user sso link

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/polyprintapi"
)

func main() {
    organizationId := TODO // interface{} | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultAPI.GetUserSSOLink(context.Background(), organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.GetUserSSOLink``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetUserSSOLink`: GetUserSSOLink200Response
    fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.GetUserSSOLink`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | [**interface{}**](.md) |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetUserSSOLinkRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**GetUserSSOLink200Response**](GetUserSSOLink200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## IntegrationsOrganizationIdPrintOrdersGet

> interface{} IntegrationsOrganizationIdPrintOrdersGet(ctx, organizationId).UserIds(userIds).ChannelId(channelId).Execute()

Get list of print orders

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/polyprintapi"
)

func main() {
    organizationId := TODO // interface{} | 
    userIds := TODO // interface{} |  (optional)
    channelId := TODO // interface{} |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultAPI.IntegrationsOrganizationIdPrintOrdersGet(context.Background(), organizationId).UserIds(userIds).ChannelId(channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.IntegrationsOrganizationIdPrintOrdersGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `IntegrationsOrganizationIdPrintOrdersGet`: interface{}
    fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.IntegrationsOrganizationIdPrintOrdersGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | [**interface{}**](.md) |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiIntegrationsOrganizationIdPrintOrdersGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **userIds** | [**interface{}**](interface{}.md) |  | 
 **channelId** | [**interface{}**](interface{}.md) |  | 

### Return type

**interface{}**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListNfcOrders

> ListNfcOrders200Response ListNfcOrders(ctx, organizationId).UserIds(userIds).Execute()

List NFC orders for users

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/polyprintapi"
)

func main() {
    organizationId := TODO // interface{} | 
    userIds := TODO // interface{} |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultAPI.ListNfcOrders(context.Background(), organizationId).UserIds(userIds).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.ListNfcOrders``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListNfcOrders`: ListNfcOrders200Response
    fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.ListNfcOrders`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | [**interface{}**](.md) |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListNfcOrdersRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **userIds** | [**interface{}**](interface{}.md) |  | 

### Return type

[**ListNfcOrders200Response**](ListNfcOrders200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## NfcOrder

> NfcOrder200Response NfcOrder(ctx, organizationId).NfcOrderRequest(nfcOrderRequest).Execute()

NFC Order

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/polyprintapi"
)

func main() {
    organizationId := TODO // interface{} | 
    nfcOrderRequest := *openapiclient.NewNfcOrderRequest() // NfcOrderRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultAPI.NfcOrder(context.Background(), organizationId).NfcOrderRequest(nfcOrderRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.NfcOrder``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `NfcOrder`: NfcOrder200Response
    fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.NfcOrder`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | [**interface{}**](.md) |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiNfcOrderRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **nfcOrderRequest** | [**NfcOrderRequest**](NfcOrderRequest.md) |  | 

### Return type

[**NfcOrder200Response**](NfcOrder200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## NfcReorder

> NfcReorder200Response NfcReorder(ctx, organizationId).NfcReorderRequest(nfcReorderRequest).Execute()

Reorder NFC



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/polyprintapi"
)

func main() {
    organizationId := TODO // interface{} | 
    nfcReorderRequest := *openapiclient.NewNfcReorderRequest() // NfcReorderRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultAPI.NfcReorder(context.Background(), organizationId).NfcReorderRequest(nfcReorderRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.NfcReorder``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `NfcReorder`: NfcReorder200Response
    fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.NfcReorder`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | [**interface{}**](.md) |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiNfcReorderRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **nfcReorderRequest** | [**NfcReorderRequest**](NfcReorderRequest.md) |  | 

### Return type

[**NfcReorder200Response**](NfcReorder200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

