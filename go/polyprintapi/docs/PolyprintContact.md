# PolyprintContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EmailAddress** | Pointer to **interface{}** |  | [optional] 
**Phone** | Pointer to **interface{}** |  | [optional] 
**Firstname** | Pointer to **interface{}** |  | [optional] 
**Lastname** | Pointer to **interface{}** |  | [optional] 
**Company** | Pointer to **interface{}** |  | [optional] 
**Gender** | Pointer to **interface{}** |  | [optional] 
**AcademicTitle** | Pointer to **interface{}** |  | [optional] 
**CountryId** | Pointer to **interface{}** |  | [optional] 
**Address** | Pointer to [**PolyprintContactAddress**](PolyprintContactAddress.md) |  | [optional] 

## Methods

### NewPolyprintContact

`func NewPolyprintContact() *PolyprintContact`

NewPolyprintContact instantiates a new PolyprintContact object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPolyprintContactWithDefaults

`func NewPolyprintContactWithDefaults() *PolyprintContact`

NewPolyprintContactWithDefaults instantiates a new PolyprintContact object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetEmailAddress

`func (o *PolyprintContact) GetEmailAddress() interface{}`

GetEmailAddress returns the EmailAddress field if non-nil, zero value otherwise.

### GetEmailAddressOk

`func (o *PolyprintContact) GetEmailAddressOk() (*interface{}, bool)`

GetEmailAddressOk returns a tuple with the EmailAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmailAddress

`func (o *PolyprintContact) SetEmailAddress(v interface{})`

SetEmailAddress sets EmailAddress field to given value.

### HasEmailAddress

`func (o *PolyprintContact) HasEmailAddress() bool`

HasEmailAddress returns a boolean if a field has been set.

### SetEmailAddressNil

`func (o *PolyprintContact) SetEmailAddressNil(b bool)`

 SetEmailAddressNil sets the value for EmailAddress to be an explicit nil

### UnsetEmailAddress
`func (o *PolyprintContact) UnsetEmailAddress()`

UnsetEmailAddress ensures that no value is present for EmailAddress, not even an explicit nil
### GetPhone

`func (o *PolyprintContact) GetPhone() interface{}`

GetPhone returns the Phone field if non-nil, zero value otherwise.

### GetPhoneOk

`func (o *PolyprintContact) GetPhoneOk() (*interface{}, bool)`

GetPhoneOk returns a tuple with the Phone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhone

`func (o *PolyprintContact) SetPhone(v interface{})`

SetPhone sets Phone field to given value.

### HasPhone

`func (o *PolyprintContact) HasPhone() bool`

HasPhone returns a boolean if a field has been set.

### SetPhoneNil

`func (o *PolyprintContact) SetPhoneNil(b bool)`

 SetPhoneNil sets the value for Phone to be an explicit nil

### UnsetPhone
`func (o *PolyprintContact) UnsetPhone()`

UnsetPhone ensures that no value is present for Phone, not even an explicit nil
### GetFirstname

`func (o *PolyprintContact) GetFirstname() interface{}`

GetFirstname returns the Firstname field if non-nil, zero value otherwise.

### GetFirstnameOk

`func (o *PolyprintContact) GetFirstnameOk() (*interface{}, bool)`

GetFirstnameOk returns a tuple with the Firstname field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstname

`func (o *PolyprintContact) SetFirstname(v interface{})`

SetFirstname sets Firstname field to given value.

### HasFirstname

`func (o *PolyprintContact) HasFirstname() bool`

HasFirstname returns a boolean if a field has been set.

### SetFirstnameNil

`func (o *PolyprintContact) SetFirstnameNil(b bool)`

 SetFirstnameNil sets the value for Firstname to be an explicit nil

### UnsetFirstname
`func (o *PolyprintContact) UnsetFirstname()`

UnsetFirstname ensures that no value is present for Firstname, not even an explicit nil
### GetLastname

`func (o *PolyprintContact) GetLastname() interface{}`

GetLastname returns the Lastname field if non-nil, zero value otherwise.

### GetLastnameOk

`func (o *PolyprintContact) GetLastnameOk() (*interface{}, bool)`

GetLastnameOk returns a tuple with the Lastname field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastname

`func (o *PolyprintContact) SetLastname(v interface{})`

SetLastname sets Lastname field to given value.

### HasLastname

`func (o *PolyprintContact) HasLastname() bool`

HasLastname returns a boolean if a field has been set.

### SetLastnameNil

`func (o *PolyprintContact) SetLastnameNil(b bool)`

 SetLastnameNil sets the value for Lastname to be an explicit nil

### UnsetLastname
`func (o *PolyprintContact) UnsetLastname()`

UnsetLastname ensures that no value is present for Lastname, not even an explicit nil
### GetCompany

`func (o *PolyprintContact) GetCompany() interface{}`

GetCompany returns the Company field if non-nil, zero value otherwise.

### GetCompanyOk

`func (o *PolyprintContact) GetCompanyOk() (*interface{}, bool)`

GetCompanyOk returns a tuple with the Company field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompany

`func (o *PolyprintContact) SetCompany(v interface{})`

SetCompany sets Company field to given value.

### HasCompany

`func (o *PolyprintContact) HasCompany() bool`

HasCompany returns a boolean if a field has been set.

### SetCompanyNil

`func (o *PolyprintContact) SetCompanyNil(b bool)`

 SetCompanyNil sets the value for Company to be an explicit nil

### UnsetCompany
`func (o *PolyprintContact) UnsetCompany()`

UnsetCompany ensures that no value is present for Company, not even an explicit nil
### GetGender

`func (o *PolyprintContact) GetGender() interface{}`

GetGender returns the Gender field if non-nil, zero value otherwise.

### GetGenderOk

`func (o *PolyprintContact) GetGenderOk() (*interface{}, bool)`

GetGenderOk returns a tuple with the Gender field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGender

`func (o *PolyprintContact) SetGender(v interface{})`

SetGender sets Gender field to given value.

### HasGender

`func (o *PolyprintContact) HasGender() bool`

HasGender returns a boolean if a field has been set.

### SetGenderNil

`func (o *PolyprintContact) SetGenderNil(b bool)`

 SetGenderNil sets the value for Gender to be an explicit nil

### UnsetGender
`func (o *PolyprintContact) UnsetGender()`

UnsetGender ensures that no value is present for Gender, not even an explicit nil
### GetAcademicTitle

`func (o *PolyprintContact) GetAcademicTitle() interface{}`

GetAcademicTitle returns the AcademicTitle field if non-nil, zero value otherwise.

### GetAcademicTitleOk

`func (o *PolyprintContact) GetAcademicTitleOk() (*interface{}, bool)`

GetAcademicTitleOk returns a tuple with the AcademicTitle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAcademicTitle

`func (o *PolyprintContact) SetAcademicTitle(v interface{})`

SetAcademicTitle sets AcademicTitle field to given value.

### HasAcademicTitle

`func (o *PolyprintContact) HasAcademicTitle() bool`

HasAcademicTitle returns a boolean if a field has been set.

### SetAcademicTitleNil

`func (o *PolyprintContact) SetAcademicTitleNil(b bool)`

 SetAcademicTitleNil sets the value for AcademicTitle to be an explicit nil

### UnsetAcademicTitle
`func (o *PolyprintContact) UnsetAcademicTitle()`

UnsetAcademicTitle ensures that no value is present for AcademicTitle, not even an explicit nil
### GetCountryId

`func (o *PolyprintContact) GetCountryId() interface{}`

GetCountryId returns the CountryId field if non-nil, zero value otherwise.

### GetCountryIdOk

`func (o *PolyprintContact) GetCountryIdOk() (*interface{}, bool)`

GetCountryIdOk returns a tuple with the CountryId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountryId

`func (o *PolyprintContact) SetCountryId(v interface{})`

SetCountryId sets CountryId field to given value.

### HasCountryId

`func (o *PolyprintContact) HasCountryId() bool`

HasCountryId returns a boolean if a field has been set.

### SetCountryIdNil

`func (o *PolyprintContact) SetCountryIdNil(b bool)`

 SetCountryIdNil sets the value for CountryId to be an explicit nil

### UnsetCountryId
`func (o *PolyprintContact) UnsetCountryId()`

UnsetCountryId ensures that no value is present for CountryId, not even an explicit nil
### GetAddress

`func (o *PolyprintContact) GetAddress() PolyprintContactAddress`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *PolyprintContact) GetAddressOk() (*PolyprintContactAddress, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *PolyprintContact) SetAddress(v PolyprintContactAddress)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *PolyprintContact) HasAddress() bool`

HasAddress returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


