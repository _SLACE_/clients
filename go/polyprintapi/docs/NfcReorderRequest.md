# NfcReorderRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Address** | Pointer to [**NfcReorderRequestAddress**](NfcReorderRequestAddress.md) |  | [optional] 
**ChannelId** | Pointer to **interface{}** |  | [optional] 
**CustomerExperienceId** | Pointer to **interface{}** |  | [optional] 
**UserId** | Pointer to **interface{}** |  | [optional] 

## Methods

### NewNfcReorderRequest

`func NewNfcReorderRequest() *NfcReorderRequest`

NewNfcReorderRequest instantiates a new NfcReorderRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNfcReorderRequestWithDefaults

`func NewNfcReorderRequestWithDefaults() *NfcReorderRequest`

NewNfcReorderRequestWithDefaults instantiates a new NfcReorderRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAddress

`func (o *NfcReorderRequest) GetAddress() NfcReorderRequestAddress`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *NfcReorderRequest) GetAddressOk() (*NfcReorderRequestAddress, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *NfcReorderRequest) SetAddress(v NfcReorderRequestAddress)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *NfcReorderRequest) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetChannelId

`func (o *NfcReorderRequest) GetChannelId() interface{}`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *NfcReorderRequest) GetChannelIdOk() (*interface{}, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *NfcReorderRequest) SetChannelId(v interface{})`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *NfcReorderRequest) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.

### SetChannelIdNil

`func (o *NfcReorderRequest) SetChannelIdNil(b bool)`

 SetChannelIdNil sets the value for ChannelId to be an explicit nil

### UnsetChannelId
`func (o *NfcReorderRequest) UnsetChannelId()`

UnsetChannelId ensures that no value is present for ChannelId, not even an explicit nil
### GetCustomerExperienceId

`func (o *NfcReorderRequest) GetCustomerExperienceId() interface{}`

GetCustomerExperienceId returns the CustomerExperienceId field if non-nil, zero value otherwise.

### GetCustomerExperienceIdOk

`func (o *NfcReorderRequest) GetCustomerExperienceIdOk() (*interface{}, bool)`

GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerExperienceId

`func (o *NfcReorderRequest) SetCustomerExperienceId(v interface{})`

SetCustomerExperienceId sets CustomerExperienceId field to given value.

### HasCustomerExperienceId

`func (o *NfcReorderRequest) HasCustomerExperienceId() bool`

HasCustomerExperienceId returns a boolean if a field has been set.

### SetCustomerExperienceIdNil

`func (o *NfcReorderRequest) SetCustomerExperienceIdNil(b bool)`

 SetCustomerExperienceIdNil sets the value for CustomerExperienceId to be an explicit nil

### UnsetCustomerExperienceId
`func (o *NfcReorderRequest) UnsetCustomerExperienceId()`

UnsetCustomerExperienceId ensures that no value is present for CustomerExperienceId, not even an explicit nil
### GetUserId

`func (o *NfcReorderRequest) GetUserId() interface{}`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *NfcReorderRequest) GetUserIdOk() (*interface{}, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *NfcReorderRequest) SetUserId(v interface{})`

SetUserId sets UserId field to given value.

### HasUserId

`func (o *NfcReorderRequest) HasUserId() bool`

HasUserId returns a boolean if a field has been set.

### SetUserIdNil

`func (o *NfcReorderRequest) SetUserIdNil(b bool)`

 SetUserIdNil sets the value for UserId to be an explicit nil

### UnsetUserId
`func (o *NfcReorderRequest) UnsetUserId()`

UnsetUserId ensures that no value is present for UserId, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


