# PolyprintContactAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CountryId** | Pointer to **interface{}** |  | [optional] 
**Street** | Pointer to **interface{}** |  | [optional] 
**ZipCode** | Pointer to **interface{}** |  | [optional] 
**City** | Pointer to **interface{}** |  | [optional] 
**State** | Pointer to **interface{}** |  | [optional] 
**HouseNo** | Pointer to **interface{}** |  | [optional] 

## Methods

### NewPolyprintContactAddress

`func NewPolyprintContactAddress() *PolyprintContactAddress`

NewPolyprintContactAddress instantiates a new PolyprintContactAddress object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPolyprintContactAddressWithDefaults

`func NewPolyprintContactAddressWithDefaults() *PolyprintContactAddress`

NewPolyprintContactAddressWithDefaults instantiates a new PolyprintContactAddress object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCountryId

`func (o *PolyprintContactAddress) GetCountryId() interface{}`

GetCountryId returns the CountryId field if non-nil, zero value otherwise.

### GetCountryIdOk

`func (o *PolyprintContactAddress) GetCountryIdOk() (*interface{}, bool)`

GetCountryIdOk returns a tuple with the CountryId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountryId

`func (o *PolyprintContactAddress) SetCountryId(v interface{})`

SetCountryId sets CountryId field to given value.

### HasCountryId

`func (o *PolyprintContactAddress) HasCountryId() bool`

HasCountryId returns a boolean if a field has been set.

### SetCountryIdNil

`func (o *PolyprintContactAddress) SetCountryIdNil(b bool)`

 SetCountryIdNil sets the value for CountryId to be an explicit nil

### UnsetCountryId
`func (o *PolyprintContactAddress) UnsetCountryId()`

UnsetCountryId ensures that no value is present for CountryId, not even an explicit nil
### GetStreet

`func (o *PolyprintContactAddress) GetStreet() interface{}`

GetStreet returns the Street field if non-nil, zero value otherwise.

### GetStreetOk

`func (o *PolyprintContactAddress) GetStreetOk() (*interface{}, bool)`

GetStreetOk returns a tuple with the Street field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreet

`func (o *PolyprintContactAddress) SetStreet(v interface{})`

SetStreet sets Street field to given value.

### HasStreet

`func (o *PolyprintContactAddress) HasStreet() bool`

HasStreet returns a boolean if a field has been set.

### SetStreetNil

`func (o *PolyprintContactAddress) SetStreetNil(b bool)`

 SetStreetNil sets the value for Street to be an explicit nil

### UnsetStreet
`func (o *PolyprintContactAddress) UnsetStreet()`

UnsetStreet ensures that no value is present for Street, not even an explicit nil
### GetZipCode

`func (o *PolyprintContactAddress) GetZipCode() interface{}`

GetZipCode returns the ZipCode field if non-nil, zero value otherwise.

### GetZipCodeOk

`func (o *PolyprintContactAddress) GetZipCodeOk() (*interface{}, bool)`

GetZipCodeOk returns a tuple with the ZipCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetZipCode

`func (o *PolyprintContactAddress) SetZipCode(v interface{})`

SetZipCode sets ZipCode field to given value.

### HasZipCode

`func (o *PolyprintContactAddress) HasZipCode() bool`

HasZipCode returns a boolean if a field has been set.

### SetZipCodeNil

`func (o *PolyprintContactAddress) SetZipCodeNil(b bool)`

 SetZipCodeNil sets the value for ZipCode to be an explicit nil

### UnsetZipCode
`func (o *PolyprintContactAddress) UnsetZipCode()`

UnsetZipCode ensures that no value is present for ZipCode, not even an explicit nil
### GetCity

`func (o *PolyprintContactAddress) GetCity() interface{}`

GetCity returns the City field if non-nil, zero value otherwise.

### GetCityOk

`func (o *PolyprintContactAddress) GetCityOk() (*interface{}, bool)`

GetCityOk returns a tuple with the City field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCity

`func (o *PolyprintContactAddress) SetCity(v interface{})`

SetCity sets City field to given value.

### HasCity

`func (o *PolyprintContactAddress) HasCity() bool`

HasCity returns a boolean if a field has been set.

### SetCityNil

`func (o *PolyprintContactAddress) SetCityNil(b bool)`

 SetCityNil sets the value for City to be an explicit nil

### UnsetCity
`func (o *PolyprintContactAddress) UnsetCity()`

UnsetCity ensures that no value is present for City, not even an explicit nil
### GetState

`func (o *PolyprintContactAddress) GetState() interface{}`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *PolyprintContactAddress) GetStateOk() (*interface{}, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *PolyprintContactAddress) SetState(v interface{})`

SetState sets State field to given value.

### HasState

`func (o *PolyprintContactAddress) HasState() bool`

HasState returns a boolean if a field has been set.

### SetStateNil

`func (o *PolyprintContactAddress) SetStateNil(b bool)`

 SetStateNil sets the value for State to be an explicit nil

### UnsetState
`func (o *PolyprintContactAddress) UnsetState()`

UnsetState ensures that no value is present for State, not even an explicit nil
### GetHouseNo

`func (o *PolyprintContactAddress) GetHouseNo() interface{}`

GetHouseNo returns the HouseNo field if non-nil, zero value otherwise.

### GetHouseNoOk

`func (o *PolyprintContactAddress) GetHouseNoOk() (*interface{}, bool)`

GetHouseNoOk returns a tuple with the HouseNo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHouseNo

`func (o *PolyprintContactAddress) SetHouseNo(v interface{})`

SetHouseNo sets HouseNo field to given value.

### HasHouseNo

`func (o *PolyprintContactAddress) HasHouseNo() bool`

HasHouseNo returns a boolean if a field has been set.

### SetHouseNoNil

`func (o *PolyprintContactAddress) SetHouseNoNil(b bool)`

 SetHouseNoNil sets the value for HouseNo to be an explicit nil

### UnsetHouseNo
`func (o *PolyprintContactAddress) UnsetHouseNo()`

UnsetHouseNo ensures that no value is present for HouseNo, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


