# ListNfcOrders200ResponseAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CompanyName** | Pointer to **interface{}** |  | [optional] 
**Recipient** | Pointer to **interface{}** | example: Johny Bobby | [optional] 
**StreetWithHouseNumber** | Pointer to **interface{}** |  | [optional] 
**Zip** | Pointer to **interface{}** |  | [optional] 
**City** | Pointer to **interface{}** |  | [optional] 

## Methods

### NewListNfcOrders200ResponseAddress

`func NewListNfcOrders200ResponseAddress() *ListNfcOrders200ResponseAddress`

NewListNfcOrders200ResponseAddress instantiates a new ListNfcOrders200ResponseAddress object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListNfcOrders200ResponseAddressWithDefaults

`func NewListNfcOrders200ResponseAddressWithDefaults() *ListNfcOrders200ResponseAddress`

NewListNfcOrders200ResponseAddressWithDefaults instantiates a new ListNfcOrders200ResponseAddress object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCompanyName

`func (o *ListNfcOrders200ResponseAddress) GetCompanyName() interface{}`

GetCompanyName returns the CompanyName field if non-nil, zero value otherwise.

### GetCompanyNameOk

`func (o *ListNfcOrders200ResponseAddress) GetCompanyNameOk() (*interface{}, bool)`

GetCompanyNameOk returns a tuple with the CompanyName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompanyName

`func (o *ListNfcOrders200ResponseAddress) SetCompanyName(v interface{})`

SetCompanyName sets CompanyName field to given value.

### HasCompanyName

`func (o *ListNfcOrders200ResponseAddress) HasCompanyName() bool`

HasCompanyName returns a boolean if a field has been set.

### SetCompanyNameNil

`func (o *ListNfcOrders200ResponseAddress) SetCompanyNameNil(b bool)`

 SetCompanyNameNil sets the value for CompanyName to be an explicit nil

### UnsetCompanyName
`func (o *ListNfcOrders200ResponseAddress) UnsetCompanyName()`

UnsetCompanyName ensures that no value is present for CompanyName, not even an explicit nil
### GetRecipient

`func (o *ListNfcOrders200ResponseAddress) GetRecipient() interface{}`

GetRecipient returns the Recipient field if non-nil, zero value otherwise.

### GetRecipientOk

`func (o *ListNfcOrders200ResponseAddress) GetRecipientOk() (*interface{}, bool)`

GetRecipientOk returns a tuple with the Recipient field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRecipient

`func (o *ListNfcOrders200ResponseAddress) SetRecipient(v interface{})`

SetRecipient sets Recipient field to given value.

### HasRecipient

`func (o *ListNfcOrders200ResponseAddress) HasRecipient() bool`

HasRecipient returns a boolean if a field has been set.

### SetRecipientNil

`func (o *ListNfcOrders200ResponseAddress) SetRecipientNil(b bool)`

 SetRecipientNil sets the value for Recipient to be an explicit nil

### UnsetRecipient
`func (o *ListNfcOrders200ResponseAddress) UnsetRecipient()`

UnsetRecipient ensures that no value is present for Recipient, not even an explicit nil
### GetStreetWithHouseNumber

`func (o *ListNfcOrders200ResponseAddress) GetStreetWithHouseNumber() interface{}`

GetStreetWithHouseNumber returns the StreetWithHouseNumber field if non-nil, zero value otherwise.

### GetStreetWithHouseNumberOk

`func (o *ListNfcOrders200ResponseAddress) GetStreetWithHouseNumberOk() (*interface{}, bool)`

GetStreetWithHouseNumberOk returns a tuple with the StreetWithHouseNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreetWithHouseNumber

`func (o *ListNfcOrders200ResponseAddress) SetStreetWithHouseNumber(v interface{})`

SetStreetWithHouseNumber sets StreetWithHouseNumber field to given value.

### HasStreetWithHouseNumber

`func (o *ListNfcOrders200ResponseAddress) HasStreetWithHouseNumber() bool`

HasStreetWithHouseNumber returns a boolean if a field has been set.

### SetStreetWithHouseNumberNil

`func (o *ListNfcOrders200ResponseAddress) SetStreetWithHouseNumberNil(b bool)`

 SetStreetWithHouseNumberNil sets the value for StreetWithHouseNumber to be an explicit nil

### UnsetStreetWithHouseNumber
`func (o *ListNfcOrders200ResponseAddress) UnsetStreetWithHouseNumber()`

UnsetStreetWithHouseNumber ensures that no value is present for StreetWithHouseNumber, not even an explicit nil
### GetZip

`func (o *ListNfcOrders200ResponseAddress) GetZip() interface{}`

GetZip returns the Zip field if non-nil, zero value otherwise.

### GetZipOk

`func (o *ListNfcOrders200ResponseAddress) GetZipOk() (*interface{}, bool)`

GetZipOk returns a tuple with the Zip field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetZip

`func (o *ListNfcOrders200ResponseAddress) SetZip(v interface{})`

SetZip sets Zip field to given value.

### HasZip

`func (o *ListNfcOrders200ResponseAddress) HasZip() bool`

HasZip returns a boolean if a field has been set.

### SetZipNil

`func (o *ListNfcOrders200ResponseAddress) SetZipNil(b bool)`

 SetZipNil sets the value for Zip to be an explicit nil

### UnsetZip
`func (o *ListNfcOrders200ResponseAddress) UnsetZip()`

UnsetZip ensures that no value is present for Zip, not even an explicit nil
### GetCity

`func (o *ListNfcOrders200ResponseAddress) GetCity() interface{}`

GetCity returns the City field if non-nil, zero value otherwise.

### GetCityOk

`func (o *ListNfcOrders200ResponseAddress) GetCityOk() (*interface{}, bool)`

GetCityOk returns a tuple with the City field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCity

`func (o *ListNfcOrders200ResponseAddress) SetCity(v interface{})`

SetCity sets City field to given value.

### HasCity

`func (o *ListNfcOrders200ResponseAddress) HasCity() bool`

HasCity returns a boolean if a field has been set.

### SetCityNil

`func (o *ListNfcOrders200ResponseAddress) SetCityNil(b bool)`

 SetCityNil sets the value for City to be an explicit nil

### UnsetCity
`func (o *ListNfcOrders200ResponseAddress) UnsetCity()`

UnsetCity ensures that no value is present for City, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


