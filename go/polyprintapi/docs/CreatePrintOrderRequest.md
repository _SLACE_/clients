# CreatePrintOrderRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DocumentId** | **interface{}** |  | 
**PrintType** | **interface{}** |  | 
**PersonalizationData** | [**CreatePrintOrderRequestPersonalizationData**](CreatePrintOrderRequestPersonalizationData.md) |  | 
**ChannelId** | **interface{}** |  | 
**TransactionId** | **interface{}** |  | 
**ProductionData** | [**CreatePrintOrderRequestProductionData**](CreatePrintOrderRequestProductionData.md) |  | 
**UserId** | Pointer to **interface{}** |  | [optional] 
**CustomerExperienceId** | Pointer to **interface{}** |  | [optional] 

## Methods

### NewCreatePrintOrderRequest

`func NewCreatePrintOrderRequest(documentId interface{}, printType interface{}, personalizationData CreatePrintOrderRequestPersonalizationData, channelId interface{}, transactionId interface{}, productionData CreatePrintOrderRequestProductionData, ) *CreatePrintOrderRequest`

NewCreatePrintOrderRequest instantiates a new CreatePrintOrderRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreatePrintOrderRequestWithDefaults

`func NewCreatePrintOrderRequestWithDefaults() *CreatePrintOrderRequest`

NewCreatePrintOrderRequestWithDefaults instantiates a new CreatePrintOrderRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDocumentId

`func (o *CreatePrintOrderRequest) GetDocumentId() interface{}`

GetDocumentId returns the DocumentId field if non-nil, zero value otherwise.

### GetDocumentIdOk

`func (o *CreatePrintOrderRequest) GetDocumentIdOk() (*interface{}, bool)`

GetDocumentIdOk returns a tuple with the DocumentId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDocumentId

`func (o *CreatePrintOrderRequest) SetDocumentId(v interface{})`

SetDocumentId sets DocumentId field to given value.


### SetDocumentIdNil

`func (o *CreatePrintOrderRequest) SetDocumentIdNil(b bool)`

 SetDocumentIdNil sets the value for DocumentId to be an explicit nil

### UnsetDocumentId
`func (o *CreatePrintOrderRequest) UnsetDocumentId()`

UnsetDocumentId ensures that no value is present for DocumentId, not even an explicit nil
### GetPrintType

`func (o *CreatePrintOrderRequest) GetPrintType() interface{}`

GetPrintType returns the PrintType field if non-nil, zero value otherwise.

### GetPrintTypeOk

`func (o *CreatePrintOrderRequest) GetPrintTypeOk() (*interface{}, bool)`

GetPrintTypeOk returns a tuple with the PrintType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrintType

`func (o *CreatePrintOrderRequest) SetPrintType(v interface{})`

SetPrintType sets PrintType field to given value.


### SetPrintTypeNil

`func (o *CreatePrintOrderRequest) SetPrintTypeNil(b bool)`

 SetPrintTypeNil sets the value for PrintType to be an explicit nil

### UnsetPrintType
`func (o *CreatePrintOrderRequest) UnsetPrintType()`

UnsetPrintType ensures that no value is present for PrintType, not even an explicit nil
### GetPersonalizationData

`func (o *CreatePrintOrderRequest) GetPersonalizationData() CreatePrintOrderRequestPersonalizationData`

GetPersonalizationData returns the PersonalizationData field if non-nil, zero value otherwise.

### GetPersonalizationDataOk

`func (o *CreatePrintOrderRequest) GetPersonalizationDataOk() (*CreatePrintOrderRequestPersonalizationData, bool)`

GetPersonalizationDataOk returns a tuple with the PersonalizationData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPersonalizationData

`func (o *CreatePrintOrderRequest) SetPersonalizationData(v CreatePrintOrderRequestPersonalizationData)`

SetPersonalizationData sets PersonalizationData field to given value.


### GetChannelId

`func (o *CreatePrintOrderRequest) GetChannelId() interface{}`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CreatePrintOrderRequest) GetChannelIdOk() (*interface{}, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CreatePrintOrderRequest) SetChannelId(v interface{})`

SetChannelId sets ChannelId field to given value.


### SetChannelIdNil

`func (o *CreatePrintOrderRequest) SetChannelIdNil(b bool)`

 SetChannelIdNil sets the value for ChannelId to be an explicit nil

### UnsetChannelId
`func (o *CreatePrintOrderRequest) UnsetChannelId()`

UnsetChannelId ensures that no value is present for ChannelId, not even an explicit nil
### GetTransactionId

`func (o *CreatePrintOrderRequest) GetTransactionId() interface{}`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *CreatePrintOrderRequest) GetTransactionIdOk() (*interface{}, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *CreatePrintOrderRequest) SetTransactionId(v interface{})`

SetTransactionId sets TransactionId field to given value.


### SetTransactionIdNil

`func (o *CreatePrintOrderRequest) SetTransactionIdNil(b bool)`

 SetTransactionIdNil sets the value for TransactionId to be an explicit nil

### UnsetTransactionId
`func (o *CreatePrintOrderRequest) UnsetTransactionId()`

UnsetTransactionId ensures that no value is present for TransactionId, not even an explicit nil
### GetProductionData

`func (o *CreatePrintOrderRequest) GetProductionData() CreatePrintOrderRequestProductionData`

GetProductionData returns the ProductionData field if non-nil, zero value otherwise.

### GetProductionDataOk

`func (o *CreatePrintOrderRequest) GetProductionDataOk() (*CreatePrintOrderRequestProductionData, bool)`

GetProductionDataOk returns a tuple with the ProductionData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProductionData

`func (o *CreatePrintOrderRequest) SetProductionData(v CreatePrintOrderRequestProductionData)`

SetProductionData sets ProductionData field to given value.


### GetUserId

`func (o *CreatePrintOrderRequest) GetUserId() interface{}`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *CreatePrintOrderRequest) GetUserIdOk() (*interface{}, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *CreatePrintOrderRequest) SetUserId(v interface{})`

SetUserId sets UserId field to given value.

### HasUserId

`func (o *CreatePrintOrderRequest) HasUserId() bool`

HasUserId returns a boolean if a field has been set.

### SetUserIdNil

`func (o *CreatePrintOrderRequest) SetUserIdNil(b bool)`

 SetUserIdNil sets the value for UserId to be an explicit nil

### UnsetUserId
`func (o *CreatePrintOrderRequest) UnsetUserId()`

UnsetUserId ensures that no value is present for UserId, not even an explicit nil
### GetCustomerExperienceId

`func (o *CreatePrintOrderRequest) GetCustomerExperienceId() interface{}`

GetCustomerExperienceId returns the CustomerExperienceId field if non-nil, zero value otherwise.

### GetCustomerExperienceIdOk

`func (o *CreatePrintOrderRequest) GetCustomerExperienceIdOk() (*interface{}, bool)`

GetCustomerExperienceIdOk returns a tuple with the CustomerExperienceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustomerExperienceId

`func (o *CreatePrintOrderRequest) SetCustomerExperienceId(v interface{})`

SetCustomerExperienceId sets CustomerExperienceId field to given value.

### HasCustomerExperienceId

`func (o *CreatePrintOrderRequest) HasCustomerExperienceId() bool`

HasCustomerExperienceId returns a boolean if a field has been set.

### SetCustomerExperienceIdNil

`func (o *CreatePrintOrderRequest) SetCustomerExperienceIdNil(b bool)`

 SetCustomerExperienceIdNil sets the value for CustomerExperienceId to be an explicit nil

### UnsetCustomerExperienceId
`func (o *CreatePrintOrderRequest) UnsetCustomerExperienceId()`

UnsetCustomerExperienceId ensures that no value is present for CustomerExperienceId, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


