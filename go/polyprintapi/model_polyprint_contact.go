/*
polyprint.v1

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package polyprintapi

import (
	"encoding/json"
)

// checks if the PolyprintContact type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &PolyprintContact{}

// PolyprintContact struct for PolyprintContact
type PolyprintContact struct {
	EmailAddress interface{} `json:"emailAddress,omitempty"`
	Phone interface{} `json:"phone,omitempty"`
	Firstname interface{} `json:"firstname,omitempty"`
	Lastname interface{} `json:"lastname,omitempty"`
	Company interface{} `json:"company,omitempty"`
	Gender interface{} `json:"gender,omitempty"`
	AcademicTitle interface{} `json:"academicTitle,omitempty"`
	CountryId interface{} `json:"countryId,omitempty"`
	Address *PolyprintContactAddress `json:"address,omitempty"`
}

// NewPolyprintContact instantiates a new PolyprintContact object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewPolyprintContact() *PolyprintContact {
	this := PolyprintContact{}
	return &this
}

// NewPolyprintContactWithDefaults instantiates a new PolyprintContact object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewPolyprintContactWithDefaults() *PolyprintContact {
	this := PolyprintContact{}
	return &this
}

// GetEmailAddress returns the EmailAddress field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *PolyprintContact) GetEmailAddress() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.EmailAddress
}

// GetEmailAddressOk returns a tuple with the EmailAddress field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *PolyprintContact) GetEmailAddressOk() (*interface{}, bool) {
	if o == nil || IsNil(o.EmailAddress) {
		return nil, false
	}
	return &o.EmailAddress, true
}

// HasEmailAddress returns a boolean if a field has been set.
func (o *PolyprintContact) HasEmailAddress() bool {
	if o != nil && IsNil(o.EmailAddress) {
		return true
	}

	return false
}

// SetEmailAddress gets a reference to the given interface{} and assigns it to the EmailAddress field.
func (o *PolyprintContact) SetEmailAddress(v interface{}) {
	o.EmailAddress = v
}

// GetPhone returns the Phone field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *PolyprintContact) GetPhone() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.Phone
}

// GetPhoneOk returns a tuple with the Phone field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *PolyprintContact) GetPhoneOk() (*interface{}, bool) {
	if o == nil || IsNil(o.Phone) {
		return nil, false
	}
	return &o.Phone, true
}

// HasPhone returns a boolean if a field has been set.
func (o *PolyprintContact) HasPhone() bool {
	if o != nil && IsNil(o.Phone) {
		return true
	}

	return false
}

// SetPhone gets a reference to the given interface{} and assigns it to the Phone field.
func (o *PolyprintContact) SetPhone(v interface{}) {
	o.Phone = v
}

// GetFirstname returns the Firstname field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *PolyprintContact) GetFirstname() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.Firstname
}

// GetFirstnameOk returns a tuple with the Firstname field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *PolyprintContact) GetFirstnameOk() (*interface{}, bool) {
	if o == nil || IsNil(o.Firstname) {
		return nil, false
	}
	return &o.Firstname, true
}

// HasFirstname returns a boolean if a field has been set.
func (o *PolyprintContact) HasFirstname() bool {
	if o != nil && IsNil(o.Firstname) {
		return true
	}

	return false
}

// SetFirstname gets a reference to the given interface{} and assigns it to the Firstname field.
func (o *PolyprintContact) SetFirstname(v interface{}) {
	o.Firstname = v
}

// GetLastname returns the Lastname field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *PolyprintContact) GetLastname() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.Lastname
}

// GetLastnameOk returns a tuple with the Lastname field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *PolyprintContact) GetLastnameOk() (*interface{}, bool) {
	if o == nil || IsNil(o.Lastname) {
		return nil, false
	}
	return &o.Lastname, true
}

// HasLastname returns a boolean if a field has been set.
func (o *PolyprintContact) HasLastname() bool {
	if o != nil && IsNil(o.Lastname) {
		return true
	}

	return false
}

// SetLastname gets a reference to the given interface{} and assigns it to the Lastname field.
func (o *PolyprintContact) SetLastname(v interface{}) {
	o.Lastname = v
}

// GetCompany returns the Company field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *PolyprintContact) GetCompany() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.Company
}

// GetCompanyOk returns a tuple with the Company field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *PolyprintContact) GetCompanyOk() (*interface{}, bool) {
	if o == nil || IsNil(o.Company) {
		return nil, false
	}
	return &o.Company, true
}

// HasCompany returns a boolean if a field has been set.
func (o *PolyprintContact) HasCompany() bool {
	if o != nil && IsNil(o.Company) {
		return true
	}

	return false
}

// SetCompany gets a reference to the given interface{} and assigns it to the Company field.
func (o *PolyprintContact) SetCompany(v interface{}) {
	o.Company = v
}

// GetGender returns the Gender field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *PolyprintContact) GetGender() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.Gender
}

// GetGenderOk returns a tuple with the Gender field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *PolyprintContact) GetGenderOk() (*interface{}, bool) {
	if o == nil || IsNil(o.Gender) {
		return nil, false
	}
	return &o.Gender, true
}

// HasGender returns a boolean if a field has been set.
func (o *PolyprintContact) HasGender() bool {
	if o != nil && IsNil(o.Gender) {
		return true
	}

	return false
}

// SetGender gets a reference to the given interface{} and assigns it to the Gender field.
func (o *PolyprintContact) SetGender(v interface{}) {
	o.Gender = v
}

// GetAcademicTitle returns the AcademicTitle field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *PolyprintContact) GetAcademicTitle() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.AcademicTitle
}

// GetAcademicTitleOk returns a tuple with the AcademicTitle field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *PolyprintContact) GetAcademicTitleOk() (*interface{}, bool) {
	if o == nil || IsNil(o.AcademicTitle) {
		return nil, false
	}
	return &o.AcademicTitle, true
}

// HasAcademicTitle returns a boolean if a field has been set.
func (o *PolyprintContact) HasAcademicTitle() bool {
	if o != nil && IsNil(o.AcademicTitle) {
		return true
	}

	return false
}

// SetAcademicTitle gets a reference to the given interface{} and assigns it to the AcademicTitle field.
func (o *PolyprintContact) SetAcademicTitle(v interface{}) {
	o.AcademicTitle = v
}

// GetCountryId returns the CountryId field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *PolyprintContact) GetCountryId() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.CountryId
}

// GetCountryIdOk returns a tuple with the CountryId field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *PolyprintContact) GetCountryIdOk() (*interface{}, bool) {
	if o == nil || IsNil(o.CountryId) {
		return nil, false
	}
	return &o.CountryId, true
}

// HasCountryId returns a boolean if a field has been set.
func (o *PolyprintContact) HasCountryId() bool {
	if o != nil && IsNil(o.CountryId) {
		return true
	}

	return false
}

// SetCountryId gets a reference to the given interface{} and assigns it to the CountryId field.
func (o *PolyprintContact) SetCountryId(v interface{}) {
	o.CountryId = v
}

// GetAddress returns the Address field value if set, zero value otherwise.
func (o *PolyprintContact) GetAddress() PolyprintContactAddress {
	if o == nil || IsNil(o.Address) {
		var ret PolyprintContactAddress
		return ret
	}
	return *o.Address
}

// GetAddressOk returns a tuple with the Address field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PolyprintContact) GetAddressOk() (*PolyprintContactAddress, bool) {
	if o == nil || IsNil(o.Address) {
		return nil, false
	}
	return o.Address, true
}

// HasAddress returns a boolean if a field has been set.
func (o *PolyprintContact) HasAddress() bool {
	if o != nil && !IsNil(o.Address) {
		return true
	}

	return false
}

// SetAddress gets a reference to the given PolyprintContactAddress and assigns it to the Address field.
func (o *PolyprintContact) SetAddress(v PolyprintContactAddress) {
	o.Address = &v
}

func (o PolyprintContact) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o PolyprintContact) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if o.EmailAddress != nil {
		toSerialize["emailAddress"] = o.EmailAddress
	}
	if o.Phone != nil {
		toSerialize["phone"] = o.Phone
	}
	if o.Firstname != nil {
		toSerialize["firstname"] = o.Firstname
	}
	if o.Lastname != nil {
		toSerialize["lastname"] = o.Lastname
	}
	if o.Company != nil {
		toSerialize["company"] = o.Company
	}
	if o.Gender != nil {
		toSerialize["gender"] = o.Gender
	}
	if o.AcademicTitle != nil {
		toSerialize["academicTitle"] = o.AcademicTitle
	}
	if o.CountryId != nil {
		toSerialize["countryId"] = o.CountryId
	}
	if !IsNil(o.Address) {
		toSerialize["address"] = o.Address
	}
	return toSerialize, nil
}

type NullablePolyprintContact struct {
	value *PolyprintContact
	isSet bool
}

func (v NullablePolyprintContact) Get() *PolyprintContact {
	return v.value
}

func (v *NullablePolyprintContact) Set(val *PolyprintContact) {
	v.value = val
	v.isSet = true
}

func (v NullablePolyprintContact) IsSet() bool {
	return v.isSet
}

func (v *NullablePolyprintContact) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePolyprintContact(val *PolyprintContact) *NullablePolyprintContact {
	return &NullablePolyprintContact{value: val, isSet: true}
}

func (v NullablePolyprintContact) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePolyprintContact) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


