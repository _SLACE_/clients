# DynamicAddressesListRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Plz** | Pointer to **int64** |  | [optional] 
**District** | Pointer to **string** |  | [optional] 
**City** | Pointer to **string** |  | [optional] 
**Street** | Pointer to **string** |  | [optional] 
**HouseNumber** | Pointer to **int64** |  | [optional] 
**HouseNumberVariant** | Pointer to **string** |  | [optional] 
**Lat** | Pointer to **string** |  | [optional] 
**Lng** | Pointer to **string** |  | [optional] 

## Methods

### NewDynamicAddressesListRequest

`func NewDynamicAddressesListRequest() *DynamicAddressesListRequest`

NewDynamicAddressesListRequest instantiates a new DynamicAddressesListRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDynamicAddressesListRequestWithDefaults

`func NewDynamicAddressesListRequestWithDefaults() *DynamicAddressesListRequest`

NewDynamicAddressesListRequestWithDefaults instantiates a new DynamicAddressesListRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPlz

`func (o *DynamicAddressesListRequest) GetPlz() int64`

GetPlz returns the Plz field if non-nil, zero value otherwise.

### GetPlzOk

`func (o *DynamicAddressesListRequest) GetPlzOk() (*int64, bool)`

GetPlzOk returns a tuple with the Plz field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPlz

`func (o *DynamicAddressesListRequest) SetPlz(v int64)`

SetPlz sets Plz field to given value.

### HasPlz

`func (o *DynamicAddressesListRequest) HasPlz() bool`

HasPlz returns a boolean if a field has been set.

### GetDistrict

`func (o *DynamicAddressesListRequest) GetDistrict() string`

GetDistrict returns the District field if non-nil, zero value otherwise.

### GetDistrictOk

`func (o *DynamicAddressesListRequest) GetDistrictOk() (*string, bool)`

GetDistrictOk returns a tuple with the District field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDistrict

`func (o *DynamicAddressesListRequest) SetDistrict(v string)`

SetDistrict sets District field to given value.

### HasDistrict

`func (o *DynamicAddressesListRequest) HasDistrict() bool`

HasDistrict returns a boolean if a field has been set.

### GetCity

`func (o *DynamicAddressesListRequest) GetCity() string`

GetCity returns the City field if non-nil, zero value otherwise.

### GetCityOk

`func (o *DynamicAddressesListRequest) GetCityOk() (*string, bool)`

GetCityOk returns a tuple with the City field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCity

`func (o *DynamicAddressesListRequest) SetCity(v string)`

SetCity sets City field to given value.

### HasCity

`func (o *DynamicAddressesListRequest) HasCity() bool`

HasCity returns a boolean if a field has been set.

### GetStreet

`func (o *DynamicAddressesListRequest) GetStreet() string`

GetStreet returns the Street field if non-nil, zero value otherwise.

### GetStreetOk

`func (o *DynamicAddressesListRequest) GetStreetOk() (*string, bool)`

GetStreetOk returns a tuple with the Street field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreet

`func (o *DynamicAddressesListRequest) SetStreet(v string)`

SetStreet sets Street field to given value.

### HasStreet

`func (o *DynamicAddressesListRequest) HasStreet() bool`

HasStreet returns a boolean if a field has been set.

### GetHouseNumber

`func (o *DynamicAddressesListRequest) GetHouseNumber() int64`

GetHouseNumber returns the HouseNumber field if non-nil, zero value otherwise.

### GetHouseNumberOk

`func (o *DynamicAddressesListRequest) GetHouseNumberOk() (*int64, bool)`

GetHouseNumberOk returns a tuple with the HouseNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHouseNumber

`func (o *DynamicAddressesListRequest) SetHouseNumber(v int64)`

SetHouseNumber sets HouseNumber field to given value.

### HasHouseNumber

`func (o *DynamicAddressesListRequest) HasHouseNumber() bool`

HasHouseNumber returns a boolean if a field has been set.

### GetHouseNumberVariant

`func (o *DynamicAddressesListRequest) GetHouseNumberVariant() string`

GetHouseNumberVariant returns the HouseNumberVariant field if non-nil, zero value otherwise.

### GetHouseNumberVariantOk

`func (o *DynamicAddressesListRequest) GetHouseNumberVariantOk() (*string, bool)`

GetHouseNumberVariantOk returns a tuple with the HouseNumberVariant field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHouseNumberVariant

`func (o *DynamicAddressesListRequest) SetHouseNumberVariant(v string)`

SetHouseNumberVariant sets HouseNumberVariant field to given value.

### HasHouseNumberVariant

`func (o *DynamicAddressesListRequest) HasHouseNumberVariant() bool`

HasHouseNumberVariant returns a boolean if a field has been set.

### GetLat

`func (o *DynamicAddressesListRequest) GetLat() string`

GetLat returns the Lat field if non-nil, zero value otherwise.

### GetLatOk

`func (o *DynamicAddressesListRequest) GetLatOk() (*string, bool)`

GetLatOk returns a tuple with the Lat field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLat

`func (o *DynamicAddressesListRequest) SetLat(v string)`

SetLat sets Lat field to given value.

### HasLat

`func (o *DynamicAddressesListRequest) HasLat() bool`

HasLat returns a boolean if a field has been set.

### GetLng

`func (o *DynamicAddressesListRequest) GetLng() string`

GetLng returns the Lng field if non-nil, zero value otherwise.

### GetLngOk

`func (o *DynamicAddressesListRequest) GetLngOk() (*string, bool)`

GetLngOk returns a tuple with the Lng field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLng

`func (o *DynamicAddressesListRequest) SetLng(v string)`

SetLng sets Lng field to given value.

### HasLng

`func (o *DynamicAddressesListRequest) HasLng() bool`

HasLng returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


