# \GiganetzAPI

All URIs are relative to *https://crm.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddressById**](GiganetzAPI.md#AddressById) | **Get** /integrations/giganetz/address-investment | Get giganetz address by id
[**DynamicAddressesList**](GiganetzAPI.md#DynamicAddressesList) | **Post** /integrations/giganetz/address-investments | Giganetz address investments
[**GetAddressByFormData**](GiganetzAPI.md#GetAddressByFormData) | **Post** /integrations/giganetz/address-investment | Get giganetz address by form data
[**PostIntegrationsGiganetzAddSignature**](GiganetzAPI.md#PostIntegrationsGiganetzAddSignature) | **Post** /integrations/giganetz/add-signature | Add signature to address



## AddressById

> AddressById200Response AddressById(ctx).Id(id).Execute()

Get giganetz address by id

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/crmapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GiganetzAPI.AddressById(context.Background()).Id(id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GiganetzAPI.AddressById``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddressById`: AddressById200Response
    fmt.Fprintf(os.Stdout, "Response from `GiganetzAPI.AddressById`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiAddressByIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string** |  | 

### Return type

[**AddressById200Response**](AddressById200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DynamicAddressesList

> DynamicAddressesList200Response DynamicAddressesList(ctx).DynamicAddressesListRequest(dynamicAddressesListRequest).Execute()

Giganetz address investments

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/crmapi"
)

func main() {
    dynamicAddressesListRequest := *openapiclient.NewDynamicAddressesListRequest() // DynamicAddressesListRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GiganetzAPI.DynamicAddressesList(context.Background()).DynamicAddressesListRequest(dynamicAddressesListRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GiganetzAPI.DynamicAddressesList``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DynamicAddressesList`: DynamicAddressesList200Response
    fmt.Fprintf(os.Stdout, "Response from `GiganetzAPI.DynamicAddressesList`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiDynamicAddressesListRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dynamicAddressesListRequest** | [**DynamicAddressesListRequest**](DynamicAddressesListRequest.md) |  | 

### Return type

[**DynamicAddressesList200Response**](DynamicAddressesList200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAddressByFormData

> GetAddressByFormData200Response GetAddressByFormData(ctx).Execute()

Get giganetz address by form data

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/crmapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GiganetzAPI.GetAddressByFormData(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GiganetzAPI.GetAddressByFormData``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAddressByFormData`: GetAddressByFormData200Response
    fmt.Fprintf(os.Stdout, "Response from `GiganetzAPI.GetAddressByFormData`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetAddressByFormDataRequest struct via the builder pattern


### Return type

[**GetAddressByFormData200Response**](GetAddressByFormData200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PostIntegrationsGiganetzAddSignature

> PostIntegrationsGiganetzAddSignature200Response PostIntegrationsGiganetzAddSignature(ctx).Execute()

Add signature to address



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/crmapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.GiganetzAPI.PostIntegrationsGiganetzAddSignature(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GiganetzAPI.PostIntegrationsGiganetzAddSignature``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PostIntegrationsGiganetzAddSignature`: PostIntegrationsGiganetzAddSignature200Response
    fmt.Fprintf(os.Stdout, "Response from `GiganetzAPI.PostIntegrationsGiganetzAddSignature`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiPostIntegrationsGiganetzAddSignatureRequest struct via the builder pattern


### Return type

[**PostIntegrationsGiganetzAddSignature200Response**](PostIntegrationsGiganetzAddSignature200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

