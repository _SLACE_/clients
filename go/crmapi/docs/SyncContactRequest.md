# SyncContactRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContactId** | **string** |  | 
**OrganizationId** | **string** |  | 
**Strategy** | **string** |  | 

## Methods

### NewSyncContactRequest

`func NewSyncContactRequest(contactId string, organizationId string, strategy string, ) *SyncContactRequest`

NewSyncContactRequest instantiates a new SyncContactRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSyncContactRequestWithDefaults

`func NewSyncContactRequestWithDefaults() *SyncContactRequest`

NewSyncContactRequestWithDefaults instantiates a new SyncContactRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContactId

`func (o *SyncContactRequest) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *SyncContactRequest) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *SyncContactRequest) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetOrganizationId

`func (o *SyncContactRequest) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *SyncContactRequest) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *SyncContactRequest) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetStrategy

`func (o *SyncContactRequest) GetStrategy() string`

GetStrategy returns the Strategy field if non-nil, zero value otherwise.

### GetStrategyOk

`func (o *SyncContactRequest) GetStrategyOk() (*string, bool)`

GetStrategyOk returns a tuple with the Strategy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStrategy

`func (o *SyncContactRequest) SetStrategy(v string)`

SetStrategy sets Strategy field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


