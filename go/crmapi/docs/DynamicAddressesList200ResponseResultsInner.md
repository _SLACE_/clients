# DynamicAddressesList200ResponseResultsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Phase** | Pointer to **int32** |  | [optional] 
**Plz** | Pointer to **int32** |  | [optional] 
**District** | Pointer to **string** |  | [optional] 
**City** | Pointer to **string** |  | [optional] 
**Street** | Pointer to **string** |  | [optional] 
**HouseNumber** | Pointer to **int32** |  | [optional] 
**HouseNumberVariant** | Pointer to **string** |  | [optional] 
**Lat** | Pointer to **string** |  | [optional] 
**Lng** | Pointer to **string** |  | [optional] 

## Methods

### NewDynamicAddressesList200ResponseResultsInner

`func NewDynamicAddressesList200ResponseResultsInner() *DynamicAddressesList200ResponseResultsInner`

NewDynamicAddressesList200ResponseResultsInner instantiates a new DynamicAddressesList200ResponseResultsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDynamicAddressesList200ResponseResultsInnerWithDefaults

`func NewDynamicAddressesList200ResponseResultsInnerWithDefaults() *DynamicAddressesList200ResponseResultsInner`

NewDynamicAddressesList200ResponseResultsInnerWithDefaults instantiates a new DynamicAddressesList200ResponseResultsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *DynamicAddressesList200ResponseResultsInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *DynamicAddressesList200ResponseResultsInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *DynamicAddressesList200ResponseResultsInner) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *DynamicAddressesList200ResponseResultsInner) HasId() bool`

HasId returns a boolean if a field has been set.

### GetPhase

`func (o *DynamicAddressesList200ResponseResultsInner) GetPhase() int32`

GetPhase returns the Phase field if non-nil, zero value otherwise.

### GetPhaseOk

`func (o *DynamicAddressesList200ResponseResultsInner) GetPhaseOk() (*int32, bool)`

GetPhaseOk returns a tuple with the Phase field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhase

`func (o *DynamicAddressesList200ResponseResultsInner) SetPhase(v int32)`

SetPhase sets Phase field to given value.

### HasPhase

`func (o *DynamicAddressesList200ResponseResultsInner) HasPhase() bool`

HasPhase returns a boolean if a field has been set.

### GetPlz

`func (o *DynamicAddressesList200ResponseResultsInner) GetPlz() int32`

GetPlz returns the Plz field if non-nil, zero value otherwise.

### GetPlzOk

`func (o *DynamicAddressesList200ResponseResultsInner) GetPlzOk() (*int32, bool)`

GetPlzOk returns a tuple with the Plz field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPlz

`func (o *DynamicAddressesList200ResponseResultsInner) SetPlz(v int32)`

SetPlz sets Plz field to given value.

### HasPlz

`func (o *DynamicAddressesList200ResponseResultsInner) HasPlz() bool`

HasPlz returns a boolean if a field has been set.

### GetDistrict

`func (o *DynamicAddressesList200ResponseResultsInner) GetDistrict() string`

GetDistrict returns the District field if non-nil, zero value otherwise.

### GetDistrictOk

`func (o *DynamicAddressesList200ResponseResultsInner) GetDistrictOk() (*string, bool)`

GetDistrictOk returns a tuple with the District field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDistrict

`func (o *DynamicAddressesList200ResponseResultsInner) SetDistrict(v string)`

SetDistrict sets District field to given value.

### HasDistrict

`func (o *DynamicAddressesList200ResponseResultsInner) HasDistrict() bool`

HasDistrict returns a boolean if a field has been set.

### GetCity

`func (o *DynamicAddressesList200ResponseResultsInner) GetCity() string`

GetCity returns the City field if non-nil, zero value otherwise.

### GetCityOk

`func (o *DynamicAddressesList200ResponseResultsInner) GetCityOk() (*string, bool)`

GetCityOk returns a tuple with the City field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCity

`func (o *DynamicAddressesList200ResponseResultsInner) SetCity(v string)`

SetCity sets City field to given value.

### HasCity

`func (o *DynamicAddressesList200ResponseResultsInner) HasCity() bool`

HasCity returns a boolean if a field has been set.

### GetStreet

`func (o *DynamicAddressesList200ResponseResultsInner) GetStreet() string`

GetStreet returns the Street field if non-nil, zero value otherwise.

### GetStreetOk

`func (o *DynamicAddressesList200ResponseResultsInner) GetStreetOk() (*string, bool)`

GetStreetOk returns a tuple with the Street field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreet

`func (o *DynamicAddressesList200ResponseResultsInner) SetStreet(v string)`

SetStreet sets Street field to given value.

### HasStreet

`func (o *DynamicAddressesList200ResponseResultsInner) HasStreet() bool`

HasStreet returns a boolean if a field has been set.

### GetHouseNumber

`func (o *DynamicAddressesList200ResponseResultsInner) GetHouseNumber() int32`

GetHouseNumber returns the HouseNumber field if non-nil, zero value otherwise.

### GetHouseNumberOk

`func (o *DynamicAddressesList200ResponseResultsInner) GetHouseNumberOk() (*int32, bool)`

GetHouseNumberOk returns a tuple with the HouseNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHouseNumber

`func (o *DynamicAddressesList200ResponseResultsInner) SetHouseNumber(v int32)`

SetHouseNumber sets HouseNumber field to given value.

### HasHouseNumber

`func (o *DynamicAddressesList200ResponseResultsInner) HasHouseNumber() bool`

HasHouseNumber returns a boolean if a field has been set.

### GetHouseNumberVariant

`func (o *DynamicAddressesList200ResponseResultsInner) GetHouseNumberVariant() string`

GetHouseNumberVariant returns the HouseNumberVariant field if non-nil, zero value otherwise.

### GetHouseNumberVariantOk

`func (o *DynamicAddressesList200ResponseResultsInner) GetHouseNumberVariantOk() (*string, bool)`

GetHouseNumberVariantOk returns a tuple with the HouseNumberVariant field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHouseNumberVariant

`func (o *DynamicAddressesList200ResponseResultsInner) SetHouseNumberVariant(v string)`

SetHouseNumberVariant sets HouseNumberVariant field to given value.

### HasHouseNumberVariant

`func (o *DynamicAddressesList200ResponseResultsInner) HasHouseNumberVariant() bool`

HasHouseNumberVariant returns a boolean if a field has been set.

### GetLat

`func (o *DynamicAddressesList200ResponseResultsInner) GetLat() string`

GetLat returns the Lat field if non-nil, zero value otherwise.

### GetLatOk

`func (o *DynamicAddressesList200ResponseResultsInner) GetLatOk() (*string, bool)`

GetLatOk returns a tuple with the Lat field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLat

`func (o *DynamicAddressesList200ResponseResultsInner) SetLat(v string)`

SetLat sets Lat field to given value.

### HasLat

`func (o *DynamicAddressesList200ResponseResultsInner) HasLat() bool`

HasLat returns a boolean if a field has been set.

### GetLng

`func (o *DynamicAddressesList200ResponseResultsInner) GetLng() string`

GetLng returns the Lng field if non-nil, zero value otherwise.

### GetLngOk

`func (o *DynamicAddressesList200ResponseResultsInner) GetLngOk() (*string, bool)`

GetLngOk returns a tuple with the Lng field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLng

`func (o *DynamicAddressesList200ResponseResultsInner) SetLng(v string)`

SetLng sets Lng field to given value.

### HasLng

`func (o *DynamicAddressesList200ResponseResultsInner) HasLng() bool`

HasLng returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


