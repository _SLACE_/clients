# PostIntegrationsGiganetzAddSignature200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MissingSignatures** | Pointer to **int32** |  | [optional] 

## Methods

### NewPostIntegrationsGiganetzAddSignature200Response

`func NewPostIntegrationsGiganetzAddSignature200Response() *PostIntegrationsGiganetzAddSignature200Response`

NewPostIntegrationsGiganetzAddSignature200Response instantiates a new PostIntegrationsGiganetzAddSignature200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPostIntegrationsGiganetzAddSignature200ResponseWithDefaults

`func NewPostIntegrationsGiganetzAddSignature200ResponseWithDefaults() *PostIntegrationsGiganetzAddSignature200Response`

NewPostIntegrationsGiganetzAddSignature200ResponseWithDefaults instantiates a new PostIntegrationsGiganetzAddSignature200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMissingSignatures

`func (o *PostIntegrationsGiganetzAddSignature200Response) GetMissingSignatures() int32`

GetMissingSignatures returns the MissingSignatures field if non-nil, zero value otherwise.

### GetMissingSignaturesOk

`func (o *PostIntegrationsGiganetzAddSignature200Response) GetMissingSignaturesOk() (*int32, bool)`

GetMissingSignaturesOk returns a tuple with the MissingSignatures field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMissingSignatures

`func (o *PostIntegrationsGiganetzAddSignature200Response) SetMissingSignatures(v int32)`

SetMissingSignatures sets MissingSignatures field to given value.

### HasMissingSignatures

`func (o *PostIntegrationsGiganetzAddSignature200Response) HasMissingSignatures() bool`

HasMissingSignatures returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


