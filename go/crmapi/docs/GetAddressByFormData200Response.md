# GetAddressByFormData200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Available** | Pointer to **bool** |  | [optional] 
**Phase** | Pointer to **int64** |  | [optional] 

## Methods

### NewGetAddressByFormData200Response

`func NewGetAddressByFormData200Response() *GetAddressByFormData200Response`

NewGetAddressByFormData200Response instantiates a new GetAddressByFormData200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetAddressByFormData200ResponseWithDefaults

`func NewGetAddressByFormData200ResponseWithDefaults() *GetAddressByFormData200Response`

NewGetAddressByFormData200ResponseWithDefaults instantiates a new GetAddressByFormData200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAvailable

`func (o *GetAddressByFormData200Response) GetAvailable() bool`

GetAvailable returns the Available field if non-nil, zero value otherwise.

### GetAvailableOk

`func (o *GetAddressByFormData200Response) GetAvailableOk() (*bool, bool)`

GetAvailableOk returns a tuple with the Available field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAvailable

`func (o *GetAddressByFormData200Response) SetAvailable(v bool)`

SetAvailable sets Available field to given value.

### HasAvailable

`func (o *GetAddressByFormData200Response) HasAvailable() bool`

HasAvailable returns a boolean if a field has been set.

### GetPhase

`func (o *GetAddressByFormData200Response) GetPhase() int64`

GetPhase returns the Phase field if non-nil, zero value otherwise.

### GetPhaseOk

`func (o *GetAddressByFormData200Response) GetPhaseOk() (*int64, bool)`

GetPhaseOk returns a tuple with the Phase field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhase

`func (o *GetAddressByFormData200Response) SetPhase(v int64)`

SetPhase sets Phase field to given value.

### HasPhase

`func (o *GetAddressByFormData200Response) HasPhase() bool`

HasPhase returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


