# AddressById200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Phase** | Pointer to **int64** |  | [optional] 
**Plz** | Pointer to **int64** |  | [optional] 
**District** | Pointer to **string** |  | [optional] 
**City** | Pointer to **string** |  | [optional] 
**Street** | Pointer to **string** |  | [optional] 
**HouseNumber** | Pointer to **int64** |  | [optional] 
**HouseNumberVariant** | Pointer to **string** |  | [optional] 
**Lat** | Pointer to **string** |  | [optional] 
**Lng** | Pointer to **string** |  | [optional] 
**Signatures** | Pointer to **int32** |  | [optional] 

## Methods

### NewAddressById200Response

`func NewAddressById200Response() *AddressById200Response`

NewAddressById200Response instantiates a new AddressById200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAddressById200ResponseWithDefaults

`func NewAddressById200ResponseWithDefaults() *AddressById200Response`

NewAddressById200ResponseWithDefaults instantiates a new AddressById200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *AddressById200Response) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *AddressById200Response) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *AddressById200Response) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *AddressById200Response) HasId() bool`

HasId returns a boolean if a field has been set.

### GetPhase

`func (o *AddressById200Response) GetPhase() int64`

GetPhase returns the Phase field if non-nil, zero value otherwise.

### GetPhaseOk

`func (o *AddressById200Response) GetPhaseOk() (*int64, bool)`

GetPhaseOk returns a tuple with the Phase field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhase

`func (o *AddressById200Response) SetPhase(v int64)`

SetPhase sets Phase field to given value.

### HasPhase

`func (o *AddressById200Response) HasPhase() bool`

HasPhase returns a boolean if a field has been set.

### GetPlz

`func (o *AddressById200Response) GetPlz() int64`

GetPlz returns the Plz field if non-nil, zero value otherwise.

### GetPlzOk

`func (o *AddressById200Response) GetPlzOk() (*int64, bool)`

GetPlzOk returns a tuple with the Plz field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPlz

`func (o *AddressById200Response) SetPlz(v int64)`

SetPlz sets Plz field to given value.

### HasPlz

`func (o *AddressById200Response) HasPlz() bool`

HasPlz returns a boolean if a field has been set.

### GetDistrict

`func (o *AddressById200Response) GetDistrict() string`

GetDistrict returns the District field if non-nil, zero value otherwise.

### GetDistrictOk

`func (o *AddressById200Response) GetDistrictOk() (*string, bool)`

GetDistrictOk returns a tuple with the District field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDistrict

`func (o *AddressById200Response) SetDistrict(v string)`

SetDistrict sets District field to given value.

### HasDistrict

`func (o *AddressById200Response) HasDistrict() bool`

HasDistrict returns a boolean if a field has been set.

### GetCity

`func (o *AddressById200Response) GetCity() string`

GetCity returns the City field if non-nil, zero value otherwise.

### GetCityOk

`func (o *AddressById200Response) GetCityOk() (*string, bool)`

GetCityOk returns a tuple with the City field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCity

`func (o *AddressById200Response) SetCity(v string)`

SetCity sets City field to given value.

### HasCity

`func (o *AddressById200Response) HasCity() bool`

HasCity returns a boolean if a field has been set.

### GetStreet

`func (o *AddressById200Response) GetStreet() string`

GetStreet returns the Street field if non-nil, zero value otherwise.

### GetStreetOk

`func (o *AddressById200Response) GetStreetOk() (*string, bool)`

GetStreetOk returns a tuple with the Street field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreet

`func (o *AddressById200Response) SetStreet(v string)`

SetStreet sets Street field to given value.

### HasStreet

`func (o *AddressById200Response) HasStreet() bool`

HasStreet returns a boolean if a field has been set.

### GetHouseNumber

`func (o *AddressById200Response) GetHouseNumber() int64`

GetHouseNumber returns the HouseNumber field if non-nil, zero value otherwise.

### GetHouseNumberOk

`func (o *AddressById200Response) GetHouseNumberOk() (*int64, bool)`

GetHouseNumberOk returns a tuple with the HouseNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHouseNumber

`func (o *AddressById200Response) SetHouseNumber(v int64)`

SetHouseNumber sets HouseNumber field to given value.

### HasHouseNumber

`func (o *AddressById200Response) HasHouseNumber() bool`

HasHouseNumber returns a boolean if a field has been set.

### GetHouseNumberVariant

`func (o *AddressById200Response) GetHouseNumberVariant() string`

GetHouseNumberVariant returns the HouseNumberVariant field if non-nil, zero value otherwise.

### GetHouseNumberVariantOk

`func (o *AddressById200Response) GetHouseNumberVariantOk() (*string, bool)`

GetHouseNumberVariantOk returns a tuple with the HouseNumberVariant field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHouseNumberVariant

`func (o *AddressById200Response) SetHouseNumberVariant(v string)`

SetHouseNumberVariant sets HouseNumberVariant field to given value.

### HasHouseNumberVariant

`func (o *AddressById200Response) HasHouseNumberVariant() bool`

HasHouseNumberVariant returns a boolean if a field has been set.

### GetLat

`func (o *AddressById200Response) GetLat() string`

GetLat returns the Lat field if non-nil, zero value otherwise.

### GetLatOk

`func (o *AddressById200Response) GetLatOk() (*string, bool)`

GetLatOk returns a tuple with the Lat field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLat

`func (o *AddressById200Response) SetLat(v string)`

SetLat sets Lat field to given value.

### HasLat

`func (o *AddressById200Response) HasLat() bool`

HasLat returns a boolean if a field has been set.

### GetLng

`func (o *AddressById200Response) GetLng() string`

GetLng returns the Lng field if non-nil, zero value otherwise.

### GetLngOk

`func (o *AddressById200Response) GetLngOk() (*string, bool)`

GetLngOk returns a tuple with the Lng field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLng

`func (o *AddressById200Response) SetLng(v string)`

SetLng sets Lng field to given value.

### HasLng

`func (o *AddressById200Response) HasLng() bool`

HasLng returns a boolean if a field has been set.

### GetSignatures

`func (o *AddressById200Response) GetSignatures() int32`

GetSignatures returns the Signatures field if non-nil, zero value otherwise.

### GetSignaturesOk

`func (o *AddressById200Response) GetSignaturesOk() (*int32, bool)`

GetSignaturesOk returns a tuple with the Signatures field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSignatures

`func (o *AddressById200Response) SetSignatures(v int32)`

SetSignatures sets Signatures field to given value.

### HasSignatures

`func (o *AddressById200Response) HasSignatures() bool`

HasSignatures returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


