# CreateInsignConfigRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Paid** | Pointer to [**CreateInsignConfigRequestPaid**](CreateInsignConfigRequestPaid.md) |  | [optional] 
**Free** | Pointer to [**CreateInsignConfigRequestFree**](CreateInsignConfigRequestFree.md) |  | [optional] 

## Methods

### NewCreateInsignConfigRequest

`func NewCreateInsignConfigRequest() *CreateInsignConfigRequest`

NewCreateInsignConfigRequest instantiates a new CreateInsignConfigRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateInsignConfigRequestWithDefaults

`func NewCreateInsignConfigRequestWithDefaults() *CreateInsignConfigRequest`

NewCreateInsignConfigRequestWithDefaults instantiates a new CreateInsignConfigRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPaid

`func (o *CreateInsignConfigRequest) GetPaid() CreateInsignConfigRequestPaid`

GetPaid returns the Paid field if non-nil, zero value otherwise.

### GetPaidOk

`func (o *CreateInsignConfigRequest) GetPaidOk() (*CreateInsignConfigRequestPaid, bool)`

GetPaidOk returns a tuple with the Paid field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPaid

`func (o *CreateInsignConfigRequest) SetPaid(v CreateInsignConfigRequestPaid)`

SetPaid sets Paid field to given value.

### HasPaid

`func (o *CreateInsignConfigRequest) HasPaid() bool`

HasPaid returns a boolean if a field has been set.

### GetFree

`func (o *CreateInsignConfigRequest) GetFree() CreateInsignConfigRequestFree`

GetFree returns the Free field if non-nil, zero value otherwise.

### GetFreeOk

`func (o *CreateInsignConfigRequest) GetFreeOk() (*CreateInsignConfigRequestFree, bool)`

GetFreeOk returns a tuple with the Free field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFree

`func (o *CreateInsignConfigRequest) SetFree(v CreateInsignConfigRequestFree)`

SetFree sets Free field to given value.

### HasFree

`func (o *CreateInsignConfigRequest) HasFree() bool`

HasFree returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


