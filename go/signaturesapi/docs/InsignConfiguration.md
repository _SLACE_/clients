# InsignConfiguration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**OrganizationId** | **string** |  | 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 
**Free** | Pointer to [**InsignConfigurationFree**](InsignConfigurationFree.md) |  | [optional] 
**Paid** | Pointer to [**InsignConfigurationPaid**](InsignConfigurationPaid.md) |  | [optional] 

## Methods

### NewInsignConfiguration

`func NewInsignConfiguration(id string, organizationId string, createdAt time.Time, updatedAt time.Time, ) *InsignConfiguration`

NewInsignConfiguration instantiates a new InsignConfiguration object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInsignConfigurationWithDefaults

`func NewInsignConfigurationWithDefaults() *InsignConfiguration`

NewInsignConfigurationWithDefaults instantiates a new InsignConfiguration object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *InsignConfiguration) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *InsignConfiguration) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *InsignConfiguration) SetId(v string)`

SetId sets Id field to given value.


### GetOrganizationId

`func (o *InsignConfiguration) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *InsignConfiguration) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *InsignConfiguration) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetCreatedAt

`func (o *InsignConfiguration) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *InsignConfiguration) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *InsignConfiguration) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *InsignConfiguration) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *InsignConfiguration) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *InsignConfiguration) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetFree

`func (o *InsignConfiguration) GetFree() InsignConfigurationFree`

GetFree returns the Free field if non-nil, zero value otherwise.

### GetFreeOk

`func (o *InsignConfiguration) GetFreeOk() (*InsignConfigurationFree, bool)`

GetFreeOk returns a tuple with the Free field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFree

`func (o *InsignConfiguration) SetFree(v InsignConfigurationFree)`

SetFree sets Free field to given value.

### HasFree

`func (o *InsignConfiguration) HasFree() bool`

HasFree returns a boolean if a field has been set.

### GetPaid

`func (o *InsignConfiguration) GetPaid() InsignConfigurationPaid`

GetPaid returns the Paid field if non-nil, zero value otherwise.

### GetPaidOk

`func (o *InsignConfiguration) GetPaidOk() (*InsignConfigurationPaid, bool)`

GetPaidOk returns a tuple with the Paid field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPaid

`func (o *InsignConfiguration) SetPaid(v InsignConfigurationPaid)`

SetPaid sets Paid field to given value.

### HasPaid

`func (o *InsignConfiguration) HasPaid() bool`

HasPaid returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


