# DocumentData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Language** | **string** |  | 
**Gender** | **string** |  | 
**Firstname** | **string** |  | 
**Lastname** | **string** |  | 
**Phone** | **string** |  | 
**Birthday** | **string** |  | 
**Address** | [**Address**](Address.md) |  | 
**Email** | **string** |  | 
**Attributes** | **map[string]interface{}** |  | 

## Methods

### NewDocumentData

`func NewDocumentData(language string, gender string, firstname string, lastname string, phone string, birthday string, address Address, email string, attributes map[string]interface{}, ) *DocumentData`

NewDocumentData instantiates a new DocumentData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDocumentDataWithDefaults

`func NewDocumentDataWithDefaults() *DocumentData`

NewDocumentDataWithDefaults instantiates a new DocumentData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLanguage

`func (o *DocumentData) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *DocumentData) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *DocumentData) SetLanguage(v string)`

SetLanguage sets Language field to given value.


### GetGender

`func (o *DocumentData) GetGender() string`

GetGender returns the Gender field if non-nil, zero value otherwise.

### GetGenderOk

`func (o *DocumentData) GetGenderOk() (*string, bool)`

GetGenderOk returns a tuple with the Gender field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGender

`func (o *DocumentData) SetGender(v string)`

SetGender sets Gender field to given value.


### GetFirstname

`func (o *DocumentData) GetFirstname() string`

GetFirstname returns the Firstname field if non-nil, zero value otherwise.

### GetFirstnameOk

`func (o *DocumentData) GetFirstnameOk() (*string, bool)`

GetFirstnameOk returns a tuple with the Firstname field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstname

`func (o *DocumentData) SetFirstname(v string)`

SetFirstname sets Firstname field to given value.


### GetLastname

`func (o *DocumentData) GetLastname() string`

GetLastname returns the Lastname field if non-nil, zero value otherwise.

### GetLastnameOk

`func (o *DocumentData) GetLastnameOk() (*string, bool)`

GetLastnameOk returns a tuple with the Lastname field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastname

`func (o *DocumentData) SetLastname(v string)`

SetLastname sets Lastname field to given value.


### GetPhone

`func (o *DocumentData) GetPhone() string`

GetPhone returns the Phone field if non-nil, zero value otherwise.

### GetPhoneOk

`func (o *DocumentData) GetPhoneOk() (*string, bool)`

GetPhoneOk returns a tuple with the Phone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhone

`func (o *DocumentData) SetPhone(v string)`

SetPhone sets Phone field to given value.


### GetBirthday

`func (o *DocumentData) GetBirthday() string`

GetBirthday returns the Birthday field if non-nil, zero value otherwise.

### GetBirthdayOk

`func (o *DocumentData) GetBirthdayOk() (*string, bool)`

GetBirthdayOk returns a tuple with the Birthday field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBirthday

`func (o *DocumentData) SetBirthday(v string)`

SetBirthday sets Birthday field to given value.


### GetAddress

`func (o *DocumentData) GetAddress() Address`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *DocumentData) GetAddressOk() (*Address, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *DocumentData) SetAddress(v Address)`

SetAddress sets Address field to given value.


### GetEmail

`func (o *DocumentData) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *DocumentData) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *DocumentData) SetEmail(v string)`

SetEmail sets Email field to given value.


### GetAttributes

`func (o *DocumentData) GetAttributes() map[string]interface{}`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *DocumentData) GetAttributesOk() (*map[string]interface{}, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *DocumentData) SetAttributes(v map[string]interface{})`

SetAttributes sets Attributes field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


