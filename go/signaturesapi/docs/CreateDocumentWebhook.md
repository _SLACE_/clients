# CreateDocumentWebhook

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TransactionId** | **string** |  | 
**ChannelId** | **string** |  | 
**ConversationId** | **string** |  | 
**ContactId** | **string** |  | 
**Document** | [**CreateDocumentWebhookDocument**](CreateDocumentWebhookDocument.md) |  | 

## Methods

### NewCreateDocumentWebhook

`func NewCreateDocumentWebhook(transactionId string, channelId string, conversationId string, contactId string, document CreateDocumentWebhookDocument, ) *CreateDocumentWebhook`

NewCreateDocumentWebhook instantiates a new CreateDocumentWebhook object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateDocumentWebhookWithDefaults

`func NewCreateDocumentWebhookWithDefaults() *CreateDocumentWebhook`

NewCreateDocumentWebhookWithDefaults instantiates a new CreateDocumentWebhook object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTransactionId

`func (o *CreateDocumentWebhook) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *CreateDocumentWebhook) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *CreateDocumentWebhook) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetChannelId

`func (o *CreateDocumentWebhook) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CreateDocumentWebhook) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CreateDocumentWebhook) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetConversationId

`func (o *CreateDocumentWebhook) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *CreateDocumentWebhook) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *CreateDocumentWebhook) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetContactId

`func (o *CreateDocumentWebhook) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *CreateDocumentWebhook) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *CreateDocumentWebhook) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetDocument

`func (o *CreateDocumentWebhook) GetDocument() CreateDocumentWebhookDocument`

GetDocument returns the Document field if non-nil, zero value otherwise.

### GetDocumentOk

`func (o *CreateDocumentWebhook) GetDocumentOk() (*CreateDocumentWebhookDocument, bool)`

GetDocumentOk returns a tuple with the Document field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDocument

`func (o *CreateDocumentWebhook) SetDocument(v CreateDocumentWebhookDocument)`

SetDocument sets Document field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


