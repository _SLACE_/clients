# \InsignAPI

All URIs are relative to *https://signature.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateInsignConfiguration**](InsignAPI.md#CreateInsignConfiguration) | **Post** /integrations/insign/organizations/{oid}/configurations | Create Insign configuration
[**DeleteInsignConfiguration**](InsignAPI.md#DeleteInsignConfiguration) | **Delete** /integrations/insign/organizations/{oid}/configurations | Delete Insign configuration
[**GetInsignConfiguration**](InsignAPI.md#GetInsignConfiguration) | **Get** /integrations/insign/organizations/{oid}/configurations | Get Insign configuration



## CreateInsignConfiguration

> InsignConfiguration CreateInsignConfiguration(ctx, oid).CreateInsignConfigRequest(createInsignConfigRequest).Execute()

Create Insign configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/signaturesapi"
)

func main() {
    oid := "oid_example" // string | Organization ID
    createInsignConfigRequest := *openapiclient.NewCreateInsignConfigRequest() // CreateInsignConfigRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InsignAPI.CreateInsignConfiguration(context.Background(), oid).CreateInsignConfigRequest(createInsignConfigRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InsignAPI.CreateInsignConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateInsignConfiguration`: InsignConfiguration
    fmt.Fprintf(os.Stdout, "Response from `InsignAPI.CreateInsignConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** | Organization ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateInsignConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **createInsignConfigRequest** | [**CreateInsignConfigRequest**](CreateInsignConfigRequest.md) |  | 

### Return type

[**InsignConfiguration**](InsignConfiguration.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteInsignConfiguration

> DeleteInsignConfiguration(ctx, oid).Execute()

Delete Insign configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/signaturesapi"
)

func main() {
    oid := "oid_example" // string | Organization ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.InsignAPI.DeleteInsignConfiguration(context.Background(), oid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InsignAPI.DeleteInsignConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** | Organization ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteInsignConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetInsignConfiguration

> InsignConfiguration GetInsignConfiguration(ctx, oid).Execute()

Get Insign configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/signaturesapi"
)

func main() {
    oid := "oid_example" // string | Organization ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.InsignAPI.GetInsignConfiguration(context.Background(), oid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `InsignAPI.GetInsignConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetInsignConfiguration`: InsignConfiguration
    fmt.Fprintf(os.Stdout, "Response from `InsignAPI.GetInsignConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** | Organization ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetInsignConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**InsignConfiguration**](InsignConfiguration.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

