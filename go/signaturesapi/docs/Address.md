# Address

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Street** | **string** |  | 
**HouseNumber** | **string** |  | 
**DoorNumber** | **string** |  | 
**Zip** | **string** |  | 
**City** | **string** |  | 
**Country** | **string** |  | 
**Ortsteil** | **string** |  | 

## Methods

### NewAddress

`func NewAddress(street string, houseNumber string, doorNumber string, zip string, city string, country string, ortsteil string, ) *Address`

NewAddress instantiates a new Address object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAddressWithDefaults

`func NewAddressWithDefaults() *Address`

NewAddressWithDefaults instantiates a new Address object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStreet

`func (o *Address) GetStreet() string`

GetStreet returns the Street field if non-nil, zero value otherwise.

### GetStreetOk

`func (o *Address) GetStreetOk() (*string, bool)`

GetStreetOk returns a tuple with the Street field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreet

`func (o *Address) SetStreet(v string)`

SetStreet sets Street field to given value.


### GetHouseNumber

`func (o *Address) GetHouseNumber() string`

GetHouseNumber returns the HouseNumber field if non-nil, zero value otherwise.

### GetHouseNumberOk

`func (o *Address) GetHouseNumberOk() (*string, bool)`

GetHouseNumberOk returns a tuple with the HouseNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHouseNumber

`func (o *Address) SetHouseNumber(v string)`

SetHouseNumber sets HouseNumber field to given value.


### GetDoorNumber

`func (o *Address) GetDoorNumber() string`

GetDoorNumber returns the DoorNumber field if non-nil, zero value otherwise.

### GetDoorNumberOk

`func (o *Address) GetDoorNumberOk() (*string, bool)`

GetDoorNumberOk returns a tuple with the DoorNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDoorNumber

`func (o *Address) SetDoorNumber(v string)`

SetDoorNumber sets DoorNumber field to given value.


### GetZip

`func (o *Address) GetZip() string`

GetZip returns the Zip field if non-nil, zero value otherwise.

### GetZipOk

`func (o *Address) GetZipOk() (*string, bool)`

GetZipOk returns a tuple with the Zip field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetZip

`func (o *Address) SetZip(v string)`

SetZip sets Zip field to given value.


### GetCity

`func (o *Address) GetCity() string`

GetCity returns the City field if non-nil, zero value otherwise.

### GetCityOk

`func (o *Address) GetCityOk() (*string, bool)`

GetCityOk returns a tuple with the City field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCity

`func (o *Address) SetCity(v string)`

SetCity sets City field to given value.


### GetCountry

`func (o *Address) GetCountry() string`

GetCountry returns the Country field if non-nil, zero value otherwise.

### GetCountryOk

`func (o *Address) GetCountryOk() (*string, bool)`

GetCountryOk returns a tuple with the Country field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountry

`func (o *Address) SetCountry(v string)`

SetCountry sets Country field to given value.


### GetOrtsteil

`func (o *Address) GetOrtsteil() string`

GetOrtsteil returns the Ortsteil field if non-nil, zero value otherwise.

### GetOrtsteilOk

`func (o *Address) GetOrtsteilOk() (*string, bool)`

GetOrtsteilOk returns a tuple with the Ortsteil field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrtsteil

`func (o *Address) SetOrtsteil(v string)`

SetOrtsteil sets Ortsteil field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


