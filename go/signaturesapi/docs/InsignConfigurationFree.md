# InsignConfigurationFree

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **string** |  | 
**ApiUrl** | **string** |  | 
**WebUrl** | **string** |  | 

## Methods

### NewInsignConfigurationFree

`func NewInsignConfigurationFree(username string, apiUrl string, webUrl string, ) *InsignConfigurationFree`

NewInsignConfigurationFree instantiates a new InsignConfigurationFree object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInsignConfigurationFreeWithDefaults

`func NewInsignConfigurationFreeWithDefaults() *InsignConfigurationFree`

NewInsignConfigurationFreeWithDefaults instantiates a new InsignConfigurationFree object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUsername

`func (o *InsignConfigurationFree) GetUsername() string`

GetUsername returns the Username field if non-nil, zero value otherwise.

### GetUsernameOk

`func (o *InsignConfigurationFree) GetUsernameOk() (*string, bool)`

GetUsernameOk returns a tuple with the Username field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsername

`func (o *InsignConfigurationFree) SetUsername(v string)`

SetUsername sets Username field to given value.


### GetApiUrl

`func (o *InsignConfigurationFree) GetApiUrl() string`

GetApiUrl returns the ApiUrl field if non-nil, zero value otherwise.

### GetApiUrlOk

`func (o *InsignConfigurationFree) GetApiUrlOk() (*string, bool)`

GetApiUrlOk returns a tuple with the ApiUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetApiUrl

`func (o *InsignConfigurationFree) SetApiUrl(v string)`

SetApiUrl sets ApiUrl field to given value.


### GetWebUrl

`func (o *InsignConfigurationFree) GetWebUrl() string`

GetWebUrl returns the WebUrl field if non-nil, zero value otherwise.

### GetWebUrlOk

`func (o *InsignConfigurationFree) GetWebUrlOk() (*string, bool)`

GetWebUrlOk returns a tuple with the WebUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWebUrl

`func (o *InsignConfigurationFree) SetWebUrl(v string)`

SetWebUrl sets WebUrl field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


