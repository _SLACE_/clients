# SignatureRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Mode** | **string** |  | 
**ContactId** | **string** |  | 
**TemplateId** | **string** |  | 
**TransactionId** | **string** |  | 
**ConversationId** | **string** |  | 
**GatewayId** | **string** |  | 
**SessionName** | **string** |  | 
**Data** | [**DocumentData**](DocumentData.md) |  | 
**MessengerId** | **string** |  | 
**MessengerType** | **string** |  | 
**OrganizationId** | **string** |  | 
**ChannelId** | **string** |  | 

## Methods

### NewSignatureRequest

`func NewSignatureRequest(mode string, contactId string, templateId string, transactionId string, conversationId string, gatewayId string, sessionName string, data DocumentData, messengerId string, messengerType string, organizationId string, channelId string, ) *SignatureRequest`

NewSignatureRequest instantiates a new SignatureRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSignatureRequestWithDefaults

`func NewSignatureRequestWithDefaults() *SignatureRequest`

NewSignatureRequestWithDefaults instantiates a new SignatureRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMode

`func (o *SignatureRequest) GetMode() string`

GetMode returns the Mode field if non-nil, zero value otherwise.

### GetModeOk

`func (o *SignatureRequest) GetModeOk() (*string, bool)`

GetModeOk returns a tuple with the Mode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMode

`func (o *SignatureRequest) SetMode(v string)`

SetMode sets Mode field to given value.


### GetContactId

`func (o *SignatureRequest) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *SignatureRequest) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *SignatureRequest) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetTemplateId

`func (o *SignatureRequest) GetTemplateId() string`

GetTemplateId returns the TemplateId field if non-nil, zero value otherwise.

### GetTemplateIdOk

`func (o *SignatureRequest) GetTemplateIdOk() (*string, bool)`

GetTemplateIdOk returns a tuple with the TemplateId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplateId

`func (o *SignatureRequest) SetTemplateId(v string)`

SetTemplateId sets TemplateId field to given value.


### GetTransactionId

`func (o *SignatureRequest) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *SignatureRequest) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *SignatureRequest) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetConversationId

`func (o *SignatureRequest) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *SignatureRequest) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *SignatureRequest) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetGatewayId

`func (o *SignatureRequest) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *SignatureRequest) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *SignatureRequest) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.


### GetSessionName

`func (o *SignatureRequest) GetSessionName() string`

GetSessionName returns the SessionName field if non-nil, zero value otherwise.

### GetSessionNameOk

`func (o *SignatureRequest) GetSessionNameOk() (*string, bool)`

GetSessionNameOk returns a tuple with the SessionName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSessionName

`func (o *SignatureRequest) SetSessionName(v string)`

SetSessionName sets SessionName field to given value.


### GetData

`func (o *SignatureRequest) GetData() DocumentData`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *SignatureRequest) GetDataOk() (*DocumentData, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *SignatureRequest) SetData(v DocumentData)`

SetData sets Data field to given value.


### GetMessengerId

`func (o *SignatureRequest) GetMessengerId() string`

GetMessengerId returns the MessengerId field if non-nil, zero value otherwise.

### GetMessengerIdOk

`func (o *SignatureRequest) GetMessengerIdOk() (*string, bool)`

GetMessengerIdOk returns a tuple with the MessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerId

`func (o *SignatureRequest) SetMessengerId(v string)`

SetMessengerId sets MessengerId field to given value.


### GetMessengerType

`func (o *SignatureRequest) GetMessengerType() string`

GetMessengerType returns the MessengerType field if non-nil, zero value otherwise.

### GetMessengerTypeOk

`func (o *SignatureRequest) GetMessengerTypeOk() (*string, bool)`

GetMessengerTypeOk returns a tuple with the MessengerType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerType

`func (o *SignatureRequest) SetMessengerType(v string)`

SetMessengerType sets MessengerType field to given value.


### GetOrganizationId

`func (o *SignatureRequest) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *SignatureRequest) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *SignatureRequest) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetChannelId

`func (o *SignatureRequest) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *SignatureRequest) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *SignatureRequest) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


