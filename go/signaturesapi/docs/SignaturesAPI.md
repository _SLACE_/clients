# \SignaturesAPI

All URIs are relative to *https://signature.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateDocumentTemplate**](SignaturesAPI.md#CreateDocumentTemplate) | **Post** /integrations/insign/templates | Create template
[**CreateSignature**](SignaturesAPI.md#CreateSignature) | **Post** /integrations/insign/signatures | Create Signature (DEPRECATED)
[**GetDocumentTemplate**](SignaturesAPI.md#GetDocumentTemplate) | **Get** /integrations/insign/templates/{id} | Get template



## CreateDocumentTemplate

> Template CreateDocumentTemplate(ctx).TemplateRequest(templateRequest).Execute()

Create template

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/signaturesapi"
)

func main() {
    templateRequest := *openapiclient.NewTemplateRequest("Name_example", "ChannelId_example", "OrganizationId_example", "Type_example", "FileId_example") // TemplateRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.SignaturesAPI.CreateDocumentTemplate(context.Background()).TemplateRequest(templateRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `SignaturesAPI.CreateDocumentTemplate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateDocumentTemplate`: Template
    fmt.Fprintf(os.Stdout, "Response from `SignaturesAPI.CreateDocumentTemplate`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateDocumentTemplateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **templateRequest** | [**TemplateRequest**](TemplateRequest.md) |  | 

### Return type

[**Template**](Template.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateSignature

> SignatureResponse CreateSignature(ctx).SignatureRequest(signatureRequest).Execute()

Create Signature (DEPRECATED)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/signaturesapi"
)

func main() {
    signatureRequest := *openapiclient.NewSignatureRequest("Mode_example", "ContactId_example", "TemplateId_example", "TransactionId_example", "ConversationId_example", "GatewayId_example", "SessionName_example", *openapiclient.NewDocumentData("Language_example", "Gender_example", "Firstname_example", "Lastname_example", "Phone_example", "Birthday_example", *openapiclient.NewAddress("Street_example", "HouseNumber_example", "DoorNumber_example", "Zip_example", "City_example", "Country_example", "Ortsteil_example"), "Email_example", map[string]interface{}(123)), "MessengerId_example", "MessengerType_example", "OrganizationId_example", "ChannelId_example") // SignatureRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.SignaturesAPI.CreateSignature(context.Background()).SignatureRequest(signatureRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `SignaturesAPI.CreateSignature``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateSignature`: SignatureResponse
    fmt.Fprintf(os.Stdout, "Response from `SignaturesAPI.CreateSignature`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateSignatureRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **signatureRequest** | [**SignatureRequest**](SignatureRequest.md) |  | 

### Return type

[**SignatureResponse**](SignatureResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDocumentTemplate

> Template GetDocumentTemplate(ctx, id).Execute()

Get template

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/signaturesapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.SignaturesAPI.GetDocumentTemplate(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `SignaturesAPI.GetDocumentTemplate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetDocumentTemplate`: Template
    fmt.Fprintf(os.Stdout, "Response from `SignaturesAPI.GetDocumentTemplate`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetDocumentTemplateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Template**](Template.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

