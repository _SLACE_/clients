# CreateDocumentWebhookDocument

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**FileUrl** | **string** |  | 

## Methods

### NewCreateDocumentWebhookDocument

`func NewCreateDocumentWebhookDocument(name string, fileUrl string, ) *CreateDocumentWebhookDocument`

NewCreateDocumentWebhookDocument instantiates a new CreateDocumentWebhookDocument object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateDocumentWebhookDocumentWithDefaults

`func NewCreateDocumentWebhookDocumentWithDefaults() *CreateDocumentWebhookDocument`

NewCreateDocumentWebhookDocumentWithDefaults instantiates a new CreateDocumentWebhookDocument object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *CreateDocumentWebhookDocument) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CreateDocumentWebhookDocument) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CreateDocumentWebhookDocument) SetName(v string)`

SetName sets Name field to given value.


### GetFileUrl

`func (o *CreateDocumentWebhookDocument) GetFileUrl() string`

GetFileUrl returns the FileUrl field if non-nil, zero value otherwise.

### GetFileUrlOk

`func (o *CreateDocumentWebhookDocument) GetFileUrlOk() (*string, bool)`

GetFileUrlOk returns a tuple with the FileUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFileUrl

`func (o *CreateDocumentWebhookDocument) SetFileUrl(v string)`

SetFileUrl sets FileUrl field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


