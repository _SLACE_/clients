# CreateInsignConfigRequestFree

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ApiUrl** | **string** |  | 
**WebUrl** | **string** |  | 
**Username** | **string** |  | 
**Password** | **string** |  | 

## Methods

### NewCreateInsignConfigRequestFree

`func NewCreateInsignConfigRequestFree(apiUrl string, webUrl string, username string, password string, ) *CreateInsignConfigRequestFree`

NewCreateInsignConfigRequestFree instantiates a new CreateInsignConfigRequestFree object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateInsignConfigRequestFreeWithDefaults

`func NewCreateInsignConfigRequestFreeWithDefaults() *CreateInsignConfigRequestFree`

NewCreateInsignConfigRequestFreeWithDefaults instantiates a new CreateInsignConfigRequestFree object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetApiUrl

`func (o *CreateInsignConfigRequestFree) GetApiUrl() string`

GetApiUrl returns the ApiUrl field if non-nil, zero value otherwise.

### GetApiUrlOk

`func (o *CreateInsignConfigRequestFree) GetApiUrlOk() (*string, bool)`

GetApiUrlOk returns a tuple with the ApiUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetApiUrl

`func (o *CreateInsignConfigRequestFree) SetApiUrl(v string)`

SetApiUrl sets ApiUrl field to given value.


### GetWebUrl

`func (o *CreateInsignConfigRequestFree) GetWebUrl() string`

GetWebUrl returns the WebUrl field if non-nil, zero value otherwise.

### GetWebUrlOk

`func (o *CreateInsignConfigRequestFree) GetWebUrlOk() (*string, bool)`

GetWebUrlOk returns a tuple with the WebUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWebUrl

`func (o *CreateInsignConfigRequestFree) SetWebUrl(v string)`

SetWebUrl sets WebUrl field to given value.


### GetUsername

`func (o *CreateInsignConfigRequestFree) GetUsername() string`

GetUsername returns the Username field if non-nil, zero value otherwise.

### GetUsernameOk

`func (o *CreateInsignConfigRequestFree) GetUsernameOk() (*string, bool)`

GetUsernameOk returns a tuple with the Username field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsername

`func (o *CreateInsignConfigRequestFree) SetUsername(v string)`

SetUsername sets Username field to given value.


### GetPassword

`func (o *CreateInsignConfigRequestFree) GetPassword() string`

GetPassword returns the Password field if non-nil, zero value otherwise.

### GetPasswordOk

`func (o *CreateInsignConfigRequestFree) GetPasswordOk() (*string, bool)`

GetPasswordOk returns a tuple with the Password field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPassword

`func (o *CreateInsignConfigRequestFree) SetPassword(v string)`

SetPassword sets Password field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


