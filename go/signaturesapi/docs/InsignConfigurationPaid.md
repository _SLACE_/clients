# InsignConfigurationPaid

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **string** |  | 
**ApiUrl** | **string** |  | 
**WebUrl** | **string** |  | 

## Methods

### NewInsignConfigurationPaid

`func NewInsignConfigurationPaid(username string, apiUrl string, webUrl string, ) *InsignConfigurationPaid`

NewInsignConfigurationPaid instantiates a new InsignConfigurationPaid object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInsignConfigurationPaidWithDefaults

`func NewInsignConfigurationPaidWithDefaults() *InsignConfigurationPaid`

NewInsignConfigurationPaidWithDefaults instantiates a new InsignConfigurationPaid object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUsername

`func (o *InsignConfigurationPaid) GetUsername() string`

GetUsername returns the Username field if non-nil, zero value otherwise.

### GetUsernameOk

`func (o *InsignConfigurationPaid) GetUsernameOk() (*string, bool)`

GetUsernameOk returns a tuple with the Username field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsername

`func (o *InsignConfigurationPaid) SetUsername(v string)`

SetUsername sets Username field to given value.


### GetApiUrl

`func (o *InsignConfigurationPaid) GetApiUrl() string`

GetApiUrl returns the ApiUrl field if non-nil, zero value otherwise.

### GetApiUrlOk

`func (o *InsignConfigurationPaid) GetApiUrlOk() (*string, bool)`

GetApiUrlOk returns a tuple with the ApiUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetApiUrl

`func (o *InsignConfigurationPaid) SetApiUrl(v string)`

SetApiUrl sets ApiUrl field to given value.


### GetWebUrl

`func (o *InsignConfigurationPaid) GetWebUrl() string`

GetWebUrl returns the WebUrl field if non-nil, zero value otherwise.

### GetWebUrlOk

`func (o *InsignConfigurationPaid) GetWebUrlOk() (*string, bool)`

GetWebUrlOk returns a tuple with the WebUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWebUrl

`func (o *InsignConfigurationPaid) SetWebUrl(v string)`

SetWebUrl sets WebUrl field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


