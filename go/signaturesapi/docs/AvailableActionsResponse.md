# AvailableActionsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Insign** | **[]string** |  | 
**Custom** | **[]string** |  | 

## Methods

### NewAvailableActionsResponse

`func NewAvailableActionsResponse(insign []string, custom []string, ) *AvailableActionsResponse`

NewAvailableActionsResponse instantiates a new AvailableActionsResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAvailableActionsResponseWithDefaults

`func NewAvailableActionsResponseWithDefaults() *AvailableActionsResponse`

NewAvailableActionsResponseWithDefaults instantiates a new AvailableActionsResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInsign

`func (o *AvailableActionsResponse) GetInsign() []string`

GetInsign returns the Insign field if non-nil, zero value otherwise.

### GetInsignOk

`func (o *AvailableActionsResponse) GetInsignOk() (*[]string, bool)`

GetInsignOk returns a tuple with the Insign field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInsign

`func (o *AvailableActionsResponse) SetInsign(v []string)`

SetInsign sets Insign field to given value.


### GetCustom

`func (o *AvailableActionsResponse) GetCustom() []string`

GetCustom returns the Custom field if non-nil, zero value otherwise.

### GetCustomOk

`func (o *AvailableActionsResponse) GetCustomOk() (*[]string, bool)`

GetCustomOk returns a tuple with the Custom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCustom

`func (o *AvailableActionsResponse) SetCustom(v []string)`

SetCustom sets Custom field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


