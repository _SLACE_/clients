# SignatureResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SignLink** | **string** |  | 

## Methods

### NewSignatureResponse

`func NewSignatureResponse(signLink string, ) *SignatureResponse`

NewSignatureResponse instantiates a new SignatureResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSignatureResponseWithDefaults

`func NewSignatureResponseWithDefaults() *SignatureResponse`

NewSignatureResponseWithDefaults instantiates a new SignatureResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSignLink

`func (o *SignatureResponse) GetSignLink() string`

GetSignLink returns the SignLink field if non-nil, zero value otherwise.

### GetSignLinkOk

`func (o *SignatureResponse) GetSignLinkOk() (*string, bool)`

GetSignLinkOk returns a tuple with the SignLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSignLink

`func (o *SignatureResponse) SetSignLink(v string)`

SetSignLink sets SignLink field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


