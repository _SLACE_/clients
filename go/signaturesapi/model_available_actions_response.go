/*
SIGNATURE API

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package signaturesapi

import (
	"encoding/json"
)

// checks if the AvailableActionsResponse type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &AvailableActionsResponse{}

// AvailableActionsResponse struct for AvailableActionsResponse
type AvailableActionsResponse struct {
	Insign []string `json:"insign"`
	Custom []string `json:"custom"`
}

// NewAvailableActionsResponse instantiates a new AvailableActionsResponse object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAvailableActionsResponse(insign []string, custom []string) *AvailableActionsResponse {
	this := AvailableActionsResponse{}
	this.Insign = insign
	this.Custom = custom
	return &this
}

// NewAvailableActionsResponseWithDefaults instantiates a new AvailableActionsResponse object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAvailableActionsResponseWithDefaults() *AvailableActionsResponse {
	this := AvailableActionsResponse{}
	return &this
}

// GetInsign returns the Insign field value
func (o *AvailableActionsResponse) GetInsign() []string {
	if o == nil {
		var ret []string
		return ret
	}

	return o.Insign
}

// GetInsignOk returns a tuple with the Insign field value
// and a boolean to check if the value has been set.
func (o *AvailableActionsResponse) GetInsignOk() ([]string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Insign, true
}

// SetInsign sets field value
func (o *AvailableActionsResponse) SetInsign(v []string) {
	o.Insign = v
}

// GetCustom returns the Custom field value
func (o *AvailableActionsResponse) GetCustom() []string {
	if o == nil {
		var ret []string
		return ret
	}

	return o.Custom
}

// GetCustomOk returns a tuple with the Custom field value
// and a boolean to check if the value has been set.
func (o *AvailableActionsResponse) GetCustomOk() ([]string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Custom, true
}

// SetCustom sets field value
func (o *AvailableActionsResponse) SetCustom(v []string) {
	o.Custom = v
}

func (o AvailableActionsResponse) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o AvailableActionsResponse) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["insign"] = o.Insign
	toSerialize["custom"] = o.Custom
	return toSerialize, nil
}

type NullableAvailableActionsResponse struct {
	value *AvailableActionsResponse
	isSet bool
}

func (v NullableAvailableActionsResponse) Get() *AvailableActionsResponse {
	return v.value
}

func (v *NullableAvailableActionsResponse) Set(val *AvailableActionsResponse) {
	v.value = val
	v.isSet = true
}

func (v NullableAvailableActionsResponse) IsSet() bool {
	return v.isSet
}

func (v *NullableAvailableActionsResponse) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAvailableActionsResponse(val *AvailableActionsResponse) *NullableAvailableActionsResponse {
	return &NullableAvailableActionsResponse{value: val, isSet: true}
}

func (v NullableAvailableActionsResponse) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAvailableActionsResponse) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


