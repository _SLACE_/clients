/*
Rewards API

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package rewardsapi

import (
	"encoding/json"
)

// checks if the CouponData type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &CouponData{}

// CouponData struct for CouponData
type CouponData struct {
	Name string `json:"name"`
	Type string `json:"type"`
	ContactId string `json:"contact_id"`
	TransactionId string `json:"transaction_id"`
	ConversationId string `json:"conversation_id"`
	ChannelId string `json:"channel_id"`
	OrganizationId string `json:"organization_id"`
	InteractionId string `json:"interaction_id"`
}

// NewCouponData instantiates a new CouponData object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCouponData(name string, type_ string, contactId string, transactionId string, conversationId string, channelId string, organizationId string, interactionId string) *CouponData {
	this := CouponData{}
	this.Name = name
	this.Type = type_
	this.ContactId = contactId
	this.TransactionId = transactionId
	this.ConversationId = conversationId
	this.ChannelId = channelId
	this.OrganizationId = organizationId
	this.InteractionId = interactionId
	return &this
}

// NewCouponDataWithDefaults instantiates a new CouponData object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCouponDataWithDefaults() *CouponData {
	this := CouponData{}
	return &this
}

// GetName returns the Name field value
func (o *CouponData) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *CouponData) GetNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *CouponData) SetName(v string) {
	o.Name = v
}

// GetType returns the Type field value
func (o *CouponData) GetType() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Type
}

// GetTypeOk returns a tuple with the Type field value
// and a boolean to check if the value has been set.
func (o *CouponData) GetTypeOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Type, true
}

// SetType sets field value
func (o *CouponData) SetType(v string) {
	o.Type = v
}

// GetContactId returns the ContactId field value
func (o *CouponData) GetContactId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ContactId
}

// GetContactIdOk returns a tuple with the ContactId field value
// and a boolean to check if the value has been set.
func (o *CouponData) GetContactIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ContactId, true
}

// SetContactId sets field value
func (o *CouponData) SetContactId(v string) {
	o.ContactId = v
}

// GetTransactionId returns the TransactionId field value
func (o *CouponData) GetTransactionId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.TransactionId
}

// GetTransactionIdOk returns a tuple with the TransactionId field value
// and a boolean to check if the value has been set.
func (o *CouponData) GetTransactionIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.TransactionId, true
}

// SetTransactionId sets field value
func (o *CouponData) SetTransactionId(v string) {
	o.TransactionId = v
}

// GetConversationId returns the ConversationId field value
func (o *CouponData) GetConversationId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ConversationId
}

// GetConversationIdOk returns a tuple with the ConversationId field value
// and a boolean to check if the value has been set.
func (o *CouponData) GetConversationIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ConversationId, true
}

// SetConversationId sets field value
func (o *CouponData) SetConversationId(v string) {
	o.ConversationId = v
}

// GetChannelId returns the ChannelId field value
func (o *CouponData) GetChannelId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ChannelId
}

// GetChannelIdOk returns a tuple with the ChannelId field value
// and a boolean to check if the value has been set.
func (o *CouponData) GetChannelIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ChannelId, true
}

// SetChannelId sets field value
func (o *CouponData) SetChannelId(v string) {
	o.ChannelId = v
}

// GetOrganizationId returns the OrganizationId field value
func (o *CouponData) GetOrganizationId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.OrganizationId
}

// GetOrganizationIdOk returns a tuple with the OrganizationId field value
// and a boolean to check if the value has been set.
func (o *CouponData) GetOrganizationIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.OrganizationId, true
}

// SetOrganizationId sets field value
func (o *CouponData) SetOrganizationId(v string) {
	o.OrganizationId = v
}

// GetInteractionId returns the InteractionId field value
func (o *CouponData) GetInteractionId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.InteractionId
}

// GetInteractionIdOk returns a tuple with the InteractionId field value
// and a boolean to check if the value has been set.
func (o *CouponData) GetInteractionIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.InteractionId, true
}

// SetInteractionId sets field value
func (o *CouponData) SetInteractionId(v string) {
	o.InteractionId = v
}

func (o CouponData) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o CouponData) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["name"] = o.Name
	toSerialize["type"] = o.Type
	toSerialize["contact_id"] = o.ContactId
	toSerialize["transaction_id"] = o.TransactionId
	toSerialize["conversation_id"] = o.ConversationId
	toSerialize["channel_id"] = o.ChannelId
	toSerialize["organization_id"] = o.OrganizationId
	toSerialize["interaction_id"] = o.InteractionId
	return toSerialize, nil
}

type NullableCouponData struct {
	value *CouponData
	isSet bool
}

func (v NullableCouponData) Get() *CouponData {
	return v.value
}

func (v *NullableCouponData) Set(val *CouponData) {
	v.value = val
	v.isSet = true
}

func (v NullableCouponData) IsSet() bool {
	return v.isSet
}

func (v *NullableCouponData) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCouponData(val *CouponData) *NullableCouponData {
	return &NullableCouponData{value: val, isSet: true}
}

func (v NullableCouponData) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCouponData) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


