/*
Rewards API

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package rewardsapi

import (
	"encoding/json"
	"time"
)

// checks if the InstantWinConfiguration type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &InstantWinConfiguration{}

// InstantWinConfiguration struct for InstantWinConfiguration
type InstantWinConfiguration struct {
	Id string `json:"id"`
	Name string `json:"name"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Prizes []InstantWinConfigurationPrizesInner `json:"prizes"`
	Type string `json:"type"`
	UsageCounter int32 `json:"usage_counter"`
}

// NewInstantWinConfiguration instantiates a new InstantWinConfiguration object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewInstantWinConfiguration(id string, name string, createdAt time.Time, updatedAt time.Time, prizes []InstantWinConfigurationPrizesInner, type_ string, usageCounter int32) *InstantWinConfiguration {
	this := InstantWinConfiguration{}
	this.Id = id
	this.Name = name
	this.CreatedAt = createdAt
	this.UpdatedAt = updatedAt
	this.Prizes = prizes
	this.Type = type_
	this.UsageCounter = usageCounter
	return &this
}

// NewInstantWinConfigurationWithDefaults instantiates a new InstantWinConfiguration object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewInstantWinConfigurationWithDefaults() *InstantWinConfiguration {
	this := InstantWinConfiguration{}
	return &this
}

// GetId returns the Id field value
func (o *InstantWinConfiguration) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *InstantWinConfiguration) GetIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *InstantWinConfiguration) SetId(v string) {
	o.Id = v
}

// GetName returns the Name field value
func (o *InstantWinConfiguration) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *InstantWinConfiguration) GetNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *InstantWinConfiguration) SetName(v string) {
	o.Name = v
}

// GetCreatedAt returns the CreatedAt field value
func (o *InstantWinConfiguration) GetCreatedAt() time.Time {
	if o == nil {
		var ret time.Time
		return ret
	}

	return o.CreatedAt
}

// GetCreatedAtOk returns a tuple with the CreatedAt field value
// and a boolean to check if the value has been set.
func (o *InstantWinConfiguration) GetCreatedAtOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return &o.CreatedAt, true
}

// SetCreatedAt sets field value
func (o *InstantWinConfiguration) SetCreatedAt(v time.Time) {
	o.CreatedAt = v
}

// GetUpdatedAt returns the UpdatedAt field value
func (o *InstantWinConfiguration) GetUpdatedAt() time.Time {
	if o == nil {
		var ret time.Time
		return ret
	}

	return o.UpdatedAt
}

// GetUpdatedAtOk returns a tuple with the UpdatedAt field value
// and a boolean to check if the value has been set.
func (o *InstantWinConfiguration) GetUpdatedAtOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return &o.UpdatedAt, true
}

// SetUpdatedAt sets field value
func (o *InstantWinConfiguration) SetUpdatedAt(v time.Time) {
	o.UpdatedAt = v
}

// GetPrizes returns the Prizes field value
func (o *InstantWinConfiguration) GetPrizes() []InstantWinConfigurationPrizesInner {
	if o == nil {
		var ret []InstantWinConfigurationPrizesInner
		return ret
	}

	return o.Prizes
}

// GetPrizesOk returns a tuple with the Prizes field value
// and a boolean to check if the value has been set.
func (o *InstantWinConfiguration) GetPrizesOk() ([]InstantWinConfigurationPrizesInner, bool) {
	if o == nil {
		return nil, false
	}
	return o.Prizes, true
}

// SetPrizes sets field value
func (o *InstantWinConfiguration) SetPrizes(v []InstantWinConfigurationPrizesInner) {
	o.Prizes = v
}

// GetType returns the Type field value
func (o *InstantWinConfiguration) GetType() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Type
}

// GetTypeOk returns a tuple with the Type field value
// and a boolean to check if the value has been set.
func (o *InstantWinConfiguration) GetTypeOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Type, true
}

// SetType sets field value
func (o *InstantWinConfiguration) SetType(v string) {
	o.Type = v
}

// GetUsageCounter returns the UsageCounter field value
func (o *InstantWinConfiguration) GetUsageCounter() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.UsageCounter
}

// GetUsageCounterOk returns a tuple with the UsageCounter field value
// and a boolean to check if the value has been set.
func (o *InstantWinConfiguration) GetUsageCounterOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.UsageCounter, true
}

// SetUsageCounter sets field value
func (o *InstantWinConfiguration) SetUsageCounter(v int32) {
	o.UsageCounter = v
}

func (o InstantWinConfiguration) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o InstantWinConfiguration) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["id"] = o.Id
	toSerialize["name"] = o.Name
	toSerialize["created_at"] = o.CreatedAt
	toSerialize["updated_at"] = o.UpdatedAt
	toSerialize["prizes"] = o.Prizes
	toSerialize["type"] = o.Type
	toSerialize["usage_counter"] = o.UsageCounter
	return toSerialize, nil
}

type NullableInstantWinConfiguration struct {
	value *InstantWinConfiguration
	isSet bool
}

func (v NullableInstantWinConfiguration) Get() *InstantWinConfiguration {
	return v.value
}

func (v *NullableInstantWinConfiguration) Set(val *InstantWinConfiguration) {
	v.value = val
	v.isSet = true
}

func (v NullableInstantWinConfiguration) IsSet() bool {
	return v.isSet
}

func (v *NullableInstantWinConfiguration) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableInstantWinConfiguration(val *InstantWinConfiguration) *NullableInstantWinConfiguration {
	return &NullableInstantWinConfiguration{value: val, isSet: true}
}

func (v NullableInstantWinConfiguration) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableInstantWinConfiguration) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


