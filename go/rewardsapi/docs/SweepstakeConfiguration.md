# SweepstakeConfiguration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [readonly] 
**Name** | **string** |  | 
**Prizes** | [**[]SweepstakePrize**](SweepstakePrize.md) |  | 
**StartAt** | Pointer to **time.Time** |  | [optional] 
**StartWhenRegistered** | Pointer to **int32** |  | [optional] 
**ContactMaxPrizes** | **int32** |  | 
**Status** | **string** |  | [readonly] 
**CreatedAt** | **time.Time** |  | [readonly] 
**UpdatedAt** | **time.Time** |  | [readonly] 
**Type** | **string** |  | 
**EventName** | **string** |  | 
**UsageCounter** | **int32** |  | [readonly] 

## Methods

### NewSweepstakeConfiguration

`func NewSweepstakeConfiguration(id string, name string, prizes []SweepstakePrize, contactMaxPrizes int32, status string, createdAt time.Time, updatedAt time.Time, type_ string, eventName string, usageCounter int32, ) *SweepstakeConfiguration`

NewSweepstakeConfiguration instantiates a new SweepstakeConfiguration object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSweepstakeConfigurationWithDefaults

`func NewSweepstakeConfigurationWithDefaults() *SweepstakeConfiguration`

NewSweepstakeConfigurationWithDefaults instantiates a new SweepstakeConfiguration object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *SweepstakeConfiguration) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *SweepstakeConfiguration) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *SweepstakeConfiguration) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *SweepstakeConfiguration) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *SweepstakeConfiguration) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *SweepstakeConfiguration) SetName(v string)`

SetName sets Name field to given value.


### GetPrizes

`func (o *SweepstakeConfiguration) GetPrizes() []SweepstakePrize`

GetPrizes returns the Prizes field if non-nil, zero value otherwise.

### GetPrizesOk

`func (o *SweepstakeConfiguration) GetPrizesOk() (*[]SweepstakePrize, bool)`

GetPrizesOk returns a tuple with the Prizes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrizes

`func (o *SweepstakeConfiguration) SetPrizes(v []SweepstakePrize)`

SetPrizes sets Prizes field to given value.


### GetStartAt

`func (o *SweepstakeConfiguration) GetStartAt() time.Time`

GetStartAt returns the StartAt field if non-nil, zero value otherwise.

### GetStartAtOk

`func (o *SweepstakeConfiguration) GetStartAtOk() (*time.Time, bool)`

GetStartAtOk returns a tuple with the StartAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartAt

`func (o *SweepstakeConfiguration) SetStartAt(v time.Time)`

SetStartAt sets StartAt field to given value.

### HasStartAt

`func (o *SweepstakeConfiguration) HasStartAt() bool`

HasStartAt returns a boolean if a field has been set.

### GetStartWhenRegistered

`func (o *SweepstakeConfiguration) GetStartWhenRegistered() int32`

GetStartWhenRegistered returns the StartWhenRegistered field if non-nil, zero value otherwise.

### GetStartWhenRegisteredOk

`func (o *SweepstakeConfiguration) GetStartWhenRegisteredOk() (*int32, bool)`

GetStartWhenRegisteredOk returns a tuple with the StartWhenRegistered field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartWhenRegistered

`func (o *SweepstakeConfiguration) SetStartWhenRegistered(v int32)`

SetStartWhenRegistered sets StartWhenRegistered field to given value.

### HasStartWhenRegistered

`func (o *SweepstakeConfiguration) HasStartWhenRegistered() bool`

HasStartWhenRegistered returns a boolean if a field has been set.

### GetContactMaxPrizes

`func (o *SweepstakeConfiguration) GetContactMaxPrizes() int32`

GetContactMaxPrizes returns the ContactMaxPrizes field if non-nil, zero value otherwise.

### GetContactMaxPrizesOk

`func (o *SweepstakeConfiguration) GetContactMaxPrizesOk() (*int32, bool)`

GetContactMaxPrizesOk returns a tuple with the ContactMaxPrizes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactMaxPrizes

`func (o *SweepstakeConfiguration) SetContactMaxPrizes(v int32)`

SetContactMaxPrizes sets ContactMaxPrizes field to given value.


### GetStatus

`func (o *SweepstakeConfiguration) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *SweepstakeConfiguration) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *SweepstakeConfiguration) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetCreatedAt

`func (o *SweepstakeConfiguration) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *SweepstakeConfiguration) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *SweepstakeConfiguration) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *SweepstakeConfiguration) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *SweepstakeConfiguration) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *SweepstakeConfiguration) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetType

`func (o *SweepstakeConfiguration) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *SweepstakeConfiguration) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *SweepstakeConfiguration) SetType(v string)`

SetType sets Type field to given value.


### GetEventName

`func (o *SweepstakeConfiguration) GetEventName() string`

GetEventName returns the EventName field if non-nil, zero value otherwise.

### GetEventNameOk

`func (o *SweepstakeConfiguration) GetEventNameOk() (*string, bool)`

GetEventNameOk returns a tuple with the EventName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventName

`func (o *SweepstakeConfiguration) SetEventName(v string)`

SetEventName sets EventName field to given value.


### GetUsageCounter

`func (o *SweepstakeConfiguration) GetUsageCounter() int32`

GetUsageCounter returns the UsageCounter field if non-nil, zero value otherwise.

### GetUsageCounterOk

`func (o *SweepstakeConfiguration) GetUsageCounterOk() (*int32, bool)`

GetUsageCounterOk returns a tuple with the UsageCounter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsageCounter

`func (o *SweepstakeConfiguration) SetUsageCounter(v int32)`

SetUsageCounter sets UsageCounter field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


