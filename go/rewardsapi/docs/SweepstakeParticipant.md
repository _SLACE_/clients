# SweepstakeParticipant

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [readonly] 
**Name** | **string** |  | 
**TransactionId** | **string** |  | 
**ContactId** | **string** |  | 
**InteractionId** | **string** |  | 
**WonPrizes** | **[]string** |  | [readonly] 
**ChannelId** | **string** |  | 
**ConversationId** | **string** |  | 
**CreatedAt** | **string** |  | [readonly] 

## Methods

### NewSweepstakeParticipant

`func NewSweepstakeParticipant(id string, name string, transactionId string, contactId string, interactionId string, wonPrizes []string, channelId string, conversationId string, createdAt string, ) *SweepstakeParticipant`

NewSweepstakeParticipant instantiates a new SweepstakeParticipant object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSweepstakeParticipantWithDefaults

`func NewSweepstakeParticipantWithDefaults() *SweepstakeParticipant`

NewSweepstakeParticipantWithDefaults instantiates a new SweepstakeParticipant object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *SweepstakeParticipant) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *SweepstakeParticipant) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *SweepstakeParticipant) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *SweepstakeParticipant) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *SweepstakeParticipant) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *SweepstakeParticipant) SetName(v string)`

SetName sets Name field to given value.


### GetTransactionId

`func (o *SweepstakeParticipant) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *SweepstakeParticipant) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *SweepstakeParticipant) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetContactId

`func (o *SweepstakeParticipant) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *SweepstakeParticipant) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *SweepstakeParticipant) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetInteractionId

`func (o *SweepstakeParticipant) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *SweepstakeParticipant) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *SweepstakeParticipant) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.


### GetWonPrizes

`func (o *SweepstakeParticipant) GetWonPrizes() []string`

GetWonPrizes returns the WonPrizes field if non-nil, zero value otherwise.

### GetWonPrizesOk

`func (o *SweepstakeParticipant) GetWonPrizesOk() (*[]string, bool)`

GetWonPrizesOk returns a tuple with the WonPrizes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWonPrizes

`func (o *SweepstakeParticipant) SetWonPrizes(v []string)`

SetWonPrizes sets WonPrizes field to given value.


### GetChannelId

`func (o *SweepstakeParticipant) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *SweepstakeParticipant) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *SweepstakeParticipant) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetConversationId

`func (o *SweepstakeParticipant) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *SweepstakeParticipant) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *SweepstakeParticipant) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetCreatedAt

`func (o *SweepstakeParticipant) GetCreatedAt() string`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *SweepstakeParticipant) GetCreatedAtOk() (*string, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *SweepstakeParticipant) SetCreatedAt(v string)`

SetCreatedAt sets CreatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


