# \DnAPI

All URIs are relative to *https://rewards.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateDNConfiguration**](DnAPI.md#CreateDNConfiguration) | **Post** /integrations/dn/organizations/{oid}/configurations | Create DN configuration
[**DeleteDNConfiguration**](DnAPI.md#DeleteDNConfiguration) | **Delete** /integrations/dn/organizations/{oid}/configurations | Delete DN configuration
[**GetDNConfiguration**](DnAPI.md#GetDNConfiguration) | **Get** /integrations/dn/organizations/{oid}/configurations | Get DN configuration



## CreateDNConfiguration

> DNConfiguration CreateDNConfiguration(ctx, oid).CreateDNConfigRequest(createDNConfigRequest).Execute()

Create DN configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    oid := "oid_example" // string | Organization ID
    createDNConfigRequest := *openapiclient.NewCreateDNConfigRequest("ClientId_example", "ClientSecret_example", "ApiUrl_example") // CreateDNConfigRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DnAPI.CreateDNConfiguration(context.Background(), oid).CreateDNConfigRequest(createDNConfigRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DnAPI.CreateDNConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateDNConfiguration`: DNConfiguration
    fmt.Fprintf(os.Stdout, "Response from `DnAPI.CreateDNConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** | Organization ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateDNConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **createDNConfigRequest** | [**CreateDNConfigRequest**](CreateDNConfigRequest.md) |  | 

### Return type

[**DNConfiguration**](DNConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteDNConfiguration

> DeleteDNConfiguration(ctx, oid).Execute()

Delete DN configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    oid := "oid_example" // string | Organization ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.DnAPI.DeleteDNConfiguration(context.Background(), oid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DnAPI.DeleteDNConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** | Organization ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteDNConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDNConfiguration

> DNConfiguration GetDNConfiguration(ctx, oid).Execute()

Get DN configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    oid := "oid_example" // string | Organization ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DnAPI.GetDNConfiguration(context.Background(), oid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DnAPI.GetDNConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetDNConfiguration`: DNConfiguration
    fmt.Fprintf(os.Stdout, "Response from `DnAPI.GetDNConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** | Organization ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetDNConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**DNConfiguration**](DNConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

