# CouponingConfigDtoMetricsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Count** | **int32** |  | 
**Status** | **int32** |  | 

## Methods

### NewCouponingConfigDtoMetricsInner

`func NewCouponingConfigDtoMetricsInner(count int32, status int32, ) *CouponingConfigDtoMetricsInner`

NewCouponingConfigDtoMetricsInner instantiates a new CouponingConfigDtoMetricsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCouponingConfigDtoMetricsInnerWithDefaults

`func NewCouponingConfigDtoMetricsInnerWithDefaults() *CouponingConfigDtoMetricsInner`

NewCouponingConfigDtoMetricsInnerWithDefaults instantiates a new CouponingConfigDtoMetricsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCount

`func (o *CouponingConfigDtoMetricsInner) GetCount() int32`

GetCount returns the Count field if non-nil, zero value otherwise.

### GetCountOk

`func (o *CouponingConfigDtoMetricsInner) GetCountOk() (*int32, bool)`

GetCountOk returns a tuple with the Count field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCount

`func (o *CouponingConfigDtoMetricsInner) SetCount(v int32)`

SetCount sets Count field to given value.


### GetStatus

`func (o *CouponingConfigDtoMetricsInner) GetStatus() int32`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *CouponingConfigDtoMetricsInner) GetStatusOk() (*int32, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *CouponingConfigDtoMetricsInner) SetStatus(v int32)`

SetStatus sets Status field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


