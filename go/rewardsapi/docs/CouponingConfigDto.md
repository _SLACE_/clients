# CouponingConfigDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [readonly] 
**Name** | **string** |  | 
**Description** | Pointer to **string** |  | [optional] 
**OrganizationId** | **string** |  | 
**CodeType** | **int32** |  | 
**GenericCode** | Pointer to **string** |  | [optional] 
**StartAt** | Pointer to **time.Time** |  | [optional] 
**EndAt** | Pointer to **time.Time** |  | [optional] 
**MaxRedemptions** | Pointer to **int32** |  | [optional] 
**ExternalName** | Pointer to **string** |  | [optional] 
**CouponExpiresAfter** | Pointer to **int32** | minutes | [optional] 
**Metrics** | [**[]CouponingConfigDtoMetricsInner**](CouponingConfigDtoMetricsInner.md) |  | [readonly] 
**ImportMetadata** | [**[]CouponingConfigDtoImportMetadataInner**](CouponingConfigDtoImportMetadataInner.md) |  | [readonly] 

## Methods

### NewCouponingConfigDto

`func NewCouponingConfigDto(id string, name string, organizationId string, codeType int32, metrics []CouponingConfigDtoMetricsInner, importMetadata []CouponingConfigDtoImportMetadataInner, ) *CouponingConfigDto`

NewCouponingConfigDto instantiates a new CouponingConfigDto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCouponingConfigDtoWithDefaults

`func NewCouponingConfigDtoWithDefaults() *CouponingConfigDto`

NewCouponingConfigDtoWithDefaults instantiates a new CouponingConfigDto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CouponingConfigDto) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CouponingConfigDto) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CouponingConfigDto) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *CouponingConfigDto) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *CouponingConfigDto) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *CouponingConfigDto) SetName(v string)`

SetName sets Name field to given value.


### GetDescription

`func (o *CouponingConfigDto) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *CouponingConfigDto) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *CouponingConfigDto) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *CouponingConfigDto) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetOrganizationId

`func (o *CouponingConfigDto) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *CouponingConfigDto) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *CouponingConfigDto) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetCodeType

`func (o *CouponingConfigDto) GetCodeType() int32`

GetCodeType returns the CodeType field if non-nil, zero value otherwise.

### GetCodeTypeOk

`func (o *CouponingConfigDto) GetCodeTypeOk() (*int32, bool)`

GetCodeTypeOk returns a tuple with the CodeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCodeType

`func (o *CouponingConfigDto) SetCodeType(v int32)`

SetCodeType sets CodeType field to given value.


### GetGenericCode

`func (o *CouponingConfigDto) GetGenericCode() string`

GetGenericCode returns the GenericCode field if non-nil, zero value otherwise.

### GetGenericCodeOk

`func (o *CouponingConfigDto) GetGenericCodeOk() (*string, bool)`

GetGenericCodeOk returns a tuple with the GenericCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGenericCode

`func (o *CouponingConfigDto) SetGenericCode(v string)`

SetGenericCode sets GenericCode field to given value.

### HasGenericCode

`func (o *CouponingConfigDto) HasGenericCode() bool`

HasGenericCode returns a boolean if a field has been set.

### GetStartAt

`func (o *CouponingConfigDto) GetStartAt() time.Time`

GetStartAt returns the StartAt field if non-nil, zero value otherwise.

### GetStartAtOk

`func (o *CouponingConfigDto) GetStartAtOk() (*time.Time, bool)`

GetStartAtOk returns a tuple with the StartAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartAt

`func (o *CouponingConfigDto) SetStartAt(v time.Time)`

SetStartAt sets StartAt field to given value.

### HasStartAt

`func (o *CouponingConfigDto) HasStartAt() bool`

HasStartAt returns a boolean if a field has been set.

### GetEndAt

`func (o *CouponingConfigDto) GetEndAt() time.Time`

GetEndAt returns the EndAt field if non-nil, zero value otherwise.

### GetEndAtOk

`func (o *CouponingConfigDto) GetEndAtOk() (*time.Time, bool)`

GetEndAtOk returns a tuple with the EndAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEndAt

`func (o *CouponingConfigDto) SetEndAt(v time.Time)`

SetEndAt sets EndAt field to given value.

### HasEndAt

`func (o *CouponingConfigDto) HasEndAt() bool`

HasEndAt returns a boolean if a field has been set.

### GetMaxRedemptions

`func (o *CouponingConfigDto) GetMaxRedemptions() int32`

GetMaxRedemptions returns the MaxRedemptions field if non-nil, zero value otherwise.

### GetMaxRedemptionsOk

`func (o *CouponingConfigDto) GetMaxRedemptionsOk() (*int32, bool)`

GetMaxRedemptionsOk returns a tuple with the MaxRedemptions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxRedemptions

`func (o *CouponingConfigDto) SetMaxRedemptions(v int32)`

SetMaxRedemptions sets MaxRedemptions field to given value.

### HasMaxRedemptions

`func (o *CouponingConfigDto) HasMaxRedemptions() bool`

HasMaxRedemptions returns a boolean if a field has been set.

### GetExternalName

`func (o *CouponingConfigDto) GetExternalName() string`

GetExternalName returns the ExternalName field if non-nil, zero value otherwise.

### GetExternalNameOk

`func (o *CouponingConfigDto) GetExternalNameOk() (*string, bool)`

GetExternalNameOk returns a tuple with the ExternalName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalName

`func (o *CouponingConfigDto) SetExternalName(v string)`

SetExternalName sets ExternalName field to given value.

### HasExternalName

`func (o *CouponingConfigDto) HasExternalName() bool`

HasExternalName returns a boolean if a field has been set.

### GetCouponExpiresAfter

`func (o *CouponingConfigDto) GetCouponExpiresAfter() int32`

GetCouponExpiresAfter returns the CouponExpiresAfter field if non-nil, zero value otherwise.

### GetCouponExpiresAfterOk

`func (o *CouponingConfigDto) GetCouponExpiresAfterOk() (*int32, bool)`

GetCouponExpiresAfterOk returns a tuple with the CouponExpiresAfter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCouponExpiresAfter

`func (o *CouponingConfigDto) SetCouponExpiresAfter(v int32)`

SetCouponExpiresAfter sets CouponExpiresAfter field to given value.

### HasCouponExpiresAfter

`func (o *CouponingConfigDto) HasCouponExpiresAfter() bool`

HasCouponExpiresAfter returns a boolean if a field has been set.

### GetMetrics

`func (o *CouponingConfigDto) GetMetrics() []CouponingConfigDtoMetricsInner`

GetMetrics returns the Metrics field if non-nil, zero value otherwise.

### GetMetricsOk

`func (o *CouponingConfigDto) GetMetricsOk() (*[]CouponingConfigDtoMetricsInner, bool)`

GetMetricsOk returns a tuple with the Metrics field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetrics

`func (o *CouponingConfigDto) SetMetrics(v []CouponingConfigDtoMetricsInner)`

SetMetrics sets Metrics field to given value.


### GetImportMetadata

`func (o *CouponingConfigDto) GetImportMetadata() []CouponingConfigDtoImportMetadataInner`

GetImportMetadata returns the ImportMetadata field if non-nil, zero value otherwise.

### GetImportMetadataOk

`func (o *CouponingConfigDto) GetImportMetadataOk() (*[]CouponingConfigDtoImportMetadataInner, bool)`

GetImportMetadataOk returns a tuple with the ImportMetadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetImportMetadata

`func (o *CouponingConfigDto) SetImportMetadata(v []CouponingConfigDtoImportMetadataInner)`

SetImportMetadata sets ImportMetadata field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


