# \RewardsAPI

All URIs are relative to *https://rewards.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateCouponingConfig**](RewardsAPI.md#CreateCouponingConfig) | **Post** /promotions/couponing/configurations | Create couponing config
[**CreateInstantWinConfiguration**](RewardsAPI.md#CreateInstantWinConfiguration) | **Post** /promotions/instant-win/configurations | Create instant win configuration
[**CreateSweepstakeConfiguration**](RewardsAPI.md#CreateSweepstakeConfiguration) | **Post** /promotions/sweepstake/configurations | Create sweepstake configuration
[**DrawInstantWin**](RewardsAPI.md#DrawInstantWin) | **Post** /promotions/instant-win/draw | Draw instant win
[**ExportCouponsFromConfig**](RewardsAPI.md#ExportCouponsFromConfig) | **Get** /promotions/couponing/export/{id} | Export coupons from config
[**ImportCoupons**](RewardsAPI.md#ImportCoupons) | **Post** /promotions/couponing/import/{id} | Import coupons
[**ListCouponingConfigs**](RewardsAPI.md#ListCouponingConfigs) | **Get** /promotions/couponing/configurations | List couponing configs
[**PutPromotionsCouponingConfigurationsId**](RewardsAPI.md#PutPromotionsCouponingConfigurationsId) | **Put** /promotions/couponing/configurations/{id} | Update couponing config
[**RedeemCoupon**](RewardsAPI.md#RedeemCoupon) | **Post** /promotions/couponing/redeem/{code} | Redeem coupon
[**RegisterSweepstakeParticipant**](RewardsAPI.md#RegisterSweepstakeParticipant) | **Post** /promotions/sweepstake/register | Register sweepstake participant
[**ResetInstantWin**](RewardsAPI.md#ResetInstantWin) | **Post** /promotions/instant-win/reset | Reset instant win
[**RunSweepstake**](RewardsAPI.md#RunSweepstake) | **Post** /promotions/sweepstake/run | Run sweepstake
[**UpdateLoyaltyPoints**](RewardsAPI.md#UpdateLoyaltyPoints) | **Post** /promotions/loyalty/update | Update loyalty points



## CreateCouponingConfig

> CouponingConfigDto CreateCouponingConfig(ctx).CouponingConfigDto(couponingConfigDto).Execute()

Create couponing config

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    couponingConfigDto := *openapiclient.NewCouponingConfigDto("Id_example", "Name_example", "OrganizationId_example", int32(123), []openapiclient.CouponingConfigDtoMetricsInner{*openapiclient.NewCouponingConfigDtoMetricsInner(int32(123), int32(123))}, []openapiclient.CouponingConfigDtoImportMetadataInner{*openapiclient.NewCouponingConfigDtoImportMetadataInner("ConfigId_example", "UserId_example", int32(123), int32(123), int32(123))}) // CouponingConfigDto |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.RewardsAPI.CreateCouponingConfig(context.Background()).CouponingConfigDto(couponingConfigDto).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.CreateCouponingConfig``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateCouponingConfig`: CouponingConfigDto
    fmt.Fprintf(os.Stdout, "Response from `RewardsAPI.CreateCouponingConfig`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateCouponingConfigRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **couponingConfigDto** | [**CouponingConfigDto**](CouponingConfigDto.md) |  | 

### Return type

[**CouponingConfigDto**](CouponingConfigDto.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateInstantWinConfiguration

> InstantWinConfiguration CreateInstantWinConfiguration(ctx).InstantWinConfiguration(instantWinConfiguration).Execute()

Create instant win configuration

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    instantWinConfiguration := *openapiclient.NewInstantWinConfiguration("Id_example", "Name_example", time.Now(), time.Now(), []openapiclient.InstantWinConfigurationPrizesInner{*openapiclient.NewInstantWinConfigurationPrizesInner(int32(123), "Name_example", int32(123), int32(123))}, "Type_example", int32(123)) // InstantWinConfiguration |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.RewardsAPI.CreateInstantWinConfiguration(context.Background()).InstantWinConfiguration(instantWinConfiguration).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.CreateInstantWinConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateInstantWinConfiguration`: InstantWinConfiguration
    fmt.Fprintf(os.Stdout, "Response from `RewardsAPI.CreateInstantWinConfiguration`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateInstantWinConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **instantWinConfiguration** | [**InstantWinConfiguration**](InstantWinConfiguration.md) |  | 

### Return type

[**InstantWinConfiguration**](InstantWinConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateSweepstakeConfiguration

> SweepstakeConfiguration CreateSweepstakeConfiguration(ctx).SweepstakeConfiguration(sweepstakeConfiguration).Execute()

Create sweepstake configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    sweepstakeConfiguration := *openapiclient.NewSweepstakeConfiguration("Id_example", "Name_example", []openapiclient.SweepstakePrize{*openapiclient.NewSweepstakePrize("Name_example", int32(123), "WinChanceType_example")}, int32(123), "Status_example", time.Now(), time.Now(), "Type_example", "EventName_example", int32(123)) // SweepstakeConfiguration |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.RewardsAPI.CreateSweepstakeConfiguration(context.Background()).SweepstakeConfiguration(sweepstakeConfiguration).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.CreateSweepstakeConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateSweepstakeConfiguration`: SweepstakeConfiguration
    fmt.Fprintf(os.Stdout, "Response from `RewardsAPI.CreateSweepstakeConfiguration`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateSweepstakeConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sweepstakeConfiguration** | [**SweepstakeConfiguration**](SweepstakeConfiguration.md) |  | 

### Return type

[**SweepstakeConfiguration**](SweepstakeConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DrawInstantWin

> InstantWin DrawInstantWin(ctx).DrawInstantWinRequest(drawInstantWinRequest).Execute()

Draw instant win

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    drawInstantWinRequest := *openapiclient.NewDrawInstantWinRequest("Name_example", "InteractionId_example", "TransactionId_example", "ContactId_example", "ChannelId_example", "ConversationId_example") // DrawInstantWinRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.RewardsAPI.DrawInstantWin(context.Background()).DrawInstantWinRequest(drawInstantWinRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.DrawInstantWin``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DrawInstantWin`: InstantWin
    fmt.Fprintf(os.Stdout, "Response from `RewardsAPI.DrawInstantWin`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiDrawInstantWinRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **drawInstantWinRequest** | [**DrawInstantWinRequest**](DrawInstantWinRequest.md) |  | 

### Return type

[**InstantWin**](InstantWin.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ExportCouponsFromConfig

> ExportCouponsFromConfig(ctx, id).Execute()

Export coupons from config

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    id := "id_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.RewardsAPI.ExportCouponsFromConfig(context.Background(), id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.ExportCouponsFromConfig``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiExportCouponsFromConfigRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ImportCoupons

> ImportCoupons(ctx, id).Update(update).File(file).Execute()

Import coupons

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    id := "id_example" // string | 
    update := "update_example" // string | true/false - whether coupons should be created or updated (e.g. redeemed) (optional)
    file := "file_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.RewardsAPI.ImportCoupons(context.Background(), id).Update(update).File(file).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.ImportCoupons``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiImportCouponsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **update** | **string** | true/false - whether coupons should be created or updated (e.g. redeemed) | 
 **file** | **string** |  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListCouponingConfigs

> CouponingConfigDto ListCouponingConfigs(ctx).OrganizationId(organizationId).Execute()

List couponing configs

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    organizationId := "organizationId_example" // string | optional filter (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.RewardsAPI.ListCouponingConfigs(context.Background()).OrganizationId(organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.ListCouponingConfigs``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListCouponingConfigs`: CouponingConfigDto
    fmt.Fprintf(os.Stdout, "Response from `RewardsAPI.ListCouponingConfigs`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiListCouponingConfigsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | **string** | optional filter | 

### Return type

[**CouponingConfigDto**](CouponingConfigDto.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PutPromotionsCouponingConfigurationsId

> CouponingConfigDto PutPromotionsCouponingConfigurationsId(ctx, id).CouponingConfigDto(couponingConfigDto).Execute()

Update couponing config

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    id := "id_example" // string | 
    couponingConfigDto := *openapiclient.NewCouponingConfigDto("Id_example", "Name_example", "OrganizationId_example", int32(123), []openapiclient.CouponingConfigDtoMetricsInner{*openapiclient.NewCouponingConfigDtoMetricsInner(int32(123), int32(123))}, []openapiclient.CouponingConfigDtoImportMetadataInner{*openapiclient.NewCouponingConfigDtoImportMetadataInner("ConfigId_example", "UserId_example", int32(123), int32(123), int32(123))}) // CouponingConfigDto |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.RewardsAPI.PutPromotionsCouponingConfigurationsId(context.Background(), id).CouponingConfigDto(couponingConfigDto).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.PutPromotionsCouponingConfigurationsId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PutPromotionsCouponingConfigurationsId`: CouponingConfigDto
    fmt.Fprintf(os.Stdout, "Response from `RewardsAPI.PutPromotionsCouponingConfigurationsId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPutPromotionsCouponingConfigurationsIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **couponingConfigDto** | [**CouponingConfigDto**](CouponingConfigDto.md) |  | 

### Return type

[**CouponingConfigDto**](CouponingConfigDto.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RedeemCoupon

> RedeemCoupon(ctx, code).CouponData(couponData).Execute()

Redeem coupon

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    code := "code_example" // string | 
    couponData := *openapiclient.NewCouponData("Name_example", "Type_example", "ContactId_example", "TransactionId_example", "ConversationId_example", "ChannelId_example", "OrganizationId_example", "InteractionId_example") // CouponData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.RewardsAPI.RedeemCoupon(context.Background(), code).CouponData(couponData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.RedeemCoupon``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**code** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiRedeemCouponRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **couponData** | [**CouponData**](CouponData.md) |  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RegisterSweepstakeParticipant

> RegisterSweepstakeParticipant(ctx).SweepstakeParticipant(sweepstakeParticipant).Execute()

Register sweepstake participant



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    sweepstakeParticipant := *openapiclient.NewSweepstakeParticipant("Id_example", "Name_example", "TransactionId_example", "ContactId_example", "InteractionId_example", []string{"WonPrizes_example"}, "ChannelId_example", "ConversationId_example", "CreatedAt_example") // SweepstakeParticipant |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.RewardsAPI.RegisterSweepstakeParticipant(context.Background()).SweepstakeParticipant(sweepstakeParticipant).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.RegisterSweepstakeParticipant``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiRegisterSweepstakeParticipantRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sweepstakeParticipant** | [**SweepstakeParticipant**](SweepstakeParticipant.md) |  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ResetInstantWin

> ResetInstantWin(ctx).ResetInstantWinRequest(resetInstantWinRequest).Execute()

Reset instant win

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    resetInstantWinRequest := *openapiclient.NewResetInstantWinRequest("Name_example") // ResetInstantWinRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.RewardsAPI.ResetInstantWin(context.Background()).ResetInstantWinRequest(resetInstantWinRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.ResetInstantWin``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiResetInstantWinRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resetInstantWinRequest** | [**ResetInstantWinRequest**](ResetInstantWinRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RunSweepstake

> RunSweepstake(ctx).RunSweepstakeRequest(runSweepstakeRequest).Execute()

Run sweepstake



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    runSweepstakeRequest := *openapiclient.NewRunSweepstakeRequest() // RunSweepstakeRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.RewardsAPI.RunSweepstake(context.Background()).RunSweepstakeRequest(runSweepstakeRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.RunSweepstake``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiRunSweepstakeRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **runSweepstakeRequest** | [**RunSweepstakeRequest**](RunSweepstakeRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateLoyaltyPoints

> LoyaltyPoints UpdateLoyaltyPoints(ctx).LoyaltyPointsChange(loyaltyPointsChange).Execute()

Update loyalty points



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/rewardsapi"
)

func main() {
    loyaltyPointsChange := *openapiclient.NewLoyaltyPointsChange("Name_example", int32(123), "ChangeType_example", "ContactId_example", "ChannelId_example", "OrganizationId_example", "ConversationId_example") // LoyaltyPointsChange |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.RewardsAPI.UpdateLoyaltyPoints(context.Background()).LoyaltyPointsChange(loyaltyPointsChange).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `RewardsAPI.UpdateLoyaltyPoints``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateLoyaltyPoints`: LoyaltyPoints
    fmt.Fprintf(os.Stdout, "Response from `RewardsAPI.UpdateLoyaltyPoints`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiUpdateLoyaltyPointsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loyaltyPointsChange** | [**LoyaltyPointsChange**](LoyaltyPointsChange.md) |  | 

### Return type

[**LoyaltyPoints**](LoyaltyPoints.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

