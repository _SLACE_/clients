# CreateLoyaltyWebhook

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TransactionId** | **string** |  | 
**ConversationId** | **string** |  | 
**ContactId** | **string** |  | 
**LoyaltyId** | **string** |  | 
**ChannelId** | **string** |  | 

## Methods

### NewCreateLoyaltyWebhook

`func NewCreateLoyaltyWebhook(transactionId string, conversationId string, contactId string, loyaltyId string, channelId string, ) *CreateLoyaltyWebhook`

NewCreateLoyaltyWebhook instantiates a new CreateLoyaltyWebhook object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateLoyaltyWebhookWithDefaults

`func NewCreateLoyaltyWebhookWithDefaults() *CreateLoyaltyWebhook`

NewCreateLoyaltyWebhookWithDefaults instantiates a new CreateLoyaltyWebhook object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTransactionId

`func (o *CreateLoyaltyWebhook) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *CreateLoyaltyWebhook) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *CreateLoyaltyWebhook) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetConversationId

`func (o *CreateLoyaltyWebhook) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *CreateLoyaltyWebhook) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *CreateLoyaltyWebhook) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetContactId

`func (o *CreateLoyaltyWebhook) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *CreateLoyaltyWebhook) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *CreateLoyaltyWebhook) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetLoyaltyId

`func (o *CreateLoyaltyWebhook) GetLoyaltyId() string`

GetLoyaltyId returns the LoyaltyId field if non-nil, zero value otherwise.

### GetLoyaltyIdOk

`func (o *CreateLoyaltyWebhook) GetLoyaltyIdOk() (*string, bool)`

GetLoyaltyIdOk returns a tuple with the LoyaltyId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLoyaltyId

`func (o *CreateLoyaltyWebhook) SetLoyaltyId(v string)`

SetLoyaltyId sets LoyaltyId field to given value.


### GetChannelId

`func (o *CreateLoyaltyWebhook) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CreateLoyaltyWebhook) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CreateLoyaltyWebhook) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


