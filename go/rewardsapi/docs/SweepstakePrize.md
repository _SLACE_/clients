# SweepstakePrize

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**WinChance** | **int32** |  | 
**WinChanceType** | **string** |  | 

## Methods

### NewSweepstakePrize

`func NewSweepstakePrize(name string, winChance int32, winChanceType string, ) *SweepstakePrize`

NewSweepstakePrize instantiates a new SweepstakePrize object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSweepstakePrizeWithDefaults

`func NewSweepstakePrizeWithDefaults() *SweepstakePrize`

NewSweepstakePrizeWithDefaults instantiates a new SweepstakePrize object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *SweepstakePrize) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *SweepstakePrize) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *SweepstakePrize) SetName(v string)`

SetName sets Name field to given value.


### GetWinChance

`func (o *SweepstakePrize) GetWinChance() int32`

GetWinChance returns the WinChance field if non-nil, zero value otherwise.

### GetWinChanceOk

`func (o *SweepstakePrize) GetWinChanceOk() (*int32, bool)`

GetWinChanceOk returns a tuple with the WinChance field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWinChance

`func (o *SweepstakePrize) SetWinChance(v int32)`

SetWinChance sets WinChance field to given value.


### GetWinChanceType

`func (o *SweepstakePrize) GetWinChanceType() string`

GetWinChanceType returns the WinChanceType field if non-nil, zero value otherwise.

### GetWinChanceTypeOk

`func (o *SweepstakePrize) GetWinChanceTypeOk() (*string, bool)`

GetWinChanceTypeOk returns a tuple with the WinChanceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWinChanceType

`func (o *SweepstakePrize) SetWinChanceType(v string)`

SetWinChanceType sets WinChanceType field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


