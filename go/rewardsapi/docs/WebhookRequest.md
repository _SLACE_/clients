# WebhookRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TransactionId** | **string** |  | 
**ConversationId** | **string** |  | 
**ContactId** | **string** |  | 
**Code** | **string** |  | 
**Attributes** | Pointer to **map[string]interface{}** |  | [optional] 
**ChannelId** | **string** |  | 
**Name** | **string** |  | 
**Type** | **string** |  | 
**DisplayType** | **string** |  | 
**Currency** | Pointer to **string** |  | [optional] 
**Value** | Pointer to **string** |  | [optional] 
**Description** | Pointer to **string** |  | [optional] 
**ExpiresAt** | Pointer to **time.Time** |  | [optional] 
**DisplayCode** | Pointer to **string** |  | [optional] 
**ExpiresIn** | Pointer to **int32** |  | [optional] 
**LoyaltyId** | **string** |  | 
**CurrentAmount** | **int32** |  | 
**Redeemed** | **bool** |  | 
**ErrorMessage** | Pointer to **string** |  | [optional] 
**TotalAmount** | **string** |  | 

## Methods

### NewWebhookRequest

`func NewWebhookRequest(transactionId string, conversationId string, contactId string, code string, channelId string, name string, type_ string, displayType string, loyaltyId string, currentAmount int32, redeemed bool, totalAmount string, ) *WebhookRequest`

NewWebhookRequest instantiates a new WebhookRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookRequestWithDefaults

`func NewWebhookRequestWithDefaults() *WebhookRequest`

NewWebhookRequestWithDefaults instantiates a new WebhookRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTransactionId

`func (o *WebhookRequest) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *WebhookRequest) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *WebhookRequest) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetConversationId

`func (o *WebhookRequest) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *WebhookRequest) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *WebhookRequest) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetContactId

`func (o *WebhookRequest) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *WebhookRequest) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *WebhookRequest) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetCode

`func (o *WebhookRequest) GetCode() string`

GetCode returns the Code field if non-nil, zero value otherwise.

### GetCodeOk

`func (o *WebhookRequest) GetCodeOk() (*string, bool)`

GetCodeOk returns a tuple with the Code field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCode

`func (o *WebhookRequest) SetCode(v string)`

SetCode sets Code field to given value.


### GetAttributes

`func (o *WebhookRequest) GetAttributes() map[string]interface{}`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *WebhookRequest) GetAttributesOk() (*map[string]interface{}, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *WebhookRequest) SetAttributes(v map[string]interface{})`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *WebhookRequest) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetChannelId

`func (o *WebhookRequest) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *WebhookRequest) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *WebhookRequest) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetName

`func (o *WebhookRequest) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *WebhookRequest) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *WebhookRequest) SetName(v string)`

SetName sets Name field to given value.


### GetType

`func (o *WebhookRequest) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WebhookRequest) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WebhookRequest) SetType(v string)`

SetType sets Type field to given value.


### GetDisplayType

`func (o *WebhookRequest) GetDisplayType() string`

GetDisplayType returns the DisplayType field if non-nil, zero value otherwise.

### GetDisplayTypeOk

`func (o *WebhookRequest) GetDisplayTypeOk() (*string, bool)`

GetDisplayTypeOk returns a tuple with the DisplayType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayType

`func (o *WebhookRequest) SetDisplayType(v string)`

SetDisplayType sets DisplayType field to given value.


### GetCurrency

`func (o *WebhookRequest) GetCurrency() string`

GetCurrency returns the Currency field if non-nil, zero value otherwise.

### GetCurrencyOk

`func (o *WebhookRequest) GetCurrencyOk() (*string, bool)`

GetCurrencyOk returns a tuple with the Currency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrency

`func (o *WebhookRequest) SetCurrency(v string)`

SetCurrency sets Currency field to given value.

### HasCurrency

`func (o *WebhookRequest) HasCurrency() bool`

HasCurrency returns a boolean if a field has been set.

### GetValue

`func (o *WebhookRequest) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *WebhookRequest) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *WebhookRequest) SetValue(v string)`

SetValue sets Value field to given value.

### HasValue

`func (o *WebhookRequest) HasValue() bool`

HasValue returns a boolean if a field has been set.

### GetDescription

`func (o *WebhookRequest) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *WebhookRequest) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *WebhookRequest) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *WebhookRequest) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetExpiresAt

`func (o *WebhookRequest) GetExpiresAt() time.Time`

GetExpiresAt returns the ExpiresAt field if non-nil, zero value otherwise.

### GetExpiresAtOk

`func (o *WebhookRequest) GetExpiresAtOk() (*time.Time, bool)`

GetExpiresAtOk returns a tuple with the ExpiresAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiresAt

`func (o *WebhookRequest) SetExpiresAt(v time.Time)`

SetExpiresAt sets ExpiresAt field to given value.

### HasExpiresAt

`func (o *WebhookRequest) HasExpiresAt() bool`

HasExpiresAt returns a boolean if a field has been set.

### GetDisplayCode

`func (o *WebhookRequest) GetDisplayCode() string`

GetDisplayCode returns the DisplayCode field if non-nil, zero value otherwise.

### GetDisplayCodeOk

`func (o *WebhookRequest) GetDisplayCodeOk() (*string, bool)`

GetDisplayCodeOk returns a tuple with the DisplayCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayCode

`func (o *WebhookRequest) SetDisplayCode(v string)`

SetDisplayCode sets DisplayCode field to given value.

### HasDisplayCode

`func (o *WebhookRequest) HasDisplayCode() bool`

HasDisplayCode returns a boolean if a field has been set.

### GetExpiresIn

`func (o *WebhookRequest) GetExpiresIn() int32`

GetExpiresIn returns the ExpiresIn field if non-nil, zero value otherwise.

### GetExpiresInOk

`func (o *WebhookRequest) GetExpiresInOk() (*int32, bool)`

GetExpiresInOk returns a tuple with the ExpiresIn field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiresIn

`func (o *WebhookRequest) SetExpiresIn(v int32)`

SetExpiresIn sets ExpiresIn field to given value.

### HasExpiresIn

`func (o *WebhookRequest) HasExpiresIn() bool`

HasExpiresIn returns a boolean if a field has been set.

### GetLoyaltyId

`func (o *WebhookRequest) GetLoyaltyId() string`

GetLoyaltyId returns the LoyaltyId field if non-nil, zero value otherwise.

### GetLoyaltyIdOk

`func (o *WebhookRequest) GetLoyaltyIdOk() (*string, bool)`

GetLoyaltyIdOk returns a tuple with the LoyaltyId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLoyaltyId

`func (o *WebhookRequest) SetLoyaltyId(v string)`

SetLoyaltyId sets LoyaltyId field to given value.


### GetCurrentAmount

`func (o *WebhookRequest) GetCurrentAmount() int32`

GetCurrentAmount returns the CurrentAmount field if non-nil, zero value otherwise.

### GetCurrentAmountOk

`func (o *WebhookRequest) GetCurrentAmountOk() (*int32, bool)`

GetCurrentAmountOk returns a tuple with the CurrentAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrentAmount

`func (o *WebhookRequest) SetCurrentAmount(v int32)`

SetCurrentAmount sets CurrentAmount field to given value.


### GetRedeemed

`func (o *WebhookRequest) GetRedeemed() bool`

GetRedeemed returns the Redeemed field if non-nil, zero value otherwise.

### GetRedeemedOk

`func (o *WebhookRequest) GetRedeemedOk() (*bool, bool)`

GetRedeemedOk returns a tuple with the Redeemed field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRedeemed

`func (o *WebhookRequest) SetRedeemed(v bool)`

SetRedeemed sets Redeemed field to given value.


### GetErrorMessage

`func (o *WebhookRequest) GetErrorMessage() string`

GetErrorMessage returns the ErrorMessage field if non-nil, zero value otherwise.

### GetErrorMessageOk

`func (o *WebhookRequest) GetErrorMessageOk() (*string, bool)`

GetErrorMessageOk returns a tuple with the ErrorMessage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorMessage

`func (o *WebhookRequest) SetErrorMessage(v string)`

SetErrorMessage sets ErrorMessage field to given value.

### HasErrorMessage

`func (o *WebhookRequest) HasErrorMessage() bool`

HasErrorMessage returns a boolean if a field has been set.

### GetTotalAmount

`func (o *WebhookRequest) GetTotalAmount() string`

GetTotalAmount returns the TotalAmount field if non-nil, zero value otherwise.

### GetTotalAmountOk

`func (o *WebhookRequest) GetTotalAmountOk() (*string, bool)`

GetTotalAmountOk returns a tuple with the TotalAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalAmount

`func (o *WebhookRequest) SetTotalAmount(v string)`

SetTotalAmount sets TotalAmount field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


