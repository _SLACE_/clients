# RedeemCouponWebhook

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TransactionId** | **string** |  | 
**ConversationId** | **string** |  | 
**ContactId** | **string** |  | 
**Code** | **string** |  | 
**Attributes** | Pointer to **map[string]interface{}** |  | [optional] 
**ChannelId** | **string** |  | 
**Redeemed** | **bool** |  | 
**ErrorMessage** | Pointer to **string** |  | [optional] 
**TotalAmount** | **string** |  | 

## Methods

### NewRedeemCouponWebhook

`func NewRedeemCouponWebhook(transactionId string, conversationId string, contactId string, code string, channelId string, redeemed bool, totalAmount string, ) *RedeemCouponWebhook`

NewRedeemCouponWebhook instantiates a new RedeemCouponWebhook object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRedeemCouponWebhookWithDefaults

`func NewRedeemCouponWebhookWithDefaults() *RedeemCouponWebhook`

NewRedeemCouponWebhookWithDefaults instantiates a new RedeemCouponWebhook object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTransactionId

`func (o *RedeemCouponWebhook) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *RedeemCouponWebhook) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *RedeemCouponWebhook) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetConversationId

`func (o *RedeemCouponWebhook) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *RedeemCouponWebhook) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *RedeemCouponWebhook) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetContactId

`func (o *RedeemCouponWebhook) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *RedeemCouponWebhook) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *RedeemCouponWebhook) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetCode

`func (o *RedeemCouponWebhook) GetCode() string`

GetCode returns the Code field if non-nil, zero value otherwise.

### GetCodeOk

`func (o *RedeemCouponWebhook) GetCodeOk() (*string, bool)`

GetCodeOk returns a tuple with the Code field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCode

`func (o *RedeemCouponWebhook) SetCode(v string)`

SetCode sets Code field to given value.


### GetAttributes

`func (o *RedeemCouponWebhook) GetAttributes() map[string]interface{}`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *RedeemCouponWebhook) GetAttributesOk() (*map[string]interface{}, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *RedeemCouponWebhook) SetAttributes(v map[string]interface{})`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *RedeemCouponWebhook) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetChannelId

`func (o *RedeemCouponWebhook) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *RedeemCouponWebhook) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *RedeemCouponWebhook) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetRedeemed

`func (o *RedeemCouponWebhook) GetRedeemed() bool`

GetRedeemed returns the Redeemed field if non-nil, zero value otherwise.

### GetRedeemedOk

`func (o *RedeemCouponWebhook) GetRedeemedOk() (*bool, bool)`

GetRedeemedOk returns a tuple with the Redeemed field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRedeemed

`func (o *RedeemCouponWebhook) SetRedeemed(v bool)`

SetRedeemed sets Redeemed field to given value.


### GetErrorMessage

`func (o *RedeemCouponWebhook) GetErrorMessage() string`

GetErrorMessage returns the ErrorMessage field if non-nil, zero value otherwise.

### GetErrorMessageOk

`func (o *RedeemCouponWebhook) GetErrorMessageOk() (*string, bool)`

GetErrorMessageOk returns a tuple with the ErrorMessage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorMessage

`func (o *RedeemCouponWebhook) SetErrorMessage(v string)`

SetErrorMessage sets ErrorMessage field to given value.

### HasErrorMessage

`func (o *RedeemCouponWebhook) HasErrorMessage() bool`

HasErrorMessage returns a boolean if a field has been set.

### GetTotalAmount

`func (o *RedeemCouponWebhook) GetTotalAmount() string`

GetTotalAmount returns the TotalAmount field if non-nil, zero value otherwise.

### GetTotalAmountOk

`func (o *RedeemCouponWebhook) GetTotalAmountOk() (*string, bool)`

GetTotalAmountOk returns a tuple with the TotalAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalAmount

`func (o *RedeemCouponWebhook) SetTotalAmount(v string)`

SetTotalAmount sets TotalAmount field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


