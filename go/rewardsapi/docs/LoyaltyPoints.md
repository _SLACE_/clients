# LoyaltyPoints

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [readonly] 
**CurrentAmount** | **int32** |  | [readonly] 
**ContactId** | **string** |  | [readonly] 
**ChannelId** | **string** |  | [readonly] 
**OrganizationId** | **string** |  | [readonly] 
**ConversationId** | **string** |  | [readonly] 

## Methods

### NewLoyaltyPoints

`func NewLoyaltyPoints(name string, currentAmount int32, contactId string, channelId string, organizationId string, conversationId string, ) *LoyaltyPoints`

NewLoyaltyPoints instantiates a new LoyaltyPoints object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLoyaltyPointsWithDefaults

`func NewLoyaltyPointsWithDefaults() *LoyaltyPoints`

NewLoyaltyPointsWithDefaults instantiates a new LoyaltyPoints object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *LoyaltyPoints) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *LoyaltyPoints) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *LoyaltyPoints) SetName(v string)`

SetName sets Name field to given value.


### GetCurrentAmount

`func (o *LoyaltyPoints) GetCurrentAmount() int32`

GetCurrentAmount returns the CurrentAmount field if non-nil, zero value otherwise.

### GetCurrentAmountOk

`func (o *LoyaltyPoints) GetCurrentAmountOk() (*int32, bool)`

GetCurrentAmountOk returns a tuple with the CurrentAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrentAmount

`func (o *LoyaltyPoints) SetCurrentAmount(v int32)`

SetCurrentAmount sets CurrentAmount field to given value.


### GetContactId

`func (o *LoyaltyPoints) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *LoyaltyPoints) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *LoyaltyPoints) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetChannelId

`func (o *LoyaltyPoints) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *LoyaltyPoints) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *LoyaltyPoints) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetOrganizationId

`func (o *LoyaltyPoints) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *LoyaltyPoints) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *LoyaltyPoints) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetConversationId

`func (o *LoyaltyPoints) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *LoyaltyPoints) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *LoyaltyPoints) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


