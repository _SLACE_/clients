# InstantWinConfiguration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [readonly] 
**Name** | **string** |  | 
**CreatedAt** | **time.Time** |  | [readonly] 
**UpdatedAt** | **time.Time** |  | [readonly] 
**Prizes** | [**[]InstantWinConfigurationPrizesInner**](InstantWinConfigurationPrizesInner.md) |  | 
**Type** | **string** |  | 
**UsageCounter** | **int32** |  | [readonly] 

## Methods

### NewInstantWinConfiguration

`func NewInstantWinConfiguration(id string, name string, createdAt time.Time, updatedAt time.Time, prizes []InstantWinConfigurationPrizesInner, type_ string, usageCounter int32, ) *InstantWinConfiguration`

NewInstantWinConfiguration instantiates a new InstantWinConfiguration object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInstantWinConfigurationWithDefaults

`func NewInstantWinConfigurationWithDefaults() *InstantWinConfiguration`

NewInstantWinConfigurationWithDefaults instantiates a new InstantWinConfiguration object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *InstantWinConfiguration) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *InstantWinConfiguration) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *InstantWinConfiguration) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *InstantWinConfiguration) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *InstantWinConfiguration) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *InstantWinConfiguration) SetName(v string)`

SetName sets Name field to given value.


### GetCreatedAt

`func (o *InstantWinConfiguration) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *InstantWinConfiguration) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *InstantWinConfiguration) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *InstantWinConfiguration) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *InstantWinConfiguration) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *InstantWinConfiguration) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetPrizes

`func (o *InstantWinConfiguration) GetPrizes() []InstantWinConfigurationPrizesInner`

GetPrizes returns the Prizes field if non-nil, zero value otherwise.

### GetPrizesOk

`func (o *InstantWinConfiguration) GetPrizesOk() (*[]InstantWinConfigurationPrizesInner, bool)`

GetPrizesOk returns a tuple with the Prizes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrizes

`func (o *InstantWinConfiguration) SetPrizes(v []InstantWinConfigurationPrizesInner)`

SetPrizes sets Prizes field to given value.


### GetType

`func (o *InstantWinConfiguration) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *InstantWinConfiguration) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *InstantWinConfiguration) SetType(v string)`

SetType sets Type field to given value.


### GetUsageCounter

`func (o *InstantWinConfiguration) GetUsageCounter() int32`

GetUsageCounter returns the UsageCounter field if non-nil, zero value otherwise.

### GetUsageCounterOk

`func (o *InstantWinConfiguration) GetUsageCounterOk() (*int32, bool)`

GetUsageCounterOk returns a tuple with the UsageCounter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsageCounter

`func (o *InstantWinConfiguration) SetUsageCounter(v int32)`

SetUsageCounter sets UsageCounter field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


