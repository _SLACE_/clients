# InstantWinConfigurationPrizesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WinChance** | **int32** |  | 
**Name** | **string** |  | 
**MaxWins** | **int32** |  | 
**WinCount** | **int32** |  | [readonly] 

## Methods

### NewInstantWinConfigurationPrizesInner

`func NewInstantWinConfigurationPrizesInner(winChance int32, name string, maxWins int32, winCount int32, ) *InstantWinConfigurationPrizesInner`

NewInstantWinConfigurationPrizesInner instantiates a new InstantWinConfigurationPrizesInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInstantWinConfigurationPrizesInnerWithDefaults

`func NewInstantWinConfigurationPrizesInnerWithDefaults() *InstantWinConfigurationPrizesInner`

NewInstantWinConfigurationPrizesInnerWithDefaults instantiates a new InstantWinConfigurationPrizesInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWinChance

`func (o *InstantWinConfigurationPrizesInner) GetWinChance() int32`

GetWinChance returns the WinChance field if non-nil, zero value otherwise.

### GetWinChanceOk

`func (o *InstantWinConfigurationPrizesInner) GetWinChanceOk() (*int32, bool)`

GetWinChanceOk returns a tuple with the WinChance field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWinChance

`func (o *InstantWinConfigurationPrizesInner) SetWinChance(v int32)`

SetWinChance sets WinChance field to given value.


### GetName

`func (o *InstantWinConfigurationPrizesInner) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *InstantWinConfigurationPrizesInner) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *InstantWinConfigurationPrizesInner) SetName(v string)`

SetName sets Name field to given value.


### GetMaxWins

`func (o *InstantWinConfigurationPrizesInner) GetMaxWins() int32`

GetMaxWins returns the MaxWins field if non-nil, zero value otherwise.

### GetMaxWinsOk

`func (o *InstantWinConfigurationPrizesInner) GetMaxWinsOk() (*int32, bool)`

GetMaxWinsOk returns a tuple with the MaxWins field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxWins

`func (o *InstantWinConfigurationPrizesInner) SetMaxWins(v int32)`

SetMaxWins sets MaxWins field to given value.


### GetWinCount

`func (o *InstantWinConfigurationPrizesInner) GetWinCount() int32`

GetWinCount returns the WinCount field if non-nil, zero value otherwise.

### GetWinCountOk

`func (o *InstantWinConfigurationPrizesInner) GetWinCountOk() (*int32, bool)`

GetWinCountOk returns a tuple with the WinCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWinCount

`func (o *InstantWinConfigurationPrizesInner) SetWinCount(v int32)`

SetWinCount sets WinCount field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


