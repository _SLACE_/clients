# DrawInstantWinRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**InteractionId** | **string** |  | 
**TransactionId** | **string** |  | 
**ContactId** | **string** |  | 
**ChannelId** | **string** |  | 
**ConversationId** | **string** |  | 

## Methods

### NewDrawInstantWinRequest

`func NewDrawInstantWinRequest(name string, interactionId string, transactionId string, contactId string, channelId string, conversationId string, ) *DrawInstantWinRequest`

NewDrawInstantWinRequest instantiates a new DrawInstantWinRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDrawInstantWinRequestWithDefaults

`func NewDrawInstantWinRequestWithDefaults() *DrawInstantWinRequest`

NewDrawInstantWinRequestWithDefaults instantiates a new DrawInstantWinRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *DrawInstantWinRequest) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *DrawInstantWinRequest) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *DrawInstantWinRequest) SetName(v string)`

SetName sets Name field to given value.


### GetInteractionId

`func (o *DrawInstantWinRequest) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *DrawInstantWinRequest) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *DrawInstantWinRequest) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.


### GetTransactionId

`func (o *DrawInstantWinRequest) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *DrawInstantWinRequest) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *DrawInstantWinRequest) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetContactId

`func (o *DrawInstantWinRequest) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *DrawInstantWinRequest) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *DrawInstantWinRequest) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetChannelId

`func (o *DrawInstantWinRequest) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *DrawInstantWinRequest) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *DrawInstantWinRequest) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetConversationId

`func (o *DrawInstantWinRequest) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *DrawInstantWinRequest) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *DrawInstantWinRequest) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


