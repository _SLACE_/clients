# InstantWin

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [readonly] 
**WinResult** | **string** |  | [readonly] 
**TransactionId** | **string** |  | [readonly] 
**InteractionId** | **string** |  | [readonly] 
**ContactId** | **string** |  | [readonly] 
**CreatedAt** | **time.Time** |  | [readonly] 
**ConfigId** | **string** |  | [readonly] 
**ChannelId** | **string** |  | [readonly] 
**ConversationId** | **string** |  | [readonly] 
**PrizeName** | **string** |  | [readonly] 
**CurrentUsage** | **int32** |  | [readonly] 

## Methods

### NewInstantWin

`func NewInstantWin(id int32, winResult string, transactionId string, interactionId string, contactId string, createdAt time.Time, configId string, channelId string, conversationId string, prizeName string, currentUsage int32, ) *InstantWin`

NewInstantWin instantiates a new InstantWin object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInstantWinWithDefaults

`func NewInstantWinWithDefaults() *InstantWin`

NewInstantWinWithDefaults instantiates a new InstantWin object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *InstantWin) GetId() int32`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *InstantWin) GetIdOk() (*int32, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *InstantWin) SetId(v int32)`

SetId sets Id field to given value.


### GetWinResult

`func (o *InstantWin) GetWinResult() string`

GetWinResult returns the WinResult field if non-nil, zero value otherwise.

### GetWinResultOk

`func (o *InstantWin) GetWinResultOk() (*string, bool)`

GetWinResultOk returns a tuple with the WinResult field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWinResult

`func (o *InstantWin) SetWinResult(v string)`

SetWinResult sets WinResult field to given value.


### GetTransactionId

`func (o *InstantWin) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *InstantWin) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *InstantWin) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetInteractionId

`func (o *InstantWin) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *InstantWin) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *InstantWin) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.


### GetContactId

`func (o *InstantWin) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *InstantWin) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *InstantWin) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetCreatedAt

`func (o *InstantWin) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *InstantWin) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *InstantWin) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetConfigId

`func (o *InstantWin) GetConfigId() string`

GetConfigId returns the ConfigId field if non-nil, zero value otherwise.

### GetConfigIdOk

`func (o *InstantWin) GetConfigIdOk() (*string, bool)`

GetConfigIdOk returns a tuple with the ConfigId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfigId

`func (o *InstantWin) SetConfigId(v string)`

SetConfigId sets ConfigId field to given value.


### GetChannelId

`func (o *InstantWin) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *InstantWin) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *InstantWin) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetConversationId

`func (o *InstantWin) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *InstantWin) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *InstantWin) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetPrizeName

`func (o *InstantWin) GetPrizeName() string`

GetPrizeName returns the PrizeName field if non-nil, zero value otherwise.

### GetPrizeNameOk

`func (o *InstantWin) GetPrizeNameOk() (*string, bool)`

GetPrizeNameOk returns a tuple with the PrizeName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrizeName

`func (o *InstantWin) SetPrizeName(v string)`

SetPrizeName sets PrizeName field to given value.


### GetCurrentUsage

`func (o *InstantWin) GetCurrentUsage() int32`

GetCurrentUsage returns the CurrentUsage field if non-nil, zero value otherwise.

### GetCurrentUsageOk

`func (o *InstantWin) GetCurrentUsageOk() (*int32, bool)`

GetCurrentUsageOk returns a tuple with the CurrentUsage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrentUsage

`func (o *InstantWin) SetCurrentUsage(v int32)`

SetCurrentUsage sets CurrentUsage field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


