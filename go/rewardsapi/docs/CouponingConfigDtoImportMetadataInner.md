# CouponingConfigDtoImportMetadataInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConfigId** | **string** |  | 
**UserId** | **string** |  | 
**Skipped** | **int32** |  | 
**Imported** | **int32** |  | 
**Updated** | **int32** |  | 

## Methods

### NewCouponingConfigDtoImportMetadataInner

`func NewCouponingConfigDtoImportMetadataInner(configId string, userId string, skipped int32, imported int32, updated int32, ) *CouponingConfigDtoImportMetadataInner`

NewCouponingConfigDtoImportMetadataInner instantiates a new CouponingConfigDtoImportMetadataInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCouponingConfigDtoImportMetadataInnerWithDefaults

`func NewCouponingConfigDtoImportMetadataInnerWithDefaults() *CouponingConfigDtoImportMetadataInner`

NewCouponingConfigDtoImportMetadataInnerWithDefaults instantiates a new CouponingConfigDtoImportMetadataInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetConfigId

`func (o *CouponingConfigDtoImportMetadataInner) GetConfigId() string`

GetConfigId returns the ConfigId field if non-nil, zero value otherwise.

### GetConfigIdOk

`func (o *CouponingConfigDtoImportMetadataInner) GetConfigIdOk() (*string, bool)`

GetConfigIdOk returns a tuple with the ConfigId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfigId

`func (o *CouponingConfigDtoImportMetadataInner) SetConfigId(v string)`

SetConfigId sets ConfigId field to given value.


### GetUserId

`func (o *CouponingConfigDtoImportMetadataInner) GetUserId() string`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *CouponingConfigDtoImportMetadataInner) GetUserIdOk() (*string, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *CouponingConfigDtoImportMetadataInner) SetUserId(v string)`

SetUserId sets UserId field to given value.


### GetSkipped

`func (o *CouponingConfigDtoImportMetadataInner) GetSkipped() int32`

GetSkipped returns the Skipped field if non-nil, zero value otherwise.

### GetSkippedOk

`func (o *CouponingConfigDtoImportMetadataInner) GetSkippedOk() (*int32, bool)`

GetSkippedOk returns a tuple with the Skipped field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSkipped

`func (o *CouponingConfigDtoImportMetadataInner) SetSkipped(v int32)`

SetSkipped sets Skipped field to given value.


### GetImported

`func (o *CouponingConfigDtoImportMetadataInner) GetImported() int32`

GetImported returns the Imported field if non-nil, zero value otherwise.

### GetImportedOk

`func (o *CouponingConfigDtoImportMetadataInner) GetImportedOk() (*int32, bool)`

GetImportedOk returns a tuple with the Imported field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetImported

`func (o *CouponingConfigDtoImportMetadataInner) SetImported(v int32)`

SetImported sets Imported field to given value.


### GetUpdated

`func (o *CouponingConfigDtoImportMetadataInner) GetUpdated() int32`

GetUpdated returns the Updated field if non-nil, zero value otherwise.

### GetUpdatedOk

`func (o *CouponingConfigDtoImportMetadataInner) GetUpdatedOk() (*int32, bool)`

GetUpdatedOk returns a tuple with the Updated field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdated

`func (o *CouponingConfigDtoImportMetadataInner) SetUpdated(v int32)`

SetUpdated sets Updated field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


