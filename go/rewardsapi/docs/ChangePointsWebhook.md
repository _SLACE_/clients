# ChangePointsWebhook

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TransactionId** | **string** |  | 
**ConversationId** | **string** |  | 
**ContactId** | **string** |  | 
**CurrentAmount** | **int32** |  | 
**ChannelId** | **string** |  | 

## Methods

### NewChangePointsWebhook

`func NewChangePointsWebhook(transactionId string, conversationId string, contactId string, currentAmount int32, channelId string, ) *ChangePointsWebhook`

NewChangePointsWebhook instantiates a new ChangePointsWebhook object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChangePointsWebhookWithDefaults

`func NewChangePointsWebhookWithDefaults() *ChangePointsWebhook`

NewChangePointsWebhookWithDefaults instantiates a new ChangePointsWebhook object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTransactionId

`func (o *ChangePointsWebhook) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *ChangePointsWebhook) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *ChangePointsWebhook) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetConversationId

`func (o *ChangePointsWebhook) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *ChangePointsWebhook) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *ChangePointsWebhook) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetContactId

`func (o *ChangePointsWebhook) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *ChangePointsWebhook) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *ChangePointsWebhook) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetCurrentAmount

`func (o *ChangePointsWebhook) GetCurrentAmount() int32`

GetCurrentAmount returns the CurrentAmount field if non-nil, zero value otherwise.

### GetCurrentAmountOk

`func (o *ChangePointsWebhook) GetCurrentAmountOk() (*int32, bool)`

GetCurrentAmountOk returns a tuple with the CurrentAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrentAmount

`func (o *ChangePointsWebhook) SetCurrentAmount(v int32)`

SetCurrentAmount sets CurrentAmount field to given value.


### GetChannelId

`func (o *ChangePointsWebhook) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *ChangePointsWebhook) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *ChangePointsWebhook) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


