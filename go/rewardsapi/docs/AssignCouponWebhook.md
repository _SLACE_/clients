# AssignCouponWebhook

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TransactionId** | **string** |  | 
**ConversationId** | **string** |  | 
**ContactId** | **string** |  | 
**Code** | **string** |  | 
**Attributes** | Pointer to **map[string]interface{}** |  | [optional] 
**ChannelId** | **string** |  | 
**Name** | **string** |  | 
**Type** | **string** |  | 
**DisplayType** | **string** |  | 
**Currency** | Pointer to **string** |  | [optional] 
**Value** | Pointer to **string** |  | [optional] 
**Description** | Pointer to **string** |  | [optional] 
**ExpiresAt** | Pointer to **time.Time** |  | [optional] 
**DisplayCode** | Pointer to **string** |  | [optional] 
**ExpiresIn** | Pointer to **int32** |  | [optional] 

## Methods

### NewAssignCouponWebhook

`func NewAssignCouponWebhook(transactionId string, conversationId string, contactId string, code string, channelId string, name string, type_ string, displayType string, ) *AssignCouponWebhook`

NewAssignCouponWebhook instantiates a new AssignCouponWebhook object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAssignCouponWebhookWithDefaults

`func NewAssignCouponWebhookWithDefaults() *AssignCouponWebhook`

NewAssignCouponWebhookWithDefaults instantiates a new AssignCouponWebhook object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTransactionId

`func (o *AssignCouponWebhook) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *AssignCouponWebhook) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *AssignCouponWebhook) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetConversationId

`func (o *AssignCouponWebhook) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *AssignCouponWebhook) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *AssignCouponWebhook) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetContactId

`func (o *AssignCouponWebhook) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *AssignCouponWebhook) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *AssignCouponWebhook) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetCode

`func (o *AssignCouponWebhook) GetCode() string`

GetCode returns the Code field if non-nil, zero value otherwise.

### GetCodeOk

`func (o *AssignCouponWebhook) GetCodeOk() (*string, bool)`

GetCodeOk returns a tuple with the Code field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCode

`func (o *AssignCouponWebhook) SetCode(v string)`

SetCode sets Code field to given value.


### GetAttributes

`func (o *AssignCouponWebhook) GetAttributes() map[string]interface{}`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *AssignCouponWebhook) GetAttributesOk() (*map[string]interface{}, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *AssignCouponWebhook) SetAttributes(v map[string]interface{})`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *AssignCouponWebhook) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetChannelId

`func (o *AssignCouponWebhook) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *AssignCouponWebhook) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *AssignCouponWebhook) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetName

`func (o *AssignCouponWebhook) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *AssignCouponWebhook) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *AssignCouponWebhook) SetName(v string)`

SetName sets Name field to given value.


### GetType

`func (o *AssignCouponWebhook) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *AssignCouponWebhook) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *AssignCouponWebhook) SetType(v string)`

SetType sets Type field to given value.


### GetDisplayType

`func (o *AssignCouponWebhook) GetDisplayType() string`

GetDisplayType returns the DisplayType field if non-nil, zero value otherwise.

### GetDisplayTypeOk

`func (o *AssignCouponWebhook) GetDisplayTypeOk() (*string, bool)`

GetDisplayTypeOk returns a tuple with the DisplayType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayType

`func (o *AssignCouponWebhook) SetDisplayType(v string)`

SetDisplayType sets DisplayType field to given value.


### GetCurrency

`func (o *AssignCouponWebhook) GetCurrency() string`

GetCurrency returns the Currency field if non-nil, zero value otherwise.

### GetCurrencyOk

`func (o *AssignCouponWebhook) GetCurrencyOk() (*string, bool)`

GetCurrencyOk returns a tuple with the Currency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrency

`func (o *AssignCouponWebhook) SetCurrency(v string)`

SetCurrency sets Currency field to given value.

### HasCurrency

`func (o *AssignCouponWebhook) HasCurrency() bool`

HasCurrency returns a boolean if a field has been set.

### GetValue

`func (o *AssignCouponWebhook) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *AssignCouponWebhook) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *AssignCouponWebhook) SetValue(v string)`

SetValue sets Value field to given value.

### HasValue

`func (o *AssignCouponWebhook) HasValue() bool`

HasValue returns a boolean if a field has been set.

### GetDescription

`func (o *AssignCouponWebhook) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *AssignCouponWebhook) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *AssignCouponWebhook) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *AssignCouponWebhook) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetExpiresAt

`func (o *AssignCouponWebhook) GetExpiresAt() time.Time`

GetExpiresAt returns the ExpiresAt field if non-nil, zero value otherwise.

### GetExpiresAtOk

`func (o *AssignCouponWebhook) GetExpiresAtOk() (*time.Time, bool)`

GetExpiresAtOk returns a tuple with the ExpiresAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiresAt

`func (o *AssignCouponWebhook) SetExpiresAt(v time.Time)`

SetExpiresAt sets ExpiresAt field to given value.

### HasExpiresAt

`func (o *AssignCouponWebhook) HasExpiresAt() bool`

HasExpiresAt returns a boolean if a field has been set.

### GetDisplayCode

`func (o *AssignCouponWebhook) GetDisplayCode() string`

GetDisplayCode returns the DisplayCode field if non-nil, zero value otherwise.

### GetDisplayCodeOk

`func (o *AssignCouponWebhook) GetDisplayCodeOk() (*string, bool)`

GetDisplayCodeOk returns a tuple with the DisplayCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayCode

`func (o *AssignCouponWebhook) SetDisplayCode(v string)`

SetDisplayCode sets DisplayCode field to given value.

### HasDisplayCode

`func (o *AssignCouponWebhook) HasDisplayCode() bool`

HasDisplayCode returns a boolean if a field has been set.

### GetExpiresIn

`func (o *AssignCouponWebhook) GetExpiresIn() int32`

GetExpiresIn returns the ExpiresIn field if non-nil, zero value otherwise.

### GetExpiresInOk

`func (o *AssignCouponWebhook) GetExpiresInOk() (*int32, bool)`

GetExpiresInOk returns a tuple with the ExpiresIn field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiresIn

`func (o *AssignCouponWebhook) SetExpiresIn(v int32)`

SetExpiresIn sets ExpiresIn field to given value.

### HasExpiresIn

`func (o *AssignCouponWebhook) HasExpiresIn() bool`

HasExpiresIn returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


