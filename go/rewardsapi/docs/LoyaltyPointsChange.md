# LoyaltyPointsChange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**Amount** | **int32** |  | 
**ChangeType** | **string** |  | 
**ContactId** | **string** |  | 
**ChannelId** | **string** |  | 
**OrganizationId** | **string** |  | 
**ConversationId** | **string** |  | 

## Methods

### NewLoyaltyPointsChange

`func NewLoyaltyPointsChange(name string, amount int32, changeType string, contactId string, channelId string, organizationId string, conversationId string, ) *LoyaltyPointsChange`

NewLoyaltyPointsChange instantiates a new LoyaltyPointsChange object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLoyaltyPointsChangeWithDefaults

`func NewLoyaltyPointsChangeWithDefaults() *LoyaltyPointsChange`

NewLoyaltyPointsChangeWithDefaults instantiates a new LoyaltyPointsChange object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *LoyaltyPointsChange) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *LoyaltyPointsChange) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *LoyaltyPointsChange) SetName(v string)`

SetName sets Name field to given value.


### GetAmount

`func (o *LoyaltyPointsChange) GetAmount() int32`

GetAmount returns the Amount field if non-nil, zero value otherwise.

### GetAmountOk

`func (o *LoyaltyPointsChange) GetAmountOk() (*int32, bool)`

GetAmountOk returns a tuple with the Amount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAmount

`func (o *LoyaltyPointsChange) SetAmount(v int32)`

SetAmount sets Amount field to given value.


### GetChangeType

`func (o *LoyaltyPointsChange) GetChangeType() string`

GetChangeType returns the ChangeType field if non-nil, zero value otherwise.

### GetChangeTypeOk

`func (o *LoyaltyPointsChange) GetChangeTypeOk() (*string, bool)`

GetChangeTypeOk returns a tuple with the ChangeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeType

`func (o *LoyaltyPointsChange) SetChangeType(v string)`

SetChangeType sets ChangeType field to given value.


### GetContactId

`func (o *LoyaltyPointsChange) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *LoyaltyPointsChange) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *LoyaltyPointsChange) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetChannelId

`func (o *LoyaltyPointsChange) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *LoyaltyPointsChange) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *LoyaltyPointsChange) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetOrganizationId

`func (o *LoyaltyPointsChange) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *LoyaltyPointsChange) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *LoyaltyPointsChange) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetConversationId

`func (o *LoyaltyPointsChange) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *LoyaltyPointsChange) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *LoyaltyPointsChange) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


