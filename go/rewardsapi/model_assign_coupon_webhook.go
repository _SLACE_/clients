/*
Rewards API

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package rewardsapi

import (
	"encoding/json"
	"time"
)

// checks if the AssignCouponWebhook type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &AssignCouponWebhook{}

// AssignCouponWebhook struct for AssignCouponWebhook
type AssignCouponWebhook struct {
	TransactionId string `json:"transaction_id"`
	ConversationId string `json:"conversation_id"`
	ContactId string `json:"contact_id"`
	Code string `json:"code"`
	Attributes map[string]interface{} `json:"attributes,omitempty"`
	ChannelId string `json:"channel_id"`
	Name string `json:"name"`
	Type string `json:"type"`
	DisplayType string `json:"display_type"`
	Currency *string `json:"currency,omitempty"`
	Value *string `json:"value,omitempty"`
	Description *string `json:"description,omitempty"`
	ExpiresAt *time.Time `json:"expires_at,omitempty"`
	DisplayCode *string `json:"display_code,omitempty"`
	ExpiresIn *int32 `json:"expires_in,omitempty"`
}

// NewAssignCouponWebhook instantiates a new AssignCouponWebhook object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAssignCouponWebhook(transactionId string, conversationId string, contactId string, code string, channelId string, name string, type_ string, displayType string) *AssignCouponWebhook {
	this := AssignCouponWebhook{}
	this.TransactionId = transactionId
	this.ConversationId = conversationId
	this.ContactId = contactId
	this.Code = code
	this.ChannelId = channelId
	this.Name = name
	this.Type = type_
	this.DisplayType = displayType
	return &this
}

// NewAssignCouponWebhookWithDefaults instantiates a new AssignCouponWebhook object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAssignCouponWebhookWithDefaults() *AssignCouponWebhook {
	this := AssignCouponWebhook{}
	return &this
}

// GetTransactionId returns the TransactionId field value
func (o *AssignCouponWebhook) GetTransactionId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.TransactionId
}

// GetTransactionIdOk returns a tuple with the TransactionId field value
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetTransactionIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.TransactionId, true
}

// SetTransactionId sets field value
func (o *AssignCouponWebhook) SetTransactionId(v string) {
	o.TransactionId = v
}

// GetConversationId returns the ConversationId field value
func (o *AssignCouponWebhook) GetConversationId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ConversationId
}

// GetConversationIdOk returns a tuple with the ConversationId field value
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetConversationIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ConversationId, true
}

// SetConversationId sets field value
func (o *AssignCouponWebhook) SetConversationId(v string) {
	o.ConversationId = v
}

// GetContactId returns the ContactId field value
func (o *AssignCouponWebhook) GetContactId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ContactId
}

// GetContactIdOk returns a tuple with the ContactId field value
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetContactIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ContactId, true
}

// SetContactId sets field value
func (o *AssignCouponWebhook) SetContactId(v string) {
	o.ContactId = v
}

// GetCode returns the Code field value
func (o *AssignCouponWebhook) GetCode() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Code
}

// GetCodeOk returns a tuple with the Code field value
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetCodeOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Code, true
}

// SetCode sets field value
func (o *AssignCouponWebhook) SetCode(v string) {
	o.Code = v
}

// GetAttributes returns the Attributes field value if set, zero value otherwise.
func (o *AssignCouponWebhook) GetAttributes() map[string]interface{} {
	if o == nil || IsNil(o.Attributes) {
		var ret map[string]interface{}
		return ret
	}
	return o.Attributes
}

// GetAttributesOk returns a tuple with the Attributes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetAttributesOk() (map[string]interface{}, bool) {
	if o == nil || IsNil(o.Attributes) {
		return map[string]interface{}{}, false
	}
	return o.Attributes, true
}

// HasAttributes returns a boolean if a field has been set.
func (o *AssignCouponWebhook) HasAttributes() bool {
	if o != nil && !IsNil(o.Attributes) {
		return true
	}

	return false
}

// SetAttributes gets a reference to the given map[string]interface{} and assigns it to the Attributes field.
func (o *AssignCouponWebhook) SetAttributes(v map[string]interface{}) {
	o.Attributes = v
}

// GetChannelId returns the ChannelId field value
func (o *AssignCouponWebhook) GetChannelId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ChannelId
}

// GetChannelIdOk returns a tuple with the ChannelId field value
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetChannelIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ChannelId, true
}

// SetChannelId sets field value
func (o *AssignCouponWebhook) SetChannelId(v string) {
	o.ChannelId = v
}

// GetName returns the Name field value
func (o *AssignCouponWebhook) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *AssignCouponWebhook) SetName(v string) {
	o.Name = v
}

// GetType returns the Type field value
func (o *AssignCouponWebhook) GetType() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Type
}

// GetTypeOk returns a tuple with the Type field value
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetTypeOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Type, true
}

// SetType sets field value
func (o *AssignCouponWebhook) SetType(v string) {
	o.Type = v
}

// GetDisplayType returns the DisplayType field value
func (o *AssignCouponWebhook) GetDisplayType() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.DisplayType
}

// GetDisplayTypeOk returns a tuple with the DisplayType field value
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetDisplayTypeOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.DisplayType, true
}

// SetDisplayType sets field value
func (o *AssignCouponWebhook) SetDisplayType(v string) {
	o.DisplayType = v
}

// GetCurrency returns the Currency field value if set, zero value otherwise.
func (o *AssignCouponWebhook) GetCurrency() string {
	if o == nil || IsNil(o.Currency) {
		var ret string
		return ret
	}
	return *o.Currency
}

// GetCurrencyOk returns a tuple with the Currency field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetCurrencyOk() (*string, bool) {
	if o == nil || IsNil(o.Currency) {
		return nil, false
	}
	return o.Currency, true
}

// HasCurrency returns a boolean if a field has been set.
func (o *AssignCouponWebhook) HasCurrency() bool {
	if o != nil && !IsNil(o.Currency) {
		return true
	}

	return false
}

// SetCurrency gets a reference to the given string and assigns it to the Currency field.
func (o *AssignCouponWebhook) SetCurrency(v string) {
	o.Currency = &v
}

// GetValue returns the Value field value if set, zero value otherwise.
func (o *AssignCouponWebhook) GetValue() string {
	if o == nil || IsNil(o.Value) {
		var ret string
		return ret
	}
	return *o.Value
}

// GetValueOk returns a tuple with the Value field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetValueOk() (*string, bool) {
	if o == nil || IsNil(o.Value) {
		return nil, false
	}
	return o.Value, true
}

// HasValue returns a boolean if a field has been set.
func (o *AssignCouponWebhook) HasValue() bool {
	if o != nil && !IsNil(o.Value) {
		return true
	}

	return false
}

// SetValue gets a reference to the given string and assigns it to the Value field.
func (o *AssignCouponWebhook) SetValue(v string) {
	o.Value = &v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *AssignCouponWebhook) GetDescription() string {
	if o == nil || IsNil(o.Description) {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetDescriptionOk() (*string, bool) {
	if o == nil || IsNil(o.Description) {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *AssignCouponWebhook) HasDescription() bool {
	if o != nil && !IsNil(o.Description) {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *AssignCouponWebhook) SetDescription(v string) {
	o.Description = &v
}

// GetExpiresAt returns the ExpiresAt field value if set, zero value otherwise.
func (o *AssignCouponWebhook) GetExpiresAt() time.Time {
	if o == nil || IsNil(o.ExpiresAt) {
		var ret time.Time
		return ret
	}
	return *o.ExpiresAt
}

// GetExpiresAtOk returns a tuple with the ExpiresAt field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetExpiresAtOk() (*time.Time, bool) {
	if o == nil || IsNil(o.ExpiresAt) {
		return nil, false
	}
	return o.ExpiresAt, true
}

// HasExpiresAt returns a boolean if a field has been set.
func (o *AssignCouponWebhook) HasExpiresAt() bool {
	if o != nil && !IsNil(o.ExpiresAt) {
		return true
	}

	return false
}

// SetExpiresAt gets a reference to the given time.Time and assigns it to the ExpiresAt field.
func (o *AssignCouponWebhook) SetExpiresAt(v time.Time) {
	o.ExpiresAt = &v
}

// GetDisplayCode returns the DisplayCode field value if set, zero value otherwise.
func (o *AssignCouponWebhook) GetDisplayCode() string {
	if o == nil || IsNil(o.DisplayCode) {
		var ret string
		return ret
	}
	return *o.DisplayCode
}

// GetDisplayCodeOk returns a tuple with the DisplayCode field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetDisplayCodeOk() (*string, bool) {
	if o == nil || IsNil(o.DisplayCode) {
		return nil, false
	}
	return o.DisplayCode, true
}

// HasDisplayCode returns a boolean if a field has been set.
func (o *AssignCouponWebhook) HasDisplayCode() bool {
	if o != nil && !IsNil(o.DisplayCode) {
		return true
	}

	return false
}

// SetDisplayCode gets a reference to the given string and assigns it to the DisplayCode field.
func (o *AssignCouponWebhook) SetDisplayCode(v string) {
	o.DisplayCode = &v
}

// GetExpiresIn returns the ExpiresIn field value if set, zero value otherwise.
func (o *AssignCouponWebhook) GetExpiresIn() int32 {
	if o == nil || IsNil(o.ExpiresIn) {
		var ret int32
		return ret
	}
	return *o.ExpiresIn
}

// GetExpiresInOk returns a tuple with the ExpiresIn field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AssignCouponWebhook) GetExpiresInOk() (*int32, bool) {
	if o == nil || IsNil(o.ExpiresIn) {
		return nil, false
	}
	return o.ExpiresIn, true
}

// HasExpiresIn returns a boolean if a field has been set.
func (o *AssignCouponWebhook) HasExpiresIn() bool {
	if o != nil && !IsNil(o.ExpiresIn) {
		return true
	}

	return false
}

// SetExpiresIn gets a reference to the given int32 and assigns it to the ExpiresIn field.
func (o *AssignCouponWebhook) SetExpiresIn(v int32) {
	o.ExpiresIn = &v
}

func (o AssignCouponWebhook) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o AssignCouponWebhook) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["transaction_id"] = o.TransactionId
	toSerialize["conversation_id"] = o.ConversationId
	toSerialize["contact_id"] = o.ContactId
	toSerialize["code"] = o.Code
	if !IsNil(o.Attributes) {
		toSerialize["attributes"] = o.Attributes
	}
	toSerialize["channel_id"] = o.ChannelId
	toSerialize["name"] = o.Name
	toSerialize["type"] = o.Type
	toSerialize["display_type"] = o.DisplayType
	if !IsNil(o.Currency) {
		toSerialize["currency"] = o.Currency
	}
	if !IsNil(o.Value) {
		toSerialize["value"] = o.Value
	}
	if !IsNil(o.Description) {
		toSerialize["description"] = o.Description
	}
	if !IsNil(o.ExpiresAt) {
		toSerialize["expires_at"] = o.ExpiresAt
	}
	if !IsNil(o.DisplayCode) {
		toSerialize["display_code"] = o.DisplayCode
	}
	if !IsNil(o.ExpiresIn) {
		toSerialize["expires_in"] = o.ExpiresIn
	}
	return toSerialize, nil
}

type NullableAssignCouponWebhook struct {
	value *AssignCouponWebhook
	isSet bool
}

func (v NullableAssignCouponWebhook) Get() *AssignCouponWebhook {
	return v.value
}

func (v *NullableAssignCouponWebhook) Set(val *AssignCouponWebhook) {
	v.value = val
	v.isSet = true
}

func (v NullableAssignCouponWebhook) IsSet() bool {
	return v.isSet
}

func (v *NullableAssignCouponWebhook) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAssignCouponWebhook(val *AssignCouponWebhook) *NullableAssignCouponWebhook {
	return &NullableAssignCouponWebhook{value: val, isSet: true}
}

func (v NullableAssignCouponWebhook) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAssignCouponWebhook) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


