/*
Rewards API

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package rewardsapi

import (
	"encoding/json"
)

// checks if the ResetInstantWinRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ResetInstantWinRequest{}

// ResetInstantWinRequest struct for ResetInstantWinRequest
type ResetInstantWinRequest struct {
	Name string `json:"name"`
}

// NewResetInstantWinRequest instantiates a new ResetInstantWinRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewResetInstantWinRequest(name string) *ResetInstantWinRequest {
	this := ResetInstantWinRequest{}
	this.Name = name
	return &this
}

// NewResetInstantWinRequestWithDefaults instantiates a new ResetInstantWinRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewResetInstantWinRequestWithDefaults() *ResetInstantWinRequest {
	this := ResetInstantWinRequest{}
	return &this
}

// GetName returns the Name field value
func (o *ResetInstantWinRequest) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *ResetInstantWinRequest) GetNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *ResetInstantWinRequest) SetName(v string) {
	o.Name = v
}

func (o ResetInstantWinRequest) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ResetInstantWinRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["name"] = o.Name
	return toSerialize, nil
}

type NullableResetInstantWinRequest struct {
	value *ResetInstantWinRequest
	isSet bool
}

func (v NullableResetInstantWinRequest) Get() *ResetInstantWinRequest {
	return v.value
}

func (v *NullableResetInstantWinRequest) Set(val *ResetInstantWinRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableResetInstantWinRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableResetInstantWinRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableResetInstantWinRequest(val *ResetInstantWinRequest) *NullableResetInstantWinRequest {
	return &NullableResetInstantWinRequest{value: val, isSet: true}
}

func (v NullableResetInstantWinRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableResetInstantWinRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


