/*
BotV2

Bot API v2

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package botapiv2

import (
	"encoding/json"
)

// checks if the Writer type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &Writer{}

// Writer struct for Writer
type Writer struct {
	Target *WriterTarget `json:"target,omitempty"`
	Source *WriterSource `json:"source,omitempty"`
	Property *string `json:"property,omitempty"`
	StaticValue *string `json:"static_value,omitempty"`
}

// NewWriter instantiates a new Writer object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewWriter() *Writer {
	this := Writer{}
	return &this
}

// NewWriterWithDefaults instantiates a new Writer object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewWriterWithDefaults() *Writer {
	this := Writer{}
	return &this
}

// GetTarget returns the Target field value if set, zero value otherwise.
func (o *Writer) GetTarget() WriterTarget {
	if o == nil || IsNil(o.Target) {
		var ret WriterTarget
		return ret
	}
	return *o.Target
}

// GetTargetOk returns a tuple with the Target field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Writer) GetTargetOk() (*WriterTarget, bool) {
	if o == nil || IsNil(o.Target) {
		return nil, false
	}
	return o.Target, true
}

// HasTarget returns a boolean if a field has been set.
func (o *Writer) HasTarget() bool {
	if o != nil && !IsNil(o.Target) {
		return true
	}

	return false
}

// SetTarget gets a reference to the given WriterTarget and assigns it to the Target field.
func (o *Writer) SetTarget(v WriterTarget) {
	o.Target = &v
}

// GetSource returns the Source field value if set, zero value otherwise.
func (o *Writer) GetSource() WriterSource {
	if o == nil || IsNil(o.Source) {
		var ret WriterSource
		return ret
	}
	return *o.Source
}

// GetSourceOk returns a tuple with the Source field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Writer) GetSourceOk() (*WriterSource, bool) {
	if o == nil || IsNil(o.Source) {
		return nil, false
	}
	return o.Source, true
}

// HasSource returns a boolean if a field has been set.
func (o *Writer) HasSource() bool {
	if o != nil && !IsNil(o.Source) {
		return true
	}

	return false
}

// SetSource gets a reference to the given WriterSource and assigns it to the Source field.
func (o *Writer) SetSource(v WriterSource) {
	o.Source = &v
}

// GetProperty returns the Property field value if set, zero value otherwise.
func (o *Writer) GetProperty() string {
	if o == nil || IsNil(o.Property) {
		var ret string
		return ret
	}
	return *o.Property
}

// GetPropertyOk returns a tuple with the Property field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Writer) GetPropertyOk() (*string, bool) {
	if o == nil || IsNil(o.Property) {
		return nil, false
	}
	return o.Property, true
}

// HasProperty returns a boolean if a field has been set.
func (o *Writer) HasProperty() bool {
	if o != nil && !IsNil(o.Property) {
		return true
	}

	return false
}

// SetProperty gets a reference to the given string and assigns it to the Property field.
func (o *Writer) SetProperty(v string) {
	o.Property = &v
}

// GetStaticValue returns the StaticValue field value if set, zero value otherwise.
func (o *Writer) GetStaticValue() string {
	if o == nil || IsNil(o.StaticValue) {
		var ret string
		return ret
	}
	return *o.StaticValue
}

// GetStaticValueOk returns a tuple with the StaticValue field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Writer) GetStaticValueOk() (*string, bool) {
	if o == nil || IsNil(o.StaticValue) {
		return nil, false
	}
	return o.StaticValue, true
}

// HasStaticValue returns a boolean if a field has been set.
func (o *Writer) HasStaticValue() bool {
	if o != nil && !IsNil(o.StaticValue) {
		return true
	}

	return false
}

// SetStaticValue gets a reference to the given string and assigns it to the StaticValue field.
func (o *Writer) SetStaticValue(v string) {
	o.StaticValue = &v
}

func (o Writer) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o Writer) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Target) {
		toSerialize["target"] = o.Target
	}
	if !IsNil(o.Source) {
		toSerialize["source"] = o.Source
	}
	if !IsNil(o.Property) {
		toSerialize["property"] = o.Property
	}
	if !IsNil(o.StaticValue) {
		toSerialize["static_value"] = o.StaticValue
	}
	return toSerialize, nil
}

type NullableWriter struct {
	value *Writer
	isSet bool
}

func (v NullableWriter) Get() *Writer {
	return v.value
}

func (v *NullableWriter) Set(val *Writer) {
	v.value = val
	v.isSet = true
}

func (v NullableWriter) IsSet() bool {
	return v.isSet
}

func (v *NullableWriter) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableWriter(val *Writer) *NullableWriter {
	return &NullableWriter{value: val, isSet: true}
}

func (v NullableWriter) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableWriter) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


