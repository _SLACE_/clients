/*
BotV2

Bot API v2

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package botapiv2

import (
	"encoding/json"
	"time"
)

// checks if the Script type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &Script{}

// Script struct for Script
type Script struct {
	Id string `json:"id"`
	ChannelId string `json:"channel_id"`
	Name string `json:"name"`
	MaxQuestions int32 `json:"max_questions"`
	Status Status `json:"status"`
	RestartAllowed bool `json:"restart_allowed"`
	CannedResponse bool `json:"canned_response"`
	Locales []Locale `json:"locales"`
	StartTrigger *StartTrigger `json:"start_trigger,omitempty"`
	Formality *Formality `json:"formality,omitempty"`
	Blocks []Block `json:"blocks"`
	Assets []Asset `json:"assets"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

// NewScript instantiates a new Script object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewScript(id string, channelId string, name string, maxQuestions int32, status Status, restartAllowed bool, cannedResponse bool, locales []Locale, blocks []Block, assets []Asset, createdAt time.Time, updatedAt time.Time) *Script {
	this := Script{}
	this.Id = id
	this.ChannelId = channelId
	this.Name = name
	this.MaxQuestions = maxQuestions
	this.Status = status
	this.RestartAllowed = restartAllowed
	this.CannedResponse = cannedResponse
	this.Locales = locales
	this.Blocks = blocks
	this.Assets = assets
	this.CreatedAt = createdAt
	this.UpdatedAt = updatedAt
	return &this
}

// NewScriptWithDefaults instantiates a new Script object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewScriptWithDefaults() *Script {
	this := Script{}
	return &this
}

// GetId returns the Id field value
func (o *Script) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *Script) GetIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *Script) SetId(v string) {
	o.Id = v
}

// GetChannelId returns the ChannelId field value
func (o *Script) GetChannelId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ChannelId
}

// GetChannelIdOk returns a tuple with the ChannelId field value
// and a boolean to check if the value has been set.
func (o *Script) GetChannelIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ChannelId, true
}

// SetChannelId sets field value
func (o *Script) SetChannelId(v string) {
	o.ChannelId = v
}

// GetName returns the Name field value
func (o *Script) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *Script) GetNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *Script) SetName(v string) {
	o.Name = v
}

// GetMaxQuestions returns the MaxQuestions field value
func (o *Script) GetMaxQuestions() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.MaxQuestions
}

// GetMaxQuestionsOk returns a tuple with the MaxQuestions field value
// and a boolean to check if the value has been set.
func (o *Script) GetMaxQuestionsOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.MaxQuestions, true
}

// SetMaxQuestions sets field value
func (o *Script) SetMaxQuestions(v int32) {
	o.MaxQuestions = v
}

// GetStatus returns the Status field value
func (o *Script) GetStatus() Status {
	if o == nil {
		var ret Status
		return ret
	}

	return o.Status
}

// GetStatusOk returns a tuple with the Status field value
// and a boolean to check if the value has been set.
func (o *Script) GetStatusOk() (*Status, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Status, true
}

// SetStatus sets field value
func (o *Script) SetStatus(v Status) {
	o.Status = v
}

// GetRestartAllowed returns the RestartAllowed field value
func (o *Script) GetRestartAllowed() bool {
	if o == nil {
		var ret bool
		return ret
	}

	return o.RestartAllowed
}

// GetRestartAllowedOk returns a tuple with the RestartAllowed field value
// and a boolean to check if the value has been set.
func (o *Script) GetRestartAllowedOk() (*bool, bool) {
	if o == nil {
		return nil, false
	}
	return &o.RestartAllowed, true
}

// SetRestartAllowed sets field value
func (o *Script) SetRestartAllowed(v bool) {
	o.RestartAllowed = v
}

// GetCannedResponse returns the CannedResponse field value
func (o *Script) GetCannedResponse() bool {
	if o == nil {
		var ret bool
		return ret
	}

	return o.CannedResponse
}

// GetCannedResponseOk returns a tuple with the CannedResponse field value
// and a boolean to check if the value has been set.
func (o *Script) GetCannedResponseOk() (*bool, bool) {
	if o == nil {
		return nil, false
	}
	return &o.CannedResponse, true
}

// SetCannedResponse sets field value
func (o *Script) SetCannedResponse(v bool) {
	o.CannedResponse = v
}

// GetLocales returns the Locales field value
func (o *Script) GetLocales() []Locale {
	if o == nil {
		var ret []Locale
		return ret
	}

	return o.Locales
}

// GetLocalesOk returns a tuple with the Locales field value
// and a boolean to check if the value has been set.
func (o *Script) GetLocalesOk() ([]Locale, bool) {
	if o == nil {
		return nil, false
	}
	return o.Locales, true
}

// SetLocales sets field value
func (o *Script) SetLocales(v []Locale) {
	o.Locales = v
}

// GetStartTrigger returns the StartTrigger field value if set, zero value otherwise.
func (o *Script) GetStartTrigger() StartTrigger {
	if o == nil || IsNil(o.StartTrigger) {
		var ret StartTrigger
		return ret
	}
	return *o.StartTrigger
}

// GetStartTriggerOk returns a tuple with the StartTrigger field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Script) GetStartTriggerOk() (*StartTrigger, bool) {
	if o == nil || IsNil(o.StartTrigger) {
		return nil, false
	}
	return o.StartTrigger, true
}

// HasStartTrigger returns a boolean if a field has been set.
func (o *Script) HasStartTrigger() bool {
	if o != nil && !IsNil(o.StartTrigger) {
		return true
	}

	return false
}

// SetStartTrigger gets a reference to the given StartTrigger and assigns it to the StartTrigger field.
func (o *Script) SetStartTrigger(v StartTrigger) {
	o.StartTrigger = &v
}

// GetFormality returns the Formality field value if set, zero value otherwise.
func (o *Script) GetFormality() Formality {
	if o == nil || IsNil(o.Formality) {
		var ret Formality
		return ret
	}
	return *o.Formality
}

// GetFormalityOk returns a tuple with the Formality field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Script) GetFormalityOk() (*Formality, bool) {
	if o == nil || IsNil(o.Formality) {
		return nil, false
	}
	return o.Formality, true
}

// HasFormality returns a boolean if a field has been set.
func (o *Script) HasFormality() bool {
	if o != nil && !IsNil(o.Formality) {
		return true
	}

	return false
}

// SetFormality gets a reference to the given Formality and assigns it to the Formality field.
func (o *Script) SetFormality(v Formality) {
	o.Formality = &v
}

// GetBlocks returns the Blocks field value
func (o *Script) GetBlocks() []Block {
	if o == nil {
		var ret []Block
		return ret
	}

	return o.Blocks
}

// GetBlocksOk returns a tuple with the Blocks field value
// and a boolean to check if the value has been set.
func (o *Script) GetBlocksOk() ([]Block, bool) {
	if o == nil {
		return nil, false
	}
	return o.Blocks, true
}

// SetBlocks sets field value
func (o *Script) SetBlocks(v []Block) {
	o.Blocks = v
}

// GetAssets returns the Assets field value
func (o *Script) GetAssets() []Asset {
	if o == nil {
		var ret []Asset
		return ret
	}

	return o.Assets
}

// GetAssetsOk returns a tuple with the Assets field value
// and a boolean to check if the value has been set.
func (o *Script) GetAssetsOk() ([]Asset, bool) {
	if o == nil {
		return nil, false
	}
	return o.Assets, true
}

// SetAssets sets field value
func (o *Script) SetAssets(v []Asset) {
	o.Assets = v
}

// GetCreatedAt returns the CreatedAt field value
func (o *Script) GetCreatedAt() time.Time {
	if o == nil {
		var ret time.Time
		return ret
	}

	return o.CreatedAt
}

// GetCreatedAtOk returns a tuple with the CreatedAt field value
// and a boolean to check if the value has been set.
func (o *Script) GetCreatedAtOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return &o.CreatedAt, true
}

// SetCreatedAt sets field value
func (o *Script) SetCreatedAt(v time.Time) {
	o.CreatedAt = v
}

// GetUpdatedAt returns the UpdatedAt field value
func (o *Script) GetUpdatedAt() time.Time {
	if o == nil {
		var ret time.Time
		return ret
	}

	return o.UpdatedAt
}

// GetUpdatedAtOk returns a tuple with the UpdatedAt field value
// and a boolean to check if the value has been set.
func (o *Script) GetUpdatedAtOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return &o.UpdatedAt, true
}

// SetUpdatedAt sets field value
func (o *Script) SetUpdatedAt(v time.Time) {
	o.UpdatedAt = v
}

func (o Script) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o Script) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["id"] = o.Id
	toSerialize["channel_id"] = o.ChannelId
	toSerialize["name"] = o.Name
	toSerialize["max_questions"] = o.MaxQuestions
	toSerialize["status"] = o.Status
	toSerialize["restart_allowed"] = o.RestartAllowed
	toSerialize["canned_response"] = o.CannedResponse
	toSerialize["locales"] = o.Locales
	if !IsNil(o.StartTrigger) {
		toSerialize["start_trigger"] = o.StartTrigger
	}
	if !IsNil(o.Formality) {
		toSerialize["formality"] = o.Formality
	}
	toSerialize["blocks"] = o.Blocks
	toSerialize["assets"] = o.Assets
	toSerialize["created_at"] = o.CreatedAt
	toSerialize["updated_at"] = o.UpdatedAt
	return toSerialize, nil
}

type NullableScript struct {
	value *Script
	isSet bool
}

func (v NullableScript) Get() *Script {
	return v.value
}

func (v *NullableScript) Set(val *Script) {
	v.value = val
	v.isSet = true
}

func (v NullableScript) IsSet() bool {
	return v.isSet
}

func (v *NullableScript) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableScript(val *Script) *NullableScript {
	return &NullableScript{value: val, isSet: true}
}

func (v NullableScript) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableScript) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


