/*
BotV2

Bot API v2

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package botapiv2

import (
	"encoding/json"
)

// checks if the Settings type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &Settings{}

// Settings struct for Settings
type Settings struct {
	Locales []string `json:"locales"`
	// It`s a map of DataSourceProperties index by DataSource
	DataSources *map[string]DataSourceProperties `json:"data_sources,omitempty"`
	// It`s a map of Operator list index by DataType
	DataTypeOperators *map[string][]Operator `json:"data_type_operators,omitempty"`
}

// NewSettings instantiates a new Settings object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewSettings(locales []string) *Settings {
	this := Settings{}
	this.Locales = locales
	return &this
}

// NewSettingsWithDefaults instantiates a new Settings object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewSettingsWithDefaults() *Settings {
	this := Settings{}
	return &this
}

// GetLocales returns the Locales field value
func (o *Settings) GetLocales() []string {
	if o == nil {
		var ret []string
		return ret
	}

	return o.Locales
}

// GetLocalesOk returns a tuple with the Locales field value
// and a boolean to check if the value has been set.
func (o *Settings) GetLocalesOk() ([]string, bool) {
	if o == nil {
		return nil, false
	}
	return o.Locales, true
}

// SetLocales sets field value
func (o *Settings) SetLocales(v []string) {
	o.Locales = v
}

// GetDataSources returns the DataSources field value if set, zero value otherwise.
func (o *Settings) GetDataSources() map[string]DataSourceProperties {
	if o == nil || IsNil(o.DataSources) {
		var ret map[string]DataSourceProperties
		return ret
	}
	return *o.DataSources
}

// GetDataSourcesOk returns a tuple with the DataSources field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Settings) GetDataSourcesOk() (*map[string]DataSourceProperties, bool) {
	if o == nil || IsNil(o.DataSources) {
		return nil, false
	}
	return o.DataSources, true
}

// HasDataSources returns a boolean if a field has been set.
func (o *Settings) HasDataSources() bool {
	if o != nil && !IsNil(o.DataSources) {
		return true
	}

	return false
}

// SetDataSources gets a reference to the given map[string]DataSourceProperties and assigns it to the DataSources field.
func (o *Settings) SetDataSources(v map[string]DataSourceProperties) {
	o.DataSources = &v
}

// GetDataTypeOperators returns the DataTypeOperators field value if set, zero value otherwise.
func (o *Settings) GetDataTypeOperators() map[string][]Operator {
	if o == nil || IsNil(o.DataTypeOperators) {
		var ret map[string][]Operator
		return ret
	}
	return *o.DataTypeOperators
}

// GetDataTypeOperatorsOk returns a tuple with the DataTypeOperators field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Settings) GetDataTypeOperatorsOk() (*map[string][]Operator, bool) {
	if o == nil || IsNil(o.DataTypeOperators) {
		return nil, false
	}
	return o.DataTypeOperators, true
}

// HasDataTypeOperators returns a boolean if a field has been set.
func (o *Settings) HasDataTypeOperators() bool {
	if o != nil && !IsNil(o.DataTypeOperators) {
		return true
	}

	return false
}

// SetDataTypeOperators gets a reference to the given map[string][]Operator and assigns it to the DataTypeOperators field.
func (o *Settings) SetDataTypeOperators(v map[string][]Operator) {
	o.DataTypeOperators = &v
}

func (o Settings) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o Settings) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["locales"] = o.Locales
	if !IsNil(o.DataSources) {
		toSerialize["data_sources"] = o.DataSources
	}
	if !IsNil(o.DataTypeOperators) {
		toSerialize["data_type_operators"] = o.DataTypeOperators
	}
	return toSerialize, nil
}

type NullableSettings struct {
	value *Settings
	isSet bool
}

func (v NullableSettings) Get() *Settings {
	return v.value
}

func (v *NullableSettings) Set(val *Settings) {
	v.value = val
	v.isSet = true
}

func (v NullableSettings) IsSet() bool {
	return v.isSet
}

func (v *NullableSettings) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableSettings(val *Settings) *NullableSettings {
	return &NullableSettings{value: val, isSet: true}
}

func (v NullableSettings) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableSettings) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


