/*
BotV2

Testing CannedResponsesAPIService

*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech);

package botapiv2

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
	openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func Test_botapiv2_CannedResponsesAPIService(t *testing.T) {

	configuration := openapiclient.NewConfiguration()
	apiClient := openapiclient.NewAPIClient(configuration)

	t.Run("Test CannedResponsesAPIService CannedResponseParams", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var id string

		resp, httpRes, err := apiClient.CannedResponsesAPI.CannedResponseParams(context.Background(), channelId, id).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

	t.Run("Test CannedResponsesAPIService SendCannedResponse", func(t *testing.T) {

		t.Skip("skip test")  // remove to run test

		var channelId string
		var id string

		resp, httpRes, err := apiClient.CannedResponsesAPI.SendCannedResponse(context.Background(), channelId, id).Execute()

		require.Nil(t, err)
		require.NotNil(t, resp)
		assert.Equal(t, 200, httpRes.StatusCode)

	})

}
