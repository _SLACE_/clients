/*
BotV2

Bot API v2

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package botapiv2

import (
	"encoding/json"
)

// checks if the ExpressionValue type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ExpressionValue{}

// ExpressionValue struct for ExpressionValue
type ExpressionValue struct {
	Source DataSource `json:"source"`
	// It's possible to use `||` separator in which case value will be treated as array. Usefull for operators like `oneOf` and similar.
	Value string `json:"value"`
}

// NewExpressionValue instantiates a new ExpressionValue object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewExpressionValue(source DataSource, value string) *ExpressionValue {
	this := ExpressionValue{}
	this.Source = source
	this.Value = value
	return &this
}

// NewExpressionValueWithDefaults instantiates a new ExpressionValue object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewExpressionValueWithDefaults() *ExpressionValue {
	this := ExpressionValue{}
	return &this
}

// GetSource returns the Source field value
func (o *ExpressionValue) GetSource() DataSource {
	if o == nil {
		var ret DataSource
		return ret
	}

	return o.Source
}

// GetSourceOk returns a tuple with the Source field value
// and a boolean to check if the value has been set.
func (o *ExpressionValue) GetSourceOk() (*DataSource, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Source, true
}

// SetSource sets field value
func (o *ExpressionValue) SetSource(v DataSource) {
	o.Source = v
}

// GetValue returns the Value field value
func (o *ExpressionValue) GetValue() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Value
}

// GetValueOk returns a tuple with the Value field value
// and a boolean to check if the value has been set.
func (o *ExpressionValue) GetValueOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Value, true
}

// SetValue sets field value
func (o *ExpressionValue) SetValue(v string) {
	o.Value = v
}

func (o ExpressionValue) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ExpressionValue) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["source"] = o.Source
	toSerialize["value"] = o.Value
	return toSerialize, nil
}

type NullableExpressionValue struct {
	value *ExpressionValue
	isSet bool
}

func (v NullableExpressionValue) Get() *ExpressionValue {
	return v.value
}

func (v *NullableExpressionValue) Set(val *ExpressionValue) {
	v.value = val
	v.isSet = true
}

func (v NullableExpressionValue) IsSet() bool {
	return v.isSet
}

func (v *NullableExpressionValue) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableExpressionValue(val *ExpressionValue) *NullableExpressionValue {
	return &NullableExpressionValue{value: val, isSet: true}
}

func (v NullableExpressionValue) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableExpressionValue) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


