# Operator

## Enum


* `OperatorAnd` (value: `"and"`)

* `OperatorOr` (value: `"or"`)

* `OperatorEq` (value: `"eq"`)

* `OperatorNeq` (value: `"neq"`)

* `OperatorLt` (value: `"lt"`)

* `OperatorLte` (value: `"lte"`)

* `OperatorGt` (value: `"gt"`)

* `OperatorGte` (value: `"gte"`)

* `OperatorContains` (value: `"ct"`)

* `OperatorAllOf` (value: `"allOf"`)

* `OperatorNoneOf` (value: `"noneOf"`)

* `OperatorOneOf` (value: `"oneOf"`)

* `OperatorEmpty` (value: `"empty"`)

* `OperatorStartsWith` (value: `"startsWith"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


