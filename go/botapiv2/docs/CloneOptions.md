# CloneOptions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelIds** | **[]string** |  | 
**ReuseScripts** | Pointer to **bool** |  | [optional] 
**RolloutVersion** | Pointer to **string** |  | [optional] 
**ForceGlobalMedia** | Pointer to **bool** |  | [optional] 

## Methods

### NewCloneOptions

`func NewCloneOptions(channelIds []string, ) *CloneOptions`

NewCloneOptions instantiates a new CloneOptions object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCloneOptionsWithDefaults

`func NewCloneOptionsWithDefaults() *CloneOptions`

NewCloneOptionsWithDefaults instantiates a new CloneOptions object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetChannelIds

`func (o *CloneOptions) GetChannelIds() []string`

GetChannelIds returns the ChannelIds field if non-nil, zero value otherwise.

### GetChannelIdsOk

`func (o *CloneOptions) GetChannelIdsOk() (*[]string, bool)`

GetChannelIdsOk returns a tuple with the ChannelIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelIds

`func (o *CloneOptions) SetChannelIds(v []string)`

SetChannelIds sets ChannelIds field to given value.


### GetReuseScripts

`func (o *CloneOptions) GetReuseScripts() bool`

GetReuseScripts returns the ReuseScripts field if non-nil, zero value otherwise.

### GetReuseScriptsOk

`func (o *CloneOptions) GetReuseScriptsOk() (*bool, bool)`

GetReuseScriptsOk returns a tuple with the ReuseScripts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReuseScripts

`func (o *CloneOptions) SetReuseScripts(v bool)`

SetReuseScripts sets ReuseScripts field to given value.

### HasReuseScripts

`func (o *CloneOptions) HasReuseScripts() bool`

HasReuseScripts returns a boolean if a field has been set.

### GetRolloutVersion

`func (o *CloneOptions) GetRolloutVersion() string`

GetRolloutVersion returns the RolloutVersion field if non-nil, zero value otherwise.

### GetRolloutVersionOk

`func (o *CloneOptions) GetRolloutVersionOk() (*string, bool)`

GetRolloutVersionOk returns a tuple with the RolloutVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRolloutVersion

`func (o *CloneOptions) SetRolloutVersion(v string)`

SetRolloutVersion sets RolloutVersion field to given value.

### HasRolloutVersion

`func (o *CloneOptions) HasRolloutVersion() bool`

HasRolloutVersion returns a boolean if a field has been set.

### GetForceGlobalMedia

`func (o *CloneOptions) GetForceGlobalMedia() bool`

GetForceGlobalMedia returns the ForceGlobalMedia field if non-nil, zero value otherwise.

### GetForceGlobalMediaOk

`func (o *CloneOptions) GetForceGlobalMediaOk() (*bool, bool)`

GetForceGlobalMediaOk returns a tuple with the ForceGlobalMedia field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetForceGlobalMedia

`func (o *CloneOptions) SetForceGlobalMedia(v bool)`

SetForceGlobalMedia sets ForceGlobalMedia field to given value.

### HasForceGlobalMedia

`func (o *CloneOptions) HasForceGlobalMedia() bool`

HasForceGlobalMedia returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


