# Settings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Locales** | **[]string** |  | 
**DataSources** | Pointer to [**map[string]DataSourceProperties**](DataSourceProperties.md) | It&#x60;s a map of DataSourceProperties index by DataSource | [optional] 
**DataTypeOperators** | Pointer to [**map[string][]Operator**](array.md) | It&#x60;s a map of Operator list index by DataType | [optional] 

## Methods

### NewSettings

`func NewSettings(locales []string, ) *Settings`

NewSettings instantiates a new Settings object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSettingsWithDefaults

`func NewSettingsWithDefaults() *Settings`

NewSettingsWithDefaults instantiates a new Settings object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLocales

`func (o *Settings) GetLocales() []string`

GetLocales returns the Locales field if non-nil, zero value otherwise.

### GetLocalesOk

`func (o *Settings) GetLocalesOk() (*[]string, bool)`

GetLocalesOk returns a tuple with the Locales field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocales

`func (o *Settings) SetLocales(v []string)`

SetLocales sets Locales field to given value.


### GetDataSources

`func (o *Settings) GetDataSources() map[string]DataSourceProperties`

GetDataSources returns the DataSources field if non-nil, zero value otherwise.

### GetDataSourcesOk

`func (o *Settings) GetDataSourcesOk() (*map[string]DataSourceProperties, bool)`

GetDataSourcesOk returns a tuple with the DataSources field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataSources

`func (o *Settings) SetDataSources(v map[string]DataSourceProperties)`

SetDataSources sets DataSources field to given value.

### HasDataSources

`func (o *Settings) HasDataSources() bool`

HasDataSources returns a boolean if a field has been set.

### GetDataTypeOperators

`func (o *Settings) GetDataTypeOperators() map[string][]Operator`

GetDataTypeOperators returns the DataTypeOperators field if non-nil, zero value otherwise.

### GetDataTypeOperatorsOk

`func (o *Settings) GetDataTypeOperatorsOk() (*map[string][]Operator, bool)`

GetDataTypeOperatorsOk returns a tuple with the DataTypeOperators field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataTypeOperators

`func (o *Settings) SetDataTypeOperators(v map[string][]Operator)`

SetDataTypeOperators sets DataTypeOperators field to given value.

### HasDataTypeOperators

`func (o *Settings) HasDataTypeOperators() bool`

HasDataTypeOperators returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


