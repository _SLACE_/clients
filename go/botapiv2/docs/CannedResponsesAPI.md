# \CannedResponsesAPI

All URIs are relative to *https://bot-v2.slace.dev/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CannedResponseParams**](CannedResponsesAPI.md#CannedResponseParams) | **Post** /channels/{channel_id}/canned-responses/{id}/params | Get canned response params
[**SendCannedResponse**](CannedResponsesAPI.md#SendCannedResponse) | **Post** /channels/{channel_id}/canned-responses/{id}/send | Send canned response



## CannedResponseParams

> map[string]interface{} CannedResponseParams(ctx, channelId, id).CannedResponseStartRequest(cannedResponseStartRequest).Execute()

Get canned response params

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 
    cannedResponseStartRequest := *openapiclient.NewCannedResponseStartRequest("ConversationId_example", "Language_example") // CannedResponseStartRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CannedResponsesAPI.CannedResponseParams(context.Background(), channelId, id).CannedResponseStartRequest(cannedResponseStartRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CannedResponsesAPI.CannedResponseParams``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CannedResponseParams`: map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `CannedResponsesAPI.CannedResponseParams`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCannedResponseParamsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **cannedResponseStartRequest** | [**CannedResponseStartRequest**](CannedResponseStartRequest.md) |  | 

### Return type

**map[string]interface{}**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SendCannedResponse

> GenericResponse SendCannedResponse(ctx, channelId, id).CannedResponseStartRequest(cannedResponseStartRequest).Execute()

Send canned response

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 
    cannedResponseStartRequest := *openapiclient.NewCannedResponseStartRequest("ConversationId_example", "Language_example") // CannedResponseStartRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CannedResponsesAPI.SendCannedResponse(context.Background(), channelId, id).CannedResponseStartRequest(cannedResponseStartRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CannedResponsesAPI.SendCannedResponse``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SendCannedResponse`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `CannedResponsesAPI.SendCannedResponse`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiSendCannedResponseRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **cannedResponseStartRequest** | [**CannedResponseStartRequest**](CannedResponseStartRequest.md) |  | 

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

