# \ScriptsAPI

All URIs are relative to *https://bot-v2.slace.dev/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CloneScript**](ScriptsAPI.md#CloneScript) | **Post** /channels/{channel_id}/scripts/{script_id}/clone | Clone script
[**CreateScript**](ScriptsAPI.md#CreateScript) | **Post** /channels/{channel_id}/scripts | Create script
[**DeleteScript**](ScriptsAPI.md#DeleteScript) | **Delete** /channels/{channel_id}/scripts/{script_id} | Delete Script
[**GenerateOpeningTemplates**](ScriptsAPI.md#GenerateOpeningTemplates) | **Post** /channels/{channel_id}/scripts/{script_id}/opening-templates | Generate Opening Templates
[**GetScript**](ScriptsAPI.md#GetScript) | **Get** /channels/{channel_id}/scripts/{script_id} | Get Script
[**GetScriptUsedParams**](ScriptsAPI.md#GetScriptUsedParams) | **Get** /channels/{channel_id}/scripts/{script_id}/used-params | List used parameters
[**GetSettings**](ScriptsAPI.md#GetSettings) | **Get** /channels/{channel_id}/settings | Get settings
[**ListScripts**](ScriptsAPI.md#ListScripts) | **Get** /channels/{channel_id}/scripts | List scripts
[**RequestOpeningTemplates**](ScriptsAPI.md#RequestOpeningTemplates) | **Post** /channels/{channel_id}/scripts/{script_id}/request-opening-templates | Request opening templates
[**StartScript**](ScriptsAPI.md#StartScript) | **Post** /channels/{channel_id}/scripts/{script_id}/start | Start script
[**UpdateScript**](ScriptsAPI.md#UpdateScript) | **Put** /channels/{channel_id}/scripts/{script_id} | Update script



## CloneScript

> CloneResult CloneScript(ctx, channelId, scriptId).CloneOptions(cloneOptions).Execute()

Clone script

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    scriptId := "scriptId_example" // string | 
    cloneOptions := *openapiclient.NewCloneOptions([]string{"ChannelIds_example"}) // CloneOptions |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ScriptsAPI.CloneScript(context.Background(), channelId, scriptId).CloneOptions(cloneOptions).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScriptsAPI.CloneScript``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CloneScript`: CloneResult
    fmt.Fprintf(os.Stdout, "Response from `ScriptsAPI.CloneScript`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**scriptId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCloneScriptRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **cloneOptions** | [**CloneOptions**](CloneOptions.md) |  | 

### Return type

[**CloneResult**](CloneResult.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateScript

> Script CreateScript(ctx, channelId).ScriptMutable(scriptMutable).Execute()

Create script



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    scriptMutable := *openapiclient.NewScriptMutable() // ScriptMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ScriptsAPI.CreateScript(context.Background(), channelId).ScriptMutable(scriptMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScriptsAPI.CreateScript``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateScript`: Script
    fmt.Fprintf(os.Stdout, "Response from `ScriptsAPI.CreateScript`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateScriptRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **scriptMutable** | [**ScriptMutable**](ScriptMutable.md) |  | 

### Return type

[**Script**](Script.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteScript

> GenericResponse DeleteScript(ctx, channelId, scriptId).Execute()

Delete Script



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    scriptId := "scriptId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ScriptsAPI.DeleteScript(context.Background(), channelId, scriptId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScriptsAPI.DeleteScript``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteScript`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `ScriptsAPI.DeleteScript`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**scriptId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteScriptRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GenerateOpeningTemplates

> Script GenerateOpeningTemplates(ctx, channelId, scriptId).Execute()

Generate Opening Templates



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    scriptId := "scriptId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ScriptsAPI.GenerateOpeningTemplates(context.Background(), channelId, scriptId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScriptsAPI.GenerateOpeningTemplates``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GenerateOpeningTemplates`: Script
    fmt.Fprintf(os.Stdout, "Response from `ScriptsAPI.GenerateOpeningTemplates`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**scriptId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGenerateOpeningTemplatesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Script**](Script.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetScript

> Script GetScript(ctx, channelId, scriptId).Execute()

Get Script



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    scriptId := "scriptId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ScriptsAPI.GetScript(context.Background(), channelId, scriptId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScriptsAPI.GetScript``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetScript`: Script
    fmt.Fprintf(os.Stdout, "Response from `ScriptsAPI.GetScript`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**scriptId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetScriptRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Script**](Script.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetScriptUsedParams

> []string GetScriptUsedParams(ctx, channelId, scriptId).Execute()

List used parameters

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    scriptId := "scriptId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ScriptsAPI.GetScriptUsedParams(context.Background(), channelId, scriptId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScriptsAPI.GetScriptUsedParams``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetScriptUsedParams`: []string
    fmt.Fprintf(os.Stdout, "Response from `ScriptsAPI.GetScriptUsedParams`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**scriptId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetScriptUsedParamsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

**[]string**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetSettings

> Settings GetSettings(ctx, channelId).Execute()

Get settings

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ScriptsAPI.GetSettings(context.Background(), channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScriptsAPI.GetSettings``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetSettings`: Settings
    fmt.Fprintf(os.Stdout, "Response from `ScriptsAPI.GetSettings`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetSettingsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Settings**](Settings.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListScripts

> ScriptsList ListScripts(ctx, channelId).Search(search).Locale(locale).Status(status).RestartAllowed(restartAllowed).StartTrigger(startTrigger).Sort(sort).Limit(limit).Offset(offset).CannedResponse(cannedResponse).Ids(ids).Execute()

List scripts



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    search := "search_example" // string |  (optional)
    locale := "locale_example" // string |  (optional)
    status := "status_example" // string |  (optional)
    restartAllowed := true // bool |  (optional)
    startTrigger := "startTrigger_example" // string |  (optional)
    sort := "sort_example" // string |  (optional)
    limit := int32(56) // int32 |  (optional)
    offset := int32(56) // int32 |  (optional)
    cannedResponse := true // bool |  (optional)
    ids := []string{"Inner_example"} // []string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ScriptsAPI.ListScripts(context.Background(), channelId).Search(search).Locale(locale).Status(status).RestartAllowed(restartAllowed).StartTrigger(startTrigger).Sort(sort).Limit(limit).Offset(offset).CannedResponse(cannedResponse).Ids(ids).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScriptsAPI.ListScripts``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListScripts`: ScriptsList
    fmt.Fprintf(os.Stdout, "Response from `ScriptsAPI.ListScripts`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListScriptsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **search** | **string** |  | 
 **locale** | **string** |  | 
 **status** | **string** |  | 
 **restartAllowed** | **bool** |  | 
 **startTrigger** | **string** |  | 
 **sort** | **string** |  | 
 **limit** | **int32** |  | 
 **offset** | **int32** |  | 
 **cannedResponse** | **bool** |  | 
 **ids** | **[]string** |  | 

### Return type

[**ScriptsList**](ScriptsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RequestOpeningTemplates

> Script RequestOpeningTemplates(ctx, channelId, scriptId).Execute()

Request opening templates



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    scriptId := "scriptId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ScriptsAPI.RequestOpeningTemplates(context.Background(), channelId, scriptId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScriptsAPI.RequestOpeningTemplates``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RequestOpeningTemplates`: Script
    fmt.Fprintf(os.Stdout, "Response from `ScriptsAPI.RequestOpeningTemplates`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**scriptId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiRequestOpeningTemplatesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Script**](Script.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StartScript

> GenericResponse StartScript(ctx, channelId, scriptId).ScriptStartRequest(scriptStartRequest).Execute()

Start script

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    scriptId := "scriptId_example" // string | 
    scriptStartRequest := *openapiclient.NewScriptStartRequest("ConversationId_example") // ScriptStartRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ScriptsAPI.StartScript(context.Background(), channelId, scriptId).ScriptStartRequest(scriptStartRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScriptsAPI.StartScript``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StartScript`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `ScriptsAPI.StartScript`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**scriptId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiStartScriptRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **scriptStartRequest** | [**ScriptStartRequest**](ScriptStartRequest.md) |  | 

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateScript

> Script UpdateScript(ctx, channelId, scriptId).ScriptMutable(scriptMutable).Execute()

Update script



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    scriptId := "scriptId_example" // string | 
    scriptMutable := *openapiclient.NewScriptMutable() // ScriptMutable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ScriptsAPI.UpdateScript(context.Background(), channelId, scriptId).ScriptMutable(scriptMutable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ScriptsAPI.UpdateScript``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateScript`: Script
    fmt.Fprintf(os.Stdout, "Response from `ScriptsAPI.UpdateScript`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**scriptId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateScriptRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **scriptMutable** | [**ScriptMutable**](ScriptMutable.md) |  | 

### Return type

[**Script**](Script.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

