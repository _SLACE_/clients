# ValidationType

## Enum


* `ValidationTypeEmail` (value: `"email"`)

* `ValidationTypePhoneNumber` (value: `"phone_number"`)

* `ValidationTypeBirthdate` (value: `"birthdate"`)

* `ValidationTypeTextLength` (value: `"text_length"`)

* `ValidationTypeTextPrefix` (value: `"text_prefix"`)

* `ValidationTypeZipCode` (value: `"zip_code"`)

* `ValidationTypeMessageType` (value: `"message_type"`)

* `ValidationTypeBIC` (value: `"bic"`)

* `ValidationTypeIBAN` (value: `"iban"`)

* `ValidationTypeNumber` (value: `"number"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


