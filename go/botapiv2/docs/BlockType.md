# BlockType

## Enum


* `BlockTypeText` (value: `"text"`)

* `BlockTypeMedia` (value: `"media"`)

* `BlockTypeQuestion` (value: `"question"`)

* `BlockTypePause` (value: `"pause"`)

* `BlockTypeReplyButtons` (value: `"reply_buttons"`)

* `BlockTypeLinkButton` (value: `"link_button"`)

* `BlockTypeCallButton` (value: `"call_button"`)

* `BlockTypeActionButtons` (value: `"action_buttons"`)

* `BlockTypeReplyList` (value: `"reply_list"`)

* `BlockTypeDynamicList` (value: `"dynamic_list"`)

* `BlockTypeRequestLocation` (value: `"request_location"`)

* `BlockTypeMediaCards` (value: `"media_cards"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


