# WriterSource

## Enum


* `WriterSourceMessageText` (value: `"message_text"`)

* `WriterSourceStatic` (value: `"static"`)

* `MESSAGE_BUTTON_PAYLOAD` (value: `"message_button_payload"`)

* `MESSAGE_MEDIA` (value: `"message_media"`)

* `MESSAGE_LOCATION_LAT` (value: `"message_location_lat"`)

* `MESSAGE_LOCATION_LNG` (value: `"message_location_lng"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


