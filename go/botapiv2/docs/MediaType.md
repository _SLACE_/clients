# MediaType

## Enum


* `MediaTypeImage` (value: `"image"`)

* `MediaTypeVideo` (value: `"video"`)

* `MediaTypeAudio` (value: `"audio"`)

* `MediaTypeFile` (value: `"file"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


