# AssetTranslation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Language** | **string** |  | 
**Text** | Pointer to **string** |  | [optional] 
**Media** | Pointer to [**AssetMedia**](AssetMedia.md) |  | [optional] 
**Template** | Pointer to [**AssetTemplateData**](AssetTemplateData.md) |  | [optional] 
**AcceptedBy** | Pointer to **string** |  | [optional] 
**AcceptedAt** | Pointer to **time.Time** |  | [optional] 
**AutoShortened** | Pointer to **bool** |  | [optional] 

## Methods

### NewAssetTranslation

`func NewAssetTranslation(language string, ) *AssetTranslation`

NewAssetTranslation instantiates a new AssetTranslation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAssetTranslationWithDefaults

`func NewAssetTranslationWithDefaults() *AssetTranslation`

NewAssetTranslationWithDefaults instantiates a new AssetTranslation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLanguage

`func (o *AssetTranslation) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *AssetTranslation) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *AssetTranslation) SetLanguage(v string)`

SetLanguage sets Language field to given value.


### GetText

`func (o *AssetTranslation) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *AssetTranslation) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *AssetTranslation) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *AssetTranslation) HasText() bool`

HasText returns a boolean if a field has been set.

### GetMedia

`func (o *AssetTranslation) GetMedia() AssetMedia`

GetMedia returns the Media field if non-nil, zero value otherwise.

### GetMediaOk

`func (o *AssetTranslation) GetMediaOk() (*AssetMedia, bool)`

GetMediaOk returns a tuple with the Media field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMedia

`func (o *AssetTranslation) SetMedia(v AssetMedia)`

SetMedia sets Media field to given value.

### HasMedia

`func (o *AssetTranslation) HasMedia() bool`

HasMedia returns a boolean if a field has been set.

### GetTemplate

`func (o *AssetTranslation) GetTemplate() AssetTemplateData`

GetTemplate returns the Template field if non-nil, zero value otherwise.

### GetTemplateOk

`func (o *AssetTranslation) GetTemplateOk() (*AssetTemplateData, bool)`

GetTemplateOk returns a tuple with the Template field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplate

`func (o *AssetTranslation) SetTemplate(v AssetTemplateData)`

SetTemplate sets Template field to given value.

### HasTemplate

`func (o *AssetTranslation) HasTemplate() bool`

HasTemplate returns a boolean if a field has been set.

### GetAcceptedBy

`func (o *AssetTranslation) GetAcceptedBy() string`

GetAcceptedBy returns the AcceptedBy field if non-nil, zero value otherwise.

### GetAcceptedByOk

`func (o *AssetTranslation) GetAcceptedByOk() (*string, bool)`

GetAcceptedByOk returns a tuple with the AcceptedBy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAcceptedBy

`func (o *AssetTranslation) SetAcceptedBy(v string)`

SetAcceptedBy sets AcceptedBy field to given value.

### HasAcceptedBy

`func (o *AssetTranslation) HasAcceptedBy() bool`

HasAcceptedBy returns a boolean if a field has been set.

### GetAcceptedAt

`func (o *AssetTranslation) GetAcceptedAt() time.Time`

GetAcceptedAt returns the AcceptedAt field if non-nil, zero value otherwise.

### GetAcceptedAtOk

`func (o *AssetTranslation) GetAcceptedAtOk() (*time.Time, bool)`

GetAcceptedAtOk returns a tuple with the AcceptedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAcceptedAt

`func (o *AssetTranslation) SetAcceptedAt(v time.Time)`

SetAcceptedAt sets AcceptedAt field to given value.

### HasAcceptedAt

`func (o *AssetTranslation) HasAcceptedAt() bool`

HasAcceptedAt returns a boolean if a field has been set.

### GetAutoShortened

`func (o *AssetTranslation) GetAutoShortened() bool`

GetAutoShortened returns the AutoShortened field if non-nil, zero value otherwise.

### GetAutoShortenedOk

`func (o *AssetTranslation) GetAutoShortenedOk() (*bool, bool)`

GetAutoShortenedOk returns a tuple with the AutoShortened field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAutoShortened

`func (o *AssetTranslation) SetAutoShortened(v bool)`

SetAutoShortened sets AutoShortened field to given value.

### HasAutoShortened

`func (o *AssetTranslation) HasAutoShortened() bool`

HasAutoShortened returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


