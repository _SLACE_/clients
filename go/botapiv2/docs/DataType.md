# DataType

## Enum


* `DataTypeBool` (value: `"bool"`)

* `DataTypeInt` (value: `"int"`)

* `DataTypeFloat` (value: `"float"`)

* `DataTypeString` (value: `"string"`)

* `DataTypeStringCI` (value: `"string_ci"`)

* `DataTypeTime` (value: `"time"`)

* `DataTypeArray` (value: `"array"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


