# Formality

## Enum


* `LESS` (value: `"prefer_less"`)

* `MORE` (value: `"prefer_more"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


