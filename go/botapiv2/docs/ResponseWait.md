# ResponseWait

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | Pointer to **bool** |  | [optional] 
**Duration** | Pointer to **string** |  | [optional] 
**Reminders** | Pointer to [**[]Reminder**](Reminder.md) |  | [optional] 
**Validation** | Pointer to [**Validation**](Validation.md) |  | [optional] 
**Writers** | Pointer to [**[]Writer**](Writer.md) |  | [optional] 

## Methods

### NewResponseWait

`func NewResponseWait() *ResponseWait`

NewResponseWait instantiates a new ResponseWait object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewResponseWaitWithDefaults

`func NewResponseWaitWithDefaults() *ResponseWait`

NewResponseWaitWithDefaults instantiates a new ResponseWait object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetEnabled

`func (o *ResponseWait) GetEnabled() bool`

GetEnabled returns the Enabled field if non-nil, zero value otherwise.

### GetEnabledOk

`func (o *ResponseWait) GetEnabledOk() (*bool, bool)`

GetEnabledOk returns a tuple with the Enabled field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEnabled

`func (o *ResponseWait) SetEnabled(v bool)`

SetEnabled sets Enabled field to given value.

### HasEnabled

`func (o *ResponseWait) HasEnabled() bool`

HasEnabled returns a boolean if a field has been set.

### GetDuration

`func (o *ResponseWait) GetDuration() string`

GetDuration returns the Duration field if non-nil, zero value otherwise.

### GetDurationOk

`func (o *ResponseWait) GetDurationOk() (*string, bool)`

GetDurationOk returns a tuple with the Duration field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDuration

`func (o *ResponseWait) SetDuration(v string)`

SetDuration sets Duration field to given value.

### HasDuration

`func (o *ResponseWait) HasDuration() bool`

HasDuration returns a boolean if a field has been set.

### GetReminders

`func (o *ResponseWait) GetReminders() []Reminder`

GetReminders returns the Reminders field if non-nil, zero value otherwise.

### GetRemindersOk

`func (o *ResponseWait) GetRemindersOk() (*[]Reminder, bool)`

GetRemindersOk returns a tuple with the Reminders field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReminders

`func (o *ResponseWait) SetReminders(v []Reminder)`

SetReminders sets Reminders field to given value.

### HasReminders

`func (o *ResponseWait) HasReminders() bool`

HasReminders returns a boolean if a field has been set.

### GetValidation

`func (o *ResponseWait) GetValidation() Validation`

GetValidation returns the Validation field if non-nil, zero value otherwise.

### GetValidationOk

`func (o *ResponseWait) GetValidationOk() (*Validation, bool)`

GetValidationOk returns a tuple with the Validation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidation

`func (o *ResponseWait) SetValidation(v Validation)`

SetValidation sets Validation field to given value.

### HasValidation

`func (o *ResponseWait) HasValidation() bool`

HasValidation returns a boolean if a field has been set.

### GetWriters

`func (o *ResponseWait) GetWriters() []Writer`

GetWriters returns the Writers field if non-nil, zero value otherwise.

### GetWritersOk

`func (o *ResponseWait) GetWritersOk() (*[]Writer, bool)`

GetWritersOk returns a tuple with the Writers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWriters

`func (o *ResponseWait) SetWriters(v []Writer)`

SetWriters sets Writers field to given value.

### HasWriters

`func (o *ResponseWait) HasWriters() bool`

HasWriters returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


