# CannedResponseStartRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConversationId** | **string** |  | 
**Language** | **string** |  | 
**Params** | Pointer to **map[string]interface{}** |  | [optional] 

## Methods

### NewCannedResponseStartRequest

`func NewCannedResponseStartRequest(conversationId string, language string, ) *CannedResponseStartRequest`

NewCannedResponseStartRequest instantiates a new CannedResponseStartRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCannedResponseStartRequestWithDefaults

`func NewCannedResponseStartRequestWithDefaults() *CannedResponseStartRequest`

NewCannedResponseStartRequestWithDefaults instantiates a new CannedResponseStartRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetConversationId

`func (o *CannedResponseStartRequest) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *CannedResponseStartRequest) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *CannedResponseStartRequest) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetLanguage

`func (o *CannedResponseStartRequest) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *CannedResponseStartRequest) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *CannedResponseStartRequest) SetLanguage(v string)`

SetLanguage sets Language field to given value.


### GetParams

`func (o *CannedResponseStartRequest) GetParams() map[string]interface{}`

GetParams returns the Params field if non-nil, zero value otherwise.

### GetParamsOk

`func (o *CannedResponseStartRequest) GetParamsOk() (*map[string]interface{}, bool)`

GetParamsOk returns a tuple with the Params field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParams

`func (o *CannedResponseStartRequest) SetParams(v map[string]interface{})`

SetParams sets Params field to given value.

### HasParams

`func (o *CannedResponseStartRequest) HasParams() bool`

HasParams returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


