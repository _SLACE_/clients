# AssetMedia

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Url** | Pointer to **string** |  | [optional] 
**Source** | [**AssetMediaSource**](AssetMediaSource.md) |  | 
**Filename** | Pointer to **string** | Only in document (file) assets | [optional] 

## Methods

### NewAssetMedia

`func NewAssetMedia(source AssetMediaSource, ) *AssetMedia`

NewAssetMedia instantiates a new AssetMedia object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAssetMediaWithDefaults

`func NewAssetMediaWithDefaults() *AssetMedia`

NewAssetMediaWithDefaults instantiates a new AssetMedia object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *AssetMedia) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *AssetMedia) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *AssetMedia) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *AssetMedia) HasId() bool`

HasId returns a boolean if a field has been set.

### GetUrl

`func (o *AssetMedia) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *AssetMedia) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *AssetMedia) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *AssetMedia) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetSource

`func (o *AssetMedia) GetSource() AssetMediaSource`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *AssetMedia) GetSourceOk() (*AssetMediaSource, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *AssetMedia) SetSource(v AssetMediaSource)`

SetSource sets Source field to given value.


### GetFilename

`func (o *AssetMedia) GetFilename() string`

GetFilename returns the Filename field if non-nil, zero value otherwise.

### GetFilenameOk

`func (o *AssetMedia) GetFilenameOk() (*string, bool)`

GetFilenameOk returns a tuple with the Filename field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFilename

`func (o *AssetMedia) SetFilename(v string)`

SetFilename sets Filename field to given value.

### HasFilename

`func (o *AssetMedia) HasFilename() bool`

HasFilename returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


