# AssetMediaSource

## Enum


* `AssetMediaSourceUrl` (value: `"url"`)

* `AssetMediaSourceFile` (value: `"file"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


