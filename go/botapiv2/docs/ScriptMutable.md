# ScriptMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**MaxQuestions** | Pointer to **int32** |  | [optional] 
**Status** | Pointer to **string** |  | [optional] 
**RestartAllowed** | Pointer to **bool** |  | [optional] 
**Locales** | Pointer to [**[]Locale**](Locale.md) |  | [optional] 
**StartTrigger** | Pointer to [**StartTrigger**](StartTrigger.md) |  | [optional] 
**StartCondition** | Pointer to [**Condition**](Condition.md) |  | [optional] 
**Blocks** | Pointer to [**[]Block**](Block.md) |  | [optional] 
**Assets** | Pointer to [**[]Asset**](Asset.md) |  | [optional] 
**Formality** | Pointer to [**Formality**](Formality.md) |  | [optional] 

## Methods

### NewScriptMutable

`func NewScriptMutable() *ScriptMutable`

NewScriptMutable instantiates a new ScriptMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScriptMutableWithDefaults

`func NewScriptMutableWithDefaults() *ScriptMutable`

NewScriptMutableWithDefaults instantiates a new ScriptMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ScriptMutable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ScriptMutable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ScriptMutable) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ScriptMutable) HasName() bool`

HasName returns a boolean if a field has been set.

### GetMaxQuestions

`func (o *ScriptMutable) GetMaxQuestions() int32`

GetMaxQuestions returns the MaxQuestions field if non-nil, zero value otherwise.

### GetMaxQuestionsOk

`func (o *ScriptMutable) GetMaxQuestionsOk() (*int32, bool)`

GetMaxQuestionsOk returns a tuple with the MaxQuestions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxQuestions

`func (o *ScriptMutable) SetMaxQuestions(v int32)`

SetMaxQuestions sets MaxQuestions field to given value.

### HasMaxQuestions

`func (o *ScriptMutable) HasMaxQuestions() bool`

HasMaxQuestions returns a boolean if a field has been set.

### GetStatus

`func (o *ScriptMutable) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *ScriptMutable) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *ScriptMutable) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *ScriptMutable) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetRestartAllowed

`func (o *ScriptMutable) GetRestartAllowed() bool`

GetRestartAllowed returns the RestartAllowed field if non-nil, zero value otherwise.

### GetRestartAllowedOk

`func (o *ScriptMutable) GetRestartAllowedOk() (*bool, bool)`

GetRestartAllowedOk returns a tuple with the RestartAllowed field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRestartAllowed

`func (o *ScriptMutable) SetRestartAllowed(v bool)`

SetRestartAllowed sets RestartAllowed field to given value.

### HasRestartAllowed

`func (o *ScriptMutable) HasRestartAllowed() bool`

HasRestartAllowed returns a boolean if a field has been set.

### GetLocales

`func (o *ScriptMutable) GetLocales() []Locale`

GetLocales returns the Locales field if non-nil, zero value otherwise.

### GetLocalesOk

`func (o *ScriptMutable) GetLocalesOk() (*[]Locale, bool)`

GetLocalesOk returns a tuple with the Locales field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocales

`func (o *ScriptMutable) SetLocales(v []Locale)`

SetLocales sets Locales field to given value.

### HasLocales

`func (o *ScriptMutable) HasLocales() bool`

HasLocales returns a boolean if a field has been set.

### GetStartTrigger

`func (o *ScriptMutable) GetStartTrigger() StartTrigger`

GetStartTrigger returns the StartTrigger field if non-nil, zero value otherwise.

### GetStartTriggerOk

`func (o *ScriptMutable) GetStartTriggerOk() (*StartTrigger, bool)`

GetStartTriggerOk returns a tuple with the StartTrigger field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartTrigger

`func (o *ScriptMutable) SetStartTrigger(v StartTrigger)`

SetStartTrigger sets StartTrigger field to given value.

### HasStartTrigger

`func (o *ScriptMutable) HasStartTrigger() bool`

HasStartTrigger returns a boolean if a field has been set.

### GetStartCondition

`func (o *ScriptMutable) GetStartCondition() Condition`

GetStartCondition returns the StartCondition field if non-nil, zero value otherwise.

### GetStartConditionOk

`func (o *ScriptMutable) GetStartConditionOk() (*Condition, bool)`

GetStartConditionOk returns a tuple with the StartCondition field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartCondition

`func (o *ScriptMutable) SetStartCondition(v Condition)`

SetStartCondition sets StartCondition field to given value.

### HasStartCondition

`func (o *ScriptMutable) HasStartCondition() bool`

HasStartCondition returns a boolean if a field has been set.

### GetBlocks

`func (o *ScriptMutable) GetBlocks() []Block`

GetBlocks returns the Blocks field if non-nil, zero value otherwise.

### GetBlocksOk

`func (o *ScriptMutable) GetBlocksOk() (*[]Block, bool)`

GetBlocksOk returns a tuple with the Blocks field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlocks

`func (o *ScriptMutable) SetBlocks(v []Block)`

SetBlocks sets Blocks field to given value.

### HasBlocks

`func (o *ScriptMutable) HasBlocks() bool`

HasBlocks returns a boolean if a field has been set.

### GetAssets

`func (o *ScriptMutable) GetAssets() []Asset`

GetAssets returns the Assets field if non-nil, zero value otherwise.

### GetAssetsOk

`func (o *ScriptMutable) GetAssetsOk() (*[]Asset, bool)`

GetAssetsOk returns a tuple with the Assets field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssets

`func (o *ScriptMutable) SetAssets(v []Asset)`

SetAssets sets Assets field to given value.

### HasAssets

`func (o *ScriptMutable) HasAssets() bool`

HasAssets returns a boolean if a field has been set.

### GetFormality

`func (o *ScriptMutable) GetFormality() Formality`

GetFormality returns the Formality field if non-nil, zero value otherwise.

### GetFormalityOk

`func (o *ScriptMutable) GetFormalityOk() (*Formality, bool)`

GetFormalityOk returns a tuple with the Formality field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormality

`func (o *ScriptMutable) SetFormality(v Formality)`

SetFormality sets Formality field to given value.

### HasFormality

`func (o *ScriptMutable) HasFormality() bool`

HasFormality returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


