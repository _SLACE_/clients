# ExpressionValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Source** | [**DataSource**](DataSource.md) |  | 
**Value** | **string** | It&#39;s possible to use &#x60;||&#x60; separator in which case value will be treated as array. Usefull for operators like &#x60;oneOf&#x60; and similar. | 

## Methods

### NewExpressionValue

`func NewExpressionValue(source DataSource, value string, ) *ExpressionValue`

NewExpressionValue instantiates a new ExpressionValue object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExpressionValueWithDefaults

`func NewExpressionValueWithDefaults() *ExpressionValue`

NewExpressionValueWithDefaults instantiates a new ExpressionValue object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSource

`func (o *ExpressionValue) GetSource() DataSource`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *ExpressionValue) GetSourceOk() (*DataSource, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *ExpressionValue) SetSource(v DataSource)`

SetSource sets Source field to given value.


### GetValue

`func (o *ExpressionValue) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *ExpressionValue) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *ExpressionValue) SetValue(v string)`

SetValue sets Value field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


