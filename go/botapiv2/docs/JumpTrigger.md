# JumpTrigger

## Enum


* `JumpTriggerMessageReceived` (value: `"message_received"`)

* `JumpTriggerWaitReached` (value: `"wait_reached"`)

* `JumpTriggerExternalEvent` (value: `"external_event"`)

* `JumpTriggerButton` (value: `"button"`)

* `JumpTriggerSuccess` (value: `"success"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


