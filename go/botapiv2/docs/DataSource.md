# DataSource

## Enum


* `DataSourceStatic` (value: `"static"`)

* `DataSourceEvent` (value: `"event"`)

* `DataSourceContact` (value: `"contact"`)

* `DataSourceTx` (value: `"tx"`)

* `DataSourceCode` (value: `"code"`)

* `DataSourceSegments` (value: `"segments"`)

* `DataSourceLabels` (value: `"labels"`)

* `DataSourceSession` (value: `"session"`)

* `DataSourceMessage` (value: `"message"`)

* `DataSourceMobileDevice` (value: `"mobile_device"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


