# Reminder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | Pointer to [**Message**](Message.md) |  | [optional] 
**ExtendWait** | Pointer to **string** |  | [optional] 

## Methods

### NewReminder

`func NewReminder() *Reminder`

NewReminder instantiates a new Reminder object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReminderWithDefaults

`func NewReminderWithDefaults() *Reminder`

NewReminderWithDefaults instantiates a new Reminder object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessage

`func (o *Reminder) GetMessage() Message`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *Reminder) GetMessageOk() (*Message, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *Reminder) SetMessage(v Message)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *Reminder) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetExtendWait

`func (o *Reminder) GetExtendWait() string`

GetExtendWait returns the ExtendWait field if non-nil, zero value otherwise.

### GetExtendWaitOk

`func (o *Reminder) GetExtendWaitOk() (*string, bool)`

GetExtendWaitOk returns a tuple with the ExtendWait field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtendWait

`func (o *Reminder) SetExtendWait(v string)`

SetExtendWait sets ExtendWait field to given value.

### HasExtendWait

`func (o *Reminder) HasExtendWait() bool`

HasExtendWait returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


