# MessageButton

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Payload** | Pointer to **string** |  | [optional] 
**Type** | [**MessageButtonType**](MessageButtonType.md) |  | 
**TextId** | **string** | Id of text asset | 
**DescriptionId** | Pointer to **string** | Id of text asset | [optional] 
**UrlOptions** | Pointer to [**MessageButtonUrlOptions**](MessageButtonUrlOptions.md) |  | [optional] 

## Methods

### NewMessageButton

`func NewMessageButton(id string, type_ MessageButtonType, textId string, ) *MessageButton`

NewMessageButton instantiates a new MessageButton object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageButtonWithDefaults

`func NewMessageButtonWithDefaults() *MessageButton`

NewMessageButtonWithDefaults instantiates a new MessageButton object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *MessageButton) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *MessageButton) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *MessageButton) SetId(v string)`

SetId sets Id field to given value.


### GetPayload

`func (o *MessageButton) GetPayload() string`

GetPayload returns the Payload field if non-nil, zero value otherwise.

### GetPayloadOk

`func (o *MessageButton) GetPayloadOk() (*string, bool)`

GetPayloadOk returns a tuple with the Payload field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayload

`func (o *MessageButton) SetPayload(v string)`

SetPayload sets Payload field to given value.

### HasPayload

`func (o *MessageButton) HasPayload() bool`

HasPayload returns a boolean if a field has been set.

### GetType

`func (o *MessageButton) GetType() MessageButtonType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MessageButton) GetTypeOk() (*MessageButtonType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MessageButton) SetType(v MessageButtonType)`

SetType sets Type field to given value.


### GetTextId

`func (o *MessageButton) GetTextId() string`

GetTextId returns the TextId field if non-nil, zero value otherwise.

### GetTextIdOk

`func (o *MessageButton) GetTextIdOk() (*string, bool)`

GetTextIdOk returns a tuple with the TextId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTextId

`func (o *MessageButton) SetTextId(v string)`

SetTextId sets TextId field to given value.


### GetDescriptionId

`func (o *MessageButton) GetDescriptionId() string`

GetDescriptionId returns the DescriptionId field if non-nil, zero value otherwise.

### GetDescriptionIdOk

`func (o *MessageButton) GetDescriptionIdOk() (*string, bool)`

GetDescriptionIdOk returns a tuple with the DescriptionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescriptionId

`func (o *MessageButton) SetDescriptionId(v string)`

SetDescriptionId sets DescriptionId field to given value.

### HasDescriptionId

`func (o *MessageButton) HasDescriptionId() bool`

HasDescriptionId returns a boolean if a field has been set.

### GetUrlOptions

`func (o *MessageButton) GetUrlOptions() MessageButtonUrlOptions`

GetUrlOptions returns the UrlOptions field if non-nil, zero value otherwise.

### GetUrlOptionsOk

`func (o *MessageButton) GetUrlOptionsOk() (*MessageButtonUrlOptions, bool)`

GetUrlOptionsOk returns a tuple with the UrlOptions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrlOptions

`func (o *MessageButton) SetUrlOptions(v MessageButtonUrlOptions)`

SetUrlOptions sets UrlOptions field to given value.

### HasUrlOptions

`func (o *MessageButton) HasUrlOptions() bool`

HasUrlOptions returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


