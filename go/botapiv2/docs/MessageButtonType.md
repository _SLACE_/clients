# MessageButtonType

## Enum


* `URL` (value: `"url"`)

* `PHONE_NUMBER` (value: `"phone_number"`)

* `CALLBACK` (value: `"callback"`)

* `SHARE_PHONE` (value: `"share_phone"`)

* `SHARE_LOCATION` (value: `"share_location"`)

* `KEYBOARD_CALLBACK` (value: `"keyboard_callback"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


