# \MediaAPI

All URIs are relative to *https://bot-v2.slace.dev/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteFile**](MediaAPI.md#DeleteFile) | **Delete** /channels/{channel_id}/media/{file_id} | Delete File
[**GetFile**](MediaAPI.md#GetFile) | **Get** /channels/{channel_id}/media/{file_id} | Get file
[**ListMediaFiles**](MediaAPI.md#ListMediaFiles) | **Get** /channels/{channel_id}/media | List media files
[**UploadNewFile**](MediaAPI.md#UploadNewFile) | **Post** /channels/{channel_id}/media | Upload new file



## DeleteFile

> GenericResponse DeleteFile(ctx, channelId, fileId).Execute()

Delete File



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    fileId := "fileId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaAPI.DeleteFile(context.Background(), channelId, fileId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaAPI.DeleteFile``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteFile`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `MediaAPI.DeleteFile`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**fileId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteFileRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetFile

> File GetFile(ctx, channelId, fileId).Execute()

Get file



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    fileId := "fileId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaAPI.GetFile(context.Background(), channelId, fileId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaAPI.GetFile``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetFile`: File
    fmt.Fprintf(os.Stdout, "Response from `MediaAPI.GetFile`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**fileId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetFileRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**File**](File.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListMediaFiles

> FilesList ListMediaFiles(ctx, channelId).Execute()

List media files



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaAPI.ListMediaFiles(context.Background(), channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaAPI.ListMediaFiles``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListMediaFiles`: FilesList
    fmt.Fprintf(os.Stdout, "Response from `MediaAPI.ListMediaFiles`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListMediaFilesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**FilesList**](FilesList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UploadNewFile

> File UploadNewFile(ctx, channelId).MediaType(mediaType).UploadNewFileRequest(uploadNewFileRequest).Execute()

Upload new file

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/botapiv2"
)

func main() {
    channelId := "channelId_example" // string | 
    mediaType := "mediaType_example" // string | 
    uploadNewFileRequest := *openapiclient.NewUploadNewFileRequest("Url_example") // UploadNewFileRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaAPI.UploadNewFile(context.Background(), channelId).MediaType(mediaType).UploadNewFileRequest(uploadNewFileRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaAPI.UploadNewFile``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UploadNewFile`: File
    fmt.Fprintf(os.Stdout, "Response from `MediaAPI.UploadNewFile`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUploadNewFileRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **mediaType** | **string** |  | 
 **uploadNewFileRequest** | [**UploadNewFileRequest**](UploadNewFileRequest.md) |  | 

### Return type

[**File**](File.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json, multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

