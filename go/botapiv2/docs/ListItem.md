# ListItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Text** | Pointer to **string** |  | [optional] 
**Payload** | Pointer to **string** |  | [optional] 
**Description** | Pointer to **string** |  | [optional] 

## Methods

### NewListItem

`func NewListItem() *ListItem`

NewListItem instantiates a new ListItem object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListItemWithDefaults

`func NewListItemWithDefaults() *ListItem`

NewListItemWithDefaults instantiates a new ListItem object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ListItem) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ListItem) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ListItem) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *ListItem) HasId() bool`

HasId returns a boolean if a field has been set.

### GetText

`func (o *ListItem) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *ListItem) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *ListItem) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *ListItem) HasText() bool`

HasText returns a boolean if a field has been set.

### GetPayload

`func (o *ListItem) GetPayload() string`

GetPayload returns the Payload field if non-nil, zero value otherwise.

### GetPayloadOk

`func (o *ListItem) GetPayloadOk() (*string, bool)`

GetPayloadOk returns a tuple with the Payload field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayload

`func (o *ListItem) SetPayload(v string)`

SetPayload sets Payload field to given value.

### HasPayload

`func (o *ListItem) HasPayload() bool`

HasPayload returns a boolean if a field has been set.

### GetDescription

`func (o *ListItem) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *ListItem) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *ListItem) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *ListItem) HasDescription() bool`

HasDescription returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


