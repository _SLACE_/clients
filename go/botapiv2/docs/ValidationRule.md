# ValidationRule

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to [**ValidationType**](ValidationType.md) |  | [optional] 
**Min** | Pointer to **int32** |  | [optional] 
**Max** | Pointer to **int32** |  | [optional] 
**Country** | Pointer to **string** | country code for zip code validation | [optional] 
**List** | Pointer to **[]string** |  | [optional] 

## Methods

### NewValidationRule

`func NewValidationRule() *ValidationRule`

NewValidationRule instantiates a new ValidationRule object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewValidationRuleWithDefaults

`func NewValidationRuleWithDefaults() *ValidationRule`

NewValidationRuleWithDefaults instantiates a new ValidationRule object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *ValidationRule) GetType() ValidationType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ValidationRule) GetTypeOk() (*ValidationType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ValidationRule) SetType(v ValidationType)`

SetType sets Type field to given value.

### HasType

`func (o *ValidationRule) HasType() bool`

HasType returns a boolean if a field has been set.

### GetMin

`func (o *ValidationRule) GetMin() int32`

GetMin returns the Min field if non-nil, zero value otherwise.

### GetMinOk

`func (o *ValidationRule) GetMinOk() (*int32, bool)`

GetMinOk returns a tuple with the Min field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMin

`func (o *ValidationRule) SetMin(v int32)`

SetMin sets Min field to given value.

### HasMin

`func (o *ValidationRule) HasMin() bool`

HasMin returns a boolean if a field has been set.

### GetMax

`func (o *ValidationRule) GetMax() int32`

GetMax returns the Max field if non-nil, zero value otherwise.

### GetMaxOk

`func (o *ValidationRule) GetMaxOk() (*int32, bool)`

GetMaxOk returns a tuple with the Max field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMax

`func (o *ValidationRule) SetMax(v int32)`

SetMax sets Max field to given value.

### HasMax

`func (o *ValidationRule) HasMax() bool`

HasMax returns a boolean if a field has been set.

### GetCountry

`func (o *ValidationRule) GetCountry() string`

GetCountry returns the Country field if non-nil, zero value otherwise.

### GetCountryOk

`func (o *ValidationRule) GetCountryOk() (*string, bool)`

GetCountryOk returns a tuple with the Country field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountry

`func (o *ValidationRule) SetCountry(v string)`

SetCountry sets Country field to given value.

### HasCountry

`func (o *ValidationRule) HasCountry() bool`

HasCountry returns a boolean if a field has been set.

### GetList

`func (o *ValidationRule) GetList() []string`

GetList returns the List field if non-nil, zero value otherwise.

### GetListOk

`func (o *ValidationRule) GetListOk() (*[]string, bool)`

GetListOk returns a tuple with the List field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetList

`func (o *ValidationRule) SetList(v []string)`

SetList sets List field to given value.

### HasList

`func (o *ValidationRule) HasList() bool`

HasList returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


