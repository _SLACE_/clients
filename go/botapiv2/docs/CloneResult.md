# CloneResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChannelsNotAvailable** | Pointer to **[]string** |  | [optional] 
**ValidationErrors** | Pointer to **[]string** |  | [optional] 
**Issues** | Pointer to **[]string** |  | [optional] 
**NewScripts** | Pointer to [**[]ClonedScript**](ClonedScript.md) |  | [optional] 
**Success** | **bool** |  | 

## Methods

### NewCloneResult

`func NewCloneResult(success bool, ) *CloneResult`

NewCloneResult instantiates a new CloneResult object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCloneResultWithDefaults

`func NewCloneResultWithDefaults() *CloneResult`

NewCloneResultWithDefaults instantiates a new CloneResult object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetChannelsNotAvailable

`func (o *CloneResult) GetChannelsNotAvailable() []string`

GetChannelsNotAvailable returns the ChannelsNotAvailable field if non-nil, zero value otherwise.

### GetChannelsNotAvailableOk

`func (o *CloneResult) GetChannelsNotAvailableOk() (*[]string, bool)`

GetChannelsNotAvailableOk returns a tuple with the ChannelsNotAvailable field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelsNotAvailable

`func (o *CloneResult) SetChannelsNotAvailable(v []string)`

SetChannelsNotAvailable sets ChannelsNotAvailable field to given value.

### HasChannelsNotAvailable

`func (o *CloneResult) HasChannelsNotAvailable() bool`

HasChannelsNotAvailable returns a boolean if a field has been set.

### GetValidationErrors

`func (o *CloneResult) GetValidationErrors() []string`

GetValidationErrors returns the ValidationErrors field if non-nil, zero value otherwise.

### GetValidationErrorsOk

`func (o *CloneResult) GetValidationErrorsOk() (*[]string, bool)`

GetValidationErrorsOk returns a tuple with the ValidationErrors field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValidationErrors

`func (o *CloneResult) SetValidationErrors(v []string)`

SetValidationErrors sets ValidationErrors field to given value.

### HasValidationErrors

`func (o *CloneResult) HasValidationErrors() bool`

HasValidationErrors returns a boolean if a field has been set.

### GetIssues

`func (o *CloneResult) GetIssues() []string`

GetIssues returns the Issues field if non-nil, zero value otherwise.

### GetIssuesOk

`func (o *CloneResult) GetIssuesOk() (*[]string, bool)`

GetIssuesOk returns a tuple with the Issues field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIssues

`func (o *CloneResult) SetIssues(v []string)`

SetIssues sets Issues field to given value.

### HasIssues

`func (o *CloneResult) HasIssues() bool`

HasIssues returns a boolean if a field has been set.

### GetNewScripts

`func (o *CloneResult) GetNewScripts() []ClonedScript`

GetNewScripts returns the NewScripts field if non-nil, zero value otherwise.

### GetNewScriptsOk

`func (o *CloneResult) GetNewScriptsOk() (*[]ClonedScript, bool)`

GetNewScriptsOk returns a tuple with the NewScripts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewScripts

`func (o *CloneResult) SetNewScripts(v []ClonedScript)`

SetNewScripts sets NewScripts field to given value.

### HasNewScripts

`func (o *CloneResult) HasNewScripts() bool`

HasNewScripts returns a boolean if a field has been set.

### GetSuccess

`func (o *CloneResult) GetSuccess() bool`

GetSuccess returns the Success field if non-nil, zero value otherwise.

### GetSuccessOk

`func (o *CloneResult) GetSuccessOk() (*bool, bool)`

GetSuccessOk returns a tuple with the Success field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSuccess

`func (o *CloneResult) SetSuccess(v bool)`

SetSuccess sets Success field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


