# Block

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Type** | [**BlockType**](BlockType.md) |  | 
**Name** | **string** |  | 
**Enabled** | **bool** |  | 
**Messages** | Pointer to [**[]Message**](Message.md) |  | [optional] 
**ResponseWait** | Pointer to [**ResponseWait**](ResponseWait.md) |  | [optional] 
**FinishWait** | Pointer to **string** | Duration string, eg. &#x60;1s&#x60;, &#x60;1m15s&#x60; | [optional] 
**StartCondition** | Pointer to [**Condition**](Condition.md) |  | [optional] 
**Jumps** | Pointer to [**[]Jump**](Jump.md) |  | [optional] 

## Methods

### NewBlock

`func NewBlock(id string, type_ BlockType, name string, enabled bool, ) *Block`

NewBlock instantiates a new Block object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBlockWithDefaults

`func NewBlockWithDefaults() *Block`

NewBlockWithDefaults instantiates a new Block object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Block) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Block) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Block) SetId(v string)`

SetId sets Id field to given value.


### GetType

`func (o *Block) GetType() BlockType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Block) GetTypeOk() (*BlockType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Block) SetType(v BlockType)`

SetType sets Type field to given value.


### GetName

`func (o *Block) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Block) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Block) SetName(v string)`

SetName sets Name field to given value.


### GetEnabled

`func (o *Block) GetEnabled() bool`

GetEnabled returns the Enabled field if non-nil, zero value otherwise.

### GetEnabledOk

`func (o *Block) GetEnabledOk() (*bool, bool)`

GetEnabledOk returns a tuple with the Enabled field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEnabled

`func (o *Block) SetEnabled(v bool)`

SetEnabled sets Enabled field to given value.


### GetMessages

`func (o *Block) GetMessages() []Message`

GetMessages returns the Messages field if non-nil, zero value otherwise.

### GetMessagesOk

`func (o *Block) GetMessagesOk() (*[]Message, bool)`

GetMessagesOk returns a tuple with the Messages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessages

`func (o *Block) SetMessages(v []Message)`

SetMessages sets Messages field to given value.

### HasMessages

`func (o *Block) HasMessages() bool`

HasMessages returns a boolean if a field has been set.

### GetResponseWait

`func (o *Block) GetResponseWait() ResponseWait`

GetResponseWait returns the ResponseWait field if non-nil, zero value otherwise.

### GetResponseWaitOk

`func (o *Block) GetResponseWaitOk() (*ResponseWait, bool)`

GetResponseWaitOk returns a tuple with the ResponseWait field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResponseWait

`func (o *Block) SetResponseWait(v ResponseWait)`

SetResponseWait sets ResponseWait field to given value.

### HasResponseWait

`func (o *Block) HasResponseWait() bool`

HasResponseWait returns a boolean if a field has been set.

### GetFinishWait

`func (o *Block) GetFinishWait() string`

GetFinishWait returns the FinishWait field if non-nil, zero value otherwise.

### GetFinishWaitOk

`func (o *Block) GetFinishWaitOk() (*string, bool)`

GetFinishWaitOk returns a tuple with the FinishWait field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFinishWait

`func (o *Block) SetFinishWait(v string)`

SetFinishWait sets FinishWait field to given value.

### HasFinishWait

`func (o *Block) HasFinishWait() bool`

HasFinishWait returns a boolean if a field has been set.

### GetStartCondition

`func (o *Block) GetStartCondition() Condition`

GetStartCondition returns the StartCondition field if non-nil, zero value otherwise.

### GetStartConditionOk

`func (o *Block) GetStartConditionOk() (*Condition, bool)`

GetStartConditionOk returns a tuple with the StartCondition field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartCondition

`func (o *Block) SetStartCondition(v Condition)`

SetStartCondition sets StartCondition field to given value.

### HasStartCondition

`func (o *Block) HasStartCondition() bool`

HasStartCondition returns a boolean if a field has been set.

### GetJumps

`func (o *Block) GetJumps() []Jump`

GetJumps returns the Jumps field if non-nil, zero value otherwise.

### GetJumpsOk

`func (o *Block) GetJumpsOk() (*[]Jump, bool)`

GetJumpsOk returns a tuple with the Jumps field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetJumps

`func (o *Block) SetJumps(v []Jump)`

SetJumps sets Jumps field to given value.

### HasJumps

`func (o *Block) HasJumps() bool`

HasJumps returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


