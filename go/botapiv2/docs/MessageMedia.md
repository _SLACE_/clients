# MessageMedia

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to [**MediaType**](MediaType.md) |  | [optional] 
**AssetId** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageMedia

`func NewMessageMedia() *MessageMedia`

NewMessageMedia instantiates a new MessageMedia object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageMediaWithDefaults

`func NewMessageMediaWithDefaults() *MessageMedia`

NewMessageMediaWithDefaults instantiates a new MessageMedia object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *MessageMedia) GetType() MediaType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MessageMedia) GetTypeOk() (*MediaType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MessageMedia) SetType(v MediaType)`

SetType sets Type field to given value.

### HasType

`func (o *MessageMedia) HasType() bool`

HasType returns a boolean if a field has been set.

### GetAssetId

`func (o *MessageMedia) GetAssetId() string`

GetAssetId returns the AssetId field if non-nil, zero value otherwise.

### GetAssetIdOk

`func (o *MessageMedia) GetAssetIdOk() (*string, bool)`

GetAssetIdOk returns a tuple with the AssetId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssetId

`func (o *MessageMedia) SetAssetId(v string)`

SetAssetId sets AssetId field to given value.

### HasAssetId

`func (o *MessageMedia) HasAssetId() bool`

HasAssetId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


