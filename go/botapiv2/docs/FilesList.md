# FilesList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]File**](File.md) |  | 
**Total** | **int32** |  | 

## Methods

### NewFilesList

`func NewFilesList(results []File, total int32, ) *FilesList`

NewFilesList instantiates a new FilesList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFilesListWithDefaults

`func NewFilesListWithDefaults() *FilesList`

NewFilesListWithDefaults instantiates a new FilesList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *FilesList) GetResults() []File`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *FilesList) GetResultsOk() (*[]File, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *FilesList) SetResults(v []File)`

SetResults sets Results field to given value.


### GetTotal

`func (o *FilesList) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *FilesList) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *FilesList) SetTotal(v int32)`

SetTotal sets Total field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


