# AssetType

## Enum


* `AssetTypeText` (value: `"text"`)

* `AssetTypeImage` (value: `"image"`)

* `AssetTypeVideo` (value: `"video"`)

* `AssetTypeAudio` (value: `"audio"`)

* `AssetTypeFile` (value: `"file"`)

* `AssetTypeTemplate` (value: `"template"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


