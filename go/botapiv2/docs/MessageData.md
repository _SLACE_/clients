# MessageData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HeaderTextId** | Pointer to **string** | Id of text asset | [optional] 
**TextId** | Pointer to **string** | Id of text asset | [optional] 
**FooterTextId** | Pointer to **string** | Id of text asset | [optional] 
**Template** | Pointer to **string** | Id of template asset | [optional] 
**Media** | Pointer to [**MessageMedia**](MessageMedia.md) |  | [optional] 
**Buttons** | Pointer to [**[]MessageButton**](MessageButton.md) |  | [optional] 
**List** | Pointer to [**MessageList**](MessageList.md) |  | [optional] 
**ListName** | Pointer to **string** | required for dynamic list - must match the key in script start list data | [optional] 
**RequestLocation** | Pointer to [**MessageRequestLocation**](MessageRequestLocation.md) |  | [optional] 
**Cards** | Pointer to [**[]MessageData**](MessageData.md) |  | [optional] 

## Methods

### NewMessageData

`func NewMessageData() *MessageData`

NewMessageData instantiates a new MessageData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageDataWithDefaults

`func NewMessageDataWithDefaults() *MessageData`

NewMessageDataWithDefaults instantiates a new MessageData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetHeaderTextId

`func (o *MessageData) GetHeaderTextId() string`

GetHeaderTextId returns the HeaderTextId field if non-nil, zero value otherwise.

### GetHeaderTextIdOk

`func (o *MessageData) GetHeaderTextIdOk() (*string, bool)`

GetHeaderTextIdOk returns a tuple with the HeaderTextId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeaderTextId

`func (o *MessageData) SetHeaderTextId(v string)`

SetHeaderTextId sets HeaderTextId field to given value.

### HasHeaderTextId

`func (o *MessageData) HasHeaderTextId() bool`

HasHeaderTextId returns a boolean if a field has been set.

### GetTextId

`func (o *MessageData) GetTextId() string`

GetTextId returns the TextId field if non-nil, zero value otherwise.

### GetTextIdOk

`func (o *MessageData) GetTextIdOk() (*string, bool)`

GetTextIdOk returns a tuple with the TextId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTextId

`func (o *MessageData) SetTextId(v string)`

SetTextId sets TextId field to given value.

### HasTextId

`func (o *MessageData) HasTextId() bool`

HasTextId returns a boolean if a field has been set.

### GetFooterTextId

`func (o *MessageData) GetFooterTextId() string`

GetFooterTextId returns the FooterTextId field if non-nil, zero value otherwise.

### GetFooterTextIdOk

`func (o *MessageData) GetFooterTextIdOk() (*string, bool)`

GetFooterTextIdOk returns a tuple with the FooterTextId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFooterTextId

`func (o *MessageData) SetFooterTextId(v string)`

SetFooterTextId sets FooterTextId field to given value.

### HasFooterTextId

`func (o *MessageData) HasFooterTextId() bool`

HasFooterTextId returns a boolean if a field has been set.

### GetTemplate

`func (o *MessageData) GetTemplate() string`

GetTemplate returns the Template field if non-nil, zero value otherwise.

### GetTemplateOk

`func (o *MessageData) GetTemplateOk() (*string, bool)`

GetTemplateOk returns a tuple with the Template field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplate

`func (o *MessageData) SetTemplate(v string)`

SetTemplate sets Template field to given value.

### HasTemplate

`func (o *MessageData) HasTemplate() bool`

HasTemplate returns a boolean if a field has been set.

### GetMedia

`func (o *MessageData) GetMedia() MessageMedia`

GetMedia returns the Media field if non-nil, zero value otherwise.

### GetMediaOk

`func (o *MessageData) GetMediaOk() (*MessageMedia, bool)`

GetMediaOk returns a tuple with the Media field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMedia

`func (o *MessageData) SetMedia(v MessageMedia)`

SetMedia sets Media field to given value.

### HasMedia

`func (o *MessageData) HasMedia() bool`

HasMedia returns a boolean if a field has been set.

### GetButtons

`func (o *MessageData) GetButtons() []MessageButton`

GetButtons returns the Buttons field if non-nil, zero value otherwise.

### GetButtonsOk

`func (o *MessageData) GetButtonsOk() (*[]MessageButton, bool)`

GetButtonsOk returns a tuple with the Buttons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtons

`func (o *MessageData) SetButtons(v []MessageButton)`

SetButtons sets Buttons field to given value.

### HasButtons

`func (o *MessageData) HasButtons() bool`

HasButtons returns a boolean if a field has been set.

### GetList

`func (o *MessageData) GetList() MessageList`

GetList returns the List field if non-nil, zero value otherwise.

### GetListOk

`func (o *MessageData) GetListOk() (*MessageList, bool)`

GetListOk returns a tuple with the List field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetList

`func (o *MessageData) SetList(v MessageList)`

SetList sets List field to given value.

### HasList

`func (o *MessageData) HasList() bool`

HasList returns a boolean if a field has been set.

### GetListName

`func (o *MessageData) GetListName() string`

GetListName returns the ListName field if non-nil, zero value otherwise.

### GetListNameOk

`func (o *MessageData) GetListNameOk() (*string, bool)`

GetListNameOk returns a tuple with the ListName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetListName

`func (o *MessageData) SetListName(v string)`

SetListName sets ListName field to given value.

### HasListName

`func (o *MessageData) HasListName() bool`

HasListName returns a boolean if a field has been set.

### GetRequestLocation

`func (o *MessageData) GetRequestLocation() MessageRequestLocation`

GetRequestLocation returns the RequestLocation field if non-nil, zero value otherwise.

### GetRequestLocationOk

`func (o *MessageData) GetRequestLocationOk() (*MessageRequestLocation, bool)`

GetRequestLocationOk returns a tuple with the RequestLocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRequestLocation

`func (o *MessageData) SetRequestLocation(v MessageRequestLocation)`

SetRequestLocation sets RequestLocation field to given value.

### HasRequestLocation

`func (o *MessageData) HasRequestLocation() bool`

HasRequestLocation returns a boolean if a field has been set.

### GetCards

`func (o *MessageData) GetCards() []MessageData`

GetCards returns the Cards field if non-nil, zero value otherwise.

### GetCardsOk

`func (o *MessageData) GetCardsOk() (*[]MessageData, bool)`

GetCardsOk returns a tuple with the Cards field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCards

`func (o *MessageData) SetCards(v []MessageData)`

SetCards sets Cards field to given value.

### HasCards

`func (o *MessageData) HasCards() bool`

HasCards returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


