# JumpTarget

## Enum


* `JumpTargetExit` (value: `"exit"`)

* `JumpTargetNext` (value: `"next"`)

* `JumpTargetBlock` (value: `"block"`)

* `JumpTargetNowhere` (value: `"no"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


