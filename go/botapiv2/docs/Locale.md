# Locale

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Locale** | **string** |  | 
**Default** | Pointer to **bool** |  | [optional] 
**Primary** | Pointer to **bool** |  | [optional] 
**SkipAutoTranslation** | Pointer to **bool** |  | [optional] 

## Methods

### NewLocale

`func NewLocale(locale string, ) *Locale`

NewLocale instantiates a new Locale object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLocaleWithDefaults

`func NewLocaleWithDefaults() *Locale`

NewLocaleWithDefaults instantiates a new Locale object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLocale

`func (o *Locale) GetLocale() string`

GetLocale returns the Locale field if non-nil, zero value otherwise.

### GetLocaleOk

`func (o *Locale) GetLocaleOk() (*string, bool)`

GetLocaleOk returns a tuple with the Locale field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocale

`func (o *Locale) SetLocale(v string)`

SetLocale sets Locale field to given value.


### GetDefault

`func (o *Locale) GetDefault() bool`

GetDefault returns the Default field if non-nil, zero value otherwise.

### GetDefaultOk

`func (o *Locale) GetDefaultOk() (*bool, bool)`

GetDefaultOk returns a tuple with the Default field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefault

`func (o *Locale) SetDefault(v bool)`

SetDefault sets Default field to given value.

### HasDefault

`func (o *Locale) HasDefault() bool`

HasDefault returns a boolean if a field has been set.

### GetPrimary

`func (o *Locale) GetPrimary() bool`

GetPrimary returns the Primary field if non-nil, zero value otherwise.

### GetPrimaryOk

`func (o *Locale) GetPrimaryOk() (*bool, bool)`

GetPrimaryOk returns a tuple with the Primary field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrimary

`func (o *Locale) SetPrimary(v bool)`

SetPrimary sets Primary field to given value.

### HasPrimary

`func (o *Locale) HasPrimary() bool`

HasPrimary returns a boolean if a field has been set.

### GetSkipAutoTranslation

`func (o *Locale) GetSkipAutoTranslation() bool`

GetSkipAutoTranslation returns the SkipAutoTranslation field if non-nil, zero value otherwise.

### GetSkipAutoTranslationOk

`func (o *Locale) GetSkipAutoTranslationOk() (*bool, bool)`

GetSkipAutoTranslationOk returns a tuple with the SkipAutoTranslation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSkipAutoTranslation

`func (o *Locale) SetSkipAutoTranslation(v bool)`

SetSkipAutoTranslation sets SkipAutoTranslation field to given value.

### HasSkipAutoTranslation

`func (o *Locale) HasSkipAutoTranslation() bool`

HasSkipAutoTranslation returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


