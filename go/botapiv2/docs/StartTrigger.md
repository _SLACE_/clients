# StartTrigger

## Enum


* `TRIGGER_ONLY` (value: `"trigger_only"`)

* `OPT_OUT` (value: `"opt_out"`)

* `MESSAGE` (value: `"message"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


