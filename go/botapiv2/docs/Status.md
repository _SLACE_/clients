# Status

## Enum


* `StatusDraft` (value: `"draft"`)

* `StatusReady` (value: `"ready"`)

* `StatusLive` (value: `"live"`)

* `StatusStopped` (value: `"stopped"`)

* `StatusArchived` (value: `"archived"`)

* `StatusProcessing` (value: `"processing"`)

* `StatusReadyForTemplates` (value: `"issues"`)

* `READY_FOR_TEMPLATES` (value: `"ready_for_templates"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


