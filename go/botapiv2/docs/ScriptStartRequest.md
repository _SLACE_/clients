# ScriptStartRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConversationId** | **string** |  | 
**TransactionId** | Pointer to **string** |  | [optional] 
**InteractionId** | Pointer to **string** |  | [optional] 
**Language** | Pointer to **string** |  | [optional] 
**StaticData** | Pointer to **map[string]interface{}** |  | [optional] 
**DynamicData** | Pointer to **map[string]interface{}** |  | [optional] 
**ListData** | Pointer to **map[string]interface{}** | map with string as keys and list of ListItem as values | [optional] 

## Methods

### NewScriptStartRequest

`func NewScriptStartRequest(conversationId string, ) *ScriptStartRequest`

NewScriptStartRequest instantiates a new ScriptStartRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScriptStartRequestWithDefaults

`func NewScriptStartRequestWithDefaults() *ScriptStartRequest`

NewScriptStartRequestWithDefaults instantiates a new ScriptStartRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetConversationId

`func (o *ScriptStartRequest) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *ScriptStartRequest) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *ScriptStartRequest) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetTransactionId

`func (o *ScriptStartRequest) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *ScriptStartRequest) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *ScriptStartRequest) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.

### HasTransactionId

`func (o *ScriptStartRequest) HasTransactionId() bool`

HasTransactionId returns a boolean if a field has been set.

### GetInteractionId

`func (o *ScriptStartRequest) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *ScriptStartRequest) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *ScriptStartRequest) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *ScriptStartRequest) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetLanguage

`func (o *ScriptStartRequest) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *ScriptStartRequest) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *ScriptStartRequest) SetLanguage(v string)`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *ScriptStartRequest) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.

### GetStaticData

`func (o *ScriptStartRequest) GetStaticData() map[string]interface{}`

GetStaticData returns the StaticData field if non-nil, zero value otherwise.

### GetStaticDataOk

`func (o *ScriptStartRequest) GetStaticDataOk() (*map[string]interface{}, bool)`

GetStaticDataOk returns a tuple with the StaticData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStaticData

`func (o *ScriptStartRequest) SetStaticData(v map[string]interface{})`

SetStaticData sets StaticData field to given value.

### HasStaticData

`func (o *ScriptStartRequest) HasStaticData() bool`

HasStaticData returns a boolean if a field has been set.

### GetDynamicData

`func (o *ScriptStartRequest) GetDynamicData() map[string]interface{}`

GetDynamicData returns the DynamicData field if non-nil, zero value otherwise.

### GetDynamicDataOk

`func (o *ScriptStartRequest) GetDynamicDataOk() (*map[string]interface{}, bool)`

GetDynamicDataOk returns a tuple with the DynamicData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDynamicData

`func (o *ScriptStartRequest) SetDynamicData(v map[string]interface{})`

SetDynamicData sets DynamicData field to given value.

### HasDynamicData

`func (o *ScriptStartRequest) HasDynamicData() bool`

HasDynamicData returns a boolean if a field has been set.

### GetListData

`func (o *ScriptStartRequest) GetListData() map[string]interface{}`

GetListData returns the ListData field if non-nil, zero value otherwise.

### GetListDataOk

`func (o *ScriptStartRequest) GetListDataOk() (*map[string]interface{}, bool)`

GetListDataOk returns a tuple with the ListData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetListData

`func (o *ScriptStartRequest) SetListData(v map[string]interface{})`

SetListData sets ListData field to given value.

### HasListData

`func (o *ScriptStartRequest) HasListData() bool`

HasListData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


