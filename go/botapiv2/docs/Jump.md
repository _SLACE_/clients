# Jump

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Trigger** | [**JumpTrigger**](JumpTrigger.md) |  | 
**Target** | [**JumpTarget**](JumpTarget.md) |  | 
**BlockId** | Pointer to **string** | Required if target is &#x60;block&#x60; | [optional] 
**ButtonId** | Pointer to **string** | Required if trigger is &#x60;button&#x60; | [optional] 
**Condition** | Pointer to [**Condition**](Condition.md) |  | [optional] 
**Integrations** | Pointer to [**Integrations**](Integrations.md) |  | [optional] 
**Writers** | Pointer to [**[]Writer**](Writer.md) |  | [optional] 

## Methods

### NewJump

`func NewJump(trigger JumpTrigger, target JumpTarget, ) *Jump`

NewJump instantiates a new Jump object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewJumpWithDefaults

`func NewJumpWithDefaults() *Jump`

NewJumpWithDefaults instantiates a new Jump object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTrigger

`func (o *Jump) GetTrigger() JumpTrigger`

GetTrigger returns the Trigger field if non-nil, zero value otherwise.

### GetTriggerOk

`func (o *Jump) GetTriggerOk() (*JumpTrigger, bool)`

GetTriggerOk returns a tuple with the Trigger field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTrigger

`func (o *Jump) SetTrigger(v JumpTrigger)`

SetTrigger sets Trigger field to given value.


### GetTarget

`func (o *Jump) GetTarget() JumpTarget`

GetTarget returns the Target field if non-nil, zero value otherwise.

### GetTargetOk

`func (o *Jump) GetTargetOk() (*JumpTarget, bool)`

GetTargetOk returns a tuple with the Target field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTarget

`func (o *Jump) SetTarget(v JumpTarget)`

SetTarget sets Target field to given value.


### GetBlockId

`func (o *Jump) GetBlockId() string`

GetBlockId returns the BlockId field if non-nil, zero value otherwise.

### GetBlockIdOk

`func (o *Jump) GetBlockIdOk() (*string, bool)`

GetBlockIdOk returns a tuple with the BlockId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockId

`func (o *Jump) SetBlockId(v string)`

SetBlockId sets BlockId field to given value.

### HasBlockId

`func (o *Jump) HasBlockId() bool`

HasBlockId returns a boolean if a field has been set.

### GetButtonId

`func (o *Jump) GetButtonId() string`

GetButtonId returns the ButtonId field if non-nil, zero value otherwise.

### GetButtonIdOk

`func (o *Jump) GetButtonIdOk() (*string, bool)`

GetButtonIdOk returns a tuple with the ButtonId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtonId

`func (o *Jump) SetButtonId(v string)`

SetButtonId sets ButtonId field to given value.

### HasButtonId

`func (o *Jump) HasButtonId() bool`

HasButtonId returns a boolean if a field has been set.

### GetCondition

`func (o *Jump) GetCondition() Condition`

GetCondition returns the Condition field if non-nil, zero value otherwise.

### GetConditionOk

`func (o *Jump) GetConditionOk() (*Condition, bool)`

GetConditionOk returns a tuple with the Condition field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCondition

`func (o *Jump) SetCondition(v Condition)`

SetCondition sets Condition field to given value.

### HasCondition

`func (o *Jump) HasCondition() bool`

HasCondition returns a boolean if a field has been set.

### GetIntegrations

`func (o *Jump) GetIntegrations() Integrations`

GetIntegrations returns the Integrations field if non-nil, zero value otherwise.

### GetIntegrationsOk

`func (o *Jump) GetIntegrationsOk() (*Integrations, bool)`

GetIntegrationsOk returns a tuple with the Integrations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIntegrations

`func (o *Jump) SetIntegrations(v Integrations)`

SetIntegrations sets Integrations field to given value.

### HasIntegrations

`func (o *Jump) HasIntegrations() bool`

HasIntegrations returns a boolean if a field has been set.

### GetWriters

`func (o *Jump) GetWriters() []Writer`

GetWriters returns the Writers field if non-nil, zero value otherwise.

### GetWritersOk

`func (o *Jump) GetWritersOk() (*[]Writer, bool)`

GetWritersOk returns a tuple with the Writers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWriters

`func (o *Jump) SetWriters(v []Writer)`

SetWriters sets Writers field to given value.

### HasWriters

`func (o *Jump) HasWriters() bool`

HasWriters returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


