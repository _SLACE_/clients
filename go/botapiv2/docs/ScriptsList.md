# ScriptsList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | Pointer to [**[]Script**](Script.md) |  | [optional] 
**Total** | Pointer to **int64** |  | [optional] 

## Methods

### NewScriptsList

`func NewScriptsList() *ScriptsList`

NewScriptsList instantiates a new ScriptsList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScriptsListWithDefaults

`func NewScriptsListWithDefaults() *ScriptsList`

NewScriptsListWithDefaults instantiates a new ScriptsList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *ScriptsList) GetResults() []Script`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *ScriptsList) GetResultsOk() (*[]Script, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *ScriptsList) SetResults(v []Script)`

SetResults sets Results field to given value.

### HasResults

`func (o *ScriptsList) HasResults() bool`

HasResults returns a boolean if a field has been set.

### GetTotal

`func (o *ScriptsList) GetTotal() int64`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *ScriptsList) GetTotalOk() (*int64, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *ScriptsList) SetTotal(v int64)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *ScriptsList) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


