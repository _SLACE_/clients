# MessageButtonUrlOptions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AutoShortLink** | Pointer to **bool** |  | [optional] 
**ExpiredUrl** | Pointer to **string** |  | [optional] 
**Ttl** | Pointer to **int32** |  | [optional] 
**WithTxId** | Pointer to **bool** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageButtonUrlOptions

`func NewMessageButtonUrlOptions() *MessageButtonUrlOptions`

NewMessageButtonUrlOptions instantiates a new MessageButtonUrlOptions object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageButtonUrlOptionsWithDefaults

`func NewMessageButtonUrlOptionsWithDefaults() *MessageButtonUrlOptions`

NewMessageButtonUrlOptionsWithDefaults instantiates a new MessageButtonUrlOptions object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAutoShortLink

`func (o *MessageButtonUrlOptions) GetAutoShortLink() bool`

GetAutoShortLink returns the AutoShortLink field if non-nil, zero value otherwise.

### GetAutoShortLinkOk

`func (o *MessageButtonUrlOptions) GetAutoShortLinkOk() (*bool, bool)`

GetAutoShortLinkOk returns a tuple with the AutoShortLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAutoShortLink

`func (o *MessageButtonUrlOptions) SetAutoShortLink(v bool)`

SetAutoShortLink sets AutoShortLink field to given value.

### HasAutoShortLink

`func (o *MessageButtonUrlOptions) HasAutoShortLink() bool`

HasAutoShortLink returns a boolean if a field has been set.

### GetExpiredUrl

`func (o *MessageButtonUrlOptions) GetExpiredUrl() string`

GetExpiredUrl returns the ExpiredUrl field if non-nil, zero value otherwise.

### GetExpiredUrlOk

`func (o *MessageButtonUrlOptions) GetExpiredUrlOk() (*string, bool)`

GetExpiredUrlOk returns a tuple with the ExpiredUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiredUrl

`func (o *MessageButtonUrlOptions) SetExpiredUrl(v string)`

SetExpiredUrl sets ExpiredUrl field to given value.

### HasExpiredUrl

`func (o *MessageButtonUrlOptions) HasExpiredUrl() bool`

HasExpiredUrl returns a boolean if a field has been set.

### GetTtl

`func (o *MessageButtonUrlOptions) GetTtl() int32`

GetTtl returns the Ttl field if non-nil, zero value otherwise.

### GetTtlOk

`func (o *MessageButtonUrlOptions) GetTtlOk() (*int32, bool)`

GetTtlOk returns a tuple with the Ttl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTtl

`func (o *MessageButtonUrlOptions) SetTtl(v int32)`

SetTtl sets Ttl field to given value.

### HasTtl

`func (o *MessageButtonUrlOptions) HasTtl() bool`

HasTtl returns a boolean if a field has been set.

### GetWithTxId

`func (o *MessageButtonUrlOptions) GetWithTxId() bool`

GetWithTxId returns the WithTxId field if non-nil, zero value otherwise.

### GetWithTxIdOk

`func (o *MessageButtonUrlOptions) GetWithTxIdOk() (*bool, bool)`

GetWithTxIdOk returns a tuple with the WithTxId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWithTxId

`func (o *MessageButtonUrlOptions) SetWithTxId(v bool)`

SetWithTxId sets WithTxId field to given value.

### HasWithTxId

`func (o *MessageButtonUrlOptions) HasWithTxId() bool`

HasWithTxId returns a boolean if a field has been set.

### GetType

`func (o *MessageButtonUrlOptions) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MessageButtonUrlOptions) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MessageButtonUrlOptions) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *MessageButtonUrlOptions) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


