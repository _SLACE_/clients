# DataSourceProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AllowCustom** | **bool** |  | 
**Properties** | [**[]DataSourceProperty**](DataSourceProperty.md) |  | 

## Methods

### NewDataSourceProperties

`func NewDataSourceProperties(allowCustom bool, properties []DataSourceProperty, ) *DataSourceProperties`

NewDataSourceProperties instantiates a new DataSourceProperties object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDataSourcePropertiesWithDefaults

`func NewDataSourcePropertiesWithDefaults() *DataSourceProperties`

NewDataSourcePropertiesWithDefaults instantiates a new DataSourceProperties object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAllowCustom

`func (o *DataSourceProperties) GetAllowCustom() bool`

GetAllowCustom returns the AllowCustom field if non-nil, zero value otherwise.

### GetAllowCustomOk

`func (o *DataSourceProperties) GetAllowCustomOk() (*bool, bool)`

GetAllowCustomOk returns a tuple with the AllowCustom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAllowCustom

`func (o *DataSourceProperties) SetAllowCustom(v bool)`

SetAllowCustom sets AllowCustom field to given value.


### GetProperties

`func (o *DataSourceProperties) GetProperties() []DataSourceProperty`

GetProperties returns the Properties field if non-nil, zero value otherwise.

### GetPropertiesOk

`func (o *DataSourceProperties) GetPropertiesOk() (*[]DataSourceProperty, bool)`

GetPropertiesOk returns a tuple with the Properties field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProperties

`func (o *DataSourceProperties) SetProperties(v []DataSourceProperty)`

SetProperties sets Properties field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


