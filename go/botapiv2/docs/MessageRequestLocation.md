# MessageRequestLocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ButtonLabelId** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageRequestLocation

`func NewMessageRequestLocation() *MessageRequestLocation`

NewMessageRequestLocation instantiates a new MessageRequestLocation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageRequestLocationWithDefaults

`func NewMessageRequestLocationWithDefaults() *MessageRequestLocation`

NewMessageRequestLocationWithDefaults instantiates a new MessageRequestLocation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetButtonLabelId

`func (o *MessageRequestLocation) GetButtonLabelId() string`

GetButtonLabelId returns the ButtonLabelId field if non-nil, zero value otherwise.

### GetButtonLabelIdOk

`func (o *MessageRequestLocation) GetButtonLabelIdOk() (*string, bool)`

GetButtonLabelIdOk returns a tuple with the ButtonLabelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtonLabelId

`func (o *MessageRequestLocation) SetButtonLabelId(v string)`

SetButtonLabelId sets ButtonLabelId field to given value.

### HasButtonLabelId

`func (o *MessageRequestLocation) HasButtonLabelId() bool`

HasButtonLabelId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


