# Integrations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AgentHandoff** | Pointer to **bool** |  | [optional] 
**OptOut** | Pointer to **bool** |  | [optional] 
**MainAction** | Pointer to **bool** |  | [optional] 
**Investment** | Pointer to **bool** |  | [optional] 
**Labels** | Pointer to [**[]Tag**](Tag.md) |  | [optional] 
**Segments** | Pointer to [**[]Tag**](Tag.md) |  | [optional] 

## Methods

### NewIntegrations

`func NewIntegrations() *Integrations`

NewIntegrations instantiates a new Integrations object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIntegrationsWithDefaults

`func NewIntegrationsWithDefaults() *Integrations`

NewIntegrationsWithDefaults instantiates a new Integrations object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAgentHandoff

`func (o *Integrations) GetAgentHandoff() bool`

GetAgentHandoff returns the AgentHandoff field if non-nil, zero value otherwise.

### GetAgentHandoffOk

`func (o *Integrations) GetAgentHandoffOk() (*bool, bool)`

GetAgentHandoffOk returns a tuple with the AgentHandoff field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAgentHandoff

`func (o *Integrations) SetAgentHandoff(v bool)`

SetAgentHandoff sets AgentHandoff field to given value.

### HasAgentHandoff

`func (o *Integrations) HasAgentHandoff() bool`

HasAgentHandoff returns a boolean if a field has been set.

### GetOptOut

`func (o *Integrations) GetOptOut() bool`

GetOptOut returns the OptOut field if non-nil, zero value otherwise.

### GetOptOutOk

`func (o *Integrations) GetOptOutOk() (*bool, bool)`

GetOptOutOk returns a tuple with the OptOut field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOptOut

`func (o *Integrations) SetOptOut(v bool)`

SetOptOut sets OptOut field to given value.

### HasOptOut

`func (o *Integrations) HasOptOut() bool`

HasOptOut returns a boolean if a field has been set.

### GetMainAction

`func (o *Integrations) GetMainAction() bool`

GetMainAction returns the MainAction field if non-nil, zero value otherwise.

### GetMainActionOk

`func (o *Integrations) GetMainActionOk() (*bool, bool)`

GetMainActionOk returns a tuple with the MainAction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMainAction

`func (o *Integrations) SetMainAction(v bool)`

SetMainAction sets MainAction field to given value.

### HasMainAction

`func (o *Integrations) HasMainAction() bool`

HasMainAction returns a boolean if a field has been set.

### GetInvestment

`func (o *Integrations) GetInvestment() bool`

GetInvestment returns the Investment field if non-nil, zero value otherwise.

### GetInvestmentOk

`func (o *Integrations) GetInvestmentOk() (*bool, bool)`

GetInvestmentOk returns a tuple with the Investment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInvestment

`func (o *Integrations) SetInvestment(v bool)`

SetInvestment sets Investment field to given value.

### HasInvestment

`func (o *Integrations) HasInvestment() bool`

HasInvestment returns a boolean if a field has been set.

### GetLabels

`func (o *Integrations) GetLabels() []Tag`

GetLabels returns the Labels field if non-nil, zero value otherwise.

### GetLabelsOk

`func (o *Integrations) GetLabelsOk() (*[]Tag, bool)`

GetLabelsOk returns a tuple with the Labels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabels

`func (o *Integrations) SetLabels(v []Tag)`

SetLabels sets Labels field to given value.

### HasLabels

`func (o *Integrations) HasLabels() bool`

HasLabels returns a boolean if a field has been set.

### GetSegments

`func (o *Integrations) GetSegments() []Tag`

GetSegments returns the Segments field if non-nil, zero value otherwise.

### GetSegmentsOk

`func (o *Integrations) GetSegmentsOk() (*[]Tag, bool)`

GetSegmentsOk returns a tuple with the Segments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSegments

`func (o *Integrations) SetSegments(v []Tag)`

SetSegments sets Segments field to given value.

### HasSegments

`func (o *Integrations) HasSegments() bool`

HasSegments returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


