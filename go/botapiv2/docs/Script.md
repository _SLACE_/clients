# Script

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**ChannelId** | **string** |  | 
**Name** | **string** |  | 
**MaxQuestions** | **int32** |  | 
**Status** | [**Status**](Status.md) |  | 
**RestartAllowed** | **bool** |  | 
**CannedResponse** | **bool** |  | 
**Locales** | [**[]Locale**](Locale.md) |  | 
**StartTrigger** | Pointer to [**StartTrigger**](StartTrigger.md) |  | [optional] 
**Formality** | Pointer to [**Formality**](Formality.md) |  | [optional] 
**Blocks** | [**[]Block**](Block.md) |  | 
**Assets** | [**[]Asset**](Asset.md) |  | 
**CreatedAt** | **time.Time** |  | 
**UpdatedAt** | **time.Time** |  | 

## Methods

### NewScript

`func NewScript(id string, channelId string, name string, maxQuestions int32, status Status, restartAllowed bool, cannedResponse bool, locales []Locale, blocks []Block, assets []Asset, createdAt time.Time, updatedAt time.Time, ) *Script`

NewScript instantiates a new Script object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScriptWithDefaults

`func NewScriptWithDefaults() *Script`

NewScriptWithDefaults instantiates a new Script object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Script) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Script) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Script) SetId(v string)`

SetId sets Id field to given value.


### GetChannelId

`func (o *Script) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *Script) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *Script) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetName

`func (o *Script) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Script) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Script) SetName(v string)`

SetName sets Name field to given value.


### GetMaxQuestions

`func (o *Script) GetMaxQuestions() int32`

GetMaxQuestions returns the MaxQuestions field if non-nil, zero value otherwise.

### GetMaxQuestionsOk

`func (o *Script) GetMaxQuestionsOk() (*int32, bool)`

GetMaxQuestionsOk returns a tuple with the MaxQuestions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxQuestions

`func (o *Script) SetMaxQuestions(v int32)`

SetMaxQuestions sets MaxQuestions field to given value.


### GetStatus

`func (o *Script) GetStatus() Status`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *Script) GetStatusOk() (*Status, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *Script) SetStatus(v Status)`

SetStatus sets Status field to given value.


### GetRestartAllowed

`func (o *Script) GetRestartAllowed() bool`

GetRestartAllowed returns the RestartAllowed field if non-nil, zero value otherwise.

### GetRestartAllowedOk

`func (o *Script) GetRestartAllowedOk() (*bool, bool)`

GetRestartAllowedOk returns a tuple with the RestartAllowed field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRestartAllowed

`func (o *Script) SetRestartAllowed(v bool)`

SetRestartAllowed sets RestartAllowed field to given value.


### GetCannedResponse

`func (o *Script) GetCannedResponse() bool`

GetCannedResponse returns the CannedResponse field if non-nil, zero value otherwise.

### GetCannedResponseOk

`func (o *Script) GetCannedResponseOk() (*bool, bool)`

GetCannedResponseOk returns a tuple with the CannedResponse field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCannedResponse

`func (o *Script) SetCannedResponse(v bool)`

SetCannedResponse sets CannedResponse field to given value.


### GetLocales

`func (o *Script) GetLocales() []Locale`

GetLocales returns the Locales field if non-nil, zero value otherwise.

### GetLocalesOk

`func (o *Script) GetLocalesOk() (*[]Locale, bool)`

GetLocalesOk returns a tuple with the Locales field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocales

`func (o *Script) SetLocales(v []Locale)`

SetLocales sets Locales field to given value.


### GetStartTrigger

`func (o *Script) GetStartTrigger() StartTrigger`

GetStartTrigger returns the StartTrigger field if non-nil, zero value otherwise.

### GetStartTriggerOk

`func (o *Script) GetStartTriggerOk() (*StartTrigger, bool)`

GetStartTriggerOk returns a tuple with the StartTrigger field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartTrigger

`func (o *Script) SetStartTrigger(v StartTrigger)`

SetStartTrigger sets StartTrigger field to given value.

### HasStartTrigger

`func (o *Script) HasStartTrigger() bool`

HasStartTrigger returns a boolean if a field has been set.

### GetFormality

`func (o *Script) GetFormality() Formality`

GetFormality returns the Formality field if non-nil, zero value otherwise.

### GetFormalityOk

`func (o *Script) GetFormalityOk() (*Formality, bool)`

GetFormalityOk returns a tuple with the Formality field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormality

`func (o *Script) SetFormality(v Formality)`

SetFormality sets Formality field to given value.

### HasFormality

`func (o *Script) HasFormality() bool`

HasFormality returns a boolean if a field has been set.

### GetBlocks

`func (o *Script) GetBlocks() []Block`

GetBlocks returns the Blocks field if non-nil, zero value otherwise.

### GetBlocksOk

`func (o *Script) GetBlocksOk() (*[]Block, bool)`

GetBlocksOk returns a tuple with the Blocks field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlocks

`func (o *Script) SetBlocks(v []Block)`

SetBlocks sets Blocks field to given value.


### GetAssets

`func (o *Script) GetAssets() []Asset`

GetAssets returns the Assets field if non-nil, zero value otherwise.

### GetAssetsOk

`func (o *Script) GetAssetsOk() (*[]Asset, bool)`

GetAssetsOk returns a tuple with the Assets field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssets

`func (o *Script) SetAssets(v []Asset)`

SetAssets sets Assets field to given value.


### GetCreatedAt

`func (o *Script) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Script) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Script) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *Script) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Script) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Script) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


