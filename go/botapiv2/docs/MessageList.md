# MessageList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ButtonNameId** | **string** | Id of text asset | 

## Methods

### NewMessageList

`func NewMessageList(buttonNameId string, ) *MessageList`

NewMessageList instantiates a new MessageList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageListWithDefaults

`func NewMessageListWithDefaults() *MessageList`

NewMessageListWithDefaults instantiates a new MessageList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetButtonNameId

`func (o *MessageList) GetButtonNameId() string`

GetButtonNameId returns the ButtonNameId field if non-nil, zero value otherwise.

### GetButtonNameIdOk

`func (o *MessageList) GetButtonNameIdOk() (*string, bool)`

GetButtonNameIdOk returns a tuple with the ButtonNameId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtonNameId

`func (o *MessageList) SetButtonNameId(v string)`

SetButtonNameId sets ButtonNameId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


