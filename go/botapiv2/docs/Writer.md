# Writer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Target** | Pointer to [**WriterTarget**](WriterTarget.md) |  | [optional] 
**Source** | Pointer to [**WriterSource**](WriterSource.md) |  | [optional] 
**Property** | Pointer to **string** |  | [optional] 
**StaticValue** | Pointer to **string** |  | [optional] 

## Methods

### NewWriter

`func NewWriter() *Writer`

NewWriter instantiates a new Writer object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWriterWithDefaults

`func NewWriterWithDefaults() *Writer`

NewWriterWithDefaults instantiates a new Writer object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTarget

`func (o *Writer) GetTarget() WriterTarget`

GetTarget returns the Target field if non-nil, zero value otherwise.

### GetTargetOk

`func (o *Writer) GetTargetOk() (*WriterTarget, bool)`

GetTargetOk returns a tuple with the Target field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTarget

`func (o *Writer) SetTarget(v WriterTarget)`

SetTarget sets Target field to given value.

### HasTarget

`func (o *Writer) HasTarget() bool`

HasTarget returns a boolean if a field has been set.

### GetSource

`func (o *Writer) GetSource() WriterSource`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *Writer) GetSourceOk() (*WriterSource, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *Writer) SetSource(v WriterSource)`

SetSource sets Source field to given value.

### HasSource

`func (o *Writer) HasSource() bool`

HasSource returns a boolean if a field has been set.

### GetProperty

`func (o *Writer) GetProperty() string`

GetProperty returns the Property field if non-nil, zero value otherwise.

### GetPropertyOk

`func (o *Writer) GetPropertyOk() (*string, bool)`

GetPropertyOk returns a tuple with the Property field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProperty

`func (o *Writer) SetProperty(v string)`

SetProperty sets Property field to given value.

### HasProperty

`func (o *Writer) HasProperty() bool`

HasProperty returns a boolean if a field has been set.

### GetStaticValue

`func (o *Writer) GetStaticValue() string`

GetStaticValue returns the StaticValue field if non-nil, zero value otherwise.

### GetStaticValueOk

`func (o *Writer) GetStaticValueOk() (*string, bool)`

GetStaticValueOk returns a tuple with the StaticValue field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStaticValue

`func (o *Writer) SetStaticValue(v string)`

SetStaticValue sets StaticValue field to given value.

### HasStaticValue

`func (o *Writer) HasStaticValue() bool`

HasStaticValue returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


