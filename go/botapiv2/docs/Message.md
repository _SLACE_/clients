# Message

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Unified** | Pointer to [**MessageData**](MessageData.md) |  | [optional] 
**MessengerCustom** | Pointer to [**map[string]MessageData**](MessageData.md) | Map of custom message configurations per messenger type | [optional] 
**CostOptimized** | Pointer to **bool** |  | [optional] 
**TemplateRequested** | Pointer to **bool** |  | [optional] 

## Methods

### NewMessage

`func NewMessage() *Message`

NewMessage instantiates a new Message object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageWithDefaults

`func NewMessageWithDefaults() *Message`

NewMessageWithDefaults instantiates a new Message object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUnified

`func (o *Message) GetUnified() MessageData`

GetUnified returns the Unified field if non-nil, zero value otherwise.

### GetUnifiedOk

`func (o *Message) GetUnifiedOk() (*MessageData, bool)`

GetUnifiedOk returns a tuple with the Unified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnified

`func (o *Message) SetUnified(v MessageData)`

SetUnified sets Unified field to given value.

### HasUnified

`func (o *Message) HasUnified() bool`

HasUnified returns a boolean if a field has been set.

### GetMessengerCustom

`func (o *Message) GetMessengerCustom() map[string]MessageData`

GetMessengerCustom returns the MessengerCustom field if non-nil, zero value otherwise.

### GetMessengerCustomOk

`func (o *Message) GetMessengerCustomOk() (*map[string]MessageData, bool)`

GetMessengerCustomOk returns a tuple with the MessengerCustom field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerCustom

`func (o *Message) SetMessengerCustom(v map[string]MessageData)`

SetMessengerCustom sets MessengerCustom field to given value.

### HasMessengerCustom

`func (o *Message) HasMessengerCustom() bool`

HasMessengerCustom returns a boolean if a field has been set.

### GetCostOptimized

`func (o *Message) GetCostOptimized() bool`

GetCostOptimized returns the CostOptimized field if non-nil, zero value otherwise.

### GetCostOptimizedOk

`func (o *Message) GetCostOptimizedOk() (*bool, bool)`

GetCostOptimizedOk returns a tuple with the CostOptimized field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCostOptimized

`func (o *Message) SetCostOptimized(v bool)`

SetCostOptimized sets CostOptimized field to given value.

### HasCostOptimized

`func (o *Message) HasCostOptimized() bool`

HasCostOptimized returns a boolean if a field has been set.

### GetTemplateRequested

`func (o *Message) GetTemplateRequested() bool`

GetTemplateRequested returns the TemplateRequested field if non-nil, zero value otherwise.

### GetTemplateRequestedOk

`func (o *Message) GetTemplateRequestedOk() (*bool, bool)`

GetTemplateRequestedOk returns a tuple with the TemplateRequested field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplateRequested

`func (o *Message) SetTemplateRequested(v bool)`

SetTemplateRequested sets TemplateRequested field to given value.

### HasTemplateRequested

`func (o *Message) HasTemplateRequested() bool`

HasTemplateRequested returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


