# Asset

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Type** | [**AssetType**](AssetType.md) |  | 
**Translations** | [**[]AssetTranslation**](AssetTranslation.md) |  | 
**DefaultMedia** | Pointer to [**AssetMedia**](AssetMedia.md) |  | [optional] 
**SkipAutoTranslation** | Pointer to **bool** |  | [optional] 

## Methods

### NewAsset

`func NewAsset(id string, type_ AssetType, translations []AssetTranslation, ) *Asset`

NewAsset instantiates a new Asset object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAssetWithDefaults

`func NewAssetWithDefaults() *Asset`

NewAssetWithDefaults instantiates a new Asset object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Asset) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Asset) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Asset) SetId(v string)`

SetId sets Id field to given value.


### GetType

`func (o *Asset) GetType() AssetType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Asset) GetTypeOk() (*AssetType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Asset) SetType(v AssetType)`

SetType sets Type field to given value.


### GetTranslations

`func (o *Asset) GetTranslations() []AssetTranslation`

GetTranslations returns the Translations field if non-nil, zero value otherwise.

### GetTranslationsOk

`func (o *Asset) GetTranslationsOk() (*[]AssetTranslation, bool)`

GetTranslationsOk returns a tuple with the Translations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTranslations

`func (o *Asset) SetTranslations(v []AssetTranslation)`

SetTranslations sets Translations field to given value.


### GetDefaultMedia

`func (o *Asset) GetDefaultMedia() AssetMedia`

GetDefaultMedia returns the DefaultMedia field if non-nil, zero value otherwise.

### GetDefaultMediaOk

`func (o *Asset) GetDefaultMediaOk() (*AssetMedia, bool)`

GetDefaultMediaOk returns a tuple with the DefaultMedia field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDefaultMedia

`func (o *Asset) SetDefaultMedia(v AssetMedia)`

SetDefaultMedia sets DefaultMedia field to given value.

### HasDefaultMedia

`func (o *Asset) HasDefaultMedia() bool`

HasDefaultMedia returns a boolean if a field has been set.

### GetSkipAutoTranslation

`func (o *Asset) GetSkipAutoTranslation() bool`

GetSkipAutoTranslation returns the SkipAutoTranslation field if non-nil, zero value otherwise.

### GetSkipAutoTranslationOk

`func (o *Asset) GetSkipAutoTranslationOk() (*bool, bool)`

GetSkipAutoTranslationOk returns a tuple with the SkipAutoTranslation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSkipAutoTranslation

`func (o *Asset) SetSkipAutoTranslation(v bool)`

SetSkipAutoTranslation sets SkipAutoTranslation field to given value.

### HasSkipAutoTranslation

`func (o *Asset) HasSkipAutoTranslation() bool`

HasSkipAutoTranslation returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


