# Validation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | Pointer to **bool** |  | [optional] 
**Rules** | Pointer to [**[]ValidationRule**](ValidationRule.md) |  | [optional] 
**ErrorMessage** | Pointer to [**Message**](Message.md) |  | [optional] 

## Methods

### NewValidation

`func NewValidation() *Validation`

NewValidation instantiates a new Validation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewValidationWithDefaults

`func NewValidationWithDefaults() *Validation`

NewValidationWithDefaults instantiates a new Validation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetEnabled

`func (o *Validation) GetEnabled() bool`

GetEnabled returns the Enabled field if non-nil, zero value otherwise.

### GetEnabledOk

`func (o *Validation) GetEnabledOk() (*bool, bool)`

GetEnabledOk returns a tuple with the Enabled field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEnabled

`func (o *Validation) SetEnabled(v bool)`

SetEnabled sets Enabled field to given value.

### HasEnabled

`func (o *Validation) HasEnabled() bool`

HasEnabled returns a boolean if a field has been set.

### GetRules

`func (o *Validation) GetRules() []ValidationRule`

GetRules returns the Rules field if non-nil, zero value otherwise.

### GetRulesOk

`func (o *Validation) GetRulesOk() (*[]ValidationRule, bool)`

GetRulesOk returns a tuple with the Rules field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRules

`func (o *Validation) SetRules(v []ValidationRule)`

SetRules sets Rules field to given value.

### HasRules

`func (o *Validation) HasRules() bool`

HasRules returns a boolean if a field has been set.

### GetErrorMessage

`func (o *Validation) GetErrorMessage() Message`

GetErrorMessage returns the ErrorMessage field if non-nil, zero value otherwise.

### GetErrorMessageOk

`func (o *Validation) GetErrorMessageOk() (*Message, bool)`

GetErrorMessageOk returns a tuple with the ErrorMessage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorMessage

`func (o *Validation) SetErrorMessage(v Message)`

SetErrorMessage sets ErrorMessage field to given value.

### HasErrorMessage

`func (o *Validation) HasErrorMessage() bool`

HasErrorMessage returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


