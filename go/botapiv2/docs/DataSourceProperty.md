# DataSourceProperty

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Path** | **string** |  | 
**Description** | Pointer to **string** |  | [optional] 
**DataType** | [**DataType**](DataType.md) |  | 
**Enum** | Pointer to **[]string** |  | [optional] 

## Methods

### NewDataSourceProperty

`func NewDataSourceProperty(path string, dataType DataType, ) *DataSourceProperty`

NewDataSourceProperty instantiates a new DataSourceProperty object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDataSourcePropertyWithDefaults

`func NewDataSourcePropertyWithDefaults() *DataSourceProperty`

NewDataSourcePropertyWithDefaults instantiates a new DataSourceProperty object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPath

`func (o *DataSourceProperty) GetPath() string`

GetPath returns the Path field if non-nil, zero value otherwise.

### GetPathOk

`func (o *DataSourceProperty) GetPathOk() (*string, bool)`

GetPathOk returns a tuple with the Path field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPath

`func (o *DataSourceProperty) SetPath(v string)`

SetPath sets Path field to given value.


### GetDescription

`func (o *DataSourceProperty) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *DataSourceProperty) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *DataSourceProperty) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *DataSourceProperty) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetDataType

`func (o *DataSourceProperty) GetDataType() DataType`

GetDataType returns the DataType field if non-nil, zero value otherwise.

### GetDataTypeOk

`func (o *DataSourceProperty) GetDataTypeOk() (*DataType, bool)`

GetDataTypeOk returns a tuple with the DataType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDataType

`func (o *DataSourceProperty) SetDataType(v DataType)`

SetDataType sets DataType field to given value.


### GetEnum

`func (o *DataSourceProperty) GetEnum() []string`

GetEnum returns the Enum field if non-nil, zero value otherwise.

### GetEnumOk

`func (o *DataSourceProperty) GetEnumOk() (*[]string, bool)`

GetEnumOk returns a tuple with the Enum field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEnum

`func (o *DataSourceProperty) SetEnum(v []string)`

SetEnum sets Enum field to given value.

### HasEnum

`func (o *DataSourceProperty) HasEnum() bool`

HasEnum returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


