/*
BotV2

Bot API v2

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package botapiv2

import (
	"encoding/json"
)

// checks if the Locale type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &Locale{}

// Locale struct for Locale
type Locale struct {
	Locale string `json:"locale"`
	Default *bool `json:"default,omitempty"`
	Primary *bool `json:"primary,omitempty"`
	SkipAutoTranslation *bool `json:"skip_auto_translation,omitempty"`
}

// NewLocale instantiates a new Locale object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewLocale(locale string) *Locale {
	this := Locale{}
	this.Locale = locale
	return &this
}

// NewLocaleWithDefaults instantiates a new Locale object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewLocaleWithDefaults() *Locale {
	this := Locale{}
	return &this
}

// GetLocale returns the Locale field value
func (o *Locale) GetLocale() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Locale
}

// GetLocaleOk returns a tuple with the Locale field value
// and a boolean to check if the value has been set.
func (o *Locale) GetLocaleOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Locale, true
}

// SetLocale sets field value
func (o *Locale) SetLocale(v string) {
	o.Locale = v
}

// GetDefault returns the Default field value if set, zero value otherwise.
func (o *Locale) GetDefault() bool {
	if o == nil || IsNil(o.Default) {
		var ret bool
		return ret
	}
	return *o.Default
}

// GetDefaultOk returns a tuple with the Default field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Locale) GetDefaultOk() (*bool, bool) {
	if o == nil || IsNil(o.Default) {
		return nil, false
	}
	return o.Default, true
}

// HasDefault returns a boolean if a field has been set.
func (o *Locale) HasDefault() bool {
	if o != nil && !IsNil(o.Default) {
		return true
	}

	return false
}

// SetDefault gets a reference to the given bool and assigns it to the Default field.
func (o *Locale) SetDefault(v bool) {
	o.Default = &v
}

// GetPrimary returns the Primary field value if set, zero value otherwise.
func (o *Locale) GetPrimary() bool {
	if o == nil || IsNil(o.Primary) {
		var ret bool
		return ret
	}
	return *o.Primary
}

// GetPrimaryOk returns a tuple with the Primary field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Locale) GetPrimaryOk() (*bool, bool) {
	if o == nil || IsNil(o.Primary) {
		return nil, false
	}
	return o.Primary, true
}

// HasPrimary returns a boolean if a field has been set.
func (o *Locale) HasPrimary() bool {
	if o != nil && !IsNil(o.Primary) {
		return true
	}

	return false
}

// SetPrimary gets a reference to the given bool and assigns it to the Primary field.
func (o *Locale) SetPrimary(v bool) {
	o.Primary = &v
}

// GetSkipAutoTranslation returns the SkipAutoTranslation field value if set, zero value otherwise.
func (o *Locale) GetSkipAutoTranslation() bool {
	if o == nil || IsNil(o.SkipAutoTranslation) {
		var ret bool
		return ret
	}
	return *o.SkipAutoTranslation
}

// GetSkipAutoTranslationOk returns a tuple with the SkipAutoTranslation field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Locale) GetSkipAutoTranslationOk() (*bool, bool) {
	if o == nil || IsNil(o.SkipAutoTranslation) {
		return nil, false
	}
	return o.SkipAutoTranslation, true
}

// HasSkipAutoTranslation returns a boolean if a field has been set.
func (o *Locale) HasSkipAutoTranslation() bool {
	if o != nil && !IsNil(o.SkipAutoTranslation) {
		return true
	}

	return false
}

// SetSkipAutoTranslation gets a reference to the given bool and assigns it to the SkipAutoTranslation field.
func (o *Locale) SetSkipAutoTranslation(v bool) {
	o.SkipAutoTranslation = &v
}

func (o Locale) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o Locale) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["locale"] = o.Locale
	if !IsNil(o.Default) {
		toSerialize["default"] = o.Default
	}
	if !IsNil(o.Primary) {
		toSerialize["primary"] = o.Primary
	}
	if !IsNil(o.SkipAutoTranslation) {
		toSerialize["skip_auto_translation"] = o.SkipAutoTranslation
	}
	return toSerialize, nil
}

type NullableLocale struct {
	value *Locale
	isSet bool
}

func (v NullableLocale) Get() *Locale {
	return v.value
}

func (v *NullableLocale) Set(val *Locale) {
	v.value = val
	v.isSet = true
}

func (v NullableLocale) IsSet() bool {
	return v.isSet
}

func (v *NullableLocale) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableLocale(val *Locale) *NullableLocale {
	return &NullableLocale{value: val, isSet: true}
}

func (v NullableLocale) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableLocale) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


