# Go API client for posapi

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

## Overview
This API client was generated by the [OpenAPI Generator](https://openapi-generator.tech) project.  By using the [OpenAPI-spec](https://www.openapis.org/) from a remote server, you can easily generate an API client.

- API version: 1.0
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.GoClientCodegen

## Installation

Install the following dependencies:

```shell
go get github.com/stretchr/testify/assert
go get golang.org/x/net/context
```

Put the package under your project folder and add the following in import:

```golang
import posapi "bitbucket.org/_SLACE_/clients/go/posapi"
```

To use a proxy, set the environment variable `HTTP_PROXY`:

```golang
os.Setenv("HTTP_PROXY", "http://proxy_name:proxy_port")
```

## Configuration of Server URL

Default configuration comes with `Servers` field that contains server objects as defined in the OpenAPI specification.

### Select Server Configuration

For using other server than the one defined on index 0 set context value `sw.ContextServerIndex` of type `int`.

```golang
ctx := context.WithValue(context.Background(), posapi.ContextServerIndex, 1)
```

### Templated Server URL

Templated server URL is formatted using default variables from configuration or from context value `sw.ContextServerVariables` of type `map[string]string`.

```golang
ctx := context.WithValue(context.Background(), posapi.ContextServerVariables, map[string]string{
	"basePath": "v2",
})
```

Note, enum values are always validated and all unused variables are silently ignored.

### URLs Configuration per Operation

Each operation can use different server URL defined using `OperationServers` map in the `Configuration`.
An operation is uniquely identified by `"{classname}Service.{nickname}"` string.
Similar rules for overriding default operation server index and variables applies by using `sw.ContextOperationServerIndices` and `sw.ContextOperationServerVariables` context maps.

```golang
ctx := context.WithValue(context.Background(), posapi.ContextOperationServerIndices, map[string]int{
	"{classname}Service.{nickname}": 2,
})
ctx = context.WithValue(context.Background(), posapi.ContextOperationServerVariables, map[string]map[string]string{
	"{classname}Service.{nickname}": {
		"port": "8443",
	},
})
```

## Documentation for API Endpoints

All URIs are relative to *https://pos.slace.com/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AnybillAPI* | [**CreateAnybillConfiguration**](docs/AnybillAPI.md#createanybillconfiguration) | **Post** /integrations/anybill/organizations/{oid}/configurations | Create Anybill configuration
*AnybillAPI* | [**DeleteAnybillConfiguration**](docs/AnybillAPI.md#deleteanybillconfiguration) | **Delete** /integrations/anybill/organizations/{oid}/configurations | Delete Anybill configuration
*AnybillAPI* | [**GetAnybillConfiguration**](docs/AnybillAPI.md#getanybillconfiguration) | **Get** /integrations/anybill/organizations/{oid}/configurations | Get Anybill configuration
*BongoAPI* | [**BongoCheckReceipt**](docs/BongoAPI.md#bongocheckreceipt) | **Post** /integrations/bongo/check-receipt | Check receipt
*CustomAPI* | [**DeleteCustomHandler**](docs/CustomAPI.md#deletecustomhandler) | **Delete** /integrations/custom/organizations/{orgId}/channels{channelId}/handlers/{id} | Delete custom handler
*CustomAPI* | [**DeleteOrganizationCustomHandler**](docs/CustomAPI.md#deleteorganizationcustomhandler) | **Delete** /integrations/custom/organizations/{orgId}/handlers/{id} | Delete organization custom handler
*CustomAPI* | [**GetCustomHandler**](docs/CustomAPI.md#getcustomhandler) | **Get** /integrations/custom/organizations/{orgId}/channels{channelId}/handlers/{id} | Get custom handler
*CustomAPI* | [**GetOrganizationCustomHandler**](docs/CustomAPI.md#getorganizationcustomhandler) | **Get** /integrations/custom/organizations/{orgId}/handlers/{id} | Get organization custom handler
*CustomAPI* | [**ListCustomHandlers**](docs/CustomAPI.md#listcustomhandlers) | **Get** /integrations/custom/organizations/{orgId}/channels{channelId}/handlers | List custom handlers
*CustomAPI* | [**ListOrganizationCustomHandlers**](docs/CustomAPI.md#listorganizationcustomhandlers) | **Get** /integrations/custom/organizations/{orgId}/handlers | List organization custom handlers
*CustomAPI* | [**OrganizationWebhook**](docs/CustomAPI.md#organizationwebhook) | **Post** /integrations/custom/organizations/{orgId}/webhooks/{type} | Webhook (organization)
*CustomAPI* | [**RegisterCustomHandler**](docs/CustomAPI.md#registercustomhandler) | **Post** /integrations/custom/organizations/{orgId}/channels{channelId}/handlers | Register custom handler
*CustomAPI* | [**RegisterOrganizationCustomHandler**](docs/CustomAPI.md#registerorganizationcustomhandler) | **Post** /integrations/custom/organizations/{orgId}/handlers | Register organization custom handler
*CustomAPI* | [**Webhook**](docs/CustomAPI.md#webhook) | **Post** /integrations/custom/organizations/{orgId}/channels{channelId}/webhooks/{type} | Webhook
*SettingsAPI* | [**GetAvailableActions**](docs/SettingsAPI.md#getavailableactions) | **Get** /settings/organizations/{organizationId}/available-actions | Get available actions for organization


## Documentation For Models

 - [AddReceiptWebhook](docs/AddReceiptWebhook.md)
 - [AddReceiptWebhookReceipt](docs/AddReceiptWebhookReceipt.md)
 - [AnybillConfiguration](docs/AnybillConfiguration.md)
 - [AvailableActionsResponse](docs/AvailableActionsResponse.md)
 - [BongoReceipt](docs/BongoReceipt.md)
 - [CheckReceiptRequest](docs/CheckReceiptRequest.md)
 - [ContactData](docs/ContactData.md)
 - [CreateAnybillConfigRequest](docs/CreateAnybillConfigRequest.md)
 - [CustomHandler](docs/CustomHandler.md)
 - [GetReceiptWebhook](docs/GetReceiptWebhook.md)
 - [GetReceiptWebhookReceipt](docs/GetReceiptWebhookReceipt.md)
 - [WebhookRequest](docs/WebhookRequest.md)


## Documentation For Authorization


Authentication schemes defined for the API:
### Authorization

- **Type**: HTTP Bearer token authentication

Example

```golang
auth := context.WithValue(context.Background(), sw.ContextAccessToken, "BEARER_TOKEN_STRING")
r, err := client.Service.Operation(auth, args)
```


## Documentation for Utility Methods

Due to the fact that model structure members are all pointers, this package contains
a number of utility functions to easily obtain pointers to values of basic types.
Each of these functions takes a value of the given basic type and returns a pointer to it:

* `PtrBool`
* `PtrInt`
* `PtrInt32`
* `PtrInt64`
* `PtrFloat`
* `PtrFloat32`
* `PtrFloat64`
* `PtrString`
* `PtrTime`

## Author



