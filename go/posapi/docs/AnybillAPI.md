# \AnybillAPI

All URIs are relative to *https://pos.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateAnybillConfiguration**](AnybillAPI.md#CreateAnybillConfiguration) | **Post** /integrations/anybill/organizations/{oid}/configurations | Create Anybill configuration
[**DeleteAnybillConfiguration**](AnybillAPI.md#DeleteAnybillConfiguration) | **Delete** /integrations/anybill/organizations/{oid}/configurations | Delete Anybill configuration
[**GetAnybillConfiguration**](AnybillAPI.md#GetAnybillConfiguration) | **Get** /integrations/anybill/organizations/{oid}/configurations | Get Anybill configuration



## CreateAnybillConfiguration

> AnybillConfiguration CreateAnybillConfiguration(ctx, oid).CreateAnybillConfigRequest(createAnybillConfigRequest).Execute()

Create Anybill configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    oid := "oid_example" // string | Organization ID
    createAnybillConfigRequest := *openapiclient.NewCreateAnybillConfigRequest("ClientId_example", "ClientSecret_example", "ApiUrl_example", "Username_example", "Password_example", "TokenUrl_example", "WebhookSecret_example") // CreateAnybillConfigRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.AnybillAPI.CreateAnybillConfiguration(context.Background(), oid).CreateAnybillConfigRequest(createAnybillConfigRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AnybillAPI.CreateAnybillConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateAnybillConfiguration`: AnybillConfiguration
    fmt.Fprintf(os.Stdout, "Response from `AnybillAPI.CreateAnybillConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** | Organization ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateAnybillConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **createAnybillConfigRequest** | [**CreateAnybillConfigRequest**](CreateAnybillConfigRequest.md) |  | 

### Return type

[**AnybillConfiguration**](AnybillConfiguration.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteAnybillConfiguration

> DeleteAnybillConfiguration(ctx, oid).Execute()

Delete Anybill configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    oid := "oid_example" // string | Organization ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.AnybillAPI.DeleteAnybillConfiguration(context.Background(), oid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AnybillAPI.DeleteAnybillConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** | Organization ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteAnybillConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAnybillConfiguration

> AnybillConfiguration GetAnybillConfiguration(ctx, oid).Execute()

Get Anybill configuration



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    oid := "oid_example" // string | Organization ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.AnybillAPI.GetAnybillConfiguration(context.Background(), oid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AnybillAPI.GetAnybillConfiguration``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetAnybillConfiguration`: AnybillConfiguration
    fmt.Fprintf(os.Stdout, "Response from `AnybillAPI.GetAnybillConfiguration`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**oid** | **string** | Organization ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetAnybillConfigurationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**AnybillConfiguration**](AnybillConfiguration.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

