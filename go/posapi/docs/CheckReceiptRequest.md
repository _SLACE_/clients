# CheckReceiptRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TransactionId** | **string** |  | 
**ContactId** | **string** |  | 
**ReceiptUrl** | **string** |  | 
**ChannelId** | **string** |  | 
**ConversationId** | **string** |  | 
**OrganizationId** | **string** |  | 
**Attempt** | **int32** |  | 

## Methods

### NewCheckReceiptRequest

`func NewCheckReceiptRequest(transactionId string, contactId string, receiptUrl string, channelId string, conversationId string, organizationId string, attempt int32, ) *CheckReceiptRequest`

NewCheckReceiptRequest instantiates a new CheckReceiptRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCheckReceiptRequestWithDefaults

`func NewCheckReceiptRequestWithDefaults() *CheckReceiptRequest`

NewCheckReceiptRequestWithDefaults instantiates a new CheckReceiptRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTransactionId

`func (o *CheckReceiptRequest) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *CheckReceiptRequest) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *CheckReceiptRequest) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetContactId

`func (o *CheckReceiptRequest) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *CheckReceiptRequest) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *CheckReceiptRequest) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetReceiptUrl

`func (o *CheckReceiptRequest) GetReceiptUrl() string`

GetReceiptUrl returns the ReceiptUrl field if non-nil, zero value otherwise.

### GetReceiptUrlOk

`func (o *CheckReceiptRequest) GetReceiptUrlOk() (*string, bool)`

GetReceiptUrlOk returns a tuple with the ReceiptUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReceiptUrl

`func (o *CheckReceiptRequest) SetReceiptUrl(v string)`

SetReceiptUrl sets ReceiptUrl field to given value.


### GetChannelId

`func (o *CheckReceiptRequest) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CheckReceiptRequest) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CheckReceiptRequest) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetConversationId

`func (o *CheckReceiptRequest) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *CheckReceiptRequest) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *CheckReceiptRequest) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetOrganizationId

`func (o *CheckReceiptRequest) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *CheckReceiptRequest) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *CheckReceiptRequest) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetAttempt

`func (o *CheckReceiptRequest) GetAttempt() int32`

GetAttempt returns the Attempt field if non-nil, zero value otherwise.

### GetAttemptOk

`func (o *CheckReceiptRequest) GetAttemptOk() (*int32, bool)`

GetAttemptOk returns a tuple with the Attempt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttempt

`func (o *CheckReceiptRequest) SetAttempt(v int32)`

SetAttempt sets Attempt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


