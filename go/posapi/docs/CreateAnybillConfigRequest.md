# CreateAnybillConfigRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ClientId** | **string** |  | 
**ClientSecret** | **string** |  | 
**ApiUrl** | **string** |  | 
**Username** | **string** |  | 
**Password** | **string** |  | 
**TokenUrl** | **string** |  | 
**WebhookSecret** | **string** |  | 

## Methods

### NewCreateAnybillConfigRequest

`func NewCreateAnybillConfigRequest(clientId string, clientSecret string, apiUrl string, username string, password string, tokenUrl string, webhookSecret string, ) *CreateAnybillConfigRequest`

NewCreateAnybillConfigRequest instantiates a new CreateAnybillConfigRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateAnybillConfigRequestWithDefaults

`func NewCreateAnybillConfigRequestWithDefaults() *CreateAnybillConfigRequest`

NewCreateAnybillConfigRequestWithDefaults instantiates a new CreateAnybillConfigRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetClientId

`func (o *CreateAnybillConfigRequest) GetClientId() string`

GetClientId returns the ClientId field if non-nil, zero value otherwise.

### GetClientIdOk

`func (o *CreateAnybillConfigRequest) GetClientIdOk() (*string, bool)`

GetClientIdOk returns a tuple with the ClientId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetClientId

`func (o *CreateAnybillConfigRequest) SetClientId(v string)`

SetClientId sets ClientId field to given value.


### GetClientSecret

`func (o *CreateAnybillConfigRequest) GetClientSecret() string`

GetClientSecret returns the ClientSecret field if non-nil, zero value otherwise.

### GetClientSecretOk

`func (o *CreateAnybillConfigRequest) GetClientSecretOk() (*string, bool)`

GetClientSecretOk returns a tuple with the ClientSecret field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetClientSecret

`func (o *CreateAnybillConfigRequest) SetClientSecret(v string)`

SetClientSecret sets ClientSecret field to given value.


### GetApiUrl

`func (o *CreateAnybillConfigRequest) GetApiUrl() string`

GetApiUrl returns the ApiUrl field if non-nil, zero value otherwise.

### GetApiUrlOk

`func (o *CreateAnybillConfigRequest) GetApiUrlOk() (*string, bool)`

GetApiUrlOk returns a tuple with the ApiUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetApiUrl

`func (o *CreateAnybillConfigRequest) SetApiUrl(v string)`

SetApiUrl sets ApiUrl field to given value.


### GetUsername

`func (o *CreateAnybillConfigRequest) GetUsername() string`

GetUsername returns the Username field if non-nil, zero value otherwise.

### GetUsernameOk

`func (o *CreateAnybillConfigRequest) GetUsernameOk() (*string, bool)`

GetUsernameOk returns a tuple with the Username field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsername

`func (o *CreateAnybillConfigRequest) SetUsername(v string)`

SetUsername sets Username field to given value.


### GetPassword

`func (o *CreateAnybillConfigRequest) GetPassword() string`

GetPassword returns the Password field if non-nil, zero value otherwise.

### GetPasswordOk

`func (o *CreateAnybillConfigRequest) GetPasswordOk() (*string, bool)`

GetPasswordOk returns a tuple with the Password field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPassword

`func (o *CreateAnybillConfigRequest) SetPassword(v string)`

SetPassword sets Password field to given value.


### GetTokenUrl

`func (o *CreateAnybillConfigRequest) GetTokenUrl() string`

GetTokenUrl returns the TokenUrl field if non-nil, zero value otherwise.

### GetTokenUrlOk

`func (o *CreateAnybillConfigRequest) GetTokenUrlOk() (*string, bool)`

GetTokenUrlOk returns a tuple with the TokenUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTokenUrl

`func (o *CreateAnybillConfigRequest) SetTokenUrl(v string)`

SetTokenUrl sets TokenUrl field to given value.


### GetWebhookSecret

`func (o *CreateAnybillConfigRequest) GetWebhookSecret() string`

GetWebhookSecret returns the WebhookSecret field if non-nil, zero value otherwise.

### GetWebhookSecretOk

`func (o *CreateAnybillConfigRequest) GetWebhookSecretOk() (*string, bool)`

GetWebhookSecretOk returns a tuple with the WebhookSecret field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWebhookSecret

`func (o *CreateAnybillConfigRequest) SetWebhookSecret(v string)`

SetWebhookSecret sets WebhookSecret field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


