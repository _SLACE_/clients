# AddReceiptWebhookReceipt

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Status** | **string** |  | 
**Error** | Pointer to **string** |  | [optional] 
**LocationId** | Pointer to **string** |  | [optional] 
**Time** | Pointer to **string** |  | [optional] 
**Date** | Pointer to **string** |  | [optional] 
**Total** | Pointer to **string** |  | [optional] 

## Methods

### NewAddReceiptWebhookReceipt

`func NewAddReceiptWebhookReceipt(status string, ) *AddReceiptWebhookReceipt`

NewAddReceiptWebhookReceipt instantiates a new AddReceiptWebhookReceipt object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAddReceiptWebhookReceiptWithDefaults

`func NewAddReceiptWebhookReceiptWithDefaults() *AddReceiptWebhookReceipt`

NewAddReceiptWebhookReceiptWithDefaults instantiates a new AddReceiptWebhookReceipt object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStatus

`func (o *AddReceiptWebhookReceipt) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *AddReceiptWebhookReceipt) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *AddReceiptWebhookReceipt) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetError

`func (o *AddReceiptWebhookReceipt) GetError() string`

GetError returns the Error field if non-nil, zero value otherwise.

### GetErrorOk

`func (o *AddReceiptWebhookReceipt) GetErrorOk() (*string, bool)`

GetErrorOk returns a tuple with the Error field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetError

`func (o *AddReceiptWebhookReceipt) SetError(v string)`

SetError sets Error field to given value.

### HasError

`func (o *AddReceiptWebhookReceipt) HasError() bool`

HasError returns a boolean if a field has been set.

### GetLocationId

`func (o *AddReceiptWebhookReceipt) GetLocationId() string`

GetLocationId returns the LocationId field if non-nil, zero value otherwise.

### GetLocationIdOk

`func (o *AddReceiptWebhookReceipt) GetLocationIdOk() (*string, bool)`

GetLocationIdOk returns a tuple with the LocationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationId

`func (o *AddReceiptWebhookReceipt) SetLocationId(v string)`

SetLocationId sets LocationId field to given value.

### HasLocationId

`func (o *AddReceiptWebhookReceipt) HasLocationId() bool`

HasLocationId returns a boolean if a field has been set.

### GetTime

`func (o *AddReceiptWebhookReceipt) GetTime() string`

GetTime returns the Time field if non-nil, zero value otherwise.

### GetTimeOk

`func (o *AddReceiptWebhookReceipt) GetTimeOk() (*string, bool)`

GetTimeOk returns a tuple with the Time field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTime

`func (o *AddReceiptWebhookReceipt) SetTime(v string)`

SetTime sets Time field to given value.

### HasTime

`func (o *AddReceiptWebhookReceipt) HasTime() bool`

HasTime returns a boolean if a field has been set.

### GetDate

`func (o *AddReceiptWebhookReceipt) GetDate() string`

GetDate returns the Date field if non-nil, zero value otherwise.

### GetDateOk

`func (o *AddReceiptWebhookReceipt) GetDateOk() (*string, bool)`

GetDateOk returns a tuple with the Date field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDate

`func (o *AddReceiptWebhookReceipt) SetDate(v string)`

SetDate sets Date field to given value.

### HasDate

`func (o *AddReceiptWebhookReceipt) HasDate() bool`

HasDate returns a boolean if a field has been set.

### GetTotal

`func (o *AddReceiptWebhookReceipt) GetTotal() string`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *AddReceiptWebhookReceipt) GetTotalOk() (*string, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *AddReceiptWebhookReceipt) SetTotal(v string)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *AddReceiptWebhookReceipt) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


