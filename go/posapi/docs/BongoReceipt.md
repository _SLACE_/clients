# BongoReceipt

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Status** | **string** |  | [readonly] 
**Error** | **string** |  | [readonly] 
**ReceiptData** | **map[string]interface{}** |  | 
**Attempt** | **int32** |  | [readonly] 
**Id** | **string** |  | [readonly] 
**TransactionId** | **string** |  | [readonly] 
**ContactId** | **string** |  | [readonly] 
**ConversationId** | **string** |  | [readonly] 
**ChannelId** | **string** |  | [readonly] 
**OrganizationId** | **string** |  | [readonly] 
**ReceiptUrl** | **string** |  | [readonly] 
**CreatedAt** | **time.Time** |  | [readonly] 
**UpdatedAt** | **time.Time** |  | [readonly] 

## Methods

### NewBongoReceipt

`func NewBongoReceipt(status string, error_ string, receiptData map[string]interface{}, attempt int32, id string, transactionId string, contactId string, conversationId string, channelId string, organizationId string, receiptUrl string, createdAt time.Time, updatedAt time.Time, ) *BongoReceipt`

NewBongoReceipt instantiates a new BongoReceipt object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBongoReceiptWithDefaults

`func NewBongoReceiptWithDefaults() *BongoReceipt`

NewBongoReceiptWithDefaults instantiates a new BongoReceipt object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStatus

`func (o *BongoReceipt) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *BongoReceipt) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *BongoReceipt) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetError

`func (o *BongoReceipt) GetError() string`

GetError returns the Error field if non-nil, zero value otherwise.

### GetErrorOk

`func (o *BongoReceipt) GetErrorOk() (*string, bool)`

GetErrorOk returns a tuple with the Error field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetError

`func (o *BongoReceipt) SetError(v string)`

SetError sets Error field to given value.


### GetReceiptData

`func (o *BongoReceipt) GetReceiptData() map[string]interface{}`

GetReceiptData returns the ReceiptData field if non-nil, zero value otherwise.

### GetReceiptDataOk

`func (o *BongoReceipt) GetReceiptDataOk() (*map[string]interface{}, bool)`

GetReceiptDataOk returns a tuple with the ReceiptData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReceiptData

`func (o *BongoReceipt) SetReceiptData(v map[string]interface{})`

SetReceiptData sets ReceiptData field to given value.


### GetAttempt

`func (o *BongoReceipt) GetAttempt() int32`

GetAttempt returns the Attempt field if non-nil, zero value otherwise.

### GetAttemptOk

`func (o *BongoReceipt) GetAttemptOk() (*int32, bool)`

GetAttemptOk returns a tuple with the Attempt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttempt

`func (o *BongoReceipt) SetAttempt(v int32)`

SetAttempt sets Attempt field to given value.


### GetId

`func (o *BongoReceipt) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *BongoReceipt) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *BongoReceipt) SetId(v string)`

SetId sets Id field to given value.


### GetTransactionId

`func (o *BongoReceipt) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *BongoReceipt) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *BongoReceipt) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetContactId

`func (o *BongoReceipt) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *BongoReceipt) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *BongoReceipt) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetConversationId

`func (o *BongoReceipt) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *BongoReceipt) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *BongoReceipt) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetChannelId

`func (o *BongoReceipt) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *BongoReceipt) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *BongoReceipt) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetOrganizationId

`func (o *BongoReceipt) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *BongoReceipt) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *BongoReceipt) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.


### GetReceiptUrl

`func (o *BongoReceipt) GetReceiptUrl() string`

GetReceiptUrl returns the ReceiptUrl field if non-nil, zero value otherwise.

### GetReceiptUrlOk

`func (o *BongoReceipt) GetReceiptUrlOk() (*string, bool)`

GetReceiptUrlOk returns a tuple with the ReceiptUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReceiptUrl

`func (o *BongoReceipt) SetReceiptUrl(v string)`

SetReceiptUrl sets ReceiptUrl field to given value.


### GetCreatedAt

`func (o *BongoReceipt) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *BongoReceipt) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *BongoReceipt) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *BongoReceipt) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *BongoReceipt) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *BongoReceipt) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


