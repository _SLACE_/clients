# \CustomAPI

All URIs are relative to *https://pos.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteCustomHandler**](CustomAPI.md#DeleteCustomHandler) | **Delete** /integrations/custom/organizations/{orgId}/channels{channelId}/handlers/{id} | Delete custom handler
[**DeleteOrganizationCustomHandler**](CustomAPI.md#DeleteOrganizationCustomHandler) | **Delete** /integrations/custom/organizations/{orgId}/handlers/{id} | Delete organization custom handler
[**GetCustomHandler**](CustomAPI.md#GetCustomHandler) | **Get** /integrations/custom/organizations/{orgId}/channels{channelId}/handlers/{id} | Get custom handler
[**GetOrganizationCustomHandler**](CustomAPI.md#GetOrganizationCustomHandler) | **Get** /integrations/custom/organizations/{orgId}/handlers/{id} | Get organization custom handler
[**ListCustomHandlers**](CustomAPI.md#ListCustomHandlers) | **Get** /integrations/custom/organizations/{orgId}/channels{channelId}/handlers | List custom handlers
[**ListOrganizationCustomHandlers**](CustomAPI.md#ListOrganizationCustomHandlers) | **Get** /integrations/custom/organizations/{orgId}/handlers | List organization custom handlers
[**OrganizationWebhook**](CustomAPI.md#OrganizationWebhook) | **Post** /integrations/custom/organizations/{orgId}/webhooks/{type} | Webhook (organization)
[**RegisterCustomHandler**](CustomAPI.md#RegisterCustomHandler) | **Post** /integrations/custom/organizations/{orgId}/channels{channelId}/handlers | Register custom handler
[**RegisterOrganizationCustomHandler**](CustomAPI.md#RegisterOrganizationCustomHandler) | **Post** /integrations/custom/organizations/{orgId}/handlers | Register organization custom handler
[**Webhook**](CustomAPI.md#Webhook) | **Post** /integrations/custom/organizations/{orgId}/channels{channelId}/webhooks/{type} | Webhook



## DeleteCustomHandler

> DeleteCustomHandler(ctx, channelId, id, orgId).Execute()

Delete custom handler



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 
    orgId := "orgId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.CustomAPI.DeleteCustomHandler(context.Background(), channelId, id, orgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CustomAPI.DeleteCustomHandler``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteCustomHandlerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteOrganizationCustomHandler

> DeleteOrganizationCustomHandler(ctx, id, orgId).Execute()

Delete organization custom handler



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    id := "id_example" // string | 
    orgId := "orgId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.CustomAPI.DeleteOrganizationCustomHandler(context.Background(), id, orgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CustomAPI.DeleteOrganizationCustomHandler``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteOrganizationCustomHandlerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCustomHandler

> CustomHandler GetCustomHandler(ctx, channelId, id, orgId).Execute()

Get custom handler



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    channelId := "channelId_example" // string | 
    id := "id_example" // string | 
    orgId := "orgId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CustomAPI.GetCustomHandler(context.Background(), channelId, id, orgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CustomAPI.GetCustomHandler``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCustomHandler`: CustomHandler
    fmt.Fprintf(os.Stdout, "Response from `CustomAPI.GetCustomHandler`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**id** | **string** |  | 
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetCustomHandlerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**CustomHandler**](CustomHandler.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationCustomHandler

> CustomHandler GetOrganizationCustomHandler(ctx, id, orgId).Execute()

Get organization custom handler



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    id := "id_example" // string | 
    orgId := "orgId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CustomAPI.GetOrganizationCustomHandler(context.Background(), id, orgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CustomAPI.GetOrganizationCustomHandler``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationCustomHandler`: CustomHandler
    fmt.Fprintf(os.Stdout, "Response from `CustomAPI.GetOrganizationCustomHandler`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**id** | **string** |  | 
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationCustomHandlerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**CustomHandler**](CustomHandler.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListCustomHandlers

> []CustomHandler ListCustomHandlers(ctx, channelId, orgId).Execute()

List custom handlers

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    channelId := "channelId_example" // string | 
    orgId := "orgId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CustomAPI.ListCustomHandlers(context.Background(), channelId, orgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CustomAPI.ListCustomHandlers``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListCustomHandlers`: []CustomHandler
    fmt.Fprintf(os.Stdout, "Response from `CustomAPI.ListCustomHandlers`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListCustomHandlersRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**[]CustomHandler**](CustomHandler.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListOrganizationCustomHandlers

> []CustomHandler ListOrganizationCustomHandlers(ctx, orgId).Execute()

List organization custom handlers

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    orgId := "orgId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CustomAPI.ListOrganizationCustomHandlers(context.Background(), orgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CustomAPI.ListOrganizationCustomHandlers``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListOrganizationCustomHandlers`: []CustomHandler
    fmt.Fprintf(os.Stdout, "Response from `CustomAPI.ListOrganizationCustomHandlers`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListOrganizationCustomHandlersRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]CustomHandler**](CustomHandler.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## OrganizationWebhook

> OrganizationWebhook(ctx, type_, orgId).WebhookRequest(webhookRequest).Execute()

Webhook (organization)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    type_ := "type__example" // string | 
    orgId := "orgId_example" // string | 
    webhookRequest := *openapiclient.NewWebhookRequest("TransactionId_example", "ChannelId_example", "ConversationId_example", "ContactId_example", *openapiclient.NewAddReceiptWebhookReceipt("Status_example")) // WebhookRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.CustomAPI.OrganizationWebhook(context.Background(), type_, orgId).WebhookRequest(webhookRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CustomAPI.OrganizationWebhook``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**type_** | **string** |  | 
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiOrganizationWebhookRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **webhookRequest** | [**WebhookRequest**](WebhookRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RegisterCustomHandler

> CustomHandler RegisterCustomHandler(ctx, channelId, orgId).CustomHandler(customHandler).Execute()

Register custom handler

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    channelId := "channelId_example" // string | 
    orgId := "orgId_example" // string | 
    customHandler := *openapiclient.NewCustomHandler("Id_example", "ChannelId_example", "Type_example", "Url_example", false, time.Now(), time.Now(), "OrganizationId_example") // CustomHandler |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CustomAPI.RegisterCustomHandler(context.Background(), channelId, orgId).CustomHandler(customHandler).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CustomAPI.RegisterCustomHandler``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RegisterCustomHandler`: CustomHandler
    fmt.Fprintf(os.Stdout, "Response from `CustomAPI.RegisterCustomHandler`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiRegisterCustomHandlerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **customHandler** | [**CustomHandler**](CustomHandler.md) |  | 

### Return type

[**CustomHandler**](CustomHandler.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RegisterOrganizationCustomHandler

> CustomHandler RegisterOrganizationCustomHandler(ctx, orgId).CustomHandler(customHandler).Execute()

Register organization custom handler

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    orgId := "orgId_example" // string | 
    customHandler := *openapiclient.NewCustomHandler("Id_example", "ChannelId_example", "Type_example", "Url_example", false, time.Now(), time.Now(), "OrganizationId_example") // CustomHandler |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.CustomAPI.RegisterOrganizationCustomHandler(context.Background(), orgId).CustomHandler(customHandler).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CustomAPI.RegisterOrganizationCustomHandler``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `RegisterOrganizationCustomHandler`: CustomHandler
    fmt.Fprintf(os.Stdout, "Response from `CustomAPI.RegisterOrganizationCustomHandler`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiRegisterOrganizationCustomHandlerRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **customHandler** | [**CustomHandler**](CustomHandler.md) |  | 

### Return type

[**CustomHandler**](CustomHandler.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## Webhook

> Webhook(ctx, channelId, type_, orgId).WebhookRequest(webhookRequest).Execute()

Webhook



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    channelId := "channelId_example" // string | 
    type_ := "type__example" // string | 
    orgId := "orgId_example" // string | 
    webhookRequest := *openapiclient.NewWebhookRequest("TransactionId_example", "ChannelId_example", "ConversationId_example", "ContactId_example", *openapiclient.NewAddReceiptWebhookReceipt("Status_example")) // WebhookRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.CustomAPI.Webhook(context.Background(), channelId, type_, orgId).WebhookRequest(webhookRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `CustomAPI.Webhook``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**type_** | **string** |  | 
**orgId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiWebhookRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **webhookRequest** | [**WebhookRequest**](WebhookRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

