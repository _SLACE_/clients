# GetReceiptWebhook

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TransactionId** | **string** |  | 
**ChannelId** | **string** |  | 
**ConversationId** | **string** |  | 
**ContactId** | **string** |  | 
**Attributes** | Pointer to **map[string]interface{}** |  | [optional] 
**Receipt** | [**GetReceiptWebhookReceipt**](GetReceiptWebhookReceipt.md) |  | 

## Methods

### NewGetReceiptWebhook

`func NewGetReceiptWebhook(transactionId string, channelId string, conversationId string, contactId string, receipt GetReceiptWebhookReceipt, ) *GetReceiptWebhook`

NewGetReceiptWebhook instantiates a new GetReceiptWebhook object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetReceiptWebhookWithDefaults

`func NewGetReceiptWebhookWithDefaults() *GetReceiptWebhook`

NewGetReceiptWebhookWithDefaults instantiates a new GetReceiptWebhook object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTransactionId

`func (o *GetReceiptWebhook) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *GetReceiptWebhook) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *GetReceiptWebhook) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetChannelId

`func (o *GetReceiptWebhook) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *GetReceiptWebhook) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *GetReceiptWebhook) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetConversationId

`func (o *GetReceiptWebhook) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *GetReceiptWebhook) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *GetReceiptWebhook) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetContactId

`func (o *GetReceiptWebhook) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *GetReceiptWebhook) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *GetReceiptWebhook) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetAttributes

`func (o *GetReceiptWebhook) GetAttributes() map[string]interface{}`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *GetReceiptWebhook) GetAttributesOk() (*map[string]interface{}, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *GetReceiptWebhook) SetAttributes(v map[string]interface{})`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *GetReceiptWebhook) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetReceipt

`func (o *GetReceiptWebhook) GetReceipt() GetReceiptWebhookReceipt`

GetReceipt returns the Receipt field if non-nil, zero value otherwise.

### GetReceiptOk

`func (o *GetReceiptWebhook) GetReceiptOk() (*GetReceiptWebhookReceipt, bool)`

GetReceiptOk returns a tuple with the Receipt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReceipt

`func (o *GetReceiptWebhook) SetReceipt(v GetReceiptWebhookReceipt)`

SetReceipt sets Receipt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


