# GetReceiptWebhookReceipt

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**FileUrl** | **string** |  | 

## Methods

### NewGetReceiptWebhookReceipt

`func NewGetReceiptWebhookReceipt(id string, fileUrl string, ) *GetReceiptWebhookReceipt`

NewGetReceiptWebhookReceipt instantiates a new GetReceiptWebhookReceipt object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetReceiptWebhookReceiptWithDefaults

`func NewGetReceiptWebhookReceiptWithDefaults() *GetReceiptWebhookReceipt`

NewGetReceiptWebhookReceiptWithDefaults instantiates a new GetReceiptWebhookReceipt object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *GetReceiptWebhookReceipt) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *GetReceiptWebhookReceipt) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *GetReceiptWebhookReceipt) SetId(v string)`

SetId sets Id field to given value.


### GetFileUrl

`func (o *GetReceiptWebhookReceipt) GetFileUrl() string`

GetFileUrl returns the FileUrl field if non-nil, zero value otherwise.

### GetFileUrlOk

`func (o *GetReceiptWebhookReceipt) GetFileUrlOk() (*string, bool)`

GetFileUrlOk returns a tuple with the FileUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFileUrl

`func (o *GetReceiptWebhookReceipt) SetFileUrl(v string)`

SetFileUrl sets FileUrl field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


