# \BongoAPI

All URIs are relative to *https://pos.slace.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**BongoCheckReceipt**](BongoAPI.md#BongoCheckReceipt) | **Post** /integrations/bongo/check-receipt | Check receipt



## BongoCheckReceipt

> BongoReceipt BongoCheckReceipt(ctx).CheckReceiptRequest(checkReceiptRequest).Execute()

Check receipt

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/posapi"
)

func main() {
    checkReceiptRequest := *openapiclient.NewCheckReceiptRequest("TransactionId_example", "ContactId_example", "ReceiptUrl_example", "ChannelId_example", "ConversationId_example", "OrganizationId_example", int32(123)) // CheckReceiptRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.BongoAPI.BongoCheckReceipt(context.Background()).CheckReceiptRequest(checkReceiptRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BongoAPI.BongoCheckReceipt``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `BongoCheckReceipt`: BongoReceipt
    fmt.Fprintf(os.Stdout, "Response from `BongoAPI.BongoCheckReceipt`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiBongoCheckReceiptRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checkReceiptRequest** | [**CheckReceiptRequest**](CheckReceiptRequest.md) |  | 

### Return type

[**BongoReceipt**](BongoReceipt.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

