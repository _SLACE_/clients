# CustomHandler

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [readonly] 
**ChannelId** | **string** |  | 
**Type** | **string** |  | 
**Url** | **string** |  | 
**Secret** | Pointer to **string** |  | [optional] 
**SecretHeader** | Pointer to **string** |  | [optional] 
**SecretConfigured** | **bool** |  | [readonly] 
**Attributes** | Pointer to **map[string]interface{}** |  | [optional] 
**CreatedAt** | **time.Time** |  | [readonly] 
**UpdatedAt** | **time.Time** |  | [readonly] 
**OrganizationId** | **string** |  | [readonly] 

## Methods

### NewCustomHandler

`func NewCustomHandler(id string, channelId string, type_ string, url string, secretConfigured bool, createdAt time.Time, updatedAt time.Time, organizationId string, ) *CustomHandler`

NewCustomHandler instantiates a new CustomHandler object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCustomHandlerWithDefaults

`func NewCustomHandlerWithDefaults() *CustomHandler`

NewCustomHandlerWithDefaults instantiates a new CustomHandler object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *CustomHandler) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *CustomHandler) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *CustomHandler) SetId(v string)`

SetId sets Id field to given value.


### GetChannelId

`func (o *CustomHandler) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *CustomHandler) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *CustomHandler) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.


### GetType

`func (o *CustomHandler) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *CustomHandler) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *CustomHandler) SetType(v string)`

SetType sets Type field to given value.


### GetUrl

`func (o *CustomHandler) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *CustomHandler) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *CustomHandler) SetUrl(v string)`

SetUrl sets Url field to given value.


### GetSecret

`func (o *CustomHandler) GetSecret() string`

GetSecret returns the Secret field if non-nil, zero value otherwise.

### GetSecretOk

`func (o *CustomHandler) GetSecretOk() (*string, bool)`

GetSecretOk returns a tuple with the Secret field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSecret

`func (o *CustomHandler) SetSecret(v string)`

SetSecret sets Secret field to given value.

### HasSecret

`func (o *CustomHandler) HasSecret() bool`

HasSecret returns a boolean if a field has been set.

### GetSecretHeader

`func (o *CustomHandler) GetSecretHeader() string`

GetSecretHeader returns the SecretHeader field if non-nil, zero value otherwise.

### GetSecretHeaderOk

`func (o *CustomHandler) GetSecretHeaderOk() (*string, bool)`

GetSecretHeaderOk returns a tuple with the SecretHeader field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSecretHeader

`func (o *CustomHandler) SetSecretHeader(v string)`

SetSecretHeader sets SecretHeader field to given value.

### HasSecretHeader

`func (o *CustomHandler) HasSecretHeader() bool`

HasSecretHeader returns a boolean if a field has been set.

### GetSecretConfigured

`func (o *CustomHandler) GetSecretConfigured() bool`

GetSecretConfigured returns the SecretConfigured field if non-nil, zero value otherwise.

### GetSecretConfiguredOk

`func (o *CustomHandler) GetSecretConfiguredOk() (*bool, bool)`

GetSecretConfiguredOk returns a tuple with the SecretConfigured field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSecretConfigured

`func (o *CustomHandler) SetSecretConfigured(v bool)`

SetSecretConfigured sets SecretConfigured field to given value.


### GetAttributes

`func (o *CustomHandler) GetAttributes() map[string]interface{}`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *CustomHandler) GetAttributesOk() (*map[string]interface{}, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *CustomHandler) SetAttributes(v map[string]interface{})`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *CustomHandler) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetCreatedAt

`func (o *CustomHandler) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *CustomHandler) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *CustomHandler) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetUpdatedAt

`func (o *CustomHandler) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *CustomHandler) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *CustomHandler) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.


### GetOrganizationId

`func (o *CustomHandler) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *CustomHandler) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *CustomHandler) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


