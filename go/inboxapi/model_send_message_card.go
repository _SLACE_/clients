/*
Inbox

Inbox API can be used for low level integration and enables direct communication with you contact base.   In most cases you will use our interaction templates for communication, but our api also allows you to send message directly.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package inboxapi

import (
	"encoding/json"
)

// checks if the SendMessageCard type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &SendMessageCard{}

// SendMessageCard struct for SendMessageCard
type SendMessageCard struct {
	Text string `json:"text"`
	MediaId *string `json:"media_id,omitempty"`
	MediaUrl *string `json:"media_url,omitempty"`
	Buttons []MessageButton `json:"buttons,omitempty"`
}

// NewSendMessageCard instantiates a new SendMessageCard object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewSendMessageCard(text string) *SendMessageCard {
	this := SendMessageCard{}
	this.Text = text
	return &this
}

// NewSendMessageCardWithDefaults instantiates a new SendMessageCard object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewSendMessageCardWithDefaults() *SendMessageCard {
	this := SendMessageCard{}
	return &this
}

// GetText returns the Text field value
func (o *SendMessageCard) GetText() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Text
}

// GetTextOk returns a tuple with the Text field value
// and a boolean to check if the value has been set.
func (o *SendMessageCard) GetTextOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Text, true
}

// SetText sets field value
func (o *SendMessageCard) SetText(v string) {
	o.Text = v
}

// GetMediaId returns the MediaId field value if set, zero value otherwise.
func (o *SendMessageCard) GetMediaId() string {
	if o == nil || IsNil(o.MediaId) {
		var ret string
		return ret
	}
	return *o.MediaId
}

// GetMediaIdOk returns a tuple with the MediaId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageCard) GetMediaIdOk() (*string, bool) {
	if o == nil || IsNil(o.MediaId) {
		return nil, false
	}
	return o.MediaId, true
}

// HasMediaId returns a boolean if a field has been set.
func (o *SendMessageCard) HasMediaId() bool {
	if o != nil && !IsNil(o.MediaId) {
		return true
	}

	return false
}

// SetMediaId gets a reference to the given string and assigns it to the MediaId field.
func (o *SendMessageCard) SetMediaId(v string) {
	o.MediaId = &v
}

// GetMediaUrl returns the MediaUrl field value if set, zero value otherwise.
func (o *SendMessageCard) GetMediaUrl() string {
	if o == nil || IsNil(o.MediaUrl) {
		var ret string
		return ret
	}
	return *o.MediaUrl
}

// GetMediaUrlOk returns a tuple with the MediaUrl field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageCard) GetMediaUrlOk() (*string, bool) {
	if o == nil || IsNil(o.MediaUrl) {
		return nil, false
	}
	return o.MediaUrl, true
}

// HasMediaUrl returns a boolean if a field has been set.
func (o *SendMessageCard) HasMediaUrl() bool {
	if o != nil && !IsNil(o.MediaUrl) {
		return true
	}

	return false
}

// SetMediaUrl gets a reference to the given string and assigns it to the MediaUrl field.
func (o *SendMessageCard) SetMediaUrl(v string) {
	o.MediaUrl = &v
}

// GetButtons returns the Buttons field value if set, zero value otherwise.
func (o *SendMessageCard) GetButtons() []MessageButton {
	if o == nil || IsNil(o.Buttons) {
		var ret []MessageButton
		return ret
	}
	return o.Buttons
}

// GetButtonsOk returns a tuple with the Buttons field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageCard) GetButtonsOk() ([]MessageButton, bool) {
	if o == nil || IsNil(o.Buttons) {
		return nil, false
	}
	return o.Buttons, true
}

// HasButtons returns a boolean if a field has been set.
func (o *SendMessageCard) HasButtons() bool {
	if o != nil && !IsNil(o.Buttons) {
		return true
	}

	return false
}

// SetButtons gets a reference to the given []MessageButton and assigns it to the Buttons field.
func (o *SendMessageCard) SetButtons(v []MessageButton) {
	o.Buttons = v
}

func (o SendMessageCard) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o SendMessageCard) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["text"] = o.Text
	if !IsNil(o.MediaId) {
		toSerialize["media_id"] = o.MediaId
	}
	if !IsNil(o.MediaUrl) {
		toSerialize["media_url"] = o.MediaUrl
	}
	if !IsNil(o.Buttons) {
		toSerialize["buttons"] = o.Buttons
	}
	return toSerialize, nil
}

type NullableSendMessageCard struct {
	value *SendMessageCard
	isSet bool
}

func (v NullableSendMessageCard) Get() *SendMessageCard {
	return v.value
}

func (v *NullableSendMessageCard) Set(val *SendMessageCard) {
	v.value = val
	v.isSet = true
}

func (v NullableSendMessageCard) IsSet() bool {
	return v.isSet
}

func (v *NullableSendMessageCard) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableSendMessageCard(val *SendMessageCard) *NullableSendMessageCard {
	return &NullableSendMessageCard{value: val, isSet: true}
}

func (v NullableSendMessageCard) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableSendMessageCard) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


