/*
Inbox

Inbox API can be used for low level integration and enables direct communication with you contact base.   In most cases you will use our interaction templates for communication, but our api also allows you to send message directly.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package inboxapi

import (
	"encoding/json"
)

// checks if the ExternalSystemLinkDelete type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ExternalSystemLinkDelete{}

// ExternalSystemLinkDelete struct for ExternalSystemLinkDelete
type ExternalSystemLinkDelete struct {
	// User ID or full profile page URL as defined in organization settings
	SystemName string `json:"system_name"`
}

// NewExternalSystemLinkDelete instantiates a new ExternalSystemLinkDelete object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewExternalSystemLinkDelete(systemName string) *ExternalSystemLinkDelete {
	this := ExternalSystemLinkDelete{}
	this.SystemName = systemName
	return &this
}

// NewExternalSystemLinkDeleteWithDefaults instantiates a new ExternalSystemLinkDelete object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewExternalSystemLinkDeleteWithDefaults() *ExternalSystemLinkDelete {
	this := ExternalSystemLinkDelete{}
	return &this
}

// GetSystemName returns the SystemName field value
func (o *ExternalSystemLinkDelete) GetSystemName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.SystemName
}

// GetSystemNameOk returns a tuple with the SystemName field value
// and a boolean to check if the value has been set.
func (o *ExternalSystemLinkDelete) GetSystemNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.SystemName, true
}

// SetSystemName sets field value
func (o *ExternalSystemLinkDelete) SetSystemName(v string) {
	o.SystemName = v
}

func (o ExternalSystemLinkDelete) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ExternalSystemLinkDelete) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["system_name"] = o.SystemName
	return toSerialize, nil
}

type NullableExternalSystemLinkDelete struct {
	value *ExternalSystemLinkDelete
	isSet bool
}

func (v NullableExternalSystemLinkDelete) Get() *ExternalSystemLinkDelete {
	return v.value
}

func (v *NullableExternalSystemLinkDelete) Set(val *ExternalSystemLinkDelete) {
	v.value = val
	v.isSet = true
}

func (v NullableExternalSystemLinkDelete) IsSet() bool {
	return v.isSet
}

func (v *NullableExternalSystemLinkDelete) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableExternalSystemLinkDelete(val *ExternalSystemLinkDelete) *NullableExternalSystemLinkDelete {
	return &NullableExternalSystemLinkDelete{value: val, isSet: true}
}

func (v NullableExternalSystemLinkDelete) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableExternalSystemLinkDelete) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


