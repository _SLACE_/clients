/*
Inbox

Inbox API can be used for low level integration and enables direct communication with you contact base.   In most cases you will use our interaction templates for communication, but our api also allows you to send message directly.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package inboxapi

import (
	"encoding/json"
)

// checks if the InboxSettingsMessengerLimitsInner type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &InboxSettingsMessengerLimitsInner{}

// InboxSettingsMessengerLimitsInner struct for InboxSettingsMessengerLimitsInner
type InboxSettingsMessengerLimitsInner struct {
	MaxTextLength int32 `json:"max_text_length"`
	SessionTimeoutSeconds string `json:"session_timeout_seconds"`
	Type Messenger `json:"type"`
	MessageTypes []InboxSettingsMessengerLimitsInnerMessageTypesInner `json:"message_types,omitempty"`
}

// NewInboxSettingsMessengerLimitsInner instantiates a new InboxSettingsMessengerLimitsInner object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewInboxSettingsMessengerLimitsInner(maxTextLength int32, sessionTimeoutSeconds string, type_ Messenger) *InboxSettingsMessengerLimitsInner {
	this := InboxSettingsMessengerLimitsInner{}
	this.MaxTextLength = maxTextLength
	this.SessionTimeoutSeconds = sessionTimeoutSeconds
	this.Type = type_
	return &this
}

// NewInboxSettingsMessengerLimitsInnerWithDefaults instantiates a new InboxSettingsMessengerLimitsInner object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewInboxSettingsMessengerLimitsInnerWithDefaults() *InboxSettingsMessengerLimitsInner {
	this := InboxSettingsMessengerLimitsInner{}
	return &this
}

// GetMaxTextLength returns the MaxTextLength field value
func (o *InboxSettingsMessengerLimitsInner) GetMaxTextLength() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.MaxTextLength
}

// GetMaxTextLengthOk returns a tuple with the MaxTextLength field value
// and a boolean to check if the value has been set.
func (o *InboxSettingsMessengerLimitsInner) GetMaxTextLengthOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.MaxTextLength, true
}

// SetMaxTextLength sets field value
func (o *InboxSettingsMessengerLimitsInner) SetMaxTextLength(v int32) {
	o.MaxTextLength = v
}

// GetSessionTimeoutSeconds returns the SessionTimeoutSeconds field value
func (o *InboxSettingsMessengerLimitsInner) GetSessionTimeoutSeconds() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.SessionTimeoutSeconds
}

// GetSessionTimeoutSecondsOk returns a tuple with the SessionTimeoutSeconds field value
// and a boolean to check if the value has been set.
func (o *InboxSettingsMessengerLimitsInner) GetSessionTimeoutSecondsOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.SessionTimeoutSeconds, true
}

// SetSessionTimeoutSeconds sets field value
func (o *InboxSettingsMessengerLimitsInner) SetSessionTimeoutSeconds(v string) {
	o.SessionTimeoutSeconds = v
}

// GetType returns the Type field value
func (o *InboxSettingsMessengerLimitsInner) GetType() Messenger {
	if o == nil {
		var ret Messenger
		return ret
	}

	return o.Type
}

// GetTypeOk returns a tuple with the Type field value
// and a boolean to check if the value has been set.
func (o *InboxSettingsMessengerLimitsInner) GetTypeOk() (*Messenger, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Type, true
}

// SetType sets field value
func (o *InboxSettingsMessengerLimitsInner) SetType(v Messenger) {
	o.Type = v
}

// GetMessageTypes returns the MessageTypes field value if set, zero value otherwise.
func (o *InboxSettingsMessengerLimitsInner) GetMessageTypes() []InboxSettingsMessengerLimitsInnerMessageTypesInner {
	if o == nil || IsNil(o.MessageTypes) {
		var ret []InboxSettingsMessengerLimitsInnerMessageTypesInner
		return ret
	}
	return o.MessageTypes
}

// GetMessageTypesOk returns a tuple with the MessageTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *InboxSettingsMessengerLimitsInner) GetMessageTypesOk() ([]InboxSettingsMessengerLimitsInnerMessageTypesInner, bool) {
	if o == nil || IsNil(o.MessageTypes) {
		return nil, false
	}
	return o.MessageTypes, true
}

// HasMessageTypes returns a boolean if a field has been set.
func (o *InboxSettingsMessengerLimitsInner) HasMessageTypes() bool {
	if o != nil && !IsNil(o.MessageTypes) {
		return true
	}

	return false
}

// SetMessageTypes gets a reference to the given []InboxSettingsMessengerLimitsInnerMessageTypesInner and assigns it to the MessageTypes field.
func (o *InboxSettingsMessengerLimitsInner) SetMessageTypes(v []InboxSettingsMessengerLimitsInnerMessageTypesInner) {
	o.MessageTypes = v
}

func (o InboxSettingsMessengerLimitsInner) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o InboxSettingsMessengerLimitsInner) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["max_text_length"] = o.MaxTextLength
	toSerialize["session_timeout_seconds"] = o.SessionTimeoutSeconds
	toSerialize["type"] = o.Type
	if !IsNil(o.MessageTypes) {
		toSerialize["message_types"] = o.MessageTypes
	}
	return toSerialize, nil
}

type NullableInboxSettingsMessengerLimitsInner struct {
	value *InboxSettingsMessengerLimitsInner
	isSet bool
}

func (v NullableInboxSettingsMessengerLimitsInner) Get() *InboxSettingsMessengerLimitsInner {
	return v.value
}

func (v *NullableInboxSettingsMessengerLimitsInner) Set(val *InboxSettingsMessengerLimitsInner) {
	v.value = val
	v.isSet = true
}

func (v NullableInboxSettingsMessengerLimitsInner) IsSet() bool {
	return v.isSet
}

func (v *NullableInboxSettingsMessengerLimitsInner) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableInboxSettingsMessengerLimitsInner(val *InboxSettingsMessengerLimitsInner) *NullableInboxSettingsMessengerLimitsInner {
	return &NullableInboxSettingsMessengerLimitsInner{value: val, isSet: true}
}

func (v NullableInboxSettingsMessengerLimitsInner) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableInboxSettingsMessengerLimitsInner) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


