# Go API client for inboxapi

Inbox API can be used for low level integration and enables direct communication with you contact base. 

In most cases you will use our interaction templates for communication, but our api also allows you to send message directly.

## Overview
This API client was generated by the [OpenAPI Generator](https://openapi-generator.tech) project.  By using the [OpenAPI-spec](https://www.openapis.org/) from a remote server, you can easily generate an API client.

- API version: 1.0
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.GoClientCodegen

## Installation

Install the following dependencies:

```shell
go get github.com/stretchr/testify/assert
go get golang.org/x/net/context
```

Put the package under your project folder and add the following in import:

```golang
import inboxapi "bitbucket.org/_SLACE_/clients/go/inboxapi"
```

To use a proxy, set the environment variable `HTTP_PROXY`:

```golang
os.Setenv("HTTP_PROXY", "http://proxy_name:proxy_port")
```

## Configuration of Server URL

Default configuration comes with `Servers` field that contains server objects as defined in the OpenAPI specification.

### Select Server Configuration

For using other server than the one defined on index 0 set context value `sw.ContextServerIndex` of type `int`.

```golang
ctx := context.WithValue(context.Background(), inboxapi.ContextServerIndex, 1)
```

### Templated Server URL

Templated server URL is formatted using default variables from configuration or from context value `sw.ContextServerVariables` of type `map[string]string`.

```golang
ctx := context.WithValue(context.Background(), inboxapi.ContextServerVariables, map[string]string{
	"basePath": "v2",
})
```

Note, enum values are always validated and all unused variables are silently ignored.

### URLs Configuration per Operation

Each operation can use different server URL defined using `OperationServers` map in the `Configuration`.
An operation is uniquely identified by `"{classname}Service.{nickname}"` string.
Similar rules for overriding default operation server index and variables applies by using `sw.ContextOperationServerIndices` and `sw.ContextOperationServerVariables` context maps.

```golang
ctx := context.WithValue(context.Background(), inboxapi.ContextOperationServerIndices, map[string]int{
	"{classname}Service.{nickname}": 2,
})
ctx = context.WithValue(context.Background(), inboxapi.ContextOperationServerVariables, map[string]map[string]string{
	"{classname}Service.{nickname}": {
		"port": "8443",
	},
})
```

## Documentation for API Endpoints

All URIs are relative to *https://api.slace.com/inbox*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ContactsAPI* | [**AddConsent**](docs/ContactsAPI.md#addconsent) | **Put** /organizations/{organization_id}/contacts/{contact_id}/consents/add | Add consent for contact
*ContactsAPI* | [**AddContactCoupon**](docs/ContactsAPI.md#addcontactcoupon) | **Post** /organizations/{organization_id}/contacts/{contact_id}/coupons | Add contact coupon
*ContactsAPI* | [**AddContactSegments**](docs/ContactsAPI.md#addcontactsegments) | **Post** /organizations/{organization_id}/contacts/{contact_id}/segments | Add segments
*ContactsAPI* | [**AddExternalId**](docs/ContactsAPI.md#addexternalid) | **Post** /organizations/{organization_id}/contacts/{contact_id}/external | Add external system ID
*ContactsAPI* | [**AddLanguage**](docs/ContactsAPI.md#addlanguage) | **Post** /organizations/{organization_id}/contacts/{contact_id}/languages | Add language
*ContactsAPI* | [**AddSegment**](docs/ContactsAPI.md#addsegment) | **Post** /organizations/{organization_id}/contacts/{contact_id}/segment | Add segment
*ContactsAPI* | [**AddSegmentGroup**](docs/ContactsAPI.md#addsegmentgroup) | **Post** /organizations/{organization_id}/contacts-segment-group | Add segment group
*ContactsAPI* | [**AttachContactComChannel**](docs/ContactsAPI.md#attachcontactcomchannel) | **Post** /organizations/{organization_id}/contacts/{contact_id}/com-channels/{com_chan_id} | Attach communication channel
*ContactsAPI* | [**AttachPlatformContactComChannel**](docs/ContactsAPI.md#attachplatformcontactcomchannel) | **Post** /channels/{channel_id}/contacts/{contact_id}/platform-com-channels/{platform_com_chan_id} | Attach platform communication channel
*ContactsAPI* | [**CreateContact**](docs/ContactsAPI.md#createcontact) | **Post** /organizations/{organization_id}/contacts | Create new contact
*ContactsAPI* | [**CreateNewActivity**](docs/ContactsAPI.md#createnewactivity) | **Post** /organizations/{organization_id}/contacts/{contact_id}/activities | Create new activity
*ContactsAPI* | [**DeleteContact**](docs/ContactsAPI.md#deletecontact) | **Delete** /organizations/{organization_id}/contacts/{contact_id} | Delete contact
*ContactsAPI* | [**DeleteContactsGroup**](docs/ContactsAPI.md#deletecontactsgroup) | **Delete** /organizations/{organization_id}/contacts-group | Delete contacts by ID
*ContactsAPI* | [**DeleteExternalId**](docs/ContactsAPI.md#deleteexternalid) | **Delete** /organizations/{organization_id}/contacts/{contact_id}/external | Delete external ID
*ContactsAPI* | [**DeleteLanguage**](docs/ContactsAPI.md#deletelanguage) | **Delete** /organizations/{organization_id}/contacts/{contact_id}/languages/{source_id} | Delete language by source
*ContactsAPI* | [**DeleteOrganizationsOrganizationIdContactsContactIdSegment**](docs/ContactsAPI.md#deleteorganizationsorganizationidcontactscontactidsegment) | **Delete** /organizations/{organization_id}/contacts/{contact_id}/segment | Delete segment
*ContactsAPI* | [**DeletePrimaryAvatar**](docs/ContactsAPI.md#deleteprimaryavatar) | **Delete** /organizations/{organization_id}/contacts/{contact_id}/avatars/primary | Delete primary avatar
*ContactsAPI* | [**DeleteSegmentGroup**](docs/ContactsAPI.md#deletesegmentgroup) | **Delete** /organizations/{organization_id}/contacts-segment-group | Delete segment group
*ContactsAPI* | [**DetachContactComChannel**](docs/ContactsAPI.md#detachcontactcomchannel) | **Delete** /organizations/{organization_id}/contacts/{contact_id}/com-channels/{com_chan_id} | Detach communication channel
*ContactsAPI* | [**DetachPlatformContactComChannel**](docs/ContactsAPI.md#detachplatformcontactcomchannel) | **Delete** /channels/{channel_id}/contacts/{contact_id}/platform-com-channels/{platform_com_chan_id} | Detach platform communication channel
*ContactsAPI* | [**ExportContacts**](docs/ContactsAPI.md#exportcontacts) | **Get** /organizations/{organization_id}/contacts-export | Export contacts
*ContactsAPI* | [**ExportContactsAttributes**](docs/ContactsAPI.md#exportcontactsattributes) | **Get** /organizations/{organization_id}/contacts-strict-attributes-export | Export contacts attributes
*ContactsAPI* | [**GetActivitiesReport**](docs/ContactsAPI.md#getactivitiesreport) | **Get** /organizations/{organization_id}/activities-report | Get activities report
*ContactsAPI* | [**GetChannelActivitiesCount**](docs/ContactsAPI.md#getchannelactivitiescount) | **Get** /organizations/{organization_id}/channels/{channelId}/activities-count | Get channel activities count
*ContactsAPI* | [**GetChannelActivitiesGrowth**](docs/ContactsAPI.md#getchannelactivitiesgrowth) | **Get** /organizations/{organization_id}/channels/{channelId}/activities-growth | Get channel activities growth
*ContactsAPI* | [**GetChannelActivitiesQualMetrics**](docs/ContactsAPI.md#getchannelactivitiesqualmetrics) | **Get** /organizations/{organization_id}/channels/{channelId}/activities-qual | Get channel activities qual metrics
*ContactsAPI* | [**GetContact**](docs/ContactsAPI.md#getcontact) | **Get** /organizations/{organization_id}/contacts/{contact_id} | Get contact
*ContactsAPI* | [**GetContactActivity**](docs/ContactsAPI.md#getcontactactivity) | **Get** /organizations/{organization_id}/contacts/{contact_id}/activities/{activity_id} | Get single contact activity
*ContactsAPI* | [**GetContactGLobal**](docs/ContactsAPI.md#getcontactglobal) | **Get** /contacts/{contact_id} | Get contact (Global)
*ContactsAPI* | [**GetContactsGrowth**](docs/ContactsAPI.md#getcontactsgrowth) | **Get** /organizations/{organization_id}/contacts-growth | Get contact growth amount
*ContactsAPI* | [**GetContactsQual**](docs/ContactsAPI.md#getcontactsqual) | **Get** /organizations/{organization_id}/contacts-qual | Get contact qual metrics
*ContactsAPI* | [**GetContactsReport**](docs/ContactsAPI.md#getcontactsreport) | **Get** /organizations/{organization_id}/contacts-report | Get contacts report
*ContactsAPI* | [**GetCoupons**](docs/ContactsAPI.md#getcoupons) | **Get** /organizations/{organization_id}/contacts-coupons/{code} | Get coupons by ID
*ContactsAPI* | [**GetLanguages**](docs/ContactsAPI.md#getlanguages) | **Get** /organizations/{organization_id}/contacts/{contact_id}/languages | Get languages
*ContactsAPI* | [**GetOrganizationActivitiesCount**](docs/ContactsAPI.md#getorganizationactivitiescount) | **Get** /organizations/{organization_id}/activities-count | Get organization activities count
*ContactsAPI* | [**GetOrganizationActivitiesGrowth**](docs/ContactsAPI.md#getorganizationactivitiesgrowth) | **Get** /organizations/{organization_id}/activities-growth | Get organization activities growth
*ContactsAPI* | [**GetOrganizationActivitiesQualMetrics**](docs/ContactsAPI.md#getorganizationactivitiesqualmetrics) | **Get** /organizations/{organization_id}/activities-qual | Get organization activities qual metrics
*ContactsAPI* | [**GetOrganizationsOrganizationIdContactsKpi**](docs/ContactsAPI.md#getorganizationsorganizationidcontactskpi) | **Get** /organizations/{organization_id}/contacts-kpi | List KPIs
*ContactsAPI* | [**GetOrganizationsOrganizationIdContactsLabels**](docs/ContactsAPI.md#getorganizationsorganizationidcontactslabels) | **Get** /organizations/{organization_id}/contacts-labels | List organization labels
*ContactsAPI* | [**ImportContacts**](docs/ContactsAPI.md#importcontacts) | **Post** /organizations/{organization_id}/contacts-import | Import contacts
*ContactsAPI* | [**ImportContactsAttributes**](docs/ContactsAPI.md#importcontactsattributes) | **Post** /organizations/{organization_id}/contacts-strict-attributes-import | Import contacts attributes
*ContactsAPI* | [**ListChannelActivities**](docs/ContactsAPI.md#listchannelactivities) | **Get** /organizations/{organization_id}/channels/{channelId}/activities | List channel activities
*ContactsAPI* | [**ListContactActivities**](docs/ContactsAPI.md#listcontactactivities) | **Get** /organizations/{organization_id}/contacts/{contact_id}/activities | List contact activities
*ContactsAPI* | [**ListContactConversations**](docs/ContactsAPI.md#listcontactconversations) | **Get** /organizations/{organization_id}/contacts/{contact_id}/conversations | List contact conversations
*ContactsAPI* | [**ListContactCoupons**](docs/ContactsAPI.md#listcontactcoupons) | **Get** /organizations/{organization_id}/contacts/{contact_id}/coupons | List contact coupons
*ContactsAPI* | [**ListContactFiles**](docs/ContactsAPI.md#listcontactfiles) | **Get** /organizations/{organization_id}/contacts/{contact_id}/files | List contact files
*ContactsAPI* | [**ListContactInteractions**](docs/ContactsAPI.md#listcontactinteractions) | **Get** /organizations/{organization_id}/contacts/{contact_id}/interactions | List contact interactions
*ContactsAPI* | [**ListContactLabels**](docs/ContactsAPI.md#listcontactlabels) | **Get** /organizations/{organization_id}/contacts/{contact_id}/labels | List contact labels
*ContactsAPI* | [**ListContactSegments**](docs/ContactsAPI.md#listcontactsegments) | **Get** /organizations/{organization_id}/contacts/{contact_id}/segments | List segments
*ContactsAPI* | [**ListContacts**](docs/ContactsAPI.md#listcontacts) | **Get** /organizations/{organization_id}/contacts | List contacts
*ContactsAPI* | [**ListContactsGroup**](docs/ContactsAPI.md#listcontactsgroup) | **Get** /organizations/{organization_id}/contacts-group | List contacts by IDs
*ContactsAPI* | [**ListLanguages**](docs/ContactsAPI.md#listlanguages) | **Get** /organizations/{organization_id}/contacts-languages | List languages
*ContactsAPI* | [**ListOrganizationActivities**](docs/ContactsAPI.md#listorganizationactivities) | **Get** /organizations/{organization_id}/activities | List organization activities
*ContactsAPI* | [**ListSegments**](docs/ContactsAPI.md#listsegments) | **Get** /organizations/{organization_id}/contacts-segments | List organization segments
*ContactsAPI* | [**PatchActivity**](docs/ContactsAPI.md#patchactivity) | **Patch** /organizations/{organization_id}/contacts/{contact_id}/activities/{activity_id} | Patch activity
*ContactsAPI* | [**PatchContact**](docs/ContactsAPI.md#patchcontact) | **Patch** /organizations/{organization_id}/contacts/{contact_id} | Patch contact
*ContactsAPI* | [**PatchCoupon**](docs/ContactsAPI.md#patchcoupon) | **Patch** /organizations/{organization_id}/contacts/{contact_id}/coupons/{code} | Patch coupon
*ContactsAPI* | [**RevokeConsent**](docs/ContactsAPI.md#revokeconsent) | **Put** /organizations/{organization_id}/contacts/{contact_id}/consents/revoke | Revoke consent from contact
*ContactsAPI* | [**UpdateContact**](docs/ContactsAPI.md#updatecontact) | **Put** /organizations/{organization_id}/contacts/{contact_id} | Update contact
*ContactsAPI* | [**UpdateCoupon**](docs/ContactsAPI.md#updatecoupon) | **Put** /organizations/{organization_id}/contacts/{contact_id}/coupons/{code} | Update coupon
*ContactsAPI* | [**UploadAvatar**](docs/ContactsAPI.md#uploadavatar) | **Post** /organizations/{organization_id}/contacts/{contact_id}/avatars | Upload avatar
*ConversationsAPI* | [**AddConversationMessageLabels**](docs/ConversationsAPI.md#addconversationmessagelabels) | **Put** /channels/{channel_id}/conversations/{cid}/messages/{msg_id}/labels | Add message labels
*ConversationsAPI* | [**AddLabels**](docs/ConversationsAPI.md#addlabels) | **Put** /channels/{channel_id}/conversations/{cid}/labels | Add labels
*ConversationsAPI* | [**AgentTakeover**](docs/ConversationsAPI.md#agenttakeover) | **Post** /channels/{channel_id}/conversations/{cid}/agent-takeover | Agent takover
*ConversationsAPI* | [**AgentTakeoverRelease**](docs/ConversationsAPI.md#agenttakeoverrelease) | **Delete** /channels/{channel_id}/conversations/{cid}/agent-takeover | Agent takeover release
*ConversationsAPI* | [**AttachTransactionToConversation**](docs/ConversationsAPI.md#attachtransactiontoconversation) | **Put** /channels/{channel_id}/conversations/{conversation_id}/transaction/{transaction_id} | Attach transaction to conversation
*ConversationsAPI* | [**ConversationBlocking**](docs/ConversationsAPI.md#conversationblocking) | **Post** /channels/{channel_id}/conversations/{conversation_id}/blocking | Block conversation
*ConversationsAPI* | [**ConversationOptOut**](docs/ConversationsAPI.md#conversationoptout) | **Post** /channels/{channel_id}/conversations/{cid}/opt-out | Conversation opt out
*ConversationsAPI* | [**CreateAgentRequest**](docs/ConversationsAPI.md#createagentrequest) | **Post** /channels/{channel_id}/conversations/{conversation_id}/agent-request | Create Agent Request
*ConversationsAPI* | [**DeleteConversation**](docs/ConversationsAPI.md#deleteconversation) | **Delete** /channels/{channel_id}/conversations/{cid} | Delete conversation
*ConversationsAPI* | [**DeleteConversationMessage**](docs/ConversationsAPI.md#deleteconversationmessage) | **Delete** /channels/{channel_id}/conversations/{cid}/messages/{msg_id} | Delete message
*ConversationsAPI* | [**DeleteConversationMessageLabel**](docs/ConversationsAPI.md#deleteconversationmessagelabel) | **Delete** /channels/{channel_id}/conversations/{cid}/messages/{msg_id}/labels/{label} | Delete message label
*ConversationsAPI* | [**DeleteLabel**](docs/ConversationsAPI.md#deletelabel) | **Delete** /channels/{channel_id}/conversations/{cid}/labels/{label} | Delete label
*ConversationsAPI* | [**DeleteLabels**](docs/ConversationsAPI.md#deletelabels) | **Delete** /channels/{channel_id}/conversations/{cid}/labels | Delete labels
*ConversationsAPI* | [**DetachTransactionFromConversation**](docs/ConversationsAPI.md#detachtransactionfromconversation) | **Delete** /channels/{channel_id}/conversations/{conversation_id}/transaction/{transaction_id} | Detach transaction from conversation
*ConversationsAPI* | [**FindConversationsSegments**](docs/ConversationsAPI.md#findconversationssegments) | **Post** /channels/{channel_id}/segments | Find conversation segments
*ConversationsAPI* | [**GetConversationById**](docs/ConversationsAPI.md#getconversationbyid) | **Get** /channels/{channel_id}/conversations/{cid} | Get conversation by ID
*ConversationsAPI* | [**GetConversationMedia**](docs/ConversationsAPI.md#getconversationmedia) | **Get** /channels/{channel_id}/conversations/{conversation_id}/media | Get media
*ConversationsAPI* | [**GetConversationMessage**](docs/ConversationsAPI.md#getconversationmessage) | **Get** /channels/{channel_id}/conversations/{cid}/messages/{msg_id} | Get message
*ConversationsAPI* | [**GetConversationMessages**](docs/ConversationsAPI.md#getconversationmessages) | **Get** /channels/{channel_id}/conversations/{cid}/messages | Get messages
*ConversationsAPI* | [**GetConversations**](docs/ConversationsAPI.md#getconversations) | **Get** /channels/{channel_id}/conversations | Get conversations
*ConversationsAPI* | [**MarkAsRead**](docs/ConversationsAPI.md#markasread) | **Post** /channels/{channel_id}/conversations/{conversation_id}/read | Mark conversation as read
*ConversationsAPI* | [**MarkAsUnread**](docs/ConversationsAPI.md#markasunread) | **Delete** /channels/{channel_id}/conversations/{conversation_id}/read | Mark conversation as unread
*ConversationsAPI* | [**MarkConversationAsRead**](docs/ConversationsAPI.md#markconversationasread) | **Post** /channels/{channel_id}/conversations/{cid}/read | Mark conversation as read
*ConversationsAPI* | [**ResolveAgentRequest**](docs/ConversationsAPI.md#resolveagentrequest) | **Delete** /channels/{channel_id}/conversations/{conversation_id}/agent-request | Resolve Agent Request
*ConversationsAPI* | [**TranslateConversationParams**](docs/ConversationsAPI.md#translateconversationparams) | **Post** /channels/{channel_id}/conversations/{cid}/translate-params | Translate conversation params
*MediaAPI* | [**GetMediaFile**](docs/MediaAPI.md#getmediafile) | **Get** /channels/{channel_id}/gateways/{gateway_id}/media/{media_id} | Get media file
*MediaAPI* | [**UploadMediaFile**](docs/MediaAPI.md#uploadmediafile) | **Post** /channels/{channel_id}/gateways/{gateway_id}/media | Upload media file
*MessagingAPI* | [**GetChannelMessages**](docs/MessagingAPI.md#getchannelmessages) | **Get** /channels/{channel_id}/messages | Get Channel Messages
*MessagingAPI* | [**SendMessage**](docs/MessagingAPI.md#sendmessage) | **Post** /channels/{channel_id}/messages | Send message
*MessagingAPI* | [**SendTemplateMessage**](docs/MessagingAPI.md#sendtemplatemessage) | **Post** /channels/{channel_id}/messages/template | Send template message
*SettingsAPI* | [**GetSettings**](docs/SettingsAPI.md#getsettings) | **Get** /settings | Get Settings


## Documentation For Models

 - [ActivitiesGrowthCount](docs/ActivitiesGrowthCount.md)
 - [ActivitiesQualMetrics](docs/ActivitiesQualMetrics.md)
 - [Activity](docs/Activity.md)
 - [ActivityFile](docs/ActivityFile.md)
 - [ActivityPatchable](docs/ActivityPatchable.md)
 - [ActivityPatchableFile](docs/ActivityPatchableFile.md)
 - [AddConsentRequest](docs/AddConsentRequest.md)
 - [AddSegmentGroupRequest](docs/AddSegmentGroupRequest.md)
 - [AddSegmentGroupRequestSegment](docs/AddSegmentGroupRequestSegment.md)
 - [AgentRequestData](docs/AgentRequestData.md)
 - [ComChannelPermission](docs/ComChannelPermission.md)
 - [CommunicationChannel](docs/CommunicationChannel.md)
 - [Contact](docs/Contact.md)
 - [ContactAddress](docs/ContactAddress.md)
 - [ContactAllOfGivenConsents](docs/ContactAllOfGivenConsents.md)
 - [ContactAvatars](docs/ContactAvatars.md)
 - [ContactCreatable](docs/ContactCreatable.md)
 - [ContactDevices](docs/ContactDevices.md)
 - [ContactERConfidence](docs/ContactERConfidence.md)
 - [ContactEditable](docs/ContactEditable.md)
 - [ContactEditableLanguage](docs/ContactEditableLanguage.md)
 - [ContactExcludeRelation](docs/ContactExcludeRelation.md)
 - [ContactIncludeRelation](docs/ContactIncludeRelation.md)
 - [ContactKPI](docs/ContactKPI.md)
 - [ContactLanguages](docs/ContactLanguages.md)
 - [ContactPatchable](docs/ContactPatchable.md)
 - [ContactStrictAttribute](docs/ContactStrictAttribute.md)
 - [ContactStrictAttributeValue](docs/ContactStrictAttributeValue.md)
 - [ContactsList](docs/ContactsList.md)
 - [ContactsQualMetrics](docs/ContactsQualMetrics.md)
 - [Conversation](docs/Conversation.md)
 - [ConversationBlockingData](docs/ConversationBlockingData.md)
 - [ConversationCustomData](docs/ConversationCustomData.md)
 - [ConversationMessage](docs/ConversationMessage.md)
 - [ConversationMessageData](docs/ConversationMessageData.md)
 - [ConversationMessageDataError](docs/ConversationMessageDataError.md)
 - [ConversationMessageDataMediaInner](docs/ConversationMessageDataMediaInner.md)
 - [ConversationMessageDataTemplate](docs/ConversationMessageDataTemplate.md)
 - [ConversationMessagesList](docs/ConversationMessagesList.md)
 - [ConversationParamsTranslateRequest](docs/ConversationParamsTranslateRequest.md)
 - [ConversationsList](docs/ConversationsList.md)
 - [Coupon](docs/Coupon.md)
 - [DeleteContactsGroupRequest](docs/DeleteContactsGroupRequest.md)
 - [DeleteOrganizationsOrganizationIdContactsContactIdSegmentRequest](docs/DeleteOrganizationsOrganizationIdContactsContactIdSegmentRequest.md)
 - [DeleteSegmentGroupRequestInner](docs/DeleteSegmentGroupRequestInner.md)
 - [ExternalSystemLink](docs/ExternalSystemLink.md)
 - [ExternalSystemLinkDelete](docs/ExternalSystemLinkDelete.md)
 - [File](docs/File.md)
 - [FindConversationsSegments200ResponseInner](docs/FindConversationsSegments200ResponseInner.md)
 - [FindConversationsSegmentsRequest](docs/FindConversationsSegmentsRequest.md)
 - [GenericResponse](docs/GenericResponse.md)
 - [GenericResponseErrors](docs/GenericResponseErrors.md)
 - [GetActivitiesReport200ResponseInner](docs/GetActivitiesReport200ResponseInner.md)
 - [GetCoupons200Response](docs/GetCoupons200Response.md)
 - [GrowthCount](docs/GrowthCount.md)
 - [InboxSettings](docs/InboxSettings.md)
 - [InboxSettingsMessengerLimitsInner](docs/InboxSettingsMessengerLimitsInner.md)
 - [InboxSettingsMessengerLimitsInnerMessageTypesInner](docs/InboxSettingsMessengerLimitsInnerMessageTypesInner.md)
 - [InboxSettingsWebPush](docs/InboxSettingsWebPush.md)
 - [InfoMessageData](docs/InfoMessageData.md)
 - [InfoMessagePayload](docs/InfoMessagePayload.md)
 - [InternalTemplate](docs/InternalTemplate.md)
 - [LabelsData](docs/LabelsData.md)
 - [List](docs/List.md)
 - [ListContactActivities200Response](docs/ListContactActivities200Response.md)
 - [ListContactConversations200Response](docs/ListContactConversations200Response.md)
 - [ListContactCoupons200Response](docs/ListContactCoupons200Response.md)
 - [ListContactInteractions200Response](docs/ListContactInteractions200Response.md)
 - [ListOrganizationActivities200Response](docs/ListOrganizationActivities200Response.md)
 - [Location](docs/Location.md)
 - [LocationPersonalized](docs/LocationPersonalized.md)
 - [MediaFile](docs/MediaFile.md)
 - [MessageButton](docs/MessageButton.md)
 - [MessageButtonType](docs/MessageButtonType.md)
 - [MessageCard](docs/MessageCard.md)
 - [MessageContact](docs/MessageContact.md)
 - [MessageContactAddress](docs/MessageContactAddress.md)
 - [MessageContactEmail](docs/MessageContactEmail.md)
 - [MessageContactIM](docs/MessageContactIM.md)
 - [MessageContactName](docs/MessageContactName.md)
 - [MessageContactOrg](docs/MessageContactOrg.md)
 - [MessageContactPhone](docs/MessageContactPhone.md)
 - [MessageContactPhoto](docs/MessageContactPhoto.md)
 - [MessageContactUrl](docs/MessageContactUrl.md)
 - [MessageFooter](docs/MessageFooter.md)
 - [MessageHeader](docs/MessageHeader.md)
 - [MessageHeaderType](docs/MessageHeaderType.md)
 - [MessageItem](docs/MessageItem.md)
 - [MessageItemType](docs/MessageItemType.md)
 - [MessageOrderDetails](docs/MessageOrderDetails.md)
 - [MessageOrderDetailsDiscount](docs/MessageOrderDetailsDiscount.md)
 - [MessageOrderDetailsExpiration](docs/MessageOrderDetailsExpiration.md)
 - [MessageOrderDetailsItemsInner](docs/MessageOrderDetailsItemsInner.md)
 - [MessageOrderStatus](docs/MessageOrderStatus.md)
 - [MessageReaction](docs/MessageReaction.md)
 - [MessageStatus](docs/MessageStatus.md)
 - [MessageType](docs/MessageType.md)
 - [Messenger](docs/Messenger.md)
 - [MessengerTag](docs/MessengerTag.md)
 - [PatchableCoupon](docs/PatchableCoupon.md)
 - [PlatformCommunicationChannel](docs/PlatformCommunicationChannel.md)
 - [ReceiverType](docs/ReceiverType.md)
 - [RevokeConsentRequest](docs/RevokeConsentRequest.md)
 - [Segment](docs/Segment.md)
 - [SegmentsEditList](docs/SegmentsEditList.md)
 - [SendMessageCard](docs/SendMessageCard.md)
 - [SendMessageContext](docs/SendMessageContext.md)
 - [SendMessageRequest](docs/SendMessageRequest.md)
 - [SendMessageType](docs/SendMessageType.md)
 - [SendTemplateRequest](docs/SendTemplateRequest.md)
 - [TemplateType](docs/TemplateType.md)
 - [UploadMediaFileRequest](docs/UploadMediaFileRequest.md)
 - [WhatsAppTemplate](docs/WhatsAppTemplate.md)
 - [WhatsAppTemplateButton](docs/WhatsAppTemplateButton.md)
 - [WhatsAppTemplateButtonType](docs/WhatsAppTemplateButtonType.md)
 - [WhatsAppTemplateCard](docs/WhatsAppTemplateCard.md)
 - [WhatsAppTemplateCategory](docs/WhatsAppTemplateCategory.md)
 - [WhatsAppTemplateComponent](docs/WhatsAppTemplateComponent.md)
 - [WhatsAppTemplateComponentExample](docs/WhatsAppTemplateComponentExample.md)
 - [WhatsAppTemplateComponentType](docs/WhatsAppTemplateComponentType.md)
 - [WhatsAppTemplateHeaderFormat](docs/WhatsAppTemplateHeaderFormat.md)
 - [WhatsAppTemplateMessageCard](docs/WhatsAppTemplateMessageCard.md)
 - [WhatsAppTemplateMessageComponent](docs/WhatsAppTemplateMessageComponent.md)
 - [WhatsAppTemplateMessageComponentParametersInner](docs/WhatsAppTemplateMessageComponentParametersInner.md)
 - [WhatsAppTemplateMessageComponentParametersInnerCurrency](docs/WhatsAppTemplateMessageComponentParametersInnerCurrency.md)
 - [WhatsAppTemplateMessageComponentParametersInnerDateTime](docs/WhatsAppTemplateMessageComponentParametersInnerDateTime.md)
 - [WhatsAppTemplateMessageMedia](docs/WhatsAppTemplateMessageMedia.md)


## Documentation For Authorization


Authentication schemes defined for the API:
### Authorization

- **Type**: HTTP Bearer token authentication

Example

```golang
auth := context.WithValue(context.Background(), sw.ContextAccessToken, "BEARER_TOKEN_STRING")
r, err := client.Service.Operation(auth, args)
```


## Documentation for Utility Methods

Due to the fact that model structure members are all pointers, this package contains
a number of utility functions to easily obtain pointers to values of basic types.
Each of these functions takes a value of the given basic type and returns a pointer to it:

* `PtrBool`
* `PtrInt`
* `PtrInt32`
* `PtrInt64`
* `PtrFloat`
* `PtrFloat32`
* `PtrFloat64`
* `PtrString`
* `PtrTime`

## Author



