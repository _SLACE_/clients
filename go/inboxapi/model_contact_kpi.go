/*
Inbox

Inbox API can be used for low level integration and enables direct communication with you contact base.   In most cases you will use our interaction templates for communication, but our api also allows you to send message directly.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package inboxapi

import (
	"encoding/json"
)

// checks if the ContactKPI type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ContactKPI{}

// ContactKPI struct for ContactKPI
type ContactKPI struct {
	ContactId *string `json:"contact_id,omitempty"`
	ConversationsCount *float32 `json:"conversations_count,omitempty"`
	InteractionsCount *float32 `json:"interactions_count,omitempty"`
	InvestmentsCount *float32 `json:"investments_count,omitempty"`
	VisitsCount *float32 `json:"visits_count,omitempty"`
	PurchasesCount *float32 `json:"purchases_count,omitempty"`
	ReferralsCount *float32 `json:"referrals_count,omitempty"`
}

// NewContactKPI instantiates a new ContactKPI object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewContactKPI() *ContactKPI {
	this := ContactKPI{}
	return &this
}

// NewContactKPIWithDefaults instantiates a new ContactKPI object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewContactKPIWithDefaults() *ContactKPI {
	this := ContactKPI{}
	return &this
}

// GetContactId returns the ContactId field value if set, zero value otherwise.
func (o *ContactKPI) GetContactId() string {
	if o == nil || IsNil(o.ContactId) {
		var ret string
		return ret
	}
	return *o.ContactId
}

// GetContactIdOk returns a tuple with the ContactId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContactKPI) GetContactIdOk() (*string, bool) {
	if o == nil || IsNil(o.ContactId) {
		return nil, false
	}
	return o.ContactId, true
}

// HasContactId returns a boolean if a field has been set.
func (o *ContactKPI) HasContactId() bool {
	if o != nil && !IsNil(o.ContactId) {
		return true
	}

	return false
}

// SetContactId gets a reference to the given string and assigns it to the ContactId field.
func (o *ContactKPI) SetContactId(v string) {
	o.ContactId = &v
}

// GetConversationsCount returns the ConversationsCount field value if set, zero value otherwise.
func (o *ContactKPI) GetConversationsCount() float32 {
	if o == nil || IsNil(o.ConversationsCount) {
		var ret float32
		return ret
	}
	return *o.ConversationsCount
}

// GetConversationsCountOk returns a tuple with the ConversationsCount field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContactKPI) GetConversationsCountOk() (*float32, bool) {
	if o == nil || IsNil(o.ConversationsCount) {
		return nil, false
	}
	return o.ConversationsCount, true
}

// HasConversationsCount returns a boolean if a field has been set.
func (o *ContactKPI) HasConversationsCount() bool {
	if o != nil && !IsNil(o.ConversationsCount) {
		return true
	}

	return false
}

// SetConversationsCount gets a reference to the given float32 and assigns it to the ConversationsCount field.
func (o *ContactKPI) SetConversationsCount(v float32) {
	o.ConversationsCount = &v
}

// GetInteractionsCount returns the InteractionsCount field value if set, zero value otherwise.
func (o *ContactKPI) GetInteractionsCount() float32 {
	if o == nil || IsNil(o.InteractionsCount) {
		var ret float32
		return ret
	}
	return *o.InteractionsCount
}

// GetInteractionsCountOk returns a tuple with the InteractionsCount field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContactKPI) GetInteractionsCountOk() (*float32, bool) {
	if o == nil || IsNil(o.InteractionsCount) {
		return nil, false
	}
	return o.InteractionsCount, true
}

// HasInteractionsCount returns a boolean if a field has been set.
func (o *ContactKPI) HasInteractionsCount() bool {
	if o != nil && !IsNil(o.InteractionsCount) {
		return true
	}

	return false
}

// SetInteractionsCount gets a reference to the given float32 and assigns it to the InteractionsCount field.
func (o *ContactKPI) SetInteractionsCount(v float32) {
	o.InteractionsCount = &v
}

// GetInvestmentsCount returns the InvestmentsCount field value if set, zero value otherwise.
func (o *ContactKPI) GetInvestmentsCount() float32 {
	if o == nil || IsNil(o.InvestmentsCount) {
		var ret float32
		return ret
	}
	return *o.InvestmentsCount
}

// GetInvestmentsCountOk returns a tuple with the InvestmentsCount field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContactKPI) GetInvestmentsCountOk() (*float32, bool) {
	if o == nil || IsNil(o.InvestmentsCount) {
		return nil, false
	}
	return o.InvestmentsCount, true
}

// HasInvestmentsCount returns a boolean if a field has been set.
func (o *ContactKPI) HasInvestmentsCount() bool {
	if o != nil && !IsNil(o.InvestmentsCount) {
		return true
	}

	return false
}

// SetInvestmentsCount gets a reference to the given float32 and assigns it to the InvestmentsCount field.
func (o *ContactKPI) SetInvestmentsCount(v float32) {
	o.InvestmentsCount = &v
}

// GetVisitsCount returns the VisitsCount field value if set, zero value otherwise.
func (o *ContactKPI) GetVisitsCount() float32 {
	if o == nil || IsNil(o.VisitsCount) {
		var ret float32
		return ret
	}
	return *o.VisitsCount
}

// GetVisitsCountOk returns a tuple with the VisitsCount field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContactKPI) GetVisitsCountOk() (*float32, bool) {
	if o == nil || IsNil(o.VisitsCount) {
		return nil, false
	}
	return o.VisitsCount, true
}

// HasVisitsCount returns a boolean if a field has been set.
func (o *ContactKPI) HasVisitsCount() bool {
	if o != nil && !IsNil(o.VisitsCount) {
		return true
	}

	return false
}

// SetVisitsCount gets a reference to the given float32 and assigns it to the VisitsCount field.
func (o *ContactKPI) SetVisitsCount(v float32) {
	o.VisitsCount = &v
}

// GetPurchasesCount returns the PurchasesCount field value if set, zero value otherwise.
func (o *ContactKPI) GetPurchasesCount() float32 {
	if o == nil || IsNil(o.PurchasesCount) {
		var ret float32
		return ret
	}
	return *o.PurchasesCount
}

// GetPurchasesCountOk returns a tuple with the PurchasesCount field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContactKPI) GetPurchasesCountOk() (*float32, bool) {
	if o == nil || IsNil(o.PurchasesCount) {
		return nil, false
	}
	return o.PurchasesCount, true
}

// HasPurchasesCount returns a boolean if a field has been set.
func (o *ContactKPI) HasPurchasesCount() bool {
	if o != nil && !IsNil(o.PurchasesCount) {
		return true
	}

	return false
}

// SetPurchasesCount gets a reference to the given float32 and assigns it to the PurchasesCount field.
func (o *ContactKPI) SetPurchasesCount(v float32) {
	o.PurchasesCount = &v
}

// GetReferralsCount returns the ReferralsCount field value if set, zero value otherwise.
func (o *ContactKPI) GetReferralsCount() float32 {
	if o == nil || IsNil(o.ReferralsCount) {
		var ret float32
		return ret
	}
	return *o.ReferralsCount
}

// GetReferralsCountOk returns a tuple with the ReferralsCount field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContactKPI) GetReferralsCountOk() (*float32, bool) {
	if o == nil || IsNil(o.ReferralsCount) {
		return nil, false
	}
	return o.ReferralsCount, true
}

// HasReferralsCount returns a boolean if a field has been set.
func (o *ContactKPI) HasReferralsCount() bool {
	if o != nil && !IsNil(o.ReferralsCount) {
		return true
	}

	return false
}

// SetReferralsCount gets a reference to the given float32 and assigns it to the ReferralsCount field.
func (o *ContactKPI) SetReferralsCount(v float32) {
	o.ReferralsCount = &v
}

func (o ContactKPI) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ContactKPI) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.ContactId) {
		toSerialize["contact_id"] = o.ContactId
	}
	if !IsNil(o.ConversationsCount) {
		toSerialize["conversations_count"] = o.ConversationsCount
	}
	if !IsNil(o.InteractionsCount) {
		toSerialize["interactions_count"] = o.InteractionsCount
	}
	if !IsNil(o.InvestmentsCount) {
		toSerialize["investments_count"] = o.InvestmentsCount
	}
	if !IsNil(o.VisitsCount) {
		toSerialize["visits_count"] = o.VisitsCount
	}
	if !IsNil(o.PurchasesCount) {
		toSerialize["purchases_count"] = o.PurchasesCount
	}
	if !IsNil(o.ReferralsCount) {
		toSerialize["referrals_count"] = o.ReferralsCount
	}
	return toSerialize, nil
}

type NullableContactKPI struct {
	value *ContactKPI
	isSet bool
}

func (v NullableContactKPI) Get() *ContactKPI {
	return v.value
}

func (v *NullableContactKPI) Set(val *ContactKPI) {
	v.value = val
	v.isSet = true
}

func (v NullableContactKPI) IsSet() bool {
	return v.isSet
}

func (v *NullableContactKPI) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableContactKPI(val *ContactKPI) *NullableContactKPI {
	return &NullableContactKPI{value: val, isSet: true}
}

func (v NullableContactKPI) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableContactKPI) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


