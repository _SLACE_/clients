# AddConsentRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Key** | Pointer to **string** |  | [optional] 
**ConsentId** | Pointer to **string** |  | [optional] 
**Context** | Pointer to **string** |  | [optional] 

## Methods

### NewAddConsentRequest

`func NewAddConsentRequest() *AddConsentRequest`

NewAddConsentRequest instantiates a new AddConsentRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAddConsentRequestWithDefaults

`func NewAddConsentRequestWithDefaults() *AddConsentRequest`

NewAddConsentRequestWithDefaults instantiates a new AddConsentRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetKey

`func (o *AddConsentRequest) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *AddConsentRequest) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *AddConsentRequest) SetKey(v string)`

SetKey sets Key field to given value.

### HasKey

`func (o *AddConsentRequest) HasKey() bool`

HasKey returns a boolean if a field has been set.

### GetConsentId

`func (o *AddConsentRequest) GetConsentId() string`

GetConsentId returns the ConsentId field if non-nil, zero value otherwise.

### GetConsentIdOk

`func (o *AddConsentRequest) GetConsentIdOk() (*string, bool)`

GetConsentIdOk returns a tuple with the ConsentId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConsentId

`func (o *AddConsentRequest) SetConsentId(v string)`

SetConsentId sets ConsentId field to given value.

### HasConsentId

`func (o *AddConsentRequest) HasConsentId() bool`

HasConsentId returns a boolean if a field has been set.

### GetContext

`func (o *AddConsentRequest) GetContext() string`

GetContext returns the Context field if non-nil, zero value otherwise.

### GetContextOk

`func (o *AddConsentRequest) GetContextOk() (*string, bool)`

GetContextOk returns a tuple with the Context field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContext

`func (o *AddConsentRequest) SetContext(v string)`

SetContext sets Context field to given value.

### HasContext

`func (o *AddConsentRequest) HasContext() bool`

HasContext returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


