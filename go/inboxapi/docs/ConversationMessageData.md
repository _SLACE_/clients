# ConversationMessageData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Media** | Pointer to [**[]ConversationMessageDataMediaInner**](ConversationMessageDataMediaInner.md) |  | [optional] 
**Error** | Pointer to [**ConversationMessageDataError**](ConversationMessageDataError.md) |  | [optional] 
**Contacts** | Pointer to [**[]MessageContact**](MessageContact.md) |  | [optional] 
**Location** | Pointer to [**Location**](Location.md) |  | [optional] 
**Template** | Pointer to [**ConversationMessageDataTemplate**](ConversationMessageDataTemplate.md) |  | [optional] 
**Header** | Pointer to [**MessageHeader**](MessageHeader.md) |  | [optional] 
**Footer** | Pointer to [**MessageFooter**](MessageFooter.md) |  | [optional] 
**Buttons** | Pointer to [**[]MessageButton**](MessageButton.md) |  | [optional] 
**List** | Pointer to [**List**](List.md) |  | [optional] 
**MessengerTag** | Pointer to [**MessengerTag**](MessengerTag.md) |  | [optional] 
**OrderDetails** | Pointer to [**MessageOrderDetails**](MessageOrderDetails.md) |  | [optional] 
**OrderStatus** | Pointer to [**MessageOrderStatus**](MessageOrderStatus.md) |  | [optional] 
**Context** | Pointer to [**SendMessageContext**](SendMessageContext.md) |  | [optional] 
**Reaction** | Pointer to [**MessageReaction**](MessageReaction.md) |  | [optional] 
**Cards** | Pointer to [**[]MessageCard**](MessageCard.md) |  | [optional] 

## Methods

### NewConversationMessageData

`func NewConversationMessageData() *ConversationMessageData`

NewConversationMessageData instantiates a new ConversationMessageData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConversationMessageDataWithDefaults

`func NewConversationMessageDataWithDefaults() *ConversationMessageData`

NewConversationMessageDataWithDefaults instantiates a new ConversationMessageData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMedia

`func (o *ConversationMessageData) GetMedia() []ConversationMessageDataMediaInner`

GetMedia returns the Media field if non-nil, zero value otherwise.

### GetMediaOk

`func (o *ConversationMessageData) GetMediaOk() (*[]ConversationMessageDataMediaInner, bool)`

GetMediaOk returns a tuple with the Media field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMedia

`func (o *ConversationMessageData) SetMedia(v []ConversationMessageDataMediaInner)`

SetMedia sets Media field to given value.

### HasMedia

`func (o *ConversationMessageData) HasMedia() bool`

HasMedia returns a boolean if a field has been set.

### GetError

`func (o *ConversationMessageData) GetError() ConversationMessageDataError`

GetError returns the Error field if non-nil, zero value otherwise.

### GetErrorOk

`func (o *ConversationMessageData) GetErrorOk() (*ConversationMessageDataError, bool)`

GetErrorOk returns a tuple with the Error field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetError

`func (o *ConversationMessageData) SetError(v ConversationMessageDataError)`

SetError sets Error field to given value.

### HasError

`func (o *ConversationMessageData) HasError() bool`

HasError returns a boolean if a field has been set.

### GetContacts

`func (o *ConversationMessageData) GetContacts() []MessageContact`

GetContacts returns the Contacts field if non-nil, zero value otherwise.

### GetContactsOk

`func (o *ConversationMessageData) GetContactsOk() (*[]MessageContact, bool)`

GetContactsOk returns a tuple with the Contacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContacts

`func (o *ConversationMessageData) SetContacts(v []MessageContact)`

SetContacts sets Contacts field to given value.

### HasContacts

`func (o *ConversationMessageData) HasContacts() bool`

HasContacts returns a boolean if a field has been set.

### GetLocation

`func (o *ConversationMessageData) GetLocation() Location`

GetLocation returns the Location field if non-nil, zero value otherwise.

### GetLocationOk

`func (o *ConversationMessageData) GetLocationOk() (*Location, bool)`

GetLocationOk returns a tuple with the Location field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocation

`func (o *ConversationMessageData) SetLocation(v Location)`

SetLocation sets Location field to given value.

### HasLocation

`func (o *ConversationMessageData) HasLocation() bool`

HasLocation returns a boolean if a field has been set.

### GetTemplate

`func (o *ConversationMessageData) GetTemplate() ConversationMessageDataTemplate`

GetTemplate returns the Template field if non-nil, zero value otherwise.

### GetTemplateOk

`func (o *ConversationMessageData) GetTemplateOk() (*ConversationMessageDataTemplate, bool)`

GetTemplateOk returns a tuple with the Template field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplate

`func (o *ConversationMessageData) SetTemplate(v ConversationMessageDataTemplate)`

SetTemplate sets Template field to given value.

### HasTemplate

`func (o *ConversationMessageData) HasTemplate() bool`

HasTemplate returns a boolean if a field has been set.

### GetHeader

`func (o *ConversationMessageData) GetHeader() MessageHeader`

GetHeader returns the Header field if non-nil, zero value otherwise.

### GetHeaderOk

`func (o *ConversationMessageData) GetHeaderOk() (*MessageHeader, bool)`

GetHeaderOk returns a tuple with the Header field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeader

`func (o *ConversationMessageData) SetHeader(v MessageHeader)`

SetHeader sets Header field to given value.

### HasHeader

`func (o *ConversationMessageData) HasHeader() bool`

HasHeader returns a boolean if a field has been set.

### GetFooter

`func (o *ConversationMessageData) GetFooter() MessageFooter`

GetFooter returns the Footer field if non-nil, zero value otherwise.

### GetFooterOk

`func (o *ConversationMessageData) GetFooterOk() (*MessageFooter, bool)`

GetFooterOk returns a tuple with the Footer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFooter

`func (o *ConversationMessageData) SetFooter(v MessageFooter)`

SetFooter sets Footer field to given value.

### HasFooter

`func (o *ConversationMessageData) HasFooter() bool`

HasFooter returns a boolean if a field has been set.

### GetButtons

`func (o *ConversationMessageData) GetButtons() []MessageButton`

GetButtons returns the Buttons field if non-nil, zero value otherwise.

### GetButtonsOk

`func (o *ConversationMessageData) GetButtonsOk() (*[]MessageButton, bool)`

GetButtonsOk returns a tuple with the Buttons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtons

`func (o *ConversationMessageData) SetButtons(v []MessageButton)`

SetButtons sets Buttons field to given value.

### HasButtons

`func (o *ConversationMessageData) HasButtons() bool`

HasButtons returns a boolean if a field has been set.

### GetList

`func (o *ConversationMessageData) GetList() List`

GetList returns the List field if non-nil, zero value otherwise.

### GetListOk

`func (o *ConversationMessageData) GetListOk() (*List, bool)`

GetListOk returns a tuple with the List field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetList

`func (o *ConversationMessageData) SetList(v List)`

SetList sets List field to given value.

### HasList

`func (o *ConversationMessageData) HasList() bool`

HasList returns a boolean if a field has been set.

### GetMessengerTag

`func (o *ConversationMessageData) GetMessengerTag() MessengerTag`

GetMessengerTag returns the MessengerTag field if non-nil, zero value otherwise.

### GetMessengerTagOk

`func (o *ConversationMessageData) GetMessengerTagOk() (*MessengerTag, bool)`

GetMessengerTagOk returns a tuple with the MessengerTag field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerTag

`func (o *ConversationMessageData) SetMessengerTag(v MessengerTag)`

SetMessengerTag sets MessengerTag field to given value.

### HasMessengerTag

`func (o *ConversationMessageData) HasMessengerTag() bool`

HasMessengerTag returns a boolean if a field has been set.

### GetOrderDetails

`func (o *ConversationMessageData) GetOrderDetails() MessageOrderDetails`

GetOrderDetails returns the OrderDetails field if non-nil, zero value otherwise.

### GetOrderDetailsOk

`func (o *ConversationMessageData) GetOrderDetailsOk() (*MessageOrderDetails, bool)`

GetOrderDetailsOk returns a tuple with the OrderDetails field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderDetails

`func (o *ConversationMessageData) SetOrderDetails(v MessageOrderDetails)`

SetOrderDetails sets OrderDetails field to given value.

### HasOrderDetails

`func (o *ConversationMessageData) HasOrderDetails() bool`

HasOrderDetails returns a boolean if a field has been set.

### GetOrderStatus

`func (o *ConversationMessageData) GetOrderStatus() MessageOrderStatus`

GetOrderStatus returns the OrderStatus field if non-nil, zero value otherwise.

### GetOrderStatusOk

`func (o *ConversationMessageData) GetOrderStatusOk() (*MessageOrderStatus, bool)`

GetOrderStatusOk returns a tuple with the OrderStatus field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderStatus

`func (o *ConversationMessageData) SetOrderStatus(v MessageOrderStatus)`

SetOrderStatus sets OrderStatus field to given value.

### HasOrderStatus

`func (o *ConversationMessageData) HasOrderStatus() bool`

HasOrderStatus returns a boolean if a field has been set.

### GetContext

`func (o *ConversationMessageData) GetContext() SendMessageContext`

GetContext returns the Context field if non-nil, zero value otherwise.

### GetContextOk

`func (o *ConversationMessageData) GetContextOk() (*SendMessageContext, bool)`

GetContextOk returns a tuple with the Context field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContext

`func (o *ConversationMessageData) SetContext(v SendMessageContext)`

SetContext sets Context field to given value.

### HasContext

`func (o *ConversationMessageData) HasContext() bool`

HasContext returns a boolean if a field has been set.

### GetReaction

`func (o *ConversationMessageData) GetReaction() MessageReaction`

GetReaction returns the Reaction field if non-nil, zero value otherwise.

### GetReactionOk

`func (o *ConversationMessageData) GetReactionOk() (*MessageReaction, bool)`

GetReactionOk returns a tuple with the Reaction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReaction

`func (o *ConversationMessageData) SetReaction(v MessageReaction)`

SetReaction sets Reaction field to given value.

### HasReaction

`func (o *ConversationMessageData) HasReaction() bool`

HasReaction returns a boolean if a field has been set.

### GetCards

`func (o *ConversationMessageData) GetCards() []MessageCard`

GetCards returns the Cards field if non-nil, zero value otherwise.

### GetCardsOk

`func (o *ConversationMessageData) GetCardsOk() (*[]MessageCard, bool)`

GetCardsOk returns a tuple with the Cards field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCards

`func (o *ConversationMessageData) SetCards(v []MessageCard)`

SetCards sets Cards field to given value.

### HasCards

`func (o *ConversationMessageData) HasCards() bool`

HasCards returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


