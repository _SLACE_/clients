# MediaFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**GatewayId** | **string** |  | 
**MimeType** | **string** |  | 
**StorageId** | **string** |  | 
**MessengerId** | Pointer to **string** |  | [optional] 
**Size** | **int32** | Byte size | 
**Width** | Pointer to **int32** |  | [optional] 
**Height** | Pointer to **int32** |  | [optional] 
**Url** | **string** |  | 
**CreatedAt** | **time.Time** |  | 

## Methods

### NewMediaFile

`func NewMediaFile(id string, gatewayId string, mimeType string, storageId string, size int32, url string, createdAt time.Time, ) *MediaFile`

NewMediaFile instantiates a new MediaFile object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMediaFileWithDefaults

`func NewMediaFileWithDefaults() *MediaFile`

NewMediaFileWithDefaults instantiates a new MediaFile object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *MediaFile) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *MediaFile) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *MediaFile) SetId(v string)`

SetId sets Id field to given value.


### GetGatewayId

`func (o *MediaFile) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *MediaFile) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *MediaFile) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.


### GetMimeType

`func (o *MediaFile) GetMimeType() string`

GetMimeType returns the MimeType field if non-nil, zero value otherwise.

### GetMimeTypeOk

`func (o *MediaFile) GetMimeTypeOk() (*string, bool)`

GetMimeTypeOk returns a tuple with the MimeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMimeType

`func (o *MediaFile) SetMimeType(v string)`

SetMimeType sets MimeType field to given value.


### GetStorageId

`func (o *MediaFile) GetStorageId() string`

GetStorageId returns the StorageId field if non-nil, zero value otherwise.

### GetStorageIdOk

`func (o *MediaFile) GetStorageIdOk() (*string, bool)`

GetStorageIdOk returns a tuple with the StorageId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStorageId

`func (o *MediaFile) SetStorageId(v string)`

SetStorageId sets StorageId field to given value.


### GetMessengerId

`func (o *MediaFile) GetMessengerId() string`

GetMessengerId returns the MessengerId field if non-nil, zero value otherwise.

### GetMessengerIdOk

`func (o *MediaFile) GetMessengerIdOk() (*string, bool)`

GetMessengerIdOk returns a tuple with the MessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerId

`func (o *MediaFile) SetMessengerId(v string)`

SetMessengerId sets MessengerId field to given value.

### HasMessengerId

`func (o *MediaFile) HasMessengerId() bool`

HasMessengerId returns a boolean if a field has been set.

### GetSize

`func (o *MediaFile) GetSize() int32`

GetSize returns the Size field if non-nil, zero value otherwise.

### GetSizeOk

`func (o *MediaFile) GetSizeOk() (*int32, bool)`

GetSizeOk returns a tuple with the Size field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSize

`func (o *MediaFile) SetSize(v int32)`

SetSize sets Size field to given value.


### GetWidth

`func (o *MediaFile) GetWidth() int32`

GetWidth returns the Width field if non-nil, zero value otherwise.

### GetWidthOk

`func (o *MediaFile) GetWidthOk() (*int32, bool)`

GetWidthOk returns a tuple with the Width field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWidth

`func (o *MediaFile) SetWidth(v int32)`

SetWidth sets Width field to given value.

### HasWidth

`func (o *MediaFile) HasWidth() bool`

HasWidth returns a boolean if a field has been set.

### GetHeight

`func (o *MediaFile) GetHeight() int32`

GetHeight returns the Height field if non-nil, zero value otherwise.

### GetHeightOk

`func (o *MediaFile) GetHeightOk() (*int32, bool)`

GetHeightOk returns a tuple with the Height field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeight

`func (o *MediaFile) SetHeight(v int32)`

SetHeight sets Height field to given value.

### HasHeight

`func (o *MediaFile) HasHeight() bool`

HasHeight returns a boolean if a field has been set.

### GetUrl

`func (o *MediaFile) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *MediaFile) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *MediaFile) SetUrl(v string)`

SetUrl sets Url field to given value.


### GetCreatedAt

`func (o *MediaFile) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *MediaFile) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *MediaFile) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


