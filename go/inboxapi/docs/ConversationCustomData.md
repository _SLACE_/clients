# ConversationCustomData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Locale** | **string** |  | 
**TransactionId** | **string** |  | 
**Categories** | Pointer to [**map[string]time.Time**](time.Time.md) |  | [optional] 

## Methods

### NewConversationCustomData

`func NewConversationCustomData(locale string, transactionId string, ) *ConversationCustomData`

NewConversationCustomData instantiates a new ConversationCustomData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConversationCustomDataWithDefaults

`func NewConversationCustomDataWithDefaults() *ConversationCustomData`

NewConversationCustomDataWithDefaults instantiates a new ConversationCustomData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLocale

`func (o *ConversationCustomData) GetLocale() string`

GetLocale returns the Locale field if non-nil, zero value otherwise.

### GetLocaleOk

`func (o *ConversationCustomData) GetLocaleOk() (*string, bool)`

GetLocaleOk returns a tuple with the Locale field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocale

`func (o *ConversationCustomData) SetLocale(v string)`

SetLocale sets Locale field to given value.


### GetTransactionId

`func (o *ConversationCustomData) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *ConversationCustomData) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *ConversationCustomData) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.


### GetCategories

`func (o *ConversationCustomData) GetCategories() map[string]time.Time`

GetCategories returns the Categories field if non-nil, zero value otherwise.

### GetCategoriesOk

`func (o *ConversationCustomData) GetCategoriesOk() (*map[string]time.Time, bool)`

GetCategoriesOk returns a tuple with the Categories field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCategories

`func (o *ConversationCustomData) SetCategories(v map[string]time.Time)`

SetCategories sets Categories field to given value.

### HasCategories

`func (o *ConversationCustomData) HasCategories() bool`

HasCategories returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


