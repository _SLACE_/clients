# AddSegmentGroupRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SelectedIds** | Pointer to **[]string** |  | [optional] 
**SelectionMode** | **string** |  | 
**Segment** | Pointer to [**AddSegmentGroupRequestSegment**](AddSegmentGroupRequestSegment.md) |  | [optional] 

## Methods

### NewAddSegmentGroupRequest

`func NewAddSegmentGroupRequest(selectionMode string, ) *AddSegmentGroupRequest`

NewAddSegmentGroupRequest instantiates a new AddSegmentGroupRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAddSegmentGroupRequestWithDefaults

`func NewAddSegmentGroupRequestWithDefaults() *AddSegmentGroupRequest`

NewAddSegmentGroupRequestWithDefaults instantiates a new AddSegmentGroupRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelectedIds

`func (o *AddSegmentGroupRequest) GetSelectedIds() []string`

GetSelectedIds returns the SelectedIds field if non-nil, zero value otherwise.

### GetSelectedIdsOk

`func (o *AddSegmentGroupRequest) GetSelectedIdsOk() (*[]string, bool)`

GetSelectedIdsOk returns a tuple with the SelectedIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelectedIds

`func (o *AddSegmentGroupRequest) SetSelectedIds(v []string)`

SetSelectedIds sets SelectedIds field to given value.

### HasSelectedIds

`func (o *AddSegmentGroupRequest) HasSelectedIds() bool`

HasSelectedIds returns a boolean if a field has been set.

### GetSelectionMode

`func (o *AddSegmentGroupRequest) GetSelectionMode() string`

GetSelectionMode returns the SelectionMode field if non-nil, zero value otherwise.

### GetSelectionModeOk

`func (o *AddSegmentGroupRequest) GetSelectionModeOk() (*string, bool)`

GetSelectionModeOk returns a tuple with the SelectionMode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelectionMode

`func (o *AddSegmentGroupRequest) SetSelectionMode(v string)`

SetSelectionMode sets SelectionMode field to given value.


### GetSegment

`func (o *AddSegmentGroupRequest) GetSegment() AddSegmentGroupRequestSegment`

GetSegment returns the Segment field if non-nil, zero value otherwise.

### GetSegmentOk

`func (o *AddSegmentGroupRequest) GetSegmentOk() (*AddSegmentGroupRequestSegment, bool)`

GetSegmentOk returns a tuple with the Segment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSegment

`func (o *AddSegmentGroupRequest) SetSegment(v AddSegmentGroupRequestSegment)`

SetSegment sets Segment field to given value.

### HasSegment

`func (o *AddSegmentGroupRequest) HasSegment() bool`

HasSegment returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


