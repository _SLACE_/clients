# MessageContactUrl

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | **string** |  | 
**Type** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageContactUrl

`func NewMessageContactUrl(url string, ) *MessageContactUrl`

NewMessageContactUrl instantiates a new MessageContactUrl object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageContactUrlWithDefaults

`func NewMessageContactUrlWithDefaults() *MessageContactUrl`

NewMessageContactUrlWithDefaults instantiates a new MessageContactUrl object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUrl

`func (o *MessageContactUrl) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *MessageContactUrl) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *MessageContactUrl) SetUrl(v string)`

SetUrl sets Url field to given value.


### GetType

`func (o *MessageContactUrl) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MessageContactUrl) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MessageContactUrl) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *MessageContactUrl) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


