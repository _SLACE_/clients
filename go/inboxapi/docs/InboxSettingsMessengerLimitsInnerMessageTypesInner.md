# InboxSettingsMessengerLimitsInnerMessageTypesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to [**MessageType**](MessageType.md) |  | [optional] 
**Size** | Pointer to **int32** |  | [optional] 
**MimeTypes** | Pointer to **[]string** |  | [optional] 

## Methods

### NewInboxSettingsMessengerLimitsInnerMessageTypesInner

`func NewInboxSettingsMessengerLimitsInnerMessageTypesInner() *InboxSettingsMessengerLimitsInnerMessageTypesInner`

NewInboxSettingsMessengerLimitsInnerMessageTypesInner instantiates a new InboxSettingsMessengerLimitsInnerMessageTypesInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInboxSettingsMessengerLimitsInnerMessageTypesInnerWithDefaults

`func NewInboxSettingsMessengerLimitsInnerMessageTypesInnerWithDefaults() *InboxSettingsMessengerLimitsInnerMessageTypesInner`

NewInboxSettingsMessengerLimitsInnerMessageTypesInnerWithDefaults instantiates a new InboxSettingsMessengerLimitsInnerMessageTypesInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *InboxSettingsMessengerLimitsInnerMessageTypesInner) GetType() MessageType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *InboxSettingsMessengerLimitsInnerMessageTypesInner) GetTypeOk() (*MessageType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *InboxSettingsMessengerLimitsInnerMessageTypesInner) SetType(v MessageType)`

SetType sets Type field to given value.

### HasType

`func (o *InboxSettingsMessengerLimitsInnerMessageTypesInner) HasType() bool`

HasType returns a boolean if a field has been set.

### GetSize

`func (o *InboxSettingsMessengerLimitsInnerMessageTypesInner) GetSize() int32`

GetSize returns the Size field if non-nil, zero value otherwise.

### GetSizeOk

`func (o *InboxSettingsMessengerLimitsInnerMessageTypesInner) GetSizeOk() (*int32, bool)`

GetSizeOk returns a tuple with the Size field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSize

`func (o *InboxSettingsMessengerLimitsInnerMessageTypesInner) SetSize(v int32)`

SetSize sets Size field to given value.

### HasSize

`func (o *InboxSettingsMessengerLimitsInnerMessageTypesInner) HasSize() bool`

HasSize returns a boolean if a field has been set.

### GetMimeTypes

`func (o *InboxSettingsMessengerLimitsInnerMessageTypesInner) GetMimeTypes() []string`

GetMimeTypes returns the MimeTypes field if non-nil, zero value otherwise.

### GetMimeTypesOk

`func (o *InboxSettingsMessengerLimitsInnerMessageTypesInner) GetMimeTypesOk() (*[]string, bool)`

GetMimeTypesOk returns a tuple with the MimeTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMimeTypes

`func (o *InboxSettingsMessengerLimitsInnerMessageTypesInner) SetMimeTypes(v []string)`

SetMimeTypes sets MimeTypes field to given value.

### HasMimeTypes

`func (o *InboxSettingsMessengerLimitsInnerMessageTypesInner) HasMimeTypes() bool`

HasMimeTypes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


