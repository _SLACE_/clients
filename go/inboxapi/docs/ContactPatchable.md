# ContactPatchable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** | Profile name. Contacts created automatically out of conversations have messenger profile name here | [optional] 
**Timezone** | Pointer to **string** | Contact timezone in IANA format | [optional] 
**Mobile** | Pointer to **string** | Mobile number in E.164 format | [optional] 
**MobileVerified** | Pointer to **bool** | Flag to mark mobile number as verified | [optional] 
**FirstName** | Pointer to **string** |  | [optional] 
**LastName** | Pointer to **string** |  | [optional] 
**Email** | Pointer to **string** |  | [optional] 
**Gender** | Pointer to **string** |  | [optional] 
**Title** | Pointer to **string** |  | [optional] 
**Formal** | Pointer to **bool** |  | [optional] 
**Birthdate** | Pointer to **string** | Birthdate in format specified in organization settings. Default is yyyy-mm-dd | [optional] 
**DayOfBirth** | Pointer to **int32** | Overriden with birthdate data if provided | [optional] 
**MonthOfBirth** | Pointer to **int32** | Overriden with birthdate data if provided | [optional] 
**Address** | Pointer to [**ContactAddress**](ContactAddress.md) |  | [optional] 
**LoyaltyPoints** | Pointer to **map[string]int32** | Stores current loyalty points of a contact. Format [providerName_projectId]: [pointsAmount] } | [optional] 
**LifetimeLoyaltyPoints** | Pointer to **map[string]int32** | Stores lifetime loyalty points of a contact. Format { [providerName_projectId]: [pointsAmount] } | [optional] 
**StrictAttributes** | Pointer to [**map[string]ContactStrictAttribute**](ContactStrictAttribute.md) | JSON with custom properties as defined in organization settings. Format: { [propertyName]: { type: [string|float|bool|date|datetime], value: [propertyValue] } } | [optional] 
**CareOf** | Pointer to **string** |  | [optional] 
**Avatars** | Pointer to [**ContactAvatars**](ContactAvatars.md) |  | [optional] 
**Languages** | Pointer to [**ContactLanguages**](ContactLanguages.md) |  | [optional] 
**MobileVerifiedAt** | Pointer to **time.Time** |  | [optional] 
**ExternalIds** | Pointer to **map[string]interface{}** |  | [optional] 
**ErConfidence** | Pointer to [**ContactERConfidence**](ContactERConfidence.md) |  | [optional] 

## Methods

### NewContactPatchable

`func NewContactPatchable() *ContactPatchable`

NewContactPatchable instantiates a new ContactPatchable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactPatchableWithDefaults

`func NewContactPatchableWithDefaults() *ContactPatchable`

NewContactPatchableWithDefaults instantiates a new ContactPatchable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ContactPatchable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ContactPatchable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ContactPatchable) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ContactPatchable) HasName() bool`

HasName returns a boolean if a field has been set.

### GetTimezone

`func (o *ContactPatchable) GetTimezone() string`

GetTimezone returns the Timezone field if non-nil, zero value otherwise.

### GetTimezoneOk

`func (o *ContactPatchable) GetTimezoneOk() (*string, bool)`

GetTimezoneOk returns a tuple with the Timezone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimezone

`func (o *ContactPatchable) SetTimezone(v string)`

SetTimezone sets Timezone field to given value.

### HasTimezone

`func (o *ContactPatchable) HasTimezone() bool`

HasTimezone returns a boolean if a field has been set.

### GetMobile

`func (o *ContactPatchable) GetMobile() string`

GetMobile returns the Mobile field if non-nil, zero value otherwise.

### GetMobileOk

`func (o *ContactPatchable) GetMobileOk() (*string, bool)`

GetMobileOk returns a tuple with the Mobile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobile

`func (o *ContactPatchable) SetMobile(v string)`

SetMobile sets Mobile field to given value.

### HasMobile

`func (o *ContactPatchable) HasMobile() bool`

HasMobile returns a boolean if a field has been set.

### GetMobileVerified

`func (o *ContactPatchable) GetMobileVerified() bool`

GetMobileVerified returns the MobileVerified field if non-nil, zero value otherwise.

### GetMobileVerifiedOk

`func (o *ContactPatchable) GetMobileVerifiedOk() (*bool, bool)`

GetMobileVerifiedOk returns a tuple with the MobileVerified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobileVerified

`func (o *ContactPatchable) SetMobileVerified(v bool)`

SetMobileVerified sets MobileVerified field to given value.

### HasMobileVerified

`func (o *ContactPatchable) HasMobileVerified() bool`

HasMobileVerified returns a boolean if a field has been set.

### GetFirstName

`func (o *ContactPatchable) GetFirstName() string`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *ContactPatchable) GetFirstNameOk() (*string, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *ContactPatchable) SetFirstName(v string)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *ContactPatchable) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *ContactPatchable) GetLastName() string`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *ContactPatchable) GetLastNameOk() (*string, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *ContactPatchable) SetLastName(v string)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *ContactPatchable) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetEmail

`func (o *ContactPatchable) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *ContactPatchable) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *ContactPatchable) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *ContactPatchable) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetGender

`func (o *ContactPatchable) GetGender() string`

GetGender returns the Gender field if non-nil, zero value otherwise.

### GetGenderOk

`func (o *ContactPatchable) GetGenderOk() (*string, bool)`

GetGenderOk returns a tuple with the Gender field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGender

`func (o *ContactPatchable) SetGender(v string)`

SetGender sets Gender field to given value.

### HasGender

`func (o *ContactPatchable) HasGender() bool`

HasGender returns a boolean if a field has been set.

### GetTitle

`func (o *ContactPatchable) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *ContactPatchable) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *ContactPatchable) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *ContactPatchable) HasTitle() bool`

HasTitle returns a boolean if a field has been set.

### GetFormal

`func (o *ContactPatchable) GetFormal() bool`

GetFormal returns the Formal field if non-nil, zero value otherwise.

### GetFormalOk

`func (o *ContactPatchable) GetFormalOk() (*bool, bool)`

GetFormalOk returns a tuple with the Formal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormal

`func (o *ContactPatchable) SetFormal(v bool)`

SetFormal sets Formal field to given value.

### HasFormal

`func (o *ContactPatchable) HasFormal() bool`

HasFormal returns a boolean if a field has been set.

### GetBirthdate

`func (o *ContactPatchable) GetBirthdate() string`

GetBirthdate returns the Birthdate field if non-nil, zero value otherwise.

### GetBirthdateOk

`func (o *ContactPatchable) GetBirthdateOk() (*string, bool)`

GetBirthdateOk returns a tuple with the Birthdate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBirthdate

`func (o *ContactPatchable) SetBirthdate(v string)`

SetBirthdate sets Birthdate field to given value.

### HasBirthdate

`func (o *ContactPatchable) HasBirthdate() bool`

HasBirthdate returns a boolean if a field has been set.

### GetDayOfBirth

`func (o *ContactPatchable) GetDayOfBirth() int32`

GetDayOfBirth returns the DayOfBirth field if non-nil, zero value otherwise.

### GetDayOfBirthOk

`func (o *ContactPatchable) GetDayOfBirthOk() (*int32, bool)`

GetDayOfBirthOk returns a tuple with the DayOfBirth field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDayOfBirth

`func (o *ContactPatchable) SetDayOfBirth(v int32)`

SetDayOfBirth sets DayOfBirth field to given value.

### HasDayOfBirth

`func (o *ContactPatchable) HasDayOfBirth() bool`

HasDayOfBirth returns a boolean if a field has been set.

### GetMonthOfBirth

`func (o *ContactPatchable) GetMonthOfBirth() int32`

GetMonthOfBirth returns the MonthOfBirth field if non-nil, zero value otherwise.

### GetMonthOfBirthOk

`func (o *ContactPatchable) GetMonthOfBirthOk() (*int32, bool)`

GetMonthOfBirthOk returns a tuple with the MonthOfBirth field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMonthOfBirth

`func (o *ContactPatchable) SetMonthOfBirth(v int32)`

SetMonthOfBirth sets MonthOfBirth field to given value.

### HasMonthOfBirth

`func (o *ContactPatchable) HasMonthOfBirth() bool`

HasMonthOfBirth returns a boolean if a field has been set.

### GetAddress

`func (o *ContactPatchable) GetAddress() ContactAddress`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *ContactPatchable) GetAddressOk() (*ContactAddress, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *ContactPatchable) SetAddress(v ContactAddress)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *ContactPatchable) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetLoyaltyPoints

`func (o *ContactPatchable) GetLoyaltyPoints() map[string]int32`

GetLoyaltyPoints returns the LoyaltyPoints field if non-nil, zero value otherwise.

### GetLoyaltyPointsOk

`func (o *ContactPatchable) GetLoyaltyPointsOk() (*map[string]int32, bool)`

GetLoyaltyPointsOk returns a tuple with the LoyaltyPoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLoyaltyPoints

`func (o *ContactPatchable) SetLoyaltyPoints(v map[string]int32)`

SetLoyaltyPoints sets LoyaltyPoints field to given value.

### HasLoyaltyPoints

`func (o *ContactPatchable) HasLoyaltyPoints() bool`

HasLoyaltyPoints returns a boolean if a field has been set.

### GetLifetimeLoyaltyPoints

`func (o *ContactPatchable) GetLifetimeLoyaltyPoints() map[string]int32`

GetLifetimeLoyaltyPoints returns the LifetimeLoyaltyPoints field if non-nil, zero value otherwise.

### GetLifetimeLoyaltyPointsOk

`func (o *ContactPatchable) GetLifetimeLoyaltyPointsOk() (*map[string]int32, bool)`

GetLifetimeLoyaltyPointsOk returns a tuple with the LifetimeLoyaltyPoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLifetimeLoyaltyPoints

`func (o *ContactPatchable) SetLifetimeLoyaltyPoints(v map[string]int32)`

SetLifetimeLoyaltyPoints sets LifetimeLoyaltyPoints field to given value.

### HasLifetimeLoyaltyPoints

`func (o *ContactPatchable) HasLifetimeLoyaltyPoints() bool`

HasLifetimeLoyaltyPoints returns a boolean if a field has been set.

### GetStrictAttributes

`func (o *ContactPatchable) GetStrictAttributes() map[string]ContactStrictAttribute`

GetStrictAttributes returns the StrictAttributes field if non-nil, zero value otherwise.

### GetStrictAttributesOk

`func (o *ContactPatchable) GetStrictAttributesOk() (*map[string]ContactStrictAttribute, bool)`

GetStrictAttributesOk returns a tuple with the StrictAttributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStrictAttributes

`func (o *ContactPatchable) SetStrictAttributes(v map[string]ContactStrictAttribute)`

SetStrictAttributes sets StrictAttributes field to given value.

### HasStrictAttributes

`func (o *ContactPatchable) HasStrictAttributes() bool`

HasStrictAttributes returns a boolean if a field has been set.

### GetCareOf

`func (o *ContactPatchable) GetCareOf() string`

GetCareOf returns the CareOf field if non-nil, zero value otherwise.

### GetCareOfOk

`func (o *ContactPatchable) GetCareOfOk() (*string, bool)`

GetCareOfOk returns a tuple with the CareOf field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCareOf

`func (o *ContactPatchable) SetCareOf(v string)`

SetCareOf sets CareOf field to given value.

### HasCareOf

`func (o *ContactPatchable) HasCareOf() bool`

HasCareOf returns a boolean if a field has been set.

### GetAvatars

`func (o *ContactPatchable) GetAvatars() ContactAvatars`

GetAvatars returns the Avatars field if non-nil, zero value otherwise.

### GetAvatarsOk

`func (o *ContactPatchable) GetAvatarsOk() (*ContactAvatars, bool)`

GetAvatarsOk returns a tuple with the Avatars field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAvatars

`func (o *ContactPatchable) SetAvatars(v ContactAvatars)`

SetAvatars sets Avatars field to given value.

### HasAvatars

`func (o *ContactPatchable) HasAvatars() bool`

HasAvatars returns a boolean if a field has been set.

### GetLanguages

`func (o *ContactPatchable) GetLanguages() ContactLanguages`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *ContactPatchable) GetLanguagesOk() (*ContactLanguages, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *ContactPatchable) SetLanguages(v ContactLanguages)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *ContactPatchable) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetMobileVerifiedAt

`func (o *ContactPatchable) GetMobileVerifiedAt() time.Time`

GetMobileVerifiedAt returns the MobileVerifiedAt field if non-nil, zero value otherwise.

### GetMobileVerifiedAtOk

`func (o *ContactPatchable) GetMobileVerifiedAtOk() (*time.Time, bool)`

GetMobileVerifiedAtOk returns a tuple with the MobileVerifiedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobileVerifiedAt

`func (o *ContactPatchable) SetMobileVerifiedAt(v time.Time)`

SetMobileVerifiedAt sets MobileVerifiedAt field to given value.

### HasMobileVerifiedAt

`func (o *ContactPatchable) HasMobileVerifiedAt() bool`

HasMobileVerifiedAt returns a boolean if a field has been set.

### GetExternalIds

`func (o *ContactPatchable) GetExternalIds() map[string]interface{}`

GetExternalIds returns the ExternalIds field if non-nil, zero value otherwise.

### GetExternalIdsOk

`func (o *ContactPatchable) GetExternalIdsOk() (*map[string]interface{}, bool)`

GetExternalIdsOk returns a tuple with the ExternalIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalIds

`func (o *ContactPatchable) SetExternalIds(v map[string]interface{})`

SetExternalIds sets ExternalIds field to given value.

### HasExternalIds

`func (o *ContactPatchable) HasExternalIds() bool`

HasExternalIds returns a boolean if a field has been set.

### GetErConfidence

`func (o *ContactPatchable) GetErConfidence() ContactERConfidence`

GetErConfidence returns the ErConfidence field if non-nil, zero value otherwise.

### GetErConfidenceOk

`func (o *ContactPatchable) GetErConfidenceOk() (*ContactERConfidence, bool)`

GetErConfidenceOk returns a tuple with the ErConfidence field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErConfidence

`func (o *ContactPatchable) SetErConfidence(v ContactERConfidence)`

SetErConfidence sets ErConfidence field to given value.

### HasErConfidence

`func (o *ContactPatchable) HasErConfidence() bool`

HasErConfidence returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


