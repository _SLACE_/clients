# DeleteContactsGroupRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SelectedIds** | Pointer to **[]string** |  | [optional] 
**SelectionMode** | Pointer to **string** |  | [optional] 
**Limit** | Pointer to **int32** |  | [optional] 
**Offset** | Pointer to **int32** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**Mobile** | Pointer to **string** |  | [optional] 
**FirstName** | Pointer to **string** |  | [optional] 
**LastName** | Pointer to **string** |  | [optional] 
**Email** | Pointer to **string** |  | [optional] 
**Active** | Pointer to **bool** |  | [optional] 
**Status** | Pointer to **string** |  | [optional] 
**MessengerTypes** | Pointer to **[]string** |  | [optional] 
**Languages** | Pointer to **[]string** |  | [optional] 
**Labels** | Pointer to **[]string** |  | [optional] 
**Gender** | Pointer to **string** |  | [optional] 
**ConversationsRange** | Pointer to **[]string** |  | [optional] 
**InteractionsRange** | Pointer to **[]string** |  | [optional] 
**VisitsRange** | Pointer to **[]string** |  | [optional] 
**PurchasesRange** | Pointer to **[]string** |  | [optional] 
**Search** | Pointer to **string** |  | [optional] 
**Sort** | Pointer to **string** |  | [optional] 

## Methods

### NewDeleteContactsGroupRequest

`func NewDeleteContactsGroupRequest() *DeleteContactsGroupRequest`

NewDeleteContactsGroupRequest instantiates a new DeleteContactsGroupRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDeleteContactsGroupRequestWithDefaults

`func NewDeleteContactsGroupRequestWithDefaults() *DeleteContactsGroupRequest`

NewDeleteContactsGroupRequestWithDefaults instantiates a new DeleteContactsGroupRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelectedIds

`func (o *DeleteContactsGroupRequest) GetSelectedIds() []string`

GetSelectedIds returns the SelectedIds field if non-nil, zero value otherwise.

### GetSelectedIdsOk

`func (o *DeleteContactsGroupRequest) GetSelectedIdsOk() (*[]string, bool)`

GetSelectedIdsOk returns a tuple with the SelectedIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelectedIds

`func (o *DeleteContactsGroupRequest) SetSelectedIds(v []string)`

SetSelectedIds sets SelectedIds field to given value.

### HasSelectedIds

`func (o *DeleteContactsGroupRequest) HasSelectedIds() bool`

HasSelectedIds returns a boolean if a field has been set.

### GetSelectionMode

`func (o *DeleteContactsGroupRequest) GetSelectionMode() string`

GetSelectionMode returns the SelectionMode field if non-nil, zero value otherwise.

### GetSelectionModeOk

`func (o *DeleteContactsGroupRequest) GetSelectionModeOk() (*string, bool)`

GetSelectionModeOk returns a tuple with the SelectionMode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelectionMode

`func (o *DeleteContactsGroupRequest) SetSelectionMode(v string)`

SetSelectionMode sets SelectionMode field to given value.

### HasSelectionMode

`func (o *DeleteContactsGroupRequest) HasSelectionMode() bool`

HasSelectionMode returns a boolean if a field has been set.

### GetLimit

`func (o *DeleteContactsGroupRequest) GetLimit() int32`

GetLimit returns the Limit field if non-nil, zero value otherwise.

### GetLimitOk

`func (o *DeleteContactsGroupRequest) GetLimitOk() (*int32, bool)`

GetLimitOk returns a tuple with the Limit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLimit

`func (o *DeleteContactsGroupRequest) SetLimit(v int32)`

SetLimit sets Limit field to given value.

### HasLimit

`func (o *DeleteContactsGroupRequest) HasLimit() bool`

HasLimit returns a boolean if a field has been set.

### GetOffset

`func (o *DeleteContactsGroupRequest) GetOffset() int32`

GetOffset returns the Offset field if non-nil, zero value otherwise.

### GetOffsetOk

`func (o *DeleteContactsGroupRequest) GetOffsetOk() (*int32, bool)`

GetOffsetOk returns a tuple with the Offset field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOffset

`func (o *DeleteContactsGroupRequest) SetOffset(v int32)`

SetOffset sets Offset field to given value.

### HasOffset

`func (o *DeleteContactsGroupRequest) HasOffset() bool`

HasOffset returns a boolean if a field has been set.

### GetName

`func (o *DeleteContactsGroupRequest) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *DeleteContactsGroupRequest) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *DeleteContactsGroupRequest) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *DeleteContactsGroupRequest) HasName() bool`

HasName returns a boolean if a field has been set.

### GetMobile

`func (o *DeleteContactsGroupRequest) GetMobile() string`

GetMobile returns the Mobile field if non-nil, zero value otherwise.

### GetMobileOk

`func (o *DeleteContactsGroupRequest) GetMobileOk() (*string, bool)`

GetMobileOk returns a tuple with the Mobile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobile

`func (o *DeleteContactsGroupRequest) SetMobile(v string)`

SetMobile sets Mobile field to given value.

### HasMobile

`func (o *DeleteContactsGroupRequest) HasMobile() bool`

HasMobile returns a boolean if a field has been set.

### GetFirstName

`func (o *DeleteContactsGroupRequest) GetFirstName() string`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *DeleteContactsGroupRequest) GetFirstNameOk() (*string, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *DeleteContactsGroupRequest) SetFirstName(v string)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *DeleteContactsGroupRequest) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *DeleteContactsGroupRequest) GetLastName() string`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *DeleteContactsGroupRequest) GetLastNameOk() (*string, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *DeleteContactsGroupRequest) SetLastName(v string)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *DeleteContactsGroupRequest) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetEmail

`func (o *DeleteContactsGroupRequest) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *DeleteContactsGroupRequest) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *DeleteContactsGroupRequest) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *DeleteContactsGroupRequest) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetActive

`func (o *DeleteContactsGroupRequest) GetActive() bool`

GetActive returns the Active field if non-nil, zero value otherwise.

### GetActiveOk

`func (o *DeleteContactsGroupRequest) GetActiveOk() (*bool, bool)`

GetActiveOk returns a tuple with the Active field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActive

`func (o *DeleteContactsGroupRequest) SetActive(v bool)`

SetActive sets Active field to given value.

### HasActive

`func (o *DeleteContactsGroupRequest) HasActive() bool`

HasActive returns a boolean if a field has been set.

### GetStatus

`func (o *DeleteContactsGroupRequest) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *DeleteContactsGroupRequest) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *DeleteContactsGroupRequest) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *DeleteContactsGroupRequest) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetMessengerTypes

`func (o *DeleteContactsGroupRequest) GetMessengerTypes() []string`

GetMessengerTypes returns the MessengerTypes field if non-nil, zero value otherwise.

### GetMessengerTypesOk

`func (o *DeleteContactsGroupRequest) GetMessengerTypesOk() (*[]string, bool)`

GetMessengerTypesOk returns a tuple with the MessengerTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerTypes

`func (o *DeleteContactsGroupRequest) SetMessengerTypes(v []string)`

SetMessengerTypes sets MessengerTypes field to given value.

### HasMessengerTypes

`func (o *DeleteContactsGroupRequest) HasMessengerTypes() bool`

HasMessengerTypes returns a boolean if a field has been set.

### GetLanguages

`func (o *DeleteContactsGroupRequest) GetLanguages() []string`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *DeleteContactsGroupRequest) GetLanguagesOk() (*[]string, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *DeleteContactsGroupRequest) SetLanguages(v []string)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *DeleteContactsGroupRequest) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetLabels

`func (o *DeleteContactsGroupRequest) GetLabels() []string`

GetLabels returns the Labels field if non-nil, zero value otherwise.

### GetLabelsOk

`func (o *DeleteContactsGroupRequest) GetLabelsOk() (*[]string, bool)`

GetLabelsOk returns a tuple with the Labels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabels

`func (o *DeleteContactsGroupRequest) SetLabels(v []string)`

SetLabels sets Labels field to given value.

### HasLabels

`func (o *DeleteContactsGroupRequest) HasLabels() bool`

HasLabels returns a boolean if a field has been set.

### GetGender

`func (o *DeleteContactsGroupRequest) GetGender() string`

GetGender returns the Gender field if non-nil, zero value otherwise.

### GetGenderOk

`func (o *DeleteContactsGroupRequest) GetGenderOk() (*string, bool)`

GetGenderOk returns a tuple with the Gender field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGender

`func (o *DeleteContactsGroupRequest) SetGender(v string)`

SetGender sets Gender field to given value.

### HasGender

`func (o *DeleteContactsGroupRequest) HasGender() bool`

HasGender returns a boolean if a field has been set.

### GetConversationsRange

`func (o *DeleteContactsGroupRequest) GetConversationsRange() []string`

GetConversationsRange returns the ConversationsRange field if non-nil, zero value otherwise.

### GetConversationsRangeOk

`func (o *DeleteContactsGroupRequest) GetConversationsRangeOk() (*[]string, bool)`

GetConversationsRangeOk returns a tuple with the ConversationsRange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationsRange

`func (o *DeleteContactsGroupRequest) SetConversationsRange(v []string)`

SetConversationsRange sets ConversationsRange field to given value.

### HasConversationsRange

`func (o *DeleteContactsGroupRequest) HasConversationsRange() bool`

HasConversationsRange returns a boolean if a field has been set.

### GetInteractionsRange

`func (o *DeleteContactsGroupRequest) GetInteractionsRange() []string`

GetInteractionsRange returns the InteractionsRange field if non-nil, zero value otherwise.

### GetInteractionsRangeOk

`func (o *DeleteContactsGroupRequest) GetInteractionsRangeOk() (*[]string, bool)`

GetInteractionsRangeOk returns a tuple with the InteractionsRange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionsRange

`func (o *DeleteContactsGroupRequest) SetInteractionsRange(v []string)`

SetInteractionsRange sets InteractionsRange field to given value.

### HasInteractionsRange

`func (o *DeleteContactsGroupRequest) HasInteractionsRange() bool`

HasInteractionsRange returns a boolean if a field has been set.

### GetVisitsRange

`func (o *DeleteContactsGroupRequest) GetVisitsRange() []string`

GetVisitsRange returns the VisitsRange field if non-nil, zero value otherwise.

### GetVisitsRangeOk

`func (o *DeleteContactsGroupRequest) GetVisitsRangeOk() (*[]string, bool)`

GetVisitsRangeOk returns a tuple with the VisitsRange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVisitsRange

`func (o *DeleteContactsGroupRequest) SetVisitsRange(v []string)`

SetVisitsRange sets VisitsRange field to given value.

### HasVisitsRange

`func (o *DeleteContactsGroupRequest) HasVisitsRange() bool`

HasVisitsRange returns a boolean if a field has been set.

### GetPurchasesRange

`func (o *DeleteContactsGroupRequest) GetPurchasesRange() []string`

GetPurchasesRange returns the PurchasesRange field if non-nil, zero value otherwise.

### GetPurchasesRangeOk

`func (o *DeleteContactsGroupRequest) GetPurchasesRangeOk() (*[]string, bool)`

GetPurchasesRangeOk returns a tuple with the PurchasesRange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPurchasesRange

`func (o *DeleteContactsGroupRequest) SetPurchasesRange(v []string)`

SetPurchasesRange sets PurchasesRange field to given value.

### HasPurchasesRange

`func (o *DeleteContactsGroupRequest) HasPurchasesRange() bool`

HasPurchasesRange returns a boolean if a field has been set.

### GetSearch

`func (o *DeleteContactsGroupRequest) GetSearch() string`

GetSearch returns the Search field if non-nil, zero value otherwise.

### GetSearchOk

`func (o *DeleteContactsGroupRequest) GetSearchOk() (*string, bool)`

GetSearchOk returns a tuple with the Search field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSearch

`func (o *DeleteContactsGroupRequest) SetSearch(v string)`

SetSearch sets Search field to given value.

### HasSearch

`func (o *DeleteContactsGroupRequest) HasSearch() bool`

HasSearch returns a boolean if a field has been set.

### GetSort

`func (o *DeleteContactsGroupRequest) GetSort() string`

GetSort returns the Sort field if non-nil, zero value otherwise.

### GetSortOk

`func (o *DeleteContactsGroupRequest) GetSortOk() (*string, bool)`

GetSortOk returns a tuple with the Sort field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSort

`func (o *DeleteContactsGroupRequest) SetSort(v string)`

SetSort sets Sort field to given value.

### HasSort

`func (o *DeleteContactsGroupRequest) HasSort() bool`

HasSort returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


