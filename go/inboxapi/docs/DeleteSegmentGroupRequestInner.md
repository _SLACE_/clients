# DeleteSegmentGroupRequestInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Key** | **string** |  | 
**SelectedIds** | Pointer to **[]string** |  | [optional] 
**SelectionMode** | **string** |  | 

## Methods

### NewDeleteSegmentGroupRequestInner

`func NewDeleteSegmentGroupRequestInner(key string, selectionMode string, ) *DeleteSegmentGroupRequestInner`

NewDeleteSegmentGroupRequestInner instantiates a new DeleteSegmentGroupRequestInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDeleteSegmentGroupRequestInnerWithDefaults

`func NewDeleteSegmentGroupRequestInnerWithDefaults() *DeleteSegmentGroupRequestInner`

NewDeleteSegmentGroupRequestInnerWithDefaults instantiates a new DeleteSegmentGroupRequestInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetKey

`func (o *DeleteSegmentGroupRequestInner) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *DeleteSegmentGroupRequestInner) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *DeleteSegmentGroupRequestInner) SetKey(v string)`

SetKey sets Key field to given value.


### GetSelectedIds

`func (o *DeleteSegmentGroupRequestInner) GetSelectedIds() []string`

GetSelectedIds returns the SelectedIds field if non-nil, zero value otherwise.

### GetSelectedIdsOk

`func (o *DeleteSegmentGroupRequestInner) GetSelectedIdsOk() (*[]string, bool)`

GetSelectedIdsOk returns a tuple with the SelectedIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelectedIds

`func (o *DeleteSegmentGroupRequestInner) SetSelectedIds(v []string)`

SetSelectedIds sets SelectedIds field to given value.

### HasSelectedIds

`func (o *DeleteSegmentGroupRequestInner) HasSelectedIds() bool`

HasSelectedIds returns a boolean if a field has been set.

### GetSelectionMode

`func (o *DeleteSegmentGroupRequestInner) GetSelectionMode() string`

GetSelectionMode returns the SelectionMode field if non-nil, zero value otherwise.

### GetSelectionModeOk

`func (o *DeleteSegmentGroupRequestInner) GetSelectionModeOk() (*string, bool)`

GetSelectionModeOk returns a tuple with the SelectionMode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelectionMode

`func (o *DeleteSegmentGroupRequestInner) SetSelectionMode(v string)`

SetSelectionMode sets SelectionMode field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


