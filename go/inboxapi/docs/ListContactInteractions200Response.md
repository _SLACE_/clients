# ListContactInteractions200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Total** | Pointer to **float32** |  | [optional] 
**Results** | Pointer to [**[]ConversationMessage**](ConversationMessage.md) |  | [optional] 

## Methods

### NewListContactInteractions200Response

`func NewListContactInteractions200Response() *ListContactInteractions200Response`

NewListContactInteractions200Response instantiates a new ListContactInteractions200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListContactInteractions200ResponseWithDefaults

`func NewListContactInteractions200ResponseWithDefaults() *ListContactInteractions200Response`

NewListContactInteractions200ResponseWithDefaults instantiates a new ListContactInteractions200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotal

`func (o *ListContactInteractions200Response) GetTotal() float32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *ListContactInteractions200Response) GetTotalOk() (*float32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *ListContactInteractions200Response) SetTotal(v float32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *ListContactInteractions200Response) HasTotal() bool`

HasTotal returns a boolean if a field has been set.

### GetResults

`func (o *ListContactInteractions200Response) GetResults() []ConversationMessage`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *ListContactInteractions200Response) GetResultsOk() (*[]ConversationMessage, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *ListContactInteractions200Response) SetResults(v []ConversationMessage)`

SetResults sets Results field to given value.

### HasResults

`func (o *ListContactInteractions200Response) HasResults() bool`

HasResults returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


