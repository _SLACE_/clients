# ActivityPatchable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Attributes** | Pointer to **map[string][]string** |  | [optional] 
**File** | Pointer to [**ActivityPatchableFile**](ActivityPatchableFile.md) |  | [optional] 

## Methods

### NewActivityPatchable

`func NewActivityPatchable() *ActivityPatchable`

NewActivityPatchable instantiates a new ActivityPatchable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewActivityPatchableWithDefaults

`func NewActivityPatchableWithDefaults() *ActivityPatchable`

NewActivityPatchableWithDefaults instantiates a new ActivityPatchable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAttributes

`func (o *ActivityPatchable) GetAttributes() map[string][]string`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *ActivityPatchable) GetAttributesOk() (*map[string][]string, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *ActivityPatchable) SetAttributes(v map[string][]string)`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *ActivityPatchable) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetFile

`func (o *ActivityPatchable) GetFile() ActivityPatchableFile`

GetFile returns the File field if non-nil, zero value otherwise.

### GetFileOk

`func (o *ActivityPatchable) GetFileOk() (*ActivityPatchableFile, bool)`

GetFileOk returns a tuple with the File field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFile

`func (o *ActivityPatchable) SetFile(v ActivityPatchableFile)`

SetFile sets File field to given value.

### HasFile

`func (o *ActivityPatchable) HasFile() bool`

HasFile returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


