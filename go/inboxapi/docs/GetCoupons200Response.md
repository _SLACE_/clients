# GetCoupons200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Total** | **int32** |  | 
**Results** | [**[]Coupon**](Coupon.md) |  | 

## Methods

### NewGetCoupons200Response

`func NewGetCoupons200Response(total int32, results []Coupon, ) *GetCoupons200Response`

NewGetCoupons200Response instantiates a new GetCoupons200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetCoupons200ResponseWithDefaults

`func NewGetCoupons200ResponseWithDefaults() *GetCoupons200Response`

NewGetCoupons200ResponseWithDefaults instantiates a new GetCoupons200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotal

`func (o *GetCoupons200Response) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *GetCoupons200Response) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *GetCoupons200Response) SetTotal(v int32)`

SetTotal sets Total field to given value.


### GetResults

`func (o *GetCoupons200Response) GetResults() []Coupon`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *GetCoupons200Response) GetResultsOk() (*[]Coupon, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *GetCoupons200Response) SetResults(v []Coupon)`

SetResults sets Results field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


