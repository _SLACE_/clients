# ContactDevices

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to **string** |  | [optional] 
**BrandCode** | Pointer to **string** |  | [optional] 
**DeviceName** | Pointer to **string** |  | [optional] 
**DeviceType** | Pointer to **string** |  | [optional] 
**OsFamilyCode** | Pointer to **string** |  | [optional] 
**OsName** | Pointer to **string** |  | [optional] 
**BrowserName** | Pointer to **string** |  | [optional] 

## Methods

### NewContactDevices

`func NewContactDevices() *ContactDevices`

NewContactDevices instantiates a new ContactDevices object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactDevicesWithDefaults

`func NewContactDevicesWithDefaults() *ContactDevices`

NewContactDevicesWithDefaults instantiates a new ContactDevices object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *ContactDevices) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ContactDevices) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ContactDevices) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ContactDevices) HasType() bool`

HasType returns a boolean if a field has been set.

### GetBrandCode

`func (o *ContactDevices) GetBrandCode() string`

GetBrandCode returns the BrandCode field if non-nil, zero value otherwise.

### GetBrandCodeOk

`func (o *ContactDevices) GetBrandCodeOk() (*string, bool)`

GetBrandCodeOk returns a tuple with the BrandCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBrandCode

`func (o *ContactDevices) SetBrandCode(v string)`

SetBrandCode sets BrandCode field to given value.

### HasBrandCode

`func (o *ContactDevices) HasBrandCode() bool`

HasBrandCode returns a boolean if a field has been set.

### GetDeviceName

`func (o *ContactDevices) GetDeviceName() string`

GetDeviceName returns the DeviceName field if non-nil, zero value otherwise.

### GetDeviceNameOk

`func (o *ContactDevices) GetDeviceNameOk() (*string, bool)`

GetDeviceNameOk returns a tuple with the DeviceName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDeviceName

`func (o *ContactDevices) SetDeviceName(v string)`

SetDeviceName sets DeviceName field to given value.

### HasDeviceName

`func (o *ContactDevices) HasDeviceName() bool`

HasDeviceName returns a boolean if a field has been set.

### GetDeviceType

`func (o *ContactDevices) GetDeviceType() string`

GetDeviceType returns the DeviceType field if non-nil, zero value otherwise.

### GetDeviceTypeOk

`func (o *ContactDevices) GetDeviceTypeOk() (*string, bool)`

GetDeviceTypeOk returns a tuple with the DeviceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDeviceType

`func (o *ContactDevices) SetDeviceType(v string)`

SetDeviceType sets DeviceType field to given value.

### HasDeviceType

`func (o *ContactDevices) HasDeviceType() bool`

HasDeviceType returns a boolean if a field has been set.

### GetOsFamilyCode

`func (o *ContactDevices) GetOsFamilyCode() string`

GetOsFamilyCode returns the OsFamilyCode field if non-nil, zero value otherwise.

### GetOsFamilyCodeOk

`func (o *ContactDevices) GetOsFamilyCodeOk() (*string, bool)`

GetOsFamilyCodeOk returns a tuple with the OsFamilyCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOsFamilyCode

`func (o *ContactDevices) SetOsFamilyCode(v string)`

SetOsFamilyCode sets OsFamilyCode field to given value.

### HasOsFamilyCode

`func (o *ContactDevices) HasOsFamilyCode() bool`

HasOsFamilyCode returns a boolean if a field has been set.

### GetOsName

`func (o *ContactDevices) GetOsName() string`

GetOsName returns the OsName field if non-nil, zero value otherwise.

### GetOsNameOk

`func (o *ContactDevices) GetOsNameOk() (*string, bool)`

GetOsNameOk returns a tuple with the OsName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOsName

`func (o *ContactDevices) SetOsName(v string)`

SetOsName sets OsName field to given value.

### HasOsName

`func (o *ContactDevices) HasOsName() bool`

HasOsName returns a boolean if a field has been set.

### GetBrowserName

`func (o *ContactDevices) GetBrowserName() string`

GetBrowserName returns the BrowserName field if non-nil, zero value otherwise.

### GetBrowserNameOk

`func (o *ContactDevices) GetBrowserNameOk() (*string, bool)`

GetBrowserNameOk returns a tuple with the BrowserName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBrowserName

`func (o *ContactDevices) SetBrowserName(v string)`

SetBrowserName sets BrowserName field to given value.

### HasBrowserName

`func (o *ContactDevices) HasBrowserName() bool`

HasBrowserName returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


