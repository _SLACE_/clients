# ContactAvatars

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Primary** | Pointer to **string** | Profile image URL | [optional] 
**Additional** | Pointer to **map[string]string** | Additional profile images in JSON format: { [source]: [url] } | [optional] 

## Methods

### NewContactAvatars

`func NewContactAvatars() *ContactAvatars`

NewContactAvatars instantiates a new ContactAvatars object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactAvatarsWithDefaults

`func NewContactAvatarsWithDefaults() *ContactAvatars`

NewContactAvatarsWithDefaults instantiates a new ContactAvatars object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPrimary

`func (o *ContactAvatars) GetPrimary() string`

GetPrimary returns the Primary field if non-nil, zero value otherwise.

### GetPrimaryOk

`func (o *ContactAvatars) GetPrimaryOk() (*string, bool)`

GetPrimaryOk returns a tuple with the Primary field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrimary

`func (o *ContactAvatars) SetPrimary(v string)`

SetPrimary sets Primary field to given value.

### HasPrimary

`func (o *ContactAvatars) HasPrimary() bool`

HasPrimary returns a boolean if a field has been set.

### GetAdditional

`func (o *ContactAvatars) GetAdditional() map[string]string`

GetAdditional returns the Additional field if non-nil, zero value otherwise.

### GetAdditionalOk

`func (o *ContactAvatars) GetAdditionalOk() (*map[string]string, bool)`

GetAdditionalOk returns a tuple with the Additional field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditional

`func (o *ContactAvatars) SetAdditional(v map[string]string)`

SetAdditional sets Additional field to given value.

### HasAdditional

`func (o *ContactAvatars) HasAdditional() bool`

HasAdditional returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


