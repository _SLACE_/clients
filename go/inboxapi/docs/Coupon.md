# Coupon

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [readonly] 
**Source** | **string** |  | 
**Code** | **string** |  | 
**ContactId** | **string** |  | 
**Status** | **string** |  | 
**CreatedAt** | **time.Time** |  | [readonly] 
**RedeemedAt** | Pointer to **time.Time** |  | [optional] 
**ExpiresAt** | Pointer to **time.Time** |  | [optional] 
**Attributes** | Pointer to **map[string][]string** |  | [optional] 
**TransactionId** | Pointer to **string** |  | [optional] 
**InteractionId** | Pointer to **string** |  | [optional] 
**ActivityId** | Pointer to **string** |  | [optional] 
**ConversationId** | Pointer to **string** |  | [optional] 
**TotalAmount** | Pointer to **string** |  | [optional] 
**DisplayCode** | Pointer to **string** |  | [optional] 
**DisplayType** | **string** |  | 
**Name** | **string** |  | 
**Description** | Pointer to **string** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 
**Currency** | Pointer to **string** |  | [optional] 
**Value** | Pointer to **string** |  | [optional] 
**ExpiresIn** | Pointer to **int32** |  | [optional] 
**LastSeenAt** | Pointer to **time.Time** |  | [optional] 
**DisplayExpiresIn** | Pointer to **int32** |  | [optional] 
**ChannelId** | Pointer to **string** |  | [optional] 

## Methods

### NewCoupon

`func NewCoupon(id string, source string, code string, contactId string, status string, createdAt time.Time, displayType string, name string, ) *Coupon`

NewCoupon instantiates a new Coupon object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCouponWithDefaults

`func NewCouponWithDefaults() *Coupon`

NewCouponWithDefaults instantiates a new Coupon object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Coupon) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Coupon) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Coupon) SetId(v string)`

SetId sets Id field to given value.


### GetSource

`func (o *Coupon) GetSource() string`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *Coupon) GetSourceOk() (*string, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *Coupon) SetSource(v string)`

SetSource sets Source field to given value.


### GetCode

`func (o *Coupon) GetCode() string`

GetCode returns the Code field if non-nil, zero value otherwise.

### GetCodeOk

`func (o *Coupon) GetCodeOk() (*string, bool)`

GetCodeOk returns a tuple with the Code field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCode

`func (o *Coupon) SetCode(v string)`

SetCode sets Code field to given value.


### GetContactId

`func (o *Coupon) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *Coupon) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *Coupon) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetStatus

`func (o *Coupon) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *Coupon) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *Coupon) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetCreatedAt

`func (o *Coupon) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Coupon) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Coupon) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.


### GetRedeemedAt

`func (o *Coupon) GetRedeemedAt() time.Time`

GetRedeemedAt returns the RedeemedAt field if non-nil, zero value otherwise.

### GetRedeemedAtOk

`func (o *Coupon) GetRedeemedAtOk() (*time.Time, bool)`

GetRedeemedAtOk returns a tuple with the RedeemedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRedeemedAt

`func (o *Coupon) SetRedeemedAt(v time.Time)`

SetRedeemedAt sets RedeemedAt field to given value.

### HasRedeemedAt

`func (o *Coupon) HasRedeemedAt() bool`

HasRedeemedAt returns a boolean if a field has been set.

### GetExpiresAt

`func (o *Coupon) GetExpiresAt() time.Time`

GetExpiresAt returns the ExpiresAt field if non-nil, zero value otherwise.

### GetExpiresAtOk

`func (o *Coupon) GetExpiresAtOk() (*time.Time, bool)`

GetExpiresAtOk returns a tuple with the ExpiresAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiresAt

`func (o *Coupon) SetExpiresAt(v time.Time)`

SetExpiresAt sets ExpiresAt field to given value.

### HasExpiresAt

`func (o *Coupon) HasExpiresAt() bool`

HasExpiresAt returns a boolean if a field has been set.

### GetAttributes

`func (o *Coupon) GetAttributes() map[string][]string`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *Coupon) GetAttributesOk() (*map[string][]string, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *Coupon) SetAttributes(v map[string][]string)`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *Coupon) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetTransactionId

`func (o *Coupon) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *Coupon) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *Coupon) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.

### HasTransactionId

`func (o *Coupon) HasTransactionId() bool`

HasTransactionId returns a boolean if a field has been set.

### GetInteractionId

`func (o *Coupon) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *Coupon) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *Coupon) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *Coupon) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetActivityId

`func (o *Coupon) GetActivityId() string`

GetActivityId returns the ActivityId field if non-nil, zero value otherwise.

### GetActivityIdOk

`func (o *Coupon) GetActivityIdOk() (*string, bool)`

GetActivityIdOk returns a tuple with the ActivityId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActivityId

`func (o *Coupon) SetActivityId(v string)`

SetActivityId sets ActivityId field to given value.

### HasActivityId

`func (o *Coupon) HasActivityId() bool`

HasActivityId returns a boolean if a field has been set.

### GetConversationId

`func (o *Coupon) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *Coupon) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *Coupon) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.

### HasConversationId

`func (o *Coupon) HasConversationId() bool`

HasConversationId returns a boolean if a field has been set.

### GetTotalAmount

`func (o *Coupon) GetTotalAmount() string`

GetTotalAmount returns the TotalAmount field if non-nil, zero value otherwise.

### GetTotalAmountOk

`func (o *Coupon) GetTotalAmountOk() (*string, bool)`

GetTotalAmountOk returns a tuple with the TotalAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalAmount

`func (o *Coupon) SetTotalAmount(v string)`

SetTotalAmount sets TotalAmount field to given value.

### HasTotalAmount

`func (o *Coupon) HasTotalAmount() bool`

HasTotalAmount returns a boolean if a field has been set.

### GetDisplayCode

`func (o *Coupon) GetDisplayCode() string`

GetDisplayCode returns the DisplayCode field if non-nil, zero value otherwise.

### GetDisplayCodeOk

`func (o *Coupon) GetDisplayCodeOk() (*string, bool)`

GetDisplayCodeOk returns a tuple with the DisplayCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayCode

`func (o *Coupon) SetDisplayCode(v string)`

SetDisplayCode sets DisplayCode field to given value.

### HasDisplayCode

`func (o *Coupon) HasDisplayCode() bool`

HasDisplayCode returns a boolean if a field has been set.

### GetDisplayType

`func (o *Coupon) GetDisplayType() string`

GetDisplayType returns the DisplayType field if non-nil, zero value otherwise.

### GetDisplayTypeOk

`func (o *Coupon) GetDisplayTypeOk() (*string, bool)`

GetDisplayTypeOk returns a tuple with the DisplayType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayType

`func (o *Coupon) SetDisplayType(v string)`

SetDisplayType sets DisplayType field to given value.


### GetName

`func (o *Coupon) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Coupon) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Coupon) SetName(v string)`

SetName sets Name field to given value.


### GetDescription

`func (o *Coupon) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *Coupon) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *Coupon) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *Coupon) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetType

`func (o *Coupon) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Coupon) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Coupon) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *Coupon) HasType() bool`

HasType returns a boolean if a field has been set.

### GetCurrency

`func (o *Coupon) GetCurrency() string`

GetCurrency returns the Currency field if non-nil, zero value otherwise.

### GetCurrencyOk

`func (o *Coupon) GetCurrencyOk() (*string, bool)`

GetCurrencyOk returns a tuple with the Currency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrency

`func (o *Coupon) SetCurrency(v string)`

SetCurrency sets Currency field to given value.

### HasCurrency

`func (o *Coupon) HasCurrency() bool`

HasCurrency returns a boolean if a field has been set.

### GetValue

`func (o *Coupon) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *Coupon) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *Coupon) SetValue(v string)`

SetValue sets Value field to given value.

### HasValue

`func (o *Coupon) HasValue() bool`

HasValue returns a boolean if a field has been set.

### GetExpiresIn

`func (o *Coupon) GetExpiresIn() int32`

GetExpiresIn returns the ExpiresIn field if non-nil, zero value otherwise.

### GetExpiresInOk

`func (o *Coupon) GetExpiresInOk() (*int32, bool)`

GetExpiresInOk returns a tuple with the ExpiresIn field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiresIn

`func (o *Coupon) SetExpiresIn(v int32)`

SetExpiresIn sets ExpiresIn field to given value.

### HasExpiresIn

`func (o *Coupon) HasExpiresIn() bool`

HasExpiresIn returns a boolean if a field has been set.

### GetLastSeenAt

`func (o *Coupon) GetLastSeenAt() time.Time`

GetLastSeenAt returns the LastSeenAt field if non-nil, zero value otherwise.

### GetLastSeenAtOk

`func (o *Coupon) GetLastSeenAtOk() (*time.Time, bool)`

GetLastSeenAtOk returns a tuple with the LastSeenAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastSeenAt

`func (o *Coupon) SetLastSeenAt(v time.Time)`

SetLastSeenAt sets LastSeenAt field to given value.

### HasLastSeenAt

`func (o *Coupon) HasLastSeenAt() bool`

HasLastSeenAt returns a boolean if a field has been set.

### GetDisplayExpiresIn

`func (o *Coupon) GetDisplayExpiresIn() int32`

GetDisplayExpiresIn returns the DisplayExpiresIn field if non-nil, zero value otherwise.

### GetDisplayExpiresInOk

`func (o *Coupon) GetDisplayExpiresInOk() (*int32, bool)`

GetDisplayExpiresInOk returns a tuple with the DisplayExpiresIn field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayExpiresIn

`func (o *Coupon) SetDisplayExpiresIn(v int32)`

SetDisplayExpiresIn sets DisplayExpiresIn field to given value.

### HasDisplayExpiresIn

`func (o *Coupon) HasDisplayExpiresIn() bool`

HasDisplayExpiresIn returns a boolean if a field has been set.

### GetChannelId

`func (o *Coupon) GetChannelId() string`

GetChannelId returns the ChannelId field if non-nil, zero value otherwise.

### GetChannelIdOk

`func (o *Coupon) GetChannelIdOk() (*string, bool)`

GetChannelIdOk returns a tuple with the ChannelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannelId

`func (o *Coupon) SetChannelId(v string)`

SetChannelId sets ChannelId field to given value.

### HasChannelId

`func (o *Coupon) HasChannelId() bool`

HasChannelId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


