# ContactsQualMetrics

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ActiveAmount** | Pointer to **int32** |  | [optional] 

## Methods

### NewContactsQualMetrics

`func NewContactsQualMetrics() *ContactsQualMetrics`

NewContactsQualMetrics instantiates a new ContactsQualMetrics object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactsQualMetricsWithDefaults

`func NewContactsQualMetricsWithDefaults() *ContactsQualMetrics`

NewContactsQualMetricsWithDefaults instantiates a new ContactsQualMetrics object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetActiveAmount

`func (o *ContactsQualMetrics) GetActiveAmount() int32`

GetActiveAmount returns the ActiveAmount field if non-nil, zero value otherwise.

### GetActiveAmountOk

`func (o *ContactsQualMetrics) GetActiveAmountOk() (*int32, bool)`

GetActiveAmountOk returns a tuple with the ActiveAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActiveAmount

`func (o *ContactsQualMetrics) SetActiveAmount(v int32)`

SetActiveAmount sets ActiveAmount field to given value.

### HasActiveAmount

`func (o *ContactsQualMetrics) HasActiveAmount() bool`

HasActiveAmount returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


