# ConversationMessageDataError

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Text** | Pointer to **string** |  | [optional] 
**Code** | Pointer to **string** |  | [optional] 

## Methods

### NewConversationMessageDataError

`func NewConversationMessageDataError() *ConversationMessageDataError`

NewConversationMessageDataError instantiates a new ConversationMessageDataError object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConversationMessageDataErrorWithDefaults

`func NewConversationMessageDataErrorWithDefaults() *ConversationMessageDataError`

NewConversationMessageDataErrorWithDefaults instantiates a new ConversationMessageDataError object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetText

`func (o *ConversationMessageDataError) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *ConversationMessageDataError) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *ConversationMessageDataError) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *ConversationMessageDataError) HasText() bool`

HasText returns a boolean if a field has been set.

### GetCode

`func (o *ConversationMessageDataError) GetCode() string`

GetCode returns the Code field if non-nil, zero value otherwise.

### GetCodeOk

`func (o *ConversationMessageDataError) GetCodeOk() (*string, bool)`

GetCodeOk returns a tuple with the Code field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCode

`func (o *ConversationMessageDataError) SetCode(v string)`

SetCode sets Code field to given value.

### HasCode

`func (o *ConversationMessageDataError) HasCode() bool`

HasCode returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


