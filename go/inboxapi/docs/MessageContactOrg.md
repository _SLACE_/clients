# MessageContactOrg

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Company** | Pointer to **string** |  | [optional] 
**Department** | Pointer to **string** |  | [optional] 
**Title** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageContactOrg

`func NewMessageContactOrg() *MessageContactOrg`

NewMessageContactOrg instantiates a new MessageContactOrg object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageContactOrgWithDefaults

`func NewMessageContactOrgWithDefaults() *MessageContactOrg`

NewMessageContactOrgWithDefaults instantiates a new MessageContactOrg object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCompany

`func (o *MessageContactOrg) GetCompany() string`

GetCompany returns the Company field if non-nil, zero value otherwise.

### GetCompanyOk

`func (o *MessageContactOrg) GetCompanyOk() (*string, bool)`

GetCompanyOk returns a tuple with the Company field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCompany

`func (o *MessageContactOrg) SetCompany(v string)`

SetCompany sets Company field to given value.

### HasCompany

`func (o *MessageContactOrg) HasCompany() bool`

HasCompany returns a boolean if a field has been set.

### GetDepartment

`func (o *MessageContactOrg) GetDepartment() string`

GetDepartment returns the Department field if non-nil, zero value otherwise.

### GetDepartmentOk

`func (o *MessageContactOrg) GetDepartmentOk() (*string, bool)`

GetDepartmentOk returns a tuple with the Department field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDepartment

`func (o *MessageContactOrg) SetDepartment(v string)`

SetDepartment sets Department field to given value.

### HasDepartment

`func (o *MessageContactOrg) HasDepartment() bool`

HasDepartment returns a boolean if a field has been set.

### GetTitle

`func (o *MessageContactOrg) GetTitle() string`

GetTitle returns the Title field if non-nil, zero value otherwise.

### GetTitleOk

`func (o *MessageContactOrg) GetTitleOk() (*string, bool)`

GetTitleOk returns a tuple with the Title field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTitle

`func (o *MessageContactOrg) SetTitle(v string)`

SetTitle sets Title field to given value.

### HasTitle

`func (o *MessageContactOrg) HasTitle() bool`

HasTitle returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


