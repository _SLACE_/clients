# ContactCreatable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | Profile name. Contacts created automatically out of conversations have messenger profile name here | 
**Timezone** | Pointer to **string** | Contact timezone in IANA format | [optional] 
**Mobile** | Pointer to **string** | Mobile number in E.164 format | [optional] 
**MobileVerified** | Pointer to **bool** | NOT EDITABLE VIA API  Flag to mark mobile number as verified | [optional] 
**FirstName** | Pointer to **string** |  | [optional] 
**LastName** | Pointer to **string** |  | [optional] 
**Email** | Pointer to **string** |  | [optional] 
**Gender** | Pointer to **string** |  | [optional] 
**Birthdate** | Pointer to **string** | Birthdate in format specified in organization settings. Default is yyyy-mm-dd | [optional] 
**MonthOfBirth** | Pointer to **int32** | Overriden with birthdate data if provided | [optional] 
**DayOfBirth** | Pointer to **int32** | Overriden with birthdate data if provided | [optional] 
**Address** | Pointer to [**ContactAddress**](ContactAddress.md) |  | [optional] 
**LoyaltyPoints** | Pointer to **map[string]int32** | NOT EDITABLE VIA API  Stores loyalty points of a contact | [optional] 
**LifetimeLoyaltyPoints** | Pointer to **map[string]int32** | NOT EDITABLE VIA API  Stores lifetime loyalty points of a contact | [optional] 
**Languages** | Pointer to [**ContactLanguages**](ContactLanguages.md) |  | [optional] 
**CareOf** | Pointer to **string** |  | [optional] 
**StrictAttributes** | Pointer to **map[string]interface{}** | JSON with custom properties as defined in organization settings. Format: { [propertyName]: { type: [string|float|bool|date|datetime], value: [propertyValue] } } | [optional] 
**MobileVerifiedAt** | Pointer to **time.Time** | NOT EDITABLE VIA API | [optional] 
**Avatars** | Pointer to [**ContactAvatars**](ContactAvatars.md) |  | [optional] 
**ExternalIds** | Pointer to **map[string]interface{}** | NOT EDITABLE VIA API | [optional] 

## Methods

### NewContactCreatable

`func NewContactCreatable(name string, ) *ContactCreatable`

NewContactCreatable instantiates a new ContactCreatable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactCreatableWithDefaults

`func NewContactCreatableWithDefaults() *ContactCreatable`

NewContactCreatableWithDefaults instantiates a new ContactCreatable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ContactCreatable) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ContactCreatable) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ContactCreatable) SetName(v string)`

SetName sets Name field to given value.


### GetTimezone

`func (o *ContactCreatable) GetTimezone() string`

GetTimezone returns the Timezone field if non-nil, zero value otherwise.

### GetTimezoneOk

`func (o *ContactCreatable) GetTimezoneOk() (*string, bool)`

GetTimezoneOk returns a tuple with the Timezone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimezone

`func (o *ContactCreatable) SetTimezone(v string)`

SetTimezone sets Timezone field to given value.

### HasTimezone

`func (o *ContactCreatable) HasTimezone() bool`

HasTimezone returns a boolean if a field has been set.

### GetMobile

`func (o *ContactCreatable) GetMobile() string`

GetMobile returns the Mobile field if non-nil, zero value otherwise.

### GetMobileOk

`func (o *ContactCreatable) GetMobileOk() (*string, bool)`

GetMobileOk returns a tuple with the Mobile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobile

`func (o *ContactCreatable) SetMobile(v string)`

SetMobile sets Mobile field to given value.

### HasMobile

`func (o *ContactCreatable) HasMobile() bool`

HasMobile returns a boolean if a field has been set.

### GetMobileVerified

`func (o *ContactCreatable) GetMobileVerified() bool`

GetMobileVerified returns the MobileVerified field if non-nil, zero value otherwise.

### GetMobileVerifiedOk

`func (o *ContactCreatable) GetMobileVerifiedOk() (*bool, bool)`

GetMobileVerifiedOk returns a tuple with the MobileVerified field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobileVerified

`func (o *ContactCreatable) SetMobileVerified(v bool)`

SetMobileVerified sets MobileVerified field to given value.

### HasMobileVerified

`func (o *ContactCreatable) HasMobileVerified() bool`

HasMobileVerified returns a boolean if a field has been set.

### GetFirstName

`func (o *ContactCreatable) GetFirstName() string`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *ContactCreatable) GetFirstNameOk() (*string, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *ContactCreatable) SetFirstName(v string)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *ContactCreatable) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *ContactCreatable) GetLastName() string`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *ContactCreatable) GetLastNameOk() (*string, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *ContactCreatable) SetLastName(v string)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *ContactCreatable) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetEmail

`func (o *ContactCreatable) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *ContactCreatable) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *ContactCreatable) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *ContactCreatable) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetGender

`func (o *ContactCreatable) GetGender() string`

GetGender returns the Gender field if non-nil, zero value otherwise.

### GetGenderOk

`func (o *ContactCreatable) GetGenderOk() (*string, bool)`

GetGenderOk returns a tuple with the Gender field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGender

`func (o *ContactCreatable) SetGender(v string)`

SetGender sets Gender field to given value.

### HasGender

`func (o *ContactCreatable) HasGender() bool`

HasGender returns a boolean if a field has been set.

### GetBirthdate

`func (o *ContactCreatable) GetBirthdate() string`

GetBirthdate returns the Birthdate field if non-nil, zero value otherwise.

### GetBirthdateOk

`func (o *ContactCreatable) GetBirthdateOk() (*string, bool)`

GetBirthdateOk returns a tuple with the Birthdate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBirthdate

`func (o *ContactCreatable) SetBirthdate(v string)`

SetBirthdate sets Birthdate field to given value.

### HasBirthdate

`func (o *ContactCreatable) HasBirthdate() bool`

HasBirthdate returns a boolean if a field has been set.

### GetMonthOfBirth

`func (o *ContactCreatable) GetMonthOfBirth() int32`

GetMonthOfBirth returns the MonthOfBirth field if non-nil, zero value otherwise.

### GetMonthOfBirthOk

`func (o *ContactCreatable) GetMonthOfBirthOk() (*int32, bool)`

GetMonthOfBirthOk returns a tuple with the MonthOfBirth field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMonthOfBirth

`func (o *ContactCreatable) SetMonthOfBirth(v int32)`

SetMonthOfBirth sets MonthOfBirth field to given value.

### HasMonthOfBirth

`func (o *ContactCreatable) HasMonthOfBirth() bool`

HasMonthOfBirth returns a boolean if a field has been set.

### GetDayOfBirth

`func (o *ContactCreatable) GetDayOfBirth() int32`

GetDayOfBirth returns the DayOfBirth field if non-nil, zero value otherwise.

### GetDayOfBirthOk

`func (o *ContactCreatable) GetDayOfBirthOk() (*int32, bool)`

GetDayOfBirthOk returns a tuple with the DayOfBirth field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDayOfBirth

`func (o *ContactCreatable) SetDayOfBirth(v int32)`

SetDayOfBirth sets DayOfBirth field to given value.

### HasDayOfBirth

`func (o *ContactCreatable) HasDayOfBirth() bool`

HasDayOfBirth returns a boolean if a field has been set.

### GetAddress

`func (o *ContactCreatable) GetAddress() ContactAddress`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *ContactCreatable) GetAddressOk() (*ContactAddress, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *ContactCreatable) SetAddress(v ContactAddress)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *ContactCreatable) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetLoyaltyPoints

`func (o *ContactCreatable) GetLoyaltyPoints() map[string]int32`

GetLoyaltyPoints returns the LoyaltyPoints field if non-nil, zero value otherwise.

### GetLoyaltyPointsOk

`func (o *ContactCreatable) GetLoyaltyPointsOk() (*map[string]int32, bool)`

GetLoyaltyPointsOk returns a tuple with the LoyaltyPoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLoyaltyPoints

`func (o *ContactCreatable) SetLoyaltyPoints(v map[string]int32)`

SetLoyaltyPoints sets LoyaltyPoints field to given value.

### HasLoyaltyPoints

`func (o *ContactCreatable) HasLoyaltyPoints() bool`

HasLoyaltyPoints returns a boolean if a field has been set.

### GetLifetimeLoyaltyPoints

`func (o *ContactCreatable) GetLifetimeLoyaltyPoints() map[string]int32`

GetLifetimeLoyaltyPoints returns the LifetimeLoyaltyPoints field if non-nil, zero value otherwise.

### GetLifetimeLoyaltyPointsOk

`func (o *ContactCreatable) GetLifetimeLoyaltyPointsOk() (*map[string]int32, bool)`

GetLifetimeLoyaltyPointsOk returns a tuple with the LifetimeLoyaltyPoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLifetimeLoyaltyPoints

`func (o *ContactCreatable) SetLifetimeLoyaltyPoints(v map[string]int32)`

SetLifetimeLoyaltyPoints sets LifetimeLoyaltyPoints field to given value.

### HasLifetimeLoyaltyPoints

`func (o *ContactCreatable) HasLifetimeLoyaltyPoints() bool`

HasLifetimeLoyaltyPoints returns a boolean if a field has been set.

### GetLanguages

`func (o *ContactCreatable) GetLanguages() ContactLanguages`

GetLanguages returns the Languages field if non-nil, zero value otherwise.

### GetLanguagesOk

`func (o *ContactCreatable) GetLanguagesOk() (*ContactLanguages, bool)`

GetLanguagesOk returns a tuple with the Languages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguages

`func (o *ContactCreatable) SetLanguages(v ContactLanguages)`

SetLanguages sets Languages field to given value.

### HasLanguages

`func (o *ContactCreatable) HasLanguages() bool`

HasLanguages returns a boolean if a field has been set.

### GetCareOf

`func (o *ContactCreatable) GetCareOf() string`

GetCareOf returns the CareOf field if non-nil, zero value otherwise.

### GetCareOfOk

`func (o *ContactCreatable) GetCareOfOk() (*string, bool)`

GetCareOfOk returns a tuple with the CareOf field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCareOf

`func (o *ContactCreatable) SetCareOf(v string)`

SetCareOf sets CareOf field to given value.

### HasCareOf

`func (o *ContactCreatable) HasCareOf() bool`

HasCareOf returns a boolean if a field has been set.

### GetStrictAttributes

`func (o *ContactCreatable) GetStrictAttributes() map[string]interface{}`

GetStrictAttributes returns the StrictAttributes field if non-nil, zero value otherwise.

### GetStrictAttributesOk

`func (o *ContactCreatable) GetStrictAttributesOk() (*map[string]interface{}, bool)`

GetStrictAttributesOk returns a tuple with the StrictAttributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStrictAttributes

`func (o *ContactCreatable) SetStrictAttributes(v map[string]interface{})`

SetStrictAttributes sets StrictAttributes field to given value.

### HasStrictAttributes

`func (o *ContactCreatable) HasStrictAttributes() bool`

HasStrictAttributes returns a boolean if a field has been set.

### GetMobileVerifiedAt

`func (o *ContactCreatable) GetMobileVerifiedAt() time.Time`

GetMobileVerifiedAt returns the MobileVerifiedAt field if non-nil, zero value otherwise.

### GetMobileVerifiedAtOk

`func (o *ContactCreatable) GetMobileVerifiedAtOk() (*time.Time, bool)`

GetMobileVerifiedAtOk returns a tuple with the MobileVerifiedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMobileVerifiedAt

`func (o *ContactCreatable) SetMobileVerifiedAt(v time.Time)`

SetMobileVerifiedAt sets MobileVerifiedAt field to given value.

### HasMobileVerifiedAt

`func (o *ContactCreatable) HasMobileVerifiedAt() bool`

HasMobileVerifiedAt returns a boolean if a field has been set.

### GetAvatars

`func (o *ContactCreatable) GetAvatars() ContactAvatars`

GetAvatars returns the Avatars field if non-nil, zero value otherwise.

### GetAvatarsOk

`func (o *ContactCreatable) GetAvatarsOk() (*ContactAvatars, bool)`

GetAvatarsOk returns a tuple with the Avatars field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAvatars

`func (o *ContactCreatable) SetAvatars(v ContactAvatars)`

SetAvatars sets Avatars field to given value.

### HasAvatars

`func (o *ContactCreatable) HasAvatars() bool`

HasAvatars returns a boolean if a field has been set.

### GetExternalIds

`func (o *ContactCreatable) GetExternalIds() map[string]interface{}`

GetExternalIds returns the ExternalIds field if non-nil, zero value otherwise.

### GetExternalIdsOk

`func (o *ContactCreatable) GetExternalIdsOk() (*map[string]interface{}, bool)`

GetExternalIdsOk returns a tuple with the ExternalIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExternalIds

`func (o *ContactCreatable) SetExternalIds(v map[string]interface{})`

SetExternalIds sets ExternalIds field to given value.

### HasExternalIds

`func (o *ContactCreatable) HasExternalIds() bool`

HasExternalIds returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


