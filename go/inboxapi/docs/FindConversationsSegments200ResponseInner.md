# FindConversationsSegments200ResponseInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Segments** | **[]string** |  | 

## Methods

### NewFindConversationsSegments200ResponseInner

`func NewFindConversationsSegments200ResponseInner(id string, segments []string, ) *FindConversationsSegments200ResponseInner`

NewFindConversationsSegments200ResponseInner instantiates a new FindConversationsSegments200ResponseInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFindConversationsSegments200ResponseInnerWithDefaults

`func NewFindConversationsSegments200ResponseInnerWithDefaults() *FindConversationsSegments200ResponseInner`

NewFindConversationsSegments200ResponseInnerWithDefaults instantiates a new FindConversationsSegments200ResponseInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *FindConversationsSegments200ResponseInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *FindConversationsSegments200ResponseInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *FindConversationsSegments200ResponseInner) SetId(v string)`

SetId sets Id field to given value.


### GetSegments

`func (o *FindConversationsSegments200ResponseInner) GetSegments() []string`

GetSegments returns the Segments field if non-nil, zero value otherwise.

### GetSegmentsOk

`func (o *FindConversationsSegments200ResponseInner) GetSegmentsOk() (*[]string, bool)`

GetSegmentsOk returns a tuple with the Segments field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSegments

`func (o *FindConversationsSegments200ResponseInner) SetSegments(v []string)`

SetSegments sets Segments field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


