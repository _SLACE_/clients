# WhatsAppTemplateComponentType

## Enum


* `WhatsAppTemplateComponentTypeHeader` (value: `"HEADER"`)

* `WhatsAppTemplateComponentTypeBody` (value: `"BODY"`)

* `WhatsAppTemplateComponentTypeFooter` (value: `"FOOTER"`)

* `WhatsAppTemplateComponentTypeButtons` (value: `"BUTTONS"`)

* `WhatsAppTemplateComponentTypeCarousel` (value: `"CAROUSEL"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


