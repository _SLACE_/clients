# WhatsAppTemplateButton

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | [**WhatsAppTemplateButtonType**](WhatsAppTemplateButtonType.md) |  | 
**Text** | **string** |  | 
**Url** | Pointer to **string** |  | [optional] 
**PhoneNumber** | Pointer to **string** |  | [optional] 
**Example** | Pointer to **[]string** |  | [optional] 

## Methods

### NewWhatsAppTemplateButton

`func NewWhatsAppTemplateButton(type_ WhatsAppTemplateButtonType, text string, ) *WhatsAppTemplateButton`

NewWhatsAppTemplateButton instantiates a new WhatsAppTemplateButton object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateButtonWithDefaults

`func NewWhatsAppTemplateButtonWithDefaults() *WhatsAppTemplateButton`

NewWhatsAppTemplateButtonWithDefaults instantiates a new WhatsAppTemplateButton object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *WhatsAppTemplateButton) GetType() WhatsAppTemplateButtonType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WhatsAppTemplateButton) GetTypeOk() (*WhatsAppTemplateButtonType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WhatsAppTemplateButton) SetType(v WhatsAppTemplateButtonType)`

SetType sets Type field to given value.


### GetText

`func (o *WhatsAppTemplateButton) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *WhatsAppTemplateButton) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *WhatsAppTemplateButton) SetText(v string)`

SetText sets Text field to given value.


### GetUrl

`func (o *WhatsAppTemplateButton) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *WhatsAppTemplateButton) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *WhatsAppTemplateButton) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *WhatsAppTemplateButton) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetPhoneNumber

`func (o *WhatsAppTemplateButton) GetPhoneNumber() string`

GetPhoneNumber returns the PhoneNumber field if non-nil, zero value otherwise.

### GetPhoneNumberOk

`func (o *WhatsAppTemplateButton) GetPhoneNumberOk() (*string, bool)`

GetPhoneNumberOk returns a tuple with the PhoneNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhoneNumber

`func (o *WhatsAppTemplateButton) SetPhoneNumber(v string)`

SetPhoneNumber sets PhoneNumber field to given value.

### HasPhoneNumber

`func (o *WhatsAppTemplateButton) HasPhoneNumber() bool`

HasPhoneNumber returns a boolean if a field has been set.

### GetExample

`func (o *WhatsAppTemplateButton) GetExample() []string`

GetExample returns the Example field if non-nil, zero value otherwise.

### GetExampleOk

`func (o *WhatsAppTemplateButton) GetExampleOk() (*[]string, bool)`

GetExampleOk returns a tuple with the Example field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExample

`func (o *WhatsAppTemplateButton) SetExample(v []string)`

SetExample sets Example field to given value.

### HasExample

`func (o *WhatsAppTemplateButton) HasExample() bool`

HasExample returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


