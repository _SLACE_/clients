# InboxSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WebPush** | [**InboxSettingsWebPush**](InboxSettingsWebPush.md) |  | 
**MessageParameters** | **[]string** | List of parameters usable in message and template texts | 
**MessengerLimits** | [**[]InboxSettingsMessengerLimitsInner**](InboxSettingsMessengerLimitsInner.md) |  | 

## Methods

### NewInboxSettings

`func NewInboxSettings(webPush InboxSettingsWebPush, messageParameters []string, messengerLimits []InboxSettingsMessengerLimitsInner, ) *InboxSettings`

NewInboxSettings instantiates a new InboxSettings object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInboxSettingsWithDefaults

`func NewInboxSettingsWithDefaults() *InboxSettings`

NewInboxSettingsWithDefaults instantiates a new InboxSettings object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWebPush

`func (o *InboxSettings) GetWebPush() InboxSettingsWebPush`

GetWebPush returns the WebPush field if non-nil, zero value otherwise.

### GetWebPushOk

`func (o *InboxSettings) GetWebPushOk() (*InboxSettingsWebPush, bool)`

GetWebPushOk returns a tuple with the WebPush field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWebPush

`func (o *InboxSettings) SetWebPush(v InboxSettingsWebPush)`

SetWebPush sets WebPush field to given value.


### GetMessageParameters

`func (o *InboxSettings) GetMessageParameters() []string`

GetMessageParameters returns the MessageParameters field if non-nil, zero value otherwise.

### GetMessageParametersOk

`func (o *InboxSettings) GetMessageParametersOk() (*[]string, bool)`

GetMessageParametersOk returns a tuple with the MessageParameters field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageParameters

`func (o *InboxSettings) SetMessageParameters(v []string)`

SetMessageParameters sets MessageParameters field to given value.


### GetMessengerLimits

`func (o *InboxSettings) GetMessengerLimits() []InboxSettingsMessengerLimitsInner`

GetMessengerLimits returns the MessengerLimits field if non-nil, zero value otherwise.

### GetMessengerLimitsOk

`func (o *InboxSettings) GetMessengerLimitsOk() (*[]InboxSettingsMessengerLimitsInner, bool)`

GetMessengerLimitsOk returns a tuple with the MessengerLimits field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerLimits

`func (o *InboxSettings) SetMessengerLimits(v []InboxSettingsMessengerLimitsInner)`

SetMessengerLimits sets MessengerLimits field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


