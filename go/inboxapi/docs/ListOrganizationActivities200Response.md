# ListOrganizationActivities200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Total** | Pointer to **int32** |  | [optional] 
**Results** | Pointer to [**[]Activity**](Activity.md) |  | [optional] 

## Methods

### NewListOrganizationActivities200Response

`func NewListOrganizationActivities200Response() *ListOrganizationActivities200Response`

NewListOrganizationActivities200Response instantiates a new ListOrganizationActivities200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListOrganizationActivities200ResponseWithDefaults

`func NewListOrganizationActivities200ResponseWithDefaults() *ListOrganizationActivities200Response`

NewListOrganizationActivities200ResponseWithDefaults instantiates a new ListOrganizationActivities200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotal

`func (o *ListOrganizationActivities200Response) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *ListOrganizationActivities200Response) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *ListOrganizationActivities200Response) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *ListOrganizationActivities200Response) HasTotal() bool`

HasTotal returns a boolean if a field has been set.

### GetResults

`func (o *ListOrganizationActivities200Response) GetResults() []Activity`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *ListOrganizationActivities200Response) GetResultsOk() (*[]Activity, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *ListOrganizationActivities200Response) SetResults(v []Activity)`

SetResults sets Results field to given value.

### HasResults

`func (o *ListOrganizationActivities200Response) HasResults() bool`

HasResults returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


