# WhatsAppTemplateButtonType

## Enum


* `WhatsAppTemplateButtonTypePhoneNumber` (value: `"PHONE_NUMBER"`)

* `WhatsAppTemplateButtonTypeUrl` (value: `"URL"`)

* `WhatsAppTemplateButtonTypeQuickReply` (value: `"QUICK_REPLY"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


