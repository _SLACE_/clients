# SendMessageType

## Enum


* `SEND_MESSAGE_TYPE_TEXT` (value: `"text"`)

* `SEND_MESSAGE_TYPE_IMAGE` (value: `"image"`)

* `SEND_MESSAGE_TYPE_VIDEO` (value: `"video"`)

* `SEND_MESSAGE_TYPE_AUDIO` (value: `"audio"`)

* `SEND_MESSAGE_TYPE_DOCUMENT` (value: `"document"`)

* `SEND_MESSAGE_TYPE_LOCATION` (value: `"location"`)

* `SEND_MESSAGE_TYPE_CONTACT` (value: `"contact"`)

* `SEND_MESSAGE_TYPE_BUTTONS` (value: `"buttons"`)

* `SEND_MESSAGE_TYPE_LIST` (value: `"list"`)

* `SEND_MESSAGE_TYPE_ORDER_DETAILS` (value: `"order_details"`)

* `SEND_MESSAGE_TYPE_ORDER_STATUS` (value: `"order_status"`)

* `SEND_MESSAGE_TYPE_REQUEST_LOCATION` (value: `"request_location"`)

* `SEND_MESSAGE_TYPE_REQUEST_PHONE_NUMBER` (value: `"request_phone_number"`)

* `SEND_MESSAGE_TYPE_REMOVE_KEYBOARD` (value: `"remove_keyboard"`)

* `SEND_MESSAGE_TYPE_MEDIA_CAROUSEL` (value: `"media_carousel"`)

* `SEND_MESSAGE_TYPE_REACTION` (value: `"reaction"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


