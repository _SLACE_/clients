# ConversationsList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | Pointer to [**[]Conversation**](Conversation.md) |  | [optional] 
**Total** | Pointer to **int32** |  | [optional] 

## Methods

### NewConversationsList

`func NewConversationsList() *ConversationsList`

NewConversationsList instantiates a new ConversationsList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConversationsListWithDefaults

`func NewConversationsListWithDefaults() *ConversationsList`

NewConversationsListWithDefaults instantiates a new ConversationsList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *ConversationsList) GetResults() []Conversation`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *ConversationsList) GetResultsOk() (*[]Conversation, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *ConversationsList) SetResults(v []Conversation)`

SetResults sets Results field to given value.

### HasResults

`func (o *ConversationsList) HasResults() bool`

HasResults returns a boolean if a field has been set.

### GetTotal

`func (o *ConversationsList) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *ConversationsList) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *ConversationsList) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *ConversationsList) HasTotal() bool`

HasTotal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


