# GetActivitiesReport200ResponseInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Value** | **int32** |  | 
**Channel** | Pointer to **string** |  | [optional] 
**Date** | Pointer to **string** |  | [optional] 
**Hour** | Pointer to **string** |  | [optional] 
**Week** | Pointer to **string** |  | [optional] 
**Weekday** | Pointer to **string** |  | [optional] 

## Methods

### NewGetActivitiesReport200ResponseInner

`func NewGetActivitiesReport200ResponseInner(value int32, ) *GetActivitiesReport200ResponseInner`

NewGetActivitiesReport200ResponseInner instantiates a new GetActivitiesReport200ResponseInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGetActivitiesReport200ResponseInnerWithDefaults

`func NewGetActivitiesReport200ResponseInnerWithDefaults() *GetActivitiesReport200ResponseInner`

NewGetActivitiesReport200ResponseInnerWithDefaults instantiates a new GetActivitiesReport200ResponseInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetValue

`func (o *GetActivitiesReport200ResponseInner) GetValue() int32`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *GetActivitiesReport200ResponseInner) GetValueOk() (*int32, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *GetActivitiesReport200ResponseInner) SetValue(v int32)`

SetValue sets Value field to given value.


### GetChannel

`func (o *GetActivitiesReport200ResponseInner) GetChannel() string`

GetChannel returns the Channel field if non-nil, zero value otherwise.

### GetChannelOk

`func (o *GetActivitiesReport200ResponseInner) GetChannelOk() (*string, bool)`

GetChannelOk returns a tuple with the Channel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChannel

`func (o *GetActivitiesReport200ResponseInner) SetChannel(v string)`

SetChannel sets Channel field to given value.

### HasChannel

`func (o *GetActivitiesReport200ResponseInner) HasChannel() bool`

HasChannel returns a boolean if a field has been set.

### GetDate

`func (o *GetActivitiesReport200ResponseInner) GetDate() string`

GetDate returns the Date field if non-nil, zero value otherwise.

### GetDateOk

`func (o *GetActivitiesReport200ResponseInner) GetDateOk() (*string, bool)`

GetDateOk returns a tuple with the Date field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDate

`func (o *GetActivitiesReport200ResponseInner) SetDate(v string)`

SetDate sets Date field to given value.

### HasDate

`func (o *GetActivitiesReport200ResponseInner) HasDate() bool`

HasDate returns a boolean if a field has been set.

### GetHour

`func (o *GetActivitiesReport200ResponseInner) GetHour() string`

GetHour returns the Hour field if non-nil, zero value otherwise.

### GetHourOk

`func (o *GetActivitiesReport200ResponseInner) GetHourOk() (*string, bool)`

GetHourOk returns a tuple with the Hour field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHour

`func (o *GetActivitiesReport200ResponseInner) SetHour(v string)`

SetHour sets Hour field to given value.

### HasHour

`func (o *GetActivitiesReport200ResponseInner) HasHour() bool`

HasHour returns a boolean if a field has been set.

### GetWeek

`func (o *GetActivitiesReport200ResponseInner) GetWeek() string`

GetWeek returns the Week field if non-nil, zero value otherwise.

### GetWeekOk

`func (o *GetActivitiesReport200ResponseInner) GetWeekOk() (*string, bool)`

GetWeekOk returns a tuple with the Week field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWeek

`func (o *GetActivitiesReport200ResponseInner) SetWeek(v string)`

SetWeek sets Week field to given value.

### HasWeek

`func (o *GetActivitiesReport200ResponseInner) HasWeek() bool`

HasWeek returns a boolean if a field has been set.

### GetWeekday

`func (o *GetActivitiesReport200ResponseInner) GetWeekday() string`

GetWeekday returns the Weekday field if non-nil, zero value otherwise.

### GetWeekdayOk

`func (o *GetActivitiesReport200ResponseInner) GetWeekdayOk() (*string, bool)`

GetWeekdayOk returns a tuple with the Weekday field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWeekday

`func (o *GetActivitiesReport200ResponseInner) SetWeekday(v string)`

SetWeekday sets Weekday field to given value.

### HasWeekday

`func (o *GetActivitiesReport200ResponseInner) HasWeekday() bool`

HasWeekday returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


