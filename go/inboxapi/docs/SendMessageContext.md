# SendMessageContext

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TransactionId** | Pointer to **string** |  | [optional] 
**InteractionId** | Pointer to **string** |  | [optional] 
**ActionId** | Pointer to **string** |  | [optional] 
**ScriptId** | Pointer to **string** |  | [optional] 
**CrId** | Pointer to **string** | Canned response id | [optional] 
**Bgr** | Pointer to **bool** |  | [optional] 

## Methods

### NewSendMessageContext

`func NewSendMessageContext() *SendMessageContext`

NewSendMessageContext instantiates a new SendMessageContext object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSendMessageContextWithDefaults

`func NewSendMessageContextWithDefaults() *SendMessageContext`

NewSendMessageContextWithDefaults instantiates a new SendMessageContext object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTransactionId

`func (o *SendMessageContext) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *SendMessageContext) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *SendMessageContext) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.

### HasTransactionId

`func (o *SendMessageContext) HasTransactionId() bool`

HasTransactionId returns a boolean if a field has been set.

### GetInteractionId

`func (o *SendMessageContext) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *SendMessageContext) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *SendMessageContext) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *SendMessageContext) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetActionId

`func (o *SendMessageContext) GetActionId() string`

GetActionId returns the ActionId field if non-nil, zero value otherwise.

### GetActionIdOk

`func (o *SendMessageContext) GetActionIdOk() (*string, bool)`

GetActionIdOk returns a tuple with the ActionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActionId

`func (o *SendMessageContext) SetActionId(v string)`

SetActionId sets ActionId field to given value.

### HasActionId

`func (o *SendMessageContext) HasActionId() bool`

HasActionId returns a boolean if a field has been set.

### GetScriptId

`func (o *SendMessageContext) GetScriptId() string`

GetScriptId returns the ScriptId field if non-nil, zero value otherwise.

### GetScriptIdOk

`func (o *SendMessageContext) GetScriptIdOk() (*string, bool)`

GetScriptIdOk returns a tuple with the ScriptId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScriptId

`func (o *SendMessageContext) SetScriptId(v string)`

SetScriptId sets ScriptId field to given value.

### HasScriptId

`func (o *SendMessageContext) HasScriptId() bool`

HasScriptId returns a boolean if a field has been set.

### GetCrId

`func (o *SendMessageContext) GetCrId() string`

GetCrId returns the CrId field if non-nil, zero value otherwise.

### GetCrIdOk

`func (o *SendMessageContext) GetCrIdOk() (*string, bool)`

GetCrIdOk returns a tuple with the CrId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCrId

`func (o *SendMessageContext) SetCrId(v string)`

SetCrId sets CrId field to given value.

### HasCrId

`func (o *SendMessageContext) HasCrId() bool`

HasCrId returns a boolean if a field has been set.

### GetBgr

`func (o *SendMessageContext) GetBgr() bool`

GetBgr returns the Bgr field if non-nil, zero value otherwise.

### GetBgrOk

`func (o *SendMessageContext) GetBgrOk() (*bool, bool)`

GetBgrOk returns a tuple with the Bgr field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBgr

`func (o *SendMessageContext) SetBgr(v bool)`

SetBgr sets Bgr field to given value.

### HasBgr

`func (o *SendMessageContext) HasBgr() bool`

HasBgr returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


