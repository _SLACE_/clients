# MessageOrderDetailsExpiration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Seconds** | **int32** |  | 
**Description** | **string** |  | 

## Methods

### NewMessageOrderDetailsExpiration

`func NewMessageOrderDetailsExpiration(seconds int32, description string, ) *MessageOrderDetailsExpiration`

NewMessageOrderDetailsExpiration instantiates a new MessageOrderDetailsExpiration object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageOrderDetailsExpirationWithDefaults

`func NewMessageOrderDetailsExpirationWithDefaults() *MessageOrderDetailsExpiration`

NewMessageOrderDetailsExpirationWithDefaults instantiates a new MessageOrderDetailsExpiration object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSeconds

`func (o *MessageOrderDetailsExpiration) GetSeconds() int32`

GetSeconds returns the Seconds field if non-nil, zero value otherwise.

### GetSecondsOk

`func (o *MessageOrderDetailsExpiration) GetSecondsOk() (*int32, bool)`

GetSecondsOk returns a tuple with the Seconds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSeconds

`func (o *MessageOrderDetailsExpiration) SetSeconds(v int32)`

SetSeconds sets Seconds field to given value.


### GetDescription

`func (o *MessageOrderDetailsExpiration) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *MessageOrderDetailsExpiration) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *MessageOrderDetailsExpiration) SetDescription(v string)`

SetDescription sets Description field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


