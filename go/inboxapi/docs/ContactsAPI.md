# \ContactsAPI

All URIs are relative to *https://api.slace.com/inbox*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddConsent**](ContactsAPI.md#AddConsent) | **Put** /organizations/{organization_id}/contacts/{contact_id}/consents/add | Add consent for contact
[**AddContactCoupon**](ContactsAPI.md#AddContactCoupon) | **Post** /organizations/{organization_id}/contacts/{contact_id}/coupons | Add contact coupon
[**AddContactSegments**](ContactsAPI.md#AddContactSegments) | **Post** /organizations/{organization_id}/contacts/{contact_id}/segments | Add segments
[**AddExternalId**](ContactsAPI.md#AddExternalId) | **Post** /organizations/{organization_id}/contacts/{contact_id}/external | Add external system ID
[**AddLanguage**](ContactsAPI.md#AddLanguage) | **Post** /organizations/{organization_id}/contacts/{contact_id}/languages | Add language
[**AddSegment**](ContactsAPI.md#AddSegment) | **Post** /organizations/{organization_id}/contacts/{contact_id}/segment | Add segment
[**AddSegmentGroup**](ContactsAPI.md#AddSegmentGroup) | **Post** /organizations/{organization_id}/contacts-segment-group | Add segment group
[**AttachContactComChannel**](ContactsAPI.md#AttachContactComChannel) | **Post** /organizations/{organization_id}/contacts/{contact_id}/com-channels/{com_chan_id} | Attach communication channel
[**AttachPlatformContactComChannel**](ContactsAPI.md#AttachPlatformContactComChannel) | **Post** /channels/{channel_id}/contacts/{contact_id}/platform-com-channels/{platform_com_chan_id} | Attach platform communication channel
[**CreateContact**](ContactsAPI.md#CreateContact) | **Post** /organizations/{organization_id}/contacts | Create new contact
[**CreateNewActivity**](ContactsAPI.md#CreateNewActivity) | **Post** /organizations/{organization_id}/contacts/{contact_id}/activities | Create new activity
[**DeleteContact**](ContactsAPI.md#DeleteContact) | **Delete** /organizations/{organization_id}/contacts/{contact_id} | Delete contact
[**DeleteContactsGroup**](ContactsAPI.md#DeleteContactsGroup) | **Delete** /organizations/{organization_id}/contacts-group | Delete contacts by ID
[**DeleteExternalId**](ContactsAPI.md#DeleteExternalId) | **Delete** /organizations/{organization_id}/contacts/{contact_id}/external | Delete external ID
[**DeleteLanguage**](ContactsAPI.md#DeleteLanguage) | **Delete** /organizations/{organization_id}/contacts/{contact_id}/languages/{source_id} | Delete language by source
[**DeleteOrganizationsOrganizationIdContactsContactIdSegment**](ContactsAPI.md#DeleteOrganizationsOrganizationIdContactsContactIdSegment) | **Delete** /organizations/{organization_id}/contacts/{contact_id}/segment | Delete segment
[**DeletePrimaryAvatar**](ContactsAPI.md#DeletePrimaryAvatar) | **Delete** /organizations/{organization_id}/contacts/{contact_id}/avatars/primary | Delete primary avatar
[**DeleteSegmentGroup**](ContactsAPI.md#DeleteSegmentGroup) | **Delete** /organizations/{organization_id}/contacts-segment-group | Delete segment group
[**DetachContactComChannel**](ContactsAPI.md#DetachContactComChannel) | **Delete** /organizations/{organization_id}/contacts/{contact_id}/com-channels/{com_chan_id} | Detach communication channel
[**DetachPlatformContactComChannel**](ContactsAPI.md#DetachPlatformContactComChannel) | **Delete** /channels/{channel_id}/contacts/{contact_id}/platform-com-channels/{platform_com_chan_id} | Detach platform communication channel
[**ExportContacts**](ContactsAPI.md#ExportContacts) | **Get** /organizations/{organization_id}/contacts-export | Export contacts
[**ExportContactsAttributes**](ContactsAPI.md#ExportContactsAttributes) | **Get** /organizations/{organization_id}/contacts-strict-attributes-export | Export contacts attributes
[**GetActivitiesReport**](ContactsAPI.md#GetActivitiesReport) | **Get** /organizations/{organization_id}/activities-report | Get activities report
[**GetChannelActivitiesCount**](ContactsAPI.md#GetChannelActivitiesCount) | **Get** /organizations/{organization_id}/channels/{channelId}/activities-count | Get channel activities count
[**GetChannelActivitiesGrowth**](ContactsAPI.md#GetChannelActivitiesGrowth) | **Get** /organizations/{organization_id}/channels/{channelId}/activities-growth | Get channel activities growth
[**GetChannelActivitiesQualMetrics**](ContactsAPI.md#GetChannelActivitiesQualMetrics) | **Get** /organizations/{organization_id}/channels/{channelId}/activities-qual | Get channel activities qual metrics
[**GetContact**](ContactsAPI.md#GetContact) | **Get** /organizations/{organization_id}/contacts/{contact_id} | Get contact
[**GetContactActivity**](ContactsAPI.md#GetContactActivity) | **Get** /organizations/{organization_id}/contacts/{contact_id}/activities/{activity_id} | Get single contact activity
[**GetContactGLobal**](ContactsAPI.md#GetContactGLobal) | **Get** /contacts/{contact_id} | Get contact (Global)
[**GetContactsGrowth**](ContactsAPI.md#GetContactsGrowth) | **Get** /organizations/{organization_id}/contacts-growth | Get contact growth amount
[**GetContactsQual**](ContactsAPI.md#GetContactsQual) | **Get** /organizations/{organization_id}/contacts-qual | Get contact qual metrics
[**GetContactsReport**](ContactsAPI.md#GetContactsReport) | **Get** /organizations/{organization_id}/contacts-report | Get contacts report
[**GetCoupons**](ContactsAPI.md#GetCoupons) | **Get** /organizations/{organization_id}/contacts-coupons/{code} | Get coupons by ID
[**GetLanguages**](ContactsAPI.md#GetLanguages) | **Get** /organizations/{organization_id}/contacts/{contact_id}/languages | Get languages
[**GetOrganizationActivitiesCount**](ContactsAPI.md#GetOrganizationActivitiesCount) | **Get** /organizations/{organization_id}/activities-count | Get organization activities count
[**GetOrganizationActivitiesGrowth**](ContactsAPI.md#GetOrganizationActivitiesGrowth) | **Get** /organizations/{organization_id}/activities-growth | Get organization activities growth
[**GetOrganizationActivitiesQualMetrics**](ContactsAPI.md#GetOrganizationActivitiesQualMetrics) | **Get** /organizations/{organization_id}/activities-qual | Get organization activities qual metrics
[**GetOrganizationsOrganizationIdContactsKpi**](ContactsAPI.md#GetOrganizationsOrganizationIdContactsKpi) | **Get** /organizations/{organization_id}/contacts-kpi | List KPIs
[**GetOrganizationsOrganizationIdContactsLabels**](ContactsAPI.md#GetOrganizationsOrganizationIdContactsLabels) | **Get** /organizations/{organization_id}/contacts-labels | List organization labels
[**ImportContacts**](ContactsAPI.md#ImportContacts) | **Post** /organizations/{organization_id}/contacts-import | Import contacts
[**ImportContactsAttributes**](ContactsAPI.md#ImportContactsAttributes) | **Post** /organizations/{organization_id}/contacts-strict-attributes-import | Import contacts attributes
[**ListChannelActivities**](ContactsAPI.md#ListChannelActivities) | **Get** /organizations/{organization_id}/channels/{channelId}/activities | List channel activities
[**ListContactActivities**](ContactsAPI.md#ListContactActivities) | **Get** /organizations/{organization_id}/contacts/{contact_id}/activities | List contact activities
[**ListContactConversations**](ContactsAPI.md#ListContactConversations) | **Get** /organizations/{organization_id}/contacts/{contact_id}/conversations | List contact conversations
[**ListContactCoupons**](ContactsAPI.md#ListContactCoupons) | **Get** /organizations/{organization_id}/contacts/{contact_id}/coupons | List contact coupons
[**ListContactFiles**](ContactsAPI.md#ListContactFiles) | **Get** /organizations/{organization_id}/contacts/{contact_id}/files | List contact files
[**ListContactInteractions**](ContactsAPI.md#ListContactInteractions) | **Get** /organizations/{organization_id}/contacts/{contact_id}/interactions | List contact interactions
[**ListContactLabels**](ContactsAPI.md#ListContactLabels) | **Get** /organizations/{organization_id}/contacts/{contact_id}/labels | List contact labels
[**ListContactSegments**](ContactsAPI.md#ListContactSegments) | **Get** /organizations/{organization_id}/contacts/{contact_id}/segments | List segments
[**ListContacts**](ContactsAPI.md#ListContacts) | **Get** /organizations/{organization_id}/contacts | List contacts
[**ListContactsGroup**](ContactsAPI.md#ListContactsGroup) | **Get** /organizations/{organization_id}/contacts-group | List contacts by IDs
[**ListLanguages**](ContactsAPI.md#ListLanguages) | **Get** /organizations/{organization_id}/contacts-languages | List languages
[**ListOrganizationActivities**](ContactsAPI.md#ListOrganizationActivities) | **Get** /organizations/{organization_id}/activities | List organization activities
[**ListSegments**](ContactsAPI.md#ListSegments) | **Get** /organizations/{organization_id}/contacts-segments | List organization segments
[**PatchActivity**](ContactsAPI.md#PatchActivity) | **Patch** /organizations/{organization_id}/contacts/{contact_id}/activities/{activity_id} | Patch activity
[**PatchContact**](ContactsAPI.md#PatchContact) | **Patch** /organizations/{organization_id}/contacts/{contact_id} | Patch contact
[**PatchCoupon**](ContactsAPI.md#PatchCoupon) | **Patch** /organizations/{organization_id}/contacts/{contact_id}/coupons/{code} | Patch coupon
[**RevokeConsent**](ContactsAPI.md#RevokeConsent) | **Put** /organizations/{organization_id}/contacts/{contact_id}/consents/revoke | Revoke consent from contact
[**UpdateContact**](ContactsAPI.md#UpdateContact) | **Put** /organizations/{organization_id}/contacts/{contact_id} | Update contact
[**UpdateCoupon**](ContactsAPI.md#UpdateCoupon) | **Put** /organizations/{organization_id}/contacts/{contact_id}/coupons/{code} | Update coupon
[**UploadAvatar**](ContactsAPI.md#UploadAvatar) | **Post** /organizations/{organization_id}/contacts/{contact_id}/avatars | Upload avatar



## AddConsent

> AddConsent(ctx, organizationId, contactId).AddConsentRequest(addConsentRequest).Execute()

Add consent for contact

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    addConsentRequest := *openapiclient.NewAddConsentRequest() // AddConsentRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ContactsAPI.AddConsent(context.Background(), organizationId, contactId).AddConsentRequest(addConsentRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.AddConsent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddConsentRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **addConsentRequest** | [**AddConsentRequest**](AddConsentRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddContactCoupon

> Coupon AddContactCoupon(ctx, organizationId, contactId).Coupon(coupon).Execute()

Add contact coupon

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    coupon := *openapiclient.NewCoupon("Id_example", "Source_example", "Code_example", "ContactId_example", "Status_example", time.Now(), "DisplayType_example", "Name_example") // Coupon |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.AddContactCoupon(context.Background(), organizationId, contactId).Coupon(coupon).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.AddContactCoupon``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddContactCoupon`: Coupon
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.AddContactCoupon`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddContactCouponRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **coupon** | [**Coupon**](Coupon.md) |  | 

### Return type

[**Coupon**](Coupon.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddContactSegments

> []Segment AddContactSegments(ctx, organizationId, contactId).SegmentsEditList(segmentsEditList).Execute()

Add segments

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    segmentsEditList := *openapiclient.NewSegmentsEditList() // SegmentsEditList |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.AddContactSegments(context.Background(), organizationId, contactId).SegmentsEditList(segmentsEditList).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.AddContactSegments``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddContactSegments`: []Segment
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.AddContactSegments`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddContactSegmentsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **segmentsEditList** | [**SegmentsEditList**](SegmentsEditList.md) |  | 

### Return type

[**[]Segment**](Segment.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddExternalId

> Contact AddExternalId(ctx, organizationId, contactId).ExternalSystemLink(externalSystemLink).Execute()

Add external system ID

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    externalSystemLink := *openapiclient.NewExternalSystemLink("SystemName_example", "UserLink_example") // ExternalSystemLink |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.AddExternalId(context.Background(), organizationId, contactId).ExternalSystemLink(externalSystemLink).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.AddExternalId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddExternalId`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.AddExternalId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddExternalIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **externalSystemLink** | [**ExternalSystemLink**](ExternalSystemLink.md) |  | 

### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddLanguage

> AddLanguage(ctx, organizationId, contactId).ContactEditableLanguage(contactEditableLanguage).Execute()

Add language

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    contactEditableLanguage := *openapiclient.NewContactEditableLanguage("Source_example", false, "Locale_example") // ContactEditableLanguage |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ContactsAPI.AddLanguage(context.Background(), organizationId, contactId).ContactEditableLanguage(contactEditableLanguage).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.AddLanguage``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddLanguageRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **contactEditableLanguage** | [**ContactEditableLanguage**](ContactEditableLanguage.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddSegment

> Segment AddSegment(ctx, organizationId, contactId).Segment(segment).Execute()

Add segment



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    segment := *openapiclient.NewSegment("SubType_example", "Key_example") // Segment |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.AddSegment(context.Background(), organizationId, contactId).Segment(segment).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.AddSegment``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddSegment`: Segment
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.AddSegment`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddSegmentRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **segment** | [**Segment**](Segment.md) |  | 

### Return type

[**Segment**](Segment.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddSegmentGroup

> AddSegmentGroup(ctx, organizationId).AddSegmentGroupRequest(addSegmentGroupRequest).Execute()

Add segment group



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    addSegmentGroupRequest := *openapiclient.NewAddSegmentGroupRequest("SelectionMode_example") // AddSegmentGroupRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ContactsAPI.AddSegmentGroup(context.Background(), organizationId).AddSegmentGroupRequest(addSegmentGroupRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.AddSegmentGroup``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddSegmentGroupRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **addSegmentGroupRequest** | [**AddSegmentGroupRequest**](AddSegmentGroupRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AttachContactComChannel

> Contact AttachContactComChannel(ctx, organizationId, contactId, comChanId).Execute()

Attach communication channel



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    comChanId := "comChanId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.AttachContactComChannel(context.Background(), organizationId, contactId, comChanId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.AttachContactComChannel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AttachContactComChannel`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.AttachContactComChannel`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 
**comChanId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAttachContactComChannelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AttachPlatformContactComChannel

> Contact AttachPlatformContactComChannel(ctx, channelId, contactId, platformComChanId).Execute()

Attach platform communication channel



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    contactId := "contactId_example" // string | 
    platformComChanId := "platformComChanId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.AttachPlatformContactComChannel(context.Background(), channelId, contactId, platformComChanId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.AttachPlatformContactComChannel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AttachPlatformContactComChannel`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.AttachPlatformContactComChannel`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**contactId** | **string** |  | 
**platformComChanId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAttachPlatformContactComChannelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateContact

> Contact CreateContact(ctx, organizationId).ContactCreatable(contactCreatable).Execute()

Create new contact



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactCreatable := *openapiclient.NewContactCreatable("Name_example") // ContactCreatable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.CreateContact(context.Background(), organizationId).ContactCreatable(contactCreatable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.CreateContact``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateContact`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.CreateContact`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateContactRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **contactCreatable** | [**ContactCreatable**](ContactCreatable.md) |  | 

### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateNewActivity

> Activity CreateNewActivity(ctx, organizationId, contactId).Activity(activity).Execute()

Create new activity

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    activity := *openapiclient.NewActivity("Type_example", int32(123), int32(123)) // Activity |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.CreateNewActivity(context.Background(), organizationId, contactId).Activity(activity).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.CreateNewActivity``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateNewActivity`: Activity
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.CreateNewActivity`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateNewActivityRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **activity** | [**Activity**](Activity.md) |  | 

### Return type

[**Activity**](Activity.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteContact

> GenericResponse DeleteContact(ctx, contactId, organizationId).Execute()

Delete contact



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    contactId := "contactId_example" // string | 
    organizationId := "organizationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.DeleteContact(context.Background(), contactId, organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.DeleteContact``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteContact`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.DeleteContact`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**contactId** | **string** |  | 
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteContactRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteContactsGroup

> DeleteContactsGroup(ctx, organizationId).DeleteContactsGroupRequest(deleteContactsGroupRequest).Execute()

Delete contacts by ID



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    deleteContactsGroupRequest := *openapiclient.NewDeleteContactsGroupRequest() // DeleteContactsGroupRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ContactsAPI.DeleteContactsGroup(context.Background(), organizationId).DeleteContactsGroupRequest(deleteContactsGroupRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.DeleteContactsGroup``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteContactsGroupRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **deleteContactsGroupRequest** | [**DeleteContactsGroupRequest**](DeleteContactsGroupRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteExternalId

> Contact DeleteExternalId(ctx, organizationId, contactId).ExternalSystemLinkDelete(externalSystemLinkDelete).Execute()

Delete external ID



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    externalSystemLinkDelete := *openapiclient.NewExternalSystemLinkDelete("SystemName_example") // ExternalSystemLinkDelete |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.DeleteExternalId(context.Background(), organizationId, contactId).ExternalSystemLinkDelete(externalSystemLinkDelete).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.DeleteExternalId``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteExternalId`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.DeleteExternalId`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteExternalIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **externalSystemLinkDelete** | [**ExternalSystemLinkDelete**](ExternalSystemLinkDelete.md) |  | 

### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteLanguage

> DeleteLanguage(ctx, organizationId, contactId, sourceId).Execute()

Delete language by source



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    sourceId := "sourceId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ContactsAPI.DeleteLanguage(context.Background(), organizationId, contactId, sourceId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.DeleteLanguage``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 
**sourceId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteLanguageRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteOrganizationsOrganizationIdContactsContactIdSegment

> DeleteOrganizationsOrganizationIdContactsContactIdSegment(ctx, organizationId, contactId).DeleteOrganizationsOrganizationIdContactsContactIdSegmentRequest(deleteOrganizationsOrganizationIdContactsContactIdSegmentRequest).Execute()

Delete segment

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    deleteOrganizationsOrganizationIdContactsContactIdSegmentRequest := *openapiclient.NewDeleteOrganizationsOrganizationIdContactsContactIdSegmentRequest("Key_example") // DeleteOrganizationsOrganizationIdContactsContactIdSegmentRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ContactsAPI.DeleteOrganizationsOrganizationIdContactsContactIdSegment(context.Background(), organizationId, contactId).DeleteOrganizationsOrganizationIdContactsContactIdSegmentRequest(deleteOrganizationsOrganizationIdContactsContactIdSegmentRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.DeleteOrganizationsOrganizationIdContactsContactIdSegment``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteOrganizationsOrganizationIdContactsContactIdSegmentRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **deleteOrganizationsOrganizationIdContactsContactIdSegmentRequest** | [**DeleteOrganizationsOrganizationIdContactsContactIdSegmentRequest**](DeleteOrganizationsOrganizationIdContactsContactIdSegmentRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeletePrimaryAvatar

> Contact DeletePrimaryAvatar(ctx, organizationId, contactId).Execute()

Delete primary avatar



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.DeletePrimaryAvatar(context.Background(), organizationId, contactId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.DeletePrimaryAvatar``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeletePrimaryAvatar`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.DeletePrimaryAvatar`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeletePrimaryAvatarRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteSegmentGroup

> DeleteSegmentGroup(ctx, organizationId).DeleteSegmentGroupRequestInner(deleteSegmentGroupRequestInner).Execute()

Delete segment group



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    deleteSegmentGroupRequestInner := []openapiclient.DeleteSegmentGroupRequestInner{*openapiclient.NewDeleteSegmentGroupRequestInner("Key_example", "SelectionMode_example")} // []DeleteSegmentGroupRequestInner |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ContactsAPI.DeleteSegmentGroup(context.Background(), organizationId).DeleteSegmentGroupRequestInner(deleteSegmentGroupRequestInner).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.DeleteSegmentGroup``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteSegmentGroupRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **deleteSegmentGroupRequestInner** | [**[]DeleteSegmentGroupRequestInner**](DeleteSegmentGroupRequestInner.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DetachContactComChannel

> Contact DetachContactComChannel(ctx, organizationId, contactId, comChanId).Execute()

Detach communication channel



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    comChanId := "comChanId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.DetachContactComChannel(context.Background(), organizationId, contactId, comChanId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.DetachContactComChannel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DetachContactComChannel`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.DetachContactComChannel`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 
**comChanId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDetachContactComChannelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DetachPlatformContactComChannel

> Contact DetachPlatformContactComChannel(ctx, channelId, contactId, platformComChanId).Execute()

Detach platform communication channel



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    contactId := "contactId_example" // string | 
    platformComChanId := "platformComChanId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.DetachPlatformContactComChannel(context.Background(), channelId, contactId, platformComChanId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.DetachPlatformContactComChannel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DetachPlatformContactComChannel`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.DetachPlatformContactComChannel`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**contactId** | **string** |  | 
**platformComChanId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDetachPlatformContactComChannelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ExportContacts

> map[string]interface{} ExportContacts(ctx, organizationId).Format(format).Execute()

Export contacts



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    format := "format_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ExportContacts(context.Background(), organizationId).Format(format).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ExportContacts``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ExportContacts`: map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ExportContacts`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiExportContactsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **format** | **string** |  | 

### Return type

**map[string]interface{}**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ExportContactsAttributes

> map[string]interface{} ExportContactsAttributes(ctx, organizationId).Execute()

Export contacts attributes



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ExportContactsAttributes(context.Background(), organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ExportContactsAttributes``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ExportContactsAttributes`: map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ExportContactsAttributes`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiExportContactsAttributesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

**map[string]interface{}**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/csv

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetActivitiesReport

> []GetActivitiesReport200ResponseInner GetActivitiesReport(ctx, organizationId).From(from).To(to).Ts(ts).GroupBy(groupBy).MessengerTypes(messengerTypes).ActivityTypes(activityTypes).OrderBy(orderBy).ChannelIds(channelIds).Tz(tz).Format(format).Execute()

Get activities report

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    from := time.Now() // time.Time | Start date of the report
    to := time.Now() // time.Time | End date of the report
    ts := "ts_example" // string | Timestamp to use in time-based split
    groupBy := []string{"Inner_example"} // []string | channel, date, hour, week, weekday  (optional)
    messengerTypes := []string{"Inner_example"} // []string | WhatsApp, InstagramMessenger, ViberChatbots, GoogleBM, FacebookMessenger, TelegramBot, Webchat,   (optional)
    activityTypes := []string{"Inner_example"} // []string | visit, couponAssignment, couponRedemption, purchase, referral, investment, interaction, goalConversion, transport,   (optional)
    orderBy := []string{"Inner_example"} // []string | same as group_by with '-' prefix for DESC (optional)
    channelIds := []string{"Inner_example"} // []string |  (optional)
    tz := "tz_example" // string | timezone (IANA format: Europe/Berlin etc) (optional)
    format := "format_example" // string | xlsx, csv or json (default) (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetActivitiesReport(context.Background(), organizationId).From(from).To(to).Ts(ts).GroupBy(groupBy).MessengerTypes(messengerTypes).ActivityTypes(activityTypes).OrderBy(orderBy).ChannelIds(channelIds).Tz(tz).Format(format).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetActivitiesReport``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetActivitiesReport`: []GetActivitiesReport200ResponseInner
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetActivitiesReport`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetActivitiesReportRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **from** | **time.Time** | Start date of the report | 
 **to** | **time.Time** | End date of the report | 
 **ts** | **string** | Timestamp to use in time-based split | 
 **groupBy** | **[]string** | channel, date, hour, week, weekday  | 
 **messengerTypes** | **[]string** | WhatsApp, InstagramMessenger, ViberChatbots, GoogleBM, FacebookMessenger, TelegramBot, Webchat,   | 
 **activityTypes** | **[]string** | visit, couponAssignment, couponRedemption, purchase, referral, investment, interaction, goalConversion, transport,   | 
 **orderBy** | **[]string** | same as group_by with &#39;-&#39; prefix for DESC | 
 **channelIds** | **[]string** |  | 
 **tz** | **string** | timezone (IANA format: Europe/Berlin etc) | 
 **format** | **string** | xlsx, csv or json (default) | 

### Return type

[**[]GetActivitiesReport200ResponseInner**](GetActivitiesReport200ResponseInner.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChannelActivitiesCount

> map[string]interface{} GetChannelActivitiesCount(ctx, organizationId, channelId).ContactId(contactId).Execute()

Get channel activities count



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    channelId := "channelId_example" // string | 
    contactId := "contactId_example" // string | count per contact (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetChannelActivitiesCount(context.Background(), organizationId, channelId).ContactId(contactId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetChannelActivitiesCount``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChannelActivitiesCount`: map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetChannelActivitiesCount`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChannelActivitiesCountRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **contactId** | **string** | count per contact | 

### Return type

**map[string]interface{}**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChannelActivitiesGrowth

> ActivitiesGrowthCount GetChannelActivitiesGrowth(ctx, organizationId, channelId).From(from).To(to).LocationIds(locationIds).FromHour(fromHour).ToHour(toHour).Execute()

Get channel activities growth



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    channelId := "channelId_example" // string | 
    from := time.Now() // time.Time |  (optional)
    to := time.Now() // time.Time |  (optional)
    locationIds := []string{"Inner_example"} // []string |  (optional)
    fromHour := int32(56) // int32 |  (optional)
    toHour := int32(56) // int32 |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetChannelActivitiesGrowth(context.Background(), organizationId, channelId).From(from).To(to).LocationIds(locationIds).FromHour(fromHour).ToHour(toHour).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetChannelActivitiesGrowth``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChannelActivitiesGrowth`: ActivitiesGrowthCount
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetChannelActivitiesGrowth`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChannelActivitiesGrowthRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **from** | **time.Time** |  | 
 **to** | **time.Time** |  | 
 **locationIds** | **[]string** |  | 
 **fromHour** | **int32** |  | 
 **toHour** | **int32** |  | 

### Return type

[**ActivitiesGrowthCount**](ActivitiesGrowthCount.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetChannelActivitiesQualMetrics

> ActivitiesQualMetrics GetChannelActivitiesQualMetrics(ctx, organizationId, channelId).Execute()

Get channel activities qual metrics

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    channelId := "channelId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetChannelActivitiesQualMetrics(context.Background(), organizationId, channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetChannelActivitiesQualMetrics``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChannelActivitiesQualMetrics`: ActivitiesQualMetrics
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetChannelActivitiesQualMetrics`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChannelActivitiesQualMetricsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**ActivitiesQualMetrics**](ActivitiesQualMetrics.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetContact

> Contact GetContact(ctx, contactId, organizationId).ExcludedRelations(excludedRelations).IncludedRelations(includedRelations).Execute()

Get contact



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    contactId := "contactId_example" // string | 
    organizationId := "organizationId_example" // string | 
    excludedRelations := []openapiclient.ContactExcludeRelation{openapiclient.ContactExcludeRelation("all")} // []ContactExcludeRelation | param takes priority over included_relations (optional)
    includedRelations := []openapiclient.ContactIncludeRelation{openapiclient.ContactIncludeRelation("Organization")} // []ContactIncludeRelation | if not empty then everything not listed is excluded by default (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetContact(context.Background(), contactId, organizationId).ExcludedRelations(excludedRelations).IncludedRelations(includedRelations).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetContact``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetContact`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetContact`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**contactId** | **string** |  | 
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetContactRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **excludedRelations** | [**[]ContactExcludeRelation**](ContactExcludeRelation.md) | param takes priority over included_relations | 
 **includedRelations** | [**[]ContactIncludeRelation**](ContactIncludeRelation.md) | if not empty then everything not listed is excluded by default | 

### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetContactActivity

> Activity GetContactActivity(ctx, organizationId, contactId, activityId).Execute()

Get single contact activity

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    activityId := "activityId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetContactActivity(context.Background(), organizationId, contactId, activityId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetContactActivity``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetContactActivity`: Activity
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetContactActivity`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 
**activityId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetContactActivityRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**Activity**](Activity.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetContactGLobal

> Contact GetContactGLobal(ctx, contactId).ExcludedRelations(excludedRelations).IncludedRelations(includedRelations).Execute()

Get contact (Global)



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    contactId := "contactId_example" // string | 
    excludedRelations := []openapiclient.ContactExcludeRelation{openapiclient.ContactExcludeRelation("all")} // []ContactExcludeRelation | param takes priority over included_relations (optional)
    includedRelations := []openapiclient.ContactIncludeRelation{openapiclient.ContactIncludeRelation("Organization")} // []ContactIncludeRelation | if not empty then everything not listed is excluded by default (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetContactGLobal(context.Background(), contactId).ExcludedRelations(excludedRelations).IncludedRelations(includedRelations).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetContactGLobal``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetContactGLobal`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetContactGLobal`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetContactGLobalRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **excludedRelations** | [**[]ContactExcludeRelation**](ContactExcludeRelation.md) | param takes priority over included_relations | 
 **includedRelations** | [**[]ContactIncludeRelation**](ContactIncludeRelation.md) | if not empty then everything not listed is excluded by default | 

### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetContactsGrowth

> GrowthCount GetContactsGrowth(ctx, organizationId).From(from).To(to).ChannelId(channelId).FromHour(fromHour).ToHour(toHour).Execute()

Get contact growth amount

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    from := time.Now() // time.Time |  (optional)
    to := time.Now() // time.Time |  (optional)
    channelId := "channelId_example" // string |  (optional)
    fromHour := int32(56) // int32 |  (optional)
    toHour := int32(56) // int32 |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetContactsGrowth(context.Background(), organizationId).From(from).To(to).ChannelId(channelId).FromHour(fromHour).ToHour(toHour).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetContactsGrowth``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetContactsGrowth`: GrowthCount
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetContactsGrowth`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetContactsGrowthRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **from** | **time.Time** |  | 
 **to** | **time.Time** |  | 
 **channelId** | **string** |  | 
 **fromHour** | **int32** |  | 
 **toHour** | **int32** |  | 

### Return type

[**GrowthCount**](GrowthCount.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetContactsQual

> ContactsQualMetrics GetContactsQual(ctx, organizationId).ChannelId(channelId).Execute()

Get contact qual metrics

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    channelId := "channelId_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetContactsQual(context.Background(), organizationId).ChannelId(channelId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetContactsQual``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetContactsQual`: ContactsQualMetrics
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetContactsQual`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetContactsQualRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **channelId** | **string** |  | 

### Return type

[**ContactsQualMetrics**](ContactsQualMetrics.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetContactsReport

> []GetActivitiesReport200ResponseInner GetContactsReport(ctx, organizationId).From(from).To(to).Ts(ts).GroupBy(groupBy).MessengerTypes(messengerTypes).OrderBy(orderBy).ChannelIds(channelIds).Metric(metric).Tz(tz).Format(format).Execute()

Get contacts report

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    from := time.Now() // time.Time | Start date of the report
    to := time.Now() // time.Time | End date of the report
    ts := "ts_example" // string | Timestamp to use in time-based split
    groupBy := []string{"Inner_example"} // []string | channel, date, hour, week, weekday  (optional)
    messengerTypes := []string{"Inner_example"} // []string | WhatsApp, InstagramMessenger, ViberChatbots, GoogleBM, FacebookMessenger, TelegramBot, Webchat,   (optional)
    orderBy := []string{"Inner_example"} // []string | same as group_by with '-' prefix for DESC (optional)
    channelIds := []string{"Inner_example"} // []string |  (optional)
    metric := "metric_example" // string |  Ignored if no time-based grouping selected (date, hour etc). 'new' contacts by default (optional)
    tz := "tz_example" // string | Timezone (IANA format: Europe/Berlin etc) (optional)
    format := "format_example" // string | xlsx, csv or json (default) (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetContactsReport(context.Background(), organizationId).From(from).To(to).Ts(ts).GroupBy(groupBy).MessengerTypes(messengerTypes).OrderBy(orderBy).ChannelIds(channelIds).Metric(metric).Tz(tz).Format(format).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetContactsReport``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetContactsReport`: []GetActivitiesReport200ResponseInner
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetContactsReport`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetContactsReportRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **from** | **time.Time** | Start date of the report | 
 **to** | **time.Time** | End date of the report | 
 **ts** | **string** | Timestamp to use in time-based split | 
 **groupBy** | **[]string** | channel, date, hour, week, weekday  | 
 **messengerTypes** | **[]string** | WhatsApp, InstagramMessenger, ViberChatbots, GoogleBM, FacebookMessenger, TelegramBot, Webchat,   | 
 **orderBy** | **[]string** | same as group_by with &#39;-&#39; prefix for DESC | 
 **channelIds** | **[]string** |  | 
 **metric** | **string** |  Ignored if no time-based grouping selected (date, hour etc). &#39;new&#39; contacts by default | 
 **tz** | **string** | Timezone (IANA format: Europe/Berlin etc) | 
 **format** | **string** | xlsx, csv or json (default) | 

### Return type

[**[]GetActivitiesReport200ResponseInner**](GetActivitiesReport200ResponseInner.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCoupons

> GetCoupons200Response GetCoupons(ctx, organizationId, code).Execute()

Get coupons by ID

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    code := "code_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetCoupons(context.Background(), organizationId, code).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetCoupons``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCoupons`: GetCoupons200Response
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetCoupons`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**code** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetCouponsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**GetCoupons200Response**](GetCoupons200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetLanguages

> ContactLanguages GetLanguages(ctx, organizationId, contactId).Execute()

Get languages

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetLanguages(context.Background(), organizationId, contactId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetLanguages``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetLanguages`: ContactLanguages
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetLanguages`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetLanguagesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**ContactLanguages**](ContactLanguages.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationActivitiesCount

> map[string]interface{} GetOrganizationActivitiesCount(ctx, organizationId).ContactId(contactId).Execute()

Get organization activities count



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetOrganizationActivitiesCount(context.Background(), organizationId).ContactId(contactId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetOrganizationActivitiesCount``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationActivitiesCount`: map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetOrganizationActivitiesCount`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationActivitiesCountRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **contactId** | **string** |  | 

### Return type

**map[string]interface{}**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationActivitiesGrowth

> ActivitiesGrowthCount GetOrganizationActivitiesGrowth(ctx, organizationId).From(from).To(to).LocationIds(locationIds).FromHour(fromHour).ToHour(toHour).Execute()

Get organization activities growth



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    from := time.Now() // time.Time |  (optional)
    to := time.Now() // time.Time |  (optional)
    locationIds := []string{"Inner_example"} // []string |  (optional)
    fromHour := int32(56) // int32 |  (optional)
    toHour := int32(56) // int32 |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetOrganizationActivitiesGrowth(context.Background(), organizationId).From(from).To(to).LocationIds(locationIds).FromHour(fromHour).ToHour(toHour).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetOrganizationActivitiesGrowth``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationActivitiesGrowth`: ActivitiesGrowthCount
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetOrganizationActivitiesGrowth`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationActivitiesGrowthRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **from** | **time.Time** |  | 
 **to** | **time.Time** |  | 
 **locationIds** | **[]string** |  | 
 **fromHour** | **int32** |  | 
 **toHour** | **int32** |  | 

### Return type

[**ActivitiesGrowthCount**](ActivitiesGrowthCount.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationActivitiesQualMetrics

> ActivitiesQualMetrics GetOrganizationActivitiesQualMetrics(ctx, organizationId).Execute()

Get organization activities qual metrics



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetOrganizationActivitiesQualMetrics(context.Background(), organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetOrganizationActivitiesQualMetrics``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationActivitiesQualMetrics`: ActivitiesQualMetrics
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetOrganizationActivitiesQualMetrics`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationActivitiesQualMetricsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**ActivitiesQualMetrics**](ActivitiesQualMetrics.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationsOrganizationIdContactsKpi

> []ContactKPI GetOrganizationsOrganizationIdContactsKpi(ctx, organizationId).ContactIds(contactIds).Execute()

List KPIs

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactIds := []string{"Inner_example"} // []string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetOrganizationsOrganizationIdContactsKpi(context.Background(), organizationId).ContactIds(contactIds).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetOrganizationsOrganizationIdContactsKpi``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationsOrganizationIdContactsKpi`: []ContactKPI
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetOrganizationsOrganizationIdContactsKpi`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationsOrganizationIdContactsKpiRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **contactIds** | **[]string** |  | 

### Return type

[**[]ContactKPI**](ContactKPI.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetOrganizationsOrganizationIdContactsLabels

> []string GetOrganizationsOrganizationIdContactsLabels(ctx, organizationId).ContactIds(contactIds).Execute()

List organization labels

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactIds := []string{"Inner_example"} // []string | optional contacts list to get labels for (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.GetOrganizationsOrganizationIdContactsLabels(context.Background(), organizationId).ContactIds(contactIds).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.GetOrganizationsOrganizationIdContactsLabels``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetOrganizationsOrganizationIdContactsLabels`: []string
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.GetOrganizationsOrganizationIdContactsLabels`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetOrganizationsOrganizationIdContactsLabelsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **contactIds** | **[]string** | optional contacts list to get labels for | 

### Return type

**[]string**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ImportContacts

> ImportContacts(ctx, organizationId).File(file).Execute()

Import contacts



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    file := "file_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ContactsAPI.ImportContacts(context.Background(), organizationId).File(file).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ImportContacts``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiImportContactsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **file** | **string** |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ImportContactsAttributes

> ImportContactsAttributes(ctx, organizationId).File(file).Execute()

Import contacts attributes



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    file := "file_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ContactsAPI.ImportContactsAttributes(context.Background(), organizationId).File(file).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ImportContactsAttributes``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiImportContactsAttributesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **file** | **string** |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListChannelActivities

> ListOrganizationActivities200Response ListChannelActivities(ctx, organizationId, channelId).Sort(sort).Limit(limit).Offset(offset).AfterId(afterId).BeforeId(beforeId).Types(types).LocationIds(locationIds).Before(before).After(after).ContactId(contactId).Execute()

List channel activities



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    channelId := "channelId_example" // string | 
    sort := "sort_example" // string |  (optional)
    limit := float32(8.14) // float32 |  (optional)
    offset := float32(8.14) // float32 |  (optional)
    afterId := "afterId_example" // string |  (optional)
    beforeId := "beforeId_example" // string |  (optional)
    types := []string{"Inner_example"} // []string |  (optional)
    locationIds := []string{"Inner_example"} // []string |  (optional)
    before := time.Now() // time.Time |  (optional)
    after := time.Now() // time.Time |  (optional)
    contactId := "contactId_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListChannelActivities(context.Background(), organizationId, channelId).Sort(sort).Limit(limit).Offset(offset).AfterId(afterId).BeforeId(beforeId).Types(types).LocationIds(locationIds).Before(before).After(after).ContactId(contactId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListChannelActivities``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListChannelActivities`: ListOrganizationActivities200Response
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListChannelActivities`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListChannelActivitiesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **sort** | **string** |  | 
 **limit** | **float32** |  | 
 **offset** | **float32** |  | 
 **afterId** | **string** |  | 
 **beforeId** | **string** |  | 
 **types** | **[]string** |  | 
 **locationIds** | **[]string** |  | 
 **before** | **time.Time** |  | 
 **after** | **time.Time** |  | 
 **contactId** | **string** |  | 

### Return type

[**ListOrganizationActivities200Response**](ListOrganizationActivities200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListContactActivities

> ListContactActivities200Response ListContactActivities(ctx, organizationId, contactId).Sort(sort).Limit(limit).Offset(offset).AfterId(afterId).BeforeId(beforeId).Types(types).LocationIds(locationIds).Before(before).After(after).State(state).Execute()

List contact activities

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    sort := "sort_example" // string |  (optional)
    limit := float32(8.14) // float32 |  (optional)
    offset := float32(8.14) // float32 |  (optional)
    afterId := "afterId_example" // string |  (optional)
    beforeId := "beforeId_example" // string |  (optional)
    types := []string{"Inner_example"} // []string |  (optional)
    locationIds := []string{"Inner_example"} // []string |  (optional)
    before := time.Now() // time.Time |  (optional)
    after := time.Now() // time.Time |  (optional)
    state := "state_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListContactActivities(context.Background(), organizationId, contactId).Sort(sort).Limit(limit).Offset(offset).AfterId(afterId).BeforeId(beforeId).Types(types).LocationIds(locationIds).Before(before).After(after).State(state).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListContactActivities``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListContactActivities`: ListContactActivities200Response
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListContactActivities`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListContactActivitiesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **sort** | **string** |  | 
 **limit** | **float32** |  | 
 **offset** | **float32** |  | 
 **afterId** | **string** |  | 
 **beforeId** | **string** |  | 
 **types** | **[]string** |  | 
 **locationIds** | **[]string** |  | 
 **before** | **time.Time** |  | 
 **after** | **time.Time** |  | 
 **state** | **string** |  | 

### Return type

[**ListContactActivities200Response**](ListContactActivities200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListContactConversations

> ListContactConversations200Response ListContactConversations(ctx, organizationId, contactId).Sort(sort).Offset(offset).Limit(limit).Execute()

List contact conversations



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    sort := "sort_example" // string |  (optional)
    offset := float32(8.14) // float32 |  (optional)
    limit := float32(8.14) // float32 |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListContactConversations(context.Background(), organizationId, contactId).Sort(sort).Offset(offset).Limit(limit).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListContactConversations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListContactConversations`: ListContactConversations200Response
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListContactConversations`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListContactConversationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **sort** | **string** |  | 
 **offset** | **float32** |  | 
 **limit** | **float32** |  | 

### Return type

[**ListContactConversations200Response**](ListContactConversations200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListContactCoupons

> ListContactCoupons200Response ListContactCoupons(ctx, organizationId, contactId).Offset(offset).Limit(limit).Sort(sort).After(after).Status(status).Source(source).ExpiresAtAfter(expiresAtAfter).Type_(type_).Execute()

List contact coupons

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    offset := float32(8.14) // float32 |  (optional)
    limit := float32(8.14) // float32 |  (optional)
    sort := "sort_example" // string |  (optional)
    after := time.Now() // time.Time |  (optional)
    status := "status_example" // string |  (optional)
    source := "source_example" // string |  (optional)
    expiresAtAfter := time.Now() // time.Time |  (optional)
    type_ := "type__example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListContactCoupons(context.Background(), organizationId, contactId).Offset(offset).Limit(limit).Sort(sort).After(after).Status(status).Source(source).ExpiresAtAfter(expiresAtAfter).Type_(type_).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListContactCoupons``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListContactCoupons`: ListContactCoupons200Response
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListContactCoupons`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListContactCouponsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **offset** | **float32** |  | 
 **limit** | **float32** |  | 
 **sort** | **string** |  | 
 **after** | **time.Time** |  | 
 **status** | **string** |  | 
 **source** | **string** |  | 
 **expiresAtAfter** | **time.Time** |  | 
 **type_** | **string** |  | 

### Return type

[**ListContactCoupons200Response**](ListContactCoupons200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListContactFiles

> []File ListContactFiles(ctx, organizationId, contactId).Offset(offset).Limit(limit).Sort(sort).Execute()

List contact files



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    offset := float32(8.14) // float32 |  (optional)
    limit := float32(8.14) // float32 |  (optional)
    sort := "sort_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListContactFiles(context.Background(), organizationId, contactId).Offset(offset).Limit(limit).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListContactFiles``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListContactFiles`: []File
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListContactFiles`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListContactFilesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **offset** | **float32** |  | 
 **limit** | **float32** |  | 
 **sort** | **string** |  | 

### Return type

[**[]File**](File.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListContactInteractions

> ListContactInteractions200Response ListContactInteractions(ctx, organizationId, contactId).Offset(offset).Limit(limit).Sort(sort).Execute()

List contact interactions

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    offset := float32(8.14) // float32 |  (optional)
    limit := float32(8.14) // float32 |  (optional)
    sort := "sort_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListContactInteractions(context.Background(), organizationId, contactId).Offset(offset).Limit(limit).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListContactInteractions``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListContactInteractions`: ListContactInteractions200Response
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListContactInteractions`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListContactInteractionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **offset** | **float32** |  | 
 **limit** | **float32** |  | 
 **sort** | **string** |  | 

### Return type

[**ListContactInteractions200Response**](ListContactInteractions200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListContactLabels

> []string ListContactLabels(ctx, organizationId, contactId).Execute()

List contact labels

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListContactLabels(context.Background(), organizationId, contactId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListContactLabels``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListContactLabels`: []string
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListContactLabels`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListContactLabelsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

**[]string**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListContactSegments

> []Segment ListContactSegments(ctx, organizationId, contactId).Execute()

List segments

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListContactSegments(context.Background(), organizationId, contactId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListContactSegments``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListContactSegments`: []Segment
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListContactSegments`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListContactSegmentsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**[]Segment**](Segment.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListContacts

> ContactsList ListContacts(ctx, organizationId).Limit(limit).Offset(offset).Name(name).Mobile(mobile).FirstName(firstName).LastName(lastName).Email(email).Active(active).Status(status).MessengerTypesAny(messengerTypesAny).MessengerTypesAll(messengerTypesAll).Languages(languages).LabelsAny(labelsAny).LabelsAll(labelsAll).Gender(gender).ConversationsRange(conversationsRange).InteractionsRange(interactionsRange).VisitsRange(visitsRange).PurchasesRange(purchasesRange).ExcludedRelations(excludedRelations).IncludedRelations(includedRelations).Search(search).Sort(sort).SegmentsAny(segmentsAny).SegmentsAll(segmentsAll).ChannelsAll(channelsAll).ChannelsAny(channelsAny).DeviceNames(deviceNames).OsNames(osNames).BrandCodes(brandCodes).BrowserNames(browserNames).OsFamilyCodes(osFamilyCodes).Execute()

List contacts



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    limit := int32(56) // int32 |  (optional)
    offset := int32(56) // int32 |  (optional)
    name := "name_example" // string |  (optional)
    mobile := "mobile_example" // string |  (optional)
    firstName := "firstName_example" // string |  (optional)
    lastName := "lastName_example" // string |  (optional)
    email := "email_example" // string |  (optional)
    active := true // bool |  (optional)
    status := "status_example" // string |  (optional)
    messengerTypesAny := []string{"Inner_example"} // []string |  (optional)
    messengerTypesAll := []string{"Inner_example"} // []string |  (optional)
    languages := []string{"Inner_example"} // []string |  (optional)
    labelsAny := []string{"Inner_example"} // []string |  (optional)
    labelsAll := []string{"Inner_example"} // []string |  (optional)
    gender := "gender_example" // string |  (optional)
    conversationsRange := []string{"Inner_example"} // []string |  (optional)
    interactionsRange := []string{"Inner_example"} // []string |  (optional)
    visitsRange := []string{"Inner_example"} // []string |  (optional)
    purchasesRange := []string{"Inner_example"} // []string |  (optional)
    excludedRelations := []openapiclient.ContactExcludeRelation{openapiclient.ContactExcludeRelation("all")} // []ContactExcludeRelation | param takes priority over included_relations (optional)
    includedRelations := []openapiclient.ContactIncludeRelation{openapiclient.ContactIncludeRelation("Organization")} // []ContactIncludeRelation | if not empty then everything not listed is excluded by default (optional)
    search := "search_example" // string |  (optional)
    sort := "sort_example" // string |  (optional)
    segmentsAny := []string{"Inner_example"} // []string |  (optional)
    segmentsAll := []string{"Inner_example"} // []string |  (optional)
    channelsAll := []string{"Inner_example"} // []string |  (optional)
    channelsAny := []string{"Inner_example"} // []string |  (optional)
    deviceNames := []string{"Inner_example"} // []string |  (optional)
    osNames := []string{"Inner_example"} // []string |  (optional)
    brandCodes := []string{"Inner_example"} // []string |  (optional)
    browserNames := []string{"Inner_example"} // []string |  (optional)
    osFamilyCodes := []string{"Inner_example"} // []string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListContacts(context.Background(), organizationId).Limit(limit).Offset(offset).Name(name).Mobile(mobile).FirstName(firstName).LastName(lastName).Email(email).Active(active).Status(status).MessengerTypesAny(messengerTypesAny).MessengerTypesAll(messengerTypesAll).Languages(languages).LabelsAny(labelsAny).LabelsAll(labelsAll).Gender(gender).ConversationsRange(conversationsRange).InteractionsRange(interactionsRange).VisitsRange(visitsRange).PurchasesRange(purchasesRange).ExcludedRelations(excludedRelations).IncludedRelations(includedRelations).Search(search).Sort(sort).SegmentsAny(segmentsAny).SegmentsAll(segmentsAll).ChannelsAll(channelsAll).ChannelsAny(channelsAny).DeviceNames(deviceNames).OsNames(osNames).BrandCodes(brandCodes).BrowserNames(browserNames).OsFamilyCodes(osFamilyCodes).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListContacts``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListContacts`: ContactsList
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListContacts`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListContactsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **limit** | **int32** |  | 
 **offset** | **int32** |  | 
 **name** | **string** |  | 
 **mobile** | **string** |  | 
 **firstName** | **string** |  | 
 **lastName** | **string** |  | 
 **email** | **string** |  | 
 **active** | **bool** |  | 
 **status** | **string** |  | 
 **messengerTypesAny** | **[]string** |  | 
 **messengerTypesAll** | **[]string** |  | 
 **languages** | **[]string** |  | 
 **labelsAny** | **[]string** |  | 
 **labelsAll** | **[]string** |  | 
 **gender** | **string** |  | 
 **conversationsRange** | **[]string** |  | 
 **interactionsRange** | **[]string** |  | 
 **visitsRange** | **[]string** |  | 
 **purchasesRange** | **[]string** |  | 
 **excludedRelations** | [**[]ContactExcludeRelation**](ContactExcludeRelation.md) | param takes priority over included_relations | 
 **includedRelations** | [**[]ContactIncludeRelation**](ContactIncludeRelation.md) | if not empty then everything not listed is excluded by default | 
 **search** | **string** |  | 
 **sort** | **string** |  | 
 **segmentsAny** | **[]string** |  | 
 **segmentsAll** | **[]string** |  | 
 **channelsAll** | **[]string** |  | 
 **channelsAny** | **[]string** |  | 
 **deviceNames** | **[]string** |  | 
 **osNames** | **[]string** |  | 
 **brandCodes** | **[]string** |  | 
 **browserNames** | **[]string** |  | 
 **osFamilyCodes** | **[]string** |  | 

### Return type

[**ContactsList**](ContactsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListContactsGroup

> ContactsList ListContactsGroup(ctx, organizationId).SelectedIds(selectedIds).Limit(limit).Offset(offset).Name(name).Mobile(mobile).FirstName(firstName).LastName(lastName).Email(email).Active(active).Status(status).MessengerTypes(messengerTypes).Languages(languages).Labels(labels).Gender(gender).ConversationsRange(conversationsRange).InteractionsRange(interactionsRange).VisitsRange(visitsRange).PurchasesRange(purchasesRange).Search(search).Sort(sort).Execute()

List contacts by IDs



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    selectedIds := []string{"Inner_example"} // []string |  (optional)
    limit := int32(56) // int32 |  (optional)
    offset := int32(56) // int32 |  (optional)
    name := "name_example" // string |  (optional)
    mobile := "mobile_example" // string |  (optional)
    firstName := "firstName_example" // string |  (optional)
    lastName := "lastName_example" // string |  (optional)
    email := "email_example" // string |  (optional)
    active := true // bool |  (optional)
    status := "status_example" // string |  (optional)
    messengerTypes := []string{"Inner_example"} // []string |  (optional)
    languages := []string{"Inner_example"} // []string |  (optional)
    labels := []string{"Inner_example"} // []string |  (optional)
    gender := "gender_example" // string |  (optional)
    conversationsRange := []string{"Inner_example"} // []string |  (optional)
    interactionsRange := []string{"Inner_example"} // []string |  (optional)
    visitsRange := []string{"Inner_example"} // []string |  (optional)
    purchasesRange := []string{"Inner_example"} // []string |  (optional)
    search := "search_example" // string |  (optional)
    sort := "sort_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListContactsGroup(context.Background(), organizationId).SelectedIds(selectedIds).Limit(limit).Offset(offset).Name(name).Mobile(mobile).FirstName(firstName).LastName(lastName).Email(email).Active(active).Status(status).MessengerTypes(messengerTypes).Languages(languages).Labels(labels).Gender(gender).ConversationsRange(conversationsRange).InteractionsRange(interactionsRange).VisitsRange(visitsRange).PurchasesRange(purchasesRange).Search(search).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListContactsGroup``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListContactsGroup`: ContactsList
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListContactsGroup`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListContactsGroupRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **selectedIds** | **[]string** |  | 
 **limit** | **int32** |  | 
 **offset** | **int32** |  | 
 **name** | **string** |  | 
 **mobile** | **string** |  | 
 **firstName** | **string** |  | 
 **lastName** | **string** |  | 
 **email** | **string** |  | 
 **active** | **bool** |  | 
 **status** | **string** |  | 
 **messengerTypes** | **[]string** |  | 
 **languages** | **[]string** |  | 
 **labels** | **[]string** |  | 
 **gender** | **string** |  | 
 **conversationsRange** | **[]string** |  | 
 **interactionsRange** | **[]string** |  | 
 **visitsRange** | **[]string** |  | 
 **purchasesRange** | **[]string** |  | 
 **search** | **string** |  | 
 **sort** | **string** |  | 

### Return type

[**ContactsList**](ContactsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListLanguages

> []string ListLanguages(ctx, organizationId).Execute()

List languages



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListLanguages(context.Background(), organizationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListLanguages``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListLanguages`: []string
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListLanguages`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListLanguagesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

**[]string**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListOrganizationActivities

> ListOrganizationActivities200Response ListOrganizationActivities(ctx, organizationId).Sort(sort).Limit(limit).Offset(offset).AfterId(afterId).BeforeId(beforeId).Types(types).LocationIds(locationIds).Before(before).After(after).ContactId(contactId).Execute()

List organization activities



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    sort := "sort_example" // string |  (optional)
    limit := float32(8.14) // float32 |  (optional)
    offset := float32(8.14) // float32 |  (optional)
    afterId := "afterId_example" // string |  (optional)
    beforeId := "beforeId_example" // string |  (optional)
    types := []string{"Inner_example"} // []string |  (optional)
    locationIds := []string{"Inner_example"} // []string |  (optional)
    before := time.Now() // time.Time |  (optional)
    after := time.Now() // time.Time |  (optional)
    contactId := "contactId_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListOrganizationActivities(context.Background(), organizationId).Sort(sort).Limit(limit).Offset(offset).AfterId(afterId).BeforeId(beforeId).Types(types).LocationIds(locationIds).Before(before).After(after).ContactId(contactId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListOrganizationActivities``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListOrganizationActivities`: ListOrganizationActivities200Response
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListOrganizationActivities`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListOrganizationActivitiesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **sort** | **string** |  | 
 **limit** | **float32** |  | 
 **offset** | **float32** |  | 
 **afterId** | **string** |  | 
 **beforeId** | **string** |  | 
 **types** | **[]string** |  | 
 **locationIds** | **[]string** |  | 
 **before** | **time.Time** |  | 
 **after** | **time.Time** |  | 
 **contactId** | **string** |  | 

### Return type

[**ListOrganizationActivities200Response**](ListOrganizationActivities200Response.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListSegments

> []string ListSegments(ctx, organizationId).Types(types).SubTypes(subTypes).Limit(limit).Offset(offset).Execute()

List organization segments



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    types := []string{"Inner_example"} // []string |  (optional)
    subTypes := []string{"Inner_example"} // []string |  (optional)
    limit := float32(8.14) // float32 |  (optional)
    offset := float32(8.14) // float32 |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.ListSegments(context.Background(), organizationId).Types(types).SubTypes(subTypes).Limit(limit).Offset(offset).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.ListSegments``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListSegments`: []string
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.ListSegments`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiListSegmentsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **types** | **[]string** |  | 
 **subTypes** | **[]string** |  | 
 **limit** | **float32** |  | 
 **offset** | **float32** |  | 

### Return type

**[]string**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PatchActivity

> Activity PatchActivity(ctx, organizationId, contactId, activityId).ActivityPatchable(activityPatchable).Execute()

Patch activity



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    activityId := "activityId_example" // string | 
    activityPatchable := *openapiclient.NewActivityPatchable() // ActivityPatchable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.PatchActivity(context.Background(), organizationId, contactId, activityId).ActivityPatchable(activityPatchable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.PatchActivity``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PatchActivity`: Activity
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.PatchActivity`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 
**activityId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPatchActivityRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **activityPatchable** | [**ActivityPatchable**](ActivityPatchable.md) |  | 

### Return type

[**Activity**](Activity.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PatchContact

> Contact PatchContact(ctx, contactId, organizationId).ContactPatchable(contactPatchable).Execute()

Patch contact

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    contactId := "contactId_example" // string | 
    organizationId := "organizationId_example" // string | 
    contactPatchable := *openapiclient.NewContactPatchable() // ContactPatchable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.PatchContact(context.Background(), contactId, organizationId).ContactPatchable(contactPatchable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.PatchContact``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PatchContact`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.PatchContact`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**contactId** | **string** |  | 
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPatchContactRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **contactPatchable** | [**ContactPatchable**](ContactPatchable.md) |  | 

### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PatchCoupon

> Coupon PatchCoupon(ctx, organizationId, contactId, code).PatchableCoupon(patchableCoupon).Execute()

Patch coupon

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    code := "code_example" // string | 
    patchableCoupon := *openapiclient.NewPatchableCoupon() // PatchableCoupon |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.PatchCoupon(context.Background(), organizationId, contactId, code).PatchableCoupon(patchableCoupon).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.PatchCoupon``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `PatchCoupon`: Coupon
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.PatchCoupon`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 
**code** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiPatchCouponRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **patchableCoupon** | [**PatchableCoupon**](PatchableCoupon.md) |  | 

### Return type

[**Coupon**](Coupon.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RevokeConsent

> RevokeConsent(ctx, organizationId, contactId).RevokeConsentRequest(revokeConsentRequest).Execute()

Revoke consent from contact

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    revokeConsentRequest := *openapiclient.NewRevokeConsentRequest() // RevokeConsentRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ContactsAPI.RevokeConsent(context.Background(), organizationId, contactId).RevokeConsentRequest(revokeConsentRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.RevokeConsent``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiRevokeConsentRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **revokeConsentRequest** | [**RevokeConsentRequest**](RevokeConsentRequest.md) |  | 

### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateContact

> Contact UpdateContact(ctx, contactId, organizationId).ContactEditable(contactEditable).Execute()

Update contact



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    contactId := "contactId_example" // string | 
    organizationId := "organizationId_example" // string | 
    contactEditable := *openapiclient.NewContactEditable("Name_example") // ContactEditable |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.UpdateContact(context.Background(), contactId, organizationId).ContactEditable(contactEditable).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.UpdateContact``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateContact`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.UpdateContact`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**contactId** | **string** |  | 
**organizationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateContactRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **contactEditable** | [**ContactEditable**](ContactEditable.md) |  | 

### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateCoupon

> Coupon UpdateCoupon(ctx, organizationId, contactId, code).Coupon(coupon).Execute()

Update coupon



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    code := "code_example" // string | 
    coupon := *openapiclient.NewCoupon("Id_example", "Source_example", "Code_example", "ContactId_example", "Status_example", time.Now(), "DisplayType_example", "Name_example") // Coupon |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.UpdateCoupon(context.Background(), organizationId, contactId, code).Coupon(coupon).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.UpdateCoupon``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateCoupon`: Coupon
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.UpdateCoupon`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 
**code** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateCouponRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **coupon** | [**Coupon**](Coupon.md) |  | 

### Return type

[**Coupon**](Coupon.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UploadAvatar

> Contact UploadAvatar(ctx, organizationId, contactId).File(file).Source(source).Execute()

Upload avatar



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    organizationId := "organizationId_example" // string | 
    contactId := "contactId_example" // string | 
    file := "file_example" // string |  (optional)
    source := "source_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ContactsAPI.UploadAvatar(context.Background(), organizationId, contactId).File(file).Source(source).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ContactsAPI.UploadAvatar``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UploadAvatar`: Contact
    fmt.Fprintf(os.Stdout, "Response from `ContactsAPI.UploadAvatar`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**organizationId** | **string** |  | 
**contactId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUploadAvatarRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **file** | **string** |  | 
 **source** | **string** |  | 

### Return type

[**Contact**](Contact.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

