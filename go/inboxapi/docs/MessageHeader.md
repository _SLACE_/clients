# MessageHeader

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | [**MessageHeaderType**](MessageHeaderType.md) |  | 
**Text** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageHeader

`func NewMessageHeader(type_ MessageHeaderType, ) *MessageHeader`

NewMessageHeader instantiates a new MessageHeader object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageHeaderWithDefaults

`func NewMessageHeaderWithDefaults() *MessageHeader`

NewMessageHeaderWithDefaults instantiates a new MessageHeader object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *MessageHeader) GetType() MessageHeaderType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MessageHeader) GetTypeOk() (*MessageHeaderType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MessageHeader) SetType(v MessageHeaderType)`

SetType sets Type field to given value.


### GetText

`func (o *MessageHeader) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *MessageHeader) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *MessageHeader) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *MessageHeader) HasText() bool`

HasText returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


