# ConversationMessageDataMediaInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Object** | Pointer to **string** |  | [optional] 
**Url** | Pointer to **string** |  | [optional] 
**MimeType** | **string** |  | 
**FileName** | Pointer to **string** |  | [optional] 
**Width** | Pointer to **int32** |  | [optional] 
**Height** | Pointer to **int32** |  | [optional] 
**CardIdx** | Pointer to **int32** |  | [optional] 
**Status** | Pointer to **string** |  | [optional] 

## Methods

### NewConversationMessageDataMediaInner

`func NewConversationMessageDataMediaInner(id string, mimeType string, ) *ConversationMessageDataMediaInner`

NewConversationMessageDataMediaInner instantiates a new ConversationMessageDataMediaInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConversationMessageDataMediaInnerWithDefaults

`func NewConversationMessageDataMediaInnerWithDefaults() *ConversationMessageDataMediaInner`

NewConversationMessageDataMediaInnerWithDefaults instantiates a new ConversationMessageDataMediaInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ConversationMessageDataMediaInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ConversationMessageDataMediaInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ConversationMessageDataMediaInner) SetId(v string)`

SetId sets Id field to given value.


### GetObject

`func (o *ConversationMessageDataMediaInner) GetObject() string`

GetObject returns the Object field if non-nil, zero value otherwise.

### GetObjectOk

`func (o *ConversationMessageDataMediaInner) GetObjectOk() (*string, bool)`

GetObjectOk returns a tuple with the Object field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetObject

`func (o *ConversationMessageDataMediaInner) SetObject(v string)`

SetObject sets Object field to given value.

### HasObject

`func (o *ConversationMessageDataMediaInner) HasObject() bool`

HasObject returns a boolean if a field has been set.

### GetUrl

`func (o *ConversationMessageDataMediaInner) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *ConversationMessageDataMediaInner) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *ConversationMessageDataMediaInner) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *ConversationMessageDataMediaInner) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetMimeType

`func (o *ConversationMessageDataMediaInner) GetMimeType() string`

GetMimeType returns the MimeType field if non-nil, zero value otherwise.

### GetMimeTypeOk

`func (o *ConversationMessageDataMediaInner) GetMimeTypeOk() (*string, bool)`

GetMimeTypeOk returns a tuple with the MimeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMimeType

`func (o *ConversationMessageDataMediaInner) SetMimeType(v string)`

SetMimeType sets MimeType field to given value.


### GetFileName

`func (o *ConversationMessageDataMediaInner) GetFileName() string`

GetFileName returns the FileName field if non-nil, zero value otherwise.

### GetFileNameOk

`func (o *ConversationMessageDataMediaInner) GetFileNameOk() (*string, bool)`

GetFileNameOk returns a tuple with the FileName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFileName

`func (o *ConversationMessageDataMediaInner) SetFileName(v string)`

SetFileName sets FileName field to given value.

### HasFileName

`func (o *ConversationMessageDataMediaInner) HasFileName() bool`

HasFileName returns a boolean if a field has been set.

### GetWidth

`func (o *ConversationMessageDataMediaInner) GetWidth() int32`

GetWidth returns the Width field if non-nil, zero value otherwise.

### GetWidthOk

`func (o *ConversationMessageDataMediaInner) GetWidthOk() (*int32, bool)`

GetWidthOk returns a tuple with the Width field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWidth

`func (o *ConversationMessageDataMediaInner) SetWidth(v int32)`

SetWidth sets Width field to given value.

### HasWidth

`func (o *ConversationMessageDataMediaInner) HasWidth() bool`

HasWidth returns a boolean if a field has been set.

### GetHeight

`func (o *ConversationMessageDataMediaInner) GetHeight() int32`

GetHeight returns the Height field if non-nil, zero value otherwise.

### GetHeightOk

`func (o *ConversationMessageDataMediaInner) GetHeightOk() (*int32, bool)`

GetHeightOk returns a tuple with the Height field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeight

`func (o *ConversationMessageDataMediaInner) SetHeight(v int32)`

SetHeight sets Height field to given value.

### HasHeight

`func (o *ConversationMessageDataMediaInner) HasHeight() bool`

HasHeight returns a boolean if a field has been set.

### GetCardIdx

`func (o *ConversationMessageDataMediaInner) GetCardIdx() int32`

GetCardIdx returns the CardIdx field if non-nil, zero value otherwise.

### GetCardIdxOk

`func (o *ConversationMessageDataMediaInner) GetCardIdxOk() (*int32, bool)`

GetCardIdxOk returns a tuple with the CardIdx field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCardIdx

`func (o *ConversationMessageDataMediaInner) SetCardIdx(v int32)`

SetCardIdx sets CardIdx field to given value.

### HasCardIdx

`func (o *ConversationMessageDataMediaInner) HasCardIdx() bool`

HasCardIdx returns a boolean if a field has been set.

### GetStatus

`func (o *ConversationMessageDataMediaInner) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *ConversationMessageDataMediaInner) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *ConversationMessageDataMediaInner) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *ConversationMessageDataMediaInner) HasStatus() bool`

HasStatus returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


