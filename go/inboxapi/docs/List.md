# List

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ButtonName** | **string** |  | 
**Items** | [**[]MessageItem**](MessageItem.md) |  | 

## Methods

### NewList

`func NewList(buttonName string, items []MessageItem, ) *List`

NewList instantiates a new List object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListWithDefaults

`func NewListWithDefaults() *List`

NewListWithDefaults instantiates a new List object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetButtonName

`func (o *List) GetButtonName() string`

GetButtonName returns the ButtonName field if non-nil, zero value otherwise.

### GetButtonNameOk

`func (o *List) GetButtonNameOk() (*string, bool)`

GetButtonNameOk returns a tuple with the ButtonName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtonName

`func (o *List) SetButtonName(v string)`

SetButtonName sets ButtonName field to given value.


### GetItems

`func (o *List) GetItems() []MessageItem`

GetItems returns the Items field if non-nil, zero value otherwise.

### GetItemsOk

`func (o *List) GetItemsOk() (*[]MessageItem, bool)`

GetItemsOk returns a tuple with the Items field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItems

`func (o *List) SetItems(v []MessageItem)`

SetItems sets Items field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


