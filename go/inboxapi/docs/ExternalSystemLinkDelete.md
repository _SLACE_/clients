# ExternalSystemLinkDelete

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SystemName** | **string** | User ID or full profile page URL as defined in organization settings | 

## Methods

### NewExternalSystemLinkDelete

`func NewExternalSystemLinkDelete(systemName string, ) *ExternalSystemLinkDelete`

NewExternalSystemLinkDelete instantiates a new ExternalSystemLinkDelete object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExternalSystemLinkDeleteWithDefaults

`func NewExternalSystemLinkDeleteWithDefaults() *ExternalSystemLinkDelete`

NewExternalSystemLinkDeleteWithDefaults instantiates a new ExternalSystemLinkDelete object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSystemName

`func (o *ExternalSystemLinkDelete) GetSystemName() string`

GetSystemName returns the SystemName field if non-nil, zero value otherwise.

### GetSystemNameOk

`func (o *ExternalSystemLinkDelete) GetSystemNameOk() (*string, bool)`

GetSystemNameOk returns a tuple with the SystemName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSystemName

`func (o *ExternalSystemLinkDelete) SetSystemName(v string)`

SetSystemName sets SystemName field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


