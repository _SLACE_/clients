# ContactEditableLanguage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Source** | **string** | Indicates the source of language info | 
**Primary** | **bool** | Flag to mark language as primary for contact | 
**Locale** | **string** | 2-letter ISO code (en, de...) | 

## Methods

### NewContactEditableLanguage

`func NewContactEditableLanguage(source string, primary bool, locale string, ) *ContactEditableLanguage`

NewContactEditableLanguage instantiates a new ContactEditableLanguage object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactEditableLanguageWithDefaults

`func NewContactEditableLanguageWithDefaults() *ContactEditableLanguage`

NewContactEditableLanguageWithDefaults instantiates a new ContactEditableLanguage object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSource

`func (o *ContactEditableLanguage) GetSource() string`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *ContactEditableLanguage) GetSourceOk() (*string, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *ContactEditableLanguage) SetSource(v string)`

SetSource sets Source field to given value.


### GetPrimary

`func (o *ContactEditableLanguage) GetPrimary() bool`

GetPrimary returns the Primary field if non-nil, zero value otherwise.

### GetPrimaryOk

`func (o *ContactEditableLanguage) GetPrimaryOk() (*bool, bool)`

GetPrimaryOk returns a tuple with the Primary field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrimary

`func (o *ContactEditableLanguage) SetPrimary(v bool)`

SetPrimary sets Primary field to given value.


### GetLocale

`func (o *ContactEditableLanguage) GetLocale() string`

GetLocale returns the Locale field if non-nil, zero value otherwise.

### GetLocaleOk

`func (o *ContactEditableLanguage) GetLocaleOk() (*string, bool)`

GetLocaleOk returns a tuple with the Locale field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocale

`func (o *ContactEditableLanguage) SetLocale(v string)`

SetLocale sets Locale field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


