# ActivitiesQualMetrics

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ReturningAmount** | Pointer to **int32** |  | [optional] 
**RepeatAmount** | Pointer to **int32** |  | [optional] 
**ReferringAmount** | Pointer to **int32** |  | [optional] 
**HookedAmount** | Pointer to **int32** |  | [optional] 
**AchievingAmount** | Pointer to **int32** |  | [optional] 

## Methods

### NewActivitiesQualMetrics

`func NewActivitiesQualMetrics() *ActivitiesQualMetrics`

NewActivitiesQualMetrics instantiates a new ActivitiesQualMetrics object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewActivitiesQualMetricsWithDefaults

`func NewActivitiesQualMetricsWithDefaults() *ActivitiesQualMetrics`

NewActivitiesQualMetricsWithDefaults instantiates a new ActivitiesQualMetrics object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetReturningAmount

`func (o *ActivitiesQualMetrics) GetReturningAmount() int32`

GetReturningAmount returns the ReturningAmount field if non-nil, zero value otherwise.

### GetReturningAmountOk

`func (o *ActivitiesQualMetrics) GetReturningAmountOk() (*int32, bool)`

GetReturningAmountOk returns a tuple with the ReturningAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReturningAmount

`func (o *ActivitiesQualMetrics) SetReturningAmount(v int32)`

SetReturningAmount sets ReturningAmount field to given value.

### HasReturningAmount

`func (o *ActivitiesQualMetrics) HasReturningAmount() bool`

HasReturningAmount returns a boolean if a field has been set.

### GetRepeatAmount

`func (o *ActivitiesQualMetrics) GetRepeatAmount() int32`

GetRepeatAmount returns the RepeatAmount field if non-nil, zero value otherwise.

### GetRepeatAmountOk

`func (o *ActivitiesQualMetrics) GetRepeatAmountOk() (*int32, bool)`

GetRepeatAmountOk returns a tuple with the RepeatAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRepeatAmount

`func (o *ActivitiesQualMetrics) SetRepeatAmount(v int32)`

SetRepeatAmount sets RepeatAmount field to given value.

### HasRepeatAmount

`func (o *ActivitiesQualMetrics) HasRepeatAmount() bool`

HasRepeatAmount returns a boolean if a field has been set.

### GetReferringAmount

`func (o *ActivitiesQualMetrics) GetReferringAmount() int32`

GetReferringAmount returns the ReferringAmount field if non-nil, zero value otherwise.

### GetReferringAmountOk

`func (o *ActivitiesQualMetrics) GetReferringAmountOk() (*int32, bool)`

GetReferringAmountOk returns a tuple with the ReferringAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferringAmount

`func (o *ActivitiesQualMetrics) SetReferringAmount(v int32)`

SetReferringAmount sets ReferringAmount field to given value.

### HasReferringAmount

`func (o *ActivitiesQualMetrics) HasReferringAmount() bool`

HasReferringAmount returns a boolean if a field has been set.

### GetHookedAmount

`func (o *ActivitiesQualMetrics) GetHookedAmount() int32`

GetHookedAmount returns the HookedAmount field if non-nil, zero value otherwise.

### GetHookedAmountOk

`func (o *ActivitiesQualMetrics) GetHookedAmountOk() (*int32, bool)`

GetHookedAmountOk returns a tuple with the HookedAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHookedAmount

`func (o *ActivitiesQualMetrics) SetHookedAmount(v int32)`

SetHookedAmount sets HookedAmount field to given value.

### HasHookedAmount

`func (o *ActivitiesQualMetrics) HasHookedAmount() bool`

HasHookedAmount returns a boolean if a field has been set.

### GetAchievingAmount

`func (o *ActivitiesQualMetrics) GetAchievingAmount() int32`

GetAchievingAmount returns the AchievingAmount field if non-nil, zero value otherwise.

### GetAchievingAmountOk

`func (o *ActivitiesQualMetrics) GetAchievingAmountOk() (*int32, bool)`

GetAchievingAmountOk returns a tuple with the AchievingAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAchievingAmount

`func (o *ActivitiesQualMetrics) SetAchievingAmount(v int32)`

SetAchievingAmount sets AchievingAmount field to given value.

### HasAchievingAmount

`func (o *ActivitiesQualMetrics) HasAchievingAmount() bool`

HasAchievingAmount returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


