# MessageOrderStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ReferenceId** | **string** |  | 
**Status** | **string** |  | [default to "delivered"]
**Description** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageOrderStatus

`func NewMessageOrderStatus(referenceId string, status string, ) *MessageOrderStatus`

NewMessageOrderStatus instantiates a new MessageOrderStatus object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageOrderStatusWithDefaults

`func NewMessageOrderStatusWithDefaults() *MessageOrderStatus`

NewMessageOrderStatusWithDefaults instantiates a new MessageOrderStatus object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetReferenceId

`func (o *MessageOrderStatus) GetReferenceId() string`

GetReferenceId returns the ReferenceId field if non-nil, zero value otherwise.

### GetReferenceIdOk

`func (o *MessageOrderStatus) GetReferenceIdOk() (*string, bool)`

GetReferenceIdOk returns a tuple with the ReferenceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferenceId

`func (o *MessageOrderStatus) SetReferenceId(v string)`

SetReferenceId sets ReferenceId field to given value.


### GetStatus

`func (o *MessageOrderStatus) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *MessageOrderStatus) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *MessageOrderStatus) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetDescription

`func (o *MessageOrderStatus) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *MessageOrderStatus) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *MessageOrderStatus) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *MessageOrderStatus) HasDescription() bool`

HasDescription returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


