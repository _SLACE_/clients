# WhatsAppTemplateComponentExample

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HeaderText** | Pointer to **[]string** |  | [optional] 
**BodyText** | Pointer to **[]string** |  | [optional] 
**FooterText** | Pointer to **[]string** |  | [optional] 
**HeaderUrl** | Pointer to **[]string** |  | [optional] 
**HeaderHandle** | Pointer to **[]string** |  | [optional] 

## Methods

### NewWhatsAppTemplateComponentExample

`func NewWhatsAppTemplateComponentExample() *WhatsAppTemplateComponentExample`

NewWhatsAppTemplateComponentExample instantiates a new WhatsAppTemplateComponentExample object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateComponentExampleWithDefaults

`func NewWhatsAppTemplateComponentExampleWithDefaults() *WhatsAppTemplateComponentExample`

NewWhatsAppTemplateComponentExampleWithDefaults instantiates a new WhatsAppTemplateComponentExample object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetHeaderText

`func (o *WhatsAppTemplateComponentExample) GetHeaderText() []string`

GetHeaderText returns the HeaderText field if non-nil, zero value otherwise.

### GetHeaderTextOk

`func (o *WhatsAppTemplateComponentExample) GetHeaderTextOk() (*[]string, bool)`

GetHeaderTextOk returns a tuple with the HeaderText field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeaderText

`func (o *WhatsAppTemplateComponentExample) SetHeaderText(v []string)`

SetHeaderText sets HeaderText field to given value.

### HasHeaderText

`func (o *WhatsAppTemplateComponentExample) HasHeaderText() bool`

HasHeaderText returns a boolean if a field has been set.

### GetBodyText

`func (o *WhatsAppTemplateComponentExample) GetBodyText() []string`

GetBodyText returns the BodyText field if non-nil, zero value otherwise.

### GetBodyTextOk

`func (o *WhatsAppTemplateComponentExample) GetBodyTextOk() (*[]string, bool)`

GetBodyTextOk returns a tuple with the BodyText field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBodyText

`func (o *WhatsAppTemplateComponentExample) SetBodyText(v []string)`

SetBodyText sets BodyText field to given value.

### HasBodyText

`func (o *WhatsAppTemplateComponentExample) HasBodyText() bool`

HasBodyText returns a boolean if a field has been set.

### GetFooterText

`func (o *WhatsAppTemplateComponentExample) GetFooterText() []string`

GetFooterText returns the FooterText field if non-nil, zero value otherwise.

### GetFooterTextOk

`func (o *WhatsAppTemplateComponentExample) GetFooterTextOk() (*[]string, bool)`

GetFooterTextOk returns a tuple with the FooterText field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFooterText

`func (o *WhatsAppTemplateComponentExample) SetFooterText(v []string)`

SetFooterText sets FooterText field to given value.

### HasFooterText

`func (o *WhatsAppTemplateComponentExample) HasFooterText() bool`

HasFooterText returns a boolean if a field has been set.

### GetHeaderUrl

`func (o *WhatsAppTemplateComponentExample) GetHeaderUrl() []string`

GetHeaderUrl returns the HeaderUrl field if non-nil, zero value otherwise.

### GetHeaderUrlOk

`func (o *WhatsAppTemplateComponentExample) GetHeaderUrlOk() (*[]string, bool)`

GetHeaderUrlOk returns a tuple with the HeaderUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeaderUrl

`func (o *WhatsAppTemplateComponentExample) SetHeaderUrl(v []string)`

SetHeaderUrl sets HeaderUrl field to given value.

### HasHeaderUrl

`func (o *WhatsAppTemplateComponentExample) HasHeaderUrl() bool`

HasHeaderUrl returns a boolean if a field has been set.

### GetHeaderHandle

`func (o *WhatsAppTemplateComponentExample) GetHeaderHandle() []string`

GetHeaderHandle returns the HeaderHandle field if non-nil, zero value otherwise.

### GetHeaderHandleOk

`func (o *WhatsAppTemplateComponentExample) GetHeaderHandleOk() (*[]string, bool)`

GetHeaderHandleOk returns a tuple with the HeaderHandle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeaderHandle

`func (o *WhatsAppTemplateComponentExample) SetHeaderHandle(v []string)`

SetHeaderHandle sets HeaderHandle field to given value.

### HasHeaderHandle

`func (o *WhatsAppTemplateComponentExample) HasHeaderHandle() bool`

HasHeaderHandle returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


