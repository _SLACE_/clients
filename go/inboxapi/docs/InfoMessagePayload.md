# InfoMessagePayload

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TransactionId** | Pointer to **string** |  | [optional] 
**OldMessengerId** | Pointer to **string** |  | [optional] 
**NewMessengerId** | Pointer to **string** |  | [optional] 
**Locale** | Pointer to **string** |  | [optional] 
**AttributionType** | Pointer to **string** |  | [optional] 
**State** | Pointer to **string** |  | [optional] 

## Methods

### NewInfoMessagePayload

`func NewInfoMessagePayload() *InfoMessagePayload`

NewInfoMessagePayload instantiates a new InfoMessagePayload object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInfoMessagePayloadWithDefaults

`func NewInfoMessagePayloadWithDefaults() *InfoMessagePayload`

NewInfoMessagePayloadWithDefaults instantiates a new InfoMessagePayload object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTransactionId

`func (o *InfoMessagePayload) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *InfoMessagePayload) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *InfoMessagePayload) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.

### HasTransactionId

`func (o *InfoMessagePayload) HasTransactionId() bool`

HasTransactionId returns a boolean if a field has been set.

### GetOldMessengerId

`func (o *InfoMessagePayload) GetOldMessengerId() string`

GetOldMessengerId returns the OldMessengerId field if non-nil, zero value otherwise.

### GetOldMessengerIdOk

`func (o *InfoMessagePayload) GetOldMessengerIdOk() (*string, bool)`

GetOldMessengerIdOk returns a tuple with the OldMessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOldMessengerId

`func (o *InfoMessagePayload) SetOldMessengerId(v string)`

SetOldMessengerId sets OldMessengerId field to given value.

### HasOldMessengerId

`func (o *InfoMessagePayload) HasOldMessengerId() bool`

HasOldMessengerId returns a boolean if a field has been set.

### GetNewMessengerId

`func (o *InfoMessagePayload) GetNewMessengerId() string`

GetNewMessengerId returns the NewMessengerId field if non-nil, zero value otherwise.

### GetNewMessengerIdOk

`func (o *InfoMessagePayload) GetNewMessengerIdOk() (*string, bool)`

GetNewMessengerIdOk returns a tuple with the NewMessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewMessengerId

`func (o *InfoMessagePayload) SetNewMessengerId(v string)`

SetNewMessengerId sets NewMessengerId field to given value.

### HasNewMessengerId

`func (o *InfoMessagePayload) HasNewMessengerId() bool`

HasNewMessengerId returns a boolean if a field has been set.

### GetLocale

`func (o *InfoMessagePayload) GetLocale() string`

GetLocale returns the Locale field if non-nil, zero value otherwise.

### GetLocaleOk

`func (o *InfoMessagePayload) GetLocaleOk() (*string, bool)`

GetLocaleOk returns a tuple with the Locale field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocale

`func (o *InfoMessagePayload) SetLocale(v string)`

SetLocale sets Locale field to given value.

### HasLocale

`func (o *InfoMessagePayload) HasLocale() bool`

HasLocale returns a boolean if a field has been set.

### GetAttributionType

`func (o *InfoMessagePayload) GetAttributionType() string`

GetAttributionType returns the AttributionType field if non-nil, zero value otherwise.

### GetAttributionTypeOk

`func (o *InfoMessagePayload) GetAttributionTypeOk() (*string, bool)`

GetAttributionTypeOk returns a tuple with the AttributionType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributionType

`func (o *InfoMessagePayload) SetAttributionType(v string)`

SetAttributionType sets AttributionType field to given value.

### HasAttributionType

`func (o *InfoMessagePayload) HasAttributionType() bool`

HasAttributionType returns a boolean if a field has been set.

### GetState

`func (o *InfoMessagePayload) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *InfoMessagePayload) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *InfoMessagePayload) SetState(v string)`

SetState sets State field to given value.

### HasState

`func (o *InfoMessagePayload) HasState() bool`

HasState returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


