# ActivitiesGrowthCount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MessengerMoment** | Pointer to [**GrowthCount**](GrowthCount.md) |  | [optional] 
**Visit** | Pointer to [**GrowthCount**](GrowthCount.md) |  | [optional] 
**Investment** | Pointer to [**GrowthCount**](GrowthCount.md) |  | [optional] 
**Purchase** | Pointer to [**GrowthCount**](GrowthCount.md) |  | [optional] 
**Referral** | Pointer to [**GrowthCount**](GrowthCount.md) |  | [optional] 

## Methods

### NewActivitiesGrowthCount

`func NewActivitiesGrowthCount() *ActivitiesGrowthCount`

NewActivitiesGrowthCount instantiates a new ActivitiesGrowthCount object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewActivitiesGrowthCountWithDefaults

`func NewActivitiesGrowthCountWithDefaults() *ActivitiesGrowthCount`

NewActivitiesGrowthCountWithDefaults instantiates a new ActivitiesGrowthCount object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessengerMoment

`func (o *ActivitiesGrowthCount) GetMessengerMoment() GrowthCount`

GetMessengerMoment returns the MessengerMoment field if non-nil, zero value otherwise.

### GetMessengerMomentOk

`func (o *ActivitiesGrowthCount) GetMessengerMomentOk() (*GrowthCount, bool)`

GetMessengerMomentOk returns a tuple with the MessengerMoment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerMoment

`func (o *ActivitiesGrowthCount) SetMessengerMoment(v GrowthCount)`

SetMessengerMoment sets MessengerMoment field to given value.

### HasMessengerMoment

`func (o *ActivitiesGrowthCount) HasMessengerMoment() bool`

HasMessengerMoment returns a boolean if a field has been set.

### GetVisit

`func (o *ActivitiesGrowthCount) GetVisit() GrowthCount`

GetVisit returns the Visit field if non-nil, zero value otherwise.

### GetVisitOk

`func (o *ActivitiesGrowthCount) GetVisitOk() (*GrowthCount, bool)`

GetVisitOk returns a tuple with the Visit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVisit

`func (o *ActivitiesGrowthCount) SetVisit(v GrowthCount)`

SetVisit sets Visit field to given value.

### HasVisit

`func (o *ActivitiesGrowthCount) HasVisit() bool`

HasVisit returns a boolean if a field has been set.

### GetInvestment

`func (o *ActivitiesGrowthCount) GetInvestment() GrowthCount`

GetInvestment returns the Investment field if non-nil, zero value otherwise.

### GetInvestmentOk

`func (o *ActivitiesGrowthCount) GetInvestmentOk() (*GrowthCount, bool)`

GetInvestmentOk returns a tuple with the Investment field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInvestment

`func (o *ActivitiesGrowthCount) SetInvestment(v GrowthCount)`

SetInvestment sets Investment field to given value.

### HasInvestment

`func (o *ActivitiesGrowthCount) HasInvestment() bool`

HasInvestment returns a boolean if a field has been set.

### GetPurchase

`func (o *ActivitiesGrowthCount) GetPurchase() GrowthCount`

GetPurchase returns the Purchase field if non-nil, zero value otherwise.

### GetPurchaseOk

`func (o *ActivitiesGrowthCount) GetPurchaseOk() (*GrowthCount, bool)`

GetPurchaseOk returns a tuple with the Purchase field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPurchase

`func (o *ActivitiesGrowthCount) SetPurchase(v GrowthCount)`

SetPurchase sets Purchase field to given value.

### HasPurchase

`func (o *ActivitiesGrowthCount) HasPurchase() bool`

HasPurchase returns a boolean if a field has been set.

### GetReferral

`func (o *ActivitiesGrowthCount) GetReferral() GrowthCount`

GetReferral returns the Referral field if non-nil, zero value otherwise.

### GetReferralOk

`func (o *ActivitiesGrowthCount) GetReferralOk() (*GrowthCount, bool)`

GetReferralOk returns a tuple with the Referral field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferral

`func (o *ActivitiesGrowthCount) SetReferral(v GrowthCount)`

SetReferral sets Referral field to given value.

### HasReferral

`func (o *ActivitiesGrowthCount) HasReferral() bool`

HasReferral returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


