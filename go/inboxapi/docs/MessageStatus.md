# MessageStatus

## Enum


* `MESSAGE_STATUS_QUEUED` (value: `"queued"`)

* `MESSAGE_STATUS_RETRYING` (value: `"retrying"`)

* `MESSAGE_STATUS_ERROR` (value: `"error"`)

* `MESSAGE_STATUS_ERROR_PROVIDER` (value: `"error_provider"`)

* `MESSAGE_STATUS_SENT` (value: `"sent"`)

* `MESSAGE_STATUS_DELIVERED` (value: `"delivered"`)

* `MESSAGE_STATUS_READ` (value: `"read"`)

* `MESSAGE_STATUS_PLAYED` (value: `"played"`)

* `MESSAGE_STATUS_DELETED` (value: `"deleted"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


