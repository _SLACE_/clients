# WhatsAppTemplateMessageComponentParametersInnerDateTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FallbackValue** | Pointer to **string** |  | [optional] 
**Timestamp** | Pointer to **string** |  | [optional] 
**DayOfWeek** | Pointer to **string** |  | [optional] 
**DayOfMonth** | Pointer to **string** |  | [optional] 
**Year** | Pointer to **string** |  | [optional] 
**Month** | Pointer to **string** |  | [optional] 
**Hour** | Pointer to **string** |  | [optional] 
**Minute** | Pointer to **string** |  | [optional] 

## Methods

### NewWhatsAppTemplateMessageComponentParametersInnerDateTime

`func NewWhatsAppTemplateMessageComponentParametersInnerDateTime() *WhatsAppTemplateMessageComponentParametersInnerDateTime`

NewWhatsAppTemplateMessageComponentParametersInnerDateTime instantiates a new WhatsAppTemplateMessageComponentParametersInnerDateTime object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMessageComponentParametersInnerDateTimeWithDefaults

`func NewWhatsAppTemplateMessageComponentParametersInnerDateTimeWithDefaults() *WhatsAppTemplateMessageComponentParametersInnerDateTime`

NewWhatsAppTemplateMessageComponentParametersInnerDateTimeWithDefaults instantiates a new WhatsAppTemplateMessageComponentParametersInnerDateTime object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFallbackValue

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetFallbackValue() string`

GetFallbackValue returns the FallbackValue field if non-nil, zero value otherwise.

### GetFallbackValueOk

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetFallbackValueOk() (*string, bool)`

GetFallbackValueOk returns a tuple with the FallbackValue field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFallbackValue

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) SetFallbackValue(v string)`

SetFallbackValue sets FallbackValue field to given value.

### HasFallbackValue

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) HasFallbackValue() bool`

HasFallbackValue returns a boolean if a field has been set.

### GetTimestamp

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetTimestamp() string`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetTimestampOk() (*string, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) SetTimestamp(v string)`

SetTimestamp sets Timestamp field to given value.

### HasTimestamp

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) HasTimestamp() bool`

HasTimestamp returns a boolean if a field has been set.

### GetDayOfWeek

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetDayOfWeek() string`

GetDayOfWeek returns the DayOfWeek field if non-nil, zero value otherwise.

### GetDayOfWeekOk

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetDayOfWeekOk() (*string, bool)`

GetDayOfWeekOk returns a tuple with the DayOfWeek field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDayOfWeek

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) SetDayOfWeek(v string)`

SetDayOfWeek sets DayOfWeek field to given value.

### HasDayOfWeek

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) HasDayOfWeek() bool`

HasDayOfWeek returns a boolean if a field has been set.

### GetDayOfMonth

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetDayOfMonth() string`

GetDayOfMonth returns the DayOfMonth field if non-nil, zero value otherwise.

### GetDayOfMonthOk

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetDayOfMonthOk() (*string, bool)`

GetDayOfMonthOk returns a tuple with the DayOfMonth field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDayOfMonth

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) SetDayOfMonth(v string)`

SetDayOfMonth sets DayOfMonth field to given value.

### HasDayOfMonth

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) HasDayOfMonth() bool`

HasDayOfMonth returns a boolean if a field has been set.

### GetYear

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetYear() string`

GetYear returns the Year field if non-nil, zero value otherwise.

### GetYearOk

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetYearOk() (*string, bool)`

GetYearOk returns a tuple with the Year field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetYear

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) SetYear(v string)`

SetYear sets Year field to given value.

### HasYear

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) HasYear() bool`

HasYear returns a boolean if a field has been set.

### GetMonth

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetMonth() string`

GetMonth returns the Month field if non-nil, zero value otherwise.

### GetMonthOk

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetMonthOk() (*string, bool)`

GetMonthOk returns a tuple with the Month field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMonth

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) SetMonth(v string)`

SetMonth sets Month field to given value.

### HasMonth

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) HasMonth() bool`

HasMonth returns a boolean if a field has been set.

### GetHour

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetHour() string`

GetHour returns the Hour field if non-nil, zero value otherwise.

### GetHourOk

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetHourOk() (*string, bool)`

GetHourOk returns a tuple with the Hour field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHour

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) SetHour(v string)`

SetHour sets Hour field to given value.

### HasHour

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) HasHour() bool`

HasHour returns a boolean if a field has been set.

### GetMinute

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetMinute() string`

GetMinute returns the Minute field if non-nil, zero value otherwise.

### GetMinuteOk

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) GetMinuteOk() (*string, bool)`

GetMinuteOk returns a tuple with the Minute field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMinute

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) SetMinute(v string)`

SetMinute sets Minute field to given value.

### HasMinute

`func (o *WhatsAppTemplateMessageComponentParametersInnerDateTime) HasMinute() bool`

HasMinute returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


