# ActivityPatchableFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | Pointer to **string** |  | [optional] 
**Url** | Pointer to **string** |  | [optional] 
**MimeType** | Pointer to **string** |  | [optional] 

## Methods

### NewActivityPatchableFile

`func NewActivityPatchableFile() *ActivityPatchableFile`

NewActivityPatchableFile instantiates a new ActivityPatchableFile object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewActivityPatchableFileWithDefaults

`func NewActivityPatchableFileWithDefaults() *ActivityPatchableFile`

NewActivityPatchableFileWithDefaults instantiates a new ActivityPatchableFile object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *ActivityPatchableFile) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *ActivityPatchableFile) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *ActivityPatchableFile) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *ActivityPatchableFile) HasName() bool`

HasName returns a boolean if a field has been set.

### GetUrl

`func (o *ActivityPatchableFile) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *ActivityPatchableFile) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *ActivityPatchableFile) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *ActivityPatchableFile) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetMimeType

`func (o *ActivityPatchableFile) GetMimeType() string`

GetMimeType returns the MimeType field if non-nil, zero value otherwise.

### GetMimeTypeOk

`func (o *ActivityPatchableFile) GetMimeTypeOk() (*string, bool)`

GetMimeTypeOk returns a tuple with the MimeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMimeType

`func (o *ActivityPatchableFile) SetMimeType(v string)`

SetMimeType sets MimeType field to given value.

### HasMimeType

`func (o *ActivityPatchableFile) HasMimeType() bool`

HasMimeType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


