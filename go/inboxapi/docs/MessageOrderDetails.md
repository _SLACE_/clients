# MessageOrderDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PaymentConfiguration** | **string** |  | 
**ReferenceId** | Pointer to **string** |  | [optional] 
**Currency** | **string** |  | 
**Tax** | **string** |  | 
**Expiration** | Pointer to [**MessageOrderDetailsExpiration**](MessageOrderDetailsExpiration.md) |  | [optional] 
**Items** | [**[]MessageOrderDetailsItemsInner**](MessageOrderDetailsItemsInner.md) |  | 
**Discount** | Pointer to [**MessageOrderDetailsDiscount**](MessageOrderDetailsDiscount.md) |  | [optional] 

## Methods

### NewMessageOrderDetails

`func NewMessageOrderDetails(paymentConfiguration string, currency string, tax string, items []MessageOrderDetailsItemsInner, ) *MessageOrderDetails`

NewMessageOrderDetails instantiates a new MessageOrderDetails object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageOrderDetailsWithDefaults

`func NewMessageOrderDetailsWithDefaults() *MessageOrderDetails`

NewMessageOrderDetailsWithDefaults instantiates a new MessageOrderDetails object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPaymentConfiguration

`func (o *MessageOrderDetails) GetPaymentConfiguration() string`

GetPaymentConfiguration returns the PaymentConfiguration field if non-nil, zero value otherwise.

### GetPaymentConfigurationOk

`func (o *MessageOrderDetails) GetPaymentConfigurationOk() (*string, bool)`

GetPaymentConfigurationOk returns a tuple with the PaymentConfiguration field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPaymentConfiguration

`func (o *MessageOrderDetails) SetPaymentConfiguration(v string)`

SetPaymentConfiguration sets PaymentConfiguration field to given value.


### GetReferenceId

`func (o *MessageOrderDetails) GetReferenceId() string`

GetReferenceId returns the ReferenceId field if non-nil, zero value otherwise.

### GetReferenceIdOk

`func (o *MessageOrderDetails) GetReferenceIdOk() (*string, bool)`

GetReferenceIdOk returns a tuple with the ReferenceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferenceId

`func (o *MessageOrderDetails) SetReferenceId(v string)`

SetReferenceId sets ReferenceId field to given value.

### HasReferenceId

`func (o *MessageOrderDetails) HasReferenceId() bool`

HasReferenceId returns a boolean if a field has been set.

### GetCurrency

`func (o *MessageOrderDetails) GetCurrency() string`

GetCurrency returns the Currency field if non-nil, zero value otherwise.

### GetCurrencyOk

`func (o *MessageOrderDetails) GetCurrencyOk() (*string, bool)`

GetCurrencyOk returns a tuple with the Currency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrency

`func (o *MessageOrderDetails) SetCurrency(v string)`

SetCurrency sets Currency field to given value.


### GetTax

`func (o *MessageOrderDetails) GetTax() string`

GetTax returns the Tax field if non-nil, zero value otherwise.

### GetTaxOk

`func (o *MessageOrderDetails) GetTaxOk() (*string, bool)`

GetTaxOk returns a tuple with the Tax field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTax

`func (o *MessageOrderDetails) SetTax(v string)`

SetTax sets Tax field to given value.


### GetExpiration

`func (o *MessageOrderDetails) GetExpiration() MessageOrderDetailsExpiration`

GetExpiration returns the Expiration field if non-nil, zero value otherwise.

### GetExpirationOk

`func (o *MessageOrderDetails) GetExpirationOk() (*MessageOrderDetailsExpiration, bool)`

GetExpirationOk returns a tuple with the Expiration field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiration

`func (o *MessageOrderDetails) SetExpiration(v MessageOrderDetailsExpiration)`

SetExpiration sets Expiration field to given value.

### HasExpiration

`func (o *MessageOrderDetails) HasExpiration() bool`

HasExpiration returns a boolean if a field has been set.

### GetItems

`func (o *MessageOrderDetails) GetItems() []MessageOrderDetailsItemsInner`

GetItems returns the Items field if non-nil, zero value otherwise.

### GetItemsOk

`func (o *MessageOrderDetails) GetItemsOk() (*[]MessageOrderDetailsItemsInner, bool)`

GetItemsOk returns a tuple with the Items field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItems

`func (o *MessageOrderDetails) SetItems(v []MessageOrderDetailsItemsInner)`

SetItems sets Items field to given value.


### GetDiscount

`func (o *MessageOrderDetails) GetDiscount() MessageOrderDetailsDiscount`

GetDiscount returns the Discount field if non-nil, zero value otherwise.

### GetDiscountOk

`func (o *MessageOrderDetails) GetDiscountOk() (*MessageOrderDetailsDiscount, bool)`

GetDiscountOk returns a tuple with the Discount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDiscount

`func (o *MessageOrderDetails) SetDiscount(v MessageOrderDetailsDiscount)`

SetDiscount sets Discount field to given value.

### HasDiscount

`func (o *MessageOrderDetails) HasDiscount() bool`

HasDiscount returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


