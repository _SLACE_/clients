# ListContactCoupons200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**[]Coupon**](Coupon.md) |  | 
**Total** | **int32** |  | 

## Methods

### NewListContactCoupons200Response

`func NewListContactCoupons200Response(results []Coupon, total int32, ) *ListContactCoupons200Response`

NewListContactCoupons200Response instantiates a new ListContactCoupons200Response object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListContactCoupons200ResponseWithDefaults

`func NewListContactCoupons200ResponseWithDefaults() *ListContactCoupons200Response`

NewListContactCoupons200ResponseWithDefaults instantiates a new ListContactCoupons200Response object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetResults

`func (o *ListContactCoupons200Response) GetResults() []Coupon`

GetResults returns the Results field if non-nil, zero value otherwise.

### GetResultsOk

`func (o *ListContactCoupons200Response) GetResultsOk() (*[]Coupon, bool)`

GetResultsOk returns a tuple with the Results field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResults

`func (o *ListContactCoupons200Response) SetResults(v []Coupon)`

SetResults sets Results field to given value.


### GetTotal

`func (o *ListContactCoupons200Response) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *ListContactCoupons200Response) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *ListContactCoupons200Response) SetTotal(v int32)`

SetTotal sets Total field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


