# UploadMediaFileRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | **string** |  | 

## Methods

### NewUploadMediaFileRequest

`func NewUploadMediaFileRequest(url string, ) *UploadMediaFileRequest`

NewUploadMediaFileRequest instantiates a new UploadMediaFileRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUploadMediaFileRequestWithDefaults

`func NewUploadMediaFileRequestWithDefaults() *UploadMediaFileRequest`

NewUploadMediaFileRequestWithDefaults instantiates a new UploadMediaFileRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUrl

`func (o *UploadMediaFileRequest) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *UploadMediaFileRequest) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *UploadMediaFileRequest) SetUrl(v string)`

SetUrl sets Url field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


