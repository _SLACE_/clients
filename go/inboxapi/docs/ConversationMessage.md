# ConversationMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**GatewayId** | **string** |  | 
**ActivityId** | Pointer to **string** |  | [optional] 
**ConversationId** | **string** |  | 
**Conversation** | Pointer to [**Conversation**](Conversation.md) |  | [optional] 
**Author** | Pointer to [**CommunicationChannel**](CommunicationChannel.md) |  | [optional] 
**Text** | Pointer to **string** |  | [optional] 
**Type** | [**MessageType**](MessageType.md) |  | 
**Status** | [**MessageStatus**](MessageStatus.md) |  | 
**Direction** | **string** |  | 
**Source** | **string** |  | 
**Data** | Pointer to [**ConversationMessageData**](ConversationMessageData.md) |  | [optional] 
**ReplyTo** | Pointer to [**ConversationMessage**](ConversationMessage.md) |  | [optional] 
**Timestamp** | **time.Time** |  | 
**UserEditedAt** | Pointer to **time.Time** |  | [optional] 
**UserDeletedAt** | Pointer to **time.Time** |  | [optional] 
**Info** | Pointer to [**InfoMessageData**](InfoMessageData.md) |  | [optional] 
**Labels** | Pointer to **[]string** |  | [optional] 

## Methods

### NewConversationMessage

`func NewConversationMessage(id string, gatewayId string, conversationId string, type_ MessageType, status MessageStatus, direction string, source string, timestamp time.Time, ) *ConversationMessage`

NewConversationMessage instantiates a new ConversationMessage object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConversationMessageWithDefaults

`func NewConversationMessageWithDefaults() *ConversationMessage`

NewConversationMessageWithDefaults instantiates a new ConversationMessage object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ConversationMessage) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ConversationMessage) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ConversationMessage) SetId(v string)`

SetId sets Id field to given value.


### GetGatewayId

`func (o *ConversationMessage) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *ConversationMessage) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *ConversationMessage) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.


### GetActivityId

`func (o *ConversationMessage) GetActivityId() string`

GetActivityId returns the ActivityId field if non-nil, zero value otherwise.

### GetActivityIdOk

`func (o *ConversationMessage) GetActivityIdOk() (*string, bool)`

GetActivityIdOk returns a tuple with the ActivityId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActivityId

`func (o *ConversationMessage) SetActivityId(v string)`

SetActivityId sets ActivityId field to given value.

### HasActivityId

`func (o *ConversationMessage) HasActivityId() bool`

HasActivityId returns a boolean if a field has been set.

### GetConversationId

`func (o *ConversationMessage) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *ConversationMessage) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *ConversationMessage) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.


### GetConversation

`func (o *ConversationMessage) GetConversation() Conversation`

GetConversation returns the Conversation field if non-nil, zero value otherwise.

### GetConversationOk

`func (o *ConversationMessage) GetConversationOk() (*Conversation, bool)`

GetConversationOk returns a tuple with the Conversation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversation

`func (o *ConversationMessage) SetConversation(v Conversation)`

SetConversation sets Conversation field to given value.

### HasConversation

`func (o *ConversationMessage) HasConversation() bool`

HasConversation returns a boolean if a field has been set.

### GetAuthor

`func (o *ConversationMessage) GetAuthor() CommunicationChannel`

GetAuthor returns the Author field if non-nil, zero value otherwise.

### GetAuthorOk

`func (o *ConversationMessage) GetAuthorOk() (*CommunicationChannel, bool)`

GetAuthorOk returns a tuple with the Author field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthor

`func (o *ConversationMessage) SetAuthor(v CommunicationChannel)`

SetAuthor sets Author field to given value.

### HasAuthor

`func (o *ConversationMessage) HasAuthor() bool`

HasAuthor returns a boolean if a field has been set.

### GetText

`func (o *ConversationMessage) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *ConversationMessage) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *ConversationMessage) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *ConversationMessage) HasText() bool`

HasText returns a boolean if a field has been set.

### GetType

`func (o *ConversationMessage) GetType() MessageType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ConversationMessage) GetTypeOk() (*MessageType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ConversationMessage) SetType(v MessageType)`

SetType sets Type field to given value.


### GetStatus

`func (o *ConversationMessage) GetStatus() MessageStatus`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *ConversationMessage) GetStatusOk() (*MessageStatus, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *ConversationMessage) SetStatus(v MessageStatus)`

SetStatus sets Status field to given value.


### GetDirection

`func (o *ConversationMessage) GetDirection() string`

GetDirection returns the Direction field if non-nil, zero value otherwise.

### GetDirectionOk

`func (o *ConversationMessage) GetDirectionOk() (*string, bool)`

GetDirectionOk returns a tuple with the Direction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDirection

`func (o *ConversationMessage) SetDirection(v string)`

SetDirection sets Direction field to given value.


### GetSource

`func (o *ConversationMessage) GetSource() string`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *ConversationMessage) GetSourceOk() (*string, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *ConversationMessage) SetSource(v string)`

SetSource sets Source field to given value.


### GetData

`func (o *ConversationMessage) GetData() ConversationMessageData`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *ConversationMessage) GetDataOk() (*ConversationMessageData, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *ConversationMessage) SetData(v ConversationMessageData)`

SetData sets Data field to given value.

### HasData

`func (o *ConversationMessage) HasData() bool`

HasData returns a boolean if a field has been set.

### GetReplyTo

`func (o *ConversationMessage) GetReplyTo() ConversationMessage`

GetReplyTo returns the ReplyTo field if non-nil, zero value otherwise.

### GetReplyToOk

`func (o *ConversationMessage) GetReplyToOk() (*ConversationMessage, bool)`

GetReplyToOk returns a tuple with the ReplyTo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReplyTo

`func (o *ConversationMessage) SetReplyTo(v ConversationMessage)`

SetReplyTo sets ReplyTo field to given value.

### HasReplyTo

`func (o *ConversationMessage) HasReplyTo() bool`

HasReplyTo returns a boolean if a field has been set.

### GetTimestamp

`func (o *ConversationMessage) GetTimestamp() time.Time`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *ConversationMessage) GetTimestampOk() (*time.Time, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *ConversationMessage) SetTimestamp(v time.Time)`

SetTimestamp sets Timestamp field to given value.


### GetUserEditedAt

`func (o *ConversationMessage) GetUserEditedAt() time.Time`

GetUserEditedAt returns the UserEditedAt field if non-nil, zero value otherwise.

### GetUserEditedAtOk

`func (o *ConversationMessage) GetUserEditedAtOk() (*time.Time, bool)`

GetUserEditedAtOk returns a tuple with the UserEditedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserEditedAt

`func (o *ConversationMessage) SetUserEditedAt(v time.Time)`

SetUserEditedAt sets UserEditedAt field to given value.

### HasUserEditedAt

`func (o *ConversationMessage) HasUserEditedAt() bool`

HasUserEditedAt returns a boolean if a field has been set.

### GetUserDeletedAt

`func (o *ConversationMessage) GetUserDeletedAt() time.Time`

GetUserDeletedAt returns the UserDeletedAt field if non-nil, zero value otherwise.

### GetUserDeletedAtOk

`func (o *ConversationMessage) GetUserDeletedAtOk() (*time.Time, bool)`

GetUserDeletedAtOk returns a tuple with the UserDeletedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserDeletedAt

`func (o *ConversationMessage) SetUserDeletedAt(v time.Time)`

SetUserDeletedAt sets UserDeletedAt field to given value.

### HasUserDeletedAt

`func (o *ConversationMessage) HasUserDeletedAt() bool`

HasUserDeletedAt returns a boolean if a field has been set.

### GetInfo

`func (o *ConversationMessage) GetInfo() InfoMessageData`

GetInfo returns the Info field if non-nil, zero value otherwise.

### GetInfoOk

`func (o *ConversationMessage) GetInfoOk() (*InfoMessageData, bool)`

GetInfoOk returns a tuple with the Info field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInfo

`func (o *ConversationMessage) SetInfo(v InfoMessageData)`

SetInfo sets Info field to given value.

### HasInfo

`func (o *ConversationMessage) HasInfo() bool`

HasInfo returns a boolean if a field has been set.

### GetLabels

`func (o *ConversationMessage) GetLabels() []string`

GetLabels returns the Labels field if non-nil, zero value otherwise.

### GetLabelsOk

`func (o *ConversationMessage) GetLabelsOk() (*[]string, bool)`

GetLabelsOk returns a tuple with the Labels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabels

`func (o *ConversationMessage) SetLabels(v []string)`

SetLabels sets Labels field to given value.

### HasLabels

`func (o *ConversationMessage) HasLabels() bool`

HasLabels returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


