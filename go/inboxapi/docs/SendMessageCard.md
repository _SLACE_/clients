# SendMessageCard

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Text** | **string** |  | 
**MediaId** | Pointer to **string** |  | [optional] 
**MediaUrl** | Pointer to **string** |  | [optional] 
**Buttons** | Pointer to [**[]MessageButton**](MessageButton.md) |  | [optional] 

## Methods

### NewSendMessageCard

`func NewSendMessageCard(text string, ) *SendMessageCard`

NewSendMessageCard instantiates a new SendMessageCard object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSendMessageCardWithDefaults

`func NewSendMessageCardWithDefaults() *SendMessageCard`

NewSendMessageCardWithDefaults instantiates a new SendMessageCard object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetText

`func (o *SendMessageCard) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *SendMessageCard) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *SendMessageCard) SetText(v string)`

SetText sets Text field to given value.


### GetMediaId

`func (o *SendMessageCard) GetMediaId() string`

GetMediaId returns the MediaId field if non-nil, zero value otherwise.

### GetMediaIdOk

`func (o *SendMessageCard) GetMediaIdOk() (*string, bool)`

GetMediaIdOk returns a tuple with the MediaId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaId

`func (o *SendMessageCard) SetMediaId(v string)`

SetMediaId sets MediaId field to given value.

### HasMediaId

`func (o *SendMessageCard) HasMediaId() bool`

HasMediaId returns a boolean if a field has been set.

### GetMediaUrl

`func (o *SendMessageCard) GetMediaUrl() string`

GetMediaUrl returns the MediaUrl field if non-nil, zero value otherwise.

### GetMediaUrlOk

`func (o *SendMessageCard) GetMediaUrlOk() (*string, bool)`

GetMediaUrlOk returns a tuple with the MediaUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaUrl

`func (o *SendMessageCard) SetMediaUrl(v string)`

SetMediaUrl sets MediaUrl field to given value.

### HasMediaUrl

`func (o *SendMessageCard) HasMediaUrl() bool`

HasMediaUrl returns a boolean if a field has been set.

### GetButtons

`func (o *SendMessageCard) GetButtons() []MessageButton`

GetButtons returns the Buttons field if non-nil, zero value otherwise.

### GetButtonsOk

`func (o *SendMessageCard) GetButtonsOk() (*[]MessageButton, bool)`

GetButtonsOk returns a tuple with the Buttons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtons

`func (o *SendMessageCard) SetButtons(v []MessageButton)`

SetButtons sets Buttons field to given value.

### HasButtons

`func (o *SendMessageCard) HasButtons() bool`

HasButtons returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


