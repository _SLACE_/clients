# ContactIncludeRelation

## Enum


* `ContactIncludeRelationOrganization` (value: `"Organization"`)

* `ContactIncludeRelationSegments` (value: `"Segments"`)

* `ContactIncludeRelationComChannels` (value: `"ComChannels"`)

* `ContactIncludeRelationPlatformComChannels` (value: `"PlatformComChannels"`)

* `ContactIncludeRelationCoupons` (value: `"Coupons"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


