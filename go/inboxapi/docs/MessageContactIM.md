# MessageContactIM

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Service** | Pointer to **string** |  | [optional] 
**UserId** | Pointer to **string** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageContactIM

`func NewMessageContactIM() *MessageContactIM`

NewMessageContactIM instantiates a new MessageContactIM object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageContactIMWithDefaults

`func NewMessageContactIMWithDefaults() *MessageContactIM`

NewMessageContactIMWithDefaults instantiates a new MessageContactIM object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetService

`func (o *MessageContactIM) GetService() string`

GetService returns the Service field if non-nil, zero value otherwise.

### GetServiceOk

`func (o *MessageContactIM) GetServiceOk() (*string, bool)`

GetServiceOk returns a tuple with the Service field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetService

`func (o *MessageContactIM) SetService(v string)`

SetService sets Service field to given value.

### HasService

`func (o *MessageContactIM) HasService() bool`

HasService returns a boolean if a field has been set.

### GetUserId

`func (o *MessageContactIM) GetUserId() string`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *MessageContactIM) GetUserIdOk() (*string, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *MessageContactIM) SetUserId(v string)`

SetUserId sets UserId field to given value.

### HasUserId

`func (o *MessageContactIM) HasUserId() bool`

HasUserId returns a boolean if a field has been set.

### GetType

`func (o *MessageContactIM) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MessageContactIM) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MessageContactIM) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *MessageContactIM) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


