# ReceiverType

## Enum


* `RECEIVER_NATIVE_MESSENGER_ID` (value: `0`)

* `RECEIVER_COMMUNICATION_CHANNEL` (value: `1`)

* `RECEIVER_CONVERSATION` (value: `2`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


