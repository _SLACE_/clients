# \MediaAPI

All URIs are relative to *https://api.slace.com/inbox*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetMediaFile**](MediaAPI.md#GetMediaFile) | **Get** /channels/{channel_id}/gateways/{gateway_id}/media/{media_id} | Get media file
[**UploadMediaFile**](MediaAPI.md#UploadMediaFile) | **Post** /channels/{channel_id}/gateways/{gateway_id}/media | Upload media file



## GetMediaFile

> MediaFile GetMediaFile(ctx, channelId, gatewayId, mediaId).Execute()

Get media file



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    gatewayId := "gatewayId_example" // string | 
    mediaId := "mediaId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaAPI.GetMediaFile(context.Background(), channelId, gatewayId, mediaId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaAPI.GetMediaFile``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMediaFile`: MediaFile
    fmt.Fprintf(os.Stdout, "Response from `MediaAPI.GetMediaFile`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**gatewayId** | **string** |  | 
**mediaId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetMediaFileRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**MediaFile**](MediaFile.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UploadMediaFile

> MediaFile UploadMediaFile(ctx, channelId, gatewayId).UploadMediaFileRequest(uploadMediaFileRequest).Execute()

Upload media file



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    gatewayId := "gatewayId_example" // string | 
    uploadMediaFileRequest := *openapiclient.NewUploadMediaFileRequest("Url_example") // UploadMediaFileRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MediaAPI.UploadMediaFile(context.Background(), channelId, gatewayId).UploadMediaFileRequest(uploadMediaFileRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MediaAPI.UploadMediaFile``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UploadMediaFile`: MediaFile
    fmt.Fprintf(os.Stdout, "Response from `MediaAPI.UploadMediaFile`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUploadMediaFileRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **uploadMediaFileRequest** | [**UploadMediaFileRequest**](UploadMediaFileRequest.md) |  | 

### Return type

[**MediaFile**](MediaFile.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json, multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

