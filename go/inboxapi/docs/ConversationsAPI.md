# \ConversationsAPI

All URIs are relative to *https://api.slace.com/inbox*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddConversationMessageLabels**](ConversationsAPI.md#AddConversationMessageLabels) | **Put** /channels/{channel_id}/conversations/{cid}/messages/{msg_id}/labels | Add message labels
[**AddLabels**](ConversationsAPI.md#AddLabels) | **Put** /channels/{channel_id}/conversations/{cid}/labels | Add labels
[**AgentTakeover**](ConversationsAPI.md#AgentTakeover) | **Post** /channels/{channel_id}/conversations/{cid}/agent-takeover | Agent takover
[**AgentTakeoverRelease**](ConversationsAPI.md#AgentTakeoverRelease) | **Delete** /channels/{channel_id}/conversations/{cid}/agent-takeover | Agent takeover release
[**AttachTransactionToConversation**](ConversationsAPI.md#AttachTransactionToConversation) | **Put** /channels/{channel_id}/conversations/{conversation_id}/transaction/{transaction_id} | Attach transaction to conversation
[**ConversationBlocking**](ConversationsAPI.md#ConversationBlocking) | **Post** /channels/{channel_id}/conversations/{conversation_id}/blocking | Block conversation
[**ConversationOptOut**](ConversationsAPI.md#ConversationOptOut) | **Post** /channels/{channel_id}/conversations/{cid}/opt-out | Conversation opt out
[**CreateAgentRequest**](ConversationsAPI.md#CreateAgentRequest) | **Post** /channels/{channel_id}/conversations/{conversation_id}/agent-request | Create Agent Request
[**DeleteConversation**](ConversationsAPI.md#DeleteConversation) | **Delete** /channels/{channel_id}/conversations/{cid} | Delete conversation
[**DeleteConversationMessage**](ConversationsAPI.md#DeleteConversationMessage) | **Delete** /channels/{channel_id}/conversations/{cid}/messages/{msg_id} | Delete message
[**DeleteConversationMessageLabel**](ConversationsAPI.md#DeleteConversationMessageLabel) | **Delete** /channels/{channel_id}/conversations/{cid}/messages/{msg_id}/labels/{label} | Delete message label
[**DeleteLabel**](ConversationsAPI.md#DeleteLabel) | **Delete** /channels/{channel_id}/conversations/{cid}/labels/{label} | Delete label
[**DeleteLabels**](ConversationsAPI.md#DeleteLabels) | **Delete** /channels/{channel_id}/conversations/{cid}/labels | Delete labels
[**DetachTransactionFromConversation**](ConversationsAPI.md#DetachTransactionFromConversation) | **Delete** /channels/{channel_id}/conversations/{conversation_id}/transaction/{transaction_id} | Detach transaction from conversation
[**FindConversationsSegments**](ConversationsAPI.md#FindConversationsSegments) | **Post** /channels/{channel_id}/segments | Find conversation segments
[**GetConversationById**](ConversationsAPI.md#GetConversationById) | **Get** /channels/{channel_id}/conversations/{cid} | Get conversation by ID
[**GetConversationMedia**](ConversationsAPI.md#GetConversationMedia) | **Get** /channels/{channel_id}/conversations/{conversation_id}/media | Get media
[**GetConversationMessage**](ConversationsAPI.md#GetConversationMessage) | **Get** /channels/{channel_id}/conversations/{cid}/messages/{msg_id} | Get message
[**GetConversationMessages**](ConversationsAPI.md#GetConversationMessages) | **Get** /channels/{channel_id}/conversations/{cid}/messages | Get messages
[**GetConversations**](ConversationsAPI.md#GetConversations) | **Get** /channels/{channel_id}/conversations | Get conversations
[**MarkAsRead**](ConversationsAPI.md#MarkAsRead) | **Post** /channels/{channel_id}/conversations/{conversation_id}/read | Mark conversation as read
[**MarkAsUnread**](ConversationsAPI.md#MarkAsUnread) | **Delete** /channels/{channel_id}/conversations/{conversation_id}/read | Mark conversation as unread
[**MarkConversationAsRead**](ConversationsAPI.md#MarkConversationAsRead) | **Post** /channels/{channel_id}/conversations/{cid}/read | Mark conversation as read
[**ResolveAgentRequest**](ConversationsAPI.md#ResolveAgentRequest) | **Delete** /channels/{channel_id}/conversations/{conversation_id}/agent-request | Resolve Agent Request
[**TranslateConversationParams**](ConversationsAPI.md#TranslateConversationParams) | **Post** /channels/{channel_id}/conversations/{cid}/translate-params | Translate conversation params



## AddConversationMessageLabels

> ConversationMessage AddConversationMessageLabels(ctx, channelId, cid, msgId).LabelsData(labelsData).Execute()

Add message labels



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | channel ID
    cid := "cid_example" // string | conversation ID
    msgId := "msgId_example" // string | message ID
    labelsData := *openapiclient.NewLabelsData() // LabelsData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.AddConversationMessageLabels(context.Background(), channelId, cid, msgId).LabelsData(labelsData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.AddConversationMessageLabels``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddConversationMessageLabels`: ConversationMessage
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.AddConversationMessageLabels`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** | channel ID | 
**cid** | **string** | conversation ID | 
**msgId** | **string** | message ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddConversationMessageLabelsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **labelsData** | [**LabelsData**](LabelsData.md) |  | 

### Return type

[**ConversationMessage**](ConversationMessage.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddLabels

> Conversation AddLabels(ctx, channelId, cid).LabelsData(labelsData).Execute()

Add labels

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | channel ID
    cid := "cid_example" // string | conversation ID
    labelsData := *openapiclient.NewLabelsData() // LabelsData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.AddLabels(context.Background(), channelId, cid).LabelsData(labelsData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.AddLabels``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AddLabels`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.AddLabels`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** | channel ID | 
**cid** | **string** | conversation ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiAddLabelsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **labelsData** | [**LabelsData**](LabelsData.md) |  | 

### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AgentTakeover

> Conversation AgentTakeover(ctx, channelId, cid).Execute()

Agent takover



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    cid := "cid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.AgentTakeover(context.Background(), channelId, cid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.AgentTakeover``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AgentTakeover`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.AgentTakeover`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**cid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAgentTakeoverRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AgentTakeoverRelease

> Conversation AgentTakeoverRelease(ctx, channelId, cid).Execute()

Agent takeover release



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    cid := "cid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.AgentTakeoverRelease(context.Background(), channelId, cid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.AgentTakeoverRelease``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AgentTakeoverRelease`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.AgentTakeoverRelease`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**cid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAgentTakeoverReleaseRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AttachTransactionToConversation

> Conversation AttachTransactionToConversation(ctx, channelId, conversationId, transactionId).Execute()

Attach transaction to conversation

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    conversationId := "conversationId_example" // string | 
    transactionId := "transactionId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.AttachTransactionToConversation(context.Background(), channelId, conversationId, transactionId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.AttachTransactionToConversation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AttachTransactionToConversation`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.AttachTransactionToConversation`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**conversationId** | **string** |  | 
**transactionId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAttachTransactionToConversationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ConversationBlocking

> Conversation ConversationBlocking(ctx, conversationId, channelId).ConversationBlockingData(conversationBlockingData).Execute()

Block conversation



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    conversationId := "conversationId_example" // string | 
    channelId := "channelId_example" // string | 
    conversationBlockingData := *openapiclient.NewConversationBlockingData(false) // ConversationBlockingData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.ConversationBlocking(context.Background(), conversationId, channelId).ConversationBlockingData(conversationBlockingData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.ConversationBlocking``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ConversationBlocking`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.ConversationBlocking`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**conversationId** | **string** |  | 
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiConversationBlockingRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **conversationBlockingData** | [**ConversationBlockingData**](ConversationBlockingData.md) |  | 

### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ConversationOptOut

> Conversation ConversationOptOut(ctx, channelId, cid).Execute()

Conversation opt out



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    cid := "cid_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.ConversationOptOut(context.Background(), channelId, cid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.ConversationOptOut``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ConversationOptOut`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.ConversationOptOut`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**cid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiConversationOptOutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateAgentRequest

> Conversation CreateAgentRequest(ctx, channelId, conversationId).AgentRequestData(agentRequestData).Execute()

Create Agent Request

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    conversationId := "conversationId_example" // string | 
    agentRequestData := *openapiclient.NewAgentRequestData("Source_example") // AgentRequestData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.CreateAgentRequest(context.Background(), channelId, conversationId).AgentRequestData(agentRequestData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.CreateAgentRequest``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateAgentRequest`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.CreateAgentRequest`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**conversationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateAgentRequestRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **agentRequestData** | [**AgentRequestData**](AgentRequestData.md) |  | 

### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteConversation

> GenericResponse DeleteConversation(ctx, channelId, cid).Execute()

Delete conversation



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | channel ID
    cid := "cid_example" // string | conversation ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.DeleteConversation(context.Background(), channelId, cid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.DeleteConversation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteConversation`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.DeleteConversation`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** | channel ID | 
**cid** | **string** | conversation ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteConversationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteConversationMessage

> GenericResponse DeleteConversationMessage(ctx, channelId, cid, msgId).Execute()

Delete message



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | channel ID
    cid := "cid_example" // string | conversation ID
    msgId := "msgId_example" // string | message ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.DeleteConversationMessage(context.Background(), channelId, cid, msgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.DeleteConversationMessage``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteConversationMessage`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.DeleteConversationMessage`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** | channel ID | 
**cid** | **string** | conversation ID | 
**msgId** | **string** | message ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteConversationMessageRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteConversationMessageLabel

> ConversationMessage DeleteConversationMessageLabel(ctx, channelId, cid, msgId, label).Execute()

Delete message label



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | channel ID
    cid := "cid_example" // string | conversation ID
    msgId := "msgId_example" // string | message ID
    label := "label_example" // string | label

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.DeleteConversationMessageLabel(context.Background(), channelId, cid, msgId, label).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.DeleteConversationMessageLabel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteConversationMessageLabel`: ConversationMessage
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.DeleteConversationMessageLabel`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** | channel ID | 
**cid** | **string** | conversation ID | 
**msgId** | **string** | message ID | 
**label** | **string** | label | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteConversationMessageLabelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------





### Return type

[**ConversationMessage**](ConversationMessage.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteLabel

> Conversation DeleteLabel(ctx, channelId, cid, label).Execute()

Delete label

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | channel ID
    cid := "cid_example" // string | conversation ID
    label := "label_example" // string | label

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.DeleteLabel(context.Background(), channelId, cid, label).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.DeleteLabel``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteLabel`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.DeleteLabel`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** | channel ID | 
**cid** | **string** | conversation ID | 
**label** | **string** | label | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteLabelRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteLabels

> Conversation DeleteLabels(ctx, channelId, cid).LabelsData(labelsData).Execute()

Delete labels

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | channel ID
    cid := "cid_example" // string | conversation ID
    labelsData := *openapiclient.NewLabelsData() // LabelsData |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.DeleteLabels(context.Background(), channelId, cid).LabelsData(labelsData).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.DeleteLabels``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteLabels`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.DeleteLabels`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** | channel ID | 
**cid** | **string** | conversation ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteLabelsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **labelsData** | [**LabelsData**](LabelsData.md) |  | 

### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DetachTransactionFromConversation

> DetachTransactionFromConversation(ctx, channelId, conversationId, transactionId).Execute()

Detach transaction from conversation

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    conversationId := "conversationId_example" // string | 
    transactionId := "transactionId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    r, err := apiClient.ConversationsAPI.DetachTransactionFromConversation(context.Background(), channelId, conversationId, transactionId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.DetachTransactionFromConversation``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**conversationId** | **string** |  | 
**transactionId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiDetachTransactionFromConversationRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

 (empty response body)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## FindConversationsSegments

> []FindConversationsSegments200ResponseInner FindConversationsSegments(ctx, channelId).FindConversationsSegmentsRequest(findConversationsSegmentsRequest).Execute()

Find conversation segments

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    findConversationsSegmentsRequest := *openapiclient.NewFindConversationsSegmentsRequest([]string{"ConversationIds_example"}) // FindConversationsSegmentsRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.FindConversationsSegments(context.Background(), channelId).FindConversationsSegmentsRequest(findConversationsSegmentsRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.FindConversationsSegments``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `FindConversationsSegments`: []FindConversationsSegments200ResponseInner
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.FindConversationsSegments`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiFindConversationsSegmentsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **findConversationsSegmentsRequest** | [**FindConversationsSegmentsRequest**](FindConversationsSegmentsRequest.md) |  | 

### Return type

[**[]FindConversationsSegments200ResponseInner**](FindConversationsSegments200ResponseInner.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetConversationById

> Conversation GetConversationById(ctx, channelId, cid).Execute()

Get conversation by ID



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | channel ID
    cid := "cid_example" // string | conversation ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.GetConversationById(context.Background(), channelId, cid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.GetConversationById``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetConversationById`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.GetConversationById`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** | channel ID | 
**cid** | **string** | conversation ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetConversationByIdRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetConversationMedia

> ConversationMessagesList GetConversationMedia(ctx, channelId, conversationId).Labels(labels).After(after).Before(before).Offset(offset).Limit(limit).Sort(sort).Execute()

Get media



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    conversationId := "conversationId_example" // string | 
    labels := []string{"Inner_example"} // []string |  (optional)
    after := "after_example" // string |  (optional)
    before := "before_example" // string |  (optional)
    offset := int32(56) // int32 |  (optional)
    limit := int32(56) // int32 |  (optional)
    sort := "sort_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.GetConversationMedia(context.Background(), channelId, conversationId).Labels(labels).After(after).Before(before).Offset(offset).Limit(limit).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.GetConversationMedia``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetConversationMedia`: ConversationMessagesList
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.GetConversationMedia`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**conversationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetConversationMediaRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **labels** | **[]string** |  | 
 **after** | **string** |  | 
 **before** | **string** |  | 
 **offset** | **int32** |  | 
 **limit** | **int32** |  | 
 **sort** | **string** |  | 

### Return type

[**ConversationMessagesList**](ConversationMessagesList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetConversationMessage

> ConversationMessage GetConversationMessage(ctx, channelId, cid, msgId).Execute()

Get message



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | channel ID
    cid := "cid_example" // string | conversation ID
    msgId := "msgId_example" // string | message ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.GetConversationMessage(context.Background(), channelId, cid, msgId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.GetConversationMessage``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetConversationMessage`: ConversationMessage
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.GetConversationMessage`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** | channel ID | 
**cid** | **string** | conversation ID | 
**msgId** | **string** | message ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetConversationMessageRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**ConversationMessage**](ConversationMessage.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetConversationMessages

> ConversationMessagesList GetConversationMessages(ctx, channelId, cid).Labels(labels).After(after).Before(before).Offset(offset).Limit(limit).Sort(sort).ActivityId(activityId).Types(types).Execute()

Get messages



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | channel ID
    cid := "cid_example" // string | conversation ID
    labels := []string{"Inner_example"} // []string |  (optional)
    after := time.Now() // time.Time |  (optional)
    before := time.Now() // time.Time |  (optional)
    offset := int32(56) // int32 |  (optional)
    limit := int32(56) // int32 |  (optional)
    sort := "sort_example" // string |  (optional)
    activityId := "activityId_example" // string |  (optional)
    types := []openapiclient.MessageType{openapiclient.MessageType("text")} // []MessageType |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.GetConversationMessages(context.Background(), channelId, cid).Labels(labels).After(after).Before(before).Offset(offset).Limit(limit).Sort(sort).ActivityId(activityId).Types(types).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.GetConversationMessages``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetConversationMessages`: ConversationMessagesList
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.GetConversationMessages`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** | channel ID | 
**cid** | **string** | conversation ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetConversationMessagesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **labels** | **[]string** |  | 
 **after** | **time.Time** |  | 
 **before** | **time.Time** |  | 
 **offset** | **int32** |  | 
 **limit** | **int32** |  | 
 **sort** | **string** |  | 
 **activityId** | **string** |  | 
 **types** | [**[]MessageType**](MessageType.md) |  | 

### Return type

[**ConversationMessagesList**](ConversationMessagesList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetConversations

> ConversationsList GetConversations(ctx, channelId).Offset(offset).Limit(limit).Sort(sort).After(after).Before(before).AfterId(afterId).BeforeId(beforeId).GatewayId(gatewayId).Search(search).OptedIn(optedIn).Blocked(blocked).BlockedInfo(blockedInfo).ComChannelType(comChannelType).MessengerTypes(messengerTypes).Labels(labels).LabelsAny(labelsAny).IxAll(ixAll).IxAny(ixAny).EpAll(epAll).EpAny(epAny).SegmentsAll(segmentsAll).SegmentsAny(segmentsAny).ContactId(contactId).Locales(locales).AgentRequested(agentRequested).AgentTaken(agentTaken).FlagsAny(flagsAny).FlagsAll(flagsAll).WithTotal(withTotal).Execute()

Get conversations



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    offset := int32(56) // int32 |  (optional)
    limit := int32(56) // int32 |  (optional)
    sort := "sort_example" // string |  (optional)
    after := "after_example" // string |  (optional)
    before := "before_example" // string |  (optional)
    afterId := "afterId_example" // string |  (optional)
    beforeId := "beforeId_example" // string |  (optional)
    gatewayId := "gatewayId_example" // string |  (optional)
    search := "search_example" // string |  (optional)
    optedIn := true // bool |  (optional)
    blocked := true // bool |  (optional)
    blockedInfo := "blockedInfo_example" // string |  (optional)
    comChannelType := "comChannelType_example" // string |  (optional)
    messengerTypes := []string{"Inner_example"} // []string |  (optional)
    labels := []string{"Inner_example"} // []string |  (optional)
    labelsAny := []string{"Inner_example"} // []string |  (optional)
    ixAll := []string{"Inner_example"} // []string | filter by interaction ids with and (optional)
    ixAny := []string{"Inner_example"} // []string | filter by interaction ids with or (optional)
    epAll := []string{"Inner_example"} // []string | filter by entry point ids with and (optional)
    epAny := []string{"Inner_example"} // []string | filter by entry point ids with or (optional)
    segmentsAll := []string{"Inner_example"} // []string |  (optional)
    segmentsAny := []string{"Inner_example"} // []string |  (optional)
    contactId := "contactId_example" // string |  (optional)
    locales := "locales_example" // string | filter by locales (optional)
    agentRequested := true // bool | conversations that require human agent attention (optional)
    agentTaken := true // bool | filter by conversations taken over by human agent (optional)
    flagsAny := []string{"Inner_example"} // []string |  (optional)
    flagsAll := []string{"Inner_example"} // []string |  (optional)
    withTotal := true // bool |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.GetConversations(context.Background(), channelId).Offset(offset).Limit(limit).Sort(sort).After(after).Before(before).AfterId(afterId).BeforeId(beforeId).GatewayId(gatewayId).Search(search).OptedIn(optedIn).Blocked(blocked).BlockedInfo(blockedInfo).ComChannelType(comChannelType).MessengerTypes(messengerTypes).Labels(labels).LabelsAny(labelsAny).IxAll(ixAll).IxAny(ixAny).EpAll(epAll).EpAny(epAny).SegmentsAll(segmentsAll).SegmentsAny(segmentsAny).ContactId(contactId).Locales(locales).AgentRequested(agentRequested).AgentTaken(agentTaken).FlagsAny(flagsAny).FlagsAll(flagsAll).WithTotal(withTotal).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.GetConversations``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetConversations`: ConversationsList
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.GetConversations`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetConversationsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **offset** | **int32** |  | 
 **limit** | **int32** |  | 
 **sort** | **string** |  | 
 **after** | **string** |  | 
 **before** | **string** |  | 
 **afterId** | **string** |  | 
 **beforeId** | **string** |  | 
 **gatewayId** | **string** |  | 
 **search** | **string** |  | 
 **optedIn** | **bool** |  | 
 **blocked** | **bool** |  | 
 **blockedInfo** | **string** |  | 
 **comChannelType** | **string** |  | 
 **messengerTypes** | **[]string** |  | 
 **labels** | **[]string** |  | 
 **labelsAny** | **[]string** |  | 
 **ixAll** | **[]string** | filter by interaction ids with and | 
 **ixAny** | **[]string** | filter by interaction ids with or | 
 **epAll** | **[]string** | filter by entry point ids with and | 
 **epAny** | **[]string** | filter by entry point ids with or | 
 **segmentsAll** | **[]string** |  | 
 **segmentsAny** | **[]string** |  | 
 **contactId** | **string** |  | 
 **locales** | **string** | filter by locales | 
 **agentRequested** | **bool** | conversations that require human agent attention | 
 **agentTaken** | **bool** | filter by conversations taken over by human agent | 
 **flagsAny** | **[]string** |  | 
 **flagsAll** | **[]string** |  | 
 **withTotal** | **bool** |  | 

### Return type

[**ConversationsList**](ConversationsList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## MarkAsRead

> Conversation MarkAsRead(ctx, channelId, conversationId).Execute()

Mark conversation as read

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    conversationId := "conversationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.MarkAsRead(context.Background(), channelId, conversationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.MarkAsRead``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `MarkAsRead`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.MarkAsRead`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**conversationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiMarkAsReadRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## MarkAsUnread

> Conversation MarkAsUnread(ctx, channelId, conversationId).Execute()

Mark conversation as unread

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    conversationId := "conversationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.MarkAsUnread(context.Background(), channelId, conversationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.MarkAsUnread``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `MarkAsUnread`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.MarkAsUnread`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**conversationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiMarkAsUnreadRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## MarkConversationAsRead

> Conversation MarkConversationAsRead(ctx, channelId, cid).Execute()

Mark conversation as read

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | channel ID
    cid := "cid_example" // string | conversation ID

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.MarkConversationAsRead(context.Background(), channelId, cid).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.MarkConversationAsRead``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `MarkConversationAsRead`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.MarkConversationAsRead`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** | channel ID | 
**cid** | **string** | conversation ID | 

### Other Parameters

Other parameters are passed through a pointer to a apiMarkConversationAsReadRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ResolveAgentRequest

> Conversation ResolveAgentRequest(ctx, channelId, conversationId).Execute()

Resolve Agent Request

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    conversationId := "conversationId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.ResolveAgentRequest(context.Background(), channelId, conversationId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.ResolveAgentRequest``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ResolveAgentRequest`: Conversation
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.ResolveAgentRequest`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**conversationId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiResolveAgentRequestRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Conversation**](Conversation.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## TranslateConversationParams

> map[string]string TranslateConversationParams(ctx, channelId, cid).ConversationParamsTranslateRequest(conversationParamsTranslateRequest).Execute()

Translate conversation params



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    cid := "cid_example" // string | 
    conversationParamsTranslateRequest := *openapiclient.NewConversationParamsTranslateRequest([]string{"Params_example"}) // ConversationParamsTranslateRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.ConversationsAPI.TranslateConversationParams(context.Background(), channelId, cid).ConversationParamsTranslateRequest(conversationParamsTranslateRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ConversationsAPI.TranslateConversationParams``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `TranslateConversationParams`: map[string]string
    fmt.Fprintf(os.Stdout, "Response from `ConversationsAPI.TranslateConversationParams`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 
**cid** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiTranslateConversationParamsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **conversationParamsTranslateRequest** | [**ConversationParamsTranslateRequest**](ConversationParamsTranslateRequest.md) |  | 

### Return type

**map[string]string**

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

