# SendTemplateRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**To** | **string** |  | 
**ReceiverType** | Pointer to [**ReceiverType**](ReceiverType.md) |  | [optional] 
**GatewayId** | **string** |  | 
**TemplateId** | **string** |  | 
**TemplateName** | **string** |  | 
**Source** | Pointer to **string** |  | [optional] 
**Language** | Pointer to **string** | Applicable for sending by name only | [optional] 
**LanguageDefaultFallback** | Pointer to **bool** | Applicable for sending by name only, when &#x60;true&#x60; we&#39;ll look for template in channels default language | [optional] 
**CostOptimized** | Pointer to **bool** | It will automatically transform template to standard message where applicable | [optional] 
**InternalComponents** | Pointer to **map[string]interface{}** | key value map with template parameters, ex. {\&quot;name\&quot;: \&quot;Tester\&quot;, \&quot;city\&quot;: \&quot;Berlin\&quot; | [optional] 
**WhatsappComponents** | Pointer to [**[]WhatsAppTemplateMessageComponent**](WhatsAppTemplateMessageComponent.md) |  | [optional] 
**Labels** | Pointer to **[]string** |  | [optional] 
**Context** | Pointer to [**SendMessageContext**](SendMessageContext.md) |  | [optional] 
**SenderUserId** | Pointer to **string** | For internal use only, in standard api call this parameter will be ignored. | [optional] 

## Methods

### NewSendTemplateRequest

`func NewSendTemplateRequest(to string, gatewayId string, templateId string, templateName string, ) *SendTemplateRequest`

NewSendTemplateRequest instantiates a new SendTemplateRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSendTemplateRequestWithDefaults

`func NewSendTemplateRequestWithDefaults() *SendTemplateRequest`

NewSendTemplateRequestWithDefaults instantiates a new SendTemplateRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTo

`func (o *SendTemplateRequest) GetTo() string`

GetTo returns the To field if non-nil, zero value otherwise.

### GetToOk

`func (o *SendTemplateRequest) GetToOk() (*string, bool)`

GetToOk returns a tuple with the To field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTo

`func (o *SendTemplateRequest) SetTo(v string)`

SetTo sets To field to given value.


### GetReceiverType

`func (o *SendTemplateRequest) GetReceiverType() ReceiverType`

GetReceiverType returns the ReceiverType field if non-nil, zero value otherwise.

### GetReceiverTypeOk

`func (o *SendTemplateRequest) GetReceiverTypeOk() (*ReceiverType, bool)`

GetReceiverTypeOk returns a tuple with the ReceiverType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReceiverType

`func (o *SendTemplateRequest) SetReceiverType(v ReceiverType)`

SetReceiverType sets ReceiverType field to given value.

### HasReceiverType

`func (o *SendTemplateRequest) HasReceiverType() bool`

HasReceiverType returns a boolean if a field has been set.

### GetGatewayId

`func (o *SendTemplateRequest) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *SendTemplateRequest) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *SendTemplateRequest) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.


### GetTemplateId

`func (o *SendTemplateRequest) GetTemplateId() string`

GetTemplateId returns the TemplateId field if non-nil, zero value otherwise.

### GetTemplateIdOk

`func (o *SendTemplateRequest) GetTemplateIdOk() (*string, bool)`

GetTemplateIdOk returns a tuple with the TemplateId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplateId

`func (o *SendTemplateRequest) SetTemplateId(v string)`

SetTemplateId sets TemplateId field to given value.


### GetTemplateName

`func (o *SendTemplateRequest) GetTemplateName() string`

GetTemplateName returns the TemplateName field if non-nil, zero value otherwise.

### GetTemplateNameOk

`func (o *SendTemplateRequest) GetTemplateNameOk() (*string, bool)`

GetTemplateNameOk returns a tuple with the TemplateName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplateName

`func (o *SendTemplateRequest) SetTemplateName(v string)`

SetTemplateName sets TemplateName field to given value.


### GetSource

`func (o *SendTemplateRequest) GetSource() string`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *SendTemplateRequest) GetSourceOk() (*string, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *SendTemplateRequest) SetSource(v string)`

SetSource sets Source field to given value.

### HasSource

`func (o *SendTemplateRequest) HasSource() bool`

HasSource returns a boolean if a field has been set.

### GetLanguage

`func (o *SendTemplateRequest) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *SendTemplateRequest) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *SendTemplateRequest) SetLanguage(v string)`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *SendTemplateRequest) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.

### GetLanguageDefaultFallback

`func (o *SendTemplateRequest) GetLanguageDefaultFallback() bool`

GetLanguageDefaultFallback returns the LanguageDefaultFallback field if non-nil, zero value otherwise.

### GetLanguageDefaultFallbackOk

`func (o *SendTemplateRequest) GetLanguageDefaultFallbackOk() (*bool, bool)`

GetLanguageDefaultFallbackOk returns a tuple with the LanguageDefaultFallback field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguageDefaultFallback

`func (o *SendTemplateRequest) SetLanguageDefaultFallback(v bool)`

SetLanguageDefaultFallback sets LanguageDefaultFallback field to given value.

### HasLanguageDefaultFallback

`func (o *SendTemplateRequest) HasLanguageDefaultFallback() bool`

HasLanguageDefaultFallback returns a boolean if a field has been set.

### GetCostOptimized

`func (o *SendTemplateRequest) GetCostOptimized() bool`

GetCostOptimized returns the CostOptimized field if non-nil, zero value otherwise.

### GetCostOptimizedOk

`func (o *SendTemplateRequest) GetCostOptimizedOk() (*bool, bool)`

GetCostOptimizedOk returns a tuple with the CostOptimized field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCostOptimized

`func (o *SendTemplateRequest) SetCostOptimized(v bool)`

SetCostOptimized sets CostOptimized field to given value.

### HasCostOptimized

`func (o *SendTemplateRequest) HasCostOptimized() bool`

HasCostOptimized returns a boolean if a field has been set.

### GetInternalComponents

`func (o *SendTemplateRequest) GetInternalComponents() map[string]interface{}`

GetInternalComponents returns the InternalComponents field if non-nil, zero value otherwise.

### GetInternalComponentsOk

`func (o *SendTemplateRequest) GetInternalComponentsOk() (*map[string]interface{}, bool)`

GetInternalComponentsOk returns a tuple with the InternalComponents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInternalComponents

`func (o *SendTemplateRequest) SetInternalComponents(v map[string]interface{})`

SetInternalComponents sets InternalComponents field to given value.

### HasInternalComponents

`func (o *SendTemplateRequest) HasInternalComponents() bool`

HasInternalComponents returns a boolean if a field has been set.

### GetWhatsappComponents

`func (o *SendTemplateRequest) GetWhatsappComponents() []WhatsAppTemplateMessageComponent`

GetWhatsappComponents returns the WhatsappComponents field if non-nil, zero value otherwise.

### GetWhatsappComponentsOk

`func (o *SendTemplateRequest) GetWhatsappComponentsOk() (*[]WhatsAppTemplateMessageComponent, bool)`

GetWhatsappComponentsOk returns a tuple with the WhatsappComponents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWhatsappComponents

`func (o *SendTemplateRequest) SetWhatsappComponents(v []WhatsAppTemplateMessageComponent)`

SetWhatsappComponents sets WhatsappComponents field to given value.

### HasWhatsappComponents

`func (o *SendTemplateRequest) HasWhatsappComponents() bool`

HasWhatsappComponents returns a boolean if a field has been set.

### GetLabels

`func (o *SendTemplateRequest) GetLabels() []string`

GetLabels returns the Labels field if non-nil, zero value otherwise.

### GetLabelsOk

`func (o *SendTemplateRequest) GetLabelsOk() (*[]string, bool)`

GetLabelsOk returns a tuple with the Labels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabels

`func (o *SendTemplateRequest) SetLabels(v []string)`

SetLabels sets Labels field to given value.

### HasLabels

`func (o *SendTemplateRequest) HasLabels() bool`

HasLabels returns a boolean if a field has been set.

### GetContext

`func (o *SendTemplateRequest) GetContext() SendMessageContext`

GetContext returns the Context field if non-nil, zero value otherwise.

### GetContextOk

`func (o *SendTemplateRequest) GetContextOk() (*SendMessageContext, bool)`

GetContextOk returns a tuple with the Context field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContext

`func (o *SendTemplateRequest) SetContext(v SendMessageContext)`

SetContext sets Context field to given value.

### HasContext

`func (o *SendTemplateRequest) HasContext() bool`

HasContext returns a boolean if a field has been set.

### GetSenderUserId

`func (o *SendTemplateRequest) GetSenderUserId() string`

GetSenderUserId returns the SenderUserId field if non-nil, zero value otherwise.

### GetSenderUserIdOk

`func (o *SendTemplateRequest) GetSenderUserIdOk() (*string, bool)`

GetSenderUserIdOk returns a tuple with the SenderUserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderUserId

`func (o *SendTemplateRequest) SetSenderUserId(v string)`

SetSenderUserId sets SenderUserId field to given value.

### HasSenderUserId

`func (o *SendTemplateRequest) HasSenderUserId() bool`

HasSenderUserId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


