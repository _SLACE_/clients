# InboxSettingsMessengerLimitsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MaxTextLength** | **int32** |  | 
**SessionTimeoutSeconds** | **string** |  | 
**Type** | [**Messenger**](Messenger.md) |  | 
**MessageTypes** | Pointer to [**[]InboxSettingsMessengerLimitsInnerMessageTypesInner**](InboxSettingsMessengerLimitsInnerMessageTypesInner.md) |  | [optional] 

## Methods

### NewInboxSettingsMessengerLimitsInner

`func NewInboxSettingsMessengerLimitsInner(maxTextLength int32, sessionTimeoutSeconds string, type_ Messenger, ) *InboxSettingsMessengerLimitsInner`

NewInboxSettingsMessengerLimitsInner instantiates a new InboxSettingsMessengerLimitsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInboxSettingsMessengerLimitsInnerWithDefaults

`func NewInboxSettingsMessengerLimitsInnerWithDefaults() *InboxSettingsMessengerLimitsInner`

NewInboxSettingsMessengerLimitsInnerWithDefaults instantiates a new InboxSettingsMessengerLimitsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMaxTextLength

`func (o *InboxSettingsMessengerLimitsInner) GetMaxTextLength() int32`

GetMaxTextLength returns the MaxTextLength field if non-nil, zero value otherwise.

### GetMaxTextLengthOk

`func (o *InboxSettingsMessengerLimitsInner) GetMaxTextLengthOk() (*int32, bool)`

GetMaxTextLengthOk returns a tuple with the MaxTextLength field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxTextLength

`func (o *InboxSettingsMessengerLimitsInner) SetMaxTextLength(v int32)`

SetMaxTextLength sets MaxTextLength field to given value.


### GetSessionTimeoutSeconds

`func (o *InboxSettingsMessengerLimitsInner) GetSessionTimeoutSeconds() string`

GetSessionTimeoutSeconds returns the SessionTimeoutSeconds field if non-nil, zero value otherwise.

### GetSessionTimeoutSecondsOk

`func (o *InboxSettingsMessengerLimitsInner) GetSessionTimeoutSecondsOk() (*string, bool)`

GetSessionTimeoutSecondsOk returns a tuple with the SessionTimeoutSeconds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSessionTimeoutSeconds

`func (o *InboxSettingsMessengerLimitsInner) SetSessionTimeoutSeconds(v string)`

SetSessionTimeoutSeconds sets SessionTimeoutSeconds field to given value.


### GetType

`func (o *InboxSettingsMessengerLimitsInner) GetType() Messenger`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *InboxSettingsMessengerLimitsInner) GetTypeOk() (*Messenger, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *InboxSettingsMessengerLimitsInner) SetType(v Messenger)`

SetType sets Type field to given value.


### GetMessageTypes

`func (o *InboxSettingsMessengerLimitsInner) GetMessageTypes() []InboxSettingsMessengerLimitsInnerMessageTypesInner`

GetMessageTypes returns the MessageTypes field if non-nil, zero value otherwise.

### GetMessageTypesOk

`func (o *InboxSettingsMessengerLimitsInner) GetMessageTypesOk() (*[]InboxSettingsMessengerLimitsInnerMessageTypesInner, bool)`

GetMessageTypesOk returns a tuple with the MessageTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageTypes

`func (o *InboxSettingsMessengerLimitsInner) SetMessageTypes(v []InboxSettingsMessengerLimitsInnerMessageTypesInner)`

SetMessageTypes sets MessageTypes field to given value.

### HasMessageTypes

`func (o *InboxSettingsMessengerLimitsInner) HasMessageTypes() bool`

HasMessageTypes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


