# ContactExcludeRelation

## Enum


* `ContactExcludeRelationAll` (value: `"all"`)

* `ContactExcludeRelationSegments` (value: `"Segments"`)

* `ContactExcludeRelationComChannels` (value: `"ComChannels"`)

* `ContactExcludeRelationPlatformComChannels` (value: `"PlatformComChannels"`)

* `ContactExcludeRelationCoupons` (value: `"Coupons"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


