# SendMessageRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**To** | **string** |  | 
**ReceiverType** | Pointer to [**ReceiverType**](ReceiverType.md) |  | [optional] 
**Type** | [**SendMessageType**](SendMessageType.md) |  | 
**GatewayId** | **string** |  | 
**Text** | Pointer to **string** |  | [optional] 
**MediaId** | Pointer to **string** |  | [optional] 
**MediaUrl** | Pointer to **string** |  | [optional] 
**MediaFilename** | Pointer to **string** |  | [optional] 
**Source** | Pointer to **string** |  | [optional] 
**Labels** | Pointer to **[]string** |  | [optional] 
**Location** | Pointer to [**LocationPersonalized**](LocationPersonalized.md) |  | [optional] 
**Buttons** | Pointer to [**[]MessageButton**](MessageButton.md) |  | [optional] 
**Header** | Pointer to [**MessageHeader**](MessageHeader.md) |  | [optional] 
**Footer** | Pointer to [**MessageFooter**](MessageFooter.md) |  | [optional] 
**List** | Pointer to [**List**](List.md) |  | [optional] 
**Contacts** | Pointer to [**[]MessageContact**](MessageContact.md) |  | [optional] 
**Context** | Pointer to [**SendMessageContext**](SendMessageContext.md) |  | [optional] 
**MessengerTag** | Pointer to [**MessengerTag**](MessengerTag.md) |  | [optional] 
**OrderDetails** | Pointer to [**MessageOrderDetails**](MessageOrderDetails.md) |  | [optional] 
**OrderStatus** | Pointer to [**MessageOrderStatus**](MessageOrderStatus.md) |  | [optional] 
**ButtonLabel** | Pointer to **string** |  | [optional] 
**SenderUserId** | Pointer to **string** | For internal use only, in standard api call this parameter will be ignored. | [optional] 
**Reaction** | Pointer to [**MessageReaction**](MessageReaction.md) |  | [optional] 
**Cards** | Pointer to [**[]SendMessageCard**](SendMessageCard.md) |  | [optional] 

## Methods

### NewSendMessageRequest

`func NewSendMessageRequest(to string, type_ SendMessageType, gatewayId string, ) *SendMessageRequest`

NewSendMessageRequest instantiates a new SendMessageRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSendMessageRequestWithDefaults

`func NewSendMessageRequestWithDefaults() *SendMessageRequest`

NewSendMessageRequestWithDefaults instantiates a new SendMessageRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTo

`func (o *SendMessageRequest) GetTo() string`

GetTo returns the To field if non-nil, zero value otherwise.

### GetToOk

`func (o *SendMessageRequest) GetToOk() (*string, bool)`

GetToOk returns a tuple with the To field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTo

`func (o *SendMessageRequest) SetTo(v string)`

SetTo sets To field to given value.


### GetReceiverType

`func (o *SendMessageRequest) GetReceiverType() ReceiverType`

GetReceiverType returns the ReceiverType field if non-nil, zero value otherwise.

### GetReceiverTypeOk

`func (o *SendMessageRequest) GetReceiverTypeOk() (*ReceiverType, bool)`

GetReceiverTypeOk returns a tuple with the ReceiverType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReceiverType

`func (o *SendMessageRequest) SetReceiverType(v ReceiverType)`

SetReceiverType sets ReceiverType field to given value.

### HasReceiverType

`func (o *SendMessageRequest) HasReceiverType() bool`

HasReceiverType returns a boolean if a field has been set.

### GetType

`func (o *SendMessageRequest) GetType() SendMessageType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *SendMessageRequest) GetTypeOk() (*SendMessageType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *SendMessageRequest) SetType(v SendMessageType)`

SetType sets Type field to given value.


### GetGatewayId

`func (o *SendMessageRequest) GetGatewayId() string`

GetGatewayId returns the GatewayId field if non-nil, zero value otherwise.

### GetGatewayIdOk

`func (o *SendMessageRequest) GetGatewayIdOk() (*string, bool)`

GetGatewayIdOk returns a tuple with the GatewayId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGatewayId

`func (o *SendMessageRequest) SetGatewayId(v string)`

SetGatewayId sets GatewayId field to given value.


### GetText

`func (o *SendMessageRequest) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *SendMessageRequest) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *SendMessageRequest) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *SendMessageRequest) HasText() bool`

HasText returns a boolean if a field has been set.

### GetMediaId

`func (o *SendMessageRequest) GetMediaId() string`

GetMediaId returns the MediaId field if non-nil, zero value otherwise.

### GetMediaIdOk

`func (o *SendMessageRequest) GetMediaIdOk() (*string, bool)`

GetMediaIdOk returns a tuple with the MediaId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaId

`func (o *SendMessageRequest) SetMediaId(v string)`

SetMediaId sets MediaId field to given value.

### HasMediaId

`func (o *SendMessageRequest) HasMediaId() bool`

HasMediaId returns a boolean if a field has been set.

### GetMediaUrl

`func (o *SendMessageRequest) GetMediaUrl() string`

GetMediaUrl returns the MediaUrl field if non-nil, zero value otherwise.

### GetMediaUrlOk

`func (o *SendMessageRequest) GetMediaUrlOk() (*string, bool)`

GetMediaUrlOk returns a tuple with the MediaUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaUrl

`func (o *SendMessageRequest) SetMediaUrl(v string)`

SetMediaUrl sets MediaUrl field to given value.

### HasMediaUrl

`func (o *SendMessageRequest) HasMediaUrl() bool`

HasMediaUrl returns a boolean if a field has been set.

### GetMediaFilename

`func (o *SendMessageRequest) GetMediaFilename() string`

GetMediaFilename returns the MediaFilename field if non-nil, zero value otherwise.

### GetMediaFilenameOk

`func (o *SendMessageRequest) GetMediaFilenameOk() (*string, bool)`

GetMediaFilenameOk returns a tuple with the MediaFilename field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaFilename

`func (o *SendMessageRequest) SetMediaFilename(v string)`

SetMediaFilename sets MediaFilename field to given value.

### HasMediaFilename

`func (o *SendMessageRequest) HasMediaFilename() bool`

HasMediaFilename returns a boolean if a field has been set.

### GetSource

`func (o *SendMessageRequest) GetSource() string`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *SendMessageRequest) GetSourceOk() (*string, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *SendMessageRequest) SetSource(v string)`

SetSource sets Source field to given value.

### HasSource

`func (o *SendMessageRequest) HasSource() bool`

HasSource returns a boolean if a field has been set.

### GetLabels

`func (o *SendMessageRequest) GetLabels() []string`

GetLabels returns the Labels field if non-nil, zero value otherwise.

### GetLabelsOk

`func (o *SendMessageRequest) GetLabelsOk() (*[]string, bool)`

GetLabelsOk returns a tuple with the Labels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLabels

`func (o *SendMessageRequest) SetLabels(v []string)`

SetLabels sets Labels field to given value.

### HasLabels

`func (o *SendMessageRequest) HasLabels() bool`

HasLabels returns a boolean if a field has been set.

### GetLocation

`func (o *SendMessageRequest) GetLocation() LocationPersonalized`

GetLocation returns the Location field if non-nil, zero value otherwise.

### GetLocationOk

`func (o *SendMessageRequest) GetLocationOk() (*LocationPersonalized, bool)`

GetLocationOk returns a tuple with the Location field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocation

`func (o *SendMessageRequest) SetLocation(v LocationPersonalized)`

SetLocation sets Location field to given value.

### HasLocation

`func (o *SendMessageRequest) HasLocation() bool`

HasLocation returns a boolean if a field has been set.

### GetButtons

`func (o *SendMessageRequest) GetButtons() []MessageButton`

GetButtons returns the Buttons field if non-nil, zero value otherwise.

### GetButtonsOk

`func (o *SendMessageRequest) GetButtonsOk() (*[]MessageButton, bool)`

GetButtonsOk returns a tuple with the Buttons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtons

`func (o *SendMessageRequest) SetButtons(v []MessageButton)`

SetButtons sets Buttons field to given value.

### HasButtons

`func (o *SendMessageRequest) HasButtons() bool`

HasButtons returns a boolean if a field has been set.

### GetHeader

`func (o *SendMessageRequest) GetHeader() MessageHeader`

GetHeader returns the Header field if non-nil, zero value otherwise.

### GetHeaderOk

`func (o *SendMessageRequest) GetHeaderOk() (*MessageHeader, bool)`

GetHeaderOk returns a tuple with the Header field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeader

`func (o *SendMessageRequest) SetHeader(v MessageHeader)`

SetHeader sets Header field to given value.

### HasHeader

`func (o *SendMessageRequest) HasHeader() bool`

HasHeader returns a boolean if a field has been set.

### GetFooter

`func (o *SendMessageRequest) GetFooter() MessageFooter`

GetFooter returns the Footer field if non-nil, zero value otherwise.

### GetFooterOk

`func (o *SendMessageRequest) GetFooterOk() (*MessageFooter, bool)`

GetFooterOk returns a tuple with the Footer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFooter

`func (o *SendMessageRequest) SetFooter(v MessageFooter)`

SetFooter sets Footer field to given value.

### HasFooter

`func (o *SendMessageRequest) HasFooter() bool`

HasFooter returns a boolean if a field has been set.

### GetList

`func (o *SendMessageRequest) GetList() List`

GetList returns the List field if non-nil, zero value otherwise.

### GetListOk

`func (o *SendMessageRequest) GetListOk() (*List, bool)`

GetListOk returns a tuple with the List field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetList

`func (o *SendMessageRequest) SetList(v List)`

SetList sets List field to given value.

### HasList

`func (o *SendMessageRequest) HasList() bool`

HasList returns a boolean if a field has been set.

### GetContacts

`func (o *SendMessageRequest) GetContacts() []MessageContact`

GetContacts returns the Contacts field if non-nil, zero value otherwise.

### GetContactsOk

`func (o *SendMessageRequest) GetContactsOk() (*[]MessageContact, bool)`

GetContactsOk returns a tuple with the Contacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContacts

`func (o *SendMessageRequest) SetContacts(v []MessageContact)`

SetContacts sets Contacts field to given value.

### HasContacts

`func (o *SendMessageRequest) HasContacts() bool`

HasContacts returns a boolean if a field has been set.

### GetContext

`func (o *SendMessageRequest) GetContext() SendMessageContext`

GetContext returns the Context field if non-nil, zero value otherwise.

### GetContextOk

`func (o *SendMessageRequest) GetContextOk() (*SendMessageContext, bool)`

GetContextOk returns a tuple with the Context field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContext

`func (o *SendMessageRequest) SetContext(v SendMessageContext)`

SetContext sets Context field to given value.

### HasContext

`func (o *SendMessageRequest) HasContext() bool`

HasContext returns a boolean if a field has been set.

### GetMessengerTag

`func (o *SendMessageRequest) GetMessengerTag() MessengerTag`

GetMessengerTag returns the MessengerTag field if non-nil, zero value otherwise.

### GetMessengerTagOk

`func (o *SendMessageRequest) GetMessengerTagOk() (*MessengerTag, bool)`

GetMessengerTagOk returns a tuple with the MessengerTag field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerTag

`func (o *SendMessageRequest) SetMessengerTag(v MessengerTag)`

SetMessengerTag sets MessengerTag field to given value.

### HasMessengerTag

`func (o *SendMessageRequest) HasMessengerTag() bool`

HasMessengerTag returns a boolean if a field has been set.

### GetOrderDetails

`func (o *SendMessageRequest) GetOrderDetails() MessageOrderDetails`

GetOrderDetails returns the OrderDetails field if non-nil, zero value otherwise.

### GetOrderDetailsOk

`func (o *SendMessageRequest) GetOrderDetailsOk() (*MessageOrderDetails, bool)`

GetOrderDetailsOk returns a tuple with the OrderDetails field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderDetails

`func (o *SendMessageRequest) SetOrderDetails(v MessageOrderDetails)`

SetOrderDetails sets OrderDetails field to given value.

### HasOrderDetails

`func (o *SendMessageRequest) HasOrderDetails() bool`

HasOrderDetails returns a boolean if a field has been set.

### GetOrderStatus

`func (o *SendMessageRequest) GetOrderStatus() MessageOrderStatus`

GetOrderStatus returns the OrderStatus field if non-nil, zero value otherwise.

### GetOrderStatusOk

`func (o *SendMessageRequest) GetOrderStatusOk() (*MessageOrderStatus, bool)`

GetOrderStatusOk returns a tuple with the OrderStatus field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrderStatus

`func (o *SendMessageRequest) SetOrderStatus(v MessageOrderStatus)`

SetOrderStatus sets OrderStatus field to given value.

### HasOrderStatus

`func (o *SendMessageRequest) HasOrderStatus() bool`

HasOrderStatus returns a boolean if a field has been set.

### GetButtonLabel

`func (o *SendMessageRequest) GetButtonLabel() string`

GetButtonLabel returns the ButtonLabel field if non-nil, zero value otherwise.

### GetButtonLabelOk

`func (o *SendMessageRequest) GetButtonLabelOk() (*string, bool)`

GetButtonLabelOk returns a tuple with the ButtonLabel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtonLabel

`func (o *SendMessageRequest) SetButtonLabel(v string)`

SetButtonLabel sets ButtonLabel field to given value.

### HasButtonLabel

`func (o *SendMessageRequest) HasButtonLabel() bool`

HasButtonLabel returns a boolean if a field has been set.

### GetSenderUserId

`func (o *SendMessageRequest) GetSenderUserId() string`

GetSenderUserId returns the SenderUserId field if non-nil, zero value otherwise.

### GetSenderUserIdOk

`func (o *SendMessageRequest) GetSenderUserIdOk() (*string, bool)`

GetSenderUserIdOk returns a tuple with the SenderUserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderUserId

`func (o *SendMessageRequest) SetSenderUserId(v string)`

SetSenderUserId sets SenderUserId field to given value.

### HasSenderUserId

`func (o *SendMessageRequest) HasSenderUserId() bool`

HasSenderUserId returns a boolean if a field has been set.

### GetReaction

`func (o *SendMessageRequest) GetReaction() MessageReaction`

GetReaction returns the Reaction field if non-nil, zero value otherwise.

### GetReactionOk

`func (o *SendMessageRequest) GetReactionOk() (*MessageReaction, bool)`

GetReactionOk returns a tuple with the Reaction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReaction

`func (o *SendMessageRequest) SetReaction(v MessageReaction)`

SetReaction sets Reaction field to given value.

### HasReaction

`func (o *SendMessageRequest) HasReaction() bool`

HasReaction returns a boolean if a field has been set.

### GetCards

`func (o *SendMessageRequest) GetCards() []SendMessageCard`

GetCards returns the Cards field if non-nil, zero value otherwise.

### GetCardsOk

`func (o *SendMessageRequest) GetCardsOk() (*[]SendMessageCard, bool)`

GetCardsOk returns a tuple with the Cards field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCards

`func (o *SendMessageRequest) SetCards(v []SendMessageCard)`

SetCards sets Cards field to given value.

### HasCards

`func (o *SendMessageRequest) HasCards() bool`

HasCards returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


