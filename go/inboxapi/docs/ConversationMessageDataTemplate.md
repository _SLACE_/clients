# ConversationMessageDataTemplate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | [**TemplateType**](TemplateType.md) |  | 
**Internal** | Pointer to [**InternalTemplate**](InternalTemplate.md) |  | [optional] 
**InternalComponents** | Pointer to **map[string]string** | The map of any string values used for cutom external references.  | [optional] 
**Whatsapp** | Pointer to [**WhatsAppTemplate**](WhatsAppTemplate.md) |  | [optional] 
**WhatsappComponents** | Pointer to [**[]WhatsAppTemplateMessageComponent**](WhatsAppTemplateMessageComponent.md) |  | [optional] 

## Methods

### NewConversationMessageDataTemplate

`func NewConversationMessageDataTemplate(type_ TemplateType, ) *ConversationMessageDataTemplate`

NewConversationMessageDataTemplate instantiates a new ConversationMessageDataTemplate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConversationMessageDataTemplateWithDefaults

`func NewConversationMessageDataTemplateWithDefaults() *ConversationMessageDataTemplate`

NewConversationMessageDataTemplateWithDefaults instantiates a new ConversationMessageDataTemplate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *ConversationMessageDataTemplate) GetType() TemplateType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ConversationMessageDataTemplate) GetTypeOk() (*TemplateType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ConversationMessageDataTemplate) SetType(v TemplateType)`

SetType sets Type field to given value.


### GetInternal

`func (o *ConversationMessageDataTemplate) GetInternal() InternalTemplate`

GetInternal returns the Internal field if non-nil, zero value otherwise.

### GetInternalOk

`func (o *ConversationMessageDataTemplate) GetInternalOk() (*InternalTemplate, bool)`

GetInternalOk returns a tuple with the Internal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInternal

`func (o *ConversationMessageDataTemplate) SetInternal(v InternalTemplate)`

SetInternal sets Internal field to given value.

### HasInternal

`func (o *ConversationMessageDataTemplate) HasInternal() bool`

HasInternal returns a boolean if a field has been set.

### GetInternalComponents

`func (o *ConversationMessageDataTemplate) GetInternalComponents() map[string]string`

GetInternalComponents returns the InternalComponents field if non-nil, zero value otherwise.

### GetInternalComponentsOk

`func (o *ConversationMessageDataTemplate) GetInternalComponentsOk() (*map[string]string, bool)`

GetInternalComponentsOk returns a tuple with the InternalComponents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInternalComponents

`func (o *ConversationMessageDataTemplate) SetInternalComponents(v map[string]string)`

SetInternalComponents sets InternalComponents field to given value.

### HasInternalComponents

`func (o *ConversationMessageDataTemplate) HasInternalComponents() bool`

HasInternalComponents returns a boolean if a field has been set.

### GetWhatsapp

`func (o *ConversationMessageDataTemplate) GetWhatsapp() WhatsAppTemplate`

GetWhatsapp returns the Whatsapp field if non-nil, zero value otherwise.

### GetWhatsappOk

`func (o *ConversationMessageDataTemplate) GetWhatsappOk() (*WhatsAppTemplate, bool)`

GetWhatsappOk returns a tuple with the Whatsapp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWhatsapp

`func (o *ConversationMessageDataTemplate) SetWhatsapp(v WhatsAppTemplate)`

SetWhatsapp sets Whatsapp field to given value.

### HasWhatsapp

`func (o *ConversationMessageDataTemplate) HasWhatsapp() bool`

HasWhatsapp returns a boolean if a field has been set.

### GetWhatsappComponents

`func (o *ConversationMessageDataTemplate) GetWhatsappComponents() []WhatsAppTemplateMessageComponent`

GetWhatsappComponents returns the WhatsappComponents field if non-nil, zero value otherwise.

### GetWhatsappComponentsOk

`func (o *ConversationMessageDataTemplate) GetWhatsappComponentsOk() (*[]WhatsAppTemplateMessageComponent, bool)`

GetWhatsappComponentsOk returns a tuple with the WhatsappComponents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWhatsappComponents

`func (o *ConversationMessageDataTemplate) SetWhatsappComponents(v []WhatsAppTemplateMessageComponent)`

SetWhatsappComponents sets WhatsappComponents field to given value.

### HasWhatsappComponents

`func (o *ConversationMessageDataTemplate) HasWhatsappComponents() bool`

HasWhatsappComponents returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


