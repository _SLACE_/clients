# InfoMessageData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to **string** |  | [optional] 
**Payload** | Pointer to [**InfoMessagePayload**](InfoMessagePayload.md) |  | [optional] 

## Methods

### NewInfoMessageData

`func NewInfoMessageData() *InfoMessageData`

NewInfoMessageData instantiates a new InfoMessageData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInfoMessageDataWithDefaults

`func NewInfoMessageDataWithDefaults() *InfoMessageData`

NewInfoMessageDataWithDefaults instantiates a new InfoMessageData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *InfoMessageData) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *InfoMessageData) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *InfoMessageData) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *InfoMessageData) HasType() bool`

HasType returns a boolean if a field has been set.

### GetPayload

`func (o *InfoMessageData) GetPayload() InfoMessagePayload`

GetPayload returns the Payload field if non-nil, zero value otherwise.

### GetPayloadOk

`func (o *InfoMessageData) GetPayloadOk() (*InfoMessagePayload, bool)`

GetPayloadOk returns a tuple with the Payload field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayload

`func (o *InfoMessageData) SetPayload(v InfoMessagePayload)`

SetPayload sets Payload field to given value.

### HasPayload

`func (o *InfoMessageData) HasPayload() bool`

HasPayload returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


