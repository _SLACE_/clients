# AddSegmentGroupRequestSegment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Key** | **string** |  | 
**ExpiresAt** | Pointer to **time.Time** |  | [optional] 
**SubType** | **string** |  | 

## Methods

### NewAddSegmentGroupRequestSegment

`func NewAddSegmentGroupRequestSegment(key string, subType string, ) *AddSegmentGroupRequestSegment`

NewAddSegmentGroupRequestSegment instantiates a new AddSegmentGroupRequestSegment object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAddSegmentGroupRequestSegmentWithDefaults

`func NewAddSegmentGroupRequestSegmentWithDefaults() *AddSegmentGroupRequestSegment`

NewAddSegmentGroupRequestSegmentWithDefaults instantiates a new AddSegmentGroupRequestSegment object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetKey

`func (o *AddSegmentGroupRequestSegment) GetKey() string`

GetKey returns the Key field if non-nil, zero value otherwise.

### GetKeyOk

`func (o *AddSegmentGroupRequestSegment) GetKeyOk() (*string, bool)`

GetKeyOk returns a tuple with the Key field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetKey

`func (o *AddSegmentGroupRequestSegment) SetKey(v string)`

SetKey sets Key field to given value.


### GetExpiresAt

`func (o *AddSegmentGroupRequestSegment) GetExpiresAt() time.Time`

GetExpiresAt returns the ExpiresAt field if non-nil, zero value otherwise.

### GetExpiresAtOk

`func (o *AddSegmentGroupRequestSegment) GetExpiresAtOk() (*time.Time, bool)`

GetExpiresAtOk returns a tuple with the ExpiresAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiresAt

`func (o *AddSegmentGroupRequestSegment) SetExpiresAt(v time.Time)`

SetExpiresAt sets ExpiresAt field to given value.

### HasExpiresAt

`func (o *AddSegmentGroupRequestSegment) HasExpiresAt() bool`

HasExpiresAt returns a boolean if a field has been set.

### GetSubType

`func (o *AddSegmentGroupRequestSegment) GetSubType() string`

GetSubType returns the SubType field if non-nil, zero value otherwise.

### GetSubTypeOk

`func (o *AddSegmentGroupRequestSegment) GetSubTypeOk() (*string, bool)`

GetSubTypeOk returns a tuple with the SubType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSubType

`func (o *AddSegmentGroupRequestSegment) SetSubType(v string)`

SetSubType sets SubType field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


