# ContactLanguages

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Primary** | Pointer to **string** | Primary language | [optional] 
**Additional** | Pointer to **map[string]string** | JSON with additional languages in format: { [source]: [languge] } | [optional] 

## Methods

### NewContactLanguages

`func NewContactLanguages() *ContactLanguages`

NewContactLanguages instantiates a new ContactLanguages object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactLanguagesWithDefaults

`func NewContactLanguagesWithDefaults() *ContactLanguages`

NewContactLanguagesWithDefaults instantiates a new ContactLanguages object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPrimary

`func (o *ContactLanguages) GetPrimary() string`

GetPrimary returns the Primary field if non-nil, zero value otherwise.

### GetPrimaryOk

`func (o *ContactLanguages) GetPrimaryOk() (*string, bool)`

GetPrimaryOk returns a tuple with the Primary field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrimary

`func (o *ContactLanguages) SetPrimary(v string)`

SetPrimary sets Primary field to given value.

### HasPrimary

`func (o *ContactLanguages) HasPrimary() bool`

HasPrimary returns a boolean if a field has been set.

### GetAdditional

`func (o *ContactLanguages) GetAdditional() map[string]string`

GetAdditional returns the Additional field if non-nil, zero value otherwise.

### GetAdditionalOk

`func (o *ContactLanguages) GetAdditionalOk() (*map[string]string, bool)`

GetAdditionalOk returns a tuple with the Additional field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditional

`func (o *ContactLanguages) SetAdditional(v map[string]string)`

SetAdditional sets Additional field to given value.

### HasAdditional

`func (o *ContactLanguages) HasAdditional() bool`

HasAdditional returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


