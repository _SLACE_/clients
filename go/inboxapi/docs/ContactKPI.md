# ContactKPI

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContactId** | Pointer to **string** |  | [optional] 
**ConversationsCount** | Pointer to **float32** |  | [optional] 
**InteractionsCount** | Pointer to **float32** |  | [optional] 
**InvestmentsCount** | Pointer to **float32** |  | [optional] 
**VisitsCount** | Pointer to **float32** |  | [optional] 
**PurchasesCount** | Pointer to **float32** |  | [optional] 
**ReferralsCount** | Pointer to **float32** |  | [optional] 

## Methods

### NewContactKPI

`func NewContactKPI() *ContactKPI`

NewContactKPI instantiates a new ContactKPI object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactKPIWithDefaults

`func NewContactKPIWithDefaults() *ContactKPI`

NewContactKPIWithDefaults instantiates a new ContactKPI object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContactId

`func (o *ContactKPI) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *ContactKPI) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *ContactKPI) SetContactId(v string)`

SetContactId sets ContactId field to given value.

### HasContactId

`func (o *ContactKPI) HasContactId() bool`

HasContactId returns a boolean if a field has been set.

### GetConversationsCount

`func (o *ContactKPI) GetConversationsCount() float32`

GetConversationsCount returns the ConversationsCount field if non-nil, zero value otherwise.

### GetConversationsCountOk

`func (o *ContactKPI) GetConversationsCountOk() (*float32, bool)`

GetConversationsCountOk returns a tuple with the ConversationsCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationsCount

`func (o *ContactKPI) SetConversationsCount(v float32)`

SetConversationsCount sets ConversationsCount field to given value.

### HasConversationsCount

`func (o *ContactKPI) HasConversationsCount() bool`

HasConversationsCount returns a boolean if a field has been set.

### GetInteractionsCount

`func (o *ContactKPI) GetInteractionsCount() float32`

GetInteractionsCount returns the InteractionsCount field if non-nil, zero value otherwise.

### GetInteractionsCountOk

`func (o *ContactKPI) GetInteractionsCountOk() (*float32, bool)`

GetInteractionsCountOk returns a tuple with the InteractionsCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionsCount

`func (o *ContactKPI) SetInteractionsCount(v float32)`

SetInteractionsCount sets InteractionsCount field to given value.

### HasInteractionsCount

`func (o *ContactKPI) HasInteractionsCount() bool`

HasInteractionsCount returns a boolean if a field has been set.

### GetInvestmentsCount

`func (o *ContactKPI) GetInvestmentsCount() float32`

GetInvestmentsCount returns the InvestmentsCount field if non-nil, zero value otherwise.

### GetInvestmentsCountOk

`func (o *ContactKPI) GetInvestmentsCountOk() (*float32, bool)`

GetInvestmentsCountOk returns a tuple with the InvestmentsCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInvestmentsCount

`func (o *ContactKPI) SetInvestmentsCount(v float32)`

SetInvestmentsCount sets InvestmentsCount field to given value.

### HasInvestmentsCount

`func (o *ContactKPI) HasInvestmentsCount() bool`

HasInvestmentsCount returns a boolean if a field has been set.

### GetVisitsCount

`func (o *ContactKPI) GetVisitsCount() float32`

GetVisitsCount returns the VisitsCount field if non-nil, zero value otherwise.

### GetVisitsCountOk

`func (o *ContactKPI) GetVisitsCountOk() (*float32, bool)`

GetVisitsCountOk returns a tuple with the VisitsCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVisitsCount

`func (o *ContactKPI) SetVisitsCount(v float32)`

SetVisitsCount sets VisitsCount field to given value.

### HasVisitsCount

`func (o *ContactKPI) HasVisitsCount() bool`

HasVisitsCount returns a boolean if a field has been set.

### GetPurchasesCount

`func (o *ContactKPI) GetPurchasesCount() float32`

GetPurchasesCount returns the PurchasesCount field if non-nil, zero value otherwise.

### GetPurchasesCountOk

`func (o *ContactKPI) GetPurchasesCountOk() (*float32, bool)`

GetPurchasesCountOk returns a tuple with the PurchasesCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPurchasesCount

`func (o *ContactKPI) SetPurchasesCount(v float32)`

SetPurchasesCount sets PurchasesCount field to given value.

### HasPurchasesCount

`func (o *ContactKPI) HasPurchasesCount() bool`

HasPurchasesCount returns a boolean if a field has been set.

### GetReferralsCount

`func (o *ContactKPI) GetReferralsCount() float32`

GetReferralsCount returns the ReferralsCount field if non-nil, zero value otherwise.

### GetReferralsCountOk

`func (o *ContactKPI) GetReferralsCountOk() (*float32, bool)`

GetReferralsCountOk returns a tuple with the ReferralsCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferralsCount

`func (o *ContactKPI) SetReferralsCount(v float32)`

SetReferralsCount sets ReferralsCount field to given value.

### HasReferralsCount

`func (o *ContactKPI) HasReferralsCount() bool`

HasReferralsCount returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


