# MessageContactPhoto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | Pointer to **string** |  | [optional] 
**MimeType** | Pointer to **string** |  | [optional] 
**Url** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageContactPhoto

`func NewMessageContactPhoto() *MessageContactPhoto`

NewMessageContactPhoto instantiates a new MessageContactPhoto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageContactPhotoWithDefaults

`func NewMessageContactPhotoWithDefaults() *MessageContactPhoto`

NewMessageContactPhotoWithDefaults instantiates a new MessageContactPhoto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetData

`func (o *MessageContactPhoto) GetData() string`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *MessageContactPhoto) GetDataOk() (*string, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *MessageContactPhoto) SetData(v string)`

SetData sets Data field to given value.

### HasData

`func (o *MessageContactPhoto) HasData() bool`

HasData returns a boolean if a field has been set.

### GetMimeType

`func (o *MessageContactPhoto) GetMimeType() string`

GetMimeType returns the MimeType field if non-nil, zero value otherwise.

### GetMimeTypeOk

`func (o *MessageContactPhoto) GetMimeTypeOk() (*string, bool)`

GetMimeTypeOk returns a tuple with the MimeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMimeType

`func (o *MessageContactPhoto) SetMimeType(v string)`

SetMimeType sets MimeType field to given value.

### HasMimeType

`func (o *MessageContactPhoto) HasMimeType() bool`

HasMimeType returns a boolean if a field has been set.

### GetUrl

`func (o *MessageContactPhoto) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *MessageContactPhoto) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *MessageContactPhoto) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *MessageContactPhoto) HasUrl() bool`

HasUrl returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


