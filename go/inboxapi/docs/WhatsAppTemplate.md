# WhatsAppTemplate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Available for gateways using CloudAPI only | [optional] 
**Status** | **string** |  | 
**RejectedReason** | Pointer to **string** |  | [optional] 
**Name** | **string** |  | 
**Language** | **string** |  | 
**Category** | [**WhatsAppTemplateCategory**](WhatsAppTemplateCategory.md) |  | 
**Components** | [**[]WhatsAppTemplateComponent**](WhatsAppTemplateComponent.md) |  | 
**AllowCategoryChange** | **bool** |  | 

## Methods

### NewWhatsAppTemplate

`func NewWhatsAppTemplate(status string, name string, language string, category WhatsAppTemplateCategory, components []WhatsAppTemplateComponent, allowCategoryChange bool, ) *WhatsAppTemplate`

NewWhatsAppTemplate instantiates a new WhatsAppTemplate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateWithDefaults

`func NewWhatsAppTemplateWithDefaults() *WhatsAppTemplate`

NewWhatsAppTemplateWithDefaults instantiates a new WhatsAppTemplate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WhatsAppTemplate) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WhatsAppTemplate) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WhatsAppTemplate) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *WhatsAppTemplate) HasId() bool`

HasId returns a boolean if a field has been set.

### GetStatus

`func (o *WhatsAppTemplate) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *WhatsAppTemplate) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *WhatsAppTemplate) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetRejectedReason

`func (o *WhatsAppTemplate) GetRejectedReason() string`

GetRejectedReason returns the RejectedReason field if non-nil, zero value otherwise.

### GetRejectedReasonOk

`func (o *WhatsAppTemplate) GetRejectedReasonOk() (*string, bool)`

GetRejectedReasonOk returns a tuple with the RejectedReason field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRejectedReason

`func (o *WhatsAppTemplate) SetRejectedReason(v string)`

SetRejectedReason sets RejectedReason field to given value.

### HasRejectedReason

`func (o *WhatsAppTemplate) HasRejectedReason() bool`

HasRejectedReason returns a boolean if a field has been set.

### GetName

`func (o *WhatsAppTemplate) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *WhatsAppTemplate) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *WhatsAppTemplate) SetName(v string)`

SetName sets Name field to given value.


### GetLanguage

`func (o *WhatsAppTemplate) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *WhatsAppTemplate) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *WhatsAppTemplate) SetLanguage(v string)`

SetLanguage sets Language field to given value.


### GetCategory

`func (o *WhatsAppTemplate) GetCategory() WhatsAppTemplateCategory`

GetCategory returns the Category field if non-nil, zero value otherwise.

### GetCategoryOk

`func (o *WhatsAppTemplate) GetCategoryOk() (*WhatsAppTemplateCategory, bool)`

GetCategoryOk returns a tuple with the Category field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCategory

`func (o *WhatsAppTemplate) SetCategory(v WhatsAppTemplateCategory)`

SetCategory sets Category field to given value.


### GetComponents

`func (o *WhatsAppTemplate) GetComponents() []WhatsAppTemplateComponent`

GetComponents returns the Components field if non-nil, zero value otherwise.

### GetComponentsOk

`func (o *WhatsAppTemplate) GetComponentsOk() (*[]WhatsAppTemplateComponent, bool)`

GetComponentsOk returns a tuple with the Components field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComponents

`func (o *WhatsAppTemplate) SetComponents(v []WhatsAppTemplateComponent)`

SetComponents sets Components field to given value.


### GetAllowCategoryChange

`func (o *WhatsAppTemplate) GetAllowCategoryChange() bool`

GetAllowCategoryChange returns the AllowCategoryChange field if non-nil, zero value otherwise.

### GetAllowCategoryChangeOk

`func (o *WhatsAppTemplate) GetAllowCategoryChangeOk() (*bool, bool)`

GetAllowCategoryChangeOk returns a tuple with the AllowCategoryChange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAllowCategoryChange

`func (o *WhatsAppTemplate) SetAllowCategoryChange(v bool)`

SetAllowCategoryChange sets AllowCategoryChange field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


