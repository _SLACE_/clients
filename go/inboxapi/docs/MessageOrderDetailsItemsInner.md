# MessageOrderDetailsItemsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**Amount** | **string** | Price as string with dot separator ie \&quot;12.61\&quot; | 
**Quantity** | **int32** |  | [default to 1]
**Discount** | Pointer to **string** |  | [optional] 
**Tax** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageOrderDetailsItemsInner

`func NewMessageOrderDetailsItemsInner(name string, amount string, quantity int32, ) *MessageOrderDetailsItemsInner`

NewMessageOrderDetailsItemsInner instantiates a new MessageOrderDetailsItemsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageOrderDetailsItemsInnerWithDefaults

`func NewMessageOrderDetailsItemsInnerWithDefaults() *MessageOrderDetailsItemsInner`

NewMessageOrderDetailsItemsInnerWithDefaults instantiates a new MessageOrderDetailsItemsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *MessageOrderDetailsItemsInner) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *MessageOrderDetailsItemsInner) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *MessageOrderDetailsItemsInner) SetName(v string)`

SetName sets Name field to given value.


### GetAmount

`func (o *MessageOrderDetailsItemsInner) GetAmount() string`

GetAmount returns the Amount field if non-nil, zero value otherwise.

### GetAmountOk

`func (o *MessageOrderDetailsItemsInner) GetAmountOk() (*string, bool)`

GetAmountOk returns a tuple with the Amount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAmount

`func (o *MessageOrderDetailsItemsInner) SetAmount(v string)`

SetAmount sets Amount field to given value.


### GetQuantity

`func (o *MessageOrderDetailsItemsInner) GetQuantity() int32`

GetQuantity returns the Quantity field if non-nil, zero value otherwise.

### GetQuantityOk

`func (o *MessageOrderDetailsItemsInner) GetQuantityOk() (*int32, bool)`

GetQuantityOk returns a tuple with the Quantity field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQuantity

`func (o *MessageOrderDetailsItemsInner) SetQuantity(v int32)`

SetQuantity sets Quantity field to given value.


### GetDiscount

`func (o *MessageOrderDetailsItemsInner) GetDiscount() string`

GetDiscount returns the Discount field if non-nil, zero value otherwise.

### GetDiscountOk

`func (o *MessageOrderDetailsItemsInner) GetDiscountOk() (*string, bool)`

GetDiscountOk returns a tuple with the Discount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDiscount

`func (o *MessageOrderDetailsItemsInner) SetDiscount(v string)`

SetDiscount sets Discount field to given value.

### HasDiscount

`func (o *MessageOrderDetailsItemsInner) HasDiscount() bool`

HasDiscount returns a boolean if a field has been set.

### GetTax

`func (o *MessageOrderDetailsItemsInner) GetTax() string`

GetTax returns the Tax field if non-nil, zero value otherwise.

### GetTaxOk

`func (o *MessageOrderDetailsItemsInner) GetTaxOk() (*string, bool)`

GetTaxOk returns a tuple with the Tax field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTax

`func (o *MessageOrderDetailsItemsInner) SetTax(v string)`

SetTax sets Tax field to given value.

### HasTax

`func (o *MessageOrderDetailsItemsInner) HasTax() bool`

HasTax returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


