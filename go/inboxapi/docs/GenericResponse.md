# GenericResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **int32** |  | 
**Message** | Pointer to **string** |  | [optional] 
**Errors** | Pointer to [**[]GenericResponseErrors**](GenericResponseErrors.md) |  | [optional] 

## Methods

### NewGenericResponse

`func NewGenericResponse(code int32, ) *GenericResponse`

NewGenericResponse instantiates a new GenericResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGenericResponseWithDefaults

`func NewGenericResponseWithDefaults() *GenericResponse`

NewGenericResponseWithDefaults instantiates a new GenericResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCode

`func (o *GenericResponse) GetCode() int32`

GetCode returns the Code field if non-nil, zero value otherwise.

### GetCodeOk

`func (o *GenericResponse) GetCodeOk() (*int32, bool)`

GetCodeOk returns a tuple with the Code field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCode

`func (o *GenericResponse) SetCode(v int32)`

SetCode sets Code field to given value.


### GetMessage

`func (o *GenericResponse) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *GenericResponse) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *GenericResponse) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *GenericResponse) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetErrors

`func (o *GenericResponse) GetErrors() []GenericResponseErrors`

GetErrors returns the Errors field if non-nil, zero value otherwise.

### GetErrorsOk

`func (o *GenericResponse) GetErrorsOk() (*[]GenericResponseErrors, bool)`

GetErrorsOk returns a tuple with the Errors field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrors

`func (o *GenericResponse) SetErrors(v []GenericResponseErrors)`

SetErrors sets Errors field to given value.

### HasErrors

`func (o *GenericResponse) HasErrors() bool`

HasErrors returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


