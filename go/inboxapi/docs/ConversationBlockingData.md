# ConversationBlockingData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Blocked** | **bool** |  | 
**BlockedInfo** | Pointer to **string** |  | [optional] 

## Methods

### NewConversationBlockingData

`func NewConversationBlockingData(blocked bool, ) *ConversationBlockingData`

NewConversationBlockingData instantiates a new ConversationBlockingData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConversationBlockingDataWithDefaults

`func NewConversationBlockingDataWithDefaults() *ConversationBlockingData`

NewConversationBlockingDataWithDefaults instantiates a new ConversationBlockingData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetBlocked

`func (o *ConversationBlockingData) GetBlocked() bool`

GetBlocked returns the Blocked field if non-nil, zero value otherwise.

### GetBlockedOk

`func (o *ConversationBlockingData) GetBlockedOk() (*bool, bool)`

GetBlockedOk returns a tuple with the Blocked field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlocked

`func (o *ConversationBlockingData) SetBlocked(v bool)`

SetBlocked sets Blocked field to given value.


### GetBlockedInfo

`func (o *ConversationBlockingData) GetBlockedInfo() string`

GetBlockedInfo returns the BlockedInfo field if non-nil, zero value otherwise.

### GetBlockedInfoOk

`func (o *ConversationBlockingData) GetBlockedInfoOk() (*string, bool)`

GetBlockedInfoOk returns a tuple with the BlockedInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockedInfo

`func (o *ConversationBlockingData) SetBlockedInfo(v string)`

SetBlockedInfo sets BlockedInfo field to given value.

### HasBlockedInfo

`func (o *ConversationBlockingData) HasBlockedInfo() bool`

HasBlockedInfo returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


