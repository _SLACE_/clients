# Activity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Type** | **string** |  | 
**OrganizationId** | Pointer to **string** |  | [optional] 
**ContactId** | Pointer to **string** |  | [optional] 
**LocationId** | Pointer to **string** |  | [optional] 
**TransactionId** | Pointer to **string** |  | [optional] 
**ConversationId** | Pointer to **string** |  | [optional] 
**Attributes** | Pointer to **map[string][]string** |  | [optional] 
**CreatedAt** | Pointer to **time.Time** |  | [optional] 
**UpdatedAt** | Pointer to **time.Time** |  | [optional] 
**OrganizationCount** | **int32** |  | 
**LocationCount** | **int32** |  | 
**OccuredAt** | Pointer to **time.Time** |  | [optional] 
**InteractionId** | Pointer to **string** |  | [optional] 
**EntryPointId** | Pointer to **string** |  | [optional] 
**State** | Pointer to **string** |  | [optional] 
**File** | Pointer to [**ActivityFile**](ActivityFile.md) |  | [optional] 

## Methods

### NewActivity

`func NewActivity(type_ string, organizationCount int32, locationCount int32, ) *Activity`

NewActivity instantiates a new Activity object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewActivityWithDefaults

`func NewActivityWithDefaults() *Activity`

NewActivityWithDefaults instantiates a new Activity object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Activity) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Activity) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Activity) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *Activity) HasId() bool`

HasId returns a boolean if a field has been set.

### GetType

`func (o *Activity) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Activity) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Activity) SetType(v string)`

SetType sets Type field to given value.


### GetOrganizationId

`func (o *Activity) GetOrganizationId() string`

GetOrganizationId returns the OrganizationId field if non-nil, zero value otherwise.

### GetOrganizationIdOk

`func (o *Activity) GetOrganizationIdOk() (*string, bool)`

GetOrganizationIdOk returns a tuple with the OrganizationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationId

`func (o *Activity) SetOrganizationId(v string)`

SetOrganizationId sets OrganizationId field to given value.

### HasOrganizationId

`func (o *Activity) HasOrganizationId() bool`

HasOrganizationId returns a boolean if a field has been set.

### GetContactId

`func (o *Activity) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *Activity) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *Activity) SetContactId(v string)`

SetContactId sets ContactId field to given value.

### HasContactId

`func (o *Activity) HasContactId() bool`

HasContactId returns a boolean if a field has been set.

### GetLocationId

`func (o *Activity) GetLocationId() string`

GetLocationId returns the LocationId field if non-nil, zero value otherwise.

### GetLocationIdOk

`func (o *Activity) GetLocationIdOk() (*string, bool)`

GetLocationIdOk returns a tuple with the LocationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationId

`func (o *Activity) SetLocationId(v string)`

SetLocationId sets LocationId field to given value.

### HasLocationId

`func (o *Activity) HasLocationId() bool`

HasLocationId returns a boolean if a field has been set.

### GetTransactionId

`func (o *Activity) GetTransactionId() string`

GetTransactionId returns the TransactionId field if non-nil, zero value otherwise.

### GetTransactionIdOk

`func (o *Activity) GetTransactionIdOk() (*string, bool)`

GetTransactionIdOk returns a tuple with the TransactionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransactionId

`func (o *Activity) SetTransactionId(v string)`

SetTransactionId sets TransactionId field to given value.

### HasTransactionId

`func (o *Activity) HasTransactionId() bool`

HasTransactionId returns a boolean if a field has been set.

### GetConversationId

`func (o *Activity) GetConversationId() string`

GetConversationId returns the ConversationId field if non-nil, zero value otherwise.

### GetConversationIdOk

`func (o *Activity) GetConversationIdOk() (*string, bool)`

GetConversationIdOk returns a tuple with the ConversationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversationId

`func (o *Activity) SetConversationId(v string)`

SetConversationId sets ConversationId field to given value.

### HasConversationId

`func (o *Activity) HasConversationId() bool`

HasConversationId returns a boolean if a field has been set.

### GetAttributes

`func (o *Activity) GetAttributes() map[string][]string`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *Activity) GetAttributesOk() (*map[string][]string, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *Activity) SetAttributes(v map[string][]string)`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *Activity) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetCreatedAt

`func (o *Activity) GetCreatedAt() time.Time`

GetCreatedAt returns the CreatedAt field if non-nil, zero value otherwise.

### GetCreatedAtOk

`func (o *Activity) GetCreatedAtOk() (*time.Time, bool)`

GetCreatedAtOk returns a tuple with the CreatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreatedAt

`func (o *Activity) SetCreatedAt(v time.Time)`

SetCreatedAt sets CreatedAt field to given value.

### HasCreatedAt

`func (o *Activity) HasCreatedAt() bool`

HasCreatedAt returns a boolean if a field has been set.

### GetUpdatedAt

`func (o *Activity) GetUpdatedAt() time.Time`

GetUpdatedAt returns the UpdatedAt field if non-nil, zero value otherwise.

### GetUpdatedAtOk

`func (o *Activity) GetUpdatedAtOk() (*time.Time, bool)`

GetUpdatedAtOk returns a tuple with the UpdatedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdatedAt

`func (o *Activity) SetUpdatedAt(v time.Time)`

SetUpdatedAt sets UpdatedAt field to given value.

### HasUpdatedAt

`func (o *Activity) HasUpdatedAt() bool`

HasUpdatedAt returns a boolean if a field has been set.

### GetOrganizationCount

`func (o *Activity) GetOrganizationCount() int32`

GetOrganizationCount returns the OrganizationCount field if non-nil, zero value otherwise.

### GetOrganizationCountOk

`func (o *Activity) GetOrganizationCountOk() (*int32, bool)`

GetOrganizationCountOk returns a tuple with the OrganizationCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrganizationCount

`func (o *Activity) SetOrganizationCount(v int32)`

SetOrganizationCount sets OrganizationCount field to given value.


### GetLocationCount

`func (o *Activity) GetLocationCount() int32`

GetLocationCount returns the LocationCount field if non-nil, zero value otherwise.

### GetLocationCountOk

`func (o *Activity) GetLocationCountOk() (*int32, bool)`

GetLocationCountOk returns a tuple with the LocationCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationCount

`func (o *Activity) SetLocationCount(v int32)`

SetLocationCount sets LocationCount field to given value.


### GetOccuredAt

`func (o *Activity) GetOccuredAt() time.Time`

GetOccuredAt returns the OccuredAt field if non-nil, zero value otherwise.

### GetOccuredAtOk

`func (o *Activity) GetOccuredAtOk() (*time.Time, bool)`

GetOccuredAtOk returns a tuple with the OccuredAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOccuredAt

`func (o *Activity) SetOccuredAt(v time.Time)`

SetOccuredAt sets OccuredAt field to given value.

### HasOccuredAt

`func (o *Activity) HasOccuredAt() bool`

HasOccuredAt returns a boolean if a field has been set.

### GetInteractionId

`func (o *Activity) GetInteractionId() string`

GetInteractionId returns the InteractionId field if non-nil, zero value otherwise.

### GetInteractionIdOk

`func (o *Activity) GetInteractionIdOk() (*string, bool)`

GetInteractionIdOk returns a tuple with the InteractionId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInteractionId

`func (o *Activity) SetInteractionId(v string)`

SetInteractionId sets InteractionId field to given value.

### HasInteractionId

`func (o *Activity) HasInteractionId() bool`

HasInteractionId returns a boolean if a field has been set.

### GetEntryPointId

`func (o *Activity) GetEntryPointId() string`

GetEntryPointId returns the EntryPointId field if non-nil, zero value otherwise.

### GetEntryPointIdOk

`func (o *Activity) GetEntryPointIdOk() (*string, bool)`

GetEntryPointIdOk returns a tuple with the EntryPointId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEntryPointId

`func (o *Activity) SetEntryPointId(v string)`

SetEntryPointId sets EntryPointId field to given value.

### HasEntryPointId

`func (o *Activity) HasEntryPointId() bool`

HasEntryPointId returns a boolean if a field has been set.

### GetState

`func (o *Activity) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *Activity) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *Activity) SetState(v string)`

SetState sets State field to given value.

### HasState

`func (o *Activity) HasState() bool`

HasState returns a boolean if a field has been set.

### GetFile

`func (o *Activity) GetFile() ActivityFile`

GetFile returns the File field if non-nil, zero value otherwise.

### GetFileOk

`func (o *Activity) GetFileOk() (*ActivityFile, bool)`

GetFileOk returns a tuple with the File field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFile

`func (o *Activity) SetFile(v ActivityFile)`

SetFile sets File field to given value.

### HasFile

`func (o *Activity) HasFile() bool`

HasFile returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


