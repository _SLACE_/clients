# \MessagingAPI

All URIs are relative to *https://api.slace.com/inbox*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetChannelMessages**](MessagingAPI.md#GetChannelMessages) | **Get** /channels/{channel_id}/messages | Get Channel Messages
[**SendMessage**](MessagingAPI.md#SendMessage) | **Post** /channels/{channel_id}/messages | Send message
[**SendTemplateMessage**](MessagingAPI.md#SendTemplateMessage) | **Post** /channels/{channel_id}/messages/template | Send template message



## GetChannelMessages

> ConversationMessagesList GetChannelMessages(ctx, channelId).Labels(labels).After(after).Before(before).Offset(offset).Limit(limit).Sort(sort).Execute()

Get Channel Messages

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    "time"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    labels := []string{"Inner_example"} // []string |  (optional)
    after := time.Now() // time.Time |  (optional)
    before := time.Now() // time.Time |  (optional)
    offset := int32(56) // int32 |  (optional)
    limit := int32(56) // int32 |  (optional)
    sort := "sort_example" // string |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MessagingAPI.GetChannelMessages(context.Background(), channelId).Labels(labels).After(after).Before(before).Offset(offset).Limit(limit).Sort(sort).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MessagingAPI.GetChannelMessages``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetChannelMessages`: ConversationMessagesList
    fmt.Fprintf(os.Stdout, "Response from `MessagingAPI.GetChannelMessages`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetChannelMessagesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **labels** | **[]string** |  | 
 **after** | **time.Time** |  | 
 **before** | **time.Time** |  | 
 **offset** | **int32** |  | 
 **limit** | **int32** |  | 
 **sort** | **string** |  | 

### Return type

[**ConversationMessagesList**](ConversationMessagesList.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SendMessage

> ConversationMessage SendMessage(ctx, channelId).SendMessageRequest(sendMessageRequest).Execute()

Send message



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    sendMessageRequest := *openapiclient.NewSendMessageRequest("To_example", openapiclient.SendMessageType("text"), "GatewayId_example") // SendMessageRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MessagingAPI.SendMessage(context.Background(), channelId).SendMessageRequest(sendMessageRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MessagingAPI.SendMessage``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SendMessage`: ConversationMessage
    fmt.Fprintf(os.Stdout, "Response from `MessagingAPI.SendMessage`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiSendMessageRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **sendMessageRequest** | [**SendMessageRequest**](SendMessageRequest.md) |  | 

### Return type

[**ConversationMessage**](ConversationMessage.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SendTemplateMessage

> ConversationMessage SendTemplateMessage(ctx, channelId).SendTemplateRequest(sendTemplateRequest).Execute()

Send template message



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "bitbucket.org/_SLACE_/clients/go/inboxapi"
)

func main() {
    channelId := "channelId_example" // string | 
    sendTemplateRequest := *openapiclient.NewSendTemplateRequest("To_example", "GatewayId_example", "TemplateId_example", "TemplateName_example") // SendTemplateRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.MessagingAPI.SendTemplateMessage(context.Background(), channelId).SendTemplateRequest(sendTemplateRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MessagingAPI.SendTemplateMessage``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SendTemplateMessage`: ConversationMessage
    fmt.Fprintf(os.Stdout, "Response from `MessagingAPI.SendTemplateMessage`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**channelId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiSendTemplateMessageRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **sendTemplateRequest** | [**SendTemplateRequest**](SendTemplateRequest.md) |  | 

### Return type

[**ConversationMessage**](ConversationMessage.md)

### Authorization

[Authorization](../README.md#Authorization)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

