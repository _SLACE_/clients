# ConversationParamsTranslateRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Params** | **[]string** |  | 
**Context** | Pointer to [**SendMessageContext**](SendMessageContext.md) |  | [optional] 

## Methods

### NewConversationParamsTranslateRequest

`func NewConversationParamsTranslateRequest(params []string, ) *ConversationParamsTranslateRequest`

NewConversationParamsTranslateRequest instantiates a new ConversationParamsTranslateRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConversationParamsTranslateRequestWithDefaults

`func NewConversationParamsTranslateRequestWithDefaults() *ConversationParamsTranslateRequest`

NewConversationParamsTranslateRequestWithDefaults instantiates a new ConversationParamsTranslateRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetParams

`func (o *ConversationParamsTranslateRequest) GetParams() []string`

GetParams returns the Params field if non-nil, zero value otherwise.

### GetParamsOk

`func (o *ConversationParamsTranslateRequest) GetParamsOk() (*[]string, bool)`

GetParamsOk returns a tuple with the Params field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParams

`func (o *ConversationParamsTranslateRequest) SetParams(v []string)`

SetParams sets Params field to given value.


### GetContext

`func (o *ConversationParamsTranslateRequest) GetContext() SendMessageContext`

GetContext returns the Context field if non-nil, zero value otherwise.

### GetContextOk

`func (o *ConversationParamsTranslateRequest) GetContextOk() (*SendMessageContext, bool)`

GetContextOk returns a tuple with the Context field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContext

`func (o *ConversationParamsTranslateRequest) SetContext(v SendMessageContext)`

SetContext sets Context field to given value.

### HasContext

`func (o *ConversationParamsTranslateRequest) HasContext() bool`

HasContext returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


