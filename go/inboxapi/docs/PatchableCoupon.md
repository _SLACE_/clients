# PatchableCoupon

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Status** | Pointer to **string** |  | [optional] 
**RedeemedAt** | Pointer to **time.Time** |  | [optional] 
**ExpiresAt** | Pointer to **time.Time** |  | [optional] 
**Attributes** | Pointer to **map[string][]string** |  | [optional] 
**TotalAmount** | Pointer to **string** |  | [optional] 
**ExpiresIn** | Pointer to **int32** |  | [optional] 
**LastSeenAt** | Pointer to **time.Time** |  | [optional] 
**DisplayExpiresIn** | Pointer to **int32** |  | [optional] 

## Methods

### NewPatchableCoupon

`func NewPatchableCoupon() *PatchableCoupon`

NewPatchableCoupon instantiates a new PatchableCoupon object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPatchableCouponWithDefaults

`func NewPatchableCouponWithDefaults() *PatchableCoupon`

NewPatchableCouponWithDefaults instantiates a new PatchableCoupon object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStatus

`func (o *PatchableCoupon) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *PatchableCoupon) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *PatchableCoupon) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *PatchableCoupon) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetRedeemedAt

`func (o *PatchableCoupon) GetRedeemedAt() time.Time`

GetRedeemedAt returns the RedeemedAt field if non-nil, zero value otherwise.

### GetRedeemedAtOk

`func (o *PatchableCoupon) GetRedeemedAtOk() (*time.Time, bool)`

GetRedeemedAtOk returns a tuple with the RedeemedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRedeemedAt

`func (o *PatchableCoupon) SetRedeemedAt(v time.Time)`

SetRedeemedAt sets RedeemedAt field to given value.

### HasRedeemedAt

`func (o *PatchableCoupon) HasRedeemedAt() bool`

HasRedeemedAt returns a boolean if a field has been set.

### GetExpiresAt

`func (o *PatchableCoupon) GetExpiresAt() time.Time`

GetExpiresAt returns the ExpiresAt field if non-nil, zero value otherwise.

### GetExpiresAtOk

`func (o *PatchableCoupon) GetExpiresAtOk() (*time.Time, bool)`

GetExpiresAtOk returns a tuple with the ExpiresAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiresAt

`func (o *PatchableCoupon) SetExpiresAt(v time.Time)`

SetExpiresAt sets ExpiresAt field to given value.

### HasExpiresAt

`func (o *PatchableCoupon) HasExpiresAt() bool`

HasExpiresAt returns a boolean if a field has been set.

### GetAttributes

`func (o *PatchableCoupon) GetAttributes() map[string][]string`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *PatchableCoupon) GetAttributesOk() (*map[string][]string, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *PatchableCoupon) SetAttributes(v map[string][]string)`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *PatchableCoupon) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetTotalAmount

`func (o *PatchableCoupon) GetTotalAmount() string`

GetTotalAmount returns the TotalAmount field if non-nil, zero value otherwise.

### GetTotalAmountOk

`func (o *PatchableCoupon) GetTotalAmountOk() (*string, bool)`

GetTotalAmountOk returns a tuple with the TotalAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalAmount

`func (o *PatchableCoupon) SetTotalAmount(v string)`

SetTotalAmount sets TotalAmount field to given value.

### HasTotalAmount

`func (o *PatchableCoupon) HasTotalAmount() bool`

HasTotalAmount returns a boolean if a field has been set.

### GetExpiresIn

`func (o *PatchableCoupon) GetExpiresIn() int32`

GetExpiresIn returns the ExpiresIn field if non-nil, zero value otherwise.

### GetExpiresInOk

`func (o *PatchableCoupon) GetExpiresInOk() (*int32, bool)`

GetExpiresInOk returns a tuple with the ExpiresIn field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExpiresIn

`func (o *PatchableCoupon) SetExpiresIn(v int32)`

SetExpiresIn sets ExpiresIn field to given value.

### HasExpiresIn

`func (o *PatchableCoupon) HasExpiresIn() bool`

HasExpiresIn returns a boolean if a field has been set.

### GetLastSeenAt

`func (o *PatchableCoupon) GetLastSeenAt() time.Time`

GetLastSeenAt returns the LastSeenAt field if non-nil, zero value otherwise.

### GetLastSeenAtOk

`func (o *PatchableCoupon) GetLastSeenAtOk() (*time.Time, bool)`

GetLastSeenAtOk returns a tuple with the LastSeenAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastSeenAt

`func (o *PatchableCoupon) SetLastSeenAt(v time.Time)`

SetLastSeenAt sets LastSeenAt field to given value.

### HasLastSeenAt

`func (o *PatchableCoupon) HasLastSeenAt() bool`

HasLastSeenAt returns a boolean if a field has been set.

### GetDisplayExpiresIn

`func (o *PatchableCoupon) GetDisplayExpiresIn() int32`

GetDisplayExpiresIn returns the DisplayExpiresIn field if non-nil, zero value otherwise.

### GetDisplayExpiresInOk

`func (o *PatchableCoupon) GetDisplayExpiresInOk() (*int32, bool)`

GetDisplayExpiresInOk returns a tuple with the DisplayExpiresIn field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayExpiresIn

`func (o *PatchableCoupon) SetDisplayExpiresIn(v int32)`

SetDisplayExpiresIn sets DisplayExpiresIn field to given value.

### HasDisplayExpiresIn

`func (o *PatchableCoupon) HasDisplayExpiresIn() bool`

HasDisplayExpiresIn returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


