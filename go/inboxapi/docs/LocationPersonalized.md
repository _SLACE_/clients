# LocationPersonalized

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Longitude** | **string** |  | 
**Latitude** | **string** |  | 
**Name** | Pointer to **string** |  | [optional] 
**Address** | Pointer to **string** |  | [optional] 

## Methods

### NewLocationPersonalized

`func NewLocationPersonalized(longitude string, latitude string, ) *LocationPersonalized`

NewLocationPersonalized instantiates a new LocationPersonalized object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLocationPersonalizedWithDefaults

`func NewLocationPersonalizedWithDefaults() *LocationPersonalized`

NewLocationPersonalizedWithDefaults instantiates a new LocationPersonalized object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLongitude

`func (o *LocationPersonalized) GetLongitude() string`

GetLongitude returns the Longitude field if non-nil, zero value otherwise.

### GetLongitudeOk

`func (o *LocationPersonalized) GetLongitudeOk() (*string, bool)`

GetLongitudeOk returns a tuple with the Longitude field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLongitude

`func (o *LocationPersonalized) SetLongitude(v string)`

SetLongitude sets Longitude field to given value.


### GetLatitude

`func (o *LocationPersonalized) GetLatitude() string`

GetLatitude returns the Latitude field if non-nil, zero value otherwise.

### GetLatitudeOk

`func (o *LocationPersonalized) GetLatitudeOk() (*string, bool)`

GetLatitudeOk returns a tuple with the Latitude field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLatitude

`func (o *LocationPersonalized) SetLatitude(v string)`

SetLatitude sets Latitude field to given value.


### GetName

`func (o *LocationPersonalized) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *LocationPersonalized) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *LocationPersonalized) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *LocationPersonalized) HasName() bool`

HasName returns a boolean if a field has been set.

### GetAddress

`func (o *LocationPersonalized) GetAddress() string`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *LocationPersonalized) GetAddressOk() (*string, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *LocationPersonalized) SetAddress(v string)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *LocationPersonalized) HasAddress() bool`

HasAddress returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


