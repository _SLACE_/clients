# WhatsAppTemplateCategory

## Enum


* `WhatsAppTemplateCategoryMarketing` (value: `"MARKETING"`)

* `WhatsAppTemplateCategoryUtility` (value: `"UTILITY"`)

* `WhatsAppTemplateCategoryAuthentication` (value: `"AUTHENTICATION"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


