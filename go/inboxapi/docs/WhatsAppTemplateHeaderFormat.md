# WhatsAppTemplateHeaderFormat

## Enum


* `WhatsAppTemplateHeaderFormatText` (value: `"TEXT"`)

* `WhatsAppTemplateHeaderFormatImage` (value: `"IMAGE"`)

* `WhatsAppTemplateHeaderFormatDocument` (value: `"DOCUMENT"`)

* `WhatsAppTemplateHeaderFormatVideo` (value: `"VIDEO"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


