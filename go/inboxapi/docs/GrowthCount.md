# GrowthCount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CurrentPeriodAmount** | **int32** |  | 
**FirstPeriodBackAmount** | **int32** |  | 
**SecondPeriodBackAmount** | **int32** |  | 

## Methods

### NewGrowthCount

`func NewGrowthCount(currentPeriodAmount int32, firstPeriodBackAmount int32, secondPeriodBackAmount int32, ) *GrowthCount`

NewGrowthCount instantiates a new GrowthCount object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGrowthCountWithDefaults

`func NewGrowthCountWithDefaults() *GrowthCount`

NewGrowthCountWithDefaults instantiates a new GrowthCount object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCurrentPeriodAmount

`func (o *GrowthCount) GetCurrentPeriodAmount() int32`

GetCurrentPeriodAmount returns the CurrentPeriodAmount field if non-nil, zero value otherwise.

### GetCurrentPeriodAmountOk

`func (o *GrowthCount) GetCurrentPeriodAmountOk() (*int32, bool)`

GetCurrentPeriodAmountOk returns a tuple with the CurrentPeriodAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrentPeriodAmount

`func (o *GrowthCount) SetCurrentPeriodAmount(v int32)`

SetCurrentPeriodAmount sets CurrentPeriodAmount field to given value.


### GetFirstPeriodBackAmount

`func (o *GrowthCount) GetFirstPeriodBackAmount() int32`

GetFirstPeriodBackAmount returns the FirstPeriodBackAmount field if non-nil, zero value otherwise.

### GetFirstPeriodBackAmountOk

`func (o *GrowthCount) GetFirstPeriodBackAmountOk() (*int32, bool)`

GetFirstPeriodBackAmountOk returns a tuple with the FirstPeriodBackAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstPeriodBackAmount

`func (o *GrowthCount) SetFirstPeriodBackAmount(v int32)`

SetFirstPeriodBackAmount sets FirstPeriodBackAmount field to given value.


### GetSecondPeriodBackAmount

`func (o *GrowthCount) GetSecondPeriodBackAmount() int32`

GetSecondPeriodBackAmount returns the SecondPeriodBackAmount field if non-nil, zero value otherwise.

### GetSecondPeriodBackAmountOk

`func (o *GrowthCount) GetSecondPeriodBackAmountOk() (*int32, bool)`

GetSecondPeriodBackAmountOk returns a tuple with the SecondPeriodBackAmount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSecondPeriodBackAmount

`func (o *GrowthCount) SetSecondPeriodBackAmount(v int32)`

SetSecondPeriodBackAmount sets SecondPeriodBackAmount field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


