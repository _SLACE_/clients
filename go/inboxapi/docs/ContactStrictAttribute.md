# ContactStrictAttribute

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to **string** |  | [optional] 
**Value** | Pointer to [**ContactStrictAttributeValue**](ContactStrictAttributeValue.md) |  | [optional] 

## Methods

### NewContactStrictAttribute

`func NewContactStrictAttribute() *ContactStrictAttribute`

NewContactStrictAttribute instantiates a new ContactStrictAttribute object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactStrictAttributeWithDefaults

`func NewContactStrictAttributeWithDefaults() *ContactStrictAttribute`

NewContactStrictAttributeWithDefaults instantiates a new ContactStrictAttribute object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *ContactStrictAttribute) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ContactStrictAttribute) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ContactStrictAttribute) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ContactStrictAttribute) HasType() bool`

HasType returns a boolean if a field has been set.

### GetValue

`func (o *ContactStrictAttribute) GetValue() ContactStrictAttributeValue`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *ContactStrictAttribute) GetValueOk() (*ContactStrictAttributeValue, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *ContactStrictAttribute) SetValue(v ContactStrictAttributeValue)`

SetValue sets Value field to given value.

### HasValue

`func (o *ContactStrictAttribute) HasValue() bool`

HasValue returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


