/*
Inbox

Inbox API can be used for low level integration and enables direct communication with you contact base.   In most cases you will use our interaction templates for communication, but our api also allows you to send message directly.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package inboxapi

import (
	"encoding/json"
)

// checks if the WhatsAppTemplateCard type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &WhatsAppTemplateCard{}

// WhatsAppTemplateCard struct for WhatsAppTemplateCard
type WhatsAppTemplateCard struct {
	Components []WhatsAppTemplateComponent `json:"components,omitempty"`
}

// NewWhatsAppTemplateCard instantiates a new WhatsAppTemplateCard object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewWhatsAppTemplateCard() *WhatsAppTemplateCard {
	this := WhatsAppTemplateCard{}
	return &this
}

// NewWhatsAppTemplateCardWithDefaults instantiates a new WhatsAppTemplateCard object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewWhatsAppTemplateCardWithDefaults() *WhatsAppTemplateCard {
	this := WhatsAppTemplateCard{}
	return &this
}

// GetComponents returns the Components field value if set, zero value otherwise.
func (o *WhatsAppTemplateCard) GetComponents() []WhatsAppTemplateComponent {
	if o == nil || IsNil(o.Components) {
		var ret []WhatsAppTemplateComponent
		return ret
	}
	return o.Components
}

// GetComponentsOk returns a tuple with the Components field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateCard) GetComponentsOk() ([]WhatsAppTemplateComponent, bool) {
	if o == nil || IsNil(o.Components) {
		return nil, false
	}
	return o.Components, true
}

// HasComponents returns a boolean if a field has been set.
func (o *WhatsAppTemplateCard) HasComponents() bool {
	if o != nil && !IsNil(o.Components) {
		return true
	}

	return false
}

// SetComponents gets a reference to the given []WhatsAppTemplateComponent and assigns it to the Components field.
func (o *WhatsAppTemplateCard) SetComponents(v []WhatsAppTemplateComponent) {
	o.Components = v
}

func (o WhatsAppTemplateCard) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o WhatsAppTemplateCard) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Components) {
		toSerialize["components"] = o.Components
	}
	return toSerialize, nil
}

type NullableWhatsAppTemplateCard struct {
	value *WhatsAppTemplateCard
	isSet bool
}

func (v NullableWhatsAppTemplateCard) Get() *WhatsAppTemplateCard {
	return v.value
}

func (v *NullableWhatsAppTemplateCard) Set(val *WhatsAppTemplateCard) {
	v.value = val
	v.isSet = true
}

func (v NullableWhatsAppTemplateCard) IsSet() bool {
	return v.isSet
}

func (v *NullableWhatsAppTemplateCard) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableWhatsAppTemplateCard(val *WhatsAppTemplateCard) *NullableWhatsAppTemplateCard {
	return &NullableWhatsAppTemplateCard{value: val, isSet: true}
}

func (v NullableWhatsAppTemplateCard) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableWhatsAppTemplateCard) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


