/*
Inbox

Inbox API can be used for low level integration and enables direct communication with you contact base.   In most cases you will use our interaction templates for communication, but our api also allows you to send message directly.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package inboxapi

import (
	"encoding/json"
)

// checks if the ListContactConversations200Response type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ListContactConversations200Response{}

// ListContactConversations200Response struct for ListContactConversations200Response
type ListContactConversations200Response struct {
	Total *float32 `json:"total,omitempty"`
	Results []Conversation `json:"results,omitempty"`
}

// NewListContactConversations200Response instantiates a new ListContactConversations200Response object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewListContactConversations200Response() *ListContactConversations200Response {
	this := ListContactConversations200Response{}
	return &this
}

// NewListContactConversations200ResponseWithDefaults instantiates a new ListContactConversations200Response object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewListContactConversations200ResponseWithDefaults() *ListContactConversations200Response {
	this := ListContactConversations200Response{}
	return &this
}

// GetTotal returns the Total field value if set, zero value otherwise.
func (o *ListContactConversations200Response) GetTotal() float32 {
	if o == nil || IsNil(o.Total) {
		var ret float32
		return ret
	}
	return *o.Total
}

// GetTotalOk returns a tuple with the Total field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ListContactConversations200Response) GetTotalOk() (*float32, bool) {
	if o == nil || IsNil(o.Total) {
		return nil, false
	}
	return o.Total, true
}

// HasTotal returns a boolean if a field has been set.
func (o *ListContactConversations200Response) HasTotal() bool {
	if o != nil && !IsNil(o.Total) {
		return true
	}

	return false
}

// SetTotal gets a reference to the given float32 and assigns it to the Total field.
func (o *ListContactConversations200Response) SetTotal(v float32) {
	o.Total = &v
}

// GetResults returns the Results field value if set, zero value otherwise.
func (o *ListContactConversations200Response) GetResults() []Conversation {
	if o == nil || IsNil(o.Results) {
		var ret []Conversation
		return ret
	}
	return o.Results
}

// GetResultsOk returns a tuple with the Results field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ListContactConversations200Response) GetResultsOk() ([]Conversation, bool) {
	if o == nil || IsNil(o.Results) {
		return nil, false
	}
	return o.Results, true
}

// HasResults returns a boolean if a field has been set.
func (o *ListContactConversations200Response) HasResults() bool {
	if o != nil && !IsNil(o.Results) {
		return true
	}

	return false
}

// SetResults gets a reference to the given []Conversation and assigns it to the Results field.
func (o *ListContactConversations200Response) SetResults(v []Conversation) {
	o.Results = v
}

func (o ListContactConversations200Response) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ListContactConversations200Response) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Total) {
		toSerialize["total"] = o.Total
	}
	if !IsNil(o.Results) {
		toSerialize["results"] = o.Results
	}
	return toSerialize, nil
}

type NullableListContactConversations200Response struct {
	value *ListContactConversations200Response
	isSet bool
}

func (v NullableListContactConversations200Response) Get() *ListContactConversations200Response {
	return v.value
}

func (v *NullableListContactConversations200Response) Set(val *ListContactConversations200Response) {
	v.value = val
	v.isSet = true
}

func (v NullableListContactConversations200Response) IsSet() bool {
	return v.isSet
}

func (v *NullableListContactConversations200Response) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableListContactConversations200Response(val *ListContactConversations200Response) *NullableListContactConversations200Response {
	return &NullableListContactConversations200Response{value: val, isSet: true}
}

func (v NullableListContactConversations200Response) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableListContactConversations200Response) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


