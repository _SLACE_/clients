/*
Inbox

Inbox API can be used for low level integration and enables direct communication with you contact base.   In most cases you will use our interaction templates for communication, but our api also allows you to send message directly.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package inboxapi

import (
	"encoding/json"
	"fmt"
)

// Messenger the model 'Messenger'
type Messenger string

// List of Messenger
const (
	MESSENGER_WHATS_APP Messenger = "WhatsApp"
	MESSENGER_TELEGRAM_BOT Messenger = "TelegramBot"
	MESSENGER_WEBCHAT Messenger = "Webchat"
	MESSENGER_VIBER_CHATBOTS Messenger = "ViberChatbots"
	MESSENGER_FACEBOOK_MESSENGER Messenger = "FacebookMessenger"
	MESSENGER_INSTAGRAM_MESSENGER Messenger = "InstagramMessenger"
	MESSENGER_SMS Messenger = "SMS"
	MESSENGER_GOOGLE_BM Messenger = "GoogleBM"
	MESSENGER_IN_APP Messenger = "InApp"
)

// All allowed values of Messenger enum
var AllowedMessengerEnumValues = []Messenger{
	"WhatsApp",
	"TelegramBot",
	"Webchat",
	"ViberChatbots",
	"FacebookMessenger",
	"InstagramMessenger",
	"SMS",
	"GoogleBM",
	"InApp",
}

func (v *Messenger) UnmarshalJSON(src []byte) error {
	var value string
	err := json.Unmarshal(src, &value)
	if err != nil {
		return err
	}
	enumTypeValue := Messenger(value)
	for _, existing := range AllowedMessengerEnumValues {
		if existing == enumTypeValue {
			*v = enumTypeValue
			return nil
		}
	}

	return fmt.Errorf("%+v is not a valid Messenger", value)
}

// NewMessengerFromValue returns a pointer to a valid Messenger
// for the value passed as argument, or an error if the value passed is not allowed by the enum
func NewMessengerFromValue(v string) (*Messenger, error) {
	ev := Messenger(v)
	if ev.IsValid() {
		return &ev, nil
	} else {
		return nil, fmt.Errorf("invalid value '%v' for Messenger: valid values are %v", v, AllowedMessengerEnumValues)
	}
}

// IsValid return true if the value is valid for the enum, false otherwise
func (v Messenger) IsValid() bool {
	for _, existing := range AllowedMessengerEnumValues {
		if existing == v {
			return true
		}
	}
	return false
}

// Ptr returns reference to Messenger value
func (v Messenger) Ptr() *Messenger {
	return &v
}

type NullableMessenger struct {
	value *Messenger
	isSet bool
}

func (v NullableMessenger) Get() *Messenger {
	return v.value
}

func (v *NullableMessenger) Set(val *Messenger) {
	v.value = val
	v.isSet = true
}

func (v NullableMessenger) IsSet() bool {
	return v.isSet
}

func (v *NullableMessenger) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMessenger(val *Messenger) *NullableMessenger {
	return &NullableMessenger{value: val, isSet: true}
}

func (v NullableMessenger) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMessenger) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}

