/*
Inbox

Inbox API can be used for low level integration and enables direct communication with you contact base.   In most cases you will use our interaction templates for communication, but our api also allows you to send message directly.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package inboxapi

import (
	"encoding/json"
)

// checks if the MessageOrderDetailsItemsInner type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &MessageOrderDetailsItemsInner{}

// MessageOrderDetailsItemsInner struct for MessageOrderDetailsItemsInner
type MessageOrderDetailsItemsInner struct {
	Name string `json:"name"`
	// Price as string with dot separator ie \"12.61\"
	Amount string `json:"amount"`
	Quantity int32 `json:"quantity"`
	Discount *string `json:"discount,omitempty"`
	Tax *string `json:"tax,omitempty"`
}

// NewMessageOrderDetailsItemsInner instantiates a new MessageOrderDetailsItemsInner object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMessageOrderDetailsItemsInner(name string, amount string, quantity int32) *MessageOrderDetailsItemsInner {
	this := MessageOrderDetailsItemsInner{}
	this.Name = name
	this.Amount = amount
	this.Quantity = quantity
	return &this
}

// NewMessageOrderDetailsItemsInnerWithDefaults instantiates a new MessageOrderDetailsItemsInner object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMessageOrderDetailsItemsInnerWithDefaults() *MessageOrderDetailsItemsInner {
	this := MessageOrderDetailsItemsInner{}
	var quantity int32 = 1
	this.Quantity = quantity
	return &this
}

// GetName returns the Name field value
func (o *MessageOrderDetailsItemsInner) GetName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Name
}

// GetNameOk returns a tuple with the Name field value
// and a boolean to check if the value has been set.
func (o *MessageOrderDetailsItemsInner) GetNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Name, true
}

// SetName sets field value
func (o *MessageOrderDetailsItemsInner) SetName(v string) {
	o.Name = v
}

// GetAmount returns the Amount field value
func (o *MessageOrderDetailsItemsInner) GetAmount() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Amount
}

// GetAmountOk returns a tuple with the Amount field value
// and a boolean to check if the value has been set.
func (o *MessageOrderDetailsItemsInner) GetAmountOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Amount, true
}

// SetAmount sets field value
func (o *MessageOrderDetailsItemsInner) SetAmount(v string) {
	o.Amount = v
}

// GetQuantity returns the Quantity field value
func (o *MessageOrderDetailsItemsInner) GetQuantity() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.Quantity
}

// GetQuantityOk returns a tuple with the Quantity field value
// and a boolean to check if the value has been set.
func (o *MessageOrderDetailsItemsInner) GetQuantityOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Quantity, true
}

// SetQuantity sets field value
func (o *MessageOrderDetailsItemsInner) SetQuantity(v int32) {
	o.Quantity = v
}

// GetDiscount returns the Discount field value if set, zero value otherwise.
func (o *MessageOrderDetailsItemsInner) GetDiscount() string {
	if o == nil || IsNil(o.Discount) {
		var ret string
		return ret
	}
	return *o.Discount
}

// GetDiscountOk returns a tuple with the Discount field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageOrderDetailsItemsInner) GetDiscountOk() (*string, bool) {
	if o == nil || IsNil(o.Discount) {
		return nil, false
	}
	return o.Discount, true
}

// HasDiscount returns a boolean if a field has been set.
func (o *MessageOrderDetailsItemsInner) HasDiscount() bool {
	if o != nil && !IsNil(o.Discount) {
		return true
	}

	return false
}

// SetDiscount gets a reference to the given string and assigns it to the Discount field.
func (o *MessageOrderDetailsItemsInner) SetDiscount(v string) {
	o.Discount = &v
}

// GetTax returns the Tax field value if set, zero value otherwise.
func (o *MessageOrderDetailsItemsInner) GetTax() string {
	if o == nil || IsNil(o.Tax) {
		var ret string
		return ret
	}
	return *o.Tax
}

// GetTaxOk returns a tuple with the Tax field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageOrderDetailsItemsInner) GetTaxOk() (*string, bool) {
	if o == nil || IsNil(o.Tax) {
		return nil, false
	}
	return o.Tax, true
}

// HasTax returns a boolean if a field has been set.
func (o *MessageOrderDetailsItemsInner) HasTax() bool {
	if o != nil && !IsNil(o.Tax) {
		return true
	}

	return false
}

// SetTax gets a reference to the given string and assigns it to the Tax field.
func (o *MessageOrderDetailsItemsInner) SetTax(v string) {
	o.Tax = &v
}

func (o MessageOrderDetailsItemsInner) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o MessageOrderDetailsItemsInner) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["name"] = o.Name
	toSerialize["amount"] = o.Amount
	toSerialize["quantity"] = o.Quantity
	if !IsNil(o.Discount) {
		toSerialize["discount"] = o.Discount
	}
	if !IsNil(o.Tax) {
		toSerialize["tax"] = o.Tax
	}
	return toSerialize, nil
}

type NullableMessageOrderDetailsItemsInner struct {
	value *MessageOrderDetailsItemsInner
	isSet bool
}

func (v NullableMessageOrderDetailsItemsInner) Get() *MessageOrderDetailsItemsInner {
	return v.value
}

func (v *NullableMessageOrderDetailsItemsInner) Set(val *MessageOrderDetailsItemsInner) {
	v.value = val
	v.isSet = true
}

func (v NullableMessageOrderDetailsItemsInner) IsSet() bool {
	return v.isSet
}

func (v *NullableMessageOrderDetailsItemsInner) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMessageOrderDetailsItemsInner(val *MessageOrderDetailsItemsInner) *NullableMessageOrderDetailsItemsInner {
	return &NullableMessageOrderDetailsItemsInner{value: val, isSet: true}
}

func (v NullableMessageOrderDetailsItemsInner) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMessageOrderDetailsItemsInner) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


