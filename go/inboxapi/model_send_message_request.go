/*
Inbox

Inbox API can be used for low level integration and enables direct communication with you contact base.   In most cases you will use our interaction templates for communication, but our api also allows you to send message directly.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package inboxapi

import (
	"encoding/json"
)

// checks if the SendMessageRequest type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &SendMessageRequest{}

// SendMessageRequest struct for SendMessageRequest
type SendMessageRequest struct {
	To string `json:"to"`
	ReceiverType *ReceiverType `json:"receiver_type,omitempty"`
	Type SendMessageType `json:"type"`
	GatewayId string `json:"gateway_id"`
	Text *string `json:"text,omitempty"`
	MediaId *string `json:"media_id,omitempty"`
	MediaUrl *string `json:"media_url,omitempty"`
	MediaFilename *string `json:"media_filename,omitempty"`
	Source *string `json:"source,omitempty"`
	Labels []string `json:"labels,omitempty"`
	Location *LocationPersonalized `json:"location,omitempty"`
	Buttons []MessageButton `json:"buttons,omitempty"`
	Header *MessageHeader `json:"header,omitempty"`
	Footer *MessageFooter `json:"footer,omitempty"`
	List *List `json:"list,omitempty"`
	Contacts []MessageContact `json:"contacts,omitempty"`
	Context *SendMessageContext `json:"context,omitempty"`
	MessengerTag *MessengerTag `json:"messenger_tag,omitempty"`
	OrderDetails *MessageOrderDetails `json:"order_details,omitempty"`
	OrderStatus *MessageOrderStatus `json:"order_status,omitempty"`
	ButtonLabel *string `json:"button_label,omitempty"`
	// For internal use only, in standard api call this parameter will be ignored.
	SenderUserId *string `json:"sender_user_id,omitempty"`
	Reaction *MessageReaction `json:"reaction,omitempty"`
	Cards []SendMessageCard `json:"cards,omitempty"`
}

// NewSendMessageRequest instantiates a new SendMessageRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewSendMessageRequest(to string, type_ SendMessageType, gatewayId string) *SendMessageRequest {
	this := SendMessageRequest{}
	this.To = to
	this.Type = type_
	this.GatewayId = gatewayId
	return &this
}

// NewSendMessageRequestWithDefaults instantiates a new SendMessageRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewSendMessageRequestWithDefaults() *SendMessageRequest {
	this := SendMessageRequest{}
	return &this
}

// GetTo returns the To field value
func (o *SendMessageRequest) GetTo() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.To
}

// GetToOk returns a tuple with the To field value
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetToOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.To, true
}

// SetTo sets field value
func (o *SendMessageRequest) SetTo(v string) {
	o.To = v
}

// GetReceiverType returns the ReceiverType field value if set, zero value otherwise.
func (o *SendMessageRequest) GetReceiverType() ReceiverType {
	if o == nil || IsNil(o.ReceiverType) {
		var ret ReceiverType
		return ret
	}
	return *o.ReceiverType
}

// GetReceiverTypeOk returns a tuple with the ReceiverType field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetReceiverTypeOk() (*ReceiverType, bool) {
	if o == nil || IsNil(o.ReceiverType) {
		return nil, false
	}
	return o.ReceiverType, true
}

// HasReceiverType returns a boolean if a field has been set.
func (o *SendMessageRequest) HasReceiverType() bool {
	if o != nil && !IsNil(o.ReceiverType) {
		return true
	}

	return false
}

// SetReceiverType gets a reference to the given ReceiverType and assigns it to the ReceiverType field.
func (o *SendMessageRequest) SetReceiverType(v ReceiverType) {
	o.ReceiverType = &v
}

// GetType returns the Type field value
func (o *SendMessageRequest) GetType() SendMessageType {
	if o == nil {
		var ret SendMessageType
		return ret
	}

	return o.Type
}

// GetTypeOk returns a tuple with the Type field value
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetTypeOk() (*SendMessageType, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Type, true
}

// SetType sets field value
func (o *SendMessageRequest) SetType(v SendMessageType) {
	o.Type = v
}

// GetGatewayId returns the GatewayId field value
func (o *SendMessageRequest) GetGatewayId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.GatewayId
}

// GetGatewayIdOk returns a tuple with the GatewayId field value
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetGatewayIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.GatewayId, true
}

// SetGatewayId sets field value
func (o *SendMessageRequest) SetGatewayId(v string) {
	o.GatewayId = v
}

// GetText returns the Text field value if set, zero value otherwise.
func (o *SendMessageRequest) GetText() string {
	if o == nil || IsNil(o.Text) {
		var ret string
		return ret
	}
	return *o.Text
}

// GetTextOk returns a tuple with the Text field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetTextOk() (*string, bool) {
	if o == nil || IsNil(o.Text) {
		return nil, false
	}
	return o.Text, true
}

// HasText returns a boolean if a field has been set.
func (o *SendMessageRequest) HasText() bool {
	if o != nil && !IsNil(o.Text) {
		return true
	}

	return false
}

// SetText gets a reference to the given string and assigns it to the Text field.
func (o *SendMessageRequest) SetText(v string) {
	o.Text = &v
}

// GetMediaId returns the MediaId field value if set, zero value otherwise.
func (o *SendMessageRequest) GetMediaId() string {
	if o == nil || IsNil(o.MediaId) {
		var ret string
		return ret
	}
	return *o.MediaId
}

// GetMediaIdOk returns a tuple with the MediaId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetMediaIdOk() (*string, bool) {
	if o == nil || IsNil(o.MediaId) {
		return nil, false
	}
	return o.MediaId, true
}

// HasMediaId returns a boolean if a field has been set.
func (o *SendMessageRequest) HasMediaId() bool {
	if o != nil && !IsNil(o.MediaId) {
		return true
	}

	return false
}

// SetMediaId gets a reference to the given string and assigns it to the MediaId field.
func (o *SendMessageRequest) SetMediaId(v string) {
	o.MediaId = &v
}

// GetMediaUrl returns the MediaUrl field value if set, zero value otherwise.
func (o *SendMessageRequest) GetMediaUrl() string {
	if o == nil || IsNil(o.MediaUrl) {
		var ret string
		return ret
	}
	return *o.MediaUrl
}

// GetMediaUrlOk returns a tuple with the MediaUrl field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetMediaUrlOk() (*string, bool) {
	if o == nil || IsNil(o.MediaUrl) {
		return nil, false
	}
	return o.MediaUrl, true
}

// HasMediaUrl returns a boolean if a field has been set.
func (o *SendMessageRequest) HasMediaUrl() bool {
	if o != nil && !IsNil(o.MediaUrl) {
		return true
	}

	return false
}

// SetMediaUrl gets a reference to the given string and assigns it to the MediaUrl field.
func (o *SendMessageRequest) SetMediaUrl(v string) {
	o.MediaUrl = &v
}

// GetMediaFilename returns the MediaFilename field value if set, zero value otherwise.
func (o *SendMessageRequest) GetMediaFilename() string {
	if o == nil || IsNil(o.MediaFilename) {
		var ret string
		return ret
	}
	return *o.MediaFilename
}

// GetMediaFilenameOk returns a tuple with the MediaFilename field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetMediaFilenameOk() (*string, bool) {
	if o == nil || IsNil(o.MediaFilename) {
		return nil, false
	}
	return o.MediaFilename, true
}

// HasMediaFilename returns a boolean if a field has been set.
func (o *SendMessageRequest) HasMediaFilename() bool {
	if o != nil && !IsNil(o.MediaFilename) {
		return true
	}

	return false
}

// SetMediaFilename gets a reference to the given string and assigns it to the MediaFilename field.
func (o *SendMessageRequest) SetMediaFilename(v string) {
	o.MediaFilename = &v
}

// GetSource returns the Source field value if set, zero value otherwise.
func (o *SendMessageRequest) GetSource() string {
	if o == nil || IsNil(o.Source) {
		var ret string
		return ret
	}
	return *o.Source
}

// GetSourceOk returns a tuple with the Source field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetSourceOk() (*string, bool) {
	if o == nil || IsNil(o.Source) {
		return nil, false
	}
	return o.Source, true
}

// HasSource returns a boolean if a field has been set.
func (o *SendMessageRequest) HasSource() bool {
	if o != nil && !IsNil(o.Source) {
		return true
	}

	return false
}

// SetSource gets a reference to the given string and assigns it to the Source field.
func (o *SendMessageRequest) SetSource(v string) {
	o.Source = &v
}

// GetLabels returns the Labels field value if set, zero value otherwise.
func (o *SendMessageRequest) GetLabels() []string {
	if o == nil || IsNil(o.Labels) {
		var ret []string
		return ret
	}
	return o.Labels
}

// GetLabelsOk returns a tuple with the Labels field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetLabelsOk() ([]string, bool) {
	if o == nil || IsNil(o.Labels) {
		return nil, false
	}
	return o.Labels, true
}

// HasLabels returns a boolean if a field has been set.
func (o *SendMessageRequest) HasLabels() bool {
	if o != nil && !IsNil(o.Labels) {
		return true
	}

	return false
}

// SetLabels gets a reference to the given []string and assigns it to the Labels field.
func (o *SendMessageRequest) SetLabels(v []string) {
	o.Labels = v
}

// GetLocation returns the Location field value if set, zero value otherwise.
func (o *SendMessageRequest) GetLocation() LocationPersonalized {
	if o == nil || IsNil(o.Location) {
		var ret LocationPersonalized
		return ret
	}
	return *o.Location
}

// GetLocationOk returns a tuple with the Location field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetLocationOk() (*LocationPersonalized, bool) {
	if o == nil || IsNil(o.Location) {
		return nil, false
	}
	return o.Location, true
}

// HasLocation returns a boolean if a field has been set.
func (o *SendMessageRequest) HasLocation() bool {
	if o != nil && !IsNil(o.Location) {
		return true
	}

	return false
}

// SetLocation gets a reference to the given LocationPersonalized and assigns it to the Location field.
func (o *SendMessageRequest) SetLocation(v LocationPersonalized) {
	o.Location = &v
}

// GetButtons returns the Buttons field value if set, zero value otherwise.
func (o *SendMessageRequest) GetButtons() []MessageButton {
	if o == nil || IsNil(o.Buttons) {
		var ret []MessageButton
		return ret
	}
	return o.Buttons
}

// GetButtonsOk returns a tuple with the Buttons field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetButtonsOk() ([]MessageButton, bool) {
	if o == nil || IsNil(o.Buttons) {
		return nil, false
	}
	return o.Buttons, true
}

// HasButtons returns a boolean if a field has been set.
func (o *SendMessageRequest) HasButtons() bool {
	if o != nil && !IsNil(o.Buttons) {
		return true
	}

	return false
}

// SetButtons gets a reference to the given []MessageButton and assigns it to the Buttons field.
func (o *SendMessageRequest) SetButtons(v []MessageButton) {
	o.Buttons = v
}

// GetHeader returns the Header field value if set, zero value otherwise.
func (o *SendMessageRequest) GetHeader() MessageHeader {
	if o == nil || IsNil(o.Header) {
		var ret MessageHeader
		return ret
	}
	return *o.Header
}

// GetHeaderOk returns a tuple with the Header field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetHeaderOk() (*MessageHeader, bool) {
	if o == nil || IsNil(o.Header) {
		return nil, false
	}
	return o.Header, true
}

// HasHeader returns a boolean if a field has been set.
func (o *SendMessageRequest) HasHeader() bool {
	if o != nil && !IsNil(o.Header) {
		return true
	}

	return false
}

// SetHeader gets a reference to the given MessageHeader and assigns it to the Header field.
func (o *SendMessageRequest) SetHeader(v MessageHeader) {
	o.Header = &v
}

// GetFooter returns the Footer field value if set, zero value otherwise.
func (o *SendMessageRequest) GetFooter() MessageFooter {
	if o == nil || IsNil(o.Footer) {
		var ret MessageFooter
		return ret
	}
	return *o.Footer
}

// GetFooterOk returns a tuple with the Footer field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetFooterOk() (*MessageFooter, bool) {
	if o == nil || IsNil(o.Footer) {
		return nil, false
	}
	return o.Footer, true
}

// HasFooter returns a boolean if a field has been set.
func (o *SendMessageRequest) HasFooter() bool {
	if o != nil && !IsNil(o.Footer) {
		return true
	}

	return false
}

// SetFooter gets a reference to the given MessageFooter and assigns it to the Footer field.
func (o *SendMessageRequest) SetFooter(v MessageFooter) {
	o.Footer = &v
}

// GetList returns the List field value if set, zero value otherwise.
func (o *SendMessageRequest) GetList() List {
	if o == nil || IsNil(o.List) {
		var ret List
		return ret
	}
	return *o.List
}

// GetListOk returns a tuple with the List field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetListOk() (*List, bool) {
	if o == nil || IsNil(o.List) {
		return nil, false
	}
	return o.List, true
}

// HasList returns a boolean if a field has been set.
func (o *SendMessageRequest) HasList() bool {
	if o != nil && !IsNil(o.List) {
		return true
	}

	return false
}

// SetList gets a reference to the given List and assigns it to the List field.
func (o *SendMessageRequest) SetList(v List) {
	o.List = &v
}

// GetContacts returns the Contacts field value if set, zero value otherwise.
func (o *SendMessageRequest) GetContacts() []MessageContact {
	if o == nil || IsNil(o.Contacts) {
		var ret []MessageContact
		return ret
	}
	return o.Contacts
}

// GetContactsOk returns a tuple with the Contacts field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetContactsOk() ([]MessageContact, bool) {
	if o == nil || IsNil(o.Contacts) {
		return nil, false
	}
	return o.Contacts, true
}

// HasContacts returns a boolean if a field has been set.
func (o *SendMessageRequest) HasContacts() bool {
	if o != nil && !IsNil(o.Contacts) {
		return true
	}

	return false
}

// SetContacts gets a reference to the given []MessageContact and assigns it to the Contacts field.
func (o *SendMessageRequest) SetContacts(v []MessageContact) {
	o.Contacts = v
}

// GetContext returns the Context field value if set, zero value otherwise.
func (o *SendMessageRequest) GetContext() SendMessageContext {
	if o == nil || IsNil(o.Context) {
		var ret SendMessageContext
		return ret
	}
	return *o.Context
}

// GetContextOk returns a tuple with the Context field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetContextOk() (*SendMessageContext, bool) {
	if o == nil || IsNil(o.Context) {
		return nil, false
	}
	return o.Context, true
}

// HasContext returns a boolean if a field has been set.
func (o *SendMessageRequest) HasContext() bool {
	if o != nil && !IsNil(o.Context) {
		return true
	}

	return false
}

// SetContext gets a reference to the given SendMessageContext and assigns it to the Context field.
func (o *SendMessageRequest) SetContext(v SendMessageContext) {
	o.Context = &v
}

// GetMessengerTag returns the MessengerTag field value if set, zero value otherwise.
func (o *SendMessageRequest) GetMessengerTag() MessengerTag {
	if o == nil || IsNil(o.MessengerTag) {
		var ret MessengerTag
		return ret
	}
	return *o.MessengerTag
}

// GetMessengerTagOk returns a tuple with the MessengerTag field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetMessengerTagOk() (*MessengerTag, bool) {
	if o == nil || IsNil(o.MessengerTag) {
		return nil, false
	}
	return o.MessengerTag, true
}

// HasMessengerTag returns a boolean if a field has been set.
func (o *SendMessageRequest) HasMessengerTag() bool {
	if o != nil && !IsNil(o.MessengerTag) {
		return true
	}

	return false
}

// SetMessengerTag gets a reference to the given MessengerTag and assigns it to the MessengerTag field.
func (o *SendMessageRequest) SetMessengerTag(v MessengerTag) {
	o.MessengerTag = &v
}

// GetOrderDetails returns the OrderDetails field value if set, zero value otherwise.
func (o *SendMessageRequest) GetOrderDetails() MessageOrderDetails {
	if o == nil || IsNil(o.OrderDetails) {
		var ret MessageOrderDetails
		return ret
	}
	return *o.OrderDetails
}

// GetOrderDetailsOk returns a tuple with the OrderDetails field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetOrderDetailsOk() (*MessageOrderDetails, bool) {
	if o == nil || IsNil(o.OrderDetails) {
		return nil, false
	}
	return o.OrderDetails, true
}

// HasOrderDetails returns a boolean if a field has been set.
func (o *SendMessageRequest) HasOrderDetails() bool {
	if o != nil && !IsNil(o.OrderDetails) {
		return true
	}

	return false
}

// SetOrderDetails gets a reference to the given MessageOrderDetails and assigns it to the OrderDetails field.
func (o *SendMessageRequest) SetOrderDetails(v MessageOrderDetails) {
	o.OrderDetails = &v
}

// GetOrderStatus returns the OrderStatus field value if set, zero value otherwise.
func (o *SendMessageRequest) GetOrderStatus() MessageOrderStatus {
	if o == nil || IsNil(o.OrderStatus) {
		var ret MessageOrderStatus
		return ret
	}
	return *o.OrderStatus
}

// GetOrderStatusOk returns a tuple with the OrderStatus field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetOrderStatusOk() (*MessageOrderStatus, bool) {
	if o == nil || IsNil(o.OrderStatus) {
		return nil, false
	}
	return o.OrderStatus, true
}

// HasOrderStatus returns a boolean if a field has been set.
func (o *SendMessageRequest) HasOrderStatus() bool {
	if o != nil && !IsNil(o.OrderStatus) {
		return true
	}

	return false
}

// SetOrderStatus gets a reference to the given MessageOrderStatus and assigns it to the OrderStatus field.
func (o *SendMessageRequest) SetOrderStatus(v MessageOrderStatus) {
	o.OrderStatus = &v
}

// GetButtonLabel returns the ButtonLabel field value if set, zero value otherwise.
func (o *SendMessageRequest) GetButtonLabel() string {
	if o == nil || IsNil(o.ButtonLabel) {
		var ret string
		return ret
	}
	return *o.ButtonLabel
}

// GetButtonLabelOk returns a tuple with the ButtonLabel field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetButtonLabelOk() (*string, bool) {
	if o == nil || IsNil(o.ButtonLabel) {
		return nil, false
	}
	return o.ButtonLabel, true
}

// HasButtonLabel returns a boolean if a field has been set.
func (o *SendMessageRequest) HasButtonLabel() bool {
	if o != nil && !IsNil(o.ButtonLabel) {
		return true
	}

	return false
}

// SetButtonLabel gets a reference to the given string and assigns it to the ButtonLabel field.
func (o *SendMessageRequest) SetButtonLabel(v string) {
	o.ButtonLabel = &v
}

// GetSenderUserId returns the SenderUserId field value if set, zero value otherwise.
func (o *SendMessageRequest) GetSenderUserId() string {
	if o == nil || IsNil(o.SenderUserId) {
		var ret string
		return ret
	}
	return *o.SenderUserId
}

// GetSenderUserIdOk returns a tuple with the SenderUserId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetSenderUserIdOk() (*string, bool) {
	if o == nil || IsNil(o.SenderUserId) {
		return nil, false
	}
	return o.SenderUserId, true
}

// HasSenderUserId returns a boolean if a field has been set.
func (o *SendMessageRequest) HasSenderUserId() bool {
	if o != nil && !IsNil(o.SenderUserId) {
		return true
	}

	return false
}

// SetSenderUserId gets a reference to the given string and assigns it to the SenderUserId field.
func (o *SendMessageRequest) SetSenderUserId(v string) {
	o.SenderUserId = &v
}

// GetReaction returns the Reaction field value if set, zero value otherwise.
func (o *SendMessageRequest) GetReaction() MessageReaction {
	if o == nil || IsNil(o.Reaction) {
		var ret MessageReaction
		return ret
	}
	return *o.Reaction
}

// GetReactionOk returns a tuple with the Reaction field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetReactionOk() (*MessageReaction, bool) {
	if o == nil || IsNil(o.Reaction) {
		return nil, false
	}
	return o.Reaction, true
}

// HasReaction returns a boolean if a field has been set.
func (o *SendMessageRequest) HasReaction() bool {
	if o != nil && !IsNil(o.Reaction) {
		return true
	}

	return false
}

// SetReaction gets a reference to the given MessageReaction and assigns it to the Reaction field.
func (o *SendMessageRequest) SetReaction(v MessageReaction) {
	o.Reaction = &v
}

// GetCards returns the Cards field value if set, zero value otherwise.
func (o *SendMessageRequest) GetCards() []SendMessageCard {
	if o == nil || IsNil(o.Cards) {
		var ret []SendMessageCard
		return ret
	}
	return o.Cards
}

// GetCardsOk returns a tuple with the Cards field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SendMessageRequest) GetCardsOk() ([]SendMessageCard, bool) {
	if o == nil || IsNil(o.Cards) {
		return nil, false
	}
	return o.Cards, true
}

// HasCards returns a boolean if a field has been set.
func (o *SendMessageRequest) HasCards() bool {
	if o != nil && !IsNil(o.Cards) {
		return true
	}

	return false
}

// SetCards gets a reference to the given []SendMessageCard and assigns it to the Cards field.
func (o *SendMessageRequest) SetCards(v []SendMessageCard) {
	o.Cards = v
}

func (o SendMessageRequest) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o SendMessageRequest) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["to"] = o.To
	if !IsNil(o.ReceiverType) {
		toSerialize["receiver_type"] = o.ReceiverType
	}
	toSerialize["type"] = o.Type
	toSerialize["gateway_id"] = o.GatewayId
	if !IsNil(o.Text) {
		toSerialize["text"] = o.Text
	}
	if !IsNil(o.MediaId) {
		toSerialize["media_id"] = o.MediaId
	}
	if !IsNil(o.MediaUrl) {
		toSerialize["media_url"] = o.MediaUrl
	}
	if !IsNil(o.MediaFilename) {
		toSerialize["media_filename"] = o.MediaFilename
	}
	if !IsNil(o.Source) {
		toSerialize["source"] = o.Source
	}
	if !IsNil(o.Labels) {
		toSerialize["labels"] = o.Labels
	}
	if !IsNil(o.Location) {
		toSerialize["location"] = o.Location
	}
	if !IsNil(o.Buttons) {
		toSerialize["buttons"] = o.Buttons
	}
	if !IsNil(o.Header) {
		toSerialize["header"] = o.Header
	}
	if !IsNil(o.Footer) {
		toSerialize["footer"] = o.Footer
	}
	if !IsNil(o.List) {
		toSerialize["list"] = o.List
	}
	if !IsNil(o.Contacts) {
		toSerialize["contacts"] = o.Contacts
	}
	if !IsNil(o.Context) {
		toSerialize["context"] = o.Context
	}
	if !IsNil(o.MessengerTag) {
		toSerialize["messenger_tag"] = o.MessengerTag
	}
	if !IsNil(o.OrderDetails) {
		toSerialize["order_details"] = o.OrderDetails
	}
	if !IsNil(o.OrderStatus) {
		toSerialize["order_status"] = o.OrderStatus
	}
	if !IsNil(o.ButtonLabel) {
		toSerialize["button_label"] = o.ButtonLabel
	}
	if !IsNil(o.SenderUserId) {
		toSerialize["sender_user_id"] = o.SenderUserId
	}
	if !IsNil(o.Reaction) {
		toSerialize["reaction"] = o.Reaction
	}
	if !IsNil(o.Cards) {
		toSerialize["cards"] = o.Cards
	}
	return toSerialize, nil
}

type NullableSendMessageRequest struct {
	value *SendMessageRequest
	isSet bool
}

func (v NullableSendMessageRequest) Get() *SendMessageRequest {
	return v.value
}

func (v *NullableSendMessageRequest) Set(val *SendMessageRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableSendMessageRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableSendMessageRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableSendMessageRequest(val *SendMessageRequest) *NullableSendMessageRequest {
	return &NullableSendMessageRequest{value: val, isSet: true}
}

func (v NullableSendMessageRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableSendMessageRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


