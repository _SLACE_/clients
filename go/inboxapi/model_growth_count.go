/*
Inbox

Inbox API can be used for low level integration and enables direct communication with you contact base.   In most cases you will use our interaction templates for communication, but our api also allows you to send message directly.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package inboxapi

import (
	"encoding/json"
)

// checks if the GrowthCount type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GrowthCount{}

// GrowthCount struct for GrowthCount
type GrowthCount struct {
	CurrentPeriodAmount int32 `json:"current_period_amount"`
	FirstPeriodBackAmount int32 `json:"first_period_back_amount"`
	SecondPeriodBackAmount int32 `json:"second_period_back_amount"`
}

// NewGrowthCount instantiates a new GrowthCount object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGrowthCount(currentPeriodAmount int32, firstPeriodBackAmount int32, secondPeriodBackAmount int32) *GrowthCount {
	this := GrowthCount{}
	this.CurrentPeriodAmount = currentPeriodAmount
	this.FirstPeriodBackAmount = firstPeriodBackAmount
	this.SecondPeriodBackAmount = secondPeriodBackAmount
	return &this
}

// NewGrowthCountWithDefaults instantiates a new GrowthCount object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGrowthCountWithDefaults() *GrowthCount {
	this := GrowthCount{}
	return &this
}

// GetCurrentPeriodAmount returns the CurrentPeriodAmount field value
func (o *GrowthCount) GetCurrentPeriodAmount() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.CurrentPeriodAmount
}

// GetCurrentPeriodAmountOk returns a tuple with the CurrentPeriodAmount field value
// and a boolean to check if the value has been set.
func (o *GrowthCount) GetCurrentPeriodAmountOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.CurrentPeriodAmount, true
}

// SetCurrentPeriodAmount sets field value
func (o *GrowthCount) SetCurrentPeriodAmount(v int32) {
	o.CurrentPeriodAmount = v
}

// GetFirstPeriodBackAmount returns the FirstPeriodBackAmount field value
func (o *GrowthCount) GetFirstPeriodBackAmount() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.FirstPeriodBackAmount
}

// GetFirstPeriodBackAmountOk returns a tuple with the FirstPeriodBackAmount field value
// and a boolean to check if the value has been set.
func (o *GrowthCount) GetFirstPeriodBackAmountOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.FirstPeriodBackAmount, true
}

// SetFirstPeriodBackAmount sets field value
func (o *GrowthCount) SetFirstPeriodBackAmount(v int32) {
	o.FirstPeriodBackAmount = v
}

// GetSecondPeriodBackAmount returns the SecondPeriodBackAmount field value
func (o *GrowthCount) GetSecondPeriodBackAmount() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.SecondPeriodBackAmount
}

// GetSecondPeriodBackAmountOk returns a tuple with the SecondPeriodBackAmount field value
// and a boolean to check if the value has been set.
func (o *GrowthCount) GetSecondPeriodBackAmountOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.SecondPeriodBackAmount, true
}

// SetSecondPeriodBackAmount sets field value
func (o *GrowthCount) SetSecondPeriodBackAmount(v int32) {
	o.SecondPeriodBackAmount = v
}

func (o GrowthCount) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GrowthCount) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["current_period_amount"] = o.CurrentPeriodAmount
	toSerialize["first_period_back_amount"] = o.FirstPeriodBackAmount
	toSerialize["second_period_back_amount"] = o.SecondPeriodBackAmount
	return toSerialize, nil
}

type NullableGrowthCount struct {
	value *GrowthCount
	isSet bool
}

func (v NullableGrowthCount) Get() *GrowthCount {
	return v.value
}

func (v *NullableGrowthCount) Set(val *GrowthCount) {
	v.value = val
	v.isSet = true
}

func (v NullableGrowthCount) IsSet() bool {
	return v.isSet
}

func (v *NullableGrowthCount) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGrowthCount(val *GrowthCount) *NullableGrowthCount {
	return &NullableGrowthCount{value: val, isSet: true}
}

func (v NullableGrowthCount) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGrowthCount) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


