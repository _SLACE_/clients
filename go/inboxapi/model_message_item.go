/*
Inbox

Inbox API can be used for low level integration and enables direct communication with you contact base.   In most cases you will use our interaction templates for communication, but our api also allows you to send message directly.

API version: 1.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package inboxapi

import (
	"encoding/json"
)

// checks if the MessageItem type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &MessageItem{}

// MessageItem struct for MessageItem
type MessageItem struct {
	Type *MessageItemType `json:"type,omitempty"`
	Text *string `json:"text,omitempty"`
	Payload *string `json:"payload,omitempty"`
	Description *string `json:"description,omitempty"`
	Items []MessageItem `json:"items,omitempty"`
}

// NewMessageItem instantiates a new MessageItem object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMessageItem() *MessageItem {
	this := MessageItem{}
	return &this
}

// NewMessageItemWithDefaults instantiates a new MessageItem object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMessageItemWithDefaults() *MessageItem {
	this := MessageItem{}
	return &this
}

// GetType returns the Type field value if set, zero value otherwise.
func (o *MessageItem) GetType() MessageItemType {
	if o == nil || IsNil(o.Type) {
		var ret MessageItemType
		return ret
	}
	return *o.Type
}

// GetTypeOk returns a tuple with the Type field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageItem) GetTypeOk() (*MessageItemType, bool) {
	if o == nil || IsNil(o.Type) {
		return nil, false
	}
	return o.Type, true
}

// HasType returns a boolean if a field has been set.
func (o *MessageItem) HasType() bool {
	if o != nil && !IsNil(o.Type) {
		return true
	}

	return false
}

// SetType gets a reference to the given MessageItemType and assigns it to the Type field.
func (o *MessageItem) SetType(v MessageItemType) {
	o.Type = &v
}

// GetText returns the Text field value if set, zero value otherwise.
func (o *MessageItem) GetText() string {
	if o == nil || IsNil(o.Text) {
		var ret string
		return ret
	}
	return *o.Text
}

// GetTextOk returns a tuple with the Text field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageItem) GetTextOk() (*string, bool) {
	if o == nil || IsNil(o.Text) {
		return nil, false
	}
	return o.Text, true
}

// HasText returns a boolean if a field has been set.
func (o *MessageItem) HasText() bool {
	if o != nil && !IsNil(o.Text) {
		return true
	}

	return false
}

// SetText gets a reference to the given string and assigns it to the Text field.
func (o *MessageItem) SetText(v string) {
	o.Text = &v
}

// GetPayload returns the Payload field value if set, zero value otherwise.
func (o *MessageItem) GetPayload() string {
	if o == nil || IsNil(o.Payload) {
		var ret string
		return ret
	}
	return *o.Payload
}

// GetPayloadOk returns a tuple with the Payload field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageItem) GetPayloadOk() (*string, bool) {
	if o == nil || IsNil(o.Payload) {
		return nil, false
	}
	return o.Payload, true
}

// HasPayload returns a boolean if a field has been set.
func (o *MessageItem) HasPayload() bool {
	if o != nil && !IsNil(o.Payload) {
		return true
	}

	return false
}

// SetPayload gets a reference to the given string and assigns it to the Payload field.
func (o *MessageItem) SetPayload(v string) {
	o.Payload = &v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *MessageItem) GetDescription() string {
	if o == nil || IsNil(o.Description) {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageItem) GetDescriptionOk() (*string, bool) {
	if o == nil || IsNil(o.Description) {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *MessageItem) HasDescription() bool {
	if o != nil && !IsNil(o.Description) {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *MessageItem) SetDescription(v string) {
	o.Description = &v
}

// GetItems returns the Items field value if set, zero value otherwise.
func (o *MessageItem) GetItems() []MessageItem {
	if o == nil || IsNil(o.Items) {
		var ret []MessageItem
		return ret
	}
	return o.Items
}

// GetItemsOk returns a tuple with the Items field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageItem) GetItemsOk() ([]MessageItem, bool) {
	if o == nil || IsNil(o.Items) {
		return nil, false
	}
	return o.Items, true
}

// HasItems returns a boolean if a field has been set.
func (o *MessageItem) HasItems() bool {
	if o != nil && !IsNil(o.Items) {
		return true
	}

	return false
}

// SetItems gets a reference to the given []MessageItem and assigns it to the Items field.
func (o *MessageItem) SetItems(v []MessageItem) {
	o.Items = v
}

func (o MessageItem) MarshalJSON() ([]byte, error) {
	toSerialize,err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o MessageItem) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Type) {
		toSerialize["type"] = o.Type
	}
	if !IsNil(o.Text) {
		toSerialize["text"] = o.Text
	}
	if !IsNil(o.Payload) {
		toSerialize["payload"] = o.Payload
	}
	if !IsNil(o.Description) {
		toSerialize["description"] = o.Description
	}
	if !IsNil(o.Items) {
		toSerialize["items"] = o.Items
	}
	return toSerialize, nil
}

type NullableMessageItem struct {
	value *MessageItem
	isSet bool
}

func (v NullableMessageItem) Get() *MessageItem {
	return v.value
}

func (v *NullableMessageItem) Set(val *MessageItem) {
	v.value = val
	v.isSet = true
}

func (v NullableMessageItem) IsSet() bool {
	return v.isSet
}

func (v *NullableMessageItem) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMessageItem(val *MessageItem) *NullableMessageItem {
	return &NullableMessageItem{value: val, isSet: true}
}

func (v NullableMessageItem) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMessageItem) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


